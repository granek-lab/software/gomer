
from optparse import OptionParser
import sys
import glob


from parse_gomer_output import ParseGomerOutput


def main():
	usage = 'usage: %prog [-h/--help] GOMER_OUTPUT_FILE_GLOBS'
	parser = OptionParser(usage)
	
	(options, args) = parser.parse_args()
	if len(args) < 1:
		print >>sys.stderr, ' '.join(sys.argv)
		parser.error("incorrect number of arguments")

	data_dict = {}
	feature_names = {}
	file_count = 0
	for gomer_file_glob in args:
		for gomer_file in glob.glob(gomer_file_glob):
			gomer_output_filehandle = file(gomer_file)
			(all_feature_hash,
			 regulated_hash_not_used,
			 excluded_hash_not_used,
			 unknown_feature_hash_not_used,
			 gomer_output_skipped_lines,
			 param_hash) = ParseGomerOutput(gomer_output_filehandle)
			file_count += 1
			conc_data_list = param_hash['Concentration Data List']
			conc_data_list.sort()
			conc_data_list.reverse()
			conc_data_string = ''


			for matrix_type, matrix_name, free_conc, free_conc_ratio, max_ka, min_ka, \
				max_ka__min_ka_ratio, max_ka_sequence, min_ka_sequence, information,  \
				pseudo_temp in conc_data_list:
				conc_data_string += '\t'.join((matrix_name, matrix_type, free_conc_ratio)) + '\t'
			data_dict[conc_data_string] = {}
			for key in all_feature_hash:
				feature = all_feature_hash[key]
				feature_names[feature.name] = None
				data_dict[conc_data_string][feature.name] = feature.score

	feature_name_list = feature_names.keys()
	feature_name_list.sort()
	print '\t'.join(feature_name_list)
	for conc_data_string in data_dict:
		print '\t'.join([str(data_dict[conc_data_string].get(feature_name, ''))
						 for feature_name in feature_name_list]) + '\t\t' + conc_data_string
	print '# File Count:', file_count
	
if __name__ == '__main__':
	main()
