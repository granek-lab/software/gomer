#! /usr/bin/env python
from __future__ import division
#@+leo
#@+node:0::@file regulated_feature_parser_gomer.py
#@+body
#@@first
#@@first
#@@language python


#@<<imports>>
#@+node:1::<<imports>>
#@+body
##import os
##os.environ['PYCHECKER'] = '--allglobals --stdlib'
##import pychecker.checker

import os
import re
import sys

import check_python_version
check_python_version.CheckVersion()

# from parser_gomer import Parser
from parser_gomer import ParseError
from open_file import OpenFile


#@-body
#@-node:1::<<imports>>


#@<<regular expressions>>
#@+node:3::<<regular expressions>>
#@+body
COMMENT_TOKEN = '#'
comment_match = re.compile('^\s*' + COMMENT_TOKEN + '(.*)')
empty_line_match = re.compile('^\s*$')

#@-body
#@-node:3::<<regular expressions>>



#@+others
#@+node:4::def ParseRegulatedFeatures
#@+body
def ParseRegulatedFeatures(filename):
	
	#@<<doc string>>
	#@+node:1::<<doc string>>
	#@+body
	"""
	File Format:
	any line which is empty, or contains only white space is ignored
	# Comment line 
	
	all comment lines are ignored
	
	FEATURENAME:a unique FEATURE label 
	STATUS: 'regulated', 'unregulated', or 'excluded'
	REGULATORS (optional): FEATURENAMES joined by '&' or '|'
	
	
	
	FEATURENAME			STATUS			REGULATORS		
	
	
	Any FEATURE that is not explicitly named in the regulated feature file is considered to be unregulated.
	
	Features named in the regulated feature file, for which only the feature name is given, (no other information, including status), are considered regulated.  In other words, the regulated feature file can be a simple list consisting solely of feature names.

	The sets of feature names returned by this parser are not overlapping.  The order of feature status precedence (from highest to lowest) is: Excluded, Unregulated, Regulated.  If a feature name occurs more than once in the regulated feature file, with different tags, the status with highest precedence is used, and the feature name is purged from the other sets (hashes) where it occurs.
	It is important to remember that a feature could be named using two different names, and this parser would not be able to tell.

	----------------------------
	returns:

	regulated_feature_names, excluded_feature_names, unregulated_feature_names
	These are dictionaries where the values are feature names as parsed from the feature file, and the keys are uppercase versions of these names.
	"""
	#@-body
	#@-node:1::<<doc string>>

	# Parser.Parse(self)  # call the base class method
	filehandle = OpenFile(filename, 'Regulated Features','r')

##	regulated_orf_hash = {}
##	excluded_orf_hash = {}
##	unregulated_orf_hash = {}
##	regulated_feature_names = []
##	excluded_feature_names = []
##	unregulated_feature_names = []
	regulated_feature_names = {}
	excluded_feature_names = {}
	unregulated_feature_names = {}
	line_number = 0

	for	line in filehandle:
		line_number += 1

		if empty_line_match.search(line):
			continue
		elif comment_match.search(line):
			continue
		
		# split_line = line.rstrip('\n').split()
		# split_line = line.rstrip('\n').split('\t+')
		# split_line = re.split('[\t]+', line.rstrip('\n'))
		line = line.strip()
		split_line = re.split('[\t]+', line)
		## print >>sys.stderr, 'split_line:', split_line
		if len(split_line) == 1:
			feature_name = split_line[0].strip()
			status = 'REGULATED'
			# print >>sys.stderr, 'Only a name is supplied for', feature_name, '. Making it a REGULATED feature.'
		elif len(split_line) < 2:
			raise RegulatedFeatureParseError(filename, line, line_number,
										 '\nERROR: Missing data fields, only: '
										 + str(len(split_line)) + ' fields present\n')
		else:
			feature_name = split_line[0].strip()
			status = split_line[1].strip()
			regulators = split_line[2:]

##		if status.upper() == 'REGULATED':
##			regulated_orf_hash[feature_name.upper()] = regulators
##		elif status.upper() == 'UNREGULATED':
##			unregulated_orf_hash[feature_name.upper()] = 1
##		elif status.upper() == 'EXCLUDED':
##			excluded_orf_hash[feature_name.upper()] = 1
##		else:
##			raise RegulatedORFParseError(filename, line, line_number,
##										 '\nERROR: Unknown status: ' + status + '\n')

# ##------------------------------------------------------------------------------
# 		if status.upper() == 'REGULATED':
# 			regulated_feature_names.append(feature_name.upper())
# 		elif status.upper() == 'UNREGULATED':
# 			unregulated_feature_names.append(feature_name.upper())
# 		elif status.upper() == 'EXCLUDED':
# 			excluded_feature_names.append(feature_name.upper())
# 		else:
# 			raise RegulatedFeatureParseError(filename, line, line_number,
# 										 '\nERROR: Unknown status: ' + status + '\n')
# ##------------------------------------------------------------------------------

##------------------------------------------------------------------------------
		if status.upper() == 'REGULATED':
			regulated_feature_names[feature_name.upper()] = feature_name
		elif status.upper() == 'UNREGULATED':
			unregulated_feature_names[feature_name.upper()] = feature_name
		elif status.upper() == 'EXCLUDED':
			excluded_feature_names[feature_name.upper()] = feature_name
		else:
			raise RegulatedFeatureParseError(filename, line, line_number,
										 '\nERROR: Unknown status: ' + status + '\n')
##------------------------------------------------------------------------------
	for excluded_feature in excluded_feature_names:
		if excluded_feature in unregulated_feature_names:
			del unregulated_feature_names[excluded_feature]
			print >>sys.stderr, 'Excluded feature is also listed as Unregulated.   It will be considered Excluded:', excluded_feature
		if excluded_feature in regulated_feature_names:
			del regulated_feature_names[excluded_feature]
			print >>sys.stderr, 'Excluded feature is also listed as Regulated.     It will be considered Excluded:', excluded_feature

	for unregulated_feature in unregulated_feature_names:
		if unregulated_feature in regulated_feature_names:
			del regulated_feature_names[unregulated_feature]
			print >>sys.stderr, 'Unregulated feature is also listed as Regulated.  It will be considered Unregulated:', unregulated_feature

	filehandle.close()
	## return regulated_orf_hash, excluded_orf_hash, unregulated_orf_hash
	return regulated_feature_names, excluded_feature_names, unregulated_feature_names




#@-body
#@-node:4::def ParseRegulatedFeatures
#@-others



#@<<Parser Errors>>
#@+node:5::<<Parser Errors>>
#@+body
#@@code
#@+others
#@+node:1::RegulatedORFParseError
#@+body
class RegulatedFeatureParseError(ParseError):
	pass
#@-body
#@-node:1::RegulatedORFParseError
#@-others




#@-body
#@-node:5::<<Parser Errors>>


#@<<command line>>
#@+node:6::<<command line>>
#@+body
#@<<def run>>
#@+node:3::<<def run>>
#@+body
def run (regulated_feature_filename=None):
	EXPECTED_ARGUMENTS = 1
	if regulated_feature_filename :
		print 'run called with parameters'
	elif len(sys.argv) == (EXPECTED_ARGUMENTS + 1):
			regulated_feature_filename = sys.argv[1]
	else :
		usage()

	if not os.path.exists(regulated_feature_filename):
		print "File doesn't exist:", regulated_feature_filename
		sys.exit(1)

	
	regulated_features, excluded_features, unregulated_features = ParseRegulatedFeatures(regulated_feature_filename)
	# print Parse(regulated_orf_filename)

	print '\n'.join([feature.ljust(15) + 'REGULATED' for feature in regulated_features.keys()])
	print '\n'.join([feature.ljust(15) + 'UNREGULATED' for feature in unregulated_features.keys()])
	print '\n'.join([feature.ljust(15) + 'EXCLUDED' for feature in excluded_features.keys()])

#@-body
#@-node:3::<<def run>>


#@<<def usage>>
#@+node:1::<<def usage>>
#@+body
def usage () :
	print '-'*70 + '\n', 'Arguments given (', len(sys.argv), '):', sys.argv,  """
	1. filename - Name of a regulated FEATURE file
	""" + '\n' + '-'*70
	sys.exit(1)

#@-body
#@-node:1::<<def usage>>



#@<<if main>>
#@+node:2::<<if main>>
#@+body
if __name__ == '__main__':
	print os.getcwd()
	run()
#@-body
#@-node:2::<<if main>>


#@-body
#@-node:6::<<command line>>



#@<<how to run>>
#@+node:2::<<how to run>>
#@-node:2::<<how to run>>
#@-body
#@-node:0::@file regulated_feature_parser_gomer.py
#@-leo
