#! /usr/bin/env python

"""
A script for testing to insure that the GetSlices and GetSiteKappa functions of a given kappa module return the same values (i.e. it checks to be sure GetSlices is working correctly).

Only works on Cooperative and Competative interactions
"""
from __future__ import division; division

import getopt
import sys
from test_reg_region_kappa import print_kappa_help, Matrix, Chrom, Feature
from gomer_load_functions import parse_parameter_string
from runtime_import import RuntimeImport

# debug = sys.stdout

FLANK_LENGTH=100
##---------------------------------------------------------------
def test_kappa(kappa_filename, parameter_string):
##====================================================
##====================================================
	parameter_dict = parse_parameter_string(parameter_string)
	module = RuntimeImport(kappa_filename)


	if module.TYPE in ('CooperativityModel',):
		use_strand = False
		primary_matrix = Matrix(10,'primary')
		secondary_matrix = Matrix(14,'secondary')
		coop_model = module.CoopModel(primary_matrix, secondary_matrix, **parameter_dict)
		test_coop_kappa(coop_model, module.NAME, use_strand, primary_matrix,
						secondary_matrix)
	elif module.TYPE in ('CompetitiveModel',):
		use_strand = False
		primary_matrix = Matrix(10,'primary')
		secondary_matrix = Matrix(14,'secondary')
		comp_model = module.CompModel(primary_matrix, secondary_matrix, **parameter_dict)
		test_coop_kappa(comp_model, module.NAME, use_strand, primary_matrix,
						secondary_matrix)
	else:
		raise 'Unknown Model type: ' + module.TYPE

def test_coop_kappa(coop_model, model_name, use_strand,
					primary_matrix, secondary_matrix):
	chrom = Chrom(10000)
	FEATURE_TO_PRIMARY_SITE = 100
	feature_list = []
	feature_list.append(Feature('+', 500, 600, chrom))
	feature_list.append(Feature('-', 2000, 1500, chrom))
	feature_list.append(Feature('+', 100, 400, chrom))
	feature_list.append(Feature('-', 9900, 9600, chrom))
	
	error_found = False
	for feature_index in range(len(feature_list)):
		feature = feature_list[feature_index]
		## print >>debug, feature.Name
		
		primary_site_strand = feature.Strand
		if feature.Strand == '+':
			primary_site_hitindex = feature.Start - FEATURE_TO_PRIMARY_SITE
		elif feature.Strand == '-':
			primary_site_hitindex = feature.Start + FEATURE_TO_PRIMARY_SITE
		else:
			raise



		coop_slice_tuple_list = coop_model.GetSlices(primary_site_hitindex, primary_site_strand, feature)
		regulatory_indices = [] # a lookup table for indices that fall within the regulatory region
		for	start, stop, plus_kappa_slice, minus_kappa_slice in coop_slice_tuple_list:
			regulatory_indices += range(start,stop)
		regulatory_indices = dict(zip(regulatory_indices, [None]*len(regulatory_indices)))
		## for start_coop_slice, stop_coop_slice, for_coop_kappa_slice, rev_coop_kappa_slice in coop_slice_tuple_list:
		for	start, stop, plus_kappa_slice, minus_kappa_slice in coop_slice_tuple_list: 
			## Check to be sure that everything not in the ranges returned by
			## GetSlices is zero (actually, only select regions flanking the ranges)
			for coop_hitindex in range(start-FLANK_LENGTH, start) + range(stop, stop+FLANK_LENGTH+1):
				plus_strand_kappa = coop_model.GetSiteKappa(primary_site_hitindex,
															primary_site_strand,
															coop_hitindex, '+', feature)
				minus_strand_kappa = coop_model.GetSiteKappa(primary_site_hitindex,
															  primary_site_strand,
															  coop_hitindex, '-', feature)
															  
				if coop_hitindex not in regulatory_indices:
					if plus_strand_kappa <> 0:
						print 'non-regulatory region kappa not 0: + ', primary_site_hitindex, primary_site_strand, coop_hitindex, plus_strand_kappa
						error_found = True
					if minus_strand_kappa <> 0:
						print 'non-regulatory region kappa not 0: - ', primary_site_hitindex, primary_site_strand, coop_hitindex, plus_strand_kappa
						error_found = True
						
				## print >>debug, 'coop_hitindex:', coop_hitindex, '+:', plus_strand_kappa, '-:', minus_strand_kappa
##				if plus_strand_kappa <> 0:
##					message = 'area outside of kappa range should be equal to ' + \
##							  'zero, it is: ' + str(plus_strand_kappa) + \
##							  '\nprimary_site_hitindex ' + str(primary_site_hitindex) + \
##							  '\nprimary_site_strand ' + str(primary_site_strand) + \
##							  '\ncoop_hitindex' + str(coop_hitindex) + \
##							  '\nfeature' + str(feature)
##					raise StandardError, message
##				if minus_strand_kappa <> 0:
##					message = 'area outside of kappa range should be equal to ' + \
##							  'zero, it is: ' + str(minus_strand_kappa) + \
##							  '\nprimary_site_hitindex ' + str(primary_site_hitindex) + \
##							  '\nprimary_site_strand ' + str(primary_site_strand) + \
##							  '\ncoop_hitindex' + str(coop_hitindex) + \
##							  '\nfeature' + str(feature)
##					raise StandardError, message
			## Check to be sure that the values of the Kappa slices are the same
			## as those returned by GetSiteKappa
			
			hitindex_list = range(start,stop)
			if len (hitindex_list) <> len(plus_kappa_slice):
				raise
			for index in range(len(plus_kappa_slice)):
				##----------------------------------------------------
				slice_kappa = plus_kappa_slice[index]
				coop_hitindex = hitindex_list[index]
				get_kappa = coop_model.GetSiteKappa(primary_site_hitindex,
													primary_site_strand,
													coop_hitindex, '+', feature)
				## print >>debug, 'coop_hitindex:', coop_hitindex, '+:', get_kappa, slice_kappa
				if slice_kappa <> get_kappa:
					message = 'GetSlices and GetSiteKappa return ' + \
							  'different kappa values (plus strand): ' \
							  + str(slice_kappa) + ', ' + str(get_kappa) 
					raise message
				##----------------------------------------------------
				slice_kappa = minus_kappa_slice[index]
				coop_hitindex = hitindex_list[index]
				get_kappa = coop_model.GetSiteKappa(primary_site_hitindex,
													primary_site_strand,
													coop_hitindex, '-', feature)
				## print >>debug, 'coop_hitindex:', coop_hitindex, '+:', get_kappa, slice_kappa
				if slice_kappa <> get_kappa:
					message = 'GetSlices and GetSiteKappa return ' + \
							  'different kappa values (minus strand): ' \
							  + str(slice_kappa) + ', ' + str(get_kappa) 
					raise message
				##----------------------------------------------------
	if not error_found:
		print >>sys.stderr, 'Perfect correspondence between GetSiteKappa and GetSlices'

def main():
	def usage():
		usage_string = ' '.join(('usage:', sys.argv[0], 'KAPPA_FILE\n',
								 ' '* ((len('usage:'))-1), sys.argv[0],
								 'KAPPA_FILE PARAMETER_STRING\n',
								 '\n-o/--output FILE : output to FILE in eps format'
								 '\n-p/--parameter:    print parameter string on plot'
								 '\n-s/--single:       only print the first kappagraph\n'
								 ))
		print >>sys.stderr, usage_string
##----------------------------------------------------------
	try:
		opts, args = getopt.getopt(sys.argv[1:], "hpso:t",
								   ["help", "parameter", "output=",
									'single', 'test_slice'])
	except getopt.GetoptError:
		# print help information and exit:
		usage()
		sys.exit(2)
	output = None
	single = False
	parameter = False
	test_slice = False
	for o, a in opts:
		if o in ("-h", "--help"):
			usage()
			sys.exit()

	if len(args) == 1:
		kappa_filename = args[0]
		print_kappa_help(kappa_filename)
	elif len(args) == 2:
		kappa_filename = args[0]
		parameter_string = args[1]
		test_kappa(kappa_filename, parameter_string)
	else:
		usage()
		sys.exit()
	

if __name__ == '__main__':
	main()
