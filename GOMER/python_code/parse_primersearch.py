#! /usr/bin/env python

from __future__ import division
import re
import sys
import os
import md5
import roman

from parser_gomer import ParseError
from integer_string_compare import sort_numeric_string
from optparse import OptionParser
from coordinate_feature_parser_gomer import CoordinateFeatureParse

MAX_AMPLIMER_LENGTH = 7000

COMMENT_TOKEN = '>'
comment_re = re.compile('^' + COMMENT_TOKEN + '(.+)$')
BASE_ALPHABET = 'acgtACGT'
base_re = re.compile('^[' + BASE_ALPHABET + ']+\s*$')



empty_line_re = re.compile('^\s*$')
primer_name_re = re.compile('^Primer name\s+([a-zA-Z0-9\-\(\)\_\~]+)\s*$')
amplimer_re = re.compile('^Amplimer\s+(\d+)\s*$')
sequence_re = re.compile('^\s*Sequence:\s+(\w+)\s*(\w*)\s*$')
modifiers_re = re.compile('^\s*\[org=Saccharomyces cerevisiae\]\s*\[strain=S288C\]\s*\[moltype=genomic\]\s*\[chromosome=([ivx]+)\]\s*$', re.IGNORECASE)
mito_modifiers_re = re.compile('^\s*\[org=Saccharomyces cerevisiae\]\s*\[strain=S288C\]\s*\[moltype=genomic\]\s*\[location=mitochondrion\]\s*\[top=circular\]\s*$', re.IGNORECASE)
forward_strand_re = re.compile('^\s*([acgt]+)\s+hits forward strand at (\d+) with (\d+) mismatches\s*$', re.IGNORECASE)
reverse_strand_re = re.compile('^\s*([acgt]+)\s+hits reverse strand at \[(\d+)\] with (\d+) mismatches\s*$', re.IGNORECASE)
amplimer_length_re = re.compile('^\s*Amplimer length: (\d+) bp\s*$')


"./primersearch -sequences /tmp/yeast_genome.fsa -primers /tmp/Yeast_Intergenic.psformat -mismatchpercent 0 -stdout -auto > /tmp/Yeast_Intergenic.primersearch_0"

SEQUENCE_TOKEN = '%SEQUENCE' 


class Amplimer:
	def __init__(self, primer_name, amplimer_num, sequence_name, chrom,
				 for_primer, rev_primer, for_bp, rev_bp, for_mismatch,
				 rev_mismatch, amplimer_length):
		self.primer_name     =  primer_name.replace('~', ' ')
		self.amplimer_num    =  amplimer_num
		self.sequence_name	 =  sequence_name	
		self.chrom			 =  chrom
		if chrom.lower() == 'mitochondrion':
			self.chrom_num		 =  17
		else:
			self.chrom_num		 =  roman.roman_to_int(chrom)
		self.for_primer		 =  for_primer		
		self.rev_primer		 =  rev_primer		
		self.for_bp			 =  for_bp
		self.rev_bp			 =  rev_bp			
		self.for_mismatch	 =  for_mismatch	
		self.rev_mismatch	 =  rev_mismatch	
		self.amplimer_length =  amplimer_length
		self.size = amplimer_length
		self.start           =  for_bp
		self.stop            =  (self.start + self.size) - 1
		if self.stop < self.start:
			raise "self.stop < self.start:"
		else:
			self.left = self.start
			self.right = self.stop

def parse_primersearch_file(filehandle, max_amplimer_length):
	amplimer_dict = {}
	duplicate_amplimer_dict = {}
	no_amplimer_list = []
	line = ' '
	potential_no_amplimer = None
	primer_hash = {}
	primer_count = 0
	while(line):
		current_amplimer_dict = amplimer_dict
		if potential_no_amplimer and (potential_no_amplimer not in amplimer_dict):
			no_amplimer_list.append(potential_no_amplimer)
			potential_no_amplimer = None
		line = filehandle.readline()
	## for line in filehandle:
		# line = line.rstrip()
		if not(empty_line_re.search(line)) and not (primer_name_re.search(line)):
			raise StandardError, "Doesn't fit format:\n" + line 
		elif empty_line_re.search(line):
			continue
		elif primer_name_re.search(line):
			cur_primer_name = primer_name_re.search(line).group(1)
			cur_primer_name = cur_primer_name.replace('~', ' ')
			cur_primer_ID = cur_primer_name.upper()
			## In the input to primersearch, spaces in primer names were replaced with tildes
			## because primersearch expects the input to be whitespace separated
			if cur_primer_ID in primer_hash:
				current_amplimer_dict = duplicate_amplimer_dict
				print >>sys.stderr, '# DUPLICATE PRIMER:', cur_primer_name
			else:
				primer_hash[cur_primer_ID] = 1
			primer_count += 1
			line = filehandle.readline()
			if empty_line_re.search(line):
				no_amplimer_list.append(cur_primer_name)
				continue
			elif not amplimer_re.search(line):
				raise StandardError, "Doesn't fit format:\n" + line 
			else:
				while(amplimer_re.search(line)):
					amplimer_num = amplimer_re.search(line).group(1)
					sequence_match = sequence_re.search(filehandle.readline())
					modifiers_line = filehandle.readline()
					modifiers_match = modifiers_re.search(modifiers_line)
					mito_modifiers_match = mito_modifiers_re.search(modifiers_line)
					forward_strand_match  = forward_strand_re.search(filehandle.readline())
					reverse_strand_match  =	reverse_strand_re.search(filehandle.readline())
					amplimer_length_match =	amplimer_length_re.search(filehandle.readline())

					if not (sequence_match and
							(modifiers_match or mito_modifiers_match) and
							forward_strand_match and reverse_strand_match
							and amplimer_length_match):
						if not sequence_match :
							print >>sys.stderr, 'Problem with sequence_match!!!!!!!'
						if not (modifiers_match or mito_modifiers_match) :
							print >>sys.stderr, 'Problem with modifiers_match or mito_modifiers_match!!!!!!!'
						if not forward_strand_match:
							print >>sys.stderr, 'Problem with forward_strand_match!!!!!!!'
						if not reverse_strand_match:
							print >>sys.stderr, 'Problem with reverse_strand_match!!!!!!!'
						if not amplimer_length_match:
							print >>sys.stderr, 'Problem with amplimer_length_match!!!!!!!'
							
						raise StandardError, "Doesn't fit format:\n" + \
							  'Primer name: ' + cur_primer_name + \
							  '\tAmplimer: ' + str(amplimer_num) + '\n'

					sequence_name1 = sequence_match.group(1)
					sequence_name2 = sequence_match.group(2)
					if sequence_name1 <> sequence_name2:
						raise StandardError, "Two sequence names don't match:\n" + \
							  'Primer name: ' + cur_primer_name + \
							  '\tAmplimer: ' + str(amplimer_num) + '\n"' +\
							  sequence_name1 + '" != "' + sequence_name2 + '"\n'
					if modifiers_match:
						chrom = modifiers_match.group(1)
					elif mito_modifiers_match:
						chrom = 'mitochondrion'
					for_primer = forward_strand_match.group(1)
					for_bp = int(forward_strand_match.group(2))
					for_mismatch = int(forward_strand_match.group(3))
					rev_primer = reverse_strand_match.group(1)
					rev_bp = int(reverse_strand_match.group(2))
					rev_mismatch = int(reverse_strand_match.group(3))
					amplimer_length = int(amplimer_length_match.group(1))
					if amplimer_length > max_amplimer_length:
						print >> sys.stderr, 'skipping amplimer which is larger than max (', \
							  max_amplimer_length, '):', amplimer_length, '\n',\
							  'Primer name:', cur_primer_name, \
							  '\tAmplimer:', amplimer_num
						potential_no_amplimer = cur_primer_ID
						# if the only amplimer(s) for this primer are longer than the
						# max_amplimer_length, then this primer needs to be addes to
						# the no_amplimer_list.
						# We check the amplimer_dict at the top of the loop

					else:
						new_amplimer = Amplimer(cur_primer_name, amplimer_num,
												sequence_name1, chrom, for_primer,
												rev_primer, for_bp, rev_bp, for_mismatch,
												rev_mismatch, amplimer_length)
						## amplimer_dict.setdefault(cur_primer_name, []).append(new_amplimer)
						current_amplimer_dict.setdefault(cur_primer_ID, []).append(new_amplimer)
					line = filehandle.readline()
	if potential_no_amplimer and potential_no_amplimer not in amplimer_dict:
		no_amplimer_list.append(potential_no_amplimer)
		potential_no_amplimer = None

	no_amplimer_list.sort() ##SORT


	##-------------------------------------------------------------------------
	## There are a few primer pair sets which occur more than once in the original
	## primer set files supplied by research genetics, because of this, their
	## amplimers occur more than once in the primersearch output.
	## parse_primersearch.py detects primer set names which occur more than once
	## in the primersearch output, it checks to be sure that the primer sequences
	## are identical, if they are, all duplicates but one is discarded, and an
	## error is reported to standard error.  If they are not identical it raises
	## an exception.
	##--------------------------------------------
	
	for amplimer_key in duplicate_amplimer_dict:
		dup_amp_for = duplicate_amplimer_dict[amplimer_key][0].for_primer
		dup_amp_rev = duplicate_amplimer_dict[amplimer_key][0].rev_primer
		cur_amp_for = amplimer_dict[amplimer_key][0].for_primer
		cur_amp_rev = amplimer_dict[amplimer_key][0].rev_primer
		if (((dup_amp_for == cur_amp_for) and (dup_amp_rev == cur_amp_rev)) or
			((dup_amp_rev == cur_amp_for) and (dup_amp_for == cur_amp_rev))):
			print >>sys.stderr, 'Primer sequences occur more than once for:', duplicate_amplimer_dict[amplimer_key][0].primer_name
		else:
			raise 'Primer pair name used more than once with different primers:', duplicate_amplimer_dict[amplimer_key][0].primer_name
	return amplimer_dict, no_amplimer_list, primer_hash, primer_count

##--------------------------------------------------------------------
class Seq(object):
	def __init__(self, seq_id, sequence):
		self.seq_id = seq_id
		self.sequence = sequence
		self.MD5 = md5.new(sequence).hexdigest()

def parse_FASTA_file(filename):
	filehandle = file(filename)
	cur_line = ' '
	line_count = 0

	sequences_list = []
	while cur_line:
		previous_file_pos = filehandle.tell()
		cur_line = filehandle.readline()
		line_count += 1
		if empty_line_re.match(cur_line):
			continue
		elif comment_re.match(cur_line):
			comment = comment_re.search(cur_line).group(1)
			ref, sequence_id, modifier_string = comment.split('|')
			sequence_id = sequence_id.strip()
			cur_seq_list = []
			while cur_line:
				previous_file_pos = filehandle.tell()
				cur_line = filehandle.readline()
				line_count += 1
				if base_re.match(cur_line):
					cur_seq_list.append(cur_line.strip())
				elif empty_line_re.match(cur_line) or comment_re.match(cur_line):
					if not cur_seq_list:
						print '|', cur_line, '|'
						raise SequenceParseError(filename,
												 cur_line, line_count,
												 'ERROR: no sequence provided for:' + sequence_id)
					sequence = ''.join(cur_seq_list)
					sequences_list.append(Seq(sequence_id, sequence))
					filehandle.seek(previous_file_pos)
					line_count -= 1
					break
				else:
					raise SequenceParseError(filename,
											 cur_line, line_count,
											 "ERROR: File doesn't match format")
		else:
			raise SequenceParseError(filename,
									 cur_line, line_count,
									 "ERROR: File doesn't match format")
	filehandle.close()
	return sequences_list
##--------------------------------------------------------------------

def main():
	def optparse_init():
		usage = "usage: %prog [options] PRIMERSEARCH_OUTPUT SEQUENCE[s]"
		parser = OptionParser(usage=usage)
		parser.add_option("-a", "--allowed_multiples", dest="allowed_multiples_filename",
						  help="FILE contains names and coordinates of primer sets with multiple ampliers that should be allowed", metavar="FILE")
		parser.add_option("-o", "--output", dest="output_filename",
						  help="output to FILE instead of stdout", metavar="FILE")
		## (options, args) = parser.parse_args()
		return parser

	parser = optparse_init()
	options, args = parser.parse_args()

	if options.output_filename:
		output_handle = file(options.output_filename, 'w')
	else:
		output_handle = sys.stdout

	if options.allowed_multiples_filename:
		## allowed_multiples_filehandle = file(options.allowed_multiples_filename)
		chromosome_hash, allowed_multiple_dict, \
						 ambiguous_feature_names, seq_info_hash = \
						 CoordinateFeatureParse(options.allowed_multiples_filename, {}, {}, {})
	else:
		allowed_multiple_dict = {}

	if len(args) < 2:
		parser.print_help()
		sys.exit(2)
	primersearch_filename = args[0]
	seq_filename_list = args[1:]

	seq_list = []
	for seq_file in seq_filename_list:
		seq_list.extend(parse_FASTA_file(seq_file))
	seq_dict = {}
	for seq in seq_list:
		seq_dict[seq.seq_id] = seq
	
	primersearch_filehandle = file(primersearch_filename)
	amplimer_dict, no_amplimer_list, primer_list, primer_count = parse_primersearch_file(primersearch_filehandle, MAX_AMPLIMER_LENGTH)

	header_string, amplimer_string, multiple_amplimer_string,              \
				   nested_multiple_amplimer_string, allowed_multiple_string, \
				   single_amplimer_count, multiple_amplimer_count, \
				   nested_multiple_amplimer_count, allowed_multiple_count \
				   = generate_output(amplimer_dict, seq_dict, allowed_multiple_dict)

	output_handle.write(header_string + '\n')

	output_handle.write('#' + 'Primer Count:' + str(primer_count) + '\n')
	output_handle.write('#' + 'Total Number of Unique Primers:' + str(len(primer_list)) + '\n')
	output_handle.write('#' + 'Number of Primers with single amplimer:' + str(single_amplimer_count) + '\n')
	if allowed_multiple_dict:
		output_handle.write('#' + 'Number of Primers with allowed multiple amplimers:'
							+ str(allowed_multiple_count) + '\n')
	output_handle.write('#' + 'Number of Primers with nested multiple amplimers:'
						+ str(nested_multiple_amplimer_count) + '\n')
	output_handle.write('#' + 'Number of Primers with no amplimer:' + str(len(no_amplimer_list)) + '\n')
	output_handle.write('#' + 'Number of Primers with multiple amplimers:' + str(multiple_amplimer_count) + '\n')
	## for duplicate in duplicate_primers:
	## 	output_handle.write('#' + ' DUPLICATE PRIMER:' + duplicate + '\n')
	output_handle.write('#' + '-'*40 + '\n#Amplimers:\n#' + '-'*40 + '\n')
	output_handle.write(amplimer_string + '\n')
	if allowed_multiple_dict:
		output_handle.write('#' + '-'*40 + '\n#Allowed Multiple Amplimers:\n#' + '-'*40 + '\n')
		output_handle.write(allowed_multiple_string + '\n')
	output_handle.write('#' + '-'*40 + '\n#Nested Multiple Amplimers:\n#' + '-'*40 + '\n')
	output_handle.write(nested_multiple_amplimer_string + '\n')
	output_handle.write('#' + '-'*40 + '\n#Multiple Amplimers:\n#' + '-'*40 + '\n')
	output_handle.write(multiple_amplimer_string + '\n')
	output_handle.write('#' + '-'*40 + '\n#Primers with no amplimers:\n#' + '-'*40 +'\n')
	output_handle.write(''.join(['#%\t' + feature + '\n' for feature in no_amplimer_list]))

#  	print '#'*60
#  	print '#'*60
#  	print '#'*60
#  	for amp_name in allowed_multiple_dict:
#  		amp = allowed_multiple_dict[amp_name]
#  		print amp_name, amp.Start, amp.Length, amp.Stop, amp.ChromosomeName, amp.Name, amp.CommonName, amp.AllNames, amp.Types
	
	#  print 'CHECK TO BE SURE HITS ARE ON EXPECTED CHROMOSOME, AND OF EXPECTED SIZE!!!!!'

def sort_by_abs(list_of_tuples, sort_pos=0):
	decorated = [(abs(tuple[sort_pos]), tuple) for tuple in list_of_tuples] 
	decorated.sort()
	return [tuple_tuple[1] for tuple_tuple in decorated]

def generate_output(amplimer_dict, seq_id_dict, allowed_multiple_dict):
	sequence_dict = {}
	amplimer_string = '#name\tchrom\tstart\tstop\n'
	multiple_amplimer_string = '#name\tchrom\tstart\tstop\tsize\tfor_mis\trev_mis\n'
	nested_multiple_amplimer_string = '#name\tchrom\tstart\tstop\tsize\tfor_mis\trev_mis\n'
	allowed_multiple_string = '#name\tchrom\tstart\tstop\tsize\tfor_mis\trev_mis\tlength diff\tdistance\n'
	good_amplimer_list = []
	single_amplimer_count = 0
	multiple_amplimer_count = 0
	nested_multiple_amplimer_count = 0
	allowed_multiple_count = 0
	amplimer_keys = amplimer_dict.keys()
	amplimer_keys.sort() ##SORT
##	for amplimer_name in amplimer_dict:
	for amplimerID in amplimer_keys:
		if len(amplimer_dict[amplimerID]) == 1:
			amplimer = amplimer_dict[amplimerID][0]
			sequence_dict[amplimer.chrom_num] = amplimer.sequence_name
			good_amplimer_list.append(amplimer)
			single_amplimer_count += 1
		else:  ## There are multiple amplimers
			innermost, remaining_multiples = get_nested_multiples(amplimer_dict[amplimerID])
			#------------------------------------------------------------------------
			if amplimerID in allowed_multiple_dict:
				best_amp_tup, other_candidates, wrong_chromosome = \
							  select_allowed_multiple(allowed_multiple_dict[amplimerID], amplimer_dict[amplimerID])
				if best_amp_tup:
					best_amp_length_diff, best_amp_distance, best_amp = best_amp_tup
					sequence_dict[best_amp.chrom_num] = best_amp.sequence_name
					allowed_multiple_string += '\t'.join((best_amp.primer_name,
														  str(best_amp.chrom_num),
														  str(best_amp.start),
														  str(best_amp.stop)
														  )) + '\n'
					allowed_multiple_string += ' '.join(('# Best match: length diff:',
														 str(best_amp_length_diff),
														 'distance:',
														 str(best_amp_distance), '\n'))

				for length_diff, distance, amplimer in other_candidates:
					allowed_multiple_string += '#'+'\t'.join((amplimer.primer_name + ':',
															  str(amplimer.chrom_num),
															  str(amplimer.start),
															  str(amplimer.stop),
															  str(amplimer.size),
															  str(amplimer.for_mismatch),
															  str(amplimer.rev_mismatch),
															  str(length_diff),
															  str(distance))) + '\n'

				for amplimer, amp_chrom, template_chrom in wrong_chromosome:
					allowed_multiple_string += '#'+'\t'.join((amplimer.primer_name + ':',
															  str(amplimer.chrom_num),
															  str(amplimer.start),
															  str(amplimer.stop),
															  str(amplimer.size),
															  str(amplimer.for_mismatch),
															  str(amplimer.rev_mismatch),
															  'wrong chromosome '+amp_chrom+' ('+template_chrom+')')) + '\n'

				allowed_multiple_string += '\n'
				allowed_multiple_count += 1
			#------------------------------------------------------------------------
			elif innermost:
				sequence_dict[innermost.chrom_num] = innermost.sequence_name
				nested_multiple_amplimer_string += '\t'.join((innermost.primer_name,
															  str(innermost.chrom_num),
															  str(innermost.start),
															  str(innermost.stop)
															  )) + '\n'
				
				for amplimer in remaining_multiples:
					nested_multiple_amplimer_string += '#'+'\t'.join((amplimer.primer_name + ':',
																	  str(amplimer.chrom_num),
																	  str(amplimer.start),
																	  str(amplimer.stop),
																	  str(amplimer.size),
																	  str(amplimer.for_mismatch),
																	  str(amplimer.rev_mismatch))) + '\n'
				nested_multiple_amplimer_string += '\n'
				nested_multiple_amplimer_count += 1
				
			## MULTIPLE AMPLIMERS
			# multiple_amplimer_string += ','.join([str(amplimer.size) for amplimer in amplimer_dict[amplimer_name]]) + '\n'
			else:
				for amplimer in amplimer_dict[amplimerID]:
					multiple_amplimer_string += '#'+'\t'.join((amplimer.primer_name + ':',
															   str(amplimer.chrom_num),
															   str(amplimer.start),
															   str(amplimer.stop),
															   str(amplimer.size),
															   str(amplimer.for_mismatch),
															   str(amplimer.rev_mismatch))) + '\n'
					sequence_dict[amplimer.chrom_num] = amplimer.sequence_name
				multiple_amplimer_string += '\n'
				multiple_amplimer_count += 1

	decorated_good_amplimer_list = [(amplimer.chrom_num, amplimer.start,
									 amplimer.stop, amplimer)
									for amplimer in good_amplimer_list]
	decorated_good_amplimer_list.sort()
	good_amplimer_list = [amplimer for chrom_num, start, stop, amplimer in
						  decorated_good_amplimer_list]
	for amplimer in good_amplimer_list:
		amplimer_string += '\t'.join((amplimer.primer_name,
									  str(amplimer.chrom_num),
									  str(amplimer.start),
									  str(amplimer.stop)
									  )) + '\n'
		
	chrom_label_keys = sequence_dict.keys()
	chrom_label_keys.sort()
	header_string = ''
	for chrom_label in chrom_label_keys:
		seq_id = sequence_dict[chrom_label]
		header_string += '\t'.join((SEQUENCE_TOKEN,
									seq_id,
									str(chrom_label),
									seq_id_dict[seq_id].MD5
									)) + '\n'
	return header_string, amplimer_string, multiple_amplimer_string, \
		   nested_multiple_amplimer_string, allowed_multiple_string, \
		   single_amplimer_count, multiple_amplimer_count, \
		   nested_multiple_amplimer_count, allowed_multiple_count


def select_allowed_multiple(template, multiples_list):
	## The allowed multiple will be chosen based on the "template"
	## The criteria are:
	##     1. Selected multiple must be on the same chromosome as the template, if there is more than one, use criteria 2.
	##     2. Selected multiple is the one closest in length to the template, if there is more than one, use criteria 3.
	##     3. The selected multiple is the one closest to the template.  The distance is the distance between the closest pair of coordinates (i.e. min([abs(left_a, left_b), abs(left_a, right_b), abs(right_a, left_b), abs(right_a, right_b)])).
	candidates = []
	wrong_chromosome = []
	for cur_amp in multiples_list:
		if int(cur_amp.chrom_num) <> int(template.ChromosomeName):
			wrong_chromosome.append((cur_amp, cur_amp.chrom_num, template.ChromosomeName))
		else:
			length_diff = abs(cur_amp.amplimer_length - template.Length)
			distance = min(abs(cur_amp.start - template.Start),
						   abs(cur_amp.start - template.Stop),
						   abs(cur_amp.stop - template.Start),
						   abs(cur_amp.stop - template.Stop))
			candidates.append((length_diff, distance, cur_amp))
	candidates.sort()
	if candidates:
		best_match = candidates.pop(0)
	else:
		best_match = None
	return best_match, candidates, wrong_chromosome

def nested_sort(amplimer_list):
	"""
	sorts amplimers like this
	#########################################
	     #########################################
	     ######################################
	         #########################################
	            ###############################
 	            ###########################
	              ####################
                           #########################################
	                                                                   ######################
	"""
	nested_list = [(amplimer.left, -amplimer.right, amplimer) for amplimer in amplimer_list]
	nested_list.sort()
	sorted_list = [amplimer for left, right, amplimer in nested_list]
	return sorted_list
	

def get_nested_multiples(multiple_amplimer_list):
	"""
	Takes a list of multiple amplimers produced by PCRing with a single pair of primers

	returns a tuple:
	       - If the amplimer are nested, the innermost (smallest) nested amplimer (the only one that would be produced by an actual PCR reaction), plus a list consisting of the remaining multiple amplimers

		   - otherwise, returns None
	"""
	## be sure all amplimers are on the same chromosome
	chroms = {}
	for amplimer in multiple_amplimer_list:
		chroms[amplimer.chrom] = 1
	if len(chroms) <> 1:
		return None, None
	
	nest_sorted_list = nested_sort(multiple_amplimer_list)
	last_left = nest_sorted_list[0].left
	last_right = nest_sorted_list[0].right
	for amplimer in nest_sorted_list:
		if last_left <= amplimer.left and amplimer.right <= last_right:
			last_left = amplimer.left
			last_right = amplimer.right
		else:
			return None, None
	## if the above for loop survives, the amplimers are all nested,
	## so we return the innermost amplimer, and the remaining list
	innermost = nest_sorted_list.pop(-1)
	return innermost, nest_sorted_list	
	



def main_check_chrom_and_size():
	from parse_PCR_primers import ParsePrimers
	if len(sys.argv) <> 3:
		print >> sys.stderr, 'usage: ', os.path.basename(sys.argv[0]), 'PRIMERSEARCH_OUTPUT', 'CHIP_PRIMER_FILE'
		sys.exit(2)
	primersearch_filename = sys.argv[1]
	primersearch_filehandle = file(primersearch_filename)
	amplimer_dict, no_amplimer_list = \
				   parse_primersearch_file(primersearch_filehandle, MAX_AMPLIMER_LENGTH)
	print 'Primers with no amplimers:\n', '\n'.join(no_amplimer_list)

##----------------------------------------------------------------------
	primer_filename = sys.argv[2]
	chip_primer_hash = ParsePrimers(primer_filename)
	print 'Found', len(chip_primer_hash), 'primer pairs'
##----------------------------------------------------------------------
	print 'CHECK TO BE SURE HITS ARE ON EXPECTED CHROMOSOME, AND OF EXPECTED SIZE!!!!!'
	size_diff_pair_hash = {}
	multiple_amplimer_hash = {}
	for amplimerID in amplimer_dict:
		if amplimerID not in  chip_primer_hash:
			print 'Primer:', amplimerID, 'not found in amplimers'
		else:
			amplimer_match = 0
			chrom_match = 0
			size_differences = []
			size_diff_pairs = []
			chip_primer = chip_primer_hash[amplimerID]
			## print dir(chip_primer), '\n', chip_primer
			# print 'Chip:', chip_primer.chrom_num, chip_primer.size
			for amplimer in amplimer_dict[amplimerID]:
				# print 'Primersearch:', amplimer.chrom_num, amplimer.size
				if (amplimer.chrom_num == chip_primer.chrom_num):
					chrom_match += 1
					if (amplimer.size == chip_primer.size):
						amplimer_match += 1
					else:
						size_differences.append(abs(amplimer.size - chip_primer.size))
						size_diff_pairs.append((amplimer.size - chip_primer.size, amplimer.size))
			if not chrom_match:
				print 'Amplimer found but it is wrong chrom:', amplimerID
			elif not amplimer_match:
				print 'Amplimer found but it is wrong size:', amplimerID, size_differences
				if len(size_diff_pairs) > 1:
					multiple_amplimer_hash[amplimerID] = size_diff_pairs
				abs_sorted = sort_by_abs(size_diff_pairs)
				# diff, amplimer_size = abs_sorted[0]
				size_diff_pair_hash[amplimerID] = abs_sorted[0]
	print '=' * 60
	for amplimerID in size_diff_pair_hash:
		diff, amplimer_size = size_diff_pair_hash[amplimerID]
		print `diff` + '\t' + `amplimer_size` + '\t' +  amplimerID 
	print '=' * 60
	print 'MULTIPLE AMPLIMERS'
	for amplimerID in multiple_amplimer_hash:
		print amplimerID + '\t' + str(multiple_amplimer_hash[amplimerID])

class SequenceParseError(ParseError):
	pass

if __name__ == '__main__':
	main()
	## main_check_chrom_and_size()

