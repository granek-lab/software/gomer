#! /usr/bin/python -t
## #! /usr/bin/env python -t
from __future__ import division

"""
*Description:

Functions for generating random strings from the supplied alphabet (with the same
probability of occurence as is found in the alphabet)

One the random_string() function allows for runs of the same character (more than
one in a row), random_string_no_pairs() does not allow runs of a character.
"""
import random
import getopt
import sys




def random_string_no_pairs(length, alphabet):
	"""
	generates random strings from alphabet (with the same occurence as is found
	in the alphabet)

	characters will not occur in runs

	Formally:
	for a string x1x2x3 ... xn,
	i = [1 .. N],
	~(xi == xi+1)
	"""
	out_string = next_char= random.choice(alphabet)
	for count in range(length - 1):
		while next_char == out_string[-1]:
			next_char= random.choice(alphabet)
		out_string += next_char
	return out_string

def random_string(length, alphabet):
	"""
	generates random strings from alphabet (with the same occurence as is found
	in the alphabet)

	characters are allowed to occur in runs
	"""
	out_string = ''
	for count in range(length):
		out_string += random.choice(alphabet)
	return out_string

def usage():
	print """random_string [-n -r] length alphabet:
	length - the length of the random string
	alphabet - the alphabet from which characters of the string will be chosen
			   (with the same occurence as is found in the alphabet)

	OPTIONS:
	-n --noruns: no runs - should not have more than one of the same character in a row
	-r --runs: (default) allow runs
"""
	sys.exit(1)

def main_run():
	try:
		opts, args = getopt.getopt(sys.argv[1:], "nr", ["noruns", "runs"])
	except getopt.GetoptError:
		# print help information and exit:
		usage()
		sys.exit(2)
	noruns = 0
	runs = 0
	for o, a in opts:
		if o in ("-n", "--noruns"):
			noruns = 1
		if o in ("-r", "--runs"):
			runs = 1

	if len(args) <> 2:
		usage()
	length = int(args[0])
	alphabet = args[1]
	print 'length:', length, 'alphabet:', alphabet
	if runs and noruns:
		usage()
	elif not noruns:
		print random_string(length, alphabet)
	else:
		print random_string_no_pairs(length, alphabet)


if __name__ == "__main__":
	main_run()
