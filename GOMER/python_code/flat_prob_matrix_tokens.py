from __future__ import division
#@+leo
#@comment Created by Leo at Fri Mar  7 17:19:26 2003
#@+node:0::@file flat_prob_matrix_tokens.py
#@+body
#@@first
#@@language python

"""
*Description:

Tokens used in probability matrix files.
"""
##import os
##os.environ['PYCHECKER'] = '--allglobals --stdlib'
##import pychecker.checker


#@<<parse tokens>>
#@+node:1::<<parse tokens>>
#@+body
NAME_TOKEN = '%NAME'
PSEUDO_TEMP_TOKEN = '%PSEUDO_TEMP'
COMMENT_TOKEN = '#'
BASE_LABEL_STRING = 'ACGT'
GAP_PROB_TOKEN = 'N'
#@-body
#@-node:1::<<parse tokens>>


#@-body
#@-node:0::@file flat_prob_matrix_tokens.py
#@-leo
