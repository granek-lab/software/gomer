#! /usr/bin/env python

import re
import os
import sys
import glob

ROCAUC_re = re.compile('^ROC AUC\s*\:\s*([0-9e\.\-]+)\s*$')
MNCP_re = re.compile('^MNCP\s*\:\s*([0-9e\.\-]+)\s*$')

for cur_glob in sys.argv[1:]:
	for filename in glob.glob(cur_glob):
		basename = os.path.basename(filename)
		filehandle = file(filename)


		ROCAUC_value = MNCP_value = None
		for line in filehandle:
			if ROCAUC_re.match(line):
				ROCAUC_match = ROCAUC_re.match(line)
				if ROCAUC_value <> None:
					raise "ROCAUC matched more than once: " + line
				ROCAUC_value = ROCAUC_match.group(1)
			elif MNCP_re.match(line):
				MNCP_match = MNCP_re.match(line)
				if MNCP_value <> None:
					raise "MNCP matched more than once: " + line
				MNCP_value = MNCP_match.group(1)

		if (not MNCP_value) or (not ROCAUC_value):
			print >>sys.stderr, basename+':', 'MNCP or ROC AUC value missing.'
		else:
			print '\t'.join((ROCAUC_value, MNCP_value, '"'+basename+'"'))


