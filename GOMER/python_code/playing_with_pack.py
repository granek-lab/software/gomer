>>> handle = file('blahblah', 'w')
>>> handle.write('Header line1\n')
>>> handle.write('Header line2\n')
>>> handle.write('Header line3\n')
>>> handle.write('Header line4\n')
>>> handle.write('BREAKLINE\n')

>>> a = Numeric.arange(10000.0)
>>> import struct
>>> for value in a:
...     handle.write(struct.pack('f', value))
... 
#-----------------------------------------------------------------
def find_data_start(handle, trigger_string):
	handle.seek(0)
	line = ' '
	while(line):
		line = handle.readline()
		if line == trigger_string + '\n':
			return handle.tell()
	## return None
	raise ValueError, 'Trigger Not Found: ' + trigger_string
#-----------------------------------------------------------------
import struct
def get_value(index, data_start, handle):
	position = data_start + struct.calcsize('f') * index
	handle.seek(position)
	data = handle.read(struct.calcsize('f'))
	return struct.unpack('f', data)
#-----------------------------------------------------------------
>>> handle.seek(0)
>>> handle.readline()
'Header line1\n'
>>> handle.readline()
'Header line2\n'
>>> handle.readline()
'Header line3\n'
>>> handle.readline()
'Header line4\n'
>>> handle.readline()
'BREAKLINE\n'
>>> handle.tell()
62L

>>> find_data_start(handle, 'BREAKLINE')
62L
>>> find_data_start(handle, 'BREAKLI')

>>> get_value(10, data_start, handle)
(10.0,)
>>> get_value(1000, data_start, handle)
(1000.0,)
>>> 


>>> import struct
>>> struct.calcsize('d')
8
>>> struct.calcsize('f')
4
>>> 

-----------------------------------------------------------------------------
NOTE: Files won't be compatible across machines, unless a uniform byte order and alignment is used
-----------------------------------------------------------------------------

Alternatively, the first character of the format string can be used to indicate the byte order, size and alignment of the packed data, according to the following table:

Character  Byte order  Size and alignment 
@ native native
= native standard
< little-endian standard
> big-endian standard
! network (= big-endian) standard

If the first character is not one of these, "@" is assumed.

Native byte order is big-endian or little-endian, depending on the host system. For example, Motorola and Sun processors are big-endian; Intel and DEC processors are little-endian.

Native size and alignment are determined using the C compiler's sizeof expression. This is always combined with native byte order.

Standard size and alignment are as follows: no alignment is required for any type (so you have to use pad bytes); short is 2 bytes; int and long are 4 bytes; long long (__int64 on Windows) is 8 bytes; float and double are 32-bit and 64-bit IEEE floating point numbers, respectively. 
