#! /usr/bin/env python
from __future__ import division
#@+leo
#@comment Created by Leo at Wed Jun  4 16:15:29 2003
#@+node:0::@file flat_prob_matrix_printer_gomer.py
#@+body
#@@first
#@@first
#@@language python


"""
*Description:

A class for outputting probability matrices (from a binding site model) in 
different formats.

Its quite possible that this class would be better as functions in a module. 
"""

#@<<imports>>
#@+node:1::<<imports>>
#@+body
##import os
##os.environ['PYCHECKER'] = '--allglobals --stdlib'
##import pychecker.checker

##import check_python_version
##check_python_version.CheckVersion()

from flat_prob_matrix_tokens import *
#@-body
#@-node:1::<<imports>>

## class FlatProbMatrixPrinter (object):

#@+others
#@+node:2::def FlatProbMatrixOutput
#@+body
def FlatProbMatrixOutput(probability_matrix):
	
	#@<<doc string>>
	#@+node:1::<<doc string>>
	#@+body
	"""
	File Format:
	any line which is empty, or contains only white space is ignored
	# Comment line 
	%NAME - Name line
	
	all comment lines are tracked, but if there are multiple %NAME lines,
	, all but the last one are ignored
	
	above are optional, but must come before the base header:
	a   c  g  t   - the order of the bases in the header does not matter,
	                but the order corresponds with the order of the
					probabilities
	.2  .4 .1 .3  - probabilities should sum to one - but they
	                will be rescaled to sum to one, regardless
	
	N   N  N  N   - this is a gap position, the bases don't matter,
	                and don't contribute to the score
	
	returns: 
	
	"""
	#@-body
	#@-node:1::<<doc string>>


	# check instance is ProbabilityMatrix ?
	# call the base class method?


	output_string = ''
	#spacer = '\t\t'
	spacer = ''
	justification = 25
	
	for comment in probability_matrix.CommentList:
		output_string += COMMENT_TOKEN + comment + '\n'
	if probability_matrix.Name:
		output_string += NAME_TOKEN + ' ' + probability_matrix.Name + '\n'
	output_string += PSEUDO_TEMP_TOKEN + ' ' + str(probability_matrix.GetPseudoTemp()) + '\n'
##	output_string += spacer.join(BASE_LABEL_STRING) + '\n'
	output_string += spacer.join([str(base).ljust(justification)
								  for base in BASE_LABEL_STRING]) + '\n' 


	for index in range(probability_matrix.Length):
		output_string += spacer.join([
			str(probability_matrix.GetProbability(base, index)).ljust(justification)
			for base in BASE_LABEL_STRING]) + '\n' 
	return output_string




#@-body
#@-node:2::def FlatProbMatrixOutput
#@+node:3::def PWMOutput
#@+body
def PWMOutput(probability_matrix):
	justification = 25

	output_string = ''
	
##	for comment in probability_matrix.CommentList:
##		output_string += COMMENT_TOKEN + comment + '\n'
##	if probability_matrix.Name:
##		output_string += NAME_TOKEN + ' ' + probability_matrix.Name + '\n'

	output_string += '## ' + PSEUDO_TEMP_TOKEN + ' ' + \
					 str(probability_matrix.GetPseudoTemp()) + '\n'

##	output_string += ''.join([str(base).ljust(justification)
##								  for base in BASE_LABEL_STRING]) + '\n' 
	output_string += '\t'.join([str(base) for base in BASE_LABEL_STRING]) + '\n' 

	for index in range(probability_matrix.Length):
##		output_string += spacer.join([
##			str(probability_matrix.GetBitValue(base, index)).ljust(justification)
##			for base in BASE_LABEL_STRING]) + '\n' 
		output_string += '\t'.join([str(probability_matrix.GetBitValue(base, index))
			for base in BASE_LABEL_STRING]) + '\n' 
	return output_string

#@-body
#@-node:3::def PWMOutput
#@+node:4::def FlatProbMatrixOutputTable
#@+body
def FlatProbMatrixOutputTable(probability_matrix, values='probs'):

	# check instance is ProbabilityMatrix ?
	# call the base class method?

	# need to figure out how to be able to print a probability matrix, or a bitscore matrix

	output_string = ''
	just_size = 6
	first_column = 3
	if values == 'probs':
		value_function = probability_matrix.GetProbability
	elif values == 'bits' :
		value_function = probability_matrix.GetBitValue
	else :
		raise ValueError, "OutputTable: 'values' parameter must be 'probs' or 'bits'"
	
	
	output_string += 'Name: ' + ' ' + probability_matrix.Name + '\n'
	for comment in probability_matrix.CommentList:
		output_string += COMMENT_TOKEN + comment + '\n'
	# output_string += spacer.join(BASE_LABEL_STRING) + '\n'
	## generate position numbering matrix
	output_string += ''.ljust(first_column) + \
					 ''.join([count.ljust(just_size) for count in \
							  map(str, range(1, probability_matrix.Length + 1))])
	output_string += '\n'
	## print the values
	for base in BASE_LABEL_STRING :
		output_string += base.ljust(first_column) + \
						 ''.join([str(round(value_function(base, index), just_size - 3)).ljust(just_size) \
								  for index in range(probability_matrix.Length)])
		output_string += '\n'
	return output_string





#@-body
#@-node:4::def FlatProbMatrixOutputTable
#@-others


#@-body
#@-node:0::@file flat_prob_matrix_printer_gomer.py
#@-leo
