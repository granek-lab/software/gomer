#! /usr/bin/env python
from __future__ import division
from __future__ import generators
#@+leo
#@comment Created by Leo at Fri Jul 18 14:18:21 2003
#@+node:0::@file chromosome_gomer.py
#@+body
#@@first
#@@first
#@@first
#@@language python


"""
*Description:

A representation of a chromosome, mainly a container/organizer.

Each chromosome holds the sequence that it represents, a list of the features found on 
the chromosome, and if the chromosome's sequence has been scored with one or more 
binding site models, it holds the hitlists resulting from each of those scorings
"""

#@<<imports>>
#@+node:1::<<imports>>
#@+body
##import os
##os.environ['PYCHECKER'] = '--allglobals --stdlib'
##import pychecker.checker

##import check_python_version
##check_python_version.CheckVersion()

import sys
import md5
import types

# import Indexed from indexed

import general_bisect
from intersect import intersect
from dummy_feature_gomer import DummyFeature
from exceptions_gomer import GomerError
#@-body
#@-node:1::<<imports>>


def _feature_compare (x, y):
	return cmp(x.Start, y.Start) or cmp(x.Stop, y.Stop)
class Chromosome(object):

	#@+others
	#@+node:2::__init__
	#@+body
	def __init__ (self, chromosome_identifier):
		self._feature_list = []
		self._chromosome_identifier = chromosome_identifier
		self._sequence = None
		self._hitlist_hash = {}
		self._md5 = None
	#@-body
	#@-node:2::__init__
	#@+node:3::AddFeature
	#@+body
	def AddFeature (self, new_feature):
		"""
		Features need to be added in the appropriate location - the feature_list should be sorted by
		start_coordinate, then by stop_coordinate (if start_coordinate is the same)	
		"""
		# for feature, index in Indexed(_feature_list):
		# 	if feature
		general_bisect.insort_right(self._feature_list, new_feature, _feature_compare)
	#@-body
	#@-node:3::AddFeature
	#@+node:4::GetOverlapping
	#@+body
	#@+doc
	# 
	# 
	# Use bisect?
	# file:///usr/share/doc/python2.2/html/lib/module-bisect.html
	# 

	#@-doc
	#@-body
	#@-node:4::GetOverlapping
	#@+node:5::GetContained
	#@+body
	#@+doc
	# 
	# 
	# Use bisect?
	# file:///usr/share/doc/python2.2/html/lib/module-bisect.html
	# 

	#@-doc
	#@@code
	# def GetContained (start, end, feature_type=None):
	def GetContained (self, start, stop):
		"""
		returns: sorted a list of all features that lie completely between start and end
			     if feature_type is given, only features of that type are returned
				 
				 
		Details: 	The GetContained method is used to determine what features on the chromosome fall 
		completely within a base pair range.  Functions of the general_bisect module are used 
		in this method, essential the method works by using the bisect functions to determine where
		dummy features would be inserted in the sorted list of features.  Any features between these 
		insertion points are what we care about.
		"""
		start_dummy = DummyFeature (start, start + 1)
		stop_dummy = DummyFeature (stop, stop + 1)
		start_index = general_bisect.bisect_left(self._feature_list, start_dummy, _feature_compare)
		stop_index = general_bisect.bisect_left(self._feature_list, stop_dummy, _feature_compare)
		print '\n\n\n\n', "GetContained probably doesn't work yet!!!!!", '\n\n\n\n'
		sys.exit(1)
		return self._feature_list[start_index:stop_index]
	#@-body
	#@-node:5::GetContained
	#@+node:6::GetNearest
	#@+body
	def GetNearest(self, start, stop):
		"""
		returns: The feature that is nearest to the range from start to stop
		
		
		What to do about contained features?		 
		Currently I'm not paying attention to whether or not the feature is contained,
		so the closest feature could be entirely contained in the range start:stop.
	
	
	
		"""
		start_dummy = DummyFeature (start, start + 1)
		stop_dummy = DummyFeature (stop, stop + 1)
		start_index = general_bisect.bisect_left(self._feature_list, start_dummy, _feature_compare)
		stop_index = general_bisect.bisect_left(self._feature_list, stop_dummy, _feature_compare)

		feature_indices_to_consider = [start_index-1,start_index,start_index+1]
## 		features_to_consider = [self._feature_list[(start_index-1)],
## 								self._feature_list[start_index],
## 								self._feature_list[(start_index+1)]]
		
		# print 'stop:', stop, 'start', start
		# print 'stop_index:', stop_index, 'start_index:', start_index
		if start_index <> stop_index:
			for index in [stop_index-1,stop_index,stop_index+1]:
				## without checking, if start and stop are not the same, but only different by 1 or two, we will add duplicates of features
				if index not in feature_indices_to_consider:
					feature_indices_to_consider.append(index)

		features_to_consider = []
		for index in feature_indices_to_consider:
			if 0 <= index < len(self._feature_list):
				features_to_consider.append(self._feature_list[index])

		shortest_distance = sys.maxint
		closest_features = []

		# print "features_to_consider:", features_to_consider
		# print "features_to_consider Names:", [feature.Name for feature in features_to_consider] 
		for cur_feature in features_to_consider:
			best_feature_distance = min((abs(stop - cur_feature.Start),
										 abs(stop - cur_feature.Stop),
										 abs(start - cur_feature.Start),
										 abs(start - cur_feature.Stop)))
			if best_feature_distance == shortest_distance:
				closest_features.append(cur_feature)
			elif best_feature_distance < shortest_distance:
				shortest_distance = best_feature_distance
				closest_features = [cur_feature]
	
			
		return closest_features
	
	#@-body
	#@-node:6::GetNearest
	#@+node:7::OrfCount
	#@+body
	def OrfCount(self):
		count = 0
		for feature in self._feature_list:
			count += feature.IsOrf
		return count
	#@-body
	#@-node:7::OrfCount
	#@+node:8::SetSequence
	#@+body
	def SetSequence(self, sequence):
		if self._sequence:
			raise ChromosomeError(self, "Sequence attribute is already set\nA Chromosome instance's sequence attribute can only be set once")
		else:
			self._sequence = sequence
	
	#@-body
	#@-node:8::SetSequence
	#@+node:9::Sequence
	#@+body
	def _get_sequence(self):return self._sequence

	#@+doc
	# 
	# def _get_sequence(self):return self._sequence
	# 	if self._sequence:
	# 		return self._sequence
	# 	else:
	# 		raise ChromosomeError('Cannot access Sequence, it is not set!!', self)

	#@-doc
	#@@code
	def _set_sequence(self, value): raise AttributeError, "Can't set 'Sequence' attribute"
	Sequence = property(_get_sequence, _set_sequence)
	
	#@-body
	#@-node:9::Sequence
	#@+node:10::Name
	#@+body
	def _get_name(self): return self._chromosome_identifier
	def _set_name(self, value): raise AttributeError, "Can't set 'Name' attribute"
	Name = property(_get_name, _set_name)
	
	#@-body
	#@-node:10::Name
	#@+node:11::MD5
	#@+body
	def _get_md5(self):
		if not self._md5:
			self._md5 = md5.new(self._sequence.Sequence).hexdigest()
			## print 'Chromosome', self._chromosome_identifier, self._md5
		return self._md5
	def _set_md5(self, value): raise AttributeError, "Can't set 'MD5' attribute"
	MD5 = property(_get_md5, _set_md5)
	
	#@-body
	#@-node:11::MD5
	#@+node:12::GetHitListSize
	#@+body
	def GetHitListSize(self, hitlist_name):
		return self._hitlist_hash[hitlist_name].Length
	
		
	
	
	#@-body
	#@-node:12::GetHitListSize
	#@+node:13::GetHitList
	#@+body
	def GetHitList(self, hitlist_name):
		return self._hitlist_hash[hitlist_name]
	
		
	
	
	#@-body
	#@-node:13::GetHitList
	#@+node:14::GetAllHitLists
	#@+body
	def GetAllHitLists(self):
		return self._hitlist_hash.keys()
	
		
	
	
	#@-body
	#@-node:14::GetAllHitLists
	#@+node:15::AddHitlist
	#@+body
	def AddHitlist(self, hitlist, hitlist_label):
		if hitlist_label not in self._hitlist_hash:
			self._hitlist_hash[hitlist_label] = hitlist
		else:
			raise ChromosomeError(self, 'Chromosome already has a hitlist named: ' + hitlist_label + '\n')
	
	
	#@-body
	#@-node:15::AddHitlist
	#@+node:16::OrfIterator
	#@+body
	def OrfIterator(self):
		for feature in self._feature_list:
			if feature.IsOrf:
				yield feature
		return
	
	
	#@-body
	#@-node:16::OrfIterator
	#@+node:17::FeatureIterator
	#@+body
	def FeatureIterator(self, feature_types=None):
		"""
		Possible values for 'feature_types':

		No value (or None) :
		    iterate over all features.
		A string (the name of a feature type):
		    Iterate over all features of the type described in the string.
		A list (or tuple) of strings:
		    Iterate over all features of the types described in the list.
		"""

		if feature_types==None:
			for feature in self._feature_list:
				yield feature
		elif type(feature_types) == types.StringType:
			for feature_types in self._feature_list:
				yield feature
		elif (type(feature_types) == types.ListType) or \
			 (type(feature_types) == types.TupleType):
			for feature in self._feature_list:
				if intersect(feature.Types, feature_types):
					yield feature
		else:
			raise ValueError, "'feature_types' must be 'None', a 'String', or a (non-string) 'Sequence'"
		return
	
	
	#@-body
	#@-node:17::FeatureIterator
	#@-others




#@<<Chromosome Error>>
#@+node:18::<<Chromosome Error>>
#@+body
class ChromosomeError(GomerError):
	def __init__(self, chromosome, message):
		# self.line, self.line_number, self.message = tuple
		##super(ChromosomeError, self).__init__()
		self.message = 'Chromosome ' + self.chromosome.Name + '\n' + message

#@-body
#@-node:18::<<Chromosome Error>>


#@-body
#@-node:0::@file chromosome_gomer.py
#@-leo
