#! /usr/bin/env python

import sys
from cache_window_list_gomer import CacheWindowList
from cache_file_handler_gomer import CacheFileHandler
from optparse import OptionParser

from gomer_load_functions import load_chromosome_table, load_chromosome_sequences, load_probability_matrix
from reverse_complement import reverse_complement




def main():
	usage = 'usage: %prog CACHE_FILE_1 CACHE_FILE_2 [-f] '
	usage += '\n   Compare CACHE_FILE_1 and CACHE_FILE_2'

	usage += '\n      %prog CACHE_FILE_1 CACHE_FILE_2 CHROM_TABLE PROB_MATRIX '
	usage += '\n\n   Compare CACHE_FILE_1 and CACHE_FILE_2, and anotate differences with sequences from CHROM_TABLE'
	parser = OptionParser(usage)
	parser.add_option("-f", "--fast", type="string", dest="fast_cache_directory",
					  metavar="FAST_CACHE",
					  help="use FAST_CACHE as the fast cache directory")

	(options, args) = parser.parse_args()
	if len(args) == 2:
		cache_file_name_1 = args[0]
		cache_file_name_2 = args[1]
		chrom_hash = {}
	elif len(args) == 4:
		cache_file_name_1 = args[0]
		cache_file_name_2 = args[1]
		chrom_table_name = args[2]
		prob_matrix_name = args[3]
		
		feature_file_list, chromosome_file_hash, chromosome_flags_hash, frequency_hash = load_chromosome_table(chrom_table_name)

		chrom_hash = {}
		load_chromosome_sequences(chromosome_file_hash, {},chrom_hash, {},{})
		
		matrix = load_probability_matrix(prob_matrix_name, frequency_hash)
		matrix_length = matrix.Length
		
	else:
		parser.error("incorrect number of arguments")
		
	## print >>sys.stderr, 'chrom_hash:', chrom_hash
	fast_cache_directory = options.fast_cache_directory
	print ' '.join(sys.argv), '\n'
	##-----------------------------------------------------------------
	handler1 = CacheFileHandler(cache_file_name_1, fast_cache_directory)
	handle1 = handler1.GetHandle()

	handler2 = CacheFileHandler(cache_file_name_2, fast_cache_directory)
	handle2 = handler2.GetHandle()

	just_small=8
	just_big=25

	if chrom_hash:
		print ''.join([x.ljust(just_small) for x in (('Chrom', 'Index', 'Strand'))] +
					  [x.ljust(just_big)   for x in (('cache_1', 'cache_2', 'rescore matches', 'sequence', 'rescore'))])
	else:
		print ''.join([x.ljust(just_small) for x in (('Chrom', 'Index', 'Strand'))] +
					  [x.ljust(just_big)   for x in (('cache_1', 'cache_2'))])
	
	sequence = rescore = rescore_match = ''
	for chrom_label in handler1.Chromosomes:
		chrom1 = handler1.GetChrom(chrom_label)
		chrom2 = handler2.GetChrom(chrom_label)

		if chrom1.GetNumWindows() <> chrom2.GetNumWindows():
			print chrom_label, 'chrom1.GetNumWindows() <> chrom2.GetNumWindows()'
		num_windows = chrom1.GetNumWindows()

		if chrom1.GetForwardOffset() <> chrom2.GetForwardOffset():
			print chrom_label, 'chrom1.GetForwardOffset() <> chrom2.GetForwardOffset()'
		for_offset = chrom1.GetForwardOffset()

		if chrom1.GetRevCompOffset() <> chrom2.GetRevCompOffset():
			print chrom_label, 'chrom1.GetRevCompOffset() <> chrom2.GetRevCompOffset()'
		rev_offset = chrom1.GetRevCompOffset()

		for strand,offset in (['forward', for_offset], ['reverse',rev_offset]):
			cur_cache_list1 = CacheWindowList(handle1, num_windows, offset)
			cur_cache_list2 = CacheWindowList(handle2, num_windows, offset)
			cur_cache_array1 = cur_cache_list1[:]
			cur_cache_array2 = cur_cache_list2[:]
			for index in xrange(len(cur_cache_array1)):
				if cur_cache_array1[index] <> cur_cache_array2[index]:
					if chrom_hash:
						sequence = chrom_hash[chrom_label].Sequence.Sequence[index:index + matrix_length]
						rescore_hitlist = matrix.FilterScoreSequence(sequence,0)
						rescore = rescore_hitlist.Forward[0]
						if strand == 'reverse':
							sequence = reverse_complement(sequence)
							rescore = rescore_hitlist.ReverseComplement[0]
						if rescore == cur_cache_array1[index]:
							rescore_match = 'cache_1'
						elif rescore == cur_cache_array2[index]:
							rescore_match = 'cache_2'
						else:
							rescore_match = 'NEITHER'
						print ''.join([str(x).ljust(just_small) for x in ((chrom_label, index, strand,))] +
									  [repr(x).ljust(just_big)   for x in ((cur_cache_array1[index], cur_cache_array2[index],
																			rescore_match, sequence, rescore))])
					else: # NOT chrom_hash
						print ''.join([str(x).ljust(just_small) for x in ((chrom_label, index, strand,))] +
									  [repr(x).ljust(just_big)   for x in ((cur_cache_array1[index], cur_cache_array2[index]))])

if __name__ == '__main__':
	main()

