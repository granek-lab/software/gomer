#! /usr/bin/env python
from __future__ import division
#@+leo
#@comment Created by Leo at Wed Jul  2 16:57:28 2003
#@+node:0::@file prob_to_pwm.py
#@+body
#@@first
#@@first
#@@language python

"""
*Description:

"""

#@<<imports>>
#@+node:1::<<imports>>
#@+body
import sys
import os
import getopt
import math
import re
import operator

#@-body
#@-node:1::<<imports>>


TOLERANCE = 1e-10

FILE_FORMAT = """
FILE FORMAT:
-----------------------------------------------------------------
Blank Line      : any line which is empty, or contains only white
                  space is ignored
# Comment line  : Begin with '#'.  All comment lines are propagated
                  to the output file
-----------------------------------------------------------------

The above lines are optional, but must come before the base header:

a   c  g  t     - Base Header  :  the order of the bases in the header
                                  does not matter, but the order MUST
                                  correspond with the order of the
                                  probabilities
.2  .4 .1 .3    - Probabilities:  probabilities MUST sum to one.
                                  Zero probabilities will be 
"""
"""
N   N  N  N     - Gaps         :  Values are converted to the base frequencies
-----------------------------------------------------------------
"""


#@<<tokens>>
#@+node:4::<<tokens>>
#@+body
COMMENT_TOKEN = '#'
BASE_LABEL_STRING = 'ACGT'
NAME_TOKEN = '%NAME'
PSEUDO_TEMP_TOKEN = '%PSEUDO_TEMP'
## GAP_PROB_TOKEN = 'N'
#@-body
#@-node:4::<<tokens>>




#@<<flat_prob_matrix_parser_gomer>>
#@+node:6::<<flat_prob_matrix_parser_gomer>>
#@+body
#@+others
#@+node:1::regular expressions
#@+body
comment_match = re.compile('^\s*' + COMMENT_TOKEN + '(.+)')
base_header_match = re.compile('^[' + BASE_LABEL_STRING + '\s]+$', re.IGNORECASE)
## gap_probabilities_match = re.compile('^[' + GAP_PROB_TOKEN + '\s]+$', re.IGNORECASE)
empty_line_match = re.compile('^\s*$')
rough_probabilities_match = re.compile('^\s*\d+[\d\.\-\se]+$')
#@-body
#@-node:1::regular expressions
#@+node:2::FlatProbMatrixParse
#@+body
def FlatProbMatrixParse(filename, base_frequency_hash):
	
	#@<<doc string>>
	#@+node:1::<<doc string>>
	#@-node:1::<<doc string>>

	try:
		filehandle = file(filename, 'rU')	
	except IOError:
		filehandle = file(filename, 'r')	

	probability_matrix = {}
	comment_list = []
	name = ''
	
	#@<<parse header>>
	#@+node:2::<<parse header>>
	#@+body
	## Parse the header section
	line_number = 0
	while 1:
		line = filehandle.readline()
		line_number += 1
		if not line :
			raise FlatMatrixParseError \
				  (filehandle.name, line, line_number,\
				   'Parse Error: Reached end of file prematurely:\nno Base Header was found')
		if comment_match.search (line) :
			comment_list.append (comment_match.search(line).group(1).rstrip())
		elif empty_line_match.search (line):
			continue
		elif base_header_match.search (line):
			base_label_header = line.rstrip()
			base_label_array = base_label_header.upper().split()
			if not len (base_label_array) == len(BASE_LABEL_STRING) :
				raise FlatMatrixParseError \
					  (filehandle.name, line, line_number,\
					   'Base Header Should contain each of the ' + \
					   str(len(BASE_LABEL_STRING)) + ' bases: ' + ','.join(BASE_LABEL_STRING) +'\n')
			for base in BASE_LABEL_STRING:
				if (base not in base_label_array) :
					raise FlatMatrixParseError \
						  (filehandle.name, line, line_number, \
					 	  'Base Header is  missing the base:' + base + '\n')
			break
		elif rough_probabilities_match.search (line) :
			raise FlatMatrixParseError \
				  (filehandle.name, line, line_number, \
				   'Probabilities Found before Base Header')
		else:
			raise FlatMatrixParseError \
				  (filehandle.name, line, line_number, \
				   'Incorrect line format')
	
	
	
	#@-body
	#@-node:2::<<parse header>>


	
	#@<<parse probabilities>>
	#@+node:3::<<parse probabilities>>
	#@+body
	## Parse the probabilities section
	
	trailing_empty_lines = 0
	line_number = 0
	while 1:
		line = filehandle.readline()
		line_number += 1
		if not line :
			if not probability_matrix:
				raise FlatMatrixParseError \
					  (filehandle.name, line, line_number, \
					   'Parse Error: Reached end of file prematurely:\nno probabilities were found')
			else :
				break
		split_line = line.rstrip().split()
	
		# any empty lines should only occur at the end of the file
		# so we need to keep track to be sure to report an error if
		# empty lines occur in the middle of the file
		if empty_line_match.search (line):
			trailing_empty_lines += 1
			continue
		elif trailing_empty_lines :
			raise FlatMatrixParseError \
					  (filehandle.name, line, line_number, \
					   'Empty lines found in probabilities' + \
					   '\nEmpty lines are only allowed in the header section, or' + \
					   '\ntrailing after all the probabilities, at the end of the file')
		if not (len (split_line) == 4) :
			raise FlatMatrixParseError \
				  (filehandle.name, line, line_number,\
				   'There should be four probabilities per line')
	
##		total_prob = reduce(operator.add, map(float, split_line))
##		if abs(1 - total_prob) > TOLERANCE:
##			raise FlatMatrixParseError (filehandle.name, line, line_number,
##										'Probabilities must sum to 1 (with ' +
##										'tolerance of' + str(TOLERANCE) + ')')
	
		## for probability, base_index in Indexed(split_line) :
		for base_index in range(len(split_line)) :
			probability = split_line[base_index]
			##if gap_probabilities_match.search(line) :
			if 0 :
				float_probability = 0
			else:
				try:
					float_probability = float(probability)
				except ValueError:
					raise FlatMatrixParseError \
						  (filehandle.name, line, line_number,\
						   'Probabilities must be floating point numbers')
				if float_probability < 0 :
					raise FlatMatrixParseError \
						  (filehandle.name, line, line_number,\
						   'Probabilities cannot be negative')
				## elif float_probability == 0 :
				##	raise FlatMatrixParseError \
				##		  (filehandle.name, line, line_number,\
				##		   'Probabilities cannot be zero')
			cur_base = base_label_array[base_index]
			probability_matrix.setdefault(cur_base, []).append(float_probability)
	
	
	#@-body
	#@-node:3::<<parse probabilities>>


	filehandle.close()
	return ProbabilityMatrix(probability_matrix, comment_list, base_frequency_hash)




#@-body
#@-node:2::FlatProbMatrixParse
#@+node:3::FlatMatrixParseError
#@+body
class FlatMatrixParseError:
	"""Exception raised for formatting errors encountered during parsing files.

	Attributes:
		line -- line of file containing error
		message -- explanation of the error
	"""

	def __init__(self, file_name, line, line_number, message):
		self.message = '\n'.join((message,
								  'Error encountered in following line (#'
								  +str(line_number) +')'+'from input file:',
								  file_name, line
								  ))
	def __str__ (self):
		return  '\n\n' + '*'*60 + '\n' + self.message + '\n' + '*'*60 + '\n'
		

#@-body
#@-node:3::FlatMatrixParseError
#@-others


#@-body
#@-node:6::<<flat_prob_matrix_parser_gomer>>


#@<<probability_matrix_gomer>>
#@+node:7::<<probability_matrix_gomer>>
#@+body
#@@first #! /usr/bin/env python
#@@first from __future__ import division
#@@language python

"""
*Description:

A matrix representation of a binding site.  

It can score a sequence, returning a hitlist instance with a scores for each
possible window of the matrix on the sequence (both forward and reverse
complement).

A probability matrix instance is initialized with a table of base probabilities, 
in addition to this probability representation, a bitscore (information content)
based representation is generated.  At the moment, this representation is what is
used to score the sequence.
"""


class ProbabilityMatrix(object):

	#@+others
	#@+node:1::_deal_with_zeroes_and_rescale
	#@+body
	def _deal_with_zeroes_and_rescale(self, probability_hash, site_length):
		zero_positions = []
		min_value = sys.maxint
		for index in range(site_length):
			for base in probability_hash:
				if 0 == probability_hash[base][index]:
					zero_positions.append((base, index))
				elif probability_hash[base][index] < min_value:
					min_value = probability_hash[base][index]
		min_value = min((min_value/2, 0.01))
	
		for base, index in zero_positions:
			probability_hash[base][index] = min_value
	
		##-------------------------------------------------------
		## RESCALE
		##-------------------------------------------------------
		for index in range(site_length):
			total_prob = reduce(operator.add,
					    [probability_hash[base][index] for
					     base in probability_hash])
			for base in probability_hash:
				probability_hash[base][index] = probability_hash[base][index]/total_prob
		return probability_hash
	#@-body
	#@-node:1::_deal_with_zeroes_and_rescale
	#@+node:2::_generate_bitvalue_hash
	#@+body
	def _generate_bitvalue_hash(self, probability_hash, site_length):
		bitvalue_hash = {}
		for base in probability_hash.keys():
			bitvalue_hash[base] = [None] * site_length
	
		for index in range (site_length):
			for base in probability_hash.keys():
				
				bitvalue_hash[base][index] = Log2(probability_hash[base][index]/self._base_frequency_hash[base]) * self._temp_scaling_factor
		return bitvalue_hash
	
	
	
	#@-body
	#@-node:2::_generate_bitvalue_hash
	#@+node:3::__init__
	#@+body
	def __init__ (self, probability_hash, comments_list, base_freq_hash):
		self._site_length = -1
		self._comments_list = comments_list
		self._R = 1.9858775229213840e-3 # kcal/(mol.K)
		self._base_frequency_hash = base_freq_hash
		##-------------------------------------------------------
		Log2 = lambda x:math.log(x)/math.log(2) # log base 2
		ln = math.log
	
		K = math.e ## K can be anything positive value other than 1 (which creates a division by zero)
		R = self._R
		pseudo_room_temperature = Log2(K)/(R * ln(K)) # Log2(K)/ln(K) * (1/R)
		self._temp_scaling_factor = 300/pseudo_room_temperature ## 0.4129516217850977
		## self._temp_scaling_factor = 1
		## print "TEMP_SCALING_FACTOR:", self._temp_scaling_factor
		##-------------------------------------------------------
		
		#@<<check site length>>
		#@+node:1::<<check site length>>
		#@+body
		# calculate and check self._site_length
		for base in probability_hash.keys():
			if self._site_length  < 0:
				self._site_length = len(probability_hash[base])
			elif not self._site_length == len(probability_hash[base]):
				raise ProbabilityMatrixError ('Number of probabilities for base: '\
											  + base + ' different from other bases\n') 
		
		#@-body
		#@-node:1::<<check site length>>

		# generate bitvalues
		self._probability_hash = self._deal_with_zeroes_and_rescale(probability_hash, self._site_length)
		self._bitvalue_hash = self._generate_bitvalue_hash(self._probability_hash, self._site_length)
	
		self._forward_scoring_hash = self._bitvalue_hash



	def Smooth(self, scale):
		scale = scale/100
		probability_hash = dict(self._probability_hash)

		for base in probability_hash:
			for index in range(len(probability_hash[base])):
##------------------------------------------------------------------------------
				probability_hash[base][index] = math.exp(scale *
														 math.log(probability_hash[base][index]/0.25))
##------------------------------------------------------------------------------
##				probability_hash[base][index] = (probability_hash[base][index]/
##												 (probability_hash[base][index] * scale))
##------------------------------------------------------------------------------
##				probability_hash[base][index] = (probability_hash[base][index]/
##												 (1 + (probability_hash[base][index] * scale)))
##------------------------------------------------------------------------------
##				probability_hash[base][index] = (probability_hash[base][index]/
##												 ((probability_hash[base][index] * scale)**2))
##------------------------------------------------------------------------------
		self._probability_hash = self._deal_with_zeroes_and_rescale(probability_hash, self._site_length)
		self._bitvalue_hash = self._generate_bitvalue_hash(self._probability_hash, self._site_length)
		self._forward_scoring_hash = self._bitvalue_hash

	#@-body
	#@-node:3::__init__
	#@+node:4::ResetTempScalingFactor
	#@+body
	def ResetTempScalingFactor(self, temp_scaling_factor):
		self._temp_scaling_factor = temp_scaling_factor
		self._bitvalue_hash = self._generate_bitvalue_hash(self._probability_hash, self._site_length)
		self._forward_scoring_hash = self._bitvalue_hash
	
	#@-body
	#@-node:4::ResetTempScalingFactor
	#@+node:5::Length
	#@+body
	def _get_length(self): return self._site_length
	def _set_length(self, value): raise AttributeError, "Can't set 'Length' attribute"
	Length = property(_get_length, _set_length)
	
	#@-body
	#@-node:5::Length
	#@+node:6::GetPseudoTemp
	#@+body
	def GetPseudoRT(self):
		return self._pseudoRT
	
	def GetPseudoTemp(self):
		return self._pseudo_temperature
	
	#@-body
	#@-node:6::GetPseudoTemp
	#@+node:7::GetProbability
	#@+body
	def GetProbability(self, base, index):
		"""
		returns : the bit value of having "base" in position "index" of a site
		"""
		return self._probability_hash[base][index]
	#@-body
	#@-node:7::GetProbability
	#@+node:8::GetBitValue
	#@+body
	def GetBitValue(self, base, index):
		"""
		returns : the bit value of having "base" in position "index" of a site
		"""
		return self._forward_scoring_hash[base][index]
	#@-body
	#@-node:8::GetBitValue
	#@+node:9::CommentList
	#@+body
	def _get_comment_list(self): return self._comments_list
	def _set_comment_list(self, value): raise AttributeError, "Can't set 'CommentList' attribute"
	CommentList = property(_get_comment_list, _set_comment_list)
	
	#@-body
	#@-node:9::CommentList
	#@-others



#@<<Matrix Errors>>
#@+node:10::<<Matrix Errors>>
#@+body
#@@code
#@+others
#@+node:1::ProbabilityMatrixError
#@+body
class ProbabilityMatrixError:
	def __init__(self, message):
		super(ProbabilityMatrixError, self).__init__()
		self.message = message

	def __str__ (self):
		return  '\n\n' + '*'*60 + '\n' + self.message + '\n' + '*'*60 + '\n'

#@-body
#@-node:1::ProbabilityMatrixError
#@-others




#@-body
#@-node:10::<<Matrix Errors>>


#@-body
#@-node:7::<<probability_matrix_gomer>>


#@<<log_base>>
#@+node:5::<<log_base>>
#@+body
def Log2 (x):
	return math.log(x)/math.log(2)

#@-body
#@-node:5::<<log_base>>



#@<<PWMOutput>>
#@+node:2::<<PWMOutput>>
#@+body
def PWMOutput(probability_matrix):
	justification = 25

	output_string = ''
	output_string += '## USE AT PSEUDO_TEMP 300\n'
	output_string += '\t'.join([str(base) for base in BASE_LABEL_STRING]) + '\n' 

	for index in range(probability_matrix.Length):
		output_string += '\t'.join([str(probability_matrix.GetBitValue(base, index))
			for base in BASE_LABEL_STRING]) + '\n' 
	return output_string

#@-body
#@-node:2::<<PWMOutput>>


#@<<FlatProbMatrixOutput>>
#@+node:3::<<FlatProbMatrixOutput>>
#@+body
def FlatProbMatrixOutput(probability_matrix, gomer=False, name=None, pseudo_temp=None):
	
	#@<<doc string>>
	#@+node:1::<<doc string>>
	#@+body
	"""
	File Format:
	any line which is empty, or contains only white space is ignored
	# Comment line 
	%NAME - Name line
	
	all comment lines are tracked, but if there are multiple %NAME lines,
	, all but the last one are ignored
	
	above are optional, but must come before the base header:
	a   c  g  t   - the order of the bases in the header does not matter,
	                but the order corresponds with the order of the
					probabilities
	.2  .4 .1 .3  - probabilities should sum to one - but they
	                will be rescaled to sum to one, regardless
	
	N   N  N  N   - this is a gap position, the bases don't matter,
	                and don't contribute to the score
	
	returns: 
	
	"""
	#@-body
	#@-node:1::<<doc string>>


	# check instance is ProbabilityMatrix ?
	# call the base class method?


	output_string = ''
	#spacer = '\t\t'
	spacer = ''
	justification = 25
	
	for comment in probability_matrix.CommentList:
		output_string += COMMENT_TOKEN + comment + '\n'
	##if probability_matrix.Name:
	if gomer:
		output_string += NAME_TOKEN + ' ' + name + '\n'
		output_string += PSEUDO_TEMP_TOKEN + ' ' + str(pseudo_temp) + '\n'
##	output_string += spacer.join(BASE_LABEL_STRING) + '\n'
	output_string += spacer.join([str(base).ljust(justification)
								  for base in BASE_LABEL_STRING]) + '\n' 


	for index in range(probability_matrix.Length):
		output_string += spacer.join([
			str(probability_matrix.GetProbability(base, index)).ljust(justification)
			for base in BASE_LABEL_STRING]) + '\n' 
	return output_string




#@-body
#@-node:3::<<FlatProbMatrixOutput>>



#@<<command line>>
#@+node:8::<<command line>>
#@+body
#@<<def main>>
#@+node:1::<<def main>>
#@+body
def main (probability_file_name=None):
	commandline = ' '.join(sys.argv)
	equal_base_freq_hash = {'A':0.25,
							'C':0.25,
							'G':0.25,
							'T':0.25}
	
	yeast_intergenic_seqs_freq = {'A':0.32395922234142199,
								  'C':0.17604077765857803, 
								  'T':0.32395922234142199,
								  'G':0.17604077765857803}

	yeast_genomic_with_mito_freq = {'A':0.30926101235029785,
									'C':0.19073898764970215, 
									'T':0.30926101235029785,
									'G':0.19073898764970215}

	yeast_genomic_freq = {'A':0.30851345595763963,
						  'C':0.19148654404236037,
						  'T':0.30851345595763963,
						  'G':0.19148654404236037}
#--------------------------------------------------------
	try:
		opts, args = getopt.getopt(sys.argv[1:], "ho:igefp:s:", ["help", "output=", "prob=",'equal', 'intergenic', 'genomic', 'genomic-with-mito', 'test-scaling=', 'format', 'gomer=', 'smooth='])
	except getopt.GetoptError:
		# print help information and exit:
		usage()
	output_handle = sys.stdout
	output_probs_handle = None
	output_gomer_handle = None
	base_freq_hash = equal_base_freq_hash
	base_freq_name = 'EQUAL'
	freq_args = 0
	test_scaling = None
	smooth = 0
	for o, arg in opts:
		if o in ("-h", "--help"):
			usage()
			sys.exit()
		if o in ("-f", "--format"):
			print FILE_FORMAT
			sys.exit()
		if o in ("-o", "--output"):
			output_handle = file(arg, 'w')
		if o in ("-s", "--smooth"):
			smooth = float(arg)
		if o in ("-p", "--prob"):
			output_probs_handle = file(arg, 'w')
		if o in ("--gomer",):
			output_gomer_handle = file(arg, 'w')
		if o in ("-e", "--equal"):
			base_freq_hash = equal_base_freq_hash
			freq_args += 1
			base_freq_name = 'equal'
		if o in ("-i", "--intergenic"):
			base_freq_hash = yeast_intergenic_seqs_freq
			freq_args += 1
			base_freq_name = 'intergenic, no mitochondrial (yeast_intergenic_seqs.fa)'
		if o in ("-g", "--genomic"):
			base_freq_hash = yeast_genomic_freq
			freq_args += 1
			base_freq_name = 'genomic, no mitochondrial'
		if o in ('--genomic-with-mito',):
			base_freq_hash = yeast_genomic_with_mito_freq
			freq_args += 1
			base_freq_name = 'genomic, with mitochondrial'
		if o in ("--test-scaling",):
			test_scaling = float(arg)

	if freq_args > 1:
		print >> STDERR, 'ONLY ONE FREQUENCY FLAG CAN BE PROVIDED'
		usage()

##--------------------------------------------------------
	## print 'ARGS:', args
	EXPECTED_ARGUMENTS = 1
	if probability_file_name :
		print 'run called with parameters'
	elif len(args) == (EXPECTED_ARGUMENTS):
		probability_file_name = args[0]
	else :
		usage()

##	if not os.path.exists(probability_file_name):
##		print "File doesn't exist:", probability_file_name
##		sys.exit(1)

	probability_matrix = FlatProbMatrixParse(probability_file_name, base_freq_hash)
	if smooth:
		probability_matrix.Smooth(smooth)

	if test_scaling:
		probability_matrix.ResetTempScalingFactor(test_scaling)

	if output_probs_handle:
		output_probs_handle.write(FlatProbMatrixOutput(probability_matrix))
	if output_gomer_handle:
		name = os.path.basename(probability_file_name)
		
		output_gomer_handle.write('# ' + commandline + '\n')
		output_gomer_handle.write(FlatProbMatrixOutput(probability_matrix,
													   gomer=True,
													   name=name,
													   pseudo_temp=300))
	if (not output_probs_handle) and (not output_gomer_handle):
		flat_pwm = PWMOutput(probability_matrix)
		output_handle.write('## BASE FREQUENCIES: ' + base_freq_name + '\n')
		sorted_bases = base_freq_hash.keys()
		sorted_bases.sort()
		output_handle.write('## ' + ', '.join([base + ':' +
											   str(round(base_freq_hash[base], 3))
											   for base in sorted_bases]) + '\n')  
		output_handle.write(flat_pwm)
		
		## output_probs_handle.write(probability_matrix)
	## print ('*' * 50	+ '\n') * 5

#@-body
#@-node:1::<<def main>>


#@<<def usage>>
#@+node:2::<<def usage>>
#@+body
def usage () :
	print 'usage:   ', os.path.basename(sys.argv[0]), 'FILE\n' + """
	FILE: a flat probability matrix file

	Produces a bit-value matrix (PWM scoring matrix) from the input file.
	
	-h/--help                   : This information
	-f/--format                 : Print a description of the input file format
	-o/--ouput FILE             : use FILE as output file
	-p/--prob FILE              : use FILE to output the actual probabilities
	                                  used to generate pwm (with zeroes adjusted)
	--gomer FILE                : Like -p/--prob, but makes FILE suitable as input
	                              to GOMER by adding values for %PSEUDO_TEMP and %NAME
    -s/--smooth PERCENT         : Smooth the probability matrix by substracting PERCENT from each probability then rescaling
	-e/--equal FILE             : use equal (0.25) base frequencies
	-i/--intergenic FILE        : use intergenic base frequencies
	-g/--genomic FILE           : use genomic base frequencies
	--genomic-with-mito FILE : use genomic WITH mitochondria base frequencies
	--test-scaling              : for testing purposes only. If you don't know 
	                              what this is, you shouldn't be using it!!!
	"""
	sys.exit(2)
#@-body
#@-node:2::<<def usage>>



#@<<if main>>
#@+node:3::<<if main>>
#@+body
if __name__ == '__main__':
	main()

#@-body
#@-node:3::<<if main>>


#@-body
#@-node:8::<<command line>>
#@-body
#@-node:0::@file prob_to_pwm.py
#@-leo
