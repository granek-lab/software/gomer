#! /usr/bin/env python
from __future__ import division

"""
Takes multiple PWMs and features, and normalizes score for
each TF to highest score found for that TF across all features.
By Sarah Kolitz and Josh Granek
April 2004
"""

##------------------------------------------------------------------------------
## WORKS ON POWERBOOK, BUT NOT ON GINGKO:
##
## ~/lab/GOMER/python_code/gomer_vision.py ~/lab/GOMER/input_files/sequence_file_tables/cerevisiae_chromosome_table_with_mito.pb.jan_2004 Ndt80_bound_matrix.probs YLR307W -fYER106W -f YOR214C  3000 3000 -c 0.01 -m Sum1_bound_matrix.probs &
##
## ~/lab/GOMER/python_code/gomer_vision.py ~/lab/GOMER/input_files/sequence_file_tables/cerevisiae_chromosome_table_with_mito.pb.jan_2004 ~/lab/GOMER/projects/Sum1_Ndt80/binding_matrices/Ndt80_bound_matrix.probs YLR307W -fYER106W -f YOR214C  3000 3000 -c 0.01 -m ~/lab/GOMER/projects/Sum1_Ndt80/binding_matrices/Sum1_bound_matrix.probs &
##
## ~/lab/GOMER/python_code/gomer_vision.py ~/lab/GOMER/input_files/sequence_file_tables/cerevisiae_chromosome_table_with_mito.pb.jan_2004 ~/lab/GOMER/projects/Sum1_Ndt80/binding_matrices/Ndt80_bound_matrix.probs --feature_file ~/lab/GOMER/projects/Sum1_Ndt80/regulated/clean/Sum1_Ndt80_intersection.exclude.reg.2  3000 3000 -c 0.01 -m ~/lab/GOMER/projects/Sum1_Ndt80/binding_matrices/Sum1_bound_matrix.probs &
##------------------------------------------------------------------------------


import sys
import os
from reverse_complement import reverse_complement
from optparse import OptionParser
import biggles
import Numeric

from gomer import load_chromosome_table, load_feature_file, load_chromosome_sequences, load_probability_matrix
from regulated_feature_parser_gomer import ParseRegulatedFeatures

point_size=6
color_array = ['red', 'goldenrod', 'blue','pink',
			   'lime', 'orange', 'purple', 'green',
			   'royalblue', 'orangered', 'black',
			   'saddlebrown']


def main():	
	
	version_num = 0.001
	version = '%prog ' + str(version_num)
	usage = 'usage: %prog [options] ChromTable ProbMatrix FEATURE UPSTREAM_BP DOWNSTREAM_BP\n'
	usage +='        --feature_file ChromTable ProbMatrix         UPSTREAM_BP DOWNSTREAM_BP\n'
	
	parser = OptionParser(usage=usage, version=version)

	parser.add_option('-p', '--postscript',
					  action="store",
					  help='Save to postscript FILE.',
      			                  metavar='FILE')
	parser.add_option('-m', '--probability_matrix',
			  action="append",
			  help='Use additional position weight matrix file PWMFILE.',
			  metavar = 'PWMFILE')

	parser.add_option('-f', '--feature_name',
			  action="append",
			  help='Examine additional FEATURE.',
			  metavar = 'FEATURE')			  

	parser.add_option('--feature_file',
			  help="Use the features found in FILE (in GOMER's regulated feature file format) instead of from the command line.",
			  metavar = 'FILE')			  

	parser.add_option('-c', '--cutoff',
			  type="float",
			  help='Show only scores above CUTOFF value.',
			  metavar = 'CUTOFF')			  


					  
	(options, args) = parser.parse_args()
	NORM_NUM_ARGS = 5



	if len(args) == (NORM_NUM_ARGS):
		chromosome_table_file_name = args[0]
		prob_matrix_filename = args[1]
		first_feature_name = args[2]
		upstream_bp = int(args[3])
		downstream_bp = int(args[4])
	elif len(args) == 4 and options.feature_file:
		chromosome_table_file_name = args[0]
		prob_matrix_filename = args[1]
		upstream_bp = int(args[2])
		downstream_bp = int(args[3])
	else :
		parser.print_help()
		sys.exit(2)

	
	prob_matrix_filenames = [prob_matrix_filename]
	if options.probability_matrix:
		prob_matrix_filenames.extend(options.probability_matrix)

	if options.feature_file:
		(regulated_feature_names,
         excluded_feature_names,
         unregulated_feature_names) = ParseRegulatedFeatures(options.feature_file)
		feature_names = regulated_feature_names.keys()
	else:
		feature_names = [first_feature_name]
		if options.feature_name:
			feature_names.extend(options.feature_name)


	print 'PWM files: ',prob_matrix_filenames
	print 'Feature names: ',feature_names

	(feature_file_list,
	 chromosome_file_hash,
	 chromosome_flags_hash,
	 frequency_hash) = load_chromosome_table(chromosome_table_file_name)

	chromosome_hash, feature_dictionary, ambiguous_feature_names, feature_type_dict = load_feature_file(feature_file_list)

	## feature_dictionary has an entry for every non-ambiguous feature_name in the genome
	## REMEMBER THAT feature_dictionary KEYS MUST BE capitalized!!!!


        ## Load chromosome sequences

	load_chromosome_sequences(chromosome_file_hash, chromosome_flags_hash,
				  chromosome_hash, feature_dictionary,
				  ambiguous_feature_names)

	if not frequency_hash:
		print 'NO BASE FREQUENCIES SUPPLIED - CALCULATING . . .'
		frequency_hash = base_count(chromosome_hash)
		print 'FREQUENCIES:', frequency_hash

	## Get PWMs from PWM files, and get overall Kamax.

	probability_matrices = []
	for matrix_filename in prob_matrix_filenames:
		probability_matrices.append(load_probability_matrix(matrix_filename, frequency_hash))


        ## WE NOW DO SCORING FOR EACH FEATURE, ONE AT A TIME:
	# We will store feature info in dictionary features_to_plot
	features_to_plot = {}
	my_features = {}
	for i in range(0,len(feature_names)):
		feature_name = feature_names[i]		

	        # gomer.py -aFILE/--ambiguous=FILE : Print ambiguous names from feature file FILE
	        #is ambiguous_feature_names all caps?
		if feature_name.upper() in ambiguous_feature_names:
			print >>sys.stderr, 'Ambiguous Name:', feature_name
			print >>sys.stderr, '\nOther Names:\n' + \
			      '\n\n'.join([','.join(feature.AllNames()) for feature in ambiguous_feature_names[feature_name.upper()]])
			sys.exit(1)

		# raise NotImplementedError, 'Test to be sure the feature is in feature_dictionary'
		feature = feature_dictionary[feature_name.upper()]


	        ## Define section of chromosome sequence to examine:

		the_chromosome_I_care_about = feature.Chromosome
		chromosome_sequence = the_chromosome_I_care_about.Sequence.Sequence # Don't ask

		# Section of sequence I care about:
		# check whether start > stop
		# if so, switch upstream with downstream and "stop" with "start"
		if feature.Start > feature.Stop:  # feature.Strand = '-' #(-=crick)
			print "Switching upstream and downstream, and start and stop"
			rel_upstream, rel_downstream = downstream_bp, upstream_bp
			feature_start = feature.Stop
			feature_stop = feature.Start
			print 'upstream bp: ',upstream_bp
			print 'downstream bp: ',downstream_bp
			print 'new feature_start:',feature_start
			print 'new feature_stop:',feature_stop
		else:
			rel_upstream, rel_downstream = upstream_bp, downstream_bp
			feature_start = feature.Start
			print 'Feature Feature_start: ',feature_start
			feature_stop = feature.Stop		
			print 'Feature Stop: ', feature_stop
		
		#-----------------------------------------------------------------------
		begin_seq_index = feature_start - rel_upstream - 1
		if begin_seq_index < 0:
			begin_seq_index = 0
			# print "Upstream  distanceYou asked to look too far upstream. I'll look from the beginning."
		end_seq_index = feature_stop + rel_downstream
		if end_seq_index > len(chromosome_sequence):
			end_seq_index = len(chromosome_sequence)
			# print "You asked to look too far downstream. I'll look until the end."
		print 'begin index: ',begin_seq_index,'  End index: ',end_seq_index
		sequence_to_score = chromosome_sequence[begin_seq_index:end_seq_index]
		if feature.Strand == '-':  #If feature is on Crick strand
			# want to score reverse complement of sequence
			sequence_to_score = reverse_complement(sequence_to_score)
		## print sequence_to_score
		#-----------------------------------------------------------------------

                ## NOW, FOR EACH TF, SCORE EACH STRAND
		TFs = {}
		for matrix in probability_matrices:  # For each TF
                    hitlist = matrix.FilterScoreSequence(sequence_to_score, filter_cutoff_ratio=0)
                    strands = {'forward':hitlist.Forward,'revcomp':hitlist.ReverseComplement}
                    # Store strand score dictionary for this TF in TFs dictionary:
		    TFs[matrix.Name] = strands

                ## FOR THIS FEATURE, STORE 'TFs' DICTIONARY IN 'features_to_plot' DICTIONARY:
                features_to_plot[feature.Name]=TFs
		my_features[feature.Name] = feature


	##===============================================================================

		
	## NORMALIZATION:
	# For each TF, find the highest Ka value (score) found in 
	# all the sequences scored; call this number Kamax.
	# Normalize each TF to its Kamax.
	Kamaxes = {}
	for matrix in probability_matrices:  # For each TF
		possKamaxes = []
		for featurekey in features_to_plot: # For each feature
			scoretuple = features_to_plot[featurekey][matrix.Name]
			for strandkey in scoretuple:
				possKamaxes.append(max(scoretuple[strandkey]))
	        Kamax = max(possKamaxes)
	        Kamaxes[matrix.Name] = Kamax
	print 'Kamaxes:', Kamaxes

	max_normalized_score_list = []
	for matrix in probability_matrices:
		for featurekey in features_to_plot: # For each feature
			scoretuple = features_to_plot[featurekey][matrix.Name]
			print 'Max raw score:',max(scoretuple['forward']+scoretuple['revcomp'])
			scoretuple['forward'] = scoretuple['forward']/Kamaxes[matrix.Name]
			scoretuple['revcomp'] = -1 * scoretuple['revcomp']/Kamaxes[matrix.Name]
			## print 'Max normalized score:',max(scoretuple['forward']+scoretuple['revcomp'])
			max_normalized_score_list.append(max(scoretuple['forward']))
			max_normalized_score_list.append(max(-scoretuple['revcomp']))
	

	## GRAPHING
	max_plots = 6
	w = max_plots
	plots_per_frame = min((max_plots,len(features_to_plot)+1 ))
	framearray_list = []
	# if the yrange is (-1,1), values that have a normalized score of 1 aren't plotted
	# framearray.yrange = (-1,1)
	# framearray.yrange = (-1.0001,1.0001)
	max_norm_score = max(max_normalized_score_list)
	max_norm_score += max_norm_score * 1e-10 
	# max_norm_score = max(max_norm_score, 1)
	print 'framearray.yrange = ', (-max_norm_score,max_norm_score)
	print 'framearray.xrange = ', (-upstream_bp, my_features[featurekey].Length+downstream_bp)
	##--------------------------------------------------------------------
	feature_keys = features_to_plot.keys()
	for feature_index in range(len(features_to_plot)):
		featurekey = feature_keys[feature_index]
		if w >= max_plots:
			#--------------------------------------------------------------------
			framearray = biggles.FramedArray(plots_per_frame, 1)
			framearray.ylabel='Normalized window score (Ka) for each feature'
			framearray.xlabel='Window number'
			framearray.cellspacing=2.
			# framearray.title=''
			framearray.yrange = (-max_norm_score,max_norm_score)
			framearray_list.append(framearray)
			if feature_index == 0:
				w=1
			else:
				w=0
			#--------------------------------------------------------------------
		#p = biggles.FramedPlot()
		#framearray[w,0].title=featurekey
		# Feature is numbered from 0, and pre-feature indices are < 0
		# So make x-axis run from -upstream_bp to my_features[featurekey].Length+downstream_bp
		framearray[w,0].xrange = (-upstream_bp, my_features[featurekey].Length+downstream_bp)


		# We have a finite number of symbols here; so can't plot more than that
		# many TFs without making changes (using colors, etc.)
		symbols = ["filled circle","filled square","filled triangle","filled diamond","filled inverted triangle","filled fancy square","filled fancy diamond","half filled circle","half filled square","half filled triangle","half filled diamond","half filled inverted triangle","half filled fancy square","half filled fancy diamond","filled octagon","circle","square","cross","dot","plus","asterisk","triangle","diamond","star","inverted triangle","starburst","fancy plus","fancy cross","fancy square","fancy diamond","octagon"]
		# For each TF, plot normalized forward strand scores vs. 
		# window number and opposite of normalized reverse
		# complement scores vs. window number (renumbering windows
		# to correspond with 0 at start of feature)
		q = 0
		if options.cutoff:
			cutoff = options.cutoff
			for matrix in probability_matrices:
				print matrix.Name, symbols[q]
				scoretuple = features_to_plot[featurekey][matrix.Name]
				x_list = []
				y_list = []
				for x,y in zip(Numeric.arange(-upstream_bp,len(scoretuple['forward'])-upstream_bp),
					       scoretuple['forward']):
					# if y >= cutoff or (-cutoff >= y):
					if y >= cutoff:
						x_list.append(x)
						y_list.append(y)
				center_line = biggles.LineY(0) 
				framearray[w,0].add(center_line)
				#---------------------------------------------------------------------
				## print x_list, y_list, color_array[q],symbols[q],point_size
				## x1 = biggles.Points([1,100], [0.5, 0.6], color=color_array[q],type=symbols[q], size=point_size)
				## print x1
				## framearray[w,0].add(x1)
				## x1 = biggles.Points([200], [0.5], color=color_array[q],type=symbols[q], size=point_size)
				## print x1
				## framearray[w,0].add(x1)
				## x1 = biggles.Points([1], [0.9999], color=color_array[q],type=symbols[q], size=point_size)
				## print x1
				## framearray[w,0].add(x1)
				#---------------------------------------------------------------------
				if x_list:
					x1 = biggles.Points(x_list, y_list, color=color_array[q%len(color_array)],
										type=symbols[q%len(symbols)], size=point_size)
					framearray[w,0].add(x1)
				##-----------------------------------------------------------
				x_list = []
				y_list = []
				for x,y in zip(Numeric.arange(-upstream_bp,len(scoretuple['revcomp'])-upstream_bp),
					       scoretuple['revcomp']):
					# if y >= cutoff or (-cutoff >= y):
					if -cutoff >= y:
						x_list.append(x)
						y_list.append(y)
				if x_list:
					x2 = biggles.Points(x_list, y_list, color=color_array[q%len(color_array)],
										type=symbols[q%len(symbols)], size=point_size)
					framearray[w,0].add(x2)
				##-----------------------------------------------------------
				q = q + 1
		else:
			for matrix in probability_matrices:
				scoretuple = features_to_plot[featurekey][matrix.Name]
				print matrix.Name, symbols[q]
				print 'max normalized score in forward list to be plotted:',max(scoretuple['forward'])
				print 'max normalized score in revcomp list to be plotted:',max(scoretuple['revcomp'])
				x1 = biggles.Points(Numeric.arange(-upstream_bp,len(scoretuple['forward'])-upstream_bp),
									scoretuple['forward'], color=color_array[q%len(color_array)],
									type=symbols[q%len(symbols)], size=point_size)
				framearray[w,0].add(x1)
				x1.label = matrix.Name
				x2 = biggles.Points(Numeric.arange(-upstream_bp,len(scoretuple['revcomp'])-upstream_bp),
									scoretuple['revcomp'], color=color_array[q%len(color_array)],
									type=symbols[q%len(symbols)], size=point_size)
				# TF has same symbol for both forward&revcomp scores
				framearray[w,0].add(x2)
				center_line = biggles.LineY(0) 
				framearray[w,0].add(center_line)
				q = q + 1
			
		##------------------------------------------------------------
		# Put a box on the plot to represent the feature
		box_min_x = 0 
		box_min_y = -0.25
		box_max_x = my_features[featurekey].Length
		box_max_y = 0.25
		box_color = 'DarkSlateGrey'
		featurebox = biggles.DataBox((box_min_x,box_min_y),(box_max_x,box_max_y), color=box_color, fillcolor=box_color, width=3)
		framearray[w,0].add(featurebox)

		label_x = (box_max_x - box_min_x)/2
		label_y = box_min_y/2
		label_color = 'DarkSlateGrey'
		featurelabel = biggles.DataLabel(label_x,label_y,featurekey,color=label_color)
		framearray[w,0].add(featurelabel)
		##------------------------------------------------------------
		w = w + 1
	key_list = []
	for i in range(len(probability_matrices)):
		key_point = biggles.Points([0], [1],
								   color=color_array[i%len(color_array)],
								   type=symbols[i%len(symbols)], size=point_size)
		key_point.label = probability_matrices[i].Name
		key_list.append(key_point)
	framearray_list[0][0,0].add(biggles.PlotKey(0.1,0.9, key_list))
		
	if options.postscript:
		# if user wanted ps, then make postscript file with that filename
		num = 0
		for framearray in framearray_list:
			if num == 0:
				file_name = options.postscript
			else:
				file_name = options.postscript + '.' + str(num)
			framearray.write_eps(file_name)
			num += 1
	else:
		for framearray in framearray_list:
			framearray.show()

if __name__ == '__main__':
	main()


"""	
Help on module genome_feature_gomer:

NAME
    genome_feature_gomer

FILE
    /home/josh/GOMER/python_code/genome_feature_gomer.py

CLASSES
    __builtin__.object
        GenomeFeature
    
    class GenomeFeature(__builtin__.object)
     |  Methods defined here:
     |  
     |  AllNames(self)
     |      A list of all of the names for this feature
     |  
     |  Properties defined here:
     |  Chromosome
     |  ChromosomeName
     |  CommonName
     |  IsOrf
     |  Length
     |  Name
     |  Start
     |  Stop
     |  Strand
     |  Types

Help on module chromosome_gomer:

NAME
    chromosome_gomer

FILE
    /home/josh/GOMER/python_code/chromosome_gomer.py

    class Chromosome(__builtin__.object)
     |  Name
     |  Sequence
"""	



