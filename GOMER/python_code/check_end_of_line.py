#! /usr/bin/env python

import sys
import re
import os

def usage():
	print >>sys.stderr, 'usage:', os.path.basename(sys.argv[0]), 'FILE'

if len(sys.argv) <> 2:
	usage()
	sys.exit(2)
	
filename = sys.argv[1]
handle = file(filename)
file_string = handle.read()

if re.findall('\r\n', file_string):
	end_of_line = r'"\r\n" (Windows style)'
elif re.findall('\n', file_string):
	end_of_line = r'"\n" (UNIX style)'
elif re.findall('\r', file_string):
	end_of_line = r'"\r" (Macintosh style)'
else:
	end_of_line = None

if end_of_line:
	print 'File: "'+handle.name+'" uses the', end_of_line, 'end of line convention'
else:
	print 'File: "'+handle.name+r'" does not contain "\r\n", "\n", or "\r" end of line characters'
