#! /usr/bin/env python
from __future__ import division
#@+leo
#@+node:0::@file dummy_feature_gomer.py
#@+body
#@@first
#@@first
#@@language python

"""
*Description:

A subclass of GenomeFeature.

I guess this is a little bit of a hack, but it seems like a slightly elegant hack.
So far, the only purpose of instances of this class is for use in the GetContained 
method of the Chromosome class.

The GetContained method is used to determine what features on the chromosome fall 
completely within a base pair range.  Functions of the general_bisect module are used 
in this method, essential the method works by using the bisect functions to determine where
dummy features would be inserted in the sorted list of features.  Any features between these 
insertion points are what we care about.
"""


#@<<imports>>
#@+node:1::<<imports>>
#@+body
##import os
##os.environ['PYCHECKER'] = '--allglobals --stdlib'
##import pychecker.checker

##import check_python_version
##check_python_version.CheckVersion()

from genome_feature_gomer import GenomeFeature

#@-body
#@-node:1::<<imports>>


class DummyFeature(GenomeFeature):

	#@+others
	#@+node:2::Orf.__init__
	#@+body
	def __init__(self, startcoord, stopcoord):
		self._start_coord = int(startcoord)
		self._stop_coord = int(stopcoord)
	
	#@-body
	#@-node:2::Orf.__init__
	#@-others
#@-body
#@-node:0::@file dummy_feature_gomer.py
#@-leo
