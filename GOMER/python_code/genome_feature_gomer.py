#! /usr/bin/env python
from __future__ import division
#@+leo
#@+node:0::@file genome_feature_gomer.py
#@+body
#@@first
#@@first
#@@language python

"""
*Description:

A Base class for features of a genome - ORFs, ARSs, rRNAs, etc.

This is not a virtual class!!

At the moment, every genome feature except ORFs are instances of GenomeFeature.
"""
##import os
##os.environ['PYCHECKER'] = '--allglobals --stdlib'
##import pychecker.checker

##import check_python_version
##check_python_version.CheckVersion()

import sys 

class GenomeFeature(object):

	#@+others
	#@+node:1::__init__
	#@+body
	def __init__(self, ref_name, common_name, alias_list, \
	 			 unique_id, alt_id_list,\
	 			 feature_type_list,\
	 			 startcoord, stopcoord, strand,\
	 			 description, chromosome_identifier, chromosome_reference):
		self._reference_name = ref_name
		self._common_name = common_name
		self._alternate_names = alias_list
		self._id = unique_id
		self._alt_id_list = alt_id_list
		self._alternate_names = alias_list
		self._feature_type_list = feature_type_list
		self._start_coord = int(startcoord)
		self._stop_coord = int(stopcoord)
		self._length = abs(self._stop_coord - self._start_coord) + 1
		self._strand = strand
		self._description = description
		self._chromosome_identifier = chromosome_identifier
		self._chromosome_reference = chromosome_reference
		
		self._name = ref_name
		self._unique_common_name = common_name


	#@-body
	#@-node:1::__init__
	#@+node:2::Start
	#@+body
	def _get_start(self): return self._start_coord
	def _set_start(self, value): raise AttributeError, "Can't set 'start' attribute"
	Start = property(_get_start, _set_start)
	
	def _get_length(self): return self._length
	def _set_length(self, value): raise AttributeError, "Can't set 'length' attribute"
	Length = property(_get_length, _set_length)
	
	#@-body
	#@-node:2::Start
	#@+node:3::Stop
	#@+body
	def _get_stop(self): return self._stop_coord
	def _set_stop(self, value): raise AttributeError, "Can't set 'stop' attribute"
	Stop = property(_get_stop, _set_stop)
	
	
	#@-body
	#@-node:3::Stop
	#@+node:4::Chromosome
	#@+body
	def _get_chromosome(self): return self._chromosome_reference
	def _set_chromosome(self, value): raise AttributeError, "Can't set 'Chromosome' attribute"
	Chromosome = property(_get_chromosome, _set_chromosome)
	"""The Chromosome instance itself"""
	
	#@-body
	#@-node:4::Chromosome
	#@+node:5::ChromosomeName
	#@+body
	def _get_chromosome_name(self): return self._chromosome_identifier
	def _set_chromosome_name(self, value): raise AttributeError, "Can't set 'ChromosomeName' attribute"
	ChromosomeName = property(_get_chromosome_name, _set_chromosome_name)
	
	
	#@-body
	#@-node:5::ChromosomeName
	#@+node:6::IsOrf
	#@+body
	## def IsOrf (self):
	## 	return 0
	def _get_is_orf(self): return 0
	def _set_is_orf(self, value): raise AttributeError, "Can't set 'IsOrf' attribute"
	IsOrf = property(_get_is_orf, _set_is_orf)
	
	#@-body
	#@-node:6::IsOrf
	#@+node:7::AllNames
	#@+body
	def AllNames (self):
		"""A list of all of the names for this feature"""
		names = [self._reference_name]
		if self._common_name:
			names.append(self._common_name)
		for new_name in self._alternate_names:
			if new_name :
				names.append(new_name)
		names.append(self._id)
		for new_name in self._alt_id_list:
			if new_name :
				names.append(new_name)
		return names
	
	#@-body
	#@-node:7::AllNames
	#@+node:8::Name
	#@+body
	# def _get_name(self): return raise NotImplemented
	def _get_name(self): return self._name
	def _set_name(self, value): raise AttributeError, "Can't set 'Name' attribute"
	Name = property(_get_name, _set_name)
	
	#@-body
	#@-node:8::Name
	#@+node:9::Types
	#@+body
	def _get_types(self): return tuple(self._feature_type_list)
	def _set_types(self, value): raise AttributeError, "Can't set 'Name' attribute"
	Types = property(_get_types, _set_types)
	
	#@-body
	#@-node:9::Types
	#@+node:10::CommonName
	#@+body
	def _get_common_name(self): return self._unique_common_name
	def _set_common_name(self, value): raise AttributeError, "Can't set 'CommonName' attribute"
	CommonName = property(_get_common_name, _set_common_name)
	
	#@-body
	#@-node:10::CommonName
	#@+node:11::Strand
	#@+body
	def _get_strand(self): return self._strand
	def _set_strand(self, value): raise AttributeError, "Can't set 'Strand' attribute"
	Strand = property(_get_strand, _set_strand)
	
	#@-body
	#@-node:11::Strand
	#@+node:12::SetUnambiguousNames()
	#@+body
	def SetUnambiguousNames(self, ambiguous_names):
		all_names = self.AllNames()
	
		index = 0
		while self._name in ambiguous_names:
			if index >= len(all_names):
				raise BlahError, 'No unambiguous name is available for feature: ' + str(all_names) + '\n'
			self._name = all_names[index]
			index += 1

		index = 0
		while self._unique_common_name in ambiguous_names:
			if index >= len(all_names):
				raise BlahError, 'No unambiguous name is available for feature: ' + str(all_names) + '\n'
			self._unique_common_name = all_names[index]
			index += 1
	
	
	#@-body
	#@-node:12::SetUnambiguousNames()
	#@-others
#@-body
#@-node:0::@file genome_feature_gomer.py
#@-leo
