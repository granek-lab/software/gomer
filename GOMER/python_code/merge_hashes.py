#! /usr/bin/env python
from __future__ import division
from __future__ import generators
#@+leo
#@+node:0::@file merge_hashes.py
#@+body
#@@first
#@@first
#@@first
#@@language python

"""
*Description:

A function for merging two hashes.  For any key that is found in both hashes 
the value accessed by that key must be the same in both hashs, otherwise an
exception is raised.

I believe this is loosely based on ideas from the Python Cookbook
"""

#@<<imports>>
#@+node:1::<<imports>>
#@+body
##import os
##os.environ['PYCHECKER'] = '--allglobals --stdlib'
##import pychecker.checker

##import check_python_version
##check_python_version.CheckVersion()

from exceptions_gomer import GomerError
#@-body
#@-node:1::<<imports>>


#@<<MergeHashError>>
#@+node:2::<<MergeHashError>>
#@+body
class MergeHashError(GomerError):
	pass

#@-body
#@-node:2::<<MergeHashError>>


def merge_hashes(hash1, hash2):
	if not hash1:
		return hash2
	elif not hash2:
		return hash1

	intersection = 0
	for key in hash1.keys():
		if key in hash2:
			if not hash1[key] == hash2[key]:
				intersection += 1

	if not intersection:
		new_hash = {}
		new_hash.update(hash1)
		new_hash.update(hash2)
	else:
		raise MergeHashError('Hashes have different entries for at least one key')
	return new_hash


#@<<if __main__>>
#@+node:3::<<if __main__>>
#@+body
if __name__ == '__main__':
	import sys
	if len(sys.argv) == 3:
		hash1 = eval(sys.argv[1])
		hash2 = eval(sys.argv[2])
	else :
		print 'Need at least two hashes'
		sys.exit(1)
	# print probs
	print 'Merged', merge_hashes(hash1, hash2)



#@-body
#@-node:3::<<if __main__>>


#@-body
#@-node:0::@file merge_hashes.py
#@-leo
