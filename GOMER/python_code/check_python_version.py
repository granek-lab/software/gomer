#! /usr/bin/env python
from __future__ import division
#@+leo
#@+node:0::@file check_python_version.py
#@+body
#@@first
#@@first
#@@language python

"""
*Description:

Very simple - checks to be sure the version of python is sufficient to support all features required
by the code
"""
##import os
##os.environ['PYCHECKER'] = '--allglobals --stdlib'
##import pychecker.checker

import sys


MINIMUM_PYTHON_VERSION = '2.2.1'
MINIMUM_PYTHON_VERSION_HEX = 0x20201f0


#@<<VersionError>>
#@+node:1::<<VersionError>>
#@+body
class VersionError(StandardError):
    def __init__(self, value):
        super(VersionError, self).__init__()
        self.value = value
    def __str__(self):
        return `self.value`


#@-body
#@-node:1::<<VersionError>>


def CheckVersion():
    if sys.hexversion < MINIMUM_PYTHON_VERSION_HEX:
        sys.stderr.write( ' '.join(('ERROR: python version', str(MINIMUM_PYTHON_VERSION), 'is required\nThe version on this system is :\n', sys.version )))
        raise VersionError, (sys.version, MINIMUM_PYTHON_VERSION)


#@<<if_main>>
#@+node:2::<<if_main>>
#@+body
if __name__ == '__main__':
    try :
        CheckVersion()
    except VersionError, (current_version, expected_version):
        sys.stderr.write(' '.join(('ERROR: Python version is:', current_version, '\nNeed version:', expected_version)))
    else :
        sys.stderr.write(' '.join(('OK: Python version is current enough', '\n\nMinimum is version:', MINIMUM_PYTHON_VERSION, '\nYou have version:', sys.version, '\n' )))

#@-body
#@-node:2::<<if_main>>
#@-body
#@-node:0::@file check_python_version.py
#@-leo
