#! /usr/bin/env python
from __future__ import division
#@+leo
#@+node:0::@file cerevisiae_sequence_parser_gomer.py
#@+body
#@@first
#@@first
#@@language python

"""
*Description:

Parses Saccharomyces cerevisiae sequence files, returning instances of the Sequence class
"""

#@<<imports>>
#@+node:3::<<imports>>
#@+body
##import os
##os.environ['PYCHECKER'] = '--allglobals --stdlib'
##import pychecker.checker

##import check_python_version
##check_python_version.CheckVersion()

import re
import fileinput
import sys

from parser_gomer import Parser
from parser_gomer import ParseError
from globals_gomer import SEQUENCE_ALPHABET, NO_INFO_SYMBOL, NULL_SYMBOL
from sequence_gomer import Sequence
import roman

from open_file import OpenFile

#@-body
#@-node:3::<<imports>>


#@<<regular expressions>>
#@+node:5::<<regular expressions>>
#@+body
COMMENT_TOKEN = '>'


#comment_re = re.compile('^\s*' + COMMENT_TOKEN + '(.+)')
comment_re = re.compile('^' + COMMENT_TOKEN + '(.+)$')
empty_line_re = re.compile('^\s*$')
sequence_re = re.compile('^([' + SEQUENCE_ALPHABET + NO_INFO_SYMBOL + NULL_SYMBOL + ']+)\s*$')
# bases_only_re = re.compile('^[' + SEQUENCE_ALPHABET + ']+$')
symbol_only_re = re.compile('^[' + SEQUENCE_ALPHABET + NO_INFO_SYMBOL + NULL_SYMBOL + ']+$')





#@-body
#@-node:5::<<regular expressions>>


## class CerevisiaeSequenceParser(Parser):

	#@+others
	#@+node:1::test cerevisiae_sequence_parser_gomer.py
	#@+body
	#@+doc
	# 
	# 
	# python cerevisiae_sequence_parser_gomer.py 
	# ~/genomes/saccharomyces_cerevisiae/oct_23_2002/chr04.fsa 4

	#@-doc
	#@-body
	#@-node:1::test cerevisiae_sequence_parser_gomer.py
	#@+node:2::Mitochondrial DNA
	#@+body
	#@+doc
	# 
	# 
	# At the moment Mitochondrial DNA is being included, this should probably 
	# be an option
	# 
	# In Saccharomyces, Mitochondrial DNA is chromosome 17
	# 

	#@-doc
	#@-body
	#@-node:2::Mitochondrial DNA
	#@+node:4::__init__
	#@+body
##	def __init__ (self):
##		Parser.__init__(self)
	#@-body
	#@-node:4::__init__
	#@+node:6::def Parse
	#@+body
##------------------------------------------------------------------------------
def ParseCerevisiaeSequence(sequence_file_name, chromosome_name, chromosome_flags):
	"""
	"""
	ORGANISM = 'Saccharomyces cerevisiae'
	MOLTYPE = 'genomic'
	STRAIN = 'S288C'

##	if __debug__:
##		print >>sys.stderr, 'Parsing Chromosome:', chromosome_name, 'From file:', sequence_file_name
	
	sequence_handle = OpenFile(sequence_file_name, 'Parse Sequence File')
	if __debug__:
		print >>sys.stderr, 'Parsing Chromosome:', chromosome_name, 'From file:', sequence_handle.name
	cur_line = ' '
	while empty_line_re.search(cur_line):
		cur_line = sequence_handle.readline()

	if comment_re.search(cur_line):
		cur_comment = comment_re.search(cur_line).group(1)
		reference, accession_number, modifier_hash = parse_comment(cur_comment, sequence_handle)
##******************************************************************************
		check_pairs = tuple((('chromosome_label', chromosome_name),
							 ('org', ORGANISM),
							 ('moltype', MOLTYPE),
							 ('strain', STRAIN)))
##		for modifier, standard in check_pairs:
##			if not modifier_hash[modifier] == standard:
##				raise SequenceParseError (sequence_handle.name,
##										  cur_line,
##										  'Error: ' + modifier + ': "' +
##										  modifier_hash[modifier] +
##										  '" does not match expected value: "'
##										  + standard + '"')

##******************************************************************************
	else:
		raise SequenceParseError (sequence_handle.name,
								  cur_line,
								  'Error: sequence must begin with a comment line')

	
	sequence = sequence_handle.read().strip()
	sequence = sequence.replace('\n','')
	sequence = sequence.replace('\r','')
	if not symbol_only_re.match(sequence):
	 	raise SequenceParseError (sequence_handle.name,
	 							  cur_line,
	 							  'Error: Only sequence and empty lines may follow the comment line')
	sequence_handle.close()
	return Sequence(sequence, modifier_hash, accession_number, sequence_file_name, chromosome_name)
##------------------------------------------------------------------------------
def OlderParseCerevisiaeSequence(sequence_file_name, chromosome_name, chromosome_flags):
	"""
	"""
	ORGANISM = 'Saccharomyces cerevisiae'
	MOLTYPE = 'genomic'
	STRAIN = 'S288C'

	if __debug__:
		print >>sys.stderr, 'Parsing Chromosome:', chromosome_name, 'From file:', sequence_file_name
	
	sequence_file_iterator = fileinput.input(sequence_file_name)
	cur_line = ' '
	while empty_line_re.search(cur_line):
		cur_line = sequence_file_iterator.readline()

	if comment_re.search(cur_line):
		cur_comment = comment_re.search(cur_line).group(1)
		reference, accession_number, modifier_hash = parse_comment(cur_comment, sequence_file_iterator)
##******************************************************************************
		check_pairs = tuple((('chromosome_label', chromosome_name),
							 ('org', ORGANISM),
							 ('moltype', MOLTYPE),
							 ('strain', STRAIN)))
		for modifier, standard in check_pairs:
			if not modifier_hash[modifier] == standard:
				raise SequenceParseError (sequence_file_iterator.filename(),
										  cur_line,
										  'Error: ' + modifier + ': "' + modifier_hash[modifier] +
										  '" does not match expected value: "' + standard + '"',
										  sequence_file_iterator.lineno())

##******************************************************************************
		cur_line = sequence_file_iterator.readline()
	else:
		raise SequenceParseError (sequence_file_iterator.filename(),
								  cur_line,
								  'Error: sequence must begin with a comment line',
								  sequence_file_iterator.lineno())

	#@<<FasterLoop>>
	#@+node:1::<<FasterLoop>>
	#@+body

	#@<<test results>>
	#@+node:1::<<test results>>
	#@+body
	#@+doc
	# 
	# The results below from several runs with both loops show that the 
	# faster loop is much faster!!
	# 
	# 
	# ncalls -  for the number of calls,
	# 
	# tottime - for the total time spent in the given function (and 
	# excluding time made in calls to sub-functions),
	# 
	# percall - is the quotient of tottime divided by ncalls
	# 
	# cumtime - is the total time spent in this and all subfunctions (from 
	# invocation till exit). This figure is accurate even for recursive functions.
	# 
	# percall -  is the quotient of cumtime divided by primitive calls
	# 
	# filename:lineno(function) - provides the respective data of each function
	# ----------------------------------------------------------------------------
	# ncalls  tottime  percall  cumtime  percall filename:lineno(function)
	#      1   66.680   66.680   67.240   67.240 
	# cerevisiae_sequence_parser_gomer.py:76(Parse) - SimpleParse
	#      1   66.450   66.450   67.170   67.170 
	# cerevisiae_sequence_parser_gomer.py:76(Parse) - SimpleParse
	#      1   66.100   66.100   66.780   66.780 
	# cerevisiae_sequence_parser_gomer.py:76(Parse) - SimpleParse
	#      1   66.850   66.850   67.490   67.490 
	# cerevisiae_sequence_parser_gomer.py:76(Parse) - SimpleParse
	#      1   63.680   63.680   64.480   64.480 
	# cerevisiae_sequence_parser_gomer.py:76(Parse) - SimpleParse
	#      1   63.540   63.540   64.120   64.120 
	# cerevisiae_sequence_parser_gomer.py:76(Parse) - SimpleParse
	#      1   63.650   63.650   64.280   64.280 
	# cerevisiae_sequence_parser_gomer.py:76(Parse) - SimpleParse
	#      1   64.120   64.120   64.750   64.750 
	# cerevisiae_sequence_parser_gomer.py:76(Parse) - SimpleParse
	#      1   63.850   63.850   64.470   64.470 
	# cerevisiae_sequence_parser_gomer.py:76(Parse) - SimpleParse
	#      1   63.880   63.880   64.430   64.430 
	# cerevisiae_sequence_parser_gomer.py:76(Parse) - SimpleParse
	#      1   63.780   63.780   64.470   64.470 
	# cerevisiae_sequence_parser_gomer.py:76(Parse) - SimpleParse
	# 
	# -----------------------------------------------------------------------------------------
	#      1    0.990    0.990    1.530    1.530 
	# cerevisiae_sequence_parser_gomer.py:76(Parse) - Faster?Parse
	#      1    0.940    0.940    1.420    1.420 
	# cerevisiae_sequence_parser_gomer.py:76(Parse) - Faster?Parse
	#      1    0.830    0.830    1.370    1.370 
	# cerevisiae_sequence_parser_gomer.py:76(Parse) - Faster?Parse
	#      1    0.940    0.940    1.390    1.390 
	# cerevisiae_sequence_parser_gomer.py:76(Parse) - Faster?Parse
	#      1    1.040    1.040    1.520    1.520 
	# cerevisiae_sequence_parser_gomer.py:76(Parse) - Faster?Parse
	#      1    0.680    0.680    1.330    1.330 
	# cerevisiae_sequence_parser_gomer.py:76(Parse) - Faster?Parse
	#      1    0.770    0.770    1.440    1.440 
	# cerevisiae_sequence_parser_gomer.py:76(Parse) - Faster?Parse
	#      1    0.870    0.870    1.320    1.320 
	# cerevisiae_sequence_parser_gomer.py:76(Parse) - Faster?Parse
	#      1    0.960    0.960    1.450    1.450 
	# cerevisiae_sequence_parser_gomer.py:76(Parse) - Faster?Parse
	#      1    0.970    0.970    1.450    1.450 
	# cerevisiae_sequence_parser_gomer.py:76(Parse) - Faster?Parse

	#@-doc
	#@-body
	#@-node:1::<<test results>>


	#@+doc
	# 
	# # this is a little complicated, but is about 60 times faster than 
	# simple concatenation - see test results

	#@-doc
	#@@code
	# @doc
	# print 'Faster?Parse'
	# @doc
	index = 0
	NUM_BLOCKS = 50
	sequence = ['']*NUM_BLOCKS
	while cur_line:
		sequence_match = sequence_re.search(cur_line)
		if sequence_match:
			if index >= len(sequence):
				sequence.extend(['']*NUM_BLOCKS)
			sequence[index] =  sequence_match.group(1)
			index += 1
			cur_line = sequence_file_iterator.readline()

		elif empty_line_re.search(cur_line): # allow for empty lines at end of file
			cur_line = sequence_file_iterator.readline()
			break
		else:
			raise SequenceParseError (sequence_file_iterator.filename(),
									  cur_line,
									  'Error: Only sequence and empty lines may follow the comment line',
									  sequence_file_iterator.lineno())

	#@-body
	#@-node:1::<<FasterLoop>>


	#@<<SimplerLoop>>
	#@+node:3::<<SimplerLoop>>
	#@+body
	#@+doc
	# 
	# # a simpler version of the "FasterLoop?" code block, but it might be slower
	# print 'SimpleParse'
	# sequence = ''
	# while cur_line:
	# 	sequence_match = sequence_re.search(cur_line)
	# 	if sequence_match:
	# 		sequence +=  sequence_match.group(1)
	# 		cur_line = sequence_file_iterator.readline()
	# 
	# 	elif empty_line_re.search(cur_line): # allow for empty lines at end 
	# of file
	# 		cur_line = sequence_file_iterator.readline()
	# 		break
	# 	else:
	# 		raise SequenceParseError (sequence_file_iterator.filename(),
	# 							  cur_line,
	# 							  sequence_file_iterator.lineno(),
	# 							  'Error: Only sequence and empty lines may follow the 
	# comment line')

	#@-doc
	#@-body
	#@-node:3::<<SimplerLoop>>


	#@<<SimpleFasterLoop>>
	#@+node:2::<<SimpleFasterLoop>>
	#@+body
	#@+doc
	# 
	# # this is a little complicated, but is about 60 times faster than 
	# simple concatenation - see test results
	# # @code
	# # @doc
	# print 'SimplerFaster?Parse'
	# # @doc
	# sequence = sequence_file_iterator.read()
	# sequence = sequence.replace('\n','')
	# 
	# if not bases_only_re.search(sequence):
	# 	raise SequenceParseError (sequence_file_iterator.filename(),
	# 							  cur_line,
	# 							  sequence_file_iterator.lineno(),
	# 							  'Error: Only sequence and empty lines may follow the 
	# comment line')

	#@-doc
	#@-body
	#@-node:2::<<SimpleFasterLoop>>

	# allow for empty lines at end of file
	while cur_line:
		if empty_line_re.search(cur_line):
			cur_line = sequence_file_iterator.readline()
		else:
			raise SequenceParseError (sequence_file_iterator.filename(),
									  cur_line,
									  'Error: Only whitespace is allowed after the sequence',
									  sequence_file_iterator.lineno()
									  )
	sequence = ''.join(sequence)
	# print sequence
	sequence_file_iterator.close()
	return Sequence(sequence, modifier_hash, accession_number, sequence_file_name, chromosome_name)
	
	
	#@-body
	#@-node:6::def Parse
	#@+node:7::def _parse_comment
	#@+body
##------------------------------------------------------------------------------
##------------------------------------------------------------------------------
def OldParseCerevisiaeSequence(sequence_file_name, chromosome_name, chromosome_flags):
	"""
	"""
	ORGANISM = 'Saccharomyces cerevisiae'
	MOLTYPE = 'genomic'
	STRAIN = 'S288C'

	if __debug__:
		print >>sys.stderr, 'Parsing Chromosome:', chromosome_name, 'From file:', sequence_file_name
	
	sequence_handle = OpenFile(sequence_file_name, 'Parse Sequence File')
	cur_line = ' '
	while empty_line_re.search(cur_line):
		cur_line = sequence_handle.readline()

	if comment_re.search(cur_line):
		cur_comment = comment_re.search(cur_line).group(1)
		reference, accession_number, modifier_hash = parse_comment(cur_comment, sequence_handle)
##******************************************************************************
		check_pairs = tuple((('chromosome_label', chromosome_name),
							 ('org', ORGANISM),
							 ('moltype', MOLTYPE),
							 ('strain', STRAIN)))
		for modifier, standard in check_pairs:
			if not modifier_hash[modifier] == standard:
				raise SequenceParseError (sequence_handle.name,
										  cur_line,
										  'Error: ' + modifier + ': "' +
										  modifier_hash[modifier] +
										  '" does not match expected value: "'
										  + standard + '"')

##******************************************************************************
		cur_line = sequence_handle.readline()
	else:
		raise SequenceParseError (sequence_handle.name,
								  cur_line,
								  'Error: sequence must begin with a comment line')

	index = 0
	NUM_BLOCKS = 50
	sequence = ['']*NUM_BLOCKS
	while cur_line:
		sequence_match = sequence_re.search(cur_line)
		if sequence_match:
			if index >= len(sequence):
				sequence.extend(['']*NUM_BLOCKS)
			sequence[index] =  sequence_match.group(1)
			index += 1
			cur_line = sequence_handle.readline()

		elif empty_line_re.search(cur_line): # allow for empty lines at end of file
			cur_line = sequence_handle.readline()
			break
		else:
			raise SequenceParseError (sequence_handle.name,
									  cur_line,
									  'Error: Only sequence and empty lines may follow the comment line')

	#@-body
	#@-node:1::<<FasterLoop>>


	#@<<SimplerLoop>>
	#@+node:3::<<SimplerLoop>>
	#@+body
	#@+doc
	# 
	# # a simpler version of the "FasterLoop?" code block, but it might be slower
	# print 'SimpleParse'
	# sequence = ''
	# while cur_line:
	# 	sequence_match = sequence_re.search(cur_line)
	# 	if sequence_match:
	# 		sequence +=  sequence_match.group(1)
	# 		cur_line = sequence_handle.readline()
	# 
	# 	elif empty_line_re.search(cur_line): # allow for empty lines at end 
	# of file
	# 		cur_line = sequence_handle.readline()
	# 		break
	# 	else:
	# 		raise SequenceParseError (sequence_handle.filename(),
	# 							  cur_line,
	# 							  sequence_handle.lineno(),
	# 							  'Error: Only sequence and empty lines may follow the 
	# comment line')

	#@-doc
	#@-body
	#@-node:3::<<SimplerLoop>>


	#@<<SimpleFasterLoop>>
	#@+node:2::<<SimpleFasterLoop>>
	#@+body
	#@+doc
	# 
	# # this is a little complicated, but is about 60 times faster than 
	# simple concatenation - see test results
	# # @code
	# # @doc
	# print 'SimplerFaster?Parse'
	# # @doc
	# sequence = sequence_handle.read()
	# sequence = sequence.replace('\n','')
	# 
	# if not bases_only_re.search(sequence):
	# 	raise SequenceParseError (sequence_handle.filename(),
	# 							  cur_line,
	# 							  sequence_handle.lineno(),
	# 							  'Error: Only sequence and empty lines may follow the 
	# comment line')

	#@-doc
	#@-body
	#@-node:2::<<SimpleFasterLoop>>

	# allow for empty lines at end of file
	while cur_line:
		if empty_line_re.search(cur_line):
			cur_line = sequence_handle.readline()
		else:
			raise SequenceParseError (sequence_handle.name,
								  cur_line,
								  'Error: Only whitespace is allowed after the sequence')
	sequence = ''.join(sequence)
	# print sequence
	sequence_handle.close()
	return Sequence(sequence, modifier_hash, accession_number, sequence_file_name, chromosome_name)
##------------------------------------------------------------------------------
















##------------------------------------------------------------------------------





def parse_comment(comment_string, sequence_file_iterator):
	comment_separator = '|'
	if comment_separator not in comment_string:
		ref = ''
		accession_number = ''
		modifier_hash = {}
		return ref, accession_number, modifier_hash
	ref, accession_number, modifier_string = comment_string.split(comment_separator)
	ref = ref.strip()
	accession_number = accession_number.strip()
	modifier_list = modifier_string.split('[')
	modifier_hash = {}
## 	for index in range(len(modifier_list)):
	for cur_modifier in modifier_list:
		if empty_line_re.search(cur_modifier):
			continue
		cur_modifier = cur_modifier.replace(']', '')
		# print cur_modifier.split('=')
		modifier_pair = cur_modifier.split('=')
		if len(modifier_pair) <> 2:
			raise SequenceParseError (sequence_file_iterator.filename(),
									  comment_string,
									  sequence_file_iterator.lineno(),
									  'Error: Modifiers must be in format '+
									  '"[modifier=value]", this is incorrect:\n' +
									  cur_modifier + '\n')
		modifier = modifier_pair[0].strip().lower()
		value = modifier_pair[1].strip()
		modifier_hash[modifier] = value
	if 'chromosome' in modifier_hash:
		modifier_hash['chromosome_label'] = str(roman.roman_to_int(modifier_hash['chromosome']))
	elif modifier_hash.get('location', '') == 'mitochondrion':
		modifier_hash['chromosome'] = 'mitochondrion'
		modifier_hash['chromosome_label'] = '17'
	else :
		raise SequenceParseError (sequence_file_iterator.filename(),
								  comment_string,
								  sequence_file_iterator.lineno(),
								  'Error: File is missing "chromosome" modifier\n')
	return ref, accession_number, modifier_hash
	
	#@-body
	#@-node:7::def _parse_comment
	#@-others



#@<<Parse Errors>>
#@+node:8::<<Parse Errors>>
#@+body
#@@code
#@+others
#@+node:1::SequenceParseError
#@+body
class SequenceParseError(ParseError):
	def __init__(self, file_name, line, message, line_number=''):
		ParseError(file_name, line, line_number, message)
#@-body
#@-node:1::SequenceParseError
#@-others




#@-body
#@-node:8::<<Parse Errors>>



#@<<command line>>
#@+node:9::<<command line>>
#@+body
#@<<def run>>
#@+node:1::<<def run>>
#@+body
def run (sequence_file_name, chromosome_label):
	ParseCerevisiaeSequence(sequence_file_name, chromosome_label, None)


#@-body
#@-node:1::<<def run>>


#@<<def usage>>
#@+node:2::<<def usage>>
#@+body
def usage () :
	print '-'*70 + '\n', 'Arguments given (', len(sys.argv), '):', sys.argv, """
	Expected:
		1. Chromosome sequence file name
		2. Chromosome label
	""" + '\n' + '-'*70
	sys.exit(1)

#@-body
#@-node:2::<<def usage>>



#@<<if main>>
#@+node:3::<<if main>>
#@+body
if __name__ == '__main__':


#@+doc
# 

#@-doc
#@@code
	# Profiling code
	import os
	import md5
	#******************
	import profile
	#******************
	print os.getcwd()
	sequence_file_name = sys.argv[1]
	chromosome_label = sys.argv[2]
##	try :
##		# run(sequence_file_name, chromosome_label)
##		profile.run('run(sequence_file_name, chromosome_label)')
##	except SequenceParseError, instance:
##		instance.print_message()
##		print '\n'
##		raise
	print 'OldParseCerevisiaeSequence'
	test_sequence = OldParseCerevisiaeSequence(sequence_file_name, chromosome_label, None)
	print len(test_sequence), md5.new(test_sequence.Sequence).hexdigest()
	for x in range(20):
		profile.run('OldParseCerevisiaeSequence(sequence_file_name, chromosome_label, None)')

	print 'ParseCerevisiaeSequence'
	original_sequence = ParseCerevisiaeSequence(sequence_file_name, chromosome_label, None)
	print len(original_sequence), md5.new(original_sequence.Sequence).hexdigest()

	if len(test_sequence) <> len(original_sequence):
		raise 'len(test_sequence) <> len(original_sequence)'
	if  md5.new(test_sequence.Sequence).hexdigest() <> md5.new(original_sequence.Sequence).hexdigest():
		raise 'md5.new(test_sequence.Sequence).hexdigest() <> md5.new(original_sequence.Sequence).hexdigest()'

	for x in range(20):
		profile.run('ParseCerevisiaeSequence(sequence_file_name, chromosome_label, None)')
		# profile.run('run(sequence_file_name, chromosome_label)')



#@@code
#@+doc
# 
# # NON-profiling code
# 	import os
# 	import sys
# 	print os.getcwd()
# 	sequence_file_name = sys.argv[1]
# 	chromosome_label = sys.argv[2]
# 	try :
# 		run(sequence_file_name, chromosome_label)
# 	except SequenceParseError, instance:
# 		instance.print_message()
# 		print '\n'
# 		raise
# 

#@-doc
#@-body
#@-node:3::<<if main>>


#@-body
#@-node:9::<<command line>>
#@-body
#@-node:0::@file cerevisiae_sequence_parser_gomer.py
#@-leo
