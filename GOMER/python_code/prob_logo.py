#! /usr/bin/env python

import sys
import os
import re
import tempfile
from optparse import OptionParser
import operator

from information import information_content_alt

def main():
	tempfile_names = [] # keep track of temp files, so they can be deleted when done - no need to leave a mess
	#-------------------------------------------------
	usage = "usage: %prog [options] PROB_FILE"
	parser = OptionParser(usage=usage)
	parser.add_option("-o", "--out", 
					  help="Output to FILE, instead of the default (prob filename with '.ps' appended)", metavar="FILE")
	parser.add_option("-t", "--title",
					  action="store_true", default=False,
					  help="Include name of hmmfile as title in logo")
	parser.add_option("-c", "--color",
					  action="store_true", default=False,
					  help="Make the logo in color")
	parser.add_option("--clean",
					  action="store_true", default=False,
					  help="Make a sequence logo without axes or axis labels")
	parser.add_option("--rotate",
					  action="store_true", default=False,
					  help="Make logo rotated by 90 degrees")

	(options, args) = parser.parse_args()
	#-------------------------------------------------
	if len(args) == 1:
		prob_filename = args[0] 
	else:
		parser.error("incorrect number of arguments")
		sys.exit(0)
	#-------------------------------------------------
	if options.out:
		output_filename = options.out
	else:
		output_filename = prob_filename + '.ps'
	print >>sys.stderr, 'Outputting to:', output_filename
	#-------------------------------------------------
	if (os.path.exists(output_filename)):
		print >>sys.stderr, output_filename, 'Refusing to erase existing output file'
		sys.exit(0)
	#-------------------------------------------------

	prob_matrix = parse_probfile(prob_filename)
##	print 'PROB MATRIX:\n', '\n'.join([str(cur_line) for cur_line in prob_matrix])

	symvec_string = gen_symvec(prob_matrix)
##	print 'SYMVEC:\n', symvec_string
##	sys.exit(0)

	tmp_symvec_filename = tempfile.mktemp("symvec")
	tempfile_names.append(tmp_symvec_filename)
	tmp_symvec_handle = file(tmp_symvec_filename,'w')
	tmp_symvec_handle.write(symvec_string)
	tmp_symvec_handle.close()
	## os.system("./gen_symvec.py -s " + prob_filename + ' ' + tmp_symvec_filename)

	if options.title:
		logo_label = os.path.basename(prob_filename)
	else:
		logo_label = ' '
	# ************
	tmp_marks_filename = tempfile.mktemp("marks")
	tempfile_names.append(tmp_marks_filename)
	marks_handle=open(tmp_marks_filename, 'w')
	marks_handle.close()
	# ************
	# ************
	tmp_wave_filename = tempfile.mktemp("wave")
	tempfile_names.append(tmp_wave_filename)
	wave_handle=open(tmp_wave_filename, 'w')
	wave_handle.close()
	# ************
	# ************
	tmp_colors_filename = tempfile.mktemp("colors")
	tempfile_names.append(tmp_colors_filename)
	colors_handle=open(tmp_colors_filename, 'w')
	if options.color:
		# 4 color:
		colors_handle.write('A .8 0 0\nC 0 .8 0\nT 0 0 .8\nG .8 0 .8\n') 
	else:
		# 1 color:
		colors_handle.write('A .0 .0 .0\nC .0 .0 .0\nT .0 .0 .0\nG .0 .0 .0\n')
	colors_handle.close()
	# ************
	# ************
	tmp_makelogop_filename = tempfile.mktemp("makelogop")
	tempfile_names.append(tmp_makelogop_filename)
	makelogop_handle=open(tmp_makelogop_filename, 'w')
	# Note: setting barbits to -2 instead of 2 means that no I-beam error bars are produced
	## print_title = 0

	# changed "rotation" to 0, to make the logo upright

	if options.rotate:
		rotation_line = "90              rotation: angle to rotate the graph"
	else:
		rotation_line = "0              rotation: angle to rotate the graph"

	if options.clean:
		barends_line = "no bars            barends: if 'b' put bars before and after each line"
		numbers_line = "off numbers      numbering: if the first letter is 'n' then each stack is numbered"
	else:
		barends_line = "bars            barends: if 'b' put bars before and after each line"
		numbers_line = "numbers on      numbering: if the first letter is 'n' then each stack is numbered"
		
	makelogop_string = '1 ' + str(len(prob_matrix)) + \
	"""            FROM to TO range to make the logo over - STARTS at ZERO, NOT ONE!!!!!!!  Bases will be cut off if range begins too late, or ends too early
0               sequence coordinate before which to put a vertical bar
15 3            (xcorner, ycorner) lower left hand corner of the logo (in cm)
""" + rotation_line + """
1               charwidth: (real, > 0) the width of the logo characters, in cm
10 0.1          barheight, barwidth: (real, > 0) height of vertical bar, in cm
-2               barbits: (real) height of the vertical bar, in bits; < 0: no I-no beam
""" + barends_line + """
no show boxes   showingbox: if 's' show a dashed box around each character
no outline      outline: if 'o' make each character as an outline
capitals on     caps: if 'c', alphabetic characters are converted to caps
33              stacksperline: number of character stacks per line output
1               linesperpage: number of lines per page output
1.5             linemove: line separation relative to the barheight
""" + numbers_line + """
0.5             shrinking: factor by which to shrink characters inside dashed box
1               strings: the number of user defined strings to follow
0 11.0 0.7      X Y coordinates of string 1 (in cm) relative to logo, scale
""" + logo_label

	makelogop_handle.write(makelogop_string)
	makelogop_handle.close()
	# ************


	# symvec, makelogop, colors, marks, wave, output
	os.system(' '.join(('/home/josh/delila/makelogo', tmp_symvec_filename,
						tmp_makelogop_filename, tmp_colors_filename,
						tmp_marks_filename, tmp_wave_filename, output_filename)))
	return tempfile_names
		 

def delete_tempfiles(tempfile_list):
	for filename in tempfile_list:
		if os.path.exists(filename):
			os.remove(filename)

##------------------------------------------------------------------------
BASE_LABEL_STRING = 'ACGT'
NAME_TOKEN = '%NAME'
TAG_TOKEN = '%'
COMMENT_TOKEN = '#'
##------------------------------------------------------------------------
comment_re = re.compile('^\s*' + COMMENT_TOKEN + '(.+)')
base_header_re = re.compile('^[' + BASE_LABEL_STRING + '\s]+$', re.IGNORECASE)
empty_line_re = re.compile('^\s*$')
name_re = re.compile('^\s*' + NAME_TOKEN + '\s+(.+)')
tag_re = re.compile('^\s*' + TAG_TOKEN + '.+$')
##------------------------------------------------------------------------
TOLERANCE=1e-8
def parse_probfile(prob_filename):
	try:
		prob_handle = file(prob_filename, 'rU')	
	except IOError:
		prob_handle = file(prob_filename, 'r')	
		
	probability_matrix = []
	name = None
	
	## Parse the header section
	line_number = 0
	while 1:
		line = prob_handle.readline()
		line_number += 1
		if not line :
			raise FlatMatrixParseError \
				  (prob_handle.name, line, line_number,\
				   'Parse Error: Reached end of file prematurely:\nno Base Header was found')
		if comment_re.search (line) :
			continue
		elif name_re.search (line) :
			name = name_re.search(line).group(1).rstrip()
		elif tag_re.search (line) :
			continue
		elif empty_line_re.search (line):
			continue
		elif base_header_re.search (line):
			base_label_header = line.rstrip()
			base_label_array = base_label_header.upper().split()
			if not len (base_label_array) == len(BASE_LABEL_STRING) :
				raise FlatMatrixParseError \
					  (prob_handle.name, line, line_number,\
					   'Base Header Should contain each of the ' + \
					   str(len(BASE_LABEL_STRING)) + ' bases: ' + ','.join(BASE_LABEL_STRING) +'\n')
			for base in BASE_LABEL_STRING:
				if (base not in base_label_array) :
					raise FlatMatrixParseError \
						  (prob_handle.name, line, line_number, \
					 	  'Base Header is  missing the base:' + base + '\n')
			break
		else:
			raise FlatMatrixParseError \
				  (prob_handle.name, line, line_number, \
				   'Incorrect line format')
	if empty_line_re.search(name):
		name = 'FILENAME_' + os.path.basename(prob_handle.name)
	## Parse the probabilities section
	while 1:
		line = prob_handle.readline()
		line_number += 1
		if not line :
			if not probability_matrix:
				raise FlatMatrixParseError \
					  (prob_handle.name, line, line_number, \
					   'Parse Error: Reached end of file prematurely:\nno probabilities were found')
			else :
				break
		split_line = line.strip().split()
	
		if empty_line_re.search (line):
			continue
		elif not (len (split_line) == 4) :
			raise FlatMatrixParseError \
				  (prob_handle.name, line, line_number,\
				   'There should be four probabilities per line')
	
##		if gap_probabilities_re.search(line) :
##			for base in base_label_array:
##				probability_matrix.setdefault(base, []).append(0)
##		else:
		cur_prob_dict = {}
		probability_matrix.append(cur_prob_dict)
		total_prob = reduce(operator.add, map(float, split_line))
		if abs(1 - total_prob) > TOLERANCE:
			raise FlatMatrixParseError (prob_handle.name, line, line_number,
										'Probabilities DO NOT sum to 1+/-'
										+ str(TOLERANCE))
		for base_index in range(len(split_line)) :
			probability = split_line[base_index]
			try:
				float_probability = float(probability)
			except ValueError:
				raise FlatMatrixParseError \
					  (prob_handle.name, line, line_number,\
					   'Probabilities must be floating point numbers')
			if float_probability < 0 :
				raise FlatMatrixParseError \
					  (prob_handle.name, line, line_number,\
					   'Probabilities cannot be negative')
			elif float_probability == 0 :
				raise FlatMatrixParseError \
					  (prob_handle.name, line, line_number,\
					   'Probabilities cannot be zero')
			cur_base = base_label_array[base_index]
			cur_prob_dict[cur_base] = float_probability
			# probability_matrix.setdefault(cur_base, []).append(float_probability)

	prob_handle.close()
	return probability_matrix

def gen_symvec(prob_matrix):
	alpha_size = len(BASE_LABEL_STRING)
	num_sequences = 10000
	variance = 0

	symvec_string = '* alpro 1.70\n*seq1\n* DNA ALIGNMENT\n* position, samples, information, variance\n'
	symvec_string += str(alpha_size) + ' number of symbols\n'
	# ***********************************************************
	for prob_index in range(len(prob_matrix)) :
		position_num = prob_index + 1
		position_info = information_content_alt([prob_matrix[prob_index][base]
												 for base in
												 BASE_LABEL_STRING])
		pseudo_count_matrix = dict([(base,
									 int(round(prob_matrix[prob_index][base]*num_sequences)))
									for base in BASE_LABEL_STRING])
		print 'PSEUDO COUNT:', pseudo_count_matrix
		calc_num_sequences = reduce(operator.add,
									[pseudo_count_matrix[base]
									 for base in BASE_LABEL_STRING])
		symvec_string += ''.join((str(position_num).ljust(4),
								  str(calc_num_sequences).ljust(9),
								  str(round(position_info, 5)).ljust(9),
								  str(round(variance, 7)).ljust(9))) + '\n'
		symvec_string += '\n'.join([base.ljust(4)+
									str(pseudo_count_matrix[base])
									for base in BASE_LABEL_STRING]) + '\n'
	return symvec_string
##------------------------------------------------------------------------
class FlatMatrixParseError:
	"""Exception raised for formatting errors encountered during parsing files.

	Attributes:
		line -- line of file containing error
		message -- explanation of the error
	"""

##	def __init__(self, file_name, line, line_number, message):
##		# self.line, self.line_number, self.message = tuple
##		# super(ParseError, self).__init__()
##		self.file_name = file_name
##		self.line = line
##		self.line_number = line_number
##		self.message = message
##		self.print_message()

##	def print_message (self):
##		print '-' * 80
##		print self.message, '\nError encountered in following line (#' \
##			  + str(self.line_number) + ')', 'from input file:\n', \
##			  self.file_name, '\n', self.line
##		print '-' * 80

	def __init__(self, file_name, line, line_number, message):
		self.message = '\n'.join((message,
								  'Error encountered in following line (#'
								  +str(line_number) +')'+'from input file:',
								  file_name, line
								  ))
	def __str__ (self):
		return  '\n\n' + '*'*60 + '\n' + self.message + '\n' + '*'*60 + '\n'
##------------------------------------------------------------------------
	
if __name__ == '__main__':
	tempfile_names = main()
	delete_tempfiles(tempfile_names)
