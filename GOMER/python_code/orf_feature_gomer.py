#! /usr/bin/env python
from __future__ import division
#@+leo
#@+node:0::@file orf_feature_gomer.py
#@+body
#@@first
#@@first
#@@language python


"""
*Description:

A subclass of GenomeFeature for representing ORFs.

Only overrides a few methods of GenomeFeature.
"""

#@<<imports>>
#@+node:1::<<imports>>
#@+body
##import os
##os.environ['PYCHECKER'] = '--allglobals --stdlib'
##import pychecker.checker

##import check_python_version
##check_python_version.CheckVersion()

from genome_feature_gomer import GenomeFeature

#@-body
#@-node:1::<<imports>>


class OrfFeature(GenomeFeature):

	#@+others
	#@+node:2::IsOrf
	#@+body
	## def IsOrf (self):
	## 	return 1
	def _get_is_orf(self): return 1
	def _set_is_orf(self, value): raise AttributeError, "Can't set 'IsOrf' attribute"
	IsOrf = property(_get_is_orf, _set_is_orf)
	
	#@-body
	#@-node:2::IsOrf
	#@+node:3::Name
	#@+body
	#@+doc
	# 
	# # def _get_name(self): return raise NotImplemented
	# def _get_name(self): return self._reference_name
	# def _set_name(self, value): raise AttributeError, "Can't set 'Name' attribute"
	# Name = property(_get_name, _set_name)

	#@-doc
	#@-body
	#@-node:3::Name
	#@+node:4::Description
	#@+body
	def _get_description(self): return self._description
	def _set_description(self, value): raise AttributeError, "Can't set 'Description' attribute"
	Description = property(_get_description, _set_description)
	
	#@-body
	#@-node:4::Description
	#@-others


#@-body
#@-node:0::@file orf_feature_gomer.py
#@-leo
