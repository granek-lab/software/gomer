#! /usr/bin/env python

from __future__ import division

"""
What we really want is the arc of the convex hull between the point which has the greatest value along the x-axis and the point which has the greatest value along the y-axis. (remember to check for special case where these are the same)

1. Find max in each dimension
"""

import re
import sys
import random
import math
import glob
import os

comment_line_re = re.compile('^#')
empty_line_re = re.compile('^\s*$')
quotes_re = re.compile("""[\'\"]+""")

def Turn(p0, p1, p2):
	cross_product = CrossProduct(p0, p1, p2)

	if cross_product > 0:
		return 'left'
	elif cross_product < 0:
		return 'right'
	else:
		return 'collinear'

def CrossProduct(p0, p1, p2):
	return (p1.x - p0.x) * (p2.y - p0.y) - (p2.x - p0.x) * (p1.y - p0.y)


def polar_angle(p0, p1):
	# theta = arctan(y/x)
	return math.atan2(p1.y - p0.y, p1.x - p0.x)

def distance(p0,p1):
	return math.hypot(p1.x - p0.x, p1.y - p0.y)

def _remove_collinear_points(p0, sorted_point_list):
	points = list(sorted_point_list)
	## get rid of all collinear points, except the most distant
	for index in range(len(points)-1,0,-1):
		cur_p = points[index]
		last_p = points[index-1]
		if CrossProduct(p0, cur_p, last_p) == 0:
			cur_p_dist = distance(p0, cur_p)
			last_p_dist = distance(p0, last_p)
			if cur_p_dist < last_p_dist:
				points.pop(index)
			else:
				points.pop(index-1)
	return points

def polar_sort(p0, point_list):

##	for p in point_list:
##		angle = polar_angle(p0, p)
##		p.label = str(round(angle,2))
##		## print >>sys.stderr, p.x, p.y, p.label
	
	decorated = [(polar_angle(p0, p),p) for p in point_list]
	decorated.sort()
	point_list = [p for angle, p in decorated]
	return _remove_collinear_points(p0, point_list)

def sort_polar_sort(p0, point_list):
	def polar_cmpfunc(a,b):
		cross = CrossProduct(p0, a, b)
		if cross > 0:   return 1
		elif cross < 0:	return -1
		else:           return 0
	point_list = list(point_list)
	point_list.sort(polar_cmpfunc)
	point_list.reverse()
	return _remove_collinear_points(p0, point_list)
	## return point_list



def test_polar_sorting(p0, points):
	def polar_cmpfunc(a,b):
		cross = CrossProduct(p0, a, b)
		if cross > 0:   return 1
		elif cross < 0:	return -1
		else:           return 0
	for i in range(len(points)):
		for j in range(i+1, len(points)):
			print >>sys.stderr, '(', points[i].x, ',', points[i].y,')', '(', points[j].x, ',', points[j].y,')', polar_cmpfunc(points[i], points[j]), polar_cmpfunc(points[j], points[i])
		

def extract_p0(point_list):
	decorated = [(p.y, p.x, p) for p  in point_list]
	decorated.sort()
	undecorated = [p for y,x,p in decorated]
	p0 = undecorated.pop(0)
	return p0, undecorated

class Point(object):
	def __init__(self, x, y, label=''):
		self.x = float(x)
		self.y = float(y)
		self.label = label

def parse_datafile(handle):
	point_list = []
	for line in handle:
		if comment_line_re.match(line) or empty_line_re.match(line):
			continue
		data, label, empty = line.split('"')
		x, y = data.strip().split()
		point_list.append(Point(x,y,label))
	return point_list
	
def print_points(data_points, number=False, p0=None, relabel=None, handle=sys.stdout):	
	if p0:
		print >>handle, p0.x, p0.y, '"p0"' + '\n\n'
	for index in range(len(data_points)):
		p = data_points[index]
		if number: label = str(index)
		elif relabel: label = relabel
		else:	   label = p.label
		print >>handle, p.x, p.y, '"'+label+'"'
	
def gen_random_points(num, xmin=0, xmax=1, ymin=0, ymax=1):
	data_points = []
	for index in range(num):
		data_points.append(Point(random.uniform(xmin, xmax), random.uniform(ymin, ymax)))
	return data_points

def generate_collinear_points(p0, data_points):
	collinear_points = []
	for point in data_points:
		theta = polar_angle(p0, point)
		r = distance(p0, point)
		for scale in [0.6, 2, 3]:
			x = point.x + (r * scale * math.cos(theta))
			y = point.y + (r * scale * math.sin(theta))
			collinear_points.append(Point(x,y))
	return collinear_points

def GrahamScan(points):
	p0, points = extract_p0(points)
	points = polar_sort(p0, points)
	## points = sort_polar_sort(p0, points)
	## print >>sys.stderr, 'p0:', p0.x, p0.y, '\n'
	## print_points(points, handle=sys.stderr)
	## print >>sys.stderr, '\n'
	stack = [p0, points[0], points[1]]
	for i in range(2, len(points)):
		## print_points(stack, handle=sys.stderr)
		while Turn(stack[-2], stack[-1], points[i]) <> 'left':
			stack.pop(-1)
		stack.append(points[i])
	return stack

ConvexHull = GrahamScan


def BestScoresHull(points):
	decorated = [(p.y, p.x, p) for p  in points]
	decorated.sort()
	undecorated = [p for y,x,p in decorated]
	topmost = undecorated[-1]

	decorated = [(p.x, p.y, p) for p  in points]
	decorated.sort()
	undecorated = [p for x,y,p in decorated]
	rightmost = undecorated[-1]

	if rightmost.x == topmost.x and rightmost.y == topmost.y:
		return [rightmost]

	hull_points = ConvexHull(points)
	rightmost_index = hull_points.index(rightmost)
	topmost_index = hull_points.index(topmost)
	if topmost_index>rightmost_index:
		best_score_hull = hull_points[rightmost_index:topmost_index+1]
	else:
		best_score_hull = hull_points[topmost_index:rightmost_index+1]
	return best_score_hull

def main_test_sort():
	data_filename = sys.argv[1]
	data_filehandle = file(data_filename)
	data_points = parse_datafile(data_filehandle)
	p0, other_points = extract_p0(data_points)
	
	collinear_points = generate_collinear_points(p0, other_points)
	data_points = [p0] + data_points + collinear_points
##---------------------------------
	data_points = sort_polar_sort(p0, data_points)
##	data_points = polar_sort(p0, data_points)
##---------------------------------
	print_points(data_points, p0=p0, number=True)

def main():
	data_filename = sys.argv[1]
	data_filehandle = file(data_filename)
	data_points = parse_datafile(data_filehandle)
	p0, other_points = extract_p0(data_points)
	
	## test_polar_sorting(p0, other_points)
	collinear_points = generate_collinear_points(p0, other_points)
	data_points = [p0] + data_points + collinear_points
	
	hull_points = GrahamScan(data_points)

	p0 = hull_points[0]
	print_points(hull_points+[p0], p0=p0)
	print '\n\n'
	print_points(data_points)



def best_score_hull_main():
	file_name_globs = sys.argv[1:]
	## data_filename = sys.argv[1]
	filename_list = []
	for cur_glob in file_name_globs:
		filename_list.extend(glob.glob(cur_glob))
	basenames = [os.path.basename(name) for name in filename_list]
	data_points_hash = {}
	for data_filename in filename_list:
		data_filehandle = file(data_filename)
		data_points_hash[os.path.basename(data_filename)] = parse_datafile(data_filehandle)

	data_points = []
	for basename in data_points_hash:
		data_points.extend(data_points_hash[basename])

	p0, other_points = extract_p0(data_points)
	
	## test_polar_sorting(p0, other_points)
	## collinear_points = generate_collinear_points(p0, other_points)
	## data_points = [p0] + data_points + collinear_points
	
	best_score_hull_points = BestScoresHull(data_points)

	## print_points(best_score_hull_points, relabel='best')
	print '## Best Score Hull Segment:', ', '.join(basenames)
	print_points(best_score_hull_points)

	for basename in data_points_hash:
		print '\n## All Points:', basename
		print_points(data_points_hash[basename])
		print ''
	print ''
		
if __name__ == '__main__':
##	main()
##	main_test_sort()
	best_score_hull_main()
