#! /usr/bin/env python


import re
import sys
import os
from globals_gomer import OUTPUT_NAME_JUST # , OUTPUT_RANK_JUST
from gomer_load_functions import parse_parameter_string

empty_line_re = re.compile('^\s*$')
all_re = re.compile('^\s*All:\s*$')
worst_re = re.compile('^\s*Worst:\s*$')
best_re = re.compile('^\s*Best:\s*$')
regulated_re = re.compile('^\s*Regulated:\s*$')
excluded_re = re.compile('^\s*Excluded:\s*$')
unknown_re = re.compile('^\s*Unknown Features:\s*$')

## data_re = re.compile('^\s*(\d+)\s+(\S+)\s+([0-9\.\-eE]+)\s*$')
data_re = re.compile('^\s*\d+\t[^\t]+\t\s*[0-9\.\+\-eE]+\s*$')
unknown_feature_re = re.compile('^[^\t]+$')
separator_re = re.compile('^[-=]+$')

roc_auc_line_re = re.compile('^ROC AUC: ([0-9\.\+\-eE]+) ; U=([0-9\.\+\-eE]+) ; z=([0-9\.\+\-eE]+)\s*$')
mncp_line_re = re.compile('^MNCP   : ([0-9\.\+\-eE]+) ; estimated prob=([0-9\.\+\-eE]+)\s*$')


free_conc_ratio_label = 'Free Concentration Ratios'
free_conc_ratio_re = re.compile('^' + free_conc_ratio_label + ': \[([0-9\.\+\-eE]+)\]\s*$')
free_conc_list_label = 'Free Concentration List'
free_conc_list_re = re.compile('^' + free_conc_list_label + ': \[([0-9\.\+\-eE]+)\]\s*$')

reg_region_model_label = 'Regulatory Region Model'
reg_region_model_re = re.compile('^' + reg_region_model_label + ':\s*(.*)\s*$')
reg_region_param_label = 'Regulatory Region Parameters'
reg_region_param_re = re.compile('^' + reg_region_param_label + ':\s*(.*)\s*$')

concentration_data_list_label = 'Concentration Data List'



# concentration_header_re = re.compile('^' + '\s+'.join(('\[Free\]', '\[Free\] ratio', 'MaxKa', 'Type','Matrix')) +'$')
concentration_header_re = re.compile('^' + '\s+'.join(('Information','\[Free\]',
													   '\[Free\] ratio','MaxKa',
													   'MinKa','MaxKa/MinKa',
													   'MaxKa Seq','MinKa Seq',
													   'Pseudo Temp','Type',
													   'Matrix' )) +'$')
concentration_data_re = re.compile('^' + '(\S+)\s+'*10 + '(.+)$')
concentration_sep_re = re.compile('-=='*25)

class Feature(object):
	def __init__(self, rank, name, score):
		self.rank = rank
		self.name = name
		self.score = score

def ParseGomerOutput(filehandle):
	all_hash = {}
	regulated_hash = {}
	excluded_hash = {}
	best_hash = {}
	worst_hash = {}
	unknown_hash = {}
	param_hash = {}
	skipped_lines = []
	concentration_data_list = []

	current_hash = None

	line = ' '
	first_line = filehandle.readline()
	param_hash['command line'] = first_line.strip()
	while line:
	## for line in filehandle:
		# data_match = data_re.match(line)
		# unknown_feature_match = unknown_feature_re.match(line)
		if empty_line_re.match(line):
			pass
			## line = filehandle.readline()
			## continue
		## elif concentration_sep_re.search(line.rstrip()):
		## 	line = filehandle.readline()
		elif concentration_header_re.match(line.rstrip()):
			line = filehandle.readline()
			while concentration_data_re.match(line):
				concentration_data_match = concentration_data_re.match(line)
				information = concentration_data_match.group(1)
				free_conc = concentration_data_match.group(2)
				free_conc_ratio = concentration_data_match.group(3)
				max_ka = concentration_data_match.group(4)
				min_ka = concentration_data_match.group(5)
				max_ka__min_ka_ratio = concentration_data_match.group(6)
				max_ka_seq = concentration_data_match.group(7)
				min_ka_seq = concentration_data_match.group(8)
				pseudo_temp = concentration_data_match.group(9)
				matrix_type = concentration_data_match.group(10)
				matrix_name = concentration_data_match.group(11).strip()
				concentration_data_list.append((matrix_type, matrix_name, free_conc,
												free_conc_ratio, max_ka, min_ka,
												max_ka__min_ka_ratio, max_ka_seq,
												min_ka_seq, information, pseudo_temp))
				line = filehandle.readline()
			continue
		elif separator_re.match(line):
			current_hash = None
		elif data_re.match(line):
			rank, name, score = line.split('\t')
			rank = int(rank.strip())
			name = name.strip()
			score = float(score.strip())
## 			rank = int(data_match.group(1))
## 			name = data_match.group(2).strip()
## 			try:
## 				score = float(data_match.group(3))
## 			except ValueError:
## 				print >>sys.stderr, line
## 				raise
			current_hash[name.upper()] = Feature(rank, name, score)
		elif all_re.match(line):
			current_hash = all_hash
			## print 'ALL'
		elif worst_re.match(line):
			current_hash = worst_hash
		elif regulated_re.match(line):
			current_hash = regulated_hash
			## print 'REGULATED'
		elif excluded_re.match(line):
			current_hash = excluded_hash
			## print 'EXCLUDED'
		elif best_re.match(line):
			current_hash = best_hash
		elif unknown_re.match(line):
			current_hash = unknown_hash
			## print 'UNKNOWN'
		elif roc_auc_line_re.match(line):
			rocauc_match = roc_auc_line_re.match(line)
			param_hash['ROC AUC'] = float(rocauc_match.group(1))
			param_hash['U score'] = float(rocauc_match.group(2))
			param_hash['Z score'] = float(rocauc_match.group(3))
		elif mncp_line_re.match(line):
			mncp_match = mncp_line_re.match(line)
			param_hash['MNCP'] = float(mncp_match.group(1))
			param_hash['estimated prob'] = float(mncp_match.group(2))
		elif free_conc_ratio_re.match(line):
			param_hash[free_conc_ratio_label] = float(free_conc_ratio_re.match(line).group(1))
		elif free_conc_list_re.match(line):
			param_hash[free_conc_list_label] = float(free_conc_list_re.match(line).group(1))

		elif reg_region_model_re.match(line):
			param_hash[reg_region_model_label] = reg_region_model_re.match(line).group(1)
		elif reg_region_param_re.match(line):
			param_hash[reg_region_param_label] = parse_parameter_string(reg_region_param_re.match(line).group(1),
																		parameter_separator=',',
																		assignment_separator=':',)
		elif unknown_feature_re.match(line) and current_hash == unknown_hash:
			name = line.strip()
			current_hash[name.upper()] = name
		else:
			skipped_lines.append(line.rstrip())
		line = filehandle.readline()
	param_hash[concentration_data_list_label] = concentration_data_list		
	return all_hash, regulated_hash, excluded_hash, unknown_hash, skipped_lines, param_hash


def print_set(hash_of_features):
	list_of_tuples = [(feature.rank, feature.name, feature.score) for feature in
					  [hash_of_features[key] for key in hash_of_features]]
	list_of_tuples.sort()
	list_of_tuples.reverse()
	rank, name, score = list_of_tuples[0]
	OUTPUT_RANK_JUST = len(str(rank))

	for rank, name, score in list_of_tuples:
		print '\t'.join((str(rank).rjust(OUTPUT_RANK_JUST), name.ljust(OUTPUT_NAME_JUST), repr(score)))

	

if __name__ == '__main__':
	import sys
	if len(sys.argv) == 2:
		gomer_output_filename = sys.argv[1]
		filehandle = file(gomer_output_filename)
	else:
		print >>sys.stderr, 'usage:', os.path.basename(sys.argv[0]), 'GOMER_OUTPUT_FILE'
		sys.exit(1)
		
	all_hash, regulated_hash, excluded_hash, unknown_hash, skipped_lines, param_hash = ParseGomerOutput(filehandle)

	print 'skipped_lines:\n', '\n'.join(['SKIPPED ' + line for line in skipped_lines])
 	print '*' * 60
	print param_hash['command line']
	print '-' * 60

	info_just = 29
	print (reg_region_model_label + ':').ljust(info_just), param_hash[reg_region_model_label]
	print (reg_region_param_label + ':').ljust(info_just), ','.join([str(key) + ':' + str(param_hash[reg_region_param_label][key])
																	 for key in param_hash[reg_region_param_label]])


	conc_just = 22
 	print '-' * 60
	print ' '.join([x.ljust(conc_just) for x in ['Information', '[Free]', '[Free] ratio',
												 'MaxKa', 'MinKa', 'MaxKa/MinKa',
												 'MaxKa Seq', 'MinKa Seq',
												 'Pseudo Temp', 'Type', 'Matrix']])
	for matrix_type, matrix_name, free_conc, free_conc_ratio, max_ka, min_ka, \
		max_ka__min_ka_ratio, max_ka_sequence, min_ka_sequence, information,  \
		pseudo_temp in param_hash[concentration_data_list_label]:
		print ' '.join([str(x).ljust(conc_just) for x in [information, free_conc, free_conc_ratio,
														  max_ka, min_ka, max_ka__min_ka_ratio,
														  max_ka_sequence, min_ka_sequence,
														  pseudo_temp, matrix_type, matrix_name]])
	

	if ('ROC AUC' in param_hash) and ('U score' in param_hash) and ('Z score' in param_hash):
		print '-' * 60
		print ' '.join(('ROC AUC:',str(param_hash['ROC AUC']),
						'; U='+str(param_hash['U score']),
						'; z='+str(param_hash['Z score'])))
		print ' '.join(('MNCP   :',str(param_hash['MNCP']),
						'; estimated prob='+str(param_hash['estimated prob'])))
		print '-' * 60

	if regulated_hash:
		print 'Regulated:'
		print_set(regulated_hash)
 	print '-' * 60
	if unknown_hash:
 		print 'Unknown Features:\n', '\n'.join([unknown_hash[key] for key in unknown_hash])
 	print '-' * 60
	print 'All:'
	print_set(all_hash)
