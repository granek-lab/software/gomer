#! /usr/bin/env python
from __future__ import division
#@+leo
#@+node:0::@file general_bisect.py
#@+body
#@@first
#@@first
#@@language python

"""
*Description:

This is a generalization of the bisect module that is part of the standard python
distribution.
This module allows for the user to supply a comparision function, just as sort
allows for a comparision function to be supplied
"""
##import os
##os.environ['PYCHECKER'] = '--allglobals --stdlib'
##import pychecker.checker


#@<< general_bisect declarations >>
#@+node:1::<< general_bisect declarations >>
#@+body
"""Bisection algorithms."""


#@-body
#@-node:1::<< general_bisect declarations >>


#@+others
#@+node:2::insort_right
#@+body
def insort_right(a, x, compare_func=cmp, lo=0, hi=None):
	"""Insert item x in list a, and keep it sorted assuming a is sorted.

	If x is already in a, insert it to the right of the rightmost x.

	Optional args lo (default 0) and hi (default len(a)) bound the
	slice of a to be searched.

	a: list to be inserted into
	x: element to be inserted
	compare_func: function to be used for comparison
	"""

	if hi is None:
		hi = len(a)
		
	while lo < hi:
	## while lo < hi:
		mid = (lo+hi)//2
		if compare_func(x, a[mid]) < 0 : hi = mid
		# if x < a[mid]: hi = mid
		else: lo = mid+1
	a.insert(lo, x)

insort = insort_right	# backward compatibility


#@-body
#@-node:2::insort_right
#@+node:3::bisect_right
#@+body
def bisect_right(a, x, compare_func=cmp, lo=0, hi=None):
	"""Return the index where to insert item x in list a, assuming a is sorted.

	The return value i is such that all e in a[:i] have e <= x, and all e in
	a[i:] have e > x.  So if x already appears in the list, i points just
	beyond the rightmost x already there.

	Optional args lo (default 0) and hi (default len(a)) bound the
	slice of a to be searched.
	"""

	if hi is None:
		hi = len(a)
	while lo < hi:
	## while lo < hi:
		mid = (lo+hi)//2
		if compare_func(x, a[mid]) < 0 : hi = mid
		# if x < a[mid]: hi = mid
		else: lo = mid+1
	return lo

#@-body
#@-node:3::bisect_right
#@+node:4::insort_left
#@+body
bisect = bisect_right	# backward compatibility

def insort_left(a, x, compare_func=cmp, lo=0, hi=None):
	"""Insert item x in list a, and keep it sorted assuming a is sorted.

	If x is already in a, insert it to the left of the leftmost x.

	Optional args lo (default 0) and hi (default len(a)) bound the
	slice of a to be searched.
	"""

	if hi is None:
		hi = len(a)
	while lo < hi:
	## while lo < hi:
		mid = (lo+hi)//2
		if compare_func(a[mid], x) < 0 : lo = mid+1
		# if a[mid] < x: lo = mid+1
		else: hi = mid
	a.insert(lo, x)

#@-body
#@-node:4::insort_left
#@+node:5::bisect_left
#@+body
def bisect_left(a, x, compare_func=cmp, lo=0, hi=None):
	"""Return the index where to insert item x in list a, assuming a is sorted.

	The return value i is such that all e in a[:i] have e < x, and all e in
	a[i:] have e >= x.	So if x already appears in the list, i points just
	before the leftmost x already there.

	Optional args lo (default 0) and hi (default len(a)) bound the
	slice of a to be searched.
	"""

	if hi is None:
		hi = len(a)
	while lo < hi:
	## while lo < hi:
		mid = (lo+hi)//2
		if compare_func(a[mid], x) < 0 : lo = mid+1
		# if a[mid] < x: lo = mid+1
		else: hi = mid
	return lo

#@-body
#@-node:5::bisect_left
#@+node:6::test
#@+body
def test():
	"""
	be sure the general_bisect and bisect standard librarys functions
	produce the same results (when using cmp as the comparison function
	on lists of numbers)
	"""
	
	import bisect
	start = 0
	stop=40
	step=4
	value=13
	for value in range (20):
		a = range(start, stop, step)
		b = range(start, stop, step)
		c = range(start, stop, step)
		d = range(start, stop, step)
		e = range(start, stop, step)
		f = range(start, stop, step)
		if bisect.insort_left(a, value) == insort_left(b, value): print 'Same',
		else : print 'DIFFERENT',
		print '\t\t', a, b

		if bisect.insort_right(c, value) == insort_right(d, value): print 'Same',
		else : print 'DIFFERENT',
		print '\t\t', c, d

		if bisect.bisect_right(e, value) == bisect_right(f, value): print 'Same',
		else : print 'DIFFERENT',
		print '\t\t',bisect_right(f, value)

		if bisect.bisect_left(e, value) == bisect_left(f, value): print 'Same',
		else : print 'DIFFERENT',
		print '\t\t',bisect_left(f, value)

	

#@-body
#@-node:6::test
#@-others


#@-body
#@-node:0::@file general_bisect.py
#@-leo
