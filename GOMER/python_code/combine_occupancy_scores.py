import re
import sys
import glob
import os
from parse_gomer_output import ParseGomerOutput

empty_re = re.compile('^\s*$')
gomer_output_filename_re = re.compile('^(\w+)\_\d+\.trainingset\.seq\.\d+\.bp\.reformat\.pwm\.out$')

def usage():
	print >>sys.stderr, '\nusage:', os.path.basename(sys.argv[0]), 'GOMER_OUTPUT_DIRECTORY IPed_FEATURES_DIRECTORY\n\n'


output_handle = sys.stdout
def main():
	if len(sys.argv) ==3:
		gomer_output_directory = sys.argv[1]
		IPed_features_directory = sys.argv[2]
		gomer_output_filename_list = glob.glob(os.path.join(gomer_output_directory, '*'))
	else:
		usage()
		sys.exit(2)

##	pull_down_counts_hash = {}
##	pull_down_counts_handle = file(pull_down_counts_filename)
##	for line in pull_down_counts_handle:
##		name, count = line.strip().split()
##		pull_down_counts_hash[name.upper()] = count

	## list_of_feature_hashes = []
	feature_combined_score_hash = {}
	feature_IP_counts = {}
	file_count = 0
	IPed_feature_count = 0
	for filename in gomer_output_filename_list:
		if gomer_output_filename_re.match(os.path.basename(filename)):
			tf_name = gomer_output_filename_re.match(os.path.basename(filename)).group(1)
			IPed_features_filename = os.path.join(IPed_features_directory, tf_name + 'fdr0.1.list')
			print '#',  IPed_features_filename
			cur_IPed_features = parse_IPed_features_file(file(IPed_features_filename))
			IPed_feature_count += len(cur_IPed_features)
			# cur_IPed_features_dict = dict(zip(cur_IPed_features, [None]*len(cur_IPed_features)))
		else:
			print >>sys.stderr, "File name doesn't match pattern (TFNAME_*.trainingset.seq.*.bp.reformat.pwm.out):", os.path.basename(filename)
			sys.exit(1)
		handle = file(filename)
		(all_feature_hash,
		 regulated_hash,
		 unknown_feature_hash,
		 gomer_output_skipped_lines,
		 param_hash) = ParseGomerOutput(handle)
		file_count += 1

		for cur_feature_name in all_feature_hash:
			cur_score =  all_feature_hash[cur_feature_name].score
			feature_combined_score_hash[cur_feature_name] = feature_combined_score_hash.get(cur_feature_name, 1) * (1 - cur_score)
	
		for feature_name in cur_IPed_features:
			feature_IP_counts[feature_name.upper()] = 1 + feature_IP_counts.get(feature_name.upper(), 0)
				
	count = 0
	for cur_feature_name in feature_combined_score_hash:
		print '#', cur_feature_name
		print (1 - feature_combined_score_hash[cur_feature_name.upper()]), feature_IP_counts.get(cur_feature_name.upper(), 0)
		count += feature_IP_counts.get(cur_feature_name.upper(), 0)

	print '# Num Files:', file_count
	print '# IPed_feature_count:', IPed_feature_count
	print '# count:', count
		
def parse_IPed_features_file(filehandle):
	IPed_features_list = []
	for line in filehandle:
		if not empty_re.match(line):
			IPed_features_list.append(line.strip())
	return IPed_features_list
		

if __name__ == "__main__":
	main()
