#! /usr/bin/env python
from __future__ import division
#@+leo
#@+node:0::@file ROC_AUC.py
#@+body
#@@first
#@@first
#@@language python
#@<< ROC_AUC declarations >>
#@+node:1::<< ROC_AUC declarations >>
#@+body
##import os
##os.environ['PYCHECKER'] = '--allglobals --stdlib'
##import pychecker.checker

##import check_python_version
##check_python_version.CheckVersion()


import sys

# MINIMUM_PYTHON_VERSION_HEX = 0x20101f0
##if sys.hexversion < MINIMUM_PYTHON_VERSION_HEX:
##	  print 'ERROR: This script is known to work with Python version 2.1.1 and higher!', '\n\nThe version on this system is :\n', sys.version, '\n\nVersion 2.1.1 might be available as "python2", you can try changing the first line of this file from:', '\n#! /usr/bin/env python\n\nto:', '\n#! /usr/bin/env python2'
##	  sys.exit(1)


##DEBUG = 0
##if DEBUG:
##	  debug = sys.stderr
##else:
##	  debug =open('/dev/null', 'w')



#@-body
#@-node:1::<< ROC_AUC declarations >>


#@+others
#@+node:2::ROC_auc
#@+body
def ROC_auc (a_values, b_values, higher_is_better=0):
	# print a_values, b_values
	if higher_is_better:
		## def better_than (x, y): return (x < y)
		better_than = lambda x,y: x < y
	else:
		# def better_than (x, y): return (x > y)
		better_than = lambda x,y: x > y
	a_values.sort()
	b_values.sort()
	if not (higher_is_better):
		a_values.reverse()
		b_values.reverse()
	# print a_values, b_values
	area_array = []
	for cur_value in a_values:
		num_better_than = 0
		num_equal_to = 0
		for b_value in b_values:
			if better_than(b_value, cur_value) :
				num_better_than += 1
			elif (b_value == cur_value) :
				num_equal_to += 1
				## print b_value, ':', cur_value
			else :
				break
		# print cur_value, 'num_better_than:', num_better_than, 'num_equal_to:', num_equal_to
		## print num_better_than, num_equal_to 
		area_array.append ((num_better_than + (float(num_equal_to)/float(2))))
		# print 'area_array:', area_array
	# print 'area_array:', area_array
	area = reduce (lambda x,y: x+y, area_array)
	# print 'area:', area
	return area

#@-body
#@-node:2::ROC_auc
#@+node:3::normalize_auc
#@+body
def normalize_auc (area, a_values, b_values):
	return float(area)/(float(len (a_values)) * float(len (b_values)))

#@-body
#@-node:3::normalize_auc
#@+node:4::NormalizedROCAUC
#@+body
def NormalizedROCAUC (a_values, b_values, higher_is_better=0):
	area = ROC_auc(a_values, b_values, higher_is_better)
	normalized_area = normalize_auc(area, a_values, b_values)
	return normalized_area

#@-body
#@-node:4::NormalizedROCAUC
#@+node:5::usage
#@+body
if __name__ == '__main__':
	"""

	"""
	import getopt
	FALSE = 0
	TRUE = 1
	EXPECTED_ARGUMENTS = 2
	def usage() :
		print (len(sys.argv) - 1), """arguments given\n\n\
		Must be given""", EXPECTED_ARGUMENTS, """arguments:\n\
		1. quoted list of values of
		2. quoted list of valies of
	
		OPTIONS:
		
		-h or --higher: higher values are better (default behavior is for lower values to be better)
	"""


	# print 'I am main'
	#**************************************************88

	

	try:
		opts, args = getopt.getopt(sys.argv[1:], "h", ["higher"])
	except getopt.GetoptError:
		# print help information and exit:
		usage()
		sys.exit(2)
	# output = None
	higher_is_better = FALSE
	for o, a in opts:
		if o in ("-h", "--higher"):
			higher_is_better = TRUE
			print "higher_is_better"

	args.insert(0, sys.argv[0])		   
	# sys.stderr.write (str(args) + '\n')
	# sys.stderr.write (str(len(sys.argv)) + '\n')


	# *****************************************************
	if len(args) == (EXPECTED_ARGUMENTS + 1):
		a_values_string = args[1]
		b_values_string = args[2]
	else :
		usage()
		sys.exit(0)

	a_values = map (float, (a_values_string.split()))
	b_values = map (float, (b_values_string.split()))
	area = ROC_auc(a_values, b_values, higher_is_better)
	print 'ROC_AUC:', area, '\nnormalized ROC_auc:', normalize_auc(area, a_values, b_values)

#@-body
#@-node:5::usage
#@-others





"""
An alternative way to speed up sorts is to construct a list of tuples whose first element is a sort key that will sort properly using the default comparison, and whose second element is the original list element.

Suppose, for example, you have a list of tuples that you want to sort by the n-th field of each tuple. The following function will do that.

def sortby(list, n):
	nlist = map(lambda x, n=n: (x[n], x), list)
	nlist.sort()
	return map(lambda (key, x): x, nlist)

Here's an example use:

>>> list = [(1, 2, 'def'), (2, -4, 'ghi'), (3, 6, 'abc')]
>>> list.sort()
>>> list 
[(1, 2, 'def'), (2, -4, 'ghi'), (3, 6, 'abc')]
>>> sortby(list, 2)
[(3, 6, 'abc'), (1, 2, 'def'), (2, -4, 'ghi')]
>>> sortby(list, 1) 
[(2, -4, 'ghi'), (1, 2, 'def'), (3, 6, 'abc')]

"""

#@-body
#@-node:0::@file ROC_AUC.py
#@-leo
