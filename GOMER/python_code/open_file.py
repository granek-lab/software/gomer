#! /usr/bin/env python
from __future__ import division
#@+leo
#@+node:0::@file open_file.py
#@+body
#@@first
#@@first
#@@language python

import sys
import os

"""
*Description:

Doesn't do anything.  I believe I started writing this to deal with opening files
and catching errors, but I might want to catch file opening errors other places.

Then again, I might want to use something like this to catch errors in opening
files, and be able to report a meaningful error - 

	OpenFile(filename, error_message) - if there is an error:
		"Failed to open filename, can you supply another file name"
"""
##import os
##os.environ['PYCHECKER'] = '--allglobals --stdlib'
##import pychecker.checker

##import check_python_version
##check_python_version.CheckVersion()

#def OpenFile()
#@-body
#@-node:0::@file open_file.py
#@-leo



def OpenFile(orig_filename,description,mode='r'):
	filename = TestFile(orig_filename,description,mode)
	handle = file(filename, mode)
	return handle

def ExpandPath(orig_path):
	path = orig_path
	## path = os.path.abspath(path)
	path = os.path.expanduser(path)
	path = os.path.expandvars(path)
	path = os.path.normpath(path)
	path = os.path.normcase(path)
	return path

def TestFile(orig_filename,description,mode='r'):
	filename = ExpandPath(orig_filename)
	if not os.path.exists(filename):
		if ('w' in mode) or ('a' in mode):
			file_path = os.path.dirname(filename)
			if file_path == '':
				file_path = os.path.abspath(file_path)
			if not os.path.isdir(file_path):
				raise OpenFileError("Cannot open file for writing, directory ("
									+os.path.dirname(filename)
									+") does not exist",
									description, orig_filename)
		else:
			raise OpenFileError("File does not exist", description, orig_filename)
	return filename


class OpenFileError(Exception):
	def __init__ (self, message, description, filename):
		# super(OpenFileError, self).__init__()
		self.message = ' '.join((description+' -', message+':\n', filename))
		
	def __str__ (self):
		return  '\n\n' + '*'*60 + '\n' + self.message + '\n' + '*'*60 + '\n'




if __name__ == '__main__':

	handle = None
	if len(sys.argv) == 2:
		filename = sys.argv[1]
		handle = OpenFile(filename, 'Test OpenFile')
	elif len(sys.argv) == 3:
		filename = sys.argv[1]
		mode = sys.argv[2]
		handle = OpenFile(filename, 'Test OpenFile', mode)
	else:
		print >>sys.stderr, 'usage:', os.path.basename(sys.argv[0]), 'FILE [MODE]'
		print >>sys.stderr, '\n\tMODE is as defined for the python builtin function "file"\n'

	if handle:
		print >>sys.stderr, '\nFILE OPENED SUCCESSFULLY'
