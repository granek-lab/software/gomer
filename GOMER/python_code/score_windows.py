#### #! /usr/bin/env python
#@+leo
#@+node:0::@file score_windows.py
#@+body
#@@first
#@@language python


#@<<imports>>
#@+node:1::<<imports>>
#@+body
from __future__ import division

import os
import sys
import Numeric
import fpformat

from cache_table_gomer import load_cache_table, append_cache_table
from cache_file_printer_gomer import CacheFilePrinter
from cache_file_handler_gomer import CacheFileHandler
from cache_window_list_gomer import CacheWindowList
from hit_list_gomer import HitList
from integer_string_compare import sort_numeric_string
from apply_genome_weights import ApplyGenomeWeights

#@-body
#@-node:1::<<imports>>



#@<<def load_cache_file>>
#@+node:2::<<def load_cache_file>>
#@+body
#@+doc
# 
# def load_cache_file(cache_directory, cache_file_name):
# 	full_cache_file_name = os.path.join(cache_directory, cache_file_name)
# 	cache_file_handler = CacheFileHandler(full_cache_file_name)
# 	return cache_file_handler

#@-doc
#@@code

def load_cache_file(probability_matrix, chromosome_hash, filter_cutoff_ratio,
					frequency_hash,
					cache_directory,cache_table_filename,
					fast_cache_directory,
					compression, delete_cache):
	"""
	load_cache_table returns a hash of cache table entries, keyed by the MD5sum of the probability matrix used to build the cache.  If the filter_cutoff_ratio is 0, then only unfiltered caches (those made with a filter ratio of 0) can be used.  Otherwise, all caches generated with a filter of zero, or greater than or equal to filter_cutoff_ratio.  These are then checked for matches with the parameters of the current run, in an order determined by the distance of the filter ratio used to generate the cache from the user supplied filter_cutoff_ratio.  The order of checking caches means that a cache won't be refiltered if there is a cache match with the same filtering threshhold. If the cache matches, a handler for that cache is returned.  If a cache matches except for filtering threshold, the cache is refiltered, and a handler for the refiltered cache is returned.  If a matching cache can't be found, None is returned.
	"""
##	sorted_chromosome_label_list = chromosome_hash.keys()
##	sorted_chromosome_label_list.sort(IntegerStringCmp)
	sorted_chromosome_label_list = sort_numeric_string(chromosome_hash.keys())

	cache_table_hash = load_cache_table(cache_directory, cache_table_filename)
	cache_matches = False
	if probability_matrix.MD5 in cache_table_hash:
		if __debug__:
			print >>sys.stderr, 'Found at least one cache file for probability matrix'
##------------------------------------------------------------------------
		possible_cache_matches = cache_table_hash[probability_matrix.MD5]
		if __debug__:
			print >>sys.stderr, 'Before Regiggering:'
			print >>sys.stderr, ','.join([`match.FilterCutoffRatio` for match in possible_cache_matches])
		if filter_cutoff_ratio == 0:
			possible_cache_matches = filter(lambda x:x.FilterCutoffRatio == 0,
											possible_cache_matches)
		else:
			possible_cache_matches = filter(
				(lambda x:x.FilterCutoffRatio == 0 or
				 x.FilterCutoffRatio >= filter_cutoff_ratio), possible_cache_matches)
		decorated = [(abs(x.FilterCutoffRatio-filter_cutoff_ratio), x.CacheFileName, x) for x in possible_cache_matches]
		decorated.sort()
		possible_cache_matches = [matches for ratio, name, matches in decorated]
		##if possible_cache_matches[0].FilterCutoffRatio == 0:
		##	zero_filter = possible_cache_matches.pop(0)
		##	possible_cache_matches.append(zero_filter)
		if __debug__:
			print >>sys.stderr, 'After Regiggering:'
			print >>sys.stderr, ','.join([`match.FilterCutoffRatio` for match in possible_cache_matches])
##------------------------------------------------------------------------
		for table_entry in possible_cache_matches:
			if __debug__:
				print >>sys.stderr, 'Checking cache file:', table_entry.CacheFileName
			##cache_file_handler = load_cache_file(cache_directory, cache_file_name)
			cache_file_handler = CacheFileHandler(os.path.join(cache_directory,
															   table_entry.CacheFileName),
												  fast_cache_directory)
			cache_matches = True
			if __debug__:
				print >>sys.stderr, 'Checking to be sure filter_cutoff_ratios are the same'
			## Higher filter_cutoff_ratios mean less stringent filtering
			## i.e. filtering at 1 cuts off more sites than filtering at 1000
			
			## IF filter_cutoff_ratio is changed to be the reciprocal, the folloring line needs to be changed to:
			## if filter_cutoff_ratio < cache_file_handler.GetFilterCutoff():
			if filter_cutoff_ratio > cache_file_handler.GetFilterCutoff() and \
				   cache_file_handler.GetFilterCutoff() <> 0.0:
				if __debug__:
					print >>sys.stderr, 'WRONG FILTER CUTOFF RATIO: current ratio=', filter_cutoff_ratio, 'cache ratio=', cache_file_handler.GetFilterCutoff()
##				print 'type(filter_cutoff_ratio)', type(filter_cutoff_ratio)
##				print 'type(cache_file_handler.GetFilterCutoff())', type(cache_file_handler.GetFilterCutoff())
##				print 'repr(filter_cutoff_ratio)', repr(filter_cutoff_ratio)
##				print 'repr(cache_file_handler.GetFilterCutoff())', repr(cache_file_handler.GetFilterCutoff())
				cache_matches = False
				continue
			if __debug__:
				print >>sys.stderr, 'Checking to be sure sequences are the same'
			for base in frequency_hash:
				base = base.upper()
				if base not in cache_file_handler.Bases:
					if __debug__:
						print >>sys.stderr, 'Base Frequency for:', base , ' not in cache file, need to generate a new cache file'
					cache_matches = False
					break
				if frequency_hash[base] <> cache_file_handler.GetBaseFrequency(base):
					if __debug__:
						print >>sys.stderr,  'Base Frequency for :', base, '(', frequency_hash[base], ')'
						print >>sys.stderr,  'does not match value in cache (', cache_file_handler.GetBaseFrequency(base), ')'
					cache_matches = False
					break

			for chromosome_label in sorted_chromosome_label_list:
				if chromosome_label not in cache_file_handler.Chromosomes:
					# print 'cache_file_handler.Chromosomes', cache_file_handler.Chromosomes
					if __debug__:
						print >>sys.stderr,  'Chromosome:', chromosome_label, ' not in cache file, need to generate a new cache file'
					cache_matches = False
					break
				if chromosome_hash[chromosome_label].MD5 <> cache_file_handler.GetChromMD5(chromosome_label):
					if __debug__:
						print >>sys.stderr,  'Chromosome Sequence for :', chromosome_label, 
						print >>sys.stderr,  'does not match MD5, need to generate a new cache file'
						print >>sys.stderr,  'from sequence:', chromosome_hash[chromosome_label].MD5, 
						print >>sys.stderr,  'from cache:', cache_file_handler.GetChromMD5(chromosome_label)
					cache_matches = False
					break
			if cache_matches:
				## IF filter_cutoff_ratio is changed to be the reciprocal, the folloring line needs to be changed to:
				## if filter_cutoff_ratio > cache_file_handler.GetFilterCutoff():

				if filter_cutoff_ratio < cache_file_handler.GetFilterCutoff() or \
					   ((cache_file_handler.GetFilterCutoff() == 0) and
						filter_cutoff_ratio <> cache_file_handler.GetFilterCutoff()):
					## if filter_cutoff_ratio == 0 and cache_file_handler.GetFilterCutoff() == 0
					## we are all set, and we don't want the "cache_file_handler.GetFilterCutoff() == 0"
					## to trigger this code block
					'NEED TO REFILTER AT LOWER STRINGGENCY'
					(refiltered_table_entry,
					 cache_file_handler) = refilter_cache(cache_file_handler,
														  probability_matrix,
														  chromosome_hash,
														  filter_cutoff_ratio,
														  cache_directory,
														  cache_table_filename,
														  fast_cache_directory,
														  compression,
														  delete_cache)
					table_entry = refiltered_table_entry
##					cache_file_handler = CacheFileHandler(os.path.join(cache_directory,
##																	   refiltered_table_entry.CacheFileName))
					cache_table_hash = load_cache_table(cache_directory, cache_table_filename)


				# cache_file_md5_from_table = cache_table_hash[probability_matrix.MD5][cache_file_name]
				if __debug__:
					print >>sys.stderr,  '+' * 40
					print >>sys.stderr,  'cache file md5 from table:', table_entry.CacheFileMD5
					print >>sys.stderr,  'cache file md5 calculated:', cache_file_handler.MD5
					print >>sys.stderr,  '+' * 40
				if table_entry.CacheFileMD5 == cache_file_handler.MD5:
					if __debug__:
						print >>sys.stderr,  'Cache File MATCHES!!'
					break
				else:
					raise StandardError, 'The md5 value of the cache file does not match the recorded value in the cache_table_hash, it is likely to be corrupt and should be deleted'



	if cache_matches:
		if __debug__:
			print >>sys.stderr,  'Sequences Match, Perfect Cache Match, Loading windows from cache'
		return cache_file_handler
	else:
		return None



#@-body
#@-node:2::<<def load_cache_file>>


#@<<def cache_score_all_windows>>
#@+node:3::<<def cache_score_all_windows>>
#@+body


def _generate_new_cache(probability_matrix, chromosome_hash,
								 filter_cutoff_ratio,
								 cache_directory,
								 fast_cache_directory,
								 frequency_hash,
								 compression=None,
								 delete_cache=False):
	#@<<generate new cache>>
	#@+node:1::<<generate new cache>>
	#@+body
	if __debug__:
		print >>sys.stderr, 'NEED TO SCORE WINDOWS, and generate cache, update cache table, and load new cache as above'

	cache_file_printer = CacheFilePrinter(cache_directory, fast_cache_directory, compression, delete_cache)
	cache_file_printer.AddProbabilityMatrix(probability_matrix)
	cache_file_printer.AddFilterCutoff(filter_cutoff_ratio)
	cache_file_printer.AddBaseFrequencies(frequency_hash)

	sorted_chromosome_label_list = sort_numeric_string(chromosome_hash.keys())
	for chromosome_label in sorted_chromosome_label_list:
		chromosome = chromosome_hash[chromosome_label]
		cache_file_printer.AddChromosome(chromosome.Name, chromosome.Sequence.Sequence)

	# print >>sys.stderr, ''
	for chromosome_label in sorted_chromosome_label_list:
		chromosome = chromosome_hash[chromosome_label]
		##sys.stderr.write ('\r' + ' '*80 + '\r')
		##sys.stderr.write ('Scoring Windows of Chromosome:' + chromosome.Name)# + '\n')
		if __debug__:
			print >>sys.stderr, 'Scoring Windows of Chromosome:', chromosome.Name
		hitlist = probability_matrix.FilterScoreSequence(chromosome.Sequence.Sequence, filter_cutoff_ratio)
		# print 'Adding Windows to cache from Chromosome:', chromosome.Name			
		cache_file_printer.AddHitlist(hitlist)
		##test_array_index(hitlist.Forward, hitlist.ReverseComplement, 'Scored', chromosome_label)
		del hitlist
		## memcheck('Chromosome ' + chromosome.Name + ' scored')
	## print >>sys.stderr, ''
	new_cache_table_entry, cache_file_handler = cache_file_printer.Finish()
	return new_cache_table_entry, cache_file_handler

def _generate_weighted_cache(genome_weight_dict, default_genome_weight,
							 unweighted_cache_file_handler,
							 probability_matrix, chromosome_hash,
							 filter_cutoff_ratio, frequency_hash,
							 cache_directory,
							 fast_cache_directory):
	##============================================
	## Initialize the weighted cache
	##============================================
	delete_cache = True
	compression = None
	cache_file_printer = CacheFilePrinter(cache_directory, fast_cache_directory, compression, delete_cache)
	cache_file_printer.AddProbabilityMatrix(probability_matrix)
	cache_file_printer.AddFilterCutoff(filter_cutoff_ratio)
	cache_file_printer.AddBaseFrequencies(frequency_hash)
	sorted_chromosome_label_list = sort_numeric_string(chromosome_hash.keys())
	for chromosome_label in sorted_chromosome_label_list:
		chromosome = chromosome_hash[chromosome_label]
		cache_file_printer.AddChromosome(chromosome.Name, chromosome.Sequence.Sequence)

	##============================================
	## generate "lists" from the unweighted cache
	##============================================
	unweighted_cache_handle = unweighted_cache_file_handler.GetHandle()
	for chromosome_label in sorted_chromosome_label_list:
		chromosome = chromosome_hash[chromosome_label]
		unweighted_cache_chromosome = unweighted_cache_file_handler.GetChrom(chromosome_label)
		num_windows = unweighted_cache_chromosome.GetNumWindows()
		forward_offset = unweighted_cache_chromosome.GetForwardOffset()
		revcomp_offset = unweighted_cache_chromosome.GetRevCompOffset()
		unweighted_forward_cache = CacheWindowList(unweighted_cache_handle, num_windows, forward_offset)
		unweighted_revcomp_cache = CacheWindowList(unweighted_cache_handle, num_windows, revcomp_offset)

		##============================================
		## weight the unweighted Ka's
		##============================================
		Ka_slice_offset = 0
		unweighted_forward_list = unweighted_forward_cache[:]
		weighted_foward_list = ApplyGenomeWeights(unweighted_forward_list,
												  Ka_slice_offset,
												  chromosome_label,
												  len(probability_matrix),
												  genome_weight_dict,
												  default_genome_weight)
		del unweighted_forward_list
		unweighted_revcomp_list = unweighted_revcomp_cache[:]
		weighted_revcomp_list = ApplyGenomeWeights(unweighted_revcomp_list,
												   Ka_slice_offset,
												   chromosome_label,
												   len(probability_matrix),
												   genome_weight_dict,
												   default_genome_weight)
		del unweighted_revcomp_list
		weighted_hitlist = HitList(weighted_foward_list,
								   weighted_revcomp_list,
								   probability_matrix, probability_matrix.MD5)
		cache_file_printer.AddHitlist(weighted_hitlist)
	new_cache_table_entry, cache_file_handler = cache_file_printer.Finish()
	return new_cache_table_entry, cache_file_handler
	



def _load_cache(cache_file_handler, sorted_chromosome_label_list, chromosome_hash,
				probability_matrix,debug):
	#@<<load windows from cache>>
	#@+node:2::<<load windows from cache>>
	#@+body
	##------------------------------------------------------------------------
	if __debug__:
		print >>sys.stderr, '*' * 50, '\nLoading windows from cache\n', '*' * 50
	##------------------------------------------------------------------------
	cache_handle = cache_file_handler.GetHandle()
	##>>>------------------------------------------------------------
	for chromosome_label in sorted_chromosome_label_list:
		chromosome = chromosome_hash[chromosome_label]
	
		cur_cache_chromosome = cache_file_handler.GetChrom(chromosome_label)
		num_windows = cur_cache_chromosome.GetNumWindows()
		forward_offset = cur_cache_chromosome.GetForwardOffset()
		revcomp_offset = cur_cache_chromosome.GetRevCompOffset()
	
		foward_cache_list = CacheWindowList(cache_handle, num_windows, forward_offset)
		revcomp_cache_list = CacheWindowList(cache_handle, num_windows, revcomp_offset)
		## (cache_file_handler.GetHits(chromosome_label))
		hitlist = HitList(foward_cache_list, revcomp_cache_list, probability_matrix, probability_matrix.MD5)
	
		## OLD FORMATTING
		## old_test_array_index(hitlist.Forward, hitlist.ReverseComplement, '', chromosome_label)
		## NEW FORMATTING
		if debug:
			test_array_index(hitlist.Forward, hitlist.ReverseComplement, '', chromosome_label)
		##test_array_index(hitlist.Forward, hitlist.ReverseComplement, 'Cached', chromosome_label)
	
		chromosome.AddHitlist(hitlist, hitlist.Name)
		## memcheck('Chromosome ' + chromosome.Name + ' scored')
	##>>>------------------------------------------------------------
	##------------------------------------------------------------------------
	
	#@-body
	#@-node:2::<<load windows from cache>>


def cache_score_all_windows(probability_matrix, chromosome_hash,
							filter_cutoff_ratio,
							cache_directory,cache_table_filename,
							fast_cache_directory,
							frequency_hash,
							genome_weight_dict, default_genome_weight,
							debug=False,
							compression=None,
							delete_cache=False):
	if __debug__:
		print >>sys.stderr, '=========================='
		print >>sys.stderr, 'cache_score_all_windows'
		print >>sys.stderr, '=========================='

## 1. Check cache table for file
## 2. If it exists, load it, if it doesn't:
	## 1. score windows and write cache file
	## 2. Update cache table 

##	sorted_chromosome_label_list = chromosome_hash.keys()
##	sorted_chromosome_label_list.sort(IntegerStringCmp)
	sorted_chromosome_label_list = sort_numeric_string(chromosome_hash.keys())

	##========================
	##========================
	## Try to load cached data
	##========================
	##========================
	cache_file_handler = load_cache_file(probability_matrix, chromosome_hash,
										 filter_cutoff_ratio,frequency_hash,
										 cache_directory, cache_table_filename,
										 fast_cache_directory,
										 compression, delete_cache)
	##========================
	##========================
	## If cached data doesn't
	## exist, score genome, and
	## generate new cache file.
	##========================
	##========================
	if not cache_file_handler:
		new_cache_table_entry, cache_file_handler = _generate_new_cache(probability_matrix, chromosome_hash,
																				 filter_cutoff_ratio,
																				 cache_directory,
																				 fast_cache_directory,
																				 frequency_hash,
																				 compression=None,
																				 delete_cache=False)
		if not delete_cache:
			append_cache_table(new_cache_table_entry, cache_directory, cache_table_filename)
		

		#@+doc
		# 
		# cache_file_handler = load_cache_file(probability_matrix, chromosome_hash,
		# 									 filter_cutoff_ratio,frequency_hash,
		# 									 cache_directory, cache_table_filename,
		# 									 compression)
		# if not cache_file_handler:
		# 	error_message = 'load_cache_file returned ' + str(cache_file_handler)\
		# 					+ " after scoring windows.\nTHIS SHOULDN'T HAPPEN"
		# 	raise TestControllerError(error_message)

		#@-doc
		#@@code
		#@-body
		#@-node:1::<<generate new cache>>
	if genome_weight_dict:
		## the weighted cache should never be saved!!!
		new_cache_table_entry, cache_file_handler = _generate_weighted_cache(genome_weight_dict,
																			 default_genome_weight,
																			 cache_file_handler,
																			 probability_matrix, chromosome_hash,
																			 filter_cutoff_ratio, frequency_hash,
																			 cache_directory,
																			 fast_cache_directory)
	_load_cache(cache_file_handler, sorted_chromosome_label_list, chromosome_hash,probability_matrix,debug)


	## return probability_matrix.Descriptor
	return probability_matrix.MD5
##	cache_file_handler.LoadHitWindows(chromosome_hash)
#@-body
#@-node:3::<<def cache_score_all_windows>>


#@<<def refilter_cache>>
#@+node:4::<<def refilter_cache>>
#@+body
def refilter_cache(cache_file_handler,
				   probability_matrix,
				   chromosome_hash,
				   filter_cutoff_ratio,
				   cache_directory, cache_table_filename,
				   fast_cache_directory,
				   compression,
				   delete_cache):
	"""
	First check to be sure that we are not trying to filter to a higher cutoff ratio (go from less to more information).  Calculate the Ka threshold.  A new cache file is generated using the info from the cache file which is being refiltered.  For each strand on each chromosome, load the hitlist into a Numeric array, then filter this array using Numeric.less to generate a binary mask, which Numeric.putmask can use to set all Ka values in the hitlist which fall below the Ka threshold to 0.0.  Then the filtered array is output to the cache file.  Using Numeric.putmask with Numeric.less makes the refiltering process very fast!
	"""

	if __debug__:
		print >>sys.stderr,  '=========================='
		print >>sys.stderr,  'refilter_cache'
		print >>sys.stderr,  '=========================='
		print >>sys.stderr, 'Refiltering Cache:', cache_file_handler.GetHandle().name, 'cutoff:', cache_file_handler.GetFilterCutoff(), ', to cutoff:', filter_cutoff_ratio
	if filter_cutoff_ratio > cache_file_handler.GetFilterCutoff() and \
		   cache_file_handler.GetFilterCutoff() <> 0:
		raise StandardError, 'ERROR: Cannot refilter to a higher filter cutoff' 
	elif filter_cutoff_ratio < 1:
		raise StandardError, 'ERROR: Cannot filter to a filter cutoff ratio less than 1' 

	max_Ka, best_values, best_sequence = probability_matrix.GetBestKa()
	filter_cutoff_Ka = max_Ka/filter_cutoff_ratio
	if __debug__: print >>sys.stderr, 'filter_cutoff_Ka:', filter_cutoff_Ka
##	sorted_chromosome_label_list = chromosome_hash.keys()
##	sorted_chromosome_label_list.sort(IntegerStringCmp)
	sorted_chromosome_label_list = sort_numeric_string(chromosome_hash.keys())

	cache_file_printer = CacheFilePrinter(cache_directory, fast_cache_directory, compression, delete_cache)
	cache_file_printer.AddProbabilityMatrix(probability_matrix)
	cache_file_printer.AddFilterCutoff(filter_cutoff_ratio)
##---------------------------------------------------------
	bases = cache_file_handler.Bases
	bases.sort()
	frequency_hash = {}
	for cur_base in bases:
		frequency_hash[cur_base] = cache_file_handler.GetBaseFrequency(cur_base)
	cache_file_printer.AddBaseFrequencies(frequency_hash)
##---------------------------------------------------------
	for chromosome_label in sorted_chromosome_label_list:
		chromosome = chromosome_hash[chromosome_label]
		cache_file_printer.AddChromosome(chromosome.Name, chromosome.Sequence.Sequence)

	for chromosome_label in sorted_chromosome_label_list:
		chromosome = chromosome_hash[chromosome_label]
		if __debug__:
			print >>sys.stderr,  'Refiltering Windows of Chromosome:', chromosome.Name
##----------------------------------------------------------------------------------
	##------------------------------------------------------------------------
		cache_handle = cache_file_handler.GetHandle()
		cur_cache_chromosome = cache_file_handler.GetChrom(chromosome_label)
		num_windows = cur_cache_chromosome.GetNumWindows()
		##---------------------------------------------------------------
		forward_offset = cur_cache_chromosome.GetForwardOffset()
		forward_cache_list = CacheWindowList(cache_handle, num_windows, forward_offset)[:]
##		##-----------------------------
##		refiltered_forward_list = where(greater_equal(forward_cache_list,Ka_filter_cutoff),
##									   forward_cache_list,
##									   0.0)
##		del forward_cache_list
		##-----------------------------
		##-----------------------------
		## zero_list = Numeric.zeros(forward_cache_list.shape, savespace=1)
		Numeric.putmask(forward_cache_list,
				Numeric.less(forward_cache_list, filter_cutoff_Ka),
				0.0)
		refiltered_forward_list = forward_cache_list
		##-----------------------------
		##---------------------------------------------------------------
		revcomp_offset = cur_cache_chromosome.GetRevCompOffset()
		revcomp_cache_list = CacheWindowList(cache_handle, num_windows, revcomp_offset)[:]
##		##-----------------------------
##		refiltered_revcomp_list = where(greater_equal(revcomp_cache_list,filter_cutoff_Ka),
##									   revcomp_cache_list,
##									   Numeric.zeros(revcomp_cache_list.shape, savespace=1))
##		del revcomp_cache_list
##		##-----------------------------
		Numeric.putmask(revcomp_cache_list,
				Numeric.less(revcomp_cache_list, filter_cutoff_Ka),
				0.0)
		refiltered_revcomp_list = revcomp_cache_list
		##---------------------------------------------------------------
		hitlist = HitList(refiltered_forward_list, refiltered_revcomp_list,
						  probability_matrix, probability_matrix.MD5)

		if __debug__:
			print >>sys.stderr,  'Adding Refiltered Windows to cache from Chromosome:', chromosome.Name			
		cache_file_printer.AddHitlist(hitlist)
		del hitlist

	new_cache_table_entry, refiltered_cache_file_handler = cache_file_printer.Finish()
	if not delete_cache:
		append_cache_table(new_cache_table_entry, cache_directory, cache_table_filename)
	return new_cache_table_entry, refiltered_cache_file_handler


#@+doc
# 
# """
# putmask (a, mask, values)
# putmask sets those elements of a for which mask is true to the corresponding 
# value in values. The array a
# must be contiguous. The argument mask must be an integer sequence of the 
# same size (but not necessarily the
# same shape) as a. The argument values will be repeated as necessary; in 
# particular it can be a scalar. The
# array values must be convertible to the type of a.
# >>> x=arange(5)
# >>> putmask(x, [1,0,1,0,1], [10,20,30,40,50])
# >>> print x
# [10 1 30 3 50]
# >>> putmask(x, [1,0,1,0,1], [-1,-2])
# >>> print x
# [-1 1 -1 3 -1]
# Note how in the last example, the third argument was treated as if it was 
# [-1, -2, -1, -2, -1].
# 
# 
# where(condition, x, y)
# where(condition,x,y) returns an array shaped like condition and has elements 
# of x and y where condition is re-spectively
# true or false
# 
# 
# where (condition, x, y)
# The where function creates an array whose values are those of x at those 
# indices where condition is true, and
# those of y otherwise. The shape of the result is the shape of condition. The 
# type of the result is determined by
# the types of x and y. Either or both of x and y and be a scalar, which is 
# then used for any element of condition
# which is true.
# 
# UFUNCS:
# greater (>) equal (==) not_equal (!=)
# greater_equal (>=) less (<) less_equal (<=)
# 
# >>> a = array([0,1,2,3,4])
# >>> print greater(a,0)
# [0 1 1 1 1]
# ---------------------------------------------------------------------------------------
# >>> import Numeric
# >>> import RandomArray
# >>> a = RandomArray.randint(0,1000, (10.0,))
# >>> a
# array([127, 153, 880, 354, 180, 131, 939, 743, 100, 605])
# >>> Numeric.where(Numeric.greater_equal(a,500), a, Numeric.zeros(a.shape, savespace=1))
# array([  0,   0, 880,   0,   0,   0, 939, 743,   0, 605])
# ------------------------------------------------
# >>> import Numeric
# >>> import RandomArray
# >>> a = RandomArray.uniform(0,1000, (10.0,))
# >>> a
# array([ 711.22311723,  776.59364178,  163.7256117 ,  630.30859663,  238.79781889,
#              785.11841902,  805.35620419,  127.60496364,  436.51492848,
#              556.83202354])
# >>> Numeric.where(Numeric.greater_equal(a,500), a, Numeric.zeros(a.shape, savespace=1))
# array([ 711.22311723,  776.59364178,    0.        ,  630.30859663,    
# 0.        ,
#              785.11841902,  805.35620419,    0.        ,    0.        ,
#              556.83202354])
# ##-----------------------------------------------------------------------------
# >>> def do_that_thing(size, use_floats=0):
# 	if use_floats:
# 		a = RandomArray.uniform(0,1000, (size,))
# 	else:
# 		a = RandomArray.randint(0,1000, (size,))
# 	print a
# 	b = Numeric.where(Numeric.greater_equal(a,500), a, Numeric.zeros(a.shape, savespace=1))
# 	Numeric.putmask(a, Numeric.less(a,500), Numeric.zeros(a.shape, savespace=1))
# 	print 'where:', b
# 	print 'putmask:', a
# 	if Numeric.alltrue(a == b):
# 		print 'they are equal'
# 	else:
# 		print 'ERROR!!! THEY ARE NOT EQUAL'
# 
# ##-----------------------------------------------------------------------------
# ##--------------------------------------------------------------------------------
# >>> def run_test(size, use_floats=0, output=0):
# 	if use_floats:
# 		a = RandomArray.uniform(0,1000, (size,))
# 	else:
# 		a = RandomArray.randint(0,1000, (size,))
# 	if output: print a
# 	b = a.copy()
# 	x = a.copy()
# 	del a
# 	t1 = time.clock()
# 	c = Numeric.where(Numeric.greater_equal(b,500), b, Numeric.zeros(b.shape, savespace=1))
# 	t2 = time.clock()
# 	Numeric.putmask(x, Numeric.less(x,500), Numeric.zeros(x.shape, savespace=1))
# 	t3 = time.clock()
# 	print 'where:', round(t2-t1, 3)
# 	print 'putmask:', round(t3-t2, 3)
# 	if output: print 'where:', c
# 	if output: print 'putmask:', x
# 	if Numeric.alltrue(c == x):
# 		print 'they are equal'
# 	else:
# 		print 'ERROR!!! THEY ARE NOT EQUAL'
# 	##-------------------------------------------------------------
# 	if use_floats:
# 		a = RandomArray.uniform(0,1000, (size,))
# 	else:
# 		a = RandomArray.randint(0,1000, (size,))
# 	if output: print a
# 	b = a.copy()
# 	x = a.copy()
# 	del a
# 	t1 = time.clock()
# 	Numeric.putmask(x, Numeric.less(x,500), Numeric.zeros(x.shape, savespace=1))
# 	t2 = time.clock()
# 	c = Numeric.where(Numeric.greater_equal(b,500), b, Numeric.zeros(b.shape, savespace=1))
# 	t3 = time.clock()
# 	print 'putmask:', round(t2-t1, 3)
# 	print 'where:', round(t3-t2, 3)
# 	if output: print 'where:', c
# 	if output: print 'putmask:', x
# 	if Numeric.alltrue(c == x):
# 		print 'they are equal'
# 	else:
# 		print 'ERROR!!! THEY ARE NOT EQUAL'
# 
# 
# 
# 
# 
# 
# """
# 
# 

#@-doc
#@-body
#@-node:4::<<def refilter_cache>>



#@<<def test_array_index>>
#@+node:5::<<def test_array_index>>
#@+body
def test_array_index(forward, revcomp, label, chromosome):	
	print '\n\n', '.' * 60
	print 'Chromosome:', chromosome
	length = len(forward)
	print label, 'Forward Length', length
	print 'forward[:10]', label, ': [', ', '.join([fpformat.sci(val, 8) for val in forward[:10]]), ']'
	print 'by index:', ','.join([fpformat.sci(forward[i], 8) for i in range(10)]), '\n'
	print 'forward[length-11:]', label, ': [', ', '.join([fpformat.sci(val, 8) for val in forward[length-11:]]), ']'	
	print 'forward[length-11:length]', label, ':', ', '.join([fpformat.sci(val, 8) for val in forward[length-11:length]])
	
	
	print 'by index:', ','.join([fpformat.sci(forward[i], 9) for i in range(length-11, length)]), '\n'
	print 'forward[500]', label, ':', fpformat.sci(forward[500], 8)
	print 'forward[500:501]', label, ': [', ', '.join([fpformat.sci(val, 8) for val in forward[500:501]]), ']'	
	print 'forward[500:502]', label, ': [', ', '.join([fpformat.sci(val, 8) for val in forward[500:502]]), ']'	
	print 'forward[500:510]', label, ': [', ', '.join([fpformat.sci(val, 8) for val in forward[500:510]]), ']'	
	


	print label, 'Revcomp Length', length
	print 'revcomp[:10]', label, ': [', ', '.join([fpformat.sci(val, 8) for val in revcomp[:10]]), ']'
	print 'by index:', ','.join([fpformat.sci(revcomp[i], 8) for i in range(10)]), '\n'
	print 'revcomp[length-11:]', label, ': [', ', '.join([fpformat.sci(val, 8) for val in revcomp[length-11:]]), ']'	
	print 'revcomp[length-11:length]', label, ':', ', '.join([fpformat.sci(val, 8) for val in revcomp[length-11:length]])
	
	
	print 'by index:', ','.join([fpformat.sci(revcomp[i], 9) for i in range(length-11, length)]), '\n'
	print 'revcomp[500]', label, ':', fpformat.sci(revcomp[500], 8)
	print 'revcomp[500:501]', label, ': [', ', '.join([fpformat.sci(val, 8) for val in revcomp[500:501]]), ']'	
	print 'revcomp[500:502]', label, ': [', ', '.join([fpformat.sci(val, 8) for val in revcomp[500:502]]), ']'	
	print 'revcomp[500:510]', label, ': [', ', '.join([fpformat.sci(val, 8) for val in revcomp[500:510]]), ']'	

#@-body
#@-node:5::<<def test_array_index>>


#@-body
#@-node:0::@file score_windows.py
#@-leo
