from __future__ import division
#@+leo
#@+node:0::@file cache_file_tokens.py
#@+body
#@@first
#@@language python

"""
*Description:

Tokens used in probability matrix files.
"""
##import os
##os.environ['PYCHECKER'] = '--allglobals --stdlib'
##import pychecker.checker


#@<<parse tokens>>
#@+node:1::<<parse tokens>>
#@+body
PROB_MATRIX_FILE_TOKEN = '%PROB_MATRIX'
PROB_MATRIX_MD5_TOKEN = '%PROB_MATRIX_MD5'
PROB_MATRIX_BEGIN_TOKEN = 'BEGIN PROB MATRIX' + '=' * 40
PROB_MATRIX_END_TOKEN = 'END PROB MATRIX' + '=' * 40
FILTER_CUTOFF_RATIO_TOKEN = '%FILTER_CUTOFF_RATIO'
FREQUENCY_TOKEN = '%FREQUENCY'

CHROMOSOME_TOKEN = '%CHROM'
DATA_START_LINE_TOKEN = '!!DATA BELOW THIS LINE!!'


#@-body
#@-node:1::<<parse tokens>>


#@-body
#@-node:0::@file cache_file_tokens.py
#@-leo
