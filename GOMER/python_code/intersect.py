from __future__ import division
#@+leo
#@+node:0::@file intersect.py
#@+body
#@@first
#@@language python

"""
*Description:

intersect(a, b) returns true if the intersection of a and b is not the empty set
"""

def intersect(a, b):
	for elem in a:
		if elem in b:
			return True
	return False
#@-body
#@-node:0::@file intersect.py
#@-leo
