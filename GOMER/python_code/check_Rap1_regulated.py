#! /usr/bin/env python

from __future__ import division

import os
import sys

from sets import Set

from regulated_feature_parser_gomer import ParseRegulatedFeatures

def main():
	if len(sys.argv) == 4:
		main_filename = sys.argv[1]
		subset1_filename = sys.argv[2]
		subset2_filename = sys.argv[3]


		(main_regulated,
		 main_excluded,
		 main_unregulated) = ParseRegulatedFeatures(main_filename)

		(subset1_regulated,
		 subset1_excluded,
		 subset1_unregulated) = ParseRegulatedFeatures(subset1_filename)

		(subset2_regulated,
		 subset2_excluded,
		 subset2_unregulated) = ParseRegulatedFeatures(subset2_filename)


	else:
		print >>sys.stderr, 'usage:', os.path.basename(sys.argv[0]), 'MAIN_FILE SUBSET1_FILE SUBSET2_FILE'
		sys.exit(1)


	print 'Regulated in', os.path.basename(subset1_filename), ':', len(subset1_regulated)
	print 'Regulated in', os.path.basename(subset2_filename), ':', len(subset2_regulated)

	print 'Excluded in', os.path.basename(subset1_filename), ':', len(subset1_excluded)
	print 'Excluded in', os.path.basename(subset2_filename), ':', len(subset2_excluded)

	##----------------------------------------------------------------
	a_file = subset1_filename
	print '='*60
	present=0
	absent_list=[]	
	print 'In', os.path.basename(subset1_filename), 'but not in', os.path.basename(main_filename), ':'
	for name in subset1_regulated:
		if name not in main_regulated: absent_list.append(name)
		else: present += 1
	print 'present:', present, 'absent:', len(absent_list) #, '\n', absent_list

	
	present=absent=0
	absent_list=[]
	print 'In', os.path.basename(subset2_filename), 'but not in', os.path.basename(main_filename), ':'
	for name in subset2_regulated:
		if name not in main_regulated:
			absent_list.append(name)
			absent += 1
		else: present += 1
	print 'present:', present, 'absent:', absent # , '\n', absent_list

	subset1_reg_set = Set(subset1_regulated.keys())
	subset2_reg_set = Set(subset2_regulated.keys())

	# print 'len(subset1_reg_set):', len(subset1_reg_set)
	# print 'len(subset2_reg_set):', len(subset2_reg_set)
	print 'subset1_reg_set.intersection(subset2_reg_set)', subset1_reg_set.intersection(subset2_reg_set)
	##----------------------------------------------------------------
	print '='*60
	present=absent=0
	absent_list=[]
	for name in main_regulated:
		if (name in subset1_regulated) or (name in subset1_excluded): present += 1
		else: absent_list.append(name)

	print 'In    ', os.path.basename(main_filename)
	print 'AND in', os.path.basename(subset1_filename), ':', present
	print 'NOT in', os.path.basename(subset1_filename), ':', len(absent_list)
	# print 'present:', present, 'absent:', absent
	# print absent_list
	in_main_not_subset1 = Set(absent_list)

	print

	present=absent=0
	absent_list=[]
	for name in main_regulated:
		if (name in subset2_regulated) or (name in subset2_excluded): present += 1
		else: absent_list.append(name)
	print 'In    ', os.path.basename(main_filename)
	print 'AND in', os.path.basename(subset2_filename), ':', present
	print 'NOT in', os.path.basename(subset2_filename), ':', len(absent_list)
	# print 'present:', present, 'absent:', absent
	# print absent_list
	in_main_not_subset2 = Set(absent_list)

	print '\nin_main_not_subset2 == in_main_not_subset1:', in_main_not_subset2 == in_main_not_subset1
	##----------------------------------------------------------------
	print '='*60
	present=absent=0
	absent_list=[]
	for name in subset2_regulated.keys() + subset2_excluded.keys():
		if (name in subset1_regulated) or (name in subset1_excluded): present += 1
		else: absent_list.append(name)
	print 'In    ', os.path.basename(subset2_filename)
	print 'AND in', os.path.basename(subset1_filename), ':', present
	print 'NOT in', os.path.basename(subset1_filename), ':', len(absent_list)

	print
	
	present=absent=0
	absent_list=[]
	for name in subset1_regulated.keys() + subset1_excluded.keys():
		if (name in subset2_regulated) or (name in subset2_excluded):  present += 1
		else: absent_list.append(name)
	print 'In    ', os.path.basename(subset1_filename)
	print 'AND in', os.path.basename(subset2_filename), ':', present
	print 'NOT in', os.path.basename(subset2_filename), ':', len(absent_list)

	subset1_reg_and_excluded = Set(subset1_regulated.keys() + subset1_excluded.keys())
	subset2_reg_and_excluded = Set(subset2_regulated.keys() + subset2_excluded.keys())

	# print '+'*60
	print
	print "subset2_reg_and_excluded == subset1_reg_and_excluded:", subset2_reg_and_excluded == subset1_reg_and_excluded
	# print '+'*60

	##----------------------------------------------------------------
	print '='*60
	present=0
	absent_list=[]
	for name in subset2_regulated:
		if name in subset1_excluded: present += 1
		else: absent_list.append(name)
	print 'Regulated in   ', os.path.basename(subset2_filename)
	print 'Excluded in    ', os.path.basename(subset1_filename), ':', present
	print 'NOT Excluded in', os.path.basename(subset1_filename), ':', len(absent_list)
	# print absent_list

	print
	
	present=absent=0
	absent_list=[]
	for name in subset2_excluded:
		if name in subset1_regulated: present += 1
		else: absent_list.append(name)
	print 'Excluded in     ', os.path.basename(subset2_filename)
	print 'Regulated in    ', os.path.basename(subset1_filename), ':', present
	print 'NOT Regulated in', os.path.basename(subset1_filename), ':', len(absent_list)

	# print absent_list
	excluded_in_subset2_not_reg_in_subset1 = Set(absent_list)
	##----------------------------------------------------------------
	subset1_reg_set = Set(subset1_regulated.keys())
	subset2_excluded_set = Set(subset2_excluded.keys())
	# print '+'*60
	print 
	print "subset1_reg == subset2_excluded:", subset1_reg_set == subset2_excluded_set
	# print '+'*60
	##----------------------------------------------------------------
	##----------------------------------------------------------------
	##===========================
	print '='*60
	present=0
	absent_list=[]
	for name in subset1_regulated:
		if name in subset2_excluded: present += 1
		else: absent_list.append(name)
	print 'Regulated in   ', os.path.basename(subset1_filename)
	print 'Excluded in    ', os.path.basename(subset2_filename), ':', present
	print 'NOT Excluded in', os.path.basename(subset2_filename), ':', len(absent_list)
	# print absent_list

	print
	
	present=absent=0
	absent_list=[]
	for name in subset1_excluded:
		if name in subset2_regulated: present += 1
		else: absent_list.append(name)
	print 'Excluded in     ', os.path.basename(subset1_filename)
	print 'Regulated in    ', os.path.basename(subset2_filename), ':', present
	print 'NOT Regulated in', os.path.basename(subset2_filename), ':', len(absent_list)

	##===========================
	excluded_in_subset1_not_reg_in_subset2 = Set(absent_list)
	##----------------------------------------------------------------
	subset1_excluded_set = Set(subset1_excluded.keys())
	subset2_reg_set = Set(subset2_regulated.keys())
	print '+'*60
	print "subset2_reg_set == subset1_excluded_set:", subset2_reg_set == subset1_excluded_set
	print '+'*60
	##----------------------------------------------------------------
	print '+'*60
	print "excluded_in_subset2_not_reg_in_subset1 == excluded_in_subset1_not_reg_in_subset2:", excluded_in_subset2_not_reg_in_subset1 == excluded_in_subset1_not_reg_in_subset2
	print '+'*60
	##----------------------------------------------------------------
	print '+'*60
	print "subset1_reg_set.intersection(subset1_excluded_set):", subset1_reg_set.intersection(subset1_excluded_set)
	print "subset2_reg_set.intersection(subset2_excluded_set):", subset2_reg_set.intersection(subset2_excluded_set)
	print "subset1_reg_set.intersection(subset2_reg_set):", subset1_reg_set.intersection(subset2_reg_set)
	print "subset1_excluded_set.intersection(subset2_excluded_set):", subset1_excluded_set.intersection(subset2_excluded_set)
	print '+'*60
	##----------------------------------------------------------------



if __name__ == "__main__":
	main()
