from __future__ import division
#@+leo
#@+node:0::@file cache_table_gomer.py
#@+body
#@@first
#@@language python


#@<<imports>>
#@+node:1::<<imports>>
#@+body
import re
import os
import sys
#@-body
#@-node:1::<<imports>>



#@<<class CacheTableEntry>>
#@+node:2::<<class CacheTableEntry>>
#@+body
class CacheTableEntry(object):
	
	def __init__(self, entry_string=None, **kwargs):
##				 probability_filename,
##				 filter_cutoff_ratio,
##				 md5sum_of_prob_file,
##				 cache_file_name,
##				 md5sum_of_cache_file):
##		(probability_filename,
##		 filter_cutoff_ratio,
##		 md5sum_of_prob_file,
##		 cache_file_name,
##		 md5sum_of_cache_file) = line.split()
		# raise None
		if entry_string:
			(self._probability_filename,
			 filter_cutoff_ratio,
			 self._md5sum_of_prob_file,
			 self._cache_file_name,
			 self._md5sum_of_cache_file) = entry_string.split()
			self._filter_cutoff_ratio = float(filter_cutoff_ratio)
			# try:
			# 	self._filter_cutoff_ratio = float(filter_cutoff_ratio)
			# except ValueError:
			# 	print >>sys.stderr, entry_string
			# 	print >>sys.stderr, type(filter_cutoff_ratio), ord(filter_cutoff_ratio)
			# 	raise
				
		else:
			self._probability_filename = kwargs['probability_filename']
			self._filter_cutoff_ratio  = kwargs['filter_cutoff_ratio']
			self._md5sum_of_prob_file  = kwargs['md5sum_of_prob_file']
			self._cache_file_name      = kwargs['cache_file_name']
			self._md5sum_of_cache_file = kwargs['md5sum_of_cache_file']
		
	def __str__(self):
		return ' '.join((self._probability_filename,
						 repr(self._filter_cutoff_ratio),
						 self._md5sum_of_prob_file,
						 self._cache_file_name,
						 self._md5sum_of_cache_file))

	#@+others
	#@+node:1::ProbFileMD5
	#@+body
	def _get_prob_file_md5sum(self): return self._md5sum_of_prob_file
	def _set_prob_file_md5sum(self, value): raise AttributeError, "Can't set 'ProbFileMD5' attribute"
	ProbFileMD5 = property(_get_prob_file_md5sum, _set_prob_file_md5sum)
	#@-body
	#@-node:1::ProbFileMD5
	#@+node:2::CacheFileName
	#@+body
	def _get_cache_file_name(self): return self._cache_file_name
	def _set_cache_file_name(self, value): raise AttributeError, "Can't set 'CacheFileName' attribute"
	CacheFileName = property(_get_cache_file_name,_set_cache_file_name)
	
	#@-body
	#@-node:2::CacheFileName
	#@+node:3::CacheFileMD5
	#@+body
	def _get_cache_file_md5sum(self): return self._md5sum_of_cache_file
	def _set_cache_file_md5sum(self, value): raise AttributeError, "Can't set 'CacheFileMD5' attribute"
	CacheFileMD5 = property(_get_cache_file_md5sum,_set_cache_file_md5sum)
	
	#@-body
	#@-node:3::CacheFileMD5
	#@+node:4::FilterCutoffRatio
	#@+body
	def _get_filter_cutoff_ratio(self): return self._filter_cutoff_ratio
	def _set_filter_cutoff_ratio(self, value): raise AttributeError, "Can't set 'FilterCutoffRatio' attribute"
	FilterCutoffRatio = property(_get_filter_cutoff_ratio,_set_filter_cutoff_ratio)
	
	#@-body
	#@-node:4::FilterCutoffRatio
	#@-others
#@-body
#@-node:2::<<class CacheTableEntry>>


#@<<def append_cache_table>>
#@+node:3::<<def append_cache_table>>
#@+body
# def append_cache_table(new_table_entry, cache_directory, cache_table_filename):
def append_cache_table(new_table_entry, cache_directory, cache_table_filename):
	full_cache_table_filename = os.path.join(cache_directory, cache_table_filename)
	cache_table_handle = file(full_cache_table_filename, 'a')
	cache_table_handle.seek(0,2)
##	entry_string = ' '.join((new_table_entry.probability_filename,
##							 new_table_entry.filter_cutoff_ratio,
##							 new_table_entry.md5sum_of_prob_file,
##							 new_table_entry.cache_file_name,
##							 new_table_entry.md5sum_of_cache_file))
	if __debug__ : print >> sys.stderr, 'New Table Entry:\n', str(new_table_entry)
	# raise None
	cache_table_handle.write(str(new_table_entry) + '\n')
	cache_table_handle.close()





	   
	

#@-body
#@-node:3::<<def append_cache_table>>


#@<<def load_cache_table>>
#@+node:4::<<def load_cache_table>>
#@+body
def load_cache_table(cache_directory, cache_table_filename):
	comment_line_re = re.compile('^#')
	empty_line_re = re.compile('^\s*$')

	prune_cache_table(cache_directory, cache_table_filename)
	full_cache_table_filename = os.path.join(cache_directory, cache_table_filename)
	cache_table_handle = file(full_cache_table_filename)
	
	"""probability_matrix_filename | md5sum_of_prob_hash_file | cache_file_name(based on probability hash) | 	md5sum_of_cache_file"""
	cache_table_hash = {}
	for line in cache_table_handle:
		line = line.strip()
		if empty_line_re.search(line):
			continue
		elif comment_line_re.search(line):
			continue
##		(probability_filename,
##		 filter_cutoff_ratio,
##		 md5sum_of_prob_file,
##		 cache_file_name,
##		 md5sum_of_cache_file) = line.split()
##		table_entry = CacheTableEntry(probability_filename,
##									  filter_cutoff_ratio,
##									  md5sum_of_prob_file,
##									  cache_file_name,
##									  md5sum_of_cache_file)
		table_entry = CacheTableEntry(line)
		cache_table_hash.setdefault(table_entry.ProbFileMD5, []).append(table_entry)
	cache_table_handle.close()
	return cache_table_hash





	   
	

#@-body
#@-node:4::<<def load_cache_table>>


#@<<def prune_cache_table>>
#@+node:5::<<def prune_cache_table>>
#@+body
def prune_cache_table(cache_directory, cache_table_filename):
	"""
	Removes entries from the cache_table_file for cache files that no longer exist
	"""
	comment_line_re = re.compile('^#')
	empty_line_re = re.compile('^\s*$')

	cache_table_base_filename = os.path.basename(cache_table_filename)
	full_cache_table_filename = os.path.join(cache_directory, cache_table_base_filename)
	cache_table_handle = file(full_cache_table_filename)
	
	entries_to_retain = []
	for line in cache_table_handle:
		line = line.strip()
		if empty_line_re.search(line):
			continue
		elif comment_line_re.search(line):
			continue
		else:
			table_entry = CacheTableEntry(line)

		if os.path.exists(os.path.join(cache_directory, table_entry.CacheFileName)):
			entries_to_retain.append(table_entry)
		else:
			print >>sys.stderr, 'Pruning the following line, for which no cache file exists, from cache table file(', full_cache_table_filename, ')\n', table_entry
	
	cache_table_handle.close()
	cache_table_handle = file(full_cache_table_filename, 'w')
	for table_entry in entries_to_retain:
		cache_table_handle.write(str(table_entry) + '\n')
	cache_table_handle.close()
	





	   

#@-body
#@-node:5::<<def prune_cache_table>>


#@-body
#@-node:0::@file cache_table_gomer.py
#@-leo


"""
  File "/local/granek/GOMER/python_code/cache_table_gomer.py", line 38, in __init__
    (self._probability_filename,

"""
