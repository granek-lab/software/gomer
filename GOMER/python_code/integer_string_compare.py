#@+leo
#@comment Created by Leo at Fri Apr 11 15:54:58 2003
#@+node:0::@file integer_string_compare.py
#@+body
#@<<imports>>
#@+node:1::<<imports>>
#@+body
##import os
##os.environ['PYCHECKER'] = '--allglobals --stdlib'
##import pychecker.checker

##import check_python_version
##check_python_version.CheckVersion()


#@-body
#@-node:1::<<imports>>


def IntegerStringCmp(x,y):
	"""
	cmp(x, y)
	Compare the two objects x and y and return an integer according to the outcome.
	The return value is negative if x < y, zero if x == y and strictly positive if x > y.
	"""
	if isStringLike(x) and isInt(x) and isStringLike(y) and isInt(y):
		return cmp(int(x), int(y))
	else:
		return cmp(x,y)


def isInt(astring):
    """ Is the given string an integer? """
    try: int(astring)
    except ValueError: return 0
    else: return 1

def isStringLike(anobj):
    try: anobj + ''
    except: return 0
    else: return 1

def sort_numeric_string(list_of_numeric_strings):
	decorated = [(try_float(num_str), num_str) for num_str in list_of_numeric_strings]
	decorated.sort()
	sorted_numeric_strings = [num_str for num,num_str in decorated]
	return sorted_numeric_strings

def try_float(x):
	"""
	try to convert to float, otherwise, just return input
	"""
	try:
		y = float(x)
	except ValueError:
		y = x
	return y

#@-body
#@-node:0::@file integer_string_compare.py
#@-leo
