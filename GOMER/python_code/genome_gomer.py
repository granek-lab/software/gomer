#! /usr/bin/env python
from __future__ import division
#@+leo
#@+node:0::@file genome_gomer.py
#@+body
#@@first
#@@first
#@@language python

"""
*Description:

Doesn't really do anything yet, I might just chunk this puppy, or I might use it
as a wrapper for the hash of all chromosomes.
"""
##import os
##os.environ['PYCHECKER'] = '--allglobals --stdlib'
##import pychecker.checker

##import check_python_version
##check_python_version.CheckVersion()


#@<<class Genome>>
#@+node:1::<<class Genome>>
#@+body
class Genome(object):

	#@+others
	#@+node:1::__init__
	#@+body
	def __init__(self, chromosome_hash, feature_dictionary):
		self._chromosome_hash = 
		self._feature_dictionary =
	#@-body
	#@-node:1::__init__
	#@+node:2::GetFeature
	#@+body
	def GetFeature(self, feature_name):
		return self._feature_dictionary[feature_name]
	#@-body
	#@-node:2::GetFeature
	#@-others
#@-body
#@-node:1::<<class Genome>>


#@-body
#@-node:0::@file genome_gomer.py
#@-leo
