#! /usr/bin/env python
#@+leo
#@+node:0::@file estimate_p_value.py
#@+body
#@@first
#@@language python

from __future__ import division
division

import sys
import random
import bisect
import math
import operator

smallest_positive_float = math.ldexp(1,-1074)
## import Numeric ##FOR_PLOT

## original values       : [3.2, 5.1, 4.5, 1.4, 13.4, 5.1]
## original values sorted: [1.4, 3.2, 4.5, 5.1, 5.1, 13.4]

## For the following, higher values are better (receive better, i.e. lower ranks)

## best-tie ranks        : 6,5,4,2,2,1
## Best-tie ranks are olympic style - if two competitors tie for first, they both
## receive gold metals, and the competitor with the next best time gets a bronze.

## worst-tie ranks       : 6,5,4,3,3,1
## Another way to look a worst-tie ranks - the rank of a member of the
## population is the number of members of the population with the same value,
## or better value.

#@+others
#@+node:1::MNCP_pvaules
#@+body


def MNCP_pvalue(regulated_values, unregulated_values, num_simulations, higher_is_better=True):
	reg_tag = 'REG'
	unreg_tag = 'UNREG'
	reg_vals_marked = [(val,reg_tag) for val in regulated_values]
	unreg_vals_marked = [(val,unreg_tag) for val in unregulated_values]
	all_val_tuples = tuples_add_worst_tie_ranks(reg_vals_marked + unreg_vals_marked, higher_is_better)
	regulated_ranks = [tup[-1] for tup in  all_val_tuples if tup[1]==reg_tag]
	all_ranks = [tup[-1] for tup in  all_val_tuples]
	return MNCP_pvalue_ranks(regulated_ranks, all_ranks, num_simulations)

def MNCP_pvalue_ranks(regulated_ranks, all_ranks, num_simulations):
##	mncp_scores = Numeric.zeros((num_simulations,), Numeric.Float)##FOR_PLOT
	regulated_ranks = list(regulated_ranks)
	num_all = len(all_ranks)
	num_regulated = len(regulated_ranks)
	real_mncp = mncp_ranks(regulated_ranks, num_all)
	num_simulations = int(num_simulations)
	ge_mncp = 0
	# simulated_mncp_list = [None] * num_simulations
	for sim_index in xrange(num_simulations):
		random_reg_ranks = random.sample(all_ranks,num_regulated)
		simulated_mncp = mncp_ranks(random_reg_ranks, num_all)
		if simulated_mncp >= real_mncp:
			ge_mncp += 1
##------------------------
##		mncp_scores[sim_index] = simulated_mncp ##FOR_PLOT
##	mncp_plot_handle = file('/tmp/mncp_for_plotting','w') ##FOR_PLOT
##	mncp_plot_handle.write(''.join([repr(score)+'\n' for score in mncp_scores])) ##FOR_PLOT
##	mncp_plot_handle.close() ##FOR_PLOT
##------------------------
#	return ge_mncp/num_simulations
	return real_mncp, ge_mncp

#@-body
#@-node:1::MNCP_pvaules
#@+node:2::MNCP
#@+body
##-----------------------------
def factorial(n):
	"""
	from python doctest documentation
	"""
	if not n >= 0:
		raise ValueError("n must be >= 0")
	if math.floor(n) != n:
		raise ValueError("n must be exact integer")
	if n+1 == n:  # catch a value like 1e300
		raise OverflowError("n too large")
	result = 1
	factor = 2
	while factor <= n:
		try:
			result *= factor
		except OverflowError:
			result *= long(factor)
		factor += 1
	return result
##-----------------------------
def FitLogCurve(data_pair_list):
	"""
	Uses least squares fitting to determine the coefficients (a and b) to fit the the logarithmic function "y = a + b ln(x)" to the date pairs in the supplied data list.

	data_pair_list should be a list (or tuple) of 2-element lists (or tuples) representing the data pairs, where the first elements of the sublists are the "x" value, and the second elements are the "y" values.
	"""
	n = len(data_pair_list)
	y_ln_x_sum = y_sum = ln_x_sum =	ln_x_squared_sum = 0
	for x,y in data_pair_list:
		try:
			ln_x = math.log(x)
		except OverflowError:
			print >>sys.stderr, 'x:', x, 'y:', y
			raise
		# ln_x = math.log(x)
		y_ln_x_sum += y * ln_x
		y_sum += y
		ln_x_sum += ln_x
		ln_x_squared_sum += ln_x**2

	b = ((n * y_ln_x_sum) - (y_sum * ln_x_sum))/((n*ln_x_squared_sum) - (ln_x_sum**2))
	a = (y_sum - (b * ln_x_sum))/n
	return a,b

def _generate_ln_data_with_error(a, b, n, x_min, x_max):
	data_list = [None] * n
	for index in xrange(n):
		x = random.uniform(x_min, x_max)
		y = a + (b * math.log(x))
		y_error = abs(y) * 0.05
		y += random.uniform(-y_error, y_error)
		data_list[index] = ((x,y))
	return data_list

def _test_log_fit(a,b,n,xmin,xmax):
	import os
	log_data =  _generate_ln_data_with_error(a,b,n,xmin,xmax)
	fit_a, fit_b = FitLogCurve(log_data)
	print 'a:', fit_a, 'b:', fit_b

	count = 0
	while os.path.exists('/tmp/log_data' + str(count)):
			count += 1
	handle = file('/tmp/log_data' + str(count), 'w')
	print >> handle, 'a:', a, ', b:', b, ', n:', n, ', xmin:', xmin, ', xmax:', xmax
	print >> handle, 'fit_a: ', fit_a, ', fit_b:', fit_b
	out_string = ''.join([str(x) + ', ' + str(y) + '\n' for x,y in log_data])
	handle.write(out_string)
	handle.close()

##-----------------------------
def N_choose_R(N, R):
	N_fact = factorial(N)
	R_fact = factorial(R)
##	try:
##		result = factorial(N)/(factorial(R) * factorial(N - R))
##	except OverflowError:
##		result = 'OverflowError'
	result = factorial(N)/(factorial(R) * factorial(N - R))
	return result
##-----------------------------
def reciprocal_N_choose_R(N, R):
	N_fact = factorial(N)
	R_fact = factorial(R)
##	try:
##		result = (factorial(R) * factorial(N - R))/factorial(N)
##	except OverflowError:
##		result = 'OverflowError'
	result = (factorial(R) * factorial(N - R))/factorial(N)
	return result

##def EstimateMNCPProb(regulated_values, unregulated_values, higher_is_better=True):
##	best_mncp, worst_mncp = BestWorstMNCP(regulated_values, unregulated_values,
##										  higher_is_better)
##	worst_prob = 1 # the probability of getting the worst MNCP score, or better
##	## If there are N total features, and R regulated features and there are no ties among all the features, the probability of the best_mncp is N choose R, which is calculated
##	## n!/(r! * (n-r)!)
##	## 
##	## However, if one (or more) of the top R features ties with one (or more) of the features not in the top R, then the probability is less (this can be detected by checking to see if the actual rank of the "Rth ranked" feature is greater that R).
##	## For example, if the best 10 features all have the same score (and therefore all receive the worst-tie rank of 10), but there are only 7 regulated features, then the probability of the best_mncp will be less than N choose R, because there is more than one selection of features that will produce the best_mncp score.
##	reg_num = len(regulated_values)
##	unreg_num = len(unregulated_values)
##	total_num = reg_num + unreg_num
##	# num_ties_in_top is the number of values within the top reg_num number of values that tie with values outside of the top reg_num
##	## for example if the ranks are [1,2,7,7,7,7,7,8,9,10,11,12,13,14,15],
##	# reg_num = 4
##	# num_ties = 5
##	# num_ties_in_top = 2
##	all_val_tuples = tuples_add_worst_tie_ranks(regulated_values +
##												unregulated_values,
##												higher_is_better)
##	last_top = all_val_tuples[reg_num-1]
##	first_rest = all_val_tuples[reg_num]
##	if last_top[-1] == first_rest[-1]:
##		tie_rank = last_top[-1]
##		num_ties_in_top = num_ties = 0
##		for index in xrange(len(all_val_tuples)):
##			if all_val_tuples[index][-1] == tie_rank:
##				num_ties += 1
##				if index < reg_num:
##					num_ties_in_top += 1
##		best_prob = 1/(N_choose_R(total_num, reg_num)/N_choose_R(num_ties, num_ties_in_top))
##		# the probability of getting the best MNCP score
##	else:
##		best_prob = 1/N_choose_R(total_num, reg_num)
##		# the probability of getting the best MNCP score

##	a, b = FitLogCurve([(best_mncp, best_prob),(worst_mncp, worst_prob)])
##	actual_mncp = MNCP(regulated_values, unregulated_values, higher_is_better)
##	estimated_prob = a + b*(math.log(actual_mncp))
##	# should y be 1-prob?
##	return estimated_prob, actual_mncp
	  

def EstimateMNCPProb(regulated_values, unregulated_values, higher_is_better=True):

	reg_num = len(regulated_values)
	unreg_num = len(unregulated_values)
	num_all = reg_num + unreg_num
	
	range_ranks = range(1, num_all+1)
	best_ranks = range_ranks[:reg_num]
	best_mncp = mncp_ranks(best_ranks, num_all)
	# print 'best_ranks:', best_ranks, 'num_all:', num_all
	worst_ranks = range_ranks[-reg_num:]
	## worst_ranks = [num_all]*reg_num
	worst_mncp = mncp_ranks(worst_ranks, num_all)
	# print 'worst_ranks:', worst_ranks, 'num_all:', num_all, 'worst_mncp:', worst_mncp

	worst_prob = 1 # the probability of getting the worst MNCP score, or better
##	number_permutations = N_choose_R(num_all, reg_num)
##	best_prob = 1/number_permutations # the probability of getting the best MNCP score
	best_prob = reciprocal_N_choose_R(num_all, reg_num)
	if best_prob == 0:
		best_prob = smallest_positive_float
	# print 'number_permutations:', number_permutations
	
##	best_mncp, worst_mncp, all_pop = BestWorstMNCP(regulated_values, unregulated_values,
##												   higher_is_better)
	## If there are N total features, and R regulated features and there are no ties among all the features, the probability of the best_mncp is N choose R, which is calculated
	## n!/(r! * (n-r)!)
	## 
	## However, if one (or more) of the top R features ties with one (or more) of the features not in the top R, then the probability is less (this can be detected by checking to see if the actual rank of the "Rth ranked" feature is greater that R).
	## For example, if the best 10 features all have the same score (and therefore all receive the worst-tie rank of 10), but there are only 7 regulated features, then the probability of the best_mncp will be less than N choose R, because there is more than one selection of features that will produce the best_mncp score.
	# num_ties_in_top is the number of values within the top reg_num number of values that tie with values outside of the top reg_num
	## for example if the ranks are [1,2,7,7,7,7,7,8,9,10,11,12,13,14,15],
	# reg_num = 4
	# num_ties = 5
	# num_ties_in_top = 2

##	last_top = all_pop[reg_num-1]
##	first_rest = all_pop[reg_num]
##	print 'last_top rank:', last_top.rank, 'last_top value:', last_top.value 
##	print 'first rest rank:', first_rest.rank, 'first_rest value:', first_rest.value 
##	number_permutations = N_choose_R(total_num, reg_num)
##	print 'number_permutations:', number_permutations
##	if last_top.rank == first_rest.rank:
##		tie_rank = last_top.rank
##		num_ties_in_top = num_ties = 0
##		for index in xrange(len(all_pop)):
##			if all_pop[index].rank == tie_rank:
##				num_ties += 1
##				if index < reg_num:
##					num_ties_in_top += 1
##		number_tie_permutes = N_choose_R(num_ties, num_ties_in_top)
##		best_prob = 1/(number_permutations/number_tie_permutes)
##		print 'number_tie_permutes:', number_tie_permutes
##		# the probability of getting the best MNCP score
##	else:
##		best_prob = 1/number_permutations
##		# the probability of getting the best MNCP score


	## We really want the probabilities to be logarithmically related to MNCP, so (despite the fact that it seems silly), we will make x be probability and y be MNCP, then at the end, solve for x (the probability)
	if __debug__: print >>sys.stderr, 'best_prob:', best_prob, 'best_mncp:', best_mncp, 'worst_prob:', worst_prob, 'worst_mncp:', worst_mncp
	a, b = FitLogCurve([(best_prob, best_mncp),(worst_prob, worst_mncp)])
	actual_mncp = MNCP(regulated_values, unregulated_values, higher_is_better)
##	print 'actual_mncp', actual_mncp
##	print 'a', a
##	print 'b', b
##	print '(actual_mncp - a)/b)', ((actual_mncp - a)/b)
##	print 'math.exp((actual_mncp - a)/b)', math.exp((actual_mncp - a)/b)
	estimated_prob = math.exp((actual_mncp - a)/b)
	## estimated_best_prob = math.exp((best_mncp - a)/b)
	## estimated_worst_prob = math.exp((worst_mncp - a)/b)
	
##	print 'best_mncp:', best_mncp, 'best_prob:', best_prob, 'worst_mncp:', worst_mncp,'worst_prob:', worst_prob
##	print 'estimated_best_prob:', estimated_best_prob,'estimated_worst_prob:', estimated_worst_prob
##	print 'a:', a, 'b:', b
##	print 'estimated_prob:', estimated_prob, 'actual_mncp:', actual_mncp
	# should y be 1-prob?
	return estimated_prob, actual_mncp
	
	

##def BestWorstMNCP(regulated_values, unregulated_values, higher_is_better=True):
##	num_reg = len(regulated_values)
##	num_unreg = len(unregulated_values)
##	num_all = num_reg + num_unreg
##	regulated_pop = [PopMember(val) for val in regulated_values]
##	unregulated_pop = [PopMember(val) for val in unregulated_values]
##	all_pop = regulated_pop + unregulated_pop
##	worst_tie_rank_members(all_pop, higher_is_better)
####	all_val_tuples = tuples_add_worst_tie_ranks(regulated_values +
####												unregulated_values,
####												higher_is_better)

##	pop_with_rank = [(member.rank, member) for member in all_pop]
##	pop_with_rank.sort()
##	all_pop = [member for rank, member in pop_with_rank]
	
##	best_members = all_pop[:num_reg]
##	best_ranks = [member.rank for member in  best_members]
##	best_mncp = mncp_ranks(best_ranks, num_all)
##	print 'best_members:', best_ranks, 'num_all:', num_all

##	worst_members = all_pop[-num_reg:]
##	worst_ranks = [member.rank for member in  worst_members]
##	worst_mncp = mncp_ranks(worst_ranks, num_all)
##	print 'worst_tuples:', worst_ranks, 'num_all:', num_all

##	return best_mncp, worst_mncp, all_pop

##def BestMNCP(regulated_values, unregulated_values):
##	num_reg = len(regulated_values)
##	num_unreg = len(unregulated_values)
##	num_all = num_reg + num_unreg
##	# print '[num_reg]*num_reg:', mncp_ranks([num_reg]*num_reg,num_all )
##	# print 'range(num_reg):', mncp_ranks(range(1,num_reg+1),num_all)
##	return mncp_ranks(range(1,num_reg+1),num_all)

##def WorstMNCP(regulated_values, unregulated_values):
##	num_reg = len(regulated_values)
##	num_unreg = len(unregulated_values)
##	num_all = num_reg + num_unreg
##	reg_ranks = [num_all]*num_reg
##	print '[num_all]*num_reg:', reg_ranks, mncp_ranks(reg_ranks,num_all)
##	reg_ranks = range((num_all-num_reg)+1,num_all+1)
##	print 'range(num_reg):', reg_ranks, mncp_ranks(reg_ranks,num_all)
##	# return 


def MNCP(regulated_values, unregulated_values, higher_is_better=True):
	reg_tag = 'REG'
	unreg_tag = 'UNREG'
	reg_vals_marked = [(val,reg_tag) for val in regulated_values]
	unreg_vals_marked = [(val,unreg_tag) for val in unregulated_values]
	all_val_tuples = tuples_add_worst_tie_ranks(reg_vals_marked + unreg_vals_marked, higher_is_better)
	# regulated_tups = [tup for tup in  all_val_tuples if tup[1]==reg_tag]
	regulated_ranks = [tup[-1] for tup in  all_val_tuples if tup[1]==reg_tag]
	## print regulated_tups, '\n',regulated_ranks
	return mncp_ranks(regulated_ranks, len(all_val_tuples))

def mncp_ranks(regulated_ranks, num_all):
	"""
	returns: the mncp when supplied the RANKS (not scores), among all features, of the regulated features
	
	num_all: N, the number of features considered (regulated and unregulated)
	
	list_regulated_ranks: A list of the ranks of the regulated features among all features.
		Requirements of ranks:
			1. Lower ranks are better, i.e. the best feature gets a rank of 1 (assuming there isn't a tie for first), and the worst ranking feature gets a rank of N.
			2. Ranks must be bottom-tie ranks.  To put it another way, the rank of a feature is the number of features with the same or better score as the feature.  So if we sort the 10 scores, and the 3rd, 4th, and 4th scores are the same, the ranks will be 1,2,5,5,5,6,7,8,9,10.
	"""
	# regulated_ranks = list(regulated_ranks)
	regulated_size = len(regulated_ranks)
	regulated_rank_pairs = add_worst_tie_ranks(regulated_ranks)
	# the first value in each pair is the rank among all features,
	# the second value is the rank among regulated
	sum = 0
	for all_rank, reg_rank in regulated_rank_pairs:
		# print reg_rank, all_rank
		sum += reg_rank/all_rank
	## print regulated_size, num_all, `sum`
	return sum * (num_all/regulated_size**2)
	## return (float(sum)/float(regulated_size)) * float(float(num_all)/float(regulated_size))



def MannWhitney(A_val_list, B_val_list, higher_is_better=1):
	n = len(A_val_list)
	m = len(B_val_list)
	
	A_population = [PopMember(val) for val in A_val_list]
	B_population = [PopMember(val) for val in B_val_list]
	joint_population = A_population + B_population

	average_tie_rank_members(joint_population, higher_is_better)
	##-------------------
	A_ranks = [member.rank for member in A_population]
	B_ranks = [member.rank for member in B_population]

	U_a = (n*m +((n*(n+1))/2)) - reduce(operator.add, A_ranks) 
	U_b = (n*m +((m*(m+1))/2)) - reduce(operator.add, B_ranks) 

	U = min((U_a, U_b))

	z = (U - ((n*m)/2))/math.sqrt((n*m*(n+m+1))/12)

	##----------------------------------------------------
##	##DEBUG
##	print 'A_ranks (Ranksums', reduce(operator.add, A_ranks), ')'
##	for rank in A_ranks:
##		print rank

##	print 'B_ranks (Ranksums', reduce(operator.add, B_ranks), ')'
##	for rank in B_ranks:
##		print rank
	##----------------------------------------------------
	
	return U, z
	

#@-body
#@-node:2::MNCP
#@+node:3::ROCAUC
#@+body
#@+doc
# 
# Its real simple to code the ROC AUC.  Here are the steps:
# 
# 1: For all regulated genes:
#      Area_slice = (# unregulated genes of lower rank order) + 0.5* 
# (#unregulated genes
# of rank order equal to the regulated gene)
# 
# 2: Area = sum(Area_slices)
# 3: Normalized AUC  = area / ((total regulated genes) * (total unregulated genes))
# have fun!
# -

#@-doc
#@@code
def slower_ROCAUC(regulated_val_list, unregulated_val_list, higher_is_better=True):
	unregulated_val_list.sort()
	regulated_val_list.sort()
	total_area = 0
	for reg_value in regulated_val_list:
		better = better_count(reg_value, unregulated_val_list, higher_is_better)
		same = same_count(reg_value, unregulated_val_list)
		total_area += better + (same/2)
	return total_area/(len(regulated_val_list) * len(unregulated_val_list))

def better_count(val, seq, higher_is_better=0):
	"""
	returns: number of values that are better than val

	In this case, better values are values which are greater (i.e. higher scores)
	"""
	seq.sort()
	if higher_is_better:
		return bisect.bisect_left(seq, val)
	else:
		return len(seq) - bisect.bisect_right(seq, val)

def same_count(val, seq):
	"""
	returns: number of values that are equal to val
	"""
	seq.sort()
	## seq.reverse()
	return bisect.bisect_right(seq, val) - bisect.bisect_left(seq, val)

def old_ROCAUC(regulated_val_list, unregulated_val_list, higher_is_better=True):
	unregulated_val_list.sort()
	num_unreg = len(unregulated_val_list)
	num_reg = len(regulated_val_list)
	## regulated_val_list.sort()
	total_area = 0
	for reg_value in regulated_val_list:
		left_index  = bisect.bisect_left(unregulated_val_list, reg_value)
		# right_index = bisect.bisect_right(unregulated_val_list, reg_value, lo=left_index)
		right_index = bisect.bisect_right(unregulated_val_list, reg_value, left_index)
		if higher_is_better: better = left_index
		else: 	             better = num_unreg - right_index
		same = right_index - left_index
		total_area += better + (same/2)
	return total_area/(num_reg * num_unreg)

#@-body
#@-node:3::ROCAUC
#@+node:4::ROCAUC_pvalues
#@+body
def slower_ROCAUC_pvalue(regulated_values, unregulated_values, num_simulations, higher_is_better=True):
	regulated_values = tuple(regulated_values)
	unregulated_values = tuple(unregulated_values)
	num_regulated = len(regulated_values)
	num_simulations = int(num_simulations)
	real_ROCAUC = old_ROCAUC(list(regulated_values),list(unregulated_values), higher_is_better)
	ge_ROCAUC = 0
	all_values = list(regulated_values + unregulated_values)
	all_values.sort()
	all_values = tuple(all_values)
	# simulated_mncp_list = [None] * num_simulations
	for dummy_index in xrange(num_simulations):
		random_reg_vals = [None] * num_regulated
		random_unreg_vals = list(all_values)
		for index in xrange(num_regulated):
			random_reg_vals[index] = random_unreg_vals.pop(random.randrange(len(random_unreg_vals)))
		simulated_ROCAUC = old_ROCAUC(random_reg_vals, random_unreg_vals, higher_is_better)
		if simulated_ROCAUC >= real_ROCAUC:
			ge_ROCAUC += 1
	return real_ROCAUC, ge_ROCAUC

##--------------------------------------------------------------------
def old_ROCAUC_pvalue(regulated_values, unregulated_values, num_simulations, higher_is_better=True):
	regulated_values = tuple(regulated_values)
	unregulated_values = tuple(unregulated_values)
	num_reg = len(regulated_values)
	num_unreg = len(unregulated_values)
	num_simulations = int(num_simulations)
	real_ROCAUC = old_ROCAUC(list(regulated_values),list(unregulated_values), higher_is_better)
	ge_ROCAUC = 0
	all_values = list(regulated_values + unregulated_values)
	all_values.sort()
	all_values = tuple(all_values)
	# simulated_mncp_list = [None] * num_simulations
	normalizing_factor = (num_reg * num_unreg)
	random_reg_vals = [None] * num_reg
	for dummy_index in xrange(num_simulations):
		# random_reg_vals = [None] * num_reg
		random_unreg_vals = list(all_values)
		for index in xrange(num_reg):
			random_reg_vals[index] = random_unreg_vals.pop(random.randrange(len(random_unreg_vals)))
##============================================================
##		simulated_ROCAUC = ROCAUC(random_reg_vals, random_unreg_vals, higher_is_better)
##============================================================
		unregulated_val_list = random_unreg_vals
		regulated_val_list = random_reg_vals
		total_area = 0
		## left_index=0                                                                           # SPEEDUP? NO!!
		## regulated_val_list.sort()                                                              # SPEEDUP? NO!!
		for reg_value in regulated_val_list:
			## left_index  = bisect.bisect_left(unregulated_val_list, reg_value, lo=left_index)  # SPEEDUP?  NO!!
			left_index  = bisect.bisect_left(unregulated_val_list, reg_value)
			## right_index = bisect.bisect_right(unregulated_val_list, reg_value, lo=left_index)
			right_index = bisect.bisect_right(unregulated_val_list, reg_value, left_index)
			if higher_is_better: better = left_index
			else: 	             better = num_unreg - right_index
			same = right_index - left_index
			total_area += better + (same/2)
		## simulated_ROCAUC = total_area/(num_reg * num_unreg)
		simulated_ROCAUC = total_area/normalizing_factor
##============================================================
		if simulated_ROCAUC >= real_ROCAUC:
			ge_ROCAUC += 1
	return real_ROCAUC, ge_ROCAUC
##--------------------------------------------------------------------

##--------------------------------------------------------------------
def test_ROCAUC_pvalue(regulated_values, unregulated_values, num_simulations, higher_is_better=True):
	regulated_values = tuple(regulated_values)
	unregulated_values = tuple(unregulated_values)
	num_reg = len(regulated_values)
	num_unreg = len(unregulated_values)
	num_simulations = int(num_simulations)
	real_ROCAUC = old_ROCAUC(list(regulated_values),list(unregulated_values), higher_is_better)
	ge_ROCAUC = 0
	all_values = list(regulated_values + unregulated_values)
	all_values.sort()
	all_values = tuple(all_values)
	# simulated_mncp_list = [None] * num_simulations
	normalizing_factor = (num_reg * num_unreg)
	random_reg_vals = [None] * num_reg
	for dummy_index in xrange(num_simulations):
		# random_reg_vals = [None] * num_reg
		random_unreg_vals = list(all_values)
		random_indices = random.sample(range(len(all_values)), num_reg)
		random_indices.sort()
		random_indices.reverse()
		for index in xrange(num_reg):
			random_reg_vals[index] = random_unreg_vals.pop(random_indices[index])
##============================================================
##		simulated_ROCAUC = ROCAUC(random_reg_vals, random_unreg_vals, higher_is_better)
##============================================================
		unregulated_val_list = random_unreg_vals
		regulated_val_list = random_reg_vals
		total_area = 0
		## left_index=0                                                                           # SPEEDUP? NO!!
		## regulated_val_list.sort()                                                              # SPEEDUP? NO!!
		for reg_value in regulated_val_list:
			## left_index  = bisect.bisect_left(unregulated_val_list, reg_value, lo=left_index)  # SPEEDUP?  NO!!
			left_index  = bisect.bisect_left(unregulated_val_list, reg_value)
			## right_index = bisect.bisect_right(unregulated_val_list, reg_value, lo=left_index)
			right_index = bisect.bisect_right(unregulated_val_list, reg_value, left_index)
			if higher_is_better: better = left_index
			else: 	             better = num_unreg - right_index
			same = right_index - left_index
			total_area += better + (same/2)
		## simulated_ROCAUC = total_area/(num_reg * num_unreg)
		simulated_ROCAUC = total_area/normalizing_factor
##============================================================
		if simulated_ROCAUC >= real_ROCAUC:
			ge_ROCAUC += 1
	return real_ROCAUC, ge_ROCAUC
##--------------------------------------------------------------------
##--------------------------------------------------------------------
##--------------------------------------------------------------------
##--------------------------------------------------------------------
## There are two counts that must be determined for each member of the
## "regulated set":
##        1. The number of unregulated genes of better rank (num_better).
##        2. The number of unregulated genes of equal rank  (num_equal).
##
##
## There are a couple of ways I think I might be able to improve the speed:
##    1. Use a dict keyed by ranks to determine how many member of the population
##       have the same value (or same rank, since this is the same thing), the
##       "num_equal" value can be calculated from this as described below.
##
##    2. Use best-tie ranks to determine the number of members of the population
##       with better ranks, the "num_better" value can be calculated from this
##       as described below.
##
##   The ranks will be determined for regulated values and unregulated values
##   combined, so some careful counting and subtraction will be required.  An
##   example is the case where the best-tie ranks of the "regulated set"
##   (among all values) are 4, 7, 10, 10, 14 and the relavant part of the
##   same_count dict is {4:1, 7:4, 10:3, 14:1}.
##
##  The "num_equal" values are 4=0, 7=3, 10=1, 14=0, because we have to subtract
##  out the contributions of the regulated set values to the counts.
##  Similarly, the "num_better" values are: 3,5,7,7,9.
##--------------------------------------------------------------------
class PopMember(object):
##	def __init__(self, value,reg_or_unreg=''):
	def __init__(self, value):
		self.value = value
## 		self.membership = reg_or_unreg
		self.rank=None
	def __cmp__(self, other):
		return cmp(self.value, other.value)
	def __str__(self):
		return str(self.value) + self.membership
	def __repr__(self):
		return repr(self.value) + self.membership

##--------------------------------------------------------------------
# Its real simple to code the ROC AUC.  Here are the steps:
# 
# 1: For all regulated genes:
#      Area_slice = (# unregulated genes of lower rank order) + 0.5* 
# (#unregulated genes
# of rank order equal to the regulated gene)
# 
# 2: Area = sum(Area_slices)
# 3: Normalized AUC  = area / ((total regulated genes) * (total unregulated genes))
# have fun!
##-----------------------------------------------------------------
##
## for feature in regulated_features:
##     area = #unregulated_features with worse value than feature +
##            1/2 x #unregulated_features with same value as feature
## W(wilcoxon) = ROCAUC = area/(number_regulated x number_unregulated)
##-----------------------------------------------------------------

## REGULATED = 'reg'
## UNREGULATED = 'unreg'

def gold_standard_ROCAUC(regulated_list, unregulated_list, higher_is_better=1):
	if higher_is_better:
		better = operator.gt
	else:
		better = operator.lt

	area = 0
	for reg_val in regulated_list:
		number_better = 0
		number_equal = 0
		for unreg_val in unregulated_list:
			if better(reg_val, unreg_val):
				number_better += 1
			elif reg_val == unreg_val:
				number_equal += 1
		area += number_better + number_equal/2
	return area/(len(regulated_list) * len(unregulated_list))


def bad_ROCAUC(regulated_val_list, unregulated_val_list, higher_is_better=1):
	regulated_population = [PopMember(val) for val in regulated_val_list]
	unregulated_population = [PopMember(val) for val in unregulated_val_list]
	all_values = regulated_population + unregulated_population

## 	best_tie_rank_members(all_values, higher_is_better)
##	print 'Regulated Ranks:', [member.rank for member in regulated_population]
##	##-------------------
##	rank_counts = {}
##	for member in all_values:
##		rank_counts[member.rank] = 1 + rank_counts.get(member.rank,0)

##	num_equal_dict = {}
##	for member in regulated_population:
##		num_equal_dict[member.rank] = num_equal_dict.get(member.rank,
##														 rank_counts[member.rank]) - 1
##	##-------------------
##	num_worse_dict = {}
##	reg_sorted_by_rank = [(member.rank, member) for member in regulated_population]
##	reg_sorted_by_rank.sort()
##	reg_sorted_by_rank = [member for value, member in reg_sorted_by_rank]
##	count=0
##	for member in reg_sorted_by_rank:
##		count += 1
##		if member.rank not in num_better_dict:
##			num_better_dict[member.rank] = member.rank - count
##	##-------------------
##	print 'num_better_dict:', num_better_dict
##	print 'num_equal_dict:', num_equal_dict
##	##-------------------
##	area = 0
##	for member in reg_sorted_by_rank:
##		area += num_better_dict[member.rank] + num_equal_dict[member.rank]/2
##	return area/(len(regulated_population) * len(unregulated_population))

 	num_worse_equal_members(all_values, higher_is_better)
##	reg_sorted_by_worse = [(member.num_worse, member) for member in regulated_population]
##	reg_sorted_by_worse.sort()
##	reg_sorted_by_worse = [member for worse, member in reg_sorted_by_worse]

	##------------------------------------------------------------------
##	print '\n(same,worse,value):', ';'.join([''.join(('(',
##													str(member.num_same),',',
##													str(member.num_worse),',',
##													str(member.value),')'))
##										   for member in regulated_population])
	##------------------------------------------------------------------
	# need to substract out regulated members from counts of num_same and num_worse
	worse_dict = {}
	for member in regulated_population:
		worse_dict.setdefault(member.num_worse, []).append(member)


	worse_keys = worse_dict.keys()
	worse_keys.sort()

	worse_regulated_count = 0
	for key in worse_keys:
		num_with_same_worse = len(worse_dict[key])
		for member in worse_dict[key] :
			## print key, member.value, member.num_same, member.num_worse
			member.num_same = member.num_same - num_with_same_worse
			member.num_worse = member.num_worse - worse_regulated_count
		worse_regulated_count += num_with_same_worse
	##------------------------------------------------------------------
##	print '\n(same,worse,value):', ';'.join([''.join(('(',
##													str(member.num_same),',',
##													str(member.num_worse),',',
##													str(member.value),')'))
##										   for member in regulated_population])
	##------------------------------------------------------------------
	area = reduce(operator.add, [member.num_worse + (member.num_same/2)
								 for member in regulated_population])
	return area/(len(regulated_population) * len(unregulated_population))

##--------------------------------------------------------------------
def ROCAUC_pvalue(regulated_values, unregulated_values, num_simulations, higher_is_better=True):
##	rocauc_scores = Numeric.zeros((num_simulations,), Numeric.Float)##FOR_PLOT
	num_reg = len(regulated_values)
	num_unreg = len(unregulated_values)
	num_simulations = int(num_simulations)
	normalizing_factor = (num_reg * num_unreg)

	real_ROCAUC = ROCAUC(regulated_values,unregulated_values,higher_is_better)
	#---------------------
	regulated_population = [PopMember(val) for val in regulated_values]
	unregulated_population = [PopMember(val) for val in unregulated_values]
	all_values = regulated_population + unregulated_population
	best_tie_rank_members(all_values, higher_is_better)
	##-------------------
	rank_counts = {}
	for member in all_values:
		rank_counts[member.rank] = 1 + rank_counts.get(member.rank,0)

	ge_ROCAUC = 0
	for sim_index in xrange(num_simulations):
		random_reg_set = random.sample(all_values, num_reg)
		##-------------------
		random_reg_sorted_by_rank = [(member.rank, member) for member in random_reg_set]
		random_reg_sorted_by_rank.sort()
		random_reg_sorted_by_rank = [member for value, member in random_reg_sorted_by_rank]
		##-------------------
		count=0
		num_better_dict = {}
		num_equal_dict = {}
		for member in random_reg_sorted_by_rank:
			num_equal_dict[member.rank] = num_equal_dict.get(
				member.rank,rank_counts[member.rank])-1
		##-------------------
			count += 1
			if member.rank not in num_better_dict:
				num_better_dict[member.rank] = member.rank - count
		##-------------------
		total_area = 0
		for member in random_reg_sorted_by_rank:
			total_area += num_better_dict[member.rank] + num_equal_dict[member.rank]/2
		simulated_ROCAUC = total_area/normalizing_factor
##============================================================
		if simulated_ROCAUC >= real_ROCAUC:
			ge_ROCAUC += 1
##-----------------------
##		rocauc_scores[sim_index] = simulated_ROCAUC ##FOR_PLOT
##	rocauc_plot_handle = file('/tmp/rocauc_for_plotting','w') ##FOR_PLOT
##	rocauc_plot_handle.write(''.join([repr(score)+'\n' for score in rocauc_scores])) ##FOR_PLOT
##	rocauc_plot_handle.close() ##FOR_PLOT
##-----------------------
	return real_ROCAUC, ge_ROCAUC
##--------------------------------------------------------------------
## END def ROCAUC_pvalue
	
def slower_standard_test_faster_ROCAUC_pvalue(regulated_values, unregulated_values, num_simulations, higher_is_better=True):
	num_reg = len(regulated_values)
	num_unreg = len(unregulated_values)
	num_simulations = int(num_simulations)
	normalizing_factor = (num_reg * num_unreg)

	real_ROCAUC = ROCAUC(regulated_values,unregulated_values,higher_is_better)
	#---------------------
	regulated_population = [PopMember(val) for val in regulated_values]
	unregulated_population = [PopMember(val) for val in unregulated_values]
	all_values = regulated_population + unregulated_population
	best_tie_rank_members(all_values, higher_is_better)
	##-------------------
	rank_counts = {}
	for member in all_values:
		rank_counts[member.rank] = 1 + rank_counts.get(member.rank,0)

	ge_ROCAUC = 0
	for sim_count in xrange(num_simulations):
		random_indices = random.sample(range(len(all_values)), num_reg)
		sim_reg = [all_values[index].value for index in random_indices]
		sim_unreg = [all_values[index].value for index in range(len(all_values))
					 if index not in random_indices]
		simulated_ROCAUC = ROCAUC(sim_reg, sim_unreg, higher_is_better)
##============================================================
		if simulated_ROCAUC >= real_ROCAUC:
			ge_ROCAUC += 1
	return real_ROCAUC, ge_ROCAUC
##--------------------------------------------------------------------
## END def slower_standard_test_faster_ROCAUC_pvalue
	
##--------------------------------------------------------------------
def average_tie_rank_members(population, higher_is_better=1):
	"""
	Members with tied values receive a rank which is the average rank of the tied values.  In other words, if higher is better, and the input values are:
	[10.7,8.2,6.3,6.3,6.3,4.1,3.7,3.7,3.3,2.4] the respective ranks will be
	[1,2,4,4,4,6,7.5,7.5,9,10]
	"""

	## Alters population (by sorting, and possibly reversing), and of course,
	##        alters members of population, by assiging ranks.
	## Has no return value, to remind user that it alters the input itself.
	population.sort()
	if higher_is_better:
		population.reverse()
	# rank = count = 1
	cur_rank = 1
	cur_val = None
	start_tie_index = None
	cur_tie = False
	for index in xrange(len(population)):
		member = population[index]
		if member.value == cur_val and (not cur_tie):
			# this is the second value in a run of ties
			start_tie_index = index - 1
			## print 'TIE', start_tie_index
			cur_tie = True
		elif cur_tie and (member.value <> cur_val):
			## this is the first value after a run of ties
			## num_ties = (index - start_tie_index) + 1
			end_tie_index = index-1
			## print 'END TIE', end_tie_index
##			ranks_of_ties = range(population[start_tie_index].rank,
##								  population[index-1].rank + 1)
			num_ties = (end_tie_index - start_tie_index) + 1
			sum_of_tie_ranks = reduce(operator.add,
									  [x.rank for x in
									   population[start_tie_index:end_tie_index+1]])
			tie_rank = sum_of_tie_ranks/num_ties

			for ties_index in range(start_tie_index, end_tie_index+1):
				population[ties_index].rank = tie_rank
			start_tie_index = None
			cur_tie = False

		member.rank = index + 1
		cur_val = member.value

	if cur_tie:
	##--------------------------------------------------
	## Need to handle the special case where the last member in the list is
	## involved in a tie.
	## This is especially important with filtered data, where worst ~4000
	## features will all have ranks of ZERO
	##--------------------------------------------------
		## The last value in the list is involved in a tie
		end_tie_index = index
		## print 'END TIE', end_tie_index
		num_ties = (end_tie_index - start_tie_index) + 1
		sum_of_tie_ranks = reduce(operator.add,
								  [x.rank for x in
								   population[start_tie_index:end_tie_index+1]])
		tie_rank = sum_of_tie_ranks/num_ties

		##-----------------------------------------------------
		## for ties_index in range(start_tie_index, index)
		##-----------------------------------------------------
		## The above line introduced a new bug, because it failed to
		## reset the rank of the last value in the list (when that value was in
		## a tie).  Since range(start,stop) excludes stop, 
		## 
		## This happened to result in a difference in output between python2.2.3
		## and python2.3, because of differences in sorting.  The stop for the
		## range above should be index+1 or end_tie_index+1 to include the last
		## member of the list.
		##
		## First some background:
		## Since the U value is the min of Ua and Ub (the U values of the two
		## sets), if the size of the sets is signicantly different, the min U value
		## tends to be the U value of the larger set.  In this case, the
		## regulated set is much larger than the unregulated set, and therefore
		## its U value is the min of the two.
		##
		## In python2.3, RME1 (the regulated feature that has a filtered score
		## of 0) was the last member in the population (the member that didn't
		## get its rank reset to the average rank of the tie), but because the
		## Ureg isn't used as the total U value, this mistake didn't show up.
		##
		## In python2.2.3, RME1 didn't happend to be sorted as the last member
		## of the population, so the member whose rank wasn't reset to the
		## average was in the unregulated group, and its incorrect rank effected
		## the Uunreg, which caused the Utotal to be wrong.
		##
		## I expect that this difference is a result of the new stable sort
		## algorithm used for list.sort() in python2.3
		##-----------------------------------------------------
		for ties_index in range(start_tie_index, end_tie_index+1): 
			population[ties_index].rank = tie_rank
		start_tie_index = None
		cur_tie = False
	

def num_worse_equal_members(population, higher_is_better=1):
	## Alters population (by sorting, and possibly reversing), and of course,
	##        alters members of population, by assiging ranks.
	## Has no return value, to remind user that it alters the input itself.

	## num_worse = number of members with worse values than this member
	## num_same = number of members with same valuea as this member (i.e. if this is the only member with this value, num_same is 1)
	population.sort()
	if not higher_is_better:
		population.reverse()

	start_equal_index = cur_val = num_same = num_worse = count = None
	cur_tie = False
	for index in xrange(len(population)):
		member = population[index]
		if member.value == cur_val and (not cur_tie):
			# this is the second value in a run of ties
			start_tie_index = index - 1
			cur_tie = True
		elif cur_tie and (member.value <> cur_val):
			## this is the first value after a run of ties
			## need to go back and set correct values for tied members
			end_tie_index = index-1
			num_ties = (end_tie_index - start_tie_index) + 1
			num_worse = population[start_tie_index].num_worse
			for ties_index in range(start_tie_index, index):
				population[ties_index].num_same = num_ties
				population[ties_index].num_worse = num_worse
			start_tie_index = end_tie_index = num_worse = num_ties = None
			cur_tie = False

		member.num_worse = index
		member.num_same = 1
		cur_val = member.value

def best_tie_rank_members(population, higher_is_better=1):
	## Alters population (by sorting, and possibly reversing), and of course,
	##        alters members of population, by assiging ranks.
	## Has no return value, to remind user that it alters the input itself.
	population.sort()
	if higher_is_better:
		population.reverse()
	rank = count = 1
	cur_val = None
	for member in population:
		if member.value <> cur_val:
			rank = count
			cur_val = member.value
		member.rank = rank
		count += 1

def worst_tie_rank_members(population, higher_is_better=1):
	## Alters population (by sorting, and possibly reversing), and of course,
	##        alters members of population, by setting its rank attribute.
	## Has no return value, to remind user that it alters the input itself.
	population.sort()
	if not higher_is_better:
		population.reverse()
	rank = count = len(population)
	cur_val = None
	for member in population:
		if member.value <> cur_val:
			rank = count
			cur_val = member.value
		member.rank = rank
		count -= 1

def check_rank_members(count):
	max_int = int(count * 0.9)
	pop = [PopMember(random.randrange(max_int)) for x in range(count)]
	print 'Best-Tie Ranks:'
	print 'value'.ljust(10) + 'rank'.ljust(10)
	best_tie_rank_members(pop)
	for member in pop:
		print str(member.value).ljust(10) + str(member.rank).ljust(10)
	print 'Worst-Tie Ranks:'
	print 'value'.ljust(10) + 'rank'.ljust(10)
	worst_tie_rank_members(pop)
	pop.reverse()
	for member in pop:
		print str(member.value).ljust(10) + str(member.rank).ljust(10)
	print 'Average-Tie Ranks:'
	print 'value'.ljust(10) + 'rank'.ljust(10)
	average_tie_rank_members(pop)
	for member in pop:
		print str(member.value).ljust(10) + str(member.rank).ljust(10)

#@-body
#@-node:4::ROCAUC_pvalues
#@+node:5::rank
#@+body

def add_worst_tie_ranks(value_list,  higher_is_better=0):
	value_list.sort()
	if not higher_is_better:
		value_list.reverse()
	tuple_rank_list = []
	rank = count = len(value_list)
	cur_val = None
	for value in value_list:
		if value <> cur_val:
			rank = count
			cur_val = value
		tuple_rank_list.append((value, rank))
		count -= 1
	return tuple_rank_list

def tuples_add_worst_tie_ranks(tuple_list,  higher_is_better=0):
	tuple_list.sort()
	if not higher_is_better:
		tuple_list.reverse()
	tuple_rank_list = []
	rank = count = len(tuple_list)
	cur_val = 1-sys.maxint
	for cur_tup in tuple_list:
		if cur_tup[0] <> cur_val:
			rank = count
			cur_val = cur_tup[0]
		tuple_rank_list.append((cur_tup + (rank,)))
		count -= 1
	return tuple_rank_list

#@-body
#@-node:5::rank
#@+node:6::main_compare
#@+body

def main_compare_with_old():
	from MNCP import MNCPScore
	from ROC_AUC import NormalizedROCAUC, ROC_auc
	def usage():
		print >>sys.stderr, 'usage:', sys.argv[0], 'REGULATED_COUNT TOTAL_COUNT REPEATS'
		print >>sys.stderr, '\n\tREGULATED_COUNT: Number of "features" that are regulated'
		print >>sys.stderr, '\tTOTAL_COUNT: Total number of "features"'
		print >>sys.stderr, '\tREPEATS: Number of times to run the test'
		
	if len(sys.argv) <> 4:
		usage()
		sys.exit(2)
	reg_num = int(sys.argv[1])
	total_num = int(sys.argv[2])
	repeats = int(sys.argv[3])
##----------------------------------------------------------------
	failures = 0
	if reg_num >= total_num:
		raise ValueError, 'TOTAL_COUNT must be larger than REGULATED_COUNT'
	max = int(total_num/1.1) ## want to ensure that there are some duplicate values
	## max = int(total_num * 100000)
	rocauc_function_list = [NormalizedROCAUC, slower_ROCAUC, old_ROCAUC, bad_ROCAUC]
	for dummy_repeat_index in xrange(repeats):
		all_values=[random.randint(0,max) for dummy_index in xrange(total_num)]
		regulated_values = all_values[:reg_num]
		unregulated_values = all_values[reg_num:]
		# old_mncp = MNCPScore(regulated_values,unregulated_values)
##----------------------------------------------------------
		for higher_is_better in (0,1):
			old_mncp = MNCPScore(regulated_values,unregulated_values, higher_is_better)
			new_mncp = MNCP(regulated_values, unregulated_values, higher_is_better)
			if old_mncp <> new_mncp:
				failures += 1
				print 'FAIL!! (difference', old_mncp-new_mncp,')old_mncp:', \
					  `old_mncp`, 'new_mncp:', `new_mncp`
	##----------------------------------------------------------
			rocauc_results = [roc_func(regulated_values,unregulated_values, higher_is_better) for
							  roc_func in rocauc_function_list]
			gold_rocauc = rocauc_results[0]
			for func, result in zip (rocauc_function_list, rocauc_results):
				print func.__name__, result
			for index in range(1,len(rocauc_results)):
				cur_result = rocauc_results[index]
				if gold_rocauc <> cur_result:
					failures += 1
					print 'FAIL!! (difference', `gold_rocauc-cur_result`,\
						  ')gold_rocauc:', `gold_rocauc`, \
						  rocauc_function_list[index].__name__, `cur_result`
	print 'FAILURES:', failures

#@-body
#@-node:6::main_compare
#@+node:7::main_pvalue
#@+body
def main_pvalues():
	def usage():
		print >>sys.stderr, 'usage:', sys.argv[0], 'REGULATED_COUNT TOTAL_COUNT NUM_SIMULATIONS'
		print >>sys.stderr, '\n\tREGULATED_COUNT: Number of "features" that are regulated'
		print >>sys.stderr, '\tTOTAL_COUNT: Total number of "features"'
		print >>sys.stderr, '\tNUM_SIMULATIONS: Number of simulations to run to determine the pvalue'
	if len(sys.argv) <> 4:
		usage()
		sys.exit(2)
	reg_num = int(sys.argv[1])
	total_num = int(sys.argv[2])
	num_simulations = int(float(sys.argv[3]))
##----------------------------------------------------------------
	if reg_num >= total_num:
		raise ValueError, 'TOTAL_COUNT must be larger than REGULATED_COUNT'
	max = int(total_num/1.1) ## want to ensure that there are some duplicate values
	## max = int(total_num * 100000) 
	all_values=[random.randint(0,max) for dummy_index in xrange(total_num)]
	regulated_values = all_values[:reg_num]
	unregulated_values = all_values[reg_num:]
##----------------------------------------------------------
##	rocauc_function_list = [ROCAUC_pvalue, test_ROCAUC_pvalue,
##							test_faster_ROCAUC_pvalue, slower_standard_test_faster_ROCAUC_pvalue]
##	rocauc_function_list = [ROCAUC_pvalue, test_ROCAUC_pvalue,
##							test_faster_ROCAUC_pvalue]
	rocauc_function_list = [test_ROCAUC_pvalue,	ROCAUC_pvalue]
	# roc_auc_result = slower_ROCAUC_pvalue(regulated_values, unregulated_values, num_simulations, higher_is_better=1)
	## roc_auc_result = ROCAUC_pvalue(regulated_values, unregulated_values, num_simulations, higher_is_better=1)
	rocauc_return_val = None
	random_state = random.getstate()
	for rocauc_func in rocauc_function_list:
		random.setstate(random_state)
		roc_auc_result = rocauc_func(list(regulated_values), list(unregulated_values),
									 num_simulations, higher_is_better=1)
		print rocauc_func.__name__, roc_auc_result
		if not rocauc_return_val:
			rocauc_return_val = roc_auc_result
	mncp_result = MNCP_pvalue(regulated_values, unregulated_values, num_simulations, higher_is_better=1)
	print 'MNCP:', mncp_result
	## return roc_auc_result, mncp_result
	return rocauc_return_val, mncp_result

#@-body
#@-node:7::main_pvalue
#@+node:8::self_test
#@+body
def self_test():
	print 'sys.argv', sys.argv
	saved_argv = sys.argv[:]
	sys.argv = [sys.argv[0], '7', '6727', '1e3']
	print 'sys.argv', sys.argv
	random_state = random.getstate()
	my_seed=1060197767.141
	random.seed(my_seed)
	ROCAUC_test_value = (0.34399447278911566, 919)
	MNCP_test_value = (0.86032430125139925, 920)
	roc_auc_result, mncp_result = main_pvalues()
	if ROCAUC_test_value<>roc_auc_result or MNCP_test_value<>mncp_result:
		print >>sys.stderr, 'Test values are not correct!'
	else:
		print 'PASSED self test'
	random.setstate(random_state)
	sys.argv = saved_argv
	print 'sys.argv', sys.argv
	sys.exit(1)


def test_ROCAUC():
	import re
	handle = file(sys.argv[1])
	lists = {}
	for line in handle:
		if re.match('NAME', line):
			cur_name = line.split()[1]
			cur_list = []
			lists[cur_name] = cur_list
		elif re.match('^\s*$', line):
			continue
		else:
			cur_list.append(float(line.split()[2]))

	reg = lists['regulated']
	unreg = lists['unregulated']
	for func in [slower_ROCAUC, old_ROCAUC, bad_ROCAUC]:
		for higher in ((True, False)):
			print func.__name__, 'higher is better', higher, func(reg, unreg, higher)


if __name__ == '__main__':
	reg_num=20
	unreg_num=6000
	min_int=0
	max_int = 4000
	reg1 = [random.randint(min_int,max_int) for x in range(reg_num)] 
	reg2 = [random.randint(min_int,max_int) for x in range(reg_num)] 
	reg3 = [random.randint(min_int,max_int) for x in range(reg_num)] 
	reg4 = [random.randint(min_int,max_int) for x in range(reg_num)] 
	reg5 = [random.randint(min_int,max_int) for x in range(reg_num)] 
	reg6 = [random.randint(min_int,max_int) for x in range(reg_num)] 

	unreg1 = [random.randint(min_int,max_int) for x in range(unreg_num)] 
	unreg2 = [random.randint(min_int,max_int) for x in range(unreg_num)] 
	unreg3 = [random.randint(min_int,max_int) for x in range(unreg_num)] 
	unreg4 = [random.randint(min_int,max_int) for x in range(unreg_num)] 
	unreg5 = [random.randint(min_int,max_int) for x in range(unreg_num)] 
	unreg6 = [random.randint(min_int,max_int) for x in range(unreg_num)] 

	global_tuple_lists = [
		[range(10), range(10,100)], 
		[range(90,100), range(90)], 
		[range(5,100,10), range(100)], 
		[[1]*10, range(10,100)], 
		[[100]*10, range(90)], 
		[[100]*10, [100]*10 + range(80)],
		[[100]*10, [100]*20 + range(70)],
		[[100]*10, [100]*30 + range(60)],
		[[100]*10, [100]*40 + range(50)],
		[[100]*10, [100]*50 + range(40)],
		[[100]*10, [100]*60 + range(30)],
		[[100]*10, [100]*70 + range(20)],
		[[100]*10, [100]*80 + range(10)],
		[[100]*10, [100]*90],
		[[1]*10, range(10,100)], 
		[[1]*10, [1]*10 + range(20,100)], 
		[[1]*10, [1]*20 + range(30,100)], 
		[[1]*10, [1]*30 + range(40,100)], 
		[[1]*10, [1]*40 + range(50,100)], 
		[[1]*10, [1]*50 + range(60,100)], 
		[[1]*10, [1]*60 + range(70,100)], 
		[[1]*10, [1]*70 + range(80,100)], 
		[[1]*10, [1]*80 + range(90,100)], 
		[[1]*10, [1]*90], 
		[[50]*10, range(5,95)], 
		[[50]*10, [50]*10 + range(10,90)], 
		[[50]*10, [50]*20 + range(15,85)], 
		[[50]*10, [50]*30 + range(20,80)], 
		[[50]*10, [50]*40 + range(25,75)], 
		[[50]*10, [50]*50 + range(30,70)], 
		[[50]*10, [50]*60 + range(35,65)], 
		[[50]*10, [50]*70 + range(40,60)], 
		[[50]*10, [50]*80 + range(45,55)], 
		[[50]*10, [50]*90],
		[reg1, unreg1],
		[reg2, unreg2],
		[reg3, unreg3],
		[reg4, unreg4],
		[reg5, unreg5],
		[reg6, unreg6],
		]

def run_gold_standard_ROCAUC():
	for reg, unreg in global_tuple_lists:
		gold_standard_ROCAUC(reg, unreg, True)
		gold_standard_ROCAUC(reg, unreg, False)

def run_slower_ROCAUC():
	for reg, unreg in global_tuple_lists:
		slower_ROCAUC(reg, unreg, True)
		slower_ROCAUC(reg, unreg, False)

def run_old_ROCAUC():
	for reg, unreg in global_tuple_lists:
		old_ROCAUC(reg, unreg, True)
		old_ROCAUC(reg, unreg, False)

def run_bad_ROCAUC():
	for reg, unreg in global_tuple_lists:
		bad_ROCAUC(reg, unreg, True)
		bad_ROCAUC(reg, unreg, False)


def test_ROCAUC():
	tuple_lists = [
		[range(10), range(10,100)], 
		[range(90,100), range(90)], 
		[range(5,100,10), range(100)], 
		[[1]*10, range(10,100)], 
		[[100]*10, range(90)], 
		[[100]*10, [100]*10 + range(80)],
		[[100]*10, [100]*20 + range(70)],
		[[100]*10, [100]*30 + range(60)],
		[[100]*10, [100]*40 + range(50)],
		[[100]*10, [100]*50 + range(40)],
		[[100]*10, [100]*60 + range(30)],
		[[100]*10, [100]*70 + range(20)],
		[[100]*10, [100]*80 + range(10)],
		[[100]*10, [100]*90],
		[[1]*10, range(10,100)], 
		[[1]*10, [1]*10 + range(20,100)], 
		[[1]*10, [1]*20 + range(30,100)], 
		[[1]*10, [1]*30 + range(40,100)], 
		[[1]*10, [1]*40 + range(50,100)], 
		[[1]*10, [1]*50 + range(60,100)], 
		[[1]*10, [1]*60 + range(70,100)], 
		[[1]*10, [1]*70 + range(80,100)], 
		[[1]*10, [1]*80 + range(90,100)], 
		[[1]*10, [1]*90], 
		[[50]*10, range(5,95)], 
		[[50]*10, [50]*10 + range(10,90)], 
		[[50]*10, [50]*20 + range(15,85)], 
		[[50]*10, [50]*30 + range(20,80)], 
		[[50]*10, [50]*40 + range(25,75)], 
		[[50]*10, [50]*50 + range(30,70)], 
		[[50]*10, [50]*60 + range(35,65)], 
		[[50]*10, [50]*70 + range(40,60)], 
		[[50]*10, [50]*80 + range(45,55)], 
		[[50]*10, [50]*90] 
		]

	ROC_function_list = ((slower_ROCAUC, old_ROCAUC, bad_ROCAUC))
	full_ROC_function_list = ((gold_standard_ROCAUC, slower_ROCAUC, old_ROCAUC, bad_ROCAUC))
	
	for higher_is_better in [True, False]:
		print 'higher_is_better:', higher_is_better
		for reg, unreg in tuple_lists:
			print 'Reg:', reg
			print 'Unreg:', unreg

			for cur_func in full_ROC_function_list:
				print cur_func.__name__.ljust(20), cur_func(reg, unreg, higher_is_better)
			print '--------------------------\n'
		
	for higher_is_better in [True, False]:
		print 'higher_is_better:', higher_is_better
		for reg, unreg in global_tuple_lists:
			gold_result = gold_standard_ROCAUC(reg, unreg, higher_is_better)
			
			for cur_func in ROC_function_list:
				cur_result = cur_func(reg, unreg, higher_is_better)
				if cur_result <> gold_result:
					print cur_func.__name__, '(', cur_result, ') differs from gold standard (', gold_result, ')'
					print 'reg:', reg
					print 'unreg:', unreg
					print '--------------------------\n'


def time_test():
	from timeit import Timer
	for cur_func in ['run_gold_standard_ROCAUC', 'run_slower_ROCAUC', 'run_old_ROCAUC', 'run_bad_ROCAUC']:
		t = Timer(cur_func+'()', 'from __main__ import ' + cur_func)
		print cur_func.ljust(15), min(t.repeat(3, 100))


ROCAUC = old_ROCAUC
		
#@-body
#@-node:8::self_test
#@+node:9::if __main__
#@+body
if __name__ == '__main__':
	try:
		import psyco
	except ImportError:
		print >>sys.stderr, 'Module "psyco" not available'
	else:
		print >>sys.stderr, 'USING "psyco"!!'
		psyco.profile()

	#--------------------------------------------------------
	# main_compare_with_old()
	test_ROCAUC()
	time_test()
	#--------------------------------------------------------
	
	##self_test()
	## for count in range(1000):
	## 	main_pvalues()
	## test_ROCAUC()

#@-body
#@-node:9::if __main__
#@-others
#@-body
#@-node:0::@file estimate_p_value.py
#@-leo
