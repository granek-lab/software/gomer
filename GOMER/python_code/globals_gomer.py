#! /usr/bin/env python
from __future__ import division
#@+leo
#@+node:0::@file globals_gomer.py
#@+body
#@@first
#@@first
#@@language python

"""
*Description:

Contains global 'constants':
	complementation_mapping - a hash mapping from each base to its complement
	SEQUENCE_ALPHABET - the letters allowed in a sequence	
"""
##import os
##os.environ['PYCHECKER'] = '--allglobals --stdlib'
##import pychecker.checker

##import check_python_version
##check_python_version.CheckVersion()

MINIMUM_FILE_COMPRESSION = 0.60

OUTPUT_NAME_JUST = 10
# OUTPUT_RANK_JUST = 6

#@+others
#@+node:1::complementation_mapping
#@+body
##complementation_mapping = {
##	'a':'t',
##	't':'a',
##	'c':'g',
##	'g':'c',
##	'n':'n',
##	'A':'T',
##	'T':'A',
##	'C':'G',
##	'G':'C',
##	'N':'N'
##	}
complementation_mapping = {
	'A':'T',
	'T':'A',
	'C':'G',
	'G':'C'
	}


#@-body
#@-node:1::complementation_mapping
#@+node:2::SEQUENCE_ALPHABET
#@+body
# SEQUENCE_ALPHABET = 'acgtnACGTN'
SEQUENCE_ALPHABET = 'ACGT'
NULL_SYMBOL = 'X'
NO_INFO_SYMBOL = 'N'
#@+doc
# 
#         A --> adenosine           M --> A C (amino)
#         C --> cytidine            S --> G C (strong)
#         G --> guanine             W --> A T (weak)
#         T --> thymidine           B --> G T C
#         U --> uridine             D --> G A T
#         R --> G A (purine)        H --> A C T
#         Y --> T C (pyrimidine)    V --> G C A
#         K --> G T (keto)          N --> A G C T (any)
#                                   -  gap of indeterminate length

#@-doc
#@-body
#@-node:2::SEQUENCE_ALPHABET
#@+node:3::ORGANISM_LIST
#@+body
ORGANISM_LIST = tuple(['Saccharomyces cerevisiae'])
#@-body
#@-node:3::ORGANISM_LIST
#@+node:4::STRAND
#@+body
PLUS_STRAND = FOR = FORWARD = '+'
MINUS_STRAND = REV = REVCOMP = REVERSE_COMPLEMENT = '-'
#@-body
#@-node:4::STRAND
#@+node:5::CACHE TABLE FILENAME
#@+body
CACHE_TABLE_FILENAME = 'cache_table'

#@-body
#@-node:5::CACHE TABLE FILENAME
#@+node:6::CONFIG_FILENAME
#@+body
CONFIG_FILENAME = 'gomer_config'



## Config Sections and Options:
KAPPAS_SECTION='kappas'
COORD_REGION_OPT='coordinate_regulatory_region'
COORD_REGION_PARAM_OPT='coordinate_regulatory_region_params'
SEQ_REGION_OPT='sequence_regulatory_region'
SEQ_REGION_PARAM_OPT='sequence_regulatory_region_params'
REG_REGION_OPT='regulatory_region'
REG_REGION_PARAM_OPT='regulatory_region_params'
##--------------------------------------------------
DIRECTORIES_SECTION='directories'
GOMER_HOME_OPT='gomer_home'
FAST_CACHE_OPT='fast_cache'
KAPPA_DIRECTORY_OPT='kappa_directory'
##--------------------------------------------------
PARAM_SECTION='parameters'
FEATURES_OPT='feature_types'
##--------------------------------------------------

#@-body
#@-node:6::CONFIG_FILENAME
#@-others


#@-body
#@-node:0::@file globals_gomer.py
#@-leo
