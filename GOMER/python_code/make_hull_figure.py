#! /usr/bin/env python

from __future__ import division
from xmgrace_interface import grace_no_safe as grace

import re
import sys
import glob

from grace_plot import GracePlot

hull_segment_label_re = re.compile('^## Best Score Hull Segment: (.*)\s*$')
all_points_label_re = re.compile('^## All Points: (.*)\s*$')
number = '([0-9\.\-eE]+)'
# data_line_re = re.compile('^' + number + '\s+' + number + '\s+(".*")\s*$')
data_line_re = re.compile('^' + number + '\s+' + number + '\s+(".*")\s*$')
empty_re = re.compile('^\s*$')

def main():

	filehandle_list = []
	for cur_glob in sys.argv[1:]:
		for filename in glob.glob(cur_glob):
			filehandle = file(filename)
			filehandle_list.append(filehandle)


	hull_point_hash, all_point_hash = parse_files(filehandle_list)
	## print hull_point_hash
	## print all_point_hash
	plot = GracePlot()
	make_plot(plot, hull_point_hash, all_point_hash)

def make_plot(plot, hull_point_hash, all_point_hash):
	all_names = hull_point_hash.keys() + all_point_hash.keys()
	unique_names = dict([(name,None) for name in all_names]).keys()

	color_number = 1
	for name in unique_names:
		if name in hull_point_hash:
			plot.AddDataSetPairsString(hull_point_hash[name], name=name, color=str(color_number))
		if name in all_point_hash:
			plot.AddDataSetPairsString(all_point_hash[name], name=name, color=str(color_number), symbol='1')
			
	
def parse_files(filehandle_list):
	hull_point_hash = {}
	all_point_hash = {}

	for filehandle in filehandle_list:
		print 'Parsing:', filehandle.name
		cur_list = None
		for line in filehandle:
			if empty_re.match(line):
				continue
			elif hull_segment_label_re.match(line):
				cur_list = []
				points_name = hull_segment_label_re.match(line).group(1)
				hull_point_hash[points_name] = cur_list
			elif all_points_label_re.match(line):
				cur_list = []
				points_name = all_points_label_re.match(line).group(1)
				hull_point_hash[points_name] = cur_list
			elif data_line_re.search(line):
				data_line_match = data_line_re.search(line)
				x = data_line_match.group(1)
				y = data_line_match.group(2)
				string = data_line_match.group(3)
				cur_list.append((x,y,string))
			else:
				raise "I'm not very hapy with this line:\n" + line

	return hull_point_hash, all_point_hash

if __name__ == '__main__':
	main()
