#! /usr/bin/env python
from __future__ import division
#@+leo
#@+node:0::@file reverse_complement.py
#@+body
#@@first
#@@first
#@@language python


"""
*Description:

Provides a reverse function and a complement function for use on sequences,
which can be combined to generate reverse complemented sequences.  I believe I
pulled this code from hmmevolver	
"""


#@<<imports>>
#@+node:1::<<imports>>
#@+body
##import os
##os.environ['PYCHECKER'] = '--allglobals --stdlib'
##import pychecker.checker

import sys
import globals_gomer

#@-body
#@-node:1::<<imports>>


#@+others
#@+node:2::complementation_mapping
#@+body

##complementation_mapping = {
##	'a':'t',
##	't':'a',
##	'c':'g',
##	'g':'c',
##	'n':'n',
##	'A':'T',
##	'T':'A',
##	'C':'G',
##	'G':'C',
##	'N':'N'
##	}

complementation_mapping = globals_gomer.complementation_mapping.copy()
complement_keys = complementation_mapping.keys()

for key in complement_keys:
	if key.isupper():
		complementation_mapping[key.lower()] = complementation_mapping[key].lower()
	elif key.islower():
		complementation_mapping[key.upper()] = complementation_mapping[key].upper()
	

#@-body
#@-node:2::complementation_mapping
#@+node:3::reverse
#@+body
def reverse(forward_string):
	list_of_characters = list(forward_string)
	list_of_characters.reverse()
	reversed_string = ''.join(list_of_characters)
	return reversed_string

#@-body
#@-node:3::reverse
#@+node:4::complement
#@+body
def complement(string):
	complemented_string = ''
	for base in string:
		complemented_string += complementation_mapping[base]
	return complemented_string


def reverse_complement(string):
	return complement(reverse(string))



#@-body
#@-node:4::complement
#@-others




#@<<command line>>
#@+node:5::<<command line>>
#@+body
#@<<def run>>
#@+node:1::<<def run>>
#@+body
def run (forward_sequence=None):
	EXPECTED_ARGUMENTS = 1
	if forward_sequence:
		pass
	elif len(sys.argv) == (EXPECTED_ARGUMENTS + 1):
			forward_sequence = sys.argv[1]
	else :
		usage()
		
	reverse_complement_sequence = complement(reverse(forward_sequence))

	print 'Forward:', forward_sequence
	print 'Reverse:', reverse_complement_sequence




#@-body
#@-node:1::<<def run>>


#@<<def usage>>
#@+node:2::<<def usage>>
#@+body
def usage () :
	print '-'*70 + '\n', 'Arguments given (', len(sys.argv), '):', sys.argv,  """
	Produces the reverse complement of the supplied sequence
	1. sequence - Sequence to reverse complement
	""" + '\n' + '-'*70
	sys.exit(1)

#@-body
#@-node:2::<<def usage>>



#@<<if main>>
#@+node:3::<<if main>>
#@+body
if __name__ == '__main__':
	
	#@<<import>>
	#@+node:1::<<import>>
	#@+body
	## import os
	
	#@-body
	#@-node:1::<<import>>

	run()

#@-body
#@-node:3::<<if main>>


#@-body
#@-node:5::<<command line>>
#@-body
#@-node:0::@file reverse_complement.py
#@-leo
