#! /usr/bin/env python
from __future__ import division
#@+leo
#@+node:0::@file cache_file_handler_gomer.py
#@+body
#@@first
#@@first
#@@language python

"""
*Description:

Parses a file containing a flat (simple text) matrix of base probabilities, and
returns a ProbabilityMatrix instance representing the values found in the flat
file
"""

#@<<imports>>
#@+node:1::<<imports>>
#@+body
##import os
##os.environ['PYCHECKER'] = '--allglobals --stdlib'
##import pychecker.checker

##import check_python_version
##check_python_version.CheckVersion()

import os
import re
import md5
from gzip import GzipFile
import tempfile
import shutil
import sys

from cache_file_tokens import *
from parser_gomer import ParseError

##import types
#@-body
#@-node:1::<<imports>>



#@<<regular expressions>>
#@+node:4::<<regular expressions>>
#@+body
#@+doc
# 
# comment_match = re.compile('^\s*' + COMMENT_TOKEN + '(.+)')
# name_match = re.compile('^\s*' + NAME_TOKEN + '\s+(.+)')
# pseudo_temp_match = re.compile('^\s*' + PSEUDO_TEMP_TOKEN + '\s+(.+)')
# base_header_match = re.compile('^[' + BASE_LABEL_STRING + '\s]+$', re.IGNORECASE)
# gap_probabilities_match = re.compile('^[' + GAP_PROB_TOKEN + '\s]+$', re.IGNORECASE)
# empty_line_match = re.compile('^\s*$')
# rough_probabilities_match = re.compile('^\s*\d+[\d\.\-\se]+$')
# 
# #------------------------------------------------
# @doc
# PROB_MATRIX_FILE_TOKEN = '%PROB_MATRIX'
# PROB_MATRIX_MD5_TOKEN = '%PROB_MATRIX_MD5'
# PROB_MATRIX_BEGIN_TOKEN = 'BEGIN PROB MATRIX' + '=' * 40
# PROB_MATRIX_END_TOKEN = 'END PROB MATRIX' + '=' * 40
# CHROMSOME_TOKEN = '$CHROM'
# DATA_START_LINE_TOKEN = '!!DATA BELOW THIS LINE!!'

#@-doc
#@@code

prob_matrix_filename_re = re.compile('^' + PROB_MATRIX_FILE_TOKEN + '\s+(.+)\s*$')
prob_matrix_md5_re = re.compile('^' + PROB_MATRIX_MD5_TOKEN + '\s+(.+)\s*$')
begin_prob_matrix_re = re.compile('^' + PROB_MATRIX_BEGIN_TOKEN + '\s*$')
end_prob_matrix_re = re.compile('^' + PROB_MATRIX_END_TOKEN + '\s*$')
chromosome_re = re.compile('^' + CHROMOSOME_TOKEN + '\s+(.+)\s+(\d+)\s+(\d+)\s+(\d+)\s+(\w+)\s*$')
data_start_line_re = re.compile('^' + DATA_START_LINE_TOKEN)

filter_cutoff_ratio_re = re.compile('^' + FILTER_CUTOFF_RATIO_TOKEN + '\s+(.+)\s*$')
frequency_re = re.compile('^' + FREQUENCY_TOKEN + '\s+(.+)\s+(.+)\s*$')

gzip_filename_re = re.compile('^.*\.gz$')
#@-body
#@-node:4::<<regular expressions>>



#@<<class CacheFileHandler>>
#@+node:2::<<class CacheFileHandler>>
#@+body
class CacheFileHandler(object):

	#@+others
	#@+node:1::__init__
	#@+body
	#@+doc
	# 
	# def __init__(self, filename_or_handle):
	# 	"""
	# 	filename_or_handle : As the name implies, this can be a handle (file 
	# object) or a filename.
	# 	If filename_or_handle was opened with
	# 	"""
	# 	if type(filename_or_handle) == types.FileType:
	# 		cache_filename = filename_or_handle.name
	# 		if filename_or_handle.mode == 'rb':
	# 			self._filehandle = filename_or_handle
	# 		else:
	# 			self._filehandle = file(cache_filename, 'rb')
	# 	elif type(filename_or_handle) == types.StringType:
	# 		cache_filename = filename_or_handle
	# 		self._filehandle = file(cache_filename, 'rb')
	# 	else:
	# 		raise BlahError('ERROR: Unexpected type (' +
	# 						str(type(filename_or_handle)) + ') for parameter "filename_or_handle"')

	#@-doc
	#@@code	
	def __init__(self, cache_filename, fast_cache_directory):
		self._filehandle = file(cache_filename, 'rb')
		if fast_cache_directory:
			tempfile.tempdir=fast_cache_directory
			fast_cache_filehandle = tempfile.TemporaryFile()
			shutil.copyfileobj(self._filehandle, fast_cache_filehandle)
			## fast_cache_filehandle.flush()
			fast_cache_filehandle.seek(0)
			self._filehandle.close()
			self._filehandle = fast_cache_filehandle
		else:
			tempfile.tempdir = os.path.dirname(cache_filename)
			
	##--------------------------------------------------------------
		##Calculate MD5 of cache file
		cache_file_md5 = md5.new()
		read_block_size = 5000000
		read_string = ' '
		while read_string:
			read_string = self._filehandle.read(read_block_size)
			cache_file_md5.update(read_string)
		self._md5 = cache_file_md5.hexdigest()
		## self._filehandle.close()
		self._filehandle.seek(0)
		del read_string
	##--------------------------------------------------------------

	#@+doc
	# 
	# 	2. When a compressed file is to be loaded, CacheFileHandler should 
	# create a TemporaryFile (try to make it in the Cache Directory) and use 
	# shutil.copyfileobj to copy from the GzipFile to an uncompressed 
	# version.  The CacheFileHandler should attach to the TemporaryFile (which 
	# will be deleted at exit).

	#@-doc
	#@@code
	#@+doc
	# 
	# 		gzipped_file_name = new_cache_file_name + gzip_extension
	# 		gzipped_handle = GzipFile(gzipped_file_name, 'wb', self._compression)
	# 		print 'Writing Cache File at Compression Level:', self._compression
	# 		shutil.copyfileobj(self._filehandle, gzipped_handle)

	#@-doc
	#@@code

	##---------------------------------------------------------------
		if gzip_filename_re.search(cache_filename):
			gzipped_handle = GzipFile(fileobj=self._filehandle, mode='rb')
			self._filehandle = tempfile.TemporaryFile()
			print >>sys.stderr, 'Reading Compressed File:', cache_filename
			shutil.copyfileobj(gzipped_handle, self._filehandle)
			gzipped_handle.close()
			del (gzipped_handle)
			self._filehandle.seek(0)
	##---------------------------------------------------------------

	#@+doc
	# 
	# ##---------------------------------------------------------------
	# 	if gzip_filename_re.search(cache_filename):
	# 		gzipped_handle = GzipFile(cache_filename, 'rb')
	# 		if os.path.exists(fast_cache_directory):
	# 			tempfile.tempdir = fast_cache_directory
	# 		else:
	# 			tempfile.tempdir = os.path.dirname(cache_filename)
	# 		self._filehandle = tempfile.TemporaryFile('w+b')
	# 		print >>sys.stderr, 'Reading Compressed File:', cache_filename
	# 		shutil.copyfileobj(gzipped_handle, self._filehandle)
	# 		gzipped_handle.close()
	# 		self._filehandle.seek(0)
	# 	else:
	# 		self._filehandle = file(cache_filename, 'rb')
	# ##---------------------------------------------------------------

	#@-doc
	#@@code		
	##--------------------------------------------------------------
		data_start = 0
		self._chromosome_hash = {}
		self._base_frequency_hash = {}
		self._filter_cutoff_ratio = None
		line = ' '
		while line:
			line = self._filehandle.readline()
			if prob_matrix_filename_re.search(line):
				self._prob_matrix_filename = prob_matrix_filename_re.search(line).group(1)
			if prob_matrix_md5_re.search(line): 
				self._prob_matrix_md5 = prob_matrix_md5_re.search(line).group(1)
			if begin_prob_matrix_re.search(line): 
				while line:
					line = self._filehandle.readline()
					if end_prob_matrix_re.search(line):
						break
				if not line:
					raise CacheFileHandlerError('ERROR: Reached end of file before encountering' +
									' end of probability matrix marker\n')
			if filter_cutoff_ratio_re.search(line):
				self._filter_cutoff_ratio = float(filter_cutoff_ratio_re.search(line).group(1))
			if frequency_re.search(line):
				frequency_match = frequency_re.search(line)
				base = frequency_match.group(1).upper()
				frequency = float(frequency_match.group(2))
				if base in self._base_frequency_hash:
					raise CacheFileHandlerError('ERROR: Duplicate Bases in cache file: '
									+ cache_filename + ' \n')
				else:
					self._base_frequency_hash[base] = frequency
			if chromosome_re.search(line):
				chromosome_match = chromosome_re.search(line)
				label = chromosome_match.group(1)
				num_windows = int(chromosome_match.group(2))
				forward_offset = int(chromosome_match.group(3))
				revcomp_offset = int(chromosome_match.group(4))
				chromosome_md5 = chromosome_match.group(5)
				if label in self._chromosome_hash:
					raise CacheFileHandlerError('ERROR: Duplicate Chromosome labels in cache file: '
									+ cache_filename + ' \n')
				self._chromosome_hash[label] = ChromosomeHandler(label,
																 num_windows,
																 forward_offset,
																 revcomp_offset,
																 chromosome_md5)
			if data_start_line_re.search(line):
				data_start = 1
				break
		
		if not data_start :
			raise CacheFileHandlerError('ERROR: Reached end of file before encountering' +
							' Data marker\n')
		self._data_segment_byte_position = self._filehandle.tell()
		for label in self._chromosome_hash:
			self._chromosome_hash[label].AddDataOffset(self._data_segment_byte_position)
		
		if self._filter_cutoff_ratio == None:
			raise CacheFileHandlerError('ERROR: Reached end of file before encountering' + FILTER_CUTOFF_RATIO_TOKEN + '\n')
		if not self._base_frequency_hash:
			raise CacheFileHandlerError('ERROR: Reached end of file before encountering' + FREQUENCY_TOKEN + '\n')
	#@-body
	#@-node:1::__init__
	#@+node:2::ChromosomeLabels
	#@+body
	def _get_chromosomes(self): return self._chromosome_hash.keys()
	def _set_chromosomes(self, value): raise AttributeError, "Can't set 'Chromosomes' attribute"
	Chromosomes = property(_get_chromosomes, _set_chromosomes)
	
	#@-body
	#@-node:2::ChromosomeLabels
	#@+node:3::Bases
	#@+body
	def _get_bases(self): return self._base_frequency_hash.keys()
	def _set_bases(self, value): raise AttributeError, "Can't set 'Bases' attribute"
	Bases = property(_get_bases, _set_bases)
	
	#@-body
	#@-node:3::Bases
	#@+node:4::GetChromMD5
	#@+body
	def GetChromMD5(self, chromosome_label):
		return self._chromosome_hash[chromosome_label].MD5
	
	#@-body
	#@-node:4::GetChromMD5
	#@+node:5::GetBaseFrequency
	#@+body
	def GetBaseFrequency(self, base):
		return self._base_frequency_hash[base.upper()]
	
	#@-body
	#@-node:5::GetBaseFrequency
	#@+node:6::GetChrom
	#@+body
	def GetChrom(self, chromosome_label):
		return self._chromosome_hash[chromosome_label]
	
	#@-body
	#@-node:6::GetChrom
	#@+node:7::GetHandle
	#@+body
	def GetHandle(self):
		return self._filehandle
	
	#@-body
	#@-node:7::GetHandle
	#@+node:8::MD5
	#@+body
	def _get_md5(self): return self._md5
	def _set_md5(self, value): raise AttributeError, "Can't set 'MD5' attribute"
	MD5 = property(_get_md5, _set_md5)
	
	#@-body
	#@-node:8::MD5
	#@+node:9::GetFilterCutoff
	#@+body
	def GetFilterCutoff(self):
		return self._filter_cutoff_ratio
	
	#@-body
	#@-node:9::GetFilterCutoff
	#@-others


#@-body
#@-node:2::<<class CacheFileHandler>>


#@<<class CacheFileHandlerDirect>>
#@+node:3::<<class CacheFileHandlerDirect>>
#@+body
class CacheFileHandlerDirect(CacheFileHandler):
	"""
	A subclass of CacheFileHandler, that simply overrides __init__ so that it can be initialized directly, (e.g. when a new cache file is built) instead of having to parse a the cache file.
	"""

	#@+others
	#@+node:1::__init__
	#@+body
	def __init__(self, filehandle, base_frequency_hash, chromosome_hash,
				 cache_file_md5, filter_cutoff_ratio,
				 prob_matrix_filename, prob_matrix_md5, data_offset):
		self._filehandle          = filehandle
		self._base_frequency_hash  = base_frequency_hash
		self._chromosome_hash      = chromosome_hash
		self._md5                  = cache_file_md5
		self._filter_cutoff_ratio  = filter_cutoff_ratio
		self._prob_matrix_filename = prob_matrix_filename
		self._prob_matrix_md5      = prob_matrix_md5
		self._data_segment_byte_position          = data_offset
		
		for label in self._chromosome_hash:
			self._chromosome_hash[label].AddDataOffset(self._data_segment_byte_position)
	
	#@-body
	#@-node:1::__init__
	#@-others


#@-body
#@-node:3::<<class CacheFileHandlerDirect>>



#@<<CacheFileHandlerError>>
#@+node:5::<<CacheFileHandlerError>>
#@+body
class CacheFileHandlerError(ParseError):
	pass
#@-body
#@-node:5::<<CacheFileHandlerError>>



#@<<class ChromosomeHandler>>
#@+node:6::<<class ChromosomeHandler>>
#@+body
class ChromosomeHandler(object):

	#@+others
	#@+node:1::__init__
	#@+body
	def __init__(self, label, num_windows, forward_offset, revcomp_offset, chromosome_md5):
		self._label = label
		self._num_windows = num_windows
		self._forward_offset = forward_offset
		self._revcomp_offset = revcomp_offset
		self._md5 = chromosome_md5
		self._data_segment_byte_position_added = 0
	#@-body
	#@-node:1::__init__
	#@+node:2::AddDataOffset
	#@+body
	def AddDataOffset(self, data_offset):
		if self._data_segment_byte_position_added:
			raise ChromsomeHandlerError, 'Can only AddDataOffset once'
		self._data_segment_byte_position_added = 1
		self._forward_offset += data_offset
		self._revcomp_offset += data_offset
		
		##print 'Chromosome:', self._label
		##print 'Forward Offset:', self._forward_offset
		##print 'Reverse Offset:', self._revcomp_offset
	
	#@-body
	#@-node:2::AddDataOffset
	#@+node:3::GetForwardOffset
	#@+body
	def GetForwardOffset(self):
		return self._forward_offset
	#@-body
	#@-node:3::GetForwardOffset
	#@+node:4::GetRevCompOffset
	#@+body
	def GetRevCompOffset(self):
		return self._revcomp_offset
	#@-body
	#@-node:4::GetRevCompOffset
	#@+node:5::GetNumWindows
	#@+body
	def GetNumWindows(self):
		return self._num_windows
	#@-body
	#@-node:5::GetNumWindows
	#@+node:6::MD5
	#@+body
	def _get_md5(self): return self._md5
	def _set_md5(self, value): raise AttributeError, "Can't set 'MD5' attribute"
	MD5 = property(_get_md5, _set_md5)
	
	#@-body
	#@-node:6::MD5
	#@-others


#@-body
#@-node:6::<<class ChromosomeHandler>>



#@+doc
# 

#@-doc
#@-body
#@-node:0::@file cache_file_handler_gomer.py
#@-leo
