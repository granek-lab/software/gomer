#! /usr/bin/env python

from __future__ import division

import os
import sys
import re
import operator

from globals_gomer import SEQUENCE_ALPHABET, NULL_SYMBOL, NO_INFO_SYMBOL
symbol_alphabet = SEQUENCE_ALPHABET + NO_INFO_SYMBOL + NULL_SYMBOL
if len(sys.argv) < 2:
	print >> sys.stderr, 'usage:', os.path.basename(sys.argv[0]), 'FASTA file[s]',\
		  '\n\n\tCalculates the total base frequency of all the sequences in'\
		  ' all the files provided.' \
		  '\n\tIt can deal with files with multiple FASTA formatted sequences per file.\n\n'
	# print >> sys.stderr, ' ' * 5, 
	sys.exit(2)



COMMENT_TOKEN = '>'
# SEQUENCE_ALPHABET = 'ACGT'
comment_re = re.compile('^' + COMMENT_TOKEN + '(.+)$')
empty_line_re = re.compile('^\s*$')
# sequence_re = re.compile('^([' + SEQUENCE_ALPHABET + ']+)\n$')
# sequence_re = re.compile('^([' + SEQUENCE_ALPHABET + ']+)\s*$')
# sequence_re = re.compile('^\w+\s*$')
sequence_re = re.compile('^([' + symbol_alphabet + ']+)\s*$')



##count_hash = {'a':0,'c':0, 'g':0,'t':0,
##				  'A':0,'C':0, 'G':0,'T':0}
count_hash = dict(zip(symbol_alphabet, [0] * len(symbol_alphabet)))
comment_list = []
for filename in sys.argv[1:]:
	filehandle = file(filename)
	line_count = 0
	for line in filehandle:
		line_count += 1
		if comment_re.match(line):
			comment_list.append(line.strip())
			continue
		elif empty_line_re.match(line):
			continue
		elif sequence_re.match(line):
			for base in line.strip().upper():
				count_hash[base] = 1 + count_hash.get(base, 0)
		else:
			error_message = 'ERROR: In ' + filename + 'line # ' + str(line_count)\
							+ 'does not fit the FASTA format:\n' + line
			raise ValueError, error_message

# final_counts	= {}
##for base in count_hash:
##	final_counts[base.upper()] = final_counts.get(base.upper(), 0) + count_hash[base]
##del(count_hash)

# sorted_bases = final_counts.keys()
sorted_bases = count_hash.keys()
sorted_bases.sort()

total_symbol_count = reduce(operator.add, count_hash.values())
total_base_count = reduce(operator.add, [count_hash[base] for base in SEQUENCE_ALPHABET])
print os.path.basename(filename)
print '\n'.join(comment_list)
print 'Total Symbol Count:', total_symbol_count
print 'Total Base Count:', total_base_count
print 'COUNTS: (forward strand)'
print '\t', ', '.join([base + ':' + str(count_hash[base]) for base in count_hash])

print '\nFREQUENCIES:'
# print '\t', ', '.join([base + ':' + str(count_hash[base]/total_count) for base in count_hash])
complemented_freqs = {
	'A': ((count_hash['A'] + count_hash['T'])/total_base_count)/2,
	'T': ((count_hash['A'] + count_hash['T'])/total_base_count)/2,
	'C': ((count_hash['C'] + count_hash['G'])/total_base_count)/2,
	'G': ((count_hash['C'] + count_hash['G'])/total_base_count)/2 }

##print '\t', ', '.join([base + ':' + str(complemented_freqs[base]/total_count)
##					   for base in complemented_freqs])
print '\t', ', '.join([base + ':' + `complemented_freqs[base]` for base in complemented_freqs])

