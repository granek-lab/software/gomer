#! /usr/bin/env python
from __future__ import division
#@+leo
#@comment Created by Leo at Fri Jul 18 16:35:29 2003
#@+node:0::@file coordinate_feature_parser_gomer.py
#@+body
#@@first
#@@first
#@@language python

MIN_ELEMS = 4 ##Number of required elements per feature line
MAX_ELEMS = 7 ##Number of total allowed elements per feature line


class SeqInfo(object):
	def __init__(self, label, seqID, md5sum):
		self.label = label
		self.seqID = seqID
		self.md5sum = md5sum


#@<<file format>>
#@+node:1::<<file format>>
#@+body
#@+doc
# 
# Comments are allowed throughout the file.  Comments are demarcated by a line 
# starting with a '#' character.
# Any line which is empty, or contains only white space is ignored.
# 
# The %SEQUENCE declarations must precede feature statements.  There should be 
# one %SEQUENCE declaration for every sequence refered to (by way of 
# chromosome_labels) in the feature statements.  The fields of the "%SEQUENCE" 
# declaration must be tab separated.  The sequence id is the ??genbank?? id 
# for the sequence, the chromosome_label must correspond to the 
# chromosome_label in the feature statements.  Using the example of chromsome 
# 1 from cerevisiae, this is the FASTA header:
# 
# >ref|NC_001133| [org=Saccharomyces cerevisiae] [strain=S288C] 
# [moltype=genomic] [chromosome=I]
# 
# The "%SEQUENCE" declaration would be:
# %SEQUENCE	NC_001133	I
# 
# The fields of the feature statements are as follows, they must be tab separated:
# 1) Name (mandatory)
# 2) Chromosome (mandatory)
# 3) StartCoord (mandatory)
# 4) StopCoord (mandatory)
# 5) Alias (optional, multiples separated by |)
# 6) Corresponding feature(s) (optional, multiples separated by |) - the 
# feature(s) this "regulatory" region is upstream of.
# 7) Description (optional)
# 
# ---------------------------------------
# START OF FILE FORMAT:
# # COMMENTS ARE ALLOWED!
# # fields are tab separated!!
# 
# %SEQUENCE sequence_id	chromosome_label
# NAME	CHROMOSOME_LABEL	START STOP	[alias|...]	[feature|...]	[DESCRIPTION]

#@-doc
#@-body
#@-node:1::<<file format>>


#@<<imports>>
#@+node:2::<<imports>>
#@+body
##import os
##os.environ['PYCHECKER'] = '--allglobals --stdlib'
##import pychecker.checker

##import check_python_version
##check_python_version.CheckVersion()

import sys
import re
from parser_gomer import ParseError
from chromosome_gomer import Chromosome
from coordinate_feature_gomer import CoordinateFeature
from open_file import OpenFile
#@+doc
# 
# import string
# from parser_gomer import Parser
# from indexed import Indexed
# from genome_feature_gomer import GenomeFeature
# from orf_feature_gomer import OrfFeature

#@-doc
#@-body
#@-node:2::<<imports>>



#@<<regular expressions>>
#@+node:3::<<regular expressions>>
#@+body
SEQUENCE_TOKEN = '%SEQUENCE'
COMMENT_TOKEN = '#'
comment_re = re.compile('^' + COMMENT_TOKEN + '.*')
seq_info_re = re.compile('^' + SEQUENCE_TOKEN + '\t' +
						 '(?P<seq_id>[^\t]+)' + '\t' +
						 '(?P<chrom_label>[^\t]+)' + '\t' +
						 '(?P<md5sum>[^\t]+)' +
						 '\s*$')
seq_info_comma_re = re.compile('^' + SEQUENCE_TOKEN + ',' +
							   '(?P<seq_id>[^,]+)' + ',' +
							   '(?P<chrom_label>[^,]+)' + ',' +
							   '(?P<md5sum>[^,]+)' + 
							   '\s*$')
##feature_line_re = re.compile('^' +
##							 '\t'.join(['[^\t]*'] * NUMBER_ELEMENTS_PER_LINE)
##							 + '$')
##feature_line_comma_re = re.compile('^' +
##								   ','.join(['[^,]*'] * NUMBER_ELEMENTS_PER_LINE)
##								   + '$')
feature_line_re = re.compile('^[^\t]*(\t[^\t]*){' + str(MIN_ELEMS-1) + ','+ str(MAX_ELEMS-1) + '}?$')
feature_line_comma_re = re.compile('^[^,]*(,[^,]*){' + str(MIN_ELEMS-1) + ','+ str(MAX_ELEMS-1) + '}?$')



empty_line_re = re.compile('^\s*$')



#@+doc
# 
# (?P<name>...)
#     Similar to regular parentheses, but the substring matched by the group 
# is accessible via the symbolic group name name. Group names must be valid 
# Python identifiers, and each group name must be defined only once within a 
# regular expression. A symbolic group is also a numbered group, just as if 
# the group were not named. So the group named 'id' in the example above can 
# also be referenced as the numbered group 1.
# 
#     For example, if the pattern is (?P<id>[a-zA-Z_]\w*), the group can be 
# referenced by its name in arguments to methods of match objects, such as 
# m.group('id') or m.end('id'), and also by name in pattern text (for example, 
# (?P=id)) and replacement text (such as \g<id>).
# 
# %SEQUENCE seq_id	chromosome_label

#@-doc
#@-body
#@-node:3::<<regular expressions>>



#@<<CoordinateFeatureParse>>
#@+node:4::<<CoordinateFeatureParse>>
#@+body
def CoordinateFeatureParse(filename, chromosome_hash, feature_dictionary,
						   ambiguous_feature_names):
	"""
	file is closed after parsing
	
	returns: a hash, keyed by chromosome_identifier, of Chromosome instances,
	         to which all the features encoded in the gene feature file have
			 been added
			 
			 a dictionary - containing references to all the features in the 
			 				genome, keyed by all the possible names for the feature.
							It should be noted that all the keys are converted to uppercase
							to make it easier to find entries
	
	"""
##-----------------------------------------------------------------------
	filehandle = OpenFile(filename, 'Parse Coordinate Feature','r')	
	## Parse the header section
	seq_info_hash = {}
	line_number = 0
	while 1:
		line = filehandle.readline()
		line_number += 1
		if not line :
			raise CoordinateFeatureParseError \
				  (filehandle.name, line, line_number,\
				   'Parse Error: Reached end of file prematurely:\nno Features were found')
		line = line.rstrip('\n') # need to supply the \n, otherwise the trailing tabs will be stripped too!!
		if comment_re.search(line) :
			continue
		elif seq_info_re.match(line) or seq_info_comma_re.match(line):
			if seq_info_re.match(line):
				seq_info_match = seq_info_re.match(line)
			else:
				seq_info_match = seq_info_comma_re.match(line)
			chrom_label = seq_info_match.group('chrom_label')
			seq_id = seq_info_match.group('seq_id')
			md5sum = seq_info_match.group('md5sum')
			seq_info_hash[chrom_label] = SeqInfo(chrom_label, seq_id, md5sum)
		elif empty_line_re.search(line):
			continue
		elif feature_line_re.match(line) or feature_line_comma_re.match:
			break
		else:
			raise CoordinateFeatureParseError \
				  (filehandle.name, line, line_number,\
				   'Parse Error: Unrecognized line in header')
##-----------------------------------------------------------------------
	# Parse the features section
	num_features = 0
	line_dict = {}
	while 1:
		if not line:
			if num_features:
				break
			else:
				raise CoordinateFeatureParseError \
					  (filehandle.name, line, line_number, \
					   'Parse Error: Reached end of file prematurely:\nno probabilities were found')
		elif comment_re.search(line) or empty_line_re.search(line):
			line = filehandle.readline()
			continue
		elif feature_line_re.match(line) :
			split_line = line.rstrip('\n').split('\t')
		elif feature_line_comma_re.match(line) :
			split_line = line.rstrip('\n').split(',')
		else:
			raise CoordinateFeatureParseError \
				  (filehandle.name, line, line_number,\
				   'Parse Error: Unrecognized line in feature section')

		line_concat = 'JOINT'.join(split_line)
		if line_concat in line_dict:
			print >>sys.stderr, 'Duplicated line ignored:', line
			## raise StandardError
			continue
		else:
			line_dict[line_concat] = 1

		alias = corresponding_features = description = ''
		if len(split_line) == 4:
			(name, chrom_label, startcoord, stopcoord) = split_line
		elif len(split_line) == 5:
			(name, chrom_label, startcoord, stopcoord, alias) = split_line
		elif len(split_line) == 6:
			(name, chrom_label, startcoord, stopcoord, alias, corresponding_features) = split_line
		elif len(split_line) == 7:
			(name, chrom_label, startcoord, stopcoord, alias, corresponding_features,
			 description) = split_line
		else :
			raise CoordinateFeatureParseError (filehandle.name, line, line_number, \
											   'Coordinate Feature file should have between' \
											   + str(MIN_ELEMS) + \
											   ' and ' + str(MAX_ELEMS) + \
											   ' elements per line, not '+\
											   str(len(split_line)) + ' elements')
		# The following is a check for correctness of the feature file
		# all of the following are mandatory as defined by the coordinate file format
		if not name :
			raise CoordinateFeatureParseError (filehandle.name, line, line_number,\
									 'Mandatory Element: Name Missing')
		elif not chrom_label :
			raise CoordinateFeatureParseError (filehandle.name, line, line_number,\
									 'Mandatory Element: Chromosome Missing')
		elif not startcoord :
			raise CoordinateFeatureParseError (filehandle.name, line, line_number,\
									 'Mandatory Element: StartCoord Missing')
		elif not stopcoord :
			raise CoordinateFeatureParseError (filehandle.name, line, line_number,\
									 'Mandatory Element: StopCoord Missing')
		if int(stopcoord) < int(startcoord):
			raise CoordinateFeatureParseError (filehandle.name, line, line_number,\
									 'StopCoord cannot be less than StartCoord')
		

		#*******
		# We need to further process some of the raw text from the feature table so that it is
		# suitable for use in the feature class
		#*******

		# We need to split up all the elements that can have multiple values
		# into lists of strings
		alias_list = alias.split('|')
		corresponding_features_list = corresponding_features.split('|')

		current_chrom = chromosome_hash.setdefault(chrom_label, 
												   Chromosome(chrom_label))
		
		new_feature = CoordinateFeature(name, chrom_label, startcoord, stopcoord,
										alias_list, corresponding_features_list,
										description, current_chrom)

		_add_names_to_dictionary (feature_dictionary,
								  ambiguous_feature_names,
								  new_feature)
		current_chrom.AddFeature(new_feature)
		num_features += 1 
		line = filehandle.readline()
		line_number += 1
	filehandle.close()
	return chromosome_hash, feature_dictionary, ambiguous_feature_names, seq_info_hash

#@-body
#@-node:4::<<CoordinateFeatureParse>>


#@<<_add_names_to_dictionary>>
#@+node:5::<<_add_names_to_dictionary>>
#@+body
##-------------------------------------------------------------------------
def _add_names_to_dictionary (feature_dictionary,
							  ambiguous_feature_names,
							  feature_instance):
	for cur_name in feature_instance.AllNames():
		cap_cur_name = cur_name.upper()
##			feature_dictionary[cap_cur_name] = feature_instance
		if cap_cur_name in ambiguous_feature_names:
			ambiguous_feature_names[cap_cur_name].append(feature_instance)
		elif cap_cur_name in feature_dictionary:
			if feature_dictionary[cap_cur_name] == feature_instance:
				continue
			else:
				value = feature_dictionary[cap_cur_name]
				del feature_dictionary[cap_cur_name]
				ambiguous_feature_names[cap_cur_name] = [value, feature_instance]
		else:
			feature_dictionary[cap_cur_name] = feature_instance
	"""
	all keys are converted to uppercase to make it easier to find entries in the 
	dictionary
	"""

#@-body
#@-node:5::<<_add_names_to_dictionary>>




#@<<Parse Errors>>
#@+node:6::<<Parse Errors>>
#@+body
#@@code
#@+others
#@+node:1::CoordinateFeatureParseError
#@+body
class CoordinateFeatureParseError(ParseError):
	pass
#@-body
#@-node:1::CoordinateFeatureParseError
#@-others




#@-body
#@-node:6::<<Parse Errors>>

	


#@<<command line>>
#@+node:7::<<command line>>
#@+body
#@<<def usage>>
#@+node:1::<<def usage>>
#@+body
def usage () :
	print '-'*70 + '\n', 'Arguments given (', len(sys.argv), '):', sys.argv, """
	1. filename - coordinate feature file
	""" + '\n' + '-'*70
	sys.exit(1)

#@-body
#@-node:1::<<def usage>>



#@<<if main>>
#@+node:2::<<if main>>
#@+body
def main():
	pass

if __name__ == '__main__':
	import os
	print os.getcwd()
##	try :
##		main()
##	except CoordinateFeatureParseError, instance:
##		instance.print_message()
##		raise 
	main()
#@-body
#@-node:2::<<if main>>


#@-body
#@-node:7::<<command line>>


#@-body
#@-node:0::@file coordinate_feature_parser_gomer.py
#@-leo
