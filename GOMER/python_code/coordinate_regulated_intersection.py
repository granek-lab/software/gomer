#! /usr/bin/env python

from __future__ import division

import os
import sys

from coordinate_feature_parser_gomer import CoordinateFeatureParse
from regulated_feature_parser_gomer import ParseRegulatedFeatures



def main():
	if len(sys.argv) == 3:
		regulated_filename = sys.argv[1]
		coordinate_filename = sys.argv[2]
		
		chromosome_hash, feature_dictionary, ambiguous_feature_names, seq_info_hash = CoordinateFeatureParse(coordinate_filename,{}, {}, {})
		regulated_feature_names, excluded_feature_names, unregulated_feature_names = ParseRegulatedFeatures(regulated_filename)

	else:
		print >>sys.stderr, 'usage:', os.path.basename(sys.argv[0]), 'REGULATED_FILE COORDINATE_FILE'
		print >>sys.stderr, '\nPrints the list of feature names that is the intersection between the names found in REGULATED_FILE and COORDINATE_FILE.  The name comparison is case insensitive.'
		sys.exit(1)

	regulated_intersection=[]
	unregulated_intersection=[]
	excluded_intersection=[]

	for key in regulated_feature_names:
		if key in feature_dictionary:
			# regulated_intersection.append(regulated_feature_names[key])
			regulated_intersection.append(feature_dictionary[key].Name)

	for key in unregulated_feature_names:
		if key in feature_dictionary:
			# unregulated_intersection.append(unregulated_feature_names[key])
			unregulated_intersection.append(feature_dictionary[key].Name)

	for key in excluded_feature_names:
		if key in feature_dictionary:
			# excluded_intersection.append(excluded_feature_names[key])
			excluded_intersection.append(feature_dictionary[key].Name)

	print '# Coordinate Feature File:', coordinate_filename
	print '# Regulated File:', regulated_filename, '\n\n'
	if regulated_intersection:
		print '# Regulated Features Found in Coordinate File:'
		print '#', '-' *60
		print '\n'.join(regulated_intersection)
		print '#', '-' *60

	if unregulated_intersection:
		print '# Unregulated Features Found in Coordinate File:'
		print '#', '-' *60
		print '\n'.join(unregulated_intersection)
		print '#', '-' *60

	if excluded_intersection:
		print '# Excluded Features Found in Coordinate File:'
		print '#', '-' *60
		print '\n'.join(excluded_intersection)
		print '#', '-' *60

if __name__ == "__main__":
	main()
