#! /usr/bin/env python

try:
	import psyco
except ImportError:
	print >>sys.stderr, 'psyco is not available.'
else:
	print >>sys.stderr, 'psyco is available and seems to be working!!'
	# psyco.log()
	psyco.profile()
