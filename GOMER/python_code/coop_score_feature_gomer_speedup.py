from __future__ import division
#@+leo
#@+node:0::@file coop_score_feature_gomer.py
#@+body
#@@first
#@@language python
##import os
##os.environ['PYCHECKER'] = '--allglobals --stdlib'
##import pychecker.checker

##import check_python_version
##check_python_version.CheckVersion()

## from globals_gomer import FORWARD, REVCOMP
FORWARD = 0
REVCOMP = 1

import sys


#@+others
#@+node:1::CooperativeScoreFeature
#@+body
def CooperativeScoreFeature(feature, site_kappa_model, primary_tf, second_tf_list, 
							free_conc_hash,	K_dimer_hash, hashof_coop_model_hashes):
	"""
	I think this function should be usable for all feature types, I need to be
	sure that all features have the required attributes.  If they do, the name
	of the function will probably be changed to "CooperativeScoreFeature", and
	the "feature" paramtere will be changed to "feature"

	PARAMS:
	        feature              : the feature of interest
			site_kappa_model : refered to as "regulatory_region_model" in
			                   function "RegulatoryRegionScoreFeatureSlice", this
							   is the model which can provide the site kappa
			primary_tf       : the name of the primary transcription factor
			free_conc_hash   : a hash (keyed by tf_name) of free concentration
			                   values for the various transcription factors being
							   considered
			second_tf_list   : a list of the names of the secondary transcription
			                   factors to be considered
			K_dimer_hash     : a "2D hash" of pairwise dimerization constants
	                           between all pairs that will be considered
   hashof_coop_model_hashes  : a "2D hash" of cooperativity (kappa_coop) models,
			                   this should have the same structure as the K_dimer_hash
	"""


	"""
	deal with complete or partial overlap of cooperative proteins
	
	primary TF is the TF that everything else is relative.
	
	In the case of simple regulatory model, the primary TF is the only TF
	"""
## def RegulatoryRegionScoreOrfSlice(orf, regulatory_region_model, binding_site_model, free_concentration_factor):

	##----------------------------------------------------------------
	## Iterate over primary TF sites "i", bound by the protein "X"
	##----------------------------------------------------------------
 	## print >> sys.stderr, 'Scoring:', feature.Name, '(', feature.Types, ')'
	coop_model_hash = hashof_coop_model_hashes[primary_tf]
	primary_tf_free_conc = free_conc_hash[primary_tf]
	second_tf_list = tuple(second_tf_list)
	regulatory_region_ranges = site_kappa_model.GetRegulatoryRegionRanges(feature)
	current_chromsome = feature.Chromosome
	hitlist_hash = {}
	for tf in second_tf_list + (primary_tf,):
		hitlist_hash[tf] = []
		hitlist_hash[tf][FORWARD] = current_chromsome.GetHitList(tf).Forward
		hitlist_hash[tf][REVCOMP] = current_chromsome.GetHitList(tf).ReverseComplement

	primary_tf_hitlist = []
	primary_tf_hitlist[FORWARD] = current_chromsome.GetHitList(primary_tf).Forward
	primary_tf_hitlist[REVCOMP] = current_chromsome.GetHitList(primary_tf).ReverseComplement

	i_sites_denom_product = 1
	for start_i_hitindex, stop_i_hitindex in regulatory_region_ranges:
		for i_hitindex in xrange(start_i_hitindex, (stop_i_hitindex + 1)):
			i_for_rev_denom_product = 1
			for i_strand in (FORWARD, REVCOMP):
				primary_Ka = primary_tf_hitlist[i_strand][i_hitindex]
				## FILTER START
				if primary_Ka == 0:
					continue
				## FILTER STOP
				i_site_kappa = site_kappa_model.GetSiteKappa(feature, i_hitindex, i_strand)
				if i_site_kappa == 0:
					continue
				K_a_eff = (primary_Ka * i_site_kappa)
				i_denominator = 1 + (primary_tf_free_conc * K_a_eff)
				## cooperativity_ranges = coop_model.GetRegulatoryRegionRanges(feature, i_hitindex)
				##----------------------------------------------------------------
				## Iterate over secondary TF proteins "Y"
				##----------------------------------------------------------------
				proteins_denom_product = 1
				for second_tf in second_tf_list:
					##----------------------------------------------------------------
					## Iterate over secondary TF sites "j" bound by the protein "Y"
					##----------------------------------------------------------------
					second_tf_free_conc = free_conc_hash[second_tf]
					coop_model = coop_model_hash[second_tf]
					cooperativity_ranges = coop_model.GetCoopRanges(i_hitindex, i_strand, feature)
					j_sites_denom_product = 1
					for start_coop_hitindex, stop_coop_hitindex in cooperativity_ranges:
						for j_hitindex in xrange(start_coop_hitindex, (stop_coop_hitindex + 1)):
							j_for_rev_denom_product = 1
							for j_strand in (FORWARD, REVCOMP):
								second_Ka = hitlist_hash[second_tf][j_strand][j_hitindex]
								## FILTER START
								if second_Ka == 0:
									continue
								## FILTER STOP
								coop_kappa = coop_model.GetSiteKappa(i_hitindex, i_strand,
																	 j_hitindex, j_strand, feature)
								if coop_kappa ==0:
									continue
								K_coop_eff = coop_kappa * K_dimer_hash[primary_tf][second_tf]
								cur_secondary_denominator = 1 + ((primary_tf_free_conc *
																  second_tf_free_conc *
																  K_coop_eff)**2
																 * K_a_eff * second_Ka)
								j_for_rev_denom_product *= cur_secondary_denominator
							j_sites_denom_product *= j_for_rev_denom_product
					proteins_denom_product *= j_sites_denom_product
				i_for_rev_denom_product *= i_denominator * proteins_denom_product
			i_sites_denom_product *= i_for_rev_denom_product
			
	feature_score = 1 - (1/i_sites_denom_product)							
	return feature_score

#@-body
#@-node:1::CooperativeScoreFeature
#@-others


#@-body
#@-node:0::@file coop_score_feature_gomer.py
#@-leo
