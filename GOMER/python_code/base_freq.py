from __future__ import division
#@+leo
#@comment Created by Leo at Tue Mar 11 13:43:55 2003
#@+node:0::@file base_freq.py
#@+body
#@@first

##import os
##os.environ['PYCHECKER'] = '--allglobals --stdlib'
##import pychecker.checker

##import check_python_version
##check_python_version.CheckVersion()

import os
import sys
import operator

from chromosome_table_parser import ChromosomeTableParser

from integer_string_compare import sort_numeric_string



#@<<frequencies>>
#@+node:1::<<frequencies>>
#@+body
def frequencies(base_count_hash):
	total_counts = reduce(operator.add, [base_count_hash[base] for base in base_count_hash])
	AT_freq = (base_count_hash['A'] + base_count_hash['T'])/total_counts
	GC_freq = (base_count_hash['G'] + base_count_hash['C'])/total_counts
	AT_GC_ratio = ((base_count_hash['A'] + base_count_hash['T'])/
				   (base_count_hash['G'] + base_count_hash['C']))
	print 'A:', AT_freq, 'T:', AT_freq, 'G:', GC_freq, 'C:', GC_freq, 'AT/GC:', AT_GC_ratio
	return AT_freq, GC_freq, AT_GC_ratio


#@-body
#@-node:1::<<frequencies>>



#@<<command line>>
#@+node:2::<<command line>>
#@+body
#@<<def run>>
#@+node:1::<<def run>>
#@+body
def run (chromosome_table_file_name=None, probability_file_name=None):
	
	#@<<process arguments>>
	#@+node:1::<<process arguments>>
	#@+body
	import sys 
	import os
	import getopt
	
	try:
		opts, args = getopt.getopt(sys.argv[1:], '', ["EXCLUDE_MITO", "INCLUDE_MITO"])
	except getopt.GetoptError:
		# print help information and exit:
		usage()
		sys.exit(2)
	include_mito = 0
	exclude_mito = 0
	for o, a in opts:
		if o in ("--EXCLUDE_MITO",):
			exclude_mito = 1
		if o in ("--INCLUDE_MITO",):
			include_mito = 1
	
	
	print '#' * 60
	print '#' * 60
	if exclude_mito and include_mito:
		print >> sys.stderr, '--EXCLUDE_MITO and --INCLUDE_MITO arguments are mutually exclusive'
		sys.exit(1)
	elif include_mito:
		print >> sys.stderr, 'INCLUDING mitochodrial DNA'
		mito = 1
	elif exclude_mito:
		print 'EXCLUDING mitochodrial DNA'
		mito = 0
	else:
		print >> sys.stderr, 'Must provide either the --EXCLUDE_MITO or --INCLUDE_MITO argument'
		sys.exit(1)
	print '#' * 60
	print '#' * 60
	
	EXPECTED_ARGUMENTS = 1 # or more - a glob can be used
	##if chromosome_table_file_name and probability_file_name:
	##	print 'run called with parameters'
	if len(args) == EXPECTED_ARGUMENTS:
		chromosome_table_file_name = args[0]
		# output_directory_name = sys.argv[2]
	else :
		usage()
	
	for file_name in [chromosome_table_file_name]:
		if not os.path.exists(file_name):
			print "File doesn't exist:", file_name
			sys.exit(1)
	
	#@-body
	#@-node:1::<<process arguments>>

	
	chromosome_table_parser = ChromosomeTableParser()
	chromosome_table_parser.LoadFile(chromosome_table_file_name)
	(feature_file_list, chromosome_file_hash,
	 chromosome_flags_hash, frequency_hash) = chromosome_table_parser.Parse()

	mito_label = '17'
	
	if not mito:
		if mito_label in chromosome_file_hash:
			del(chromosome_file_hash[mito_label])

	chromosome_windows_hash = {}
	chromosome_counts_hash = {}

	genome_count_hash = {'A':0, 'C':0, 'G':0, 'T':0}



##	chromosome_keys = chromosome_file_hash.keys()
##	chromosome_keys.sort(IntegerStringCmp)
	chromosome_keys = sort_numeric_string(chromosome_file_hash.keys())
	for chrom_label in chromosome_keys:
		chrom_sequence_filename = chromosome_file_hash[chrom_label]
		# chrom_sequence_hash[chrom_label] = LoadSequence(chrom_sequence_filename)
		cur_chrom_sequence = LoadSequence(chrom_sequence_filename)
		print 'Length of Chromosome', chrom_label, 'is', len(cur_chrom_sequence)
		sequence_count_hash, current_window_ratio = CountBases(cur_chrom_sequence)
		chromosome_windows_hash[chrom_label] = current_window_ratio
		chromosome_counts_hash[chrom_label] = sequence_count_hash
		print chrom_label, sequence_count_hash
		frequencies(sequence_count_hash)						
		for base in genome_count_hash:
			genome_count_hash[base] += sequence_count_hash[base]
		print '\n'
	print 'Genome:', genome_count_hash
	frequencies(genome_count_hash)

	



def CountBases(sequence, window_size=10000):
	ATcounter = 0
	sequence_count_hash = {'A':0, 'C':0, 'G':0, 'T':0}
	window_count_hash = {'A':0, 'C':0, 'G':0, 'T':0}
	current_window_ratio = [None] * ((len(sequence) - window_size) + 1)
	for index in xrange(len(sequence)):
		cur_base = sequence[index]
		sequence_count_hash[cur_base] += 1
		window_count_hash[cur_base] += 1
		tail_base = sequence[(index - window_size)]
		window_index = (index - (window_size - 1))
		if window_index >= 0:
			if window_index >= 1:
				window_count_hash[tail_base] -= 1
			current_window_ratio[window_index] = ((window_count_hash['A'] +
												   window_count_hash['T'])/
												  (window_count_hash['C'] +
												   window_count_hash['G']))
	return sequence_count_hash, current_window_ratio
	
def LoadSequence(filename):
	handle = file(filename)
	header = handle.readline()
	print 'HEADER:', header.strip()
	sequence = handle.read()
	sequence = sequence.replace('\n','')
	sequence = sequence.upper()
	handle.close()
	return sequence

##	print 'Load Chromosome Sequence Files'
##	print '=============================='
##	sequence_parser = CerevisiaeSequenceParser()
##	for chromosome_label in chromosome_file_hash:
##		chromosome_sequence_file = chromosome_file_hash[chromosome_label]
##		chromosome_flags = chromosome_flags_hash.get(chromosome_label, '')
##		cur_sequence = sequence_parser.ParseFile(chromosome_file_hash[chromosome_label],
##												 chromosome_label, chromosome_flags)
##		chromosome_hash[chromosome_label].SetSequence(cur_sequence) 
##		print 'Length of Chromosome', chromosome_label, 'is', len(cur_sequence)





####	(feature_file_list,
####	 chromosome_file_hash,
####	 chromosome_flags_hash) = load_chromosome_table(chromosome_table_file_name)

##	# chromosome_hash, feature_dictionary = load_feature_file(feature_file_list)



##	# load_chromosome_sequences(chromosome_file_hash, chromosome_flags_hash, chromosome_hash)


##def load_chromosome_table(chromosome_table_file_name):
##	print 'Load Chromosome Table File'
##	print '=========================='
##	chromosome_table_parser = ChromosomeTableParser()
##	chromosome_table_parser.LoadFile(chromosome_table_file_name)
##	feature_file_list, chromosome_file_hash, chromosome_flags_hash = chromosome_table_parser.Parse()

##	return feature_file_list, chromosome_file_hash, chromosome_flags_hash



##def load_chromosome_sequences(chromosome_file_hash, chromosome_flags_hash, chromosome_hash):
#@-body
#@-node:1::<<def run>>


#@<<def usage>>
#@+node:2::<<def usage>>
#@+body
def usage () :
	print """
Expecting:
	1. Chromosome Table File name
	2. Output Directory
"""	
	sys.exit(1)


#@-body
#@-node:2::<<def usage>>



#@<<if main>>
#@+node:3::<<if main>>
#@+body
if __name__ == '__main__':
	run()
	print "SKIP MITOCHONDRIAL SEQUENCE???????????"
	print "SKIP MITOCHONDRIAL SEQUENCE???????????"
	print "SKIP MITOCHONDRIAL SEQUENCE???????????"
	print "SKIP MITOCHONDRIAL SEQUENCE???????????"

#@-body
#@-node:3::<<if main>>


#@-body
#@-node:2::<<command line>>
#@-body
#@-node:0::@file base_freq.py
#@-leo
