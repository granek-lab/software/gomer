from __future__ import division
#@+leo
#@+node:0::@file sequence_feature_gomer.py
#@+body
#@@first
#@@language python


"""
*Description:

A subclass of GenomeFeature for representing . . . 
"""


#@<<imports>>
#@+node:1::<<imports>>
#@+body
#@+doc
# 
# import os
# os.environ['PYCHECKER'] = '--allglobals --stdlib'
# import pychecker.checker

#@-doc
#@@code

##import check_python_version
##check_python_version.CheckVersion()

from genome_feature_gomer import GenomeFeature

import md5
#@-body
#@-node:1::<<imports>>

SEQUENCE_TYPE = 'SEQUENCE'
class DummySequence:
	def __init__(self, sequence):
		self._sequence = sequence
	def _get_sequence(self): return self._sequence
	def _set_sequence(self, value): raise AttributeError, "Can't set 'Sequence' attribute"
	Sequence = property(_get_sequence, _set_sequence)



class SequenceFeature(GenomeFeature):

	#@+others
	#@+node:2::__init__
	#@+body
	def __init__(self, name, info_hash, sequence):
	
		self._sequence = sequence
		self._dummy_sequence_instance = DummySequence(sequence)
		self._hitlist_hash = {}
	
		if 'seq' in info_hash:
			self._origin_sequence = info_hash['seq']
	
		if 'chrom' in info_hash:
			chrom_label = info_hash['chrom']
		else:
			chrom_label=''
			
		if 'start' in info_hash:
			startcoord = info_hash['start']
		else:
			startcoord = '-1'
	
		if 'stop' in info_hash:
			stopcoord = info_hash['stop']
		else:
			stopcoord = '-1'
	
		self._md5 = None
	
		chrom_reference=self # the feature acts as its own chromosome
		unique_id=''
		alt_id_list=()
		feature_type_list=(SEQUENCE_TYPE,)
		strand=''
		description=''
		alias_list = ()
		super(SequenceFeature, self).__init__(name, name,alias_list,unique_id,
												alt_id_list,feature_type_list,
												startcoord, stopcoord, strand,
												description, chrom_label, chrom_reference)
	#@-body
	#@-node:2::__init__
	#@+node:3::__len__
	#@+body
	def __len__(self):
		return len(self._sequence)
	
	#@-body
	#@-node:3::__len__
	#@+node:4::MD5
	#@+body
	def _get_md5(self):
		if not self._md5:
			self._md5 = md5.new(self._sequence).hexdigest()
		return self._md5
	def _set_md5(self, value): raise AttributeError, "Can't set 'MD5' attribute"
	MD5 = property(_get_md5, _set_md5)
	
	#@-body
	#@-node:4::MD5
	#@+node:5::GetHitListSize
	#@+body
	def GetHitListSize(self, hitlist_name):
		return self._hitlist_hash[hitlist_name].Length
	
	#@-body
	#@-node:5::GetHitListSize
	#@+node:6::GetHitList
	#@+body
	def GetHitList(self, hitlist_name):
		return self._hitlist_hash[hitlist_name]
	
	#@-body
	#@-node:6::GetHitList
	#@+node:7::GetAllHitLists
	#@+body
	def GetAllHitLists(self):
		return self._hitlist_hash.keys()
	
	#@-body
	#@-node:7::GetAllHitLists
	#@+node:8::AddHitlist
	#@+body
	def AddHitlist(self, hitlist, hitlist_label):
		if hitlist_label not in self._hitlist_hash:
			self._hitlist_hash[hitlist_label] = hitlist
		else:
			raise SequenceFeatureError(self, 'SequenceFeature already has a hitlist named: ' + hitlist_label + '\n')
	
	
	#@-body
	#@-node:8::AddHitlist
	#@+node:9::Sequence
	#@+body
	def _get_sequence(self):return self._dummy_sequence_instance
	def _set_sequence(self, value): raise AttributeError, "Can't set 'Sequence' attribute"
	Sequence = property(_get_sequence, _set_sequence)
	
	#@-body
	#@-node:9::Sequence
	#@+node:10::FeatureIterator
	#@+body
	def FeatureIterator(self, feature_types=None):
		if feature_types and (SEQUENCE_TYPE not in feature_types):
			raise SequenceFeatureError(self, SEQUENCE_TYPE + ' not in feature_types'  + '\n')
		##yield self
		return (self,)
	
	#@-body
	#@-node:10::FeatureIterator
	#@-others



#@-body
#@-node:0::@file sequence_feature_gomer.py
#@-leo
