#! /usr/bin/env python
from __future__ import division
#@+leo
#@comment Created by Leo at Wed Jun 18 17:19:28 2003
#@+node:0::@file hit_list_gomer.py
#@+body
#@@first
#@@first
#@@language python

"""
*Description:

A class for data from scoring a sequence with a binding site model.
This is mostly a container class, holds a list of scores for each window in the
forward sequence, and another list for each in the reverse complemented sequence.

Other important data maintained is a reference to the binding site model used.
The length of the binding site model (which you need to be able to figure out the
sequence with which each window corresponds).  The name of the binding site model
used, so that each chromsome can hold multiple hit lists, corresponding to
different binding site models, and they can be distinguished
"""
##import os
##os.environ['PYCHECKER'] = '--allglobals --stdlib'
##import pychecker.checker

##import check_python_version
##check_python_version.CheckVersion()



#@<<class HitList>>
#@+node:1::<<class HitList>>
#@+body
class HitList(object):

	#@+others
	#@+node:1::__init__
	#@+body
	def __init__(self, score_list, rev_comp_score_list, binding_site_model, hitlist_name=''):

	#@+doc
	# 
	# 	self._forward_score_list = tuple(score_list)
	# 	self._reverse_complement_score_list = tuple(rev_comp_score_list)

	#@-doc
	#@@code
		self._forward_score_list = score_list
		self._reverse_complement_score_list = rev_comp_score_list
		self._binding_model_descriptor = binding_site_model.Descriptor
		self._binding_model_length = binding_site_model.Length
		if hitlist_name:
			self._name = hitlist_name
		else:
			self._name = binding_site_model.Descriptor
	
	#@-body
	#@-node:1::__init__
	#@+node:2::@doc __getitem__
	#@+body
	#@+doc
	# 
	# def  __getitem__(self, indexOrSlice):
	#     try:
	#         return self._sequence[indexOrSlice]
	#     except TypeError:
	#         return self._sequence[indexOrSlice.start:indexOrSlice.stop]
	# 

	#@-doc
	#@-body
	#@-node:2::@doc __getitem__
	#@+node:3::Forward
	#@+body
	def _get_forward(self): return self._forward_score_list
	def _set_forward(self, value): raise AttributeError, "Can't set 'Forward' attribute"
	Forward = property(_get_forward, _set_forward)
	
	#@-body
	#@-node:3::Forward
	#@+node:4::ReverseComplement
	#@+body
	def _get_reverse_complement(self): return self._reverse_complement_score_list
	def _set_reverse_complement(self, value): raise AttributeError, "Can't set 'ReverseComplement' attribute"
	ReverseComplement = property(_get_reverse_complement, _set_reverse_complement)
	
	#@-body
	#@-node:4::ReverseComplement
	#@+node:5::Name
	#@+body
	def _get_name(self): return self._name
	def _set_name(self, value): raise AttributeError, "Can't set 'Name' attribute"
	Name = property(_get_name, _set_name)
	
	#@-body
	#@-node:5::Name
	#@+node:6::Length
	#@+body
	def _get_length(self):
		forward_length = len(self._forward_score_list)
		revcomp_length = len(self._reverse_complement_score_list)
		if forward_length <> revcomp_length:
			raise HitListError('Forward and Revcomp parts of hitlist have different lengths!!\n')
		return forward_length
	def _set_length(self, value): raise AttributeError, "Can't set 'Length' attribute"
	Length = property(_get_length, _set_length)
	
	#@-body
	#@-node:6::Length
	#@+node:7::Output
	#@+body
	def Output(self, file_handle):
		file_handle.write(str(self.Forward))
		file_handle.write('\n\n')
		file_handle.write(str(self.ReverseComplement))
		file_handle.write('\n\n')
	#@-body
	#@-node:7::Output
	#@-others
#@-body
#@-node:1::<<class HitList>>



#@<<class TestList>>
#@+node:2::<<class TestList>>
#@+body
class TestList(object):

	#@+others
	#@+node:1::__init__
	#@+body
	def __init__(self, score_list, rev_comp_score_list):
		self._forward_score_list = tuple(score_list)
		self._reverse_complement_score_list = tuple(rev_comp_score_list)
		self._dummy = 'dummy'
	
	#@-body
	#@-node:1::__init__
	#@+node:2::Forward
	#@+body
	def _get_forward(self): return self._forward_score_list
	def _set_forward(self, value): raise AttributeError, "Can't set 'Forward' attribute"
	Forward = property(_get_forward, _set_forward)
	
	#@-body
	#@-node:2::Forward
	#@+node:3::Dummy
	#@+body
	def _get_dummy(self): return self._dummy
	def _set_dummy(self, value): raise AttributeError, "Can't set 'Dummy' attribute"
	Dummy = property(_get_dummy, _set_dummy)
	
	#@-body
	#@-node:3::Dummy
	#@+node:4::ReverseComplement
	#@+body
	def _get_reverse_complement(self): return self._reverse_complement_score_list
	def _set_reverse_complement(self, value): raise AttributeError, "Can't set 'ReverseComplement' attribute"
	ReverseComplement = property(_get_reverse_complement, _set_reverse_complement)
	
	#@-body
	#@-node:4::ReverseComplement
	#@+node:5::GetHit
	#@-node:5::GetHit
	#@-others
#@-body
#@-node:2::<<class TestList>>



#@<<command line>>
#@+node:3::<<command line>>
#@+body
#@<<def run>>
#@+node:1::<<def run>>
#@+body
def run ():
	blah = TestList([1,2,3,4,5,6,7,8,9,10], range(-1, -11, -1))
	print 'blah.Forward', blah.Forward 
	print 'blah.Forward[2:4]', blah.Forward[2:4]
	print 'blah.ReverseComplement', blah.ReverseComplement
	print 'blah.Dummy', blah.Dummy
	print '\n\n'

#@+doc
# 
# 	#------------------------------------------------------
# 	try:
# 		blah.Forward[2:4] = [1,2,3]
# 	except TypeError, message:
# 		print 'Good!: got error trying to assign to slice:"', message, '"'
# 	else :
# 		print 'BAD!: NO Error assigning to slice'
# 	#------------------------------------------------------
# 	try:
# 		blah.Forward = [1,2,3]
# 	except AttributeError, message:
# 		print 'Good!: got error trying to assign to Forward Attribute:"', message, '"'
# 	else :
# 		print 'BAD!: NO Error trying to assign to Forward Attribute'
# 	#------------------------------------------------------
# 	try:
# 		blah.ReverseComplement[4] = 7
# 	except TypeError, message:
# 		print 'Good!: got error trying to assign to Forward index:"', message, '"'
# 	else :
# 		print 'BAD!: NO Error assigning to item'
# 	#------------------------------------------------------
# 	try:
# 		blah.ReverseComplement = 10
# 	except AttributeError, message:
# 		print 'Good!: got error trying to assign to ReverseComplement 
# Attribute:"', message, '"'
# 	else :
# 		print 'BAD!: NO Error trying to assign to ReverseComplement Attribute'
# 	#------------------------------------------------------
# 	try:
# 		blah.Dummy = 100
# 	except AttributeError, message:
# 		print 'Good!: got error trying to assign to Dummy Attribute:"', message, '"'
# 	else :
# 		print 'BAD!: NO Error trying to assign to Dummy Attribute'
# 	#------------------------------------------------------
# 
# 

#@-doc
#@-body
#@-node:1::<<def run>>


#@<<def usage>>
#@+node:2::<<def usage>>
#@+body
def usage () :
	print """
Expecting:
"""	
	sys.exit(1)


#@-body
#@-node:2::<<def usage>>



#@<<if main>>
#@+node:3::<<if main>>
#@+body
if __name__ == '__main__':
	import os
	print os.getcwd()
	run()

#@-body
#@-node:3::<<if main>>


#@-body
#@-node:3::<<command line>>


#@-body
#@-node:0::@file hit_list_gomer.py
#@-leo
