#! /usr/bin/env python

from __future__ import division
import sys
import os
import re
import math
from optparse import OptionParser

# from parse_raw_coordinate_file import  ParseRawCoordinates
from coordinate_feature_parser_gomer import CoordinateFeatureParse
from parse_gomer_output import ParseGomerOutput
from regulated_feature_parser_gomer import ParseRegulatedFeatures
# from generate_ROC_curve import PlotROCCurve
from generate_ROC_curve import OutputROCCurveData
from grace_plot import GracePlot

from estimate_p_value import MNCP, ROCAUC

filename_re = re.compile('flank(\d+)_tail(\d+)\.')


def cutoff_fraction(regulated_scores, unregulated_scores, cutoff):
	cutoff_index = int(math.ceil(cutoff *  len(regulated_scores)))
	regulated_scores = regulated_scores[:]
	regulated_scores.sort()
	regulated_scores.reverse()
	unregulated_scores = unregulated_scores[:]
	unregulated_scores.sort()
	unregulated_scores.reverse()
	
	cutoff_score = regulated_scores[cutoff_index]
	unregulated_above_cutoff = [score for score in unregulated_scores if score >= cutoff_score]
	unregulated_fraction = len(unregulated_above_cutoff)/len(unregulated_scores)
	return cutoff_score, unregulated_fraction

def main():
	##--------------------------------------------------------------------------
	usage = 'usage: %prog [-h/--help] [-o] GOMER_OUTPUT ORF_COORDINATES INTERGENIC_COORDINATES REGULATED'
	parser = OptionParser(usage)
	parser.add_option("-o", "--out_dir",metavar="OUT DIR",
					  help="Save output to OUT DIR (instead of the directory where GOMER_OUTPUT is found")
	parser.add_option("-c", "--cutoff",metavar="CUTOFF", type='float',
					  help="Print the occupancy score such that CUTOFF fraction of the regulated features have scores it.  Also print the fraction of unregulated features that have scores the score at the CUTOFF")
	
	(options, args) = parser.parse_args()
	if len(args) <> 4:
		print >>sys.stderr, ' '.join(sys.argv)
		parser.error("incorrect number of arguments")
	gomer_output_filename = args[0]
	orf_feature_filename = args[1]
	intergenic_feature_filename = args[2]
	regulated_filename = args[3]

	gomer_output_filehandle       = file(gomer_output_filename)
	# orf_feature_filehandle        = file(orf_feature_filename)
	# intergenic_feature_filehandle = file(intergenic_feature_filename)
	# regulated_filehandle          = file(regulated_filename)

	##--------------------------------------------------------------------------
## 	(orf_single_amplimer_hash,
## 	 orf_multiple_amplimer_hash,
## 	 orf_no_amplimer_hash,
## 	 orf_skipped_lines) = ParseRawCoordinates(orf_feature_filehandle)

## 	(intergenic_single_amplimer_hash,
## 	 intergenic_multiple_amplimer_hash,
## 	 intergenic_no_amplimer_hash,
## 	 intergenic_skipped_lines) = ParseRawCoordinates(intergenic_feature_filehandle)
	##--------------------------------------------------------------------------
	unused_chrom_hash, orf_coordinate_dict, unused_ambiguous_feature_names, unusedseq_info_hash = \
						 CoordinateFeatureParse(orf_feature_filename,{},{},{})

	unused_chrom_hash, intergenic_coordinate_dict, unused_ambiguous_feature_names, unused_seq_info_hash = \
					   CoordinateFeatureParse(intergenic_feature_filename,{},{},{})
##--------------------------------------------------------------------------

	(all_feature_hash,
	 regulated_hash,
	 unknown_feature_hash,
	 gomer_output_skipped_lines,
	 param_hash) = ParseGomerOutput(gomer_output_filehandle)

	(regulated_feature_names,
	 excluded_feature_names,
	 unregulated_feature_names) = ParseRegulatedFeatures(regulated_filename)
	
	regulated_feature_hash = dict([(name.upper(), None) for name in regulated_feature_names])




	intergenic_regulated_scores = []
	intergenic_unregulated_scores = []
	orf_regulated_scores = []
	orf_unregulated_scores = []

##------------------------------------------------------------------------------
## 	for key in all_feature_hash:
## 		feature = all_feature_hash[key]
## 		if (key in orf_single_amplimer_hash) or \
## 			   (key in orf_multiple_amplimer_hash):
## 			if key in regulated_feature_hash:
## 				orf_regulated_scores.append(feature.score)
## 			else:
## 				orf_unregulated_scores.append(feature.score)
## 		elif (key in intergenic_single_amplimer_hash) or \
## 				 (key in intergenic_multiple_amplimer_hash):
## 			if key in regulated_feature_hash:
## 				intergenic_regulated_scores.append(feature.score)
## 			else:
## 				intergenic_unregulated_scores.append(feature.score)
## 		else:
## 			raise ', '.join(('Unknown feature name:', key, feature.name))
##------------------------------------------------------------------------------
 	for key in all_feature_hash:
 		feature = all_feature_hash[key]
		up_key = key.upper()
		if  up_key in orf_coordinate_dict :
 			if key in regulated_feature_hash:
 				orf_regulated_scores.append(feature.score)
 			else:
 				orf_unregulated_scores.append(feature.score)
 		elif up_key in intergenic_coordinate_dict :
 			if key in regulated_feature_hash:
 				intergenic_regulated_scores.append(feature.score)
 			else:
 				intergenic_unregulated_scores.append(feature.score)
 		else:
 			raise ', '.join(('Unknown feature name:', key, feature.name))
##------------------------------------------------------------------------------




	if filename_re.search(os.path.basename(gomer_output_filename)):
		filename_match = filename_re.search(os.path.basename(gomer_output_filename))
		flank = filename_match.group(1)
		tail = filename_match.group(2)
		subtitle = ' '.join(('Flank', flank+',', 'Tail', tail))
	else:
		subtitle = os.path.basename(gomer_output_filename) 
	## plot.Title(subtitle)
	## plot.Title('ROC Curve')
	## plot.Subtitle(subtitle)
	##--------------------------------------------------------------------------
## 	plot = GracePlot(square_plot=1)
## 	plot.XLabel('Fraction of Unregulated')
## 	plot.YLabel('Fraction of Regulated')
## 	plot.AddDiagonal()
## 	PlotROCCurve(plot, intergenic_regulated_scores,intergenic_unregulated_scores,stair_step=True,
## 				 data_set_name='Intergenic')
## 	PlotROCCurve(plot, orf_regulated_scores, orf_unregulated_scores, stair_step=True, data_set_name='ORF')
## 	plot.Save(gomer_output_filename+'.ps', 'postscript')
## 	## plot.Save(gomer_output_filename+'.eps', 'eps')
## 	plot.Exit()
	##--------------------------------------------------------------------------
	if options.out_dir:
		output_directory = options.out_dir
		data_filename = os.path.join(output_directory, os.path.basename(gomer_output_filename + '.data'))
	else:
		data_filename = gomer_output_filename+'.data'
	data_filehandle = file(data_filename, 'w')
	stair_step = True
	plot_data_points = False
	print >>data_filehandle, '##------------------------------------------'
	print >>data_filehandle, '##', os.path.basename(gomer_output_filename)
	print >>data_filehandle, '## Regulated file:', os.path.basename(regulated_filename)
	print >>data_filehandle, '##------------------------------------------'
	print >>data_filehandle, '## Diagonal Line'
	print >>data_filehandle, '0\t0\n', '1\t1\n'
	print >>data_filehandle, '##------------------------------------------'
	print >>data_filehandle, '## Intergenic Coordinate File:', os.path.basename(intergenic_feature_filename)
	print >>data_filehandle, '##------------------------------------------'
	print >>data_filehandle, '## Intergenic ROCAUC:', ROCAUC(intergenic_regulated_scores,
															 intergenic_unregulated_scores,
															 higher_is_better=True)
	print >>data_filehandle, '## Intergenic MNCP:', MNCP(intergenic_regulated_scores,
														 intergenic_unregulated_scores,
														 higher_is_better=True)
	print >>data_filehandle, '##------------------------------------------'
	if options.cutoff:
		intergenic_cutoff_score, intergenic_unregulated_fraction = cutoff_fraction(intergenic_regulated_scores,
																				   intergenic_unregulated_scores,
																				   options.cutoff)
		print >>data_filehandle, '## Intergenic Cutoff Fraction:', options.cutoff
		print >>data_filehandle, '## Intergenic Regulated Cutoff Occupancy:', intergenic_cutoff_score
		print >>data_filehandle, '## Intergenic Unregulated Cutoff Fraction:', intergenic_unregulated_fraction
		print >>data_filehandle, '##------------------------------------------'
	OutputROCCurveData(data_filehandle, intergenic_regulated_scores,
					   intergenic_unregulated_scores,stair_step=stair_step,
					   data_set_name='Intergenic', plot_data_points=plot_data_points)
	print >>data_filehandle, '##------------------------------------------'
	print >>data_filehandle, '## ORF Coordinate File:', os.path.basename(orf_feature_filename)
	print >>data_filehandle, '##------------------------------------------'
	print >>data_filehandle, '##------------------------------------------'
	print >>data_filehandle, '## ORF ROCAUC:', ROCAUC(orf_regulated_scores,
													  orf_unregulated_scores,
													  higher_is_better=True)
	print >>data_filehandle, '## ORF MNCP:', MNCP(orf_regulated_scores,
												  orf_unregulated_scores,
												  higher_is_better=True)
	print >>data_filehandle, '##------------------------------------------'
	if options.cutoff:
		orf_cutoff_score, orf_unregulated_fraction = cutoff_fraction(orf_regulated_scores,
																	 orf_unregulated_scores,
																	 options.cutoff)
		print >>data_filehandle, '## ORF Cutoff Fraction:', options.cutoff
		print >>data_filehandle, '## ORF Regulated Cutoff Occupancy:', orf_cutoff_score
		print >>data_filehandle, '## ORF Unregulated Cutoff Fraction:', orf_unregulated_fraction
		print >>data_filehandle, '##------------------------------------------'
	OutputROCCurveData(data_filehandle, orf_regulated_scores,
					   orf_unregulated_scores, stair_step=stair_step,
					   data_set_name='ORF', plot_data_points=plot_data_points)
	data_filehandle.close()
	##--------------------------------------------------------------------------

## 	for function in [ROCAUC, MNCP]:
## 		print '\t'.join((os.path.basename(gomer_output_filename), 'Intergenic',
## 						 function.__name__,
## 						 str(function(intergenic_regulated_scores,
## 									  intergenic_unregulated_scores,
## 									  higher_is_better=True))))

## 	for function in [ROCAUC, MNCP]:
## 		print '\t'.join((os.path.basename(gomer_output_filename), 'Orf',
## 						 function.__name__,
## 						 str(function(orf_regulated_scores,
## 									  orf_unregulated_scores,
## 									  higher_is_better=True))))


if __name__ == '__main__':
	main()
