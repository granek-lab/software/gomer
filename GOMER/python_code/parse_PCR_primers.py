#! /usr/bin/env python

from __future__ import division

import re
import sys
import csv

dna_re = re.compile('^[acgt]+$', re.IGNORECASE)
chrom_comment_label_re = re.compile('^>.+\[chromosome=([ixv]+)\]', re.IGNORECASE)
mito_comment_label_re = re.compile('^>.+\[location=(mitochondrion)]', re.IGNORECASE)
empty_line_re = re.compile('^\s*$')
whitespace_re = re.compile('\s+')

"""
http://www.resgen.com/products/YGP.php3
Yeast GenePairs Primers
GenePairs are primer pairs that have been proven to amplify all or a specific portion of a given gene. For the human, ResGenTM offers over 20,000 such primer pairs. These amplify a portion of the 3' untranslated region of gene transcripts that have been placed on the recently updated human gene map. To this list we add Yeast ORF specific primers for over 6,000 ORFs. These primers have been designed to amplify, from genomic DNA, the complete coding region including the start and stop codons. For yeast this is possible as very few yeast genes contain introns. The 5' end of all forward primers has been designed to start with the sequence 5'-GGAATTCCAGCTGACCACC followed by the start codon ATG and first 25 or so bases of the ORF. The 5' end of all reverse primers has been designed to start with the sequence 5'-GATCCCCGGGAATTGCCATG followed by the wild type stop codon and reverse complement of the last 25 or so bases of the ORF. The ORF specific region of the forward and reverse PCR primers was adjusted in length to yield a Tm of from 68C to 72C for that portion of the primer. 
"""
"""
http://www.resgen.com/products/YeIRP.php3
Yeast Intergenic Region Primers
Research Genetics now offers over 6,000 Yeast Intergenic Region primer pairs. These primers have been designed to amplify, from genomic DNA, the regions that lie between the Open Reading Frames (ORFs). The 5' end of all forward primers has been designed to start with the sequence 5' -CCGCTGCTAGGCGCGCCGTG followed by the first 25 or so bases of the intergenic region. The 5' end of all reverse primers has been designed to start with the sequence 5' -GCAGGGATGCGGCCGCTGAC followed by the last 25 or so bases of the intergenic region. The common sequences added to the 5' ends of the primers may be used to modify the amplified intergenic region in a number of ways.1 Each intergenic fragment may be amplified using common primers, producing additional product. Thus these fragments may be modified to allow cloning by homologous recombination. These regions also contain restriction enzyme sites, allowing direct cloning of the products. Applications include cloning of yeast promoters upstream of reporter genes and assaying deletions or insertions in yeast genes2.
"""

class PrimerPair:
	def __init__(self, plate, well, name, chrom, start, end, size,
				 for_seq_num, for_seq, rev_seq_num, rev_seq):
		self.plate = plate
		self.well = well
		self.name = name
		self.chrom = chrom
		if chrom == 'Q':
			self.chrom_num = 17
		else:
			self.chrom_num = int(chrom)
		self.start = start 
		self.end = end 
		self.size = int(size)
		self.for_seq_num = for_seq_num 
		self.for_seq = for_seq 
		self.rev_seq_num = rev_seq_num 
		self.rev_seq = rev_seq 
		self.for_primer_matches = []
		self.rev_primer_matches = []
		self.pcr_products = []

	def AddForMatches(self, list_of_match_tuples):
		self.for_primer_matches.extend(list_of_match_tuples)
	def AddRevMatches(self, list_of_match_tuples):
		self.rev_primer_matches.extend(list_of_match_tuples)
	def AddProduct(self, pcr_product_tuple):
		self.pcr_products.append(pcr_product_tuple)

class PrimerMatch:
	def __init__(self, start, end, strand, chrom, mismatches):
		self.start = start 
		self.end = end 
		self.strand = strand
		self.chrom = chrom 
		self.mismatches = mismatches 

complement_hash = {'a':'t',
				   't':'a',
				   'c':'g',
				   'g':'c',
				   'A':'T',
				   'T':'A',
				   'C':'G',
				   'G':'C' }

def ReverseComplement(sequence):
	complement_sequence = [complement_hash[base] for base in sequence]
	complement_sequence.reverse()
	
	## reverse_sequence_list.reverse()
	return ''.join(complement_sequence)
							   

def FindPrimerSites(primer, sequence_hash, mismatches_allowed):
	matches = []
	primer_len = len(primer)
	last_primer_index = primer_len -1
	first_primer_index = 0
	rc_primer = ReverseComplement(primer)
	for sequence_label in sequence_hash:
		sequence = sequence_hash[sequence_label]
		seq_len = len(sequence)
##--------------------------------------------------------------------
		for s_index in xrange((seq_len - primer_len) + 1):
			mismatches_count = 0
			## for p_index in range(primer_len):
			for p_index in range(last_primer_index, -1, -1):
				if sequence[s_index + p_index] <> primer[p_index]:
					mismatches_count += 1
					if mismatches_count > mismatches_allowed:
						break
					elif p_index == last_primer_index:
						mismatches_count = mismatches_allowed + 1
						break
			if mismatches_count <= mismatches_allowed:
				matches.append((s_index+1,'f', sequence_label, mismatches_count))
##--------------------------------------------------------------------
		for s_index in xrange((seq_len - primer_len) + 2):
			mismatches_count = 0
			for p_index in range(primer_len):
				if sequence[s_index + p_index] <> rc_primer[p_index]:
					mismatches_count += 1
					if mismatches_count > mismatches_allowed:
						break
					elif p_index == first_primer_index:
						mismatches_count = mismatches_allowed + 1
						break
			if mismatches_count <= mismatches_allowed:
				matches.append((s_index+1,'r', sequence_label, mismatches_count))
##--------------------------------------------------------------------
	return matches


def IterFindPrimerSites(primer, sequence_hash, mismatches_allowed):
	mismatches_count = 0
	matches = []
	rc_primer = ReverseComplement(primer)

	## primer_re = re.compile(primer, re.IGNORECASE)
	## rc_primer_re = re.compile(rc_primer, re.IGNORECASE)
	primer_re = re.compile(primer)
	rc_primer_re = re.compile(rc_primer)
	
	for sequence_label in sequence_hash:
		sequence = sequence_hash[sequence_label]
		for primer_match in primer_re.finditer(sequence):
			matches.append((primer_match.start()+1,'f', sequence_label, mismatches_count))
		for rc_primer_match in rc_primer_re.finditer(sequence):
			matches.append((rc_primer_match.start()+1,'r', sequence_label, mismatches_count))
	return matches

def MismatchesIterFindPrimerSites(primer, sequence_hash, mismatches_allowed):
	num_mismatch = 0
	matches = []
	rc_primer = ReverseComplement(primer)

	## primer_re = re.compile(primer, re.IGNORECASE)
	## rc_primer_re = re.compile(rc_primer, re.IGNORECASE)
	while (num_mismatch <= mismatches_allowed) and not matches:
		if num_mismatch > 0:
			print primer.name, 'TRYING MISMATCH:', num_mismatch
		primer_list = scan_insert_string(primer, '.', num_mismatch)
		rc_primer_list = scan_insert_string(rc_primer, '.', num_mismatch)
		for mismatch_primer, mismatch_rc_primer in zip(primer_list, rc_primer_list):
			primer_re = re.compile(mismatch_primer)
			rc_primer_re = re.compile(mismatch_rc_primer)
			for sequence_label in sequence_hash:
				sequence = sequence_hash[sequence_label]
				for primer_match in primer_re.finditer(sequence):
					matches.append(PrimerMatch(primer_match.start()+1,
											   primer_match.end(),'f',
											   sequence_label, num_mismatch))

				for rc_primer_match in rc_primer_re.finditer(sequence):
					matches.append(PrimerMatch(rc_primer_match.start()+1,
											   rc_primer_match.end(),'r',
											   sequence_label, num_mismatch))
		num_mismatch +=1
	return matches


####----------------------------------------
##def scan_insert_string(original_string, inserted_string='.', num_inserts=1):
##	inserted_strings = []
##	for i in range(len(original_string)-(num_inserts-1)):
##		new_string = original_string[:i] + inserted_string*num_inserts + original_string[i+num_inserts:]
##		inserted_string.append(new_string)
#### insert_wild('abcdefghijk', num_inserts=2)
####----------------------------------------
##----------------------------------------
##def scan_insert_string(original_string, insert='.', num_inserts=1):
##	if num_inserts < 0 :
##		raise ValueError
##	elif num_inserts == 0:
##		return (original_string,)
##	elif num_inserts == 1:
##		inserted_strings = []
##		for i in range(len(original_string)):
##			new_string = original_string[:i] + insert + original_string[i+1:]
##			inserted_strings.append(new_string)
##		return inserted_strings
##	else:
##		inserted_strings = []
##		for returned_string in scan_insert_string(original_string, insert, num_inserts-1):
##			inserted_strings.extend(scan_insert_string(returned_string, insert , 1))
##		return inserted_strings
									
#### insert_wild('abcdefghijk', num_inserts=2)
##----------------------------------------
##----------------------------------------
def scan_insert_string(instring, insert='.', num_inserts=1):
	if num_inserts < 0 :
		raise ValueError
	elif len(instring) == 1:
		return (instring,)
	elif len(instring) == 0:
		return ('',)
	elif num_inserts == 0:
		return (instring,)
	else :
		inserted_strings = []
		for i in range(len(instring)):
			for prefix in scan_insert_string(instring[:i], insert, num_inserts-1):
				for sufix in scan_insert_string(instring[i+1:], insert, num_inserts-1):
					new_string = prefix + insert + sufix
					inserted_strings.append(new_string)
		return inserted_strings
##----------------------------------------
def FindPrimedProduct(primer_pair, max_primed_distance):
	"""
	max_primed_distance : the maximum allowed distance between the 3' ends of the primers
	"""
	primed_product = []
	primer_matches = primer_pair.for_primer_matches + primer_pair.rev_primer_matches
	for index1 in range(len(primer_matches)):
		match1 = primer_matches[index1]
		for index2 in range(index1+1, len(primer_matches)):
			match2 = primer_matches[index2]
			if match1.chrom <> match2.chrom:
				continue
			elif match1.strand == match2.strand:
				continue
			if match1.strand == 'f':
				for_strand = match1
				rev_strand = match2
			else:
				for_strand = match2
				rev_strand = match1
			if (rev_strand.start <= for_strand.end):
				continue
			elif ((rev_strand.start - for_strand.end) - 1) <= max_primed_distance:
				primer_pair.AddProduct((for_strand.start, rev_strand.end,
										(rev_strand.end-for_strand.start)+1, for_strand.chrom))

	

def TestStringFindFindPrimerSites(primer, sequence_hash, mismatches_allowed):
	mismatches_count = 0
	matches = []
	rc_primer = ReverseComplement(primer)

	for sequence_label in sequence_hash:
		sequence = sequence_hash[sequence_label]
##-----------------------------------------------------------------------------
		index = -1
		while 1:
			index = sequence.find(primer, index+1)
			if index < 0:
				break
			else:
				matches.append((index+1,'f', sequence_label, mismatches_count))
##-----------------------------------------------------------------------------
##-----------------------------------------------------------------------------
		index = -1
		while 1:
			index = sequence.find(rc_primer, index+1)
			if index < 0:
				break
			else:
				matches.append((index+1,'r', sequence_label, mismatches_count))
##-----------------------------------------------------------------------------
	return matches


def ParseSequence(seq_filename):
	seq_filehandle = file(seq_filename)
	comment_line = seq_filehandle.readline()
	if chrom_comment_label_re.search(comment_line):
		chrom_label = chrom_comment_label_re.search(comment_line).group(1)
	elif mito_comment_label_re.search(comment_line):
		chrom_label = mito_comment_label_re.search(comment_line).group(1)
	else:
		raise StandardError, "Can't find chromosome label in sequence comment line:\n" + comment_line
	
	sequence = seq_filehandle.read()
	sequence = sequence.replace('\n', '').upper()
	if not dna_re.search(sequence):
		raise StandardError, 'Sequence' + seq_filename + 'contains non-base characters'

	seq_filehandle.close()
	return sequence, chrom_label

def ParsePrimers(primer_filename, for_seq_to_remove, rev_seq_to_remove):
	primer_filehandle = file(primer_filename)
	## for_seq_to_remove = 'CCGCTGCTAGGCGCGCCGTG'
	## rev_seq_to_remove = 'GCAGGGATGCGGCCGCTGAC'
	primer_hash = {}
	for_seq_to_remove_re = re.compile('^' + for_seq_to_remove + '([acgt]+)$', re.IGNORECASE)
	rev_seq_to_remove_re = re.compile('^' + rev_seq_to_remove + '([acgt]+)$', re.IGNORECASE)
	header_line = primer_filehandle.readline()
	print ':'.join(header_line.split())
	for line in primer_filehandle:
		if empty_line_re.search(line):
			continue
		line = line.strip()
		if len(line.split('\t')) <> 11:
			print len(line.split('\t')), line
		
		(plate, well, name, chrom, start, end, size,
		 for_seq_num, for_seq, rev_seq_num, rev_seq) = line.split('\t')

		if for_seq_to_remove_re.search(for_seq):
			for_seq = for_seq_to_remove_re.search(for_seq).group(1)
		else:
			print "Couldn't find", for_seq_to_remove, 'in', for_seq
		if rev_seq_to_remove_re.search(rev_seq):
			rev_seq = rev_seq_to_remove_re.search(rev_seq).group(1)
		else:
			print "Couldn't find", rev_seq_to_remove, 'in\n             :' + rev_seq
##		for_seq = for_seq_to_remove_re.search(for_seq).group(1)
##		rev_seq = rev_seq_to_remove_re.search(rev_seq).group(1)
		primer_hash[name] = PrimerPair(plate, well, name, chrom, start, end, size,
									   for_seq_num, for_seq.upper(), rev_seq_num, rev_seq.upper())

	primer_filehandle.close()
	return primer_hash

def ParsePrimersList(primer_filename, for_seq_to_remove, rev_seq_to_remove):
	primer_list = []
	for_seq_to_remove_re = re.compile('^' + for_seq_to_remove + '([acgt]+)$', re.IGNORECASE)
	rev_seq_to_remove_re = re.compile('^' + rev_seq_to_remove + '([acgt]+)$', re.IGNORECASE)

	primer_filehandle = file(primer_filename)
	# header_line = reader.next()
	header_line = primer_filehandle.readline()
	header_line = header_line.strip()
	reader = csv.reader(primer_filehandle,dialect='excel')
	## reader = csv.reader(file(primer_filename),dialect='excel')

	for row in reader:
		if len(row) <> 13:
			print >>sys.stderr, row
		ORF = row[3]
		for_seq = row[5]
		rev_seq = row[7]

		if for_seq_to_remove_re.search(for_seq):
			for_seq = for_seq_to_remove_re.search(for_seq).group(1)
		else:
			print "Couldn't find", for_seq_to_remove, 'in', for_seq
		if rev_seq_to_remove_re.search(rev_seq):
			rev_seq = rev_seq_to_remove_re.search(rev_seq).group(1)
		else:
			print "Couldn't find", rev_seq_to_remove, 'in\n             :' + rev_seq
		primer_list.append((ORF, for_seq.upper(), rev_seq.upper()))
	return primer_list, header_line





##	primer_list = []
##	for_seq_to_remove_re = re.compile('^' + for_seq_to_remove + '([acgt]+)$', re.IGNORECASE)
##	rev_seq_to_remove_re = re.compile('^' + rev_seq_to_remove + '([acgt]+)$', re.IGNORECASE)
##	header_line = primer_filehandle.readline()
##	print ':'.join(header_line.split())
##	for line in primer_filehandle:
##		if empty_line_re.search(line):
##			continue
##		line = line.strip()
##		if len(line.split('\t')) <> 13:
##			print len(line.split('\t')), '*', ':'.join(line.split())
####		print len(line.split('\t'))
##		(Sort,Plate,Well,ORF,For_seq_no,for_seq,Rev_seq_no,rev_seq,
##		 SGD_ID,Gene,Chr,Start,End) =line.split('\t')

##		if for_seq_to_remove_re.search(for_seq):
##			for_seq = for_seq_to_remove_re.search(for_seq).group(1)
##		else:
##			print "Couldn't find", for_seq_to_remove, 'in', for_seq
##		if rev_seq_to_remove_re.search(rev_seq):
##			rev_seq = rev_seq_to_remove_re.search(rev_seq).group(1)
##		else:
##			print "Couldn't find", rev_seq_to_remove, 'in\n             :' + rev_seq
##		primer_list.append((ORF, for_seq.upper(), rev_seq.upper()))
##	primer_filehandle.close()
##	return primer_list

def old_main():
	allowed_mismatches = 0
	for_seq_to_remove = sys.argv[1]
	rev_seq_to_remove = sys.argv[2]
	primer_filename = sys.argv[3]
	primer_hash = ParsePrimers(primer_filename, for_seq_to_remove, rev_seq_to_remove)
	print 'Found', len(primer_hash), 'primer pairs'
	
	sequence_hash = {}
	for seq_filename in sys.argv[4:]:
		sequence, sequence_label = ParseSequence(seq_filename)
		sequence_hash[sequence_label] = sequence

	counter = 0
	print 'primer pair number:'
	for primer_pair_name in primer_hash:
		sys.stdout.write(`counter` +  ', ')
		## sys.stdout.flush()
		counter += 1
		for_primer = primer_hash[primer_pair_name].for_seq
		rev_primer = primer_hash[primer_pair_name].rev_seq

		primer_hash[primer_pair_name].AddForMatches(FindPrimerSites(for_primer, sequence_hash, allowed_mismatches))
		primer_hash[primer_pair_name].AddRevMatches(FindPrimerSites(rev_primer, sequence_hash, allowed_mismatches))


##	for primer_pair_name in primer_hash:
		print primer_pair_name, 'For', primer_hash[primer_pair_name].for_primer_matches
		print primer_pair_name, 'Rev', primer_hash[primer_pair_name].rev_primer_matches

def main():
	def usage():
		print >>sys.stderr, 'usage:', sys.argv[0], 'FOR_SEQ_TO_REMOVE REV_SEQ_TO_REMOVE PRIMER_FILE'
	if len(sys.argv) <> 4:
		usage()
		sys.exit()
	for_seq_to_remove = sys.argv[1]
	rev_seq_to_remove = sys.argv[2]
	primer_filename = sys.argv[3]
	## primer_hash = ParsePrimers(primer_filename, for_seq_to_remove, rev_seq_to_remove)
	primer_list,header_line = ParsePrimersList(primer_filename, for_seq_to_remove, rev_seq_to_remove)
	print >>sys.stderr, 'Found', len(primer_list), 'primer pairs'
	
	print '#', header_line
	for primer_name, for_primer, rev_primer in primer_list:
		if whitespace_re.search(primer_name):
			primer_name = whitespace_re.sub('~', primer_name)
			print >>sys.stderr, 'Replacing whitespace in primer name', primer_name, 'with "~"'
		print '\t'.join((primer_name, for_primer, rev_primer))

def new_main():
	allowed_mismatches = 0
	primer_filename = sys.argv[1]
	primer_hash = ParsePrimers(primer_filename)
	print 'Found', len(primer_hash), 'primer pairs'
	
	sequence_hash = {}
	for seq_filename in sys.argv[2:]:
		sequence, sequence_label = ParseSequence(seq_filename)
		sequence_hash[sequence_label] = sequence

	counter = 0
	print 'primer pair number:'
	for primer_pair_name in primer_hash:
		if counter % 100 == 0:
			sys.stdout.write(`counter` +  ', ')
			sys.stdout.flush()
		counter += 1
		for_primer = primer_hash[primer_pair_name].for_seq
		rev_primer = primer_hash[primer_pair_name].rev_seq

		primer_hash[primer_pair_name].AddForMatches(IterFindPrimerSites(for_primer, sequence_hash, allowed_mismatches))
		primer_hash[primer_pair_name].AddRevMatches(IterFindPrimerSites(rev_primer, sequence_hash, allowed_mismatches))
		## print primer_pair_name, 'For', primer_hash[primer_pair_name].for_primer_matches
		## print primer_pair_name, 'Rev', primer_hash[primer_pair_name].rev_primer_matches
##	print '\n' * 10, 'MISSING PRIMERS:'
##	for primer_pair_name in primer_hash:
		if len(primer_hash[primer_pair_name].for_primer_matches) == 0:
			print primer_pair_name, 'For', primer_hash[primer_pair_name].for_primer_matches
		if len(primer_hash[primer_pair_name].rev_primer_matches) == 0:
			print primer_pair_name, 'Rev', primer_hash[primer_pair_name].rev_primer_matches

def new_main_mismatch():
	allowed_mismatches = 3
	primer_filename = sys.argv[1]
	primer_hash = ParsePrimers(primer_filename)
	print 'Found', len(primer_hash), 'primer pairs'
	
	sequence_hash = {}
	for seq_filename in sys.argv[2:]:
		sequence, sequence_label = ParseSequence(seq_filename)
		sequence_hash[sequence_label] = sequence

	counter = 0
	print 'primer pair number:'
	for primer_pair_name in primer_hash:
		if counter % 100 == 0:
			sys.stdout.write(`counter` +  ', ')
			sys.stdout.flush()
		counter += 1
		for_primer = primer_hash[primer_pair_name].for_seq
		rev_primer = primer_hash[primer_pair_name].rev_seq

		primer_hash[primer_pair_name].AddForMatches(MismatchesIterFindPrimerSites(for_primer, sequence_hash, allowed_mismatches))
		primer_hash[primer_pair_name].AddRevMatches(MismatchesIterFindPrimerSites(rev_primer, sequence_hash, allowed_mismatches))
		## print primer_pair_name, 'For', primer_hash[primer_pair_name].for_primer_matches
		## print primer_pair_name, 'Rev', primer_hash[primer_pair_name].rev_primer_matches

##	print '\n' * 10, 'MISSING PRIMERS:'
##	for primer_pair_name in primer_hash:
		if len(primer_hash[primer_pair_name].for_primer_matches) == 0:
			print counter
			print primer_pair_name, 'For', primer_hash[primer_pair_name].for_primer_matches
		if len(primer_hash[primer_pair_name].rev_primer_matches) == 0:
			print counter
			print primer_pair_name, 'Rev', primer_hash[primer_pair_name].rev_primer_matches

def mismatch_find_pcr_product():
	allowed_mismatches = 3
	primer_filename = sys.argv[1]
	primer_hash = ParsePrimers(primer_filename)
	print 'Found', len(primer_hash), 'primer pairs'
	
	sequence_hash = {}
	for seq_filename in sys.argv[2:]:
		sequence, sequence_label = ParseSequence(seq_filename)
		sequence_hash[sequence_label] = sequence

	counter = 0
	print 'primer pair number:'
	for primer_pair_name in primer_hash:
		if counter % 100 == 0:
			sys.stdout.write(`counter` +  ', ')
			sys.stdout.flush()
		counter += 1
		for_primer = primer_hash[primer_pair_name].for_seq
		rev_primer = primer_hash[primer_pair_name].rev_seq

		primer_hash[primer_pair_name].AddForMatches(MismatchesIterFindPrimerSites(for_primer, sequence_hash, allowed_mismatches))
		primer_hash[primer_pair_name].AddRevMatches(MismatchesIterFindPrimerSites(rev_primer, sequence_hash, allowed_mismatches))

	print 'Finding PCR products'
	for primer_pair_name in primer_hash:
		FindPrimedProduct(primer_hash[primer_pair_name], 5000)
		print primer_hash[primer_pair_name].name, 
		if not primer_hash[primer_pair_name].pcr_products:
			print 'NO PRODUCT FOUND'
		else:
			print primer_hash[primer_pair_name].pcr_products


def test():
	import time
	for_seq_to_remove = 'CCGCTGCTAGGCGCGCCGTG'
	rev_seq_to_remove = 'GCAGGGATGCGGCCGCTGAC'

	YBLWtau1        = 'GTATTAATGAAGAAGTGAGTACTGATCT'
	iYBLWdelta3	    = 'TGAGGATTCGGGTAAAATAGGG'
	iYBLWtau1	    = 'CGTTCCAACAGTCTCGATCCA'
	itC_GCA_P2	    = 'CGCCATACGAGCTTTTGAATT'
	iYDR265W	    = 'CCTGCGGCAATAGAGGAACTA'
	iYLR460C_dash_1	= 'TTTCCAATTCTTACTCCCCACTATC'
	tC_GCA_P2	    = 'AAGCTCGCACTCAGGATCG'

	primer_hash = {	'iYBLWdelta3'     : iYBLWdelta3,
					'iYBLWtau1'       : iYBLWtau1,
					'itC_GCA_P2'      : itC_GCA_P2,
					'iYDR265W'        : iYDR265W,
					'iYLR460C_dash_1' : iYLR460C_dash_1,
					'YBLWtau1'        : YBLWtau1,
					'tC_GCA_P2'       : tC_GCA_P2
					}

	primer_hash = {	'iYBLWdelta3'     : iYBLWdelta3	}

	allowed_mismatches = 0
	
	sequence_hash = {}
	for seq_filename in sys.argv[2:]:
		sequence, sequence_label = ParseSequence(seq_filename)
		sequence_hash[sequence_label] = sequence


##	for test_function in (TestStringFindFindPrimerSites,
##						  FindIterFindPrimerSites,
##						  TestStringFindFindPrimerSites,
##						  FindIterFindPrimerSites):
	for test_function in (IterFindPrimerSites,FindPrimerSites):
		iterations = 1
		start = time.clock()
		for count in range(iterations):
			tau_matches = []
			other_matches = []
			match_strings = []
			for primer_name in primer_hash:
				primer = primer_hash[primer_name]
				match_strings.append(primer_name + str(test_function(primer, sequence_hash, allowed_mismatches)))
		stend = time.clock()
		print test_function.__name__, ':', stend - start

		for cur_string in match_strings:
			print cur_string
		

if __name__ == '__main__':
	##test()
	## new_main()
	## new_main_mismatch()
	## mismatch_find_pcr_product()
	main()
