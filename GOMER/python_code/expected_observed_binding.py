"""
definitions:

for each TF
threshold_rank = ceiling of (# features IPed)/(1-FDR)

For each feature count:
1. number of times it is pulled down
2. number of times it's rank for a given TF is less than or equal to the threshold_rank for that TF
"""


from __future__ import division
division

import re
import sys
import glob
import os
import math
from parse_gomer_output import ParseGomerOutput

empty_re = re.compile('^\s*$')
gomer_output_filename_re = re.compile('^(\w+)\_\d+\.trainingset\.seq\.\d+\.bp\.reformat\.pwm\.out$')


def usage():
	print >>sys.stderr, '\nusage:', os.path.basename(sys.argv[0]), 'FDR GOMER_OUTPUT_DIRECTORY IPed_FEATURES_DIRECTORY\n\n'

output_handle = sys.stdout
def main():
	if len(sys.argv) ==4:
		false_discovery_rate = float(sys.argv[1])
		gomer_output_directory = sys.argv[2]
		IPed_features_directory = sys.argv[3]
		gomer_output_filename_list = glob.glob(os.path.join(gomer_output_directory, '*'))
		# IPed_feature_filenames = glob.glob(os.path.join(IPed_features_directory, '*'))
	else:
		usage()
		sys.exit(2)


	feature_IP_counts = {}
	feature_threshold_counts = {}
	print '# Num Files:', len(gomer_output_filename_list)
	for filename in gomer_output_filename_list:
		if gomer_output_filename_re.match(os.path.basename(filename)):
			tf_name = gomer_output_filename_re.match(os.path.basename(filename)).group(1)
			IPed_features_filename = os.path.join(IPed_features_directory, tf_name + 'fdr0.1.list')
		else:
			print >>sys.stderr, "File name doesn't match pattern (TFNAME_*.trainingset.seq.*.bp.reformat.pwm.out):", os.path.basename(filename)
			sys.exit(1)
		handle = file(filename)
		(all_feature_hash,
		 regulated_hash,
		 unknown_feature_hash,
		 gomer_output_skipped_lines,
		 param_hash) = ParseGomerOutput(handle)
		# list_of_feature_hashes.append(all_feature_hash)

		cur_IPed_features = parse_IPed_features_file(file(IPed_features_filename))
		num_IPed_features = len(cur_IPed_features)
		cur_IPed_features_dict = dict(zip(cur_IPed_features, [None]*len(cur_IPed_features)))
		threshold_rank = int(math.ceil(num_IPed_features/(1-false_discovery_rate)))
		rank_sorted_features = [(feature.rank, feature) for feature in
								[all_feature_hash[feature_name] for feature_name in all_feature_hash]]
		rank_sorted_features.sort()
		rank, feature = rank_sorted_features[threshold_rank-1]
		if rank <> threshold_rank:
			raise "rank <> threshold_rank - rank:" + str(rank) + ' threshold_rank:' + str(threshold_rank)
		for feature_name in all_feature_hash:
			if feature_name in cur_IPed_features_dict:
				feature_IP_counts[feature_name] = 1 + feature_IP_counts.get(feature_name, 0)
			else:
				feature_IP_counts[feature_name] = feature_IP_counts.get(feature_name, 0)
				
			if all_feature_hash[feature_name].rank <= threshold_rank:
				feature_threshold_counts[feature_name] = 1 + feature_threshold_counts.get(feature_name, 0)
			else:
				feature_threshold_counts[feature_name] = feature_threshold_counts.get(feature_name, 0)


	print '## IP_count\tthreshold_count'
	for cur_feature_name in feature_IP_counts:
		print '#', cur_feature_name
		print feature_IP_counts[cur_feature_name], feature_threshold_counts[cur_feature_name]

		
def parse_IPed_features_file(filehandle):
	IPed_features_list = []
	for line in filehandle:
		if not empty_re.match(line):
			IPed_features_list.append(line.strip())
	return IPed_features_list


if __name__ == "__main__":
	main()
