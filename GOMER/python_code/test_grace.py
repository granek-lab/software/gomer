from xmgrace_interface import grace_no_safe as grace
g = grace()

g.Command ("g0.s0 point 0,0")
g.Command ("g0.s0 point 0.7,0.3")
g.Command ("g0.s0 point 1,1")
g.Command ("g0.s0 symbol 1")
g.Command ("g0.s0 symbol size 0.5")
g.Command ("g0.s0 symbol fill pattern 1")
g.Command ("redraw")
g.Flush()

g.Command ("g0.s1 point 0.2,0.1")
g.Command ("g0.s1 point 0.3,0.2")
g.Command ("g0.s1 point 0.4,0.5")
g.Command ("g0.s1 point 0.5,0.52")
g.Command ("g0.s1 symbol 2")
g.Command ("g0.s1 symbol size 0.5")
g.Command ("g0.s1 symbol fill pattern 1")
g.Command ("redraw")
g.Flush()

g.Command ('g0 xaxis label "blah"')
g.Command ('g0 yaxis label "y blah"')
g.Command ("redraw")
g.Flush()
g.Command ('title "I am plot"')
g.Command ('subtitle "Hear me roar"')
g.Command ("redraw")
g.Flush()

g.Command ('g0.s0 legend "data one"')
g.Command ('g0.s1 legend "data two"')
g.Command ('legend on')
g.Command ('legend loctype world')
g.Command ('legend 0,1')
g.Command ("redraw")
g.Flush()



g.Command ('autoscale')
g.Command ("redraw")
g.Flush()


g.Command ("page background fill off")
g.Command ("redraw")
g.Flush()

device = 'PostScript'
option = 'greyscale'
# g.Command ('hardcopy device "%s"' % device.lower())
# g.Command ('device "%s" op "%s"' % (device, option))

g.Command ('hardcopy device "eps"')
g.Command ('device "EPS" op "bbox:tight"')
output_filename= '/tmp/blah.eps'

# @device "X11" dpi 72
# device "EPS" op "bbox:page"
g.Command ('print to "%s"' % output_filename)
g.Command ('print')
g.Flush()


##output_filename = '/tmp/blah.ps'
##device= 'postscript'
##g.Command ('hardcopy device "%s"' % device)
##g.Command ('print to "%s"' % output_filename)
##g.Command ('print')
##g.Flush()

##g.Command ('exit')
##g.Flush()
