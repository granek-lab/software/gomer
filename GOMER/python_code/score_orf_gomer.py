#! /usr/bin/env python
from __future__ import division
#@+leo
#@+node:0::@file score_orf_gomer.py
#@+body
#@@first
#@@first
#@@language python

"""
*Description:

Module for functions for determining the score of an ORF.

RegulatoryRegionScoreOrf - given a several parameters:
	- orf reference
	- regulatory region model
	- binding site model
	- free concentration factor

	calculates a score for the orf as to how "well" it is regulated.

	The reverse complement windows are treated the same as forward windows, the
	calculation of the orf score simply works on twice as many windows as one
	would expect from considering the number of frames on the forward
	orienation of the sequence.
	In other words, for the calculation of the orf score, all windows are
	treated the same - whether the window is forward and reverse complement.
"""
##import os
##os.environ['PYCHECKER'] = '--allglobals --stdlib'
##import pychecker.checker
##import check_python_version
##check_python_version.CheckVersion()

import sys
# import operator

from globals_gomer import FORWARD, REVCOMP
from integer_string_compare import sort_numeric_string


## #@+others
## #@+node:1::RegulatoryRegionScoreOrf
## #@+body
## def RegulatoryRegionScoreOrf(orf, regulatory_region_model, binding_site_model, free_concentration_factor):
## 	regulatory_region_ranges = regulatory_region_model.GetRegulatoryRegionRanges(orf)
## 	cumulative_denominator_product = 1
## 	hit_list = orf.Chromosome.GetHitList(binding_site_model.Descriptor)
## 	for cur_range in regulatory_region_ranges:
## 		(start_hitlist_index, stop_hitlist_index) = cur_range
## 		for cur_hitlist_index in xrange(start_hitlist_index, (stop_hitlist_index + 1)):
## 			cur_forward_hit_score = hit_list.Forward[cur_hitlist_index]
## 			cur_revcomp_hit_score = hit_list.ReverseComplement[cur_hitlist_index]
## 			cur_site_kappa = regulatory_region_model.GetSiteKappa(orf, cur_hitlist_index)
## 			##DEBUG print 'Forward:', cur_hitlist_index, free_concentration_factor, cur_forward_hit_score, cur_site_kappa
## 			forward_denominator = 1 + (free_concentration_factor *
## 									   cur_forward_hit_score *
## 									   cur_site_kappa)
## 			##denominator_list.append(forward_denominator)
## 			cumulative_denominator_product *= forward_denominator


## 			##DEBUG print 'Revcomp:', cur_hitlist_index, free_concentration_factor, cur_revcomp_hit_score, cur_site_kappa
## 			revcomp_denominator = 1 + (free_concentration_factor *
## 									   cur_revcomp_hit_score *
## 									   cur_site_kappa)
## 			##denominator_list.append(revcomp_denominator)
## 			cumulative_denominator_product *= revcomp_denominator
			

## #@@code			
## 	##denominator_list.sort() # THIS IS A SILLY SORT FOR ANAL FLOATING POINT ERROR AVOIDANCE
## 	#*******************************************************
## 	## print >> sys.stderr, 'DENOMINATOR_LIST:', denominator_list
## 	## sys.exit(1)
## 	#*******************************************************
## 	##cumulative_denominator_product = reduce(operator.mul, denominator_list, 1)
## 	orf_score = 1 - (1/cumulative_denominator_product)
## 	return orf_score



## #@-body
## #@-node:1::RegulatoryRegionScoreOrf
## #@+node:2::RegulatoryRegionScoreOrfSlice
## #@+body
def RegulatoryRegionScoreOrfSlice(orf, regulatory_region_model, binding_site_model, free_concentration_factor):
	regulatory_region_ranges = regulatory_region_model.GetRegulatoryRegionRanges(orf)
	# cumulative_denominator_product = 1
	cumulative_product = 1
	hit_list = orf.Chromosome.GetHitList(binding_site_model.MD5)
	for cur_range in regulatory_region_ranges:
		(first_i_hitindex, last_i_hitindex) = cur_range

		stop_i_hitindex = last_i_hitindex + 1

		forward_Ka_list = hit_list.Forward[first_i_hitindex :stop_i_hitindex]
		revcomp_Ka_list = hit_list.ReverseComplement[first_i_hitindex :stop_i_hitindex]
		if len(forward_Ka_list) <> len(revcomp_Ka_list):
			raise StandardError, 'Length of forward and revcomp slices should be identical!!!'

		kappa_indices = xrange(first_i_hitindex,stop_i_hitindex)
##		forward_denominator_list = [None] * (stop_i_hitindex - first_i_hitindex)
##		revcomp_denominator_list = [None] * (stop_i_hitindex - first_i_hitindex)
		for index in xrange(len(forward_Ka_list)):
			kappa_index = kappa_indices[index]
##------------------------------------------------------------------------------
			cur_forward_Ka = forward_Ka_list[index]
			## START FILTER
##			if cur_forward_Ka == 0:
##				forward_denominator_list[index] = 1
##			else:
			if cur_forward_Ka > 0:
			## END FILTER (to remove FILTER, also unindent following code block)
				for_site_kappa = regulatory_region_model.GetSiteKappa(orf, kappa_index, FORWARD)
				forward_denominator = 1 + (free_concentration_factor *
										   cur_forward_Ka *
										   for_site_kappa)
##				forward_denominator_list[index] = forward_denominator
				# cumulative_denominator_product *= forward_denominator
				cumulative_product *= 1/forward_denominator
##------------------------------------------------------------------------------
			cur_revcomp_Ka = revcomp_Ka_list[index]
			## START FILTER
##			if cur_revcomp_Ka == 0:
##				revcomp_denominator_list[index] = 1
##			else:
			if cur_revcomp_Ka > 0:
			## END FILTER (to remove FILTER, also unindent following code block)
				revcomp_site_kappa = regulatory_region_model.GetSiteKappa(orf, kappa_index, REVCOMP)
				revcomp_denominator = 1 + (free_concentration_factor *
										   cur_revcomp_Ka *
										   revcomp_site_kappa)
##				revcomp_denominator_list[index] = revcomp_denominator
				# cumulative_denominator_product *= revcomp_denominator
				cumulative_product *= 1/revcomp_denominator
##		denominator_list = denominator_list + forward_denominator_list + revcomp_denominator_list
##	denominator_list.sort() # THIS IS A SILLY SORT FOR ANAL FLOATING POINT ERROR AVOIDANCE
	#*******************************************************
	##print >> sys.stderr, 'DENOMINATOR_LIST:', denominator_list
	##sys.exit(1)
	#*******************************************************
##	cumulative_denominator_product = reduce(operator.mul, denominator_list, 1)
	# orf_score = 1 - (1/cumulative_denominator_product)
	orf_score = 1 - cumulative_product
	return orf_score

##==============================================================================
##==============================================================================
def score_features(feature_types, chromosome_hash, binding_site_model, free_concentration_factor,
			   regulatory_region_model):
	list_of_score_tuples = []
	sorted_chromosome_label_list = sort_numeric_string(chromosome_hash.keys())
	if __debug__:
		print >>sys.stderr, 'Scoring Feature Types:', feature_types
	for chromosome_label in sorted_chromosome_label_list:
		chromosome = chromosome_hash[chromosome_label]
## 		sys.stderr.write ('\r' + ' '*80 + '\r')
## 		print >>sys.stderr, 'Scoring Features of Chromosome:', chromosome_label,
		if __debug__:
			# print >>sys.stderr, 'Scoring Features of Chromosome:', chromosome_label
			print >>sys.stderr, 'Scoring Features', feature_types, 'of Chromosome:', chromosome_label
		for cur_feature in chromosome.FeatureIterator(feature_types):
			cur_feature_score = RegulatoryRegionScoreOrfSlice(cur_feature,
															  regulatory_region_model,
															  binding_site_model,
															  free_concentration_factor)
			list_of_score_tuples.append(tuple((cur_feature_score,cur_feature)))
	return list_of_score_tuples

#@-body
#@-node:2::RegulatoryRegionScoreOrfSlice
#@-others


#@-body
#@-node:0::@file score_orf_gomer.py
#@-leo
