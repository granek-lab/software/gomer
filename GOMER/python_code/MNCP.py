#! /usr/bin/env python
from __future__ import division
#@+leo
#@+node:0::@file MNCP.py
#@+body
#@@first
#@@first
#@@language python


#@<< MNCP declarations >>
#@+node:1::<< MNCP declarations >>
#@+body
#! /usr/bin/env python
from __future__ import division

##import os
##os.environ['PYCHECKER'] = '--allglobals --stdlib'
##import pychecker.checker

##import check_python_version
##check_python_version.CheckVersion()


import sys
import operator

##MINIMUM_PYTHON_VERSION_HEX = 0x20101f0
##if sys.hexversion < MINIMUM_PYTHON_VERSION_HEX:
##	print 'ERROR: This script is known to work with Python version 2.1.1 and higher!', '\n\nThe version on this system is :\n', sys.version, '\n\nVersion 2.1.1 might be available as "python2", you can try changing the first line of this file from:', '\n#! /usr/bin/env python\n\nto:', '\n#! /usr/bin/env python2'
##	sys.exit(1)


EXPECTED_ARGUMENTS = 2
##DEBUG = 0
##if DEBUG:
##	debug = sys.stderr
##else:
##	debug =open('/dev/null', 'w')


#	 debug = 

#@-body
#@-node:1::<< MNCP declarations >>


#@+others
#@+node:2::class MNCPValue
#@+body
class MNCPValue:

	#@+others
	#@+node:1::__init__
	#@+body
	def __init__ (self, value, group, original_position):
		self.value = value
		self.group = group
		self.original_position = original_position
		self.group_rankscore = ''
		self.combined_rankscore = ''
	
	#@-body
	#@-node:1::__init__
	#@-others


#@-body
#@-node:2::class MNCPValue
#@+node:3::usage
#@+body
def usage() :
	print (len(sys.argv) - 1), """arguments given\n\n\
	Must be given""", EXPECTED_ARGUMENTS, """arguments:\n\
	1. quoted list of values of
	2. quoted list of valies of

	OPTIONS:

	-h or --higher: higher values are better (default behavior is for lower values to be better)
	-t or --test: run test values
	"""

#@-body
#@-node:3::usage
#@+node:4::sortbyvalue
#@+body
# combined_list = []
# DATA_VALUE_INDEX = 0
# GROUP_INDEX = 1
# ORIGINAL_POSITION_INDEX = 2
# RANKSCORE_INDEX = 3
"""
group 'a' is the experimental group
group 'b' is the control group
"""

"""
function: sortbyvalue
input:	  list of MNCPValue instances
output:	  list of MNCPValue instances sorted by their 'value' attribute
"""
def sortbyvalue(list, higher_is_better=0): 
	nlist = map(lambda x: (x.value, x), list)
	nlist.sort()
	if higher_is_better:
		nlist.reverse()
	return map(lambda (key, x): x, nlist)

##def print_list_of_clarkevalues (list_to_print):
##	for instance in list_to_print:
##		debug.write ('('+ ','.join([str(instance.value),
##									str(instance.group),
##									str(instance.original_position),
##									str(instance.group_rankscore),
##									str(instance.combined_rankscore)])+') ')
##	debug.write('\n')
##	# print ''
	

#@-body
#@-node:4::sortbyvalue
#@+node:5::rankscore_combined
#@+body
def rankscore_combined (both_groups, higher_is_better=0):
	list_to_score = sortbyvalue (both_groups, higher_is_better)
	for index in range((len (list_to_score))-1,-1,-1):
		# print index
		if index == (len (list_to_score))-1:
			list_to_score[index].combined_rankscore = len (list_to_score)
		elif (list_to_score[index].value == list_to_score[index + 1].value):
			list_to_score[index].combined_rankscore = list_to_score[index + 1].combined_rankscore
		else :
			list_to_score[index].combined_rankscore = index + 1
			
	return list_to_score

#@-body
#@-node:5::rankscore_combined
#@+node:6::rankscore_group
#@+body
def rankscore_group (group, higher_is_better=0):
	list_to_score = sortbyvalue (group, higher_is_better)
	for index in range((len (list_to_score))-1,-1,-1):
		# print index
		if index == (len (list_to_score))-1:
			list_to_score[index].group_rankscore = len (list_to_score)
		elif (list_to_score[index].value == list_to_score[index + 1].value):
			list_to_score[index].group_rankscore = list_to_score[index + 1].group_rankscore
		else :
			list_to_score[index].group_rankscore = index + 1
			
	return list_to_score

#@-body
#@-node:6::rankscore_group
#@+node:7::calc_and_sum_score
#@+body
def calc_and_sum_score (experimental_group, control_group):
##	sum = 0.0
##	for member in experimental_group :
##		# score += ((len(control_group)/len(experimental_group)) * (member.group_rankscore/member.combined_rankscore))
##		# print (len(control_group) + len (experimental_group)), len (experimental_group), member.group_rankscore, member.combined_rankscore, float (float(member.group_rankscore)/float(member.combined_rankscore))
##		# score += float (float ( (len(control_group) + len (experimental_group))/len(experimental_group)) * float (float(member.group_rankscore)/float(member.combined_rankscore)))
##		# print member.group_rankscore, member.combined_rankscore
##		sum +=	float (float(member.group_rankscore)/float(member.combined_rankscore))
##		# print sum
	number_all = float (len(control_group) + len (experimental_group))
	number_exp = float (len(experimental_group))
	
	reg_pairs = [(member.combined_rankscore, member.group_rankscore)
			   for member in experimental_group]
	reg_pairs.sort()
	reg_pairs.reverse()
	reduce_sum = reduce(operator.add, [reg_rank/all_rank for all_rank, reg_rank in reg_pairs])
	return reduce_sum * (number_all/number_exp**2)
	## return sum * (number_all/number_exp**2)
	## return (float(sum)/float(number_exp)) * float(float(number_all)/float(number_exp))
	## return (sum/float (len(experimental_group))) * (float ((float (len(control_group) + len (experimental_group)))/float (len(experimental_group))))
	
	# return (score/(len(control_group) + len(experimental_group)))
	# return (score/(len(control_group)))

#@-body
#@-node:7::calc_and_sum_score
#@+node:8::MNCPScore
#@+body
# ***************************************************************************

def MNCPScore (a_values, b_values, higher_is_better=0):
	
	a_group = []
	for count in range(len(a_values)):
		a_group.append (MNCPValue(a_values[count], 'a', count))
	# print_list_of_clarkevalues (a_group)
	rankscore_group (a_group, higher_is_better) 
	# print_list_of_clarkevalues (a_group)

	b_group = []
	for count in range(len(b_values)):
		b_group.append (MNCPValue(b_values[count], 'b', count))
	# print_list_of_clarkevalues (b_group)
	rankscore_group (b_group, higher_is_better)
	# print_list_of_clarkevalues (b_group)

	# combined_list = a_group + b_group
	# print combined_list

	rankscore_combined (a_group + b_group, higher_is_better)

	# print combined_list
	## print_list_of_clarkevalues (a_group + b_group)
	return calc_and_sum_score(a_group, b_group)

#@-body
#@-node:8::MNCPScore
#@-others



if __name__ == '__main__':
	"""
	./MNCP.py "1 3 3 7" "2 3 6 8" 
	"""
	import getopt
	FALSE = 0
	TRUE = 1
	# print 'I am main'
	#**************************************************88

	

	try:
		opts, args = getopt.getopt(sys.argv[1:], "ht", ["higher","test"])
	except getopt.GetoptError:
		# print help information and exit:
		usage()
		sys.exit(2)
	# output = None
	higher_is_better = FALSE
	test_run = FALSE
	for o, a in opts:
		if o in ("-h", "--higher"):
			higher_is_better = TRUE
			print "higher_is_better"
		elif o in ("-t", "--test"):
			test_run = TRUE
			print "running test"

	if test_run:
		triplet_list = [((1,3,3,7),(2,3,6,8),1.3857142857142857),
						((28,16,6,34,20,15,14,21), (21,13,25,11,16,37,0,26), 1.0085858585858585),
						((23,17,28,29,19,9,12,24), (19,28,23,16,3,17,38,28), 1.0613636363636363), 
						((33,36,13,14,40,23,11,34), (28,31,39,1,34,34,23,35), 1.1597222222222223), 
						((1,0,27,11,27,12,19,31), (14,37,4,24,20,34,6,10), 1.3188339438339436)]

		for a_values,b_values,result in triplet_list:
			score = MNCPScore(a_values, b_values, higher_is_better)
##			if abs(score - result) < 1e-16:
			if score == result:
				passes = 'OK'
			else:
				passes = 'FAILURE'
			print 'MNCP is:', score, 'Should be:', result, passes, '\tA:', a_values, '  B:', b_values


		sys.exit(0)

	# sys.stderr.write (str(args) + '\n')
	# sys.stderr.write (str(len(sys.argv)) + '\n')
	# *****************************************************
	args.insert(0, sys.argv[0])		   
	if len(args) == (EXPECTED_ARGUMENTS + 1):
		a_values_string = args[1]
		b_values_string = args[2]
	else :
		usage()
		sys.exit(0)

	a_values = map (float, (a_values_string.split()))
	b_values = map (float, (b_values_string.split()))
	score = MNCPScore(a_values, b_values, higher_is_better)
	print 'MNCP score:', score

	# print 'MNCP score:', calc_and_sum_score(a_group, b_group)


"""
An alternative way to speed up sorts is to construct a list of tuples whose first element is a sort key that will sort properly using the default comparison, and whose second element is the original list element.

Suppose, for example, you have a list of tuples that you want to sort by the n-th field of each tuple. The following function will do that.

def sortby(list, n):
	nlist = map(lambda x, n=n: (x[n], x), list)
	nlist.sort()
	return map(lambda (key, x): x, nlist)

Here's an example use:

>>> list = [(1, 2, 'def'), (2, -4, 'ghi'), (3, 6, 'abc')]
>>> list.sort()
>>> list 
[(1, 2, 'def'), (2, -4, 'ghi'), (3, 6, 'abc')]
>>> sortby(list, 2)
[(3, 6, 'abc'), (1, 2, 'def'), (2, -4, 'ghi')]
>>> sortby(list, 1) 
[(2, -4, 'ghi'), (1, 2, 'def'), (3, 6, 'abc')]

"""

#@-body
#@-node:0::@file MNCP.py
#@-leo
