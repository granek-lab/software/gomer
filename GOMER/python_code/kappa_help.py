#! /usr/bin/env python
#@+leo
#@+node:0::@file test_reg_region_kappa.py
#@+body
#@@first
#@@language python

"""
The blue and red "curves" are made of lines of the length of the binding site (the default length for this program is 10bp).  The x-axis of the plot is "Site Index", and the regulatory region is defined in terms of site index values, (the first base of the site minus one).  Because of this, you will note that it appears that sites at the left end of the regulatory region have kappa values of zero, and that a few site past the right end of the regulatory region get get non-zero kappa values (this effect is particularly noticeable using, for example, the SingleSquareRegulatoryRegionModel).  This misleading effect occurs because of the facts mentioned above: the sites that "overlap" the left end of the regulatory region have indices that are less than the minimum index of the regulatory region, but because the length of the site is greater than one, the site overlaps the index of the regulatory region.  In other words, site number 99 will have 9 bases that "overlap" a regulatory region starting at index 100, but it does not fall within the regulatory region, so it automatically receives a kappa value of zero.  The reason for the right end of the regulatory region is similar.
"""

#@<<imports>>
#@+node:1::<<imports>>
#@+body
from __future__ import division, generators

import sys
import textwrap
# import getopt
from optparse import OptionParser





from runtime_import import RuntimeImport
# from gomer import parse_parameter_string
from gomer_load_functions import parse_parameter_string


try:
	import biggles
except ImportError:
	print >>sys.stderr, 'Module "biggles" is not available.  You will not be able to make kappagraphs'
	biggles = None

#@-body
#@-node:1::<<imports>>

## FLANK_LENGTH = 10
FLANK_LENGTH = 100


#@+others
#@+node:2::print_kappa_help
#@+body
def print_kappa_help(kappa_file):
	module = RuntimeImport(kappa_file)

	wrapper = textwrap.TextWrapper(subsequent_indent = ' '*5,
								   replace_whitespace = False,
								   break_long_words = False,
								   width = 80)
	## info_string = '\n' + '-'*60 + '\n'
	info_string = '\n'
	info_string += 'NAME: ' + module.NAME + '\n\n'
	info_string += 'TYPE: ' + module.TYPE + '\n\n'
	info_string += 'REQUIRES_STRAND: '
	if module.REQUIRES_STRAND:
		info_string += 'True\n\n'
	else:
		info_string += 'False\n\n'
	info_string += 'PARAMETERS:\n'
	for parameter in module.PARAMETERS:
		info_string += ' ' * 5 + parameter + ' = ' + str(module.PARAMETERS[parameter]).ljust(30) + '\n'
	info_string += '\nPARAMETER STRING EXAMPLES:\n'
	for example in module.PARAMETER_STRING_EXAMPLE:
		info_string += ' ' * 5 + '"' + example + '"\n'
	info_string += '\n'
	# info_string = wrapper.fill(info_string)

	wrapper = textwrap.TextWrapper(initial_indent = ' '*5,
								   subsequent_indent = ' '*5,
								   replace_whitespace = True,
								   break_long_words = False,
								   width = 80)

	## info_string += 'DESCRIPTION:\n' + wrapper.fill(module.DESCRIPTION)
	info_string += 'DESCRIPTION:\n' + module.DESCRIPTION

	print >> sys.stderr, info_string

#@-body
#@-node:2::print_kappa_help
#@+node:3::class Matrix
#@+body

class Matrix(object):

	#@+others
	#@+node:1::__init__
	#@+body
	def __init__(self, length, descriptor):
		self.Length = length
		self.Descriptor = descriptor
		self.MD5 = descriptor
	
	#@-body
	#@-node:1::__init__
	#@-others


#@-body
#@-node:3::class Matrix
#@+node:4::class Feature
#@+body

class Feature(object):

	#@+others
	#@+node:1::__init__
	#@+body
	def __init__(self, strand, start, stop, chrom):
		self.Strand = strand
		self.Start = start
		self.Stop = stop
		self.Name = ' '.join((strand, `start`, 'to', `stop`))
		self.Chromosome = chrom
		self.Length = self._length()
		
	def _length(self):
		return abs(self.Stop - self.Start) + 1

	#@-body
	#@-node:1::__init__
	#@-others


#@-body
#@-node:4::class Feature
#@+node:5::class Chrom
#@+body
class Chrom(object):

	#@+others
	#@+node:1::__init__
	#@+body
	def __init__(self, hitlist_size):
		self.hitlist_size = hitlist_size
	
	#@-body
	#@-node:1::__init__
	#@+node:2::GetHitListSize
	#@+body
	def GetHitListSize(self,name):
		return self.hitlist_size
	
	#@-body
	#@-node:2::GetHitListSize
	#@-others


#@-body
#@-node:5::class Chrom
#@+node:6::test_kappa
#@+body
def test_kappa(kappa_filename, parameter_string, output, print_parameters, single, data_output):
##====================================================
##====================================================
	parameter_dict = parse_parameter_string(parameter_string)
	module = RuntimeImport(kappa_filename)

	if print_parameters:
		print_parameters = parameter_dict
	else:
		print_parameters = ()

	if module.TYPE in ('RegulatoryRegionModel',
					   'CoordinateRegulatoryRegionModel',
					   'SequenceFeatureRegulatoryRegionModel'):
		bind_matrix = Matrix(10,'dummy1')
		## bind_matrix = Matrix(11,'dummy1')
		reg_region_model = module.RegulatoryRegionModel(bind_matrix, **parameter_dict)
		if module.TYPE in ('RegulatoryRegionModel',):
			use_strand = True
		elif module.TYPE in ('CoordinateRegulatoryRegionModel',
							 'SequenceFeatureRegulatoryRegionModel'):
			use_strand = False
		else:
			raise 'Unknown Model type: ' + module.TYPE
		test_regulatory_region_kappa(reg_region_model, module.NAME, use_strand,
									 bind_matrix, print_parameters, output, single, data_output)
	elif module.TYPE in ('CooperativityModel',):
		use_strand = False
		primary_matrix = Matrix(10,'primary')
		secondary_matrix = Matrix(14,'secondary')
		coop_model = module.CoopModel(primary_matrix, secondary_matrix, **parameter_dict)
		test_coop_kappa(coop_model, module.NAME, use_strand, primary_matrix,
						secondary_matrix, print_parameters, output, single)
	else:
		raise 'Unknown Model type: ' + module.TYPE


##--------------------------------------------------------------------			
def test_regulatory_region_kappa(reg_region_model, model_name, use_strand,
								 bind_matrix, parameter_dict, output, single, data_output):
	chrom = Chrom(10000)

	feature_list = []
	if use_strand:
##		feature_list.append(Feature('+', 500, 600, chrom))
		feature_list.append(Feature('+', 1000, 1200, chrom))
		feature_list.append(Feature('-', 2000, 1500, chrom))
		feature_list.append(Feature('+', 100, 400, chrom))
		feature_list.append(Feature('-', 9900, 9600, chrom))
	else:
##		feature_list.append(Feature('', 500, 800, chrom))
		feature_list.append(Feature('', 800, 1800, chrom))
#		feature_list.append(Feature('', 1000, 1200, chrom))
		feature_list.append(Feature('', 1500, 2000, chrom))
		feature_list.append(Feature('', 100, 400, chrom))
		feature_list.append(Feature('', 9600, 9900, chrom))
##====================================================
	if single:
		feature_list = feature_list[:1]
##====================================================
	## figure = PyChartFigure()
	param_string = ';'.join([key+'='+str(parameter_dict[key]) for key in parameter_dict])
##	figure = BigglesFigure(model_name) # + '('+param_string+')')


	## BEGIN hack1 ===========================
	## temp_hack_ranges = [((0, 1438),), ((1550, 3037),), ((0, 538),), ((9450, 9999),)]
	## temp_hack_ranges =    [((199, 2390),), ((899, 2590),),((0, 990),),((8999, 9999),)]
	## END   hack1 =============================

	if data_output:
		figure = DataFigure(data_output, model_name, param_string)
	else:
		figure = BigglesFigure(model_name, param_string)
	for feature_index in range(len(feature_list)):
		feature = feature_list[feature_index]
		reg_region_ranges = reg_region_model.GetRegulatoryRegionRanges(feature)
		print reg_region_ranges
		## BEGIN hack1 -------------------------------
		## hack_reg_region_ranges = temp_hack_ranges[feature_index]
		## print 'HACK:', hack_reg_region_ranges
		## reg_region_ranges = hack_reg_region_ranges
		## END   hack1 ---------------------------------
		hit_indices = []
		for start, stop in reg_region_ranges:
			hit_indices += range(start-FLANK_LENGTH, stop+FLANK_LENGTH+1)

		plus_strand_kappas = [reg_region_model.GetSiteKappa(feature, i, '+')
							  for i in hit_indices]
		##Negate the Site Kappa for the minus strand for plotting purposes
		minus_strand_kappas = [-(reg_region_model.GetSiteKappa(feature, i, '-'))
							  for i in hit_indices]

		figure.AddFeature(feature, hit_indices,
						  plus_strand_kappas, minus_strand_kappas,
						  bind_matrix.Length,
						  reg_region_ranges)
	figure.Display(output)
##--------------------------------------------------------------------			

##--------------------------------------------------------------------			
def test_coop_kappa(coop_model, model_name, use_strand,	primary_matrix,
					secondary_matrix, parameter_dict, output, single, data_output):
	chrom = Chrom(10000)
	FEATURE_TO_PRIMARY_SITE = 100
	feature_list = []
	feature_list.append(Feature('+', 500, 600, chrom))
	feature_list.append(Feature('-', 2000, 1500, chrom))
	feature_list.append(Feature('+', 100, 400, chrom))
	feature_list.append(Feature('-', 9900, 9600, chrom))
##====================================================
	if single:
		feature_list = feature_list[:1]
##====================================================
	## figure = PyChartFigure()
	param_string = ';'.join([key+'='+str(parameter_dict[key]) for key in parameter_dict])

	if data_output:
		figure = DataFigure(data_output, model_name, param_string)
	else:
		figure = BigglesFigure(model_name, param_string)
	for feature_index in range(len(feature_list)):
		feature = feature_list[feature_index]

		primary_site_strand = feature.Strand
		if feature.Strand == '+':
			primary_site_hitindex = feature.Start - FEATURE_TO_PRIMARY_SITE
		elif feature.Strand == '-':
			primary_site_hitindex = feature.Start + FEATURE_TO_PRIMARY_SITE
		else:
			raise

		coop_region_ranges = coop_model.GetCoopRanges(primary_site_hitindex, primary_site_strand, feature)
		print coop_region_ranges
		hit_indices = []

		for start, stop in coop_region_ranges:
			hit_indices += range(start-FLANK_LENGTH, stop+FLANK_LENGTH+1)

		plus_strand_kappas = [coop_model.GetSiteKappa(primary_site_hitindex, primary_site_strand, i, '+', feature)
							  for i in hit_indices]
		##Negate the Site Kappa for the minus strand for plotting purposes
		minus_strand_kappas = [-(coop_model.GetSiteKappa(primary_site_hitindex, primary_site_strand, i, '-', feature))
							  for i in hit_indices]
		figure.AddFeature(feature, hit_indices,
						  plus_strand_kappas, minus_strand_kappas,
						  secondary_matrix.Length,
						  coop_region_ranges, (primary_site_strand, primary_site_hitindex, primary_matrix.Length))
	figure.Display(output)
##--------------------------------------------------------------------			

class DataFigure(object):
	def __init__(self, data_output_filename, title='', param_string=''):
		self._title = title
		self._parameter_string = param_string
		self.outhandle = file(data_output_filename, 'w')
	def AddFeature(self, feature, hit_indices, plus_strand_kappas, minus_strand_kappas,
				   site_length, reg_region_ranges, primary_site=None):
		##++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
		print >>self.outhandle, '#############################################'
		if (len(plus_strand_kappas) + len(minus_strand_kappas)) > 0:
			all_kappas = plus_strand_kappas + minus_strand_kappas
			max_abs = max((abs(max(all_kappas)),
						   abs(min(all_kappas))))
			box_height = max_abs/2
		else:
			box_height = 1
		if feature.Strand == '+':
			bottom=0
			##left = feature.Start - bind_matrix.Length
			left = feature.Start-1
			right = feature.Stop-1
		elif feature.Strand == '-':
			bottom=-box_height
			left = feature.Start-1
			##right = feature.Stop - bind_matrix.Length
			right = feature.Stop-1
		else :
			bottom=-(box_height/2)
			left = feature.Start-1
			right = feature.Stop-1
		top= bottom + box_height

		# p.title = feature.Name
##++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
		if primary_site:
			primary_bh = box_height * 2
			primary_strand, primary_hitindex, primary_length = primary_site
			if primary_strand == '+':
				primary_b=0
			elif primary_strand == '-':
				primary_b=-primary_bh
			else:
				raise
			primary_l = primary_hitindex
			primary_r = primary_l + primary_length
			primary_t= primary_b + primary_bh
##			p.add(biggles.DataBox((primary_l,primary_b),
##								  (primary_r,primary_t), color=site_color))
##++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
##		p.add(biggles.DataBox((left,bottom),
##							  (right,top), color=feature_color))
##		self._add_arrow(left,bottom,top,right,p)
		reg_box_y_offset = max((abs(bottom), abs(top))) * 1.2
		for start,stop in reg_region_ranges:
##			p.add(biggles.DataBox((start,-reg_box_y_offset),
##								  ((stop+site_length)-1,reg_box_y_offset), color=reg_color,
##								  linetype='longdashed', linewidth=2))			
			for x,y in zip(hit_indices, plus_strand_kappas):
				print >>self.outhandle, x,y
				print >>self.outhandle,x+(site_length-1),y, '\n'
			for x,y in zip(hit_indices, minus_strand_kappas):
				print >>self.outhandle, x,y
				print >>self.outhandle,x+(site_length-1),y, '\n'


	def _add_parameter_string(self):
		pass

	def Display(self, output):
		pass
	def _add_arrow(self,left,bottom, top, right, plot):
		return None


#@-body
#@-node:6::test_kappa
#@+node:7::BigglesFigure
#@+body
if biggles:
	class BigglesFigure(object):
		def __init__(self, title='', param_string=''):
			self._plot_list = []
			self._title = title
			self._parameter_string = param_string
		def AddFeature(self, feature, hit_indices, plus_strand_kappas, minus_strand_kappas,
					   site_length, reg_region_ranges, primary_site=None):
			##++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
			if (len(plus_strand_kappas) + len(minus_strand_kappas)) > 0:
				all_kappas = plus_strand_kappas + minus_strand_kappas
				max_abs = max((abs(max(all_kappas)),
							   abs(min(all_kappas))))
				box_height = max_abs/2
			else:
				box_height = 1
			if feature.Strand == '+':
				bottom=0
				##left = feature.Start - bind_matrix.Length
				left = feature.Start-1
				right = feature.Stop-1
			elif feature.Strand == '-':
				bottom=-box_height
				left = feature.Start-1
				##right = feature.Stop - bind_matrix.Length
				right = feature.Stop-1
			else :
				bottom=-(box_height/2)
				left = feature.Start-1
				right = feature.Stop-1
			top= bottom + box_height
	##++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
	##	def AddFeature(self, feature, hit_indices, plus_strand_kappas, minus_strand_kappas,
	##				   site_length, left,right,top,bottom, reg_region_ranges):

			p = biggles.FramedPlot()
			p.title = feature.Name
	##		p.add(biggles.Curve(hit_indices, plus_strand_kappas, color="red"))
	##		p.add(biggles.Curve(hit_indices, minus_strand_kappas, color="blue"))
	## 		p.add(biggles.ErrorBarsX(plus_strand_kappas,
	##								 hit_indices,
	##								 [x+(site_length-1) for x in hit_indices],
	##								 color="red"))
	##		p.add(biggles.ErrorBarsX(minus_strand_kappas,
	##								 hit_indices,
	##								 [x+(site_length-1) for x in hit_indices],
	##								 color="blue"))
	##++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
			if primary_site:
				primary_bh = box_height * 2
				primary_strand, primary_hitindex, primary_length = primary_site
				if primary_strand == '+':
					primary_b=0
				elif primary_strand == '-':
					primary_b=-primary_bh
				else:
					raise
				primary_l = primary_hitindex
				primary_r = primary_l + primary_length
				primary_t= primary_b + primary_bh
				p.add(biggles.DataBox((primary_l,primary_b),
									  (primary_r,primary_t), color=site_color))
	##++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
			p.add(biggles.DataBox((left,bottom),
								  (right,top), color=feature_color))
			self._add_arrow(left,bottom,top,right,p)
			reg_box_y_offset = max((abs(bottom), abs(top))) * 1.2
			for start,stop in reg_region_ranges:
	##			p.add(biggles.DataBox((start,-reg_box_y_offset),
	##								  (stop,reg_box_y_offset), color=reg_color,
	##								  linetype='dotted'))
	##			p.add(biggles.DataBox((start,-reg_box_y_offset),
	##								  ((stop+site_length),reg_box_y_offset), color=reg_color,
	##								  linetype='dotted'))			
				p.add(biggles.DataBox((start,-reg_box_y_offset),
									  ((stop+site_length)-1,reg_box_y_offset), color=reg_color,
									  linetype='longdashed', linewidth=2))			

			for x,y in zip(hit_indices, plus_strand_kappas):
				p.add(biggles.DataLine((x,y), (x+(site_length-1),y), color=plus_color, linewidth=2))
			for x,y in zip(hit_indices, minus_strand_kappas):
				p.add(biggles.DataLine((x,y), (x+(site_length-1),y), color=minus_color, linewidth=2))
	##		p.add(biggles.LineX(start))
	##		p.add(biggles.LineX(stop))
	##		p.add(biggles.LineY(0, linetype='dotted'))
			## p.xlabel = "Hit Index"
			p.ylabel = "Kappa"
			if not self._plot_list:
	##-----------------------------------------------------------------
				## key_plot = biggles.Plot()
				## for x,y in zip(hit_indices, plus_strand_kappas):
				## key_plot.add(biggles.DataLine((0,0), (0,0)))
	##-----------------------------------------------------------------
				plus_line = biggles.DataLine((0,0), (1,1), color=plus_color, linewidth=2)
				plus_line.label = '+ strand sites'
				minus_line = biggles.DataLine((0,0), (1,1), color=minus_color, linewidth=2)
				minus_line.label = '- strand sites'
				feature_box = biggles.DataBox((0,0),(1,1), color=feature_color, linewidth=2)
				feature_box.label = 'feature'
				reg_box = biggles.DataBox((0,0),(1,1), color=reg_color, linetype='longdashed', linewidth=2)
				reg_box.label = 'reg region'
	##			## p.add(biggles.PlotKey(0.02,0.9, (plus_line,minus_line,feature_box, reg_box)))
				key = biggles.PlotKey(0.02,0.9, (feature_box,reg_box, plus_line,minus_line), fontface=FONT)
				p.add(key)
	##			## box = biggles.PlotBox((0,0),(1,1))
	##			## key_plot.add(box)
	##			self._plot_list.append(key_plot)
			self._plot_list.append(p)

		def _add_parameter_string(self):
			subtitle_plot = biggles.Plot()
			subtitle_plot.add( biggles.PlotLabel(.5, .5, self._parameter_string, size=10) )
			subtitle_plot.add( biggles.Point(.5, .5, size=0) )
			self._plot_list.append(subtitle_plot)

		def Display(self, output):
			self._plot_list[-1].xlabel = "Site Index"
			if self._parameter_string:
				self._add_parameter_string()
			table = biggles.Table(len(self._plot_list),1)
			for index in range(len(self._plot_list)):
				table[index,0] = self._plot_list[index]
			table.title = self._title
			if output:
				table.write_eps(output)
			else:
				table.show()
		def _add_arrow(self,left,bottom, top, right, plot):
			line_offset = abs(left-right) * 0.05
			line_left = min((left,right)) + line_offset
			line_right = max((left,right)) - line_offset
			line_length = line_right - line_left
			if bottom<=0 and top<=0:
				direction = 'left'
				line_y = (min((top,bottom))-max((top,bottom)))/2
				joint_x = line_left
				point_end_x = joint_x + (line_length * 0.04)
			elif bottom>=0 and top>=0:
				direction = 'right'
				line_y = (max((top,bottom))-min((top,bottom)))/2
				joint_x = line_right
				point_end_x = joint_x - (line_length * 0.04)
			else:
				direction = None
				line_y = 0
				return



			joint_y = line_y
			point_end_y_offset = abs(line_y)
			arrow_line =biggles.DataLine((line_left,line_y), (line_right,line_y),
										 color=arrow_color, linewidth=arrow_linewidth)
			point_bottom = biggles.DataLine((joint_x,line_y), (point_end_x,line_y-point_end_y_offset),
											color=arrow_color, linewidth=arrow_linewidth)
			point_top = biggles.DataLine((joint_x,line_y), (point_end_x,line_y+point_end_y_offset),
										 color=arrow_color, linewidth=arrow_linewidth)
			plot.add(arrow_line, point_top, point_bottom)

#@-body
#@-node:7::BigglesFigure
#@+node:8::PyChartFigure
#@+body
#@+doc
# 
# 
# from pychart import area, line_plot, canvas
# 
# class PyChartFigure(object):
# 	def __init__(self):
# 		self._plot_list = []
# 		self.y = make_incrementor(250).next
# 
# 
# 	def AddFeature(self, feature, hit_indices, plus_strand_kappas, minus_strand_kappas,
# 				   left,right,top,bottom):
# 
# 
# 		if not(len(hit_indices) ==
# 			   len(plus_strand_kappas) ==
# 			   len(minus_strand_kappas)):
# 			raise StandardError, 'Data lists have different sizes!!!!!'
# 		data_matrix = zip(hit_indices, plus_strand_kappas, minus_strand_kappas)
# 
# 		ar = area.T(legend=None, loc=(0,self.y()), size=(100,100))
# 		ar.add_plot(line_plot.T(data=data_matrix, ycol=1),
# 					line_plot.T(data=data_matrix, ycol=2))
# ##		p = biggles.FramedPlot()
# ##		p.title = feature.Name
# ##		p.add(biggles.Curve(hit_indices, plus_strand_kappas, color="red"))
# ##		p.add(biggles.Curve(hit_indices, minus_strand_kappas, color="blue"))
# ##		p.add(biggles.DataBox((left,bottom),
# ##							  (right,top), color='green'))
# ####		p.add(biggles.LineX(start))
# ####		p.add(biggles.LineX(stop))
# ####		p.add(biggles.LineY(0, linetype='dotted'))
# ##		p.xlabel = "Hit Index"
# ##		p.ylabel = "Kappa"
# 		self._plot_list.append(ar)
# 	def Display(self):
# ##		table = biggles.Table(len(self._plot_list),1)
# ##		for index in range(len(self._plot_list)):
# ##			table[index,0] = self._plot_list[index]
# ##		table.show()
# 		x11_can = canvas.init(format='x11')
# 		for cur_area in self._plot_list:
# 			cur_area.draw(x11_can)
# ##--------------------------------------------------------------------------------

#@-doc
#@-body
#@-node:8::PyChartFigure
#@+node:9::if __name__ == __main__
#@+body

plus_color = 'red'
minus_color = 'blue'
feature_color = 'green'
arrow_color = feature_color
reg_color = 'black'
arrow_linewidth = 5
site_color = 'orange'
FONT='Arial'

def make_incrementor(inc, initial=0):
	value=initial
	while True:
		yield value
		value += inc


##---------------------------------------------------------------
def main():
##---------------------------------------------------------------
	usage = "usage: %prog [options] KAPPA_FILE\n" + " "*30 + "KAPPA_FILE PARAMETER_STRING\n"
	parser = OptionParser(usage=usage)	
	parser.add_option("-o", "--output",help="Output to FILE in eps format write report to FILE", metavar="FILE")
	parser.add_option("-d", "--data",help="Save kappagraph data to FILE", metavar="FILE")
	parser.add_option("-p", "--parameter",action="store_true", default=False
					  ,help="Print parameter string on plot")
	parser.add_option("-s", "--single",action="store_true", default=False
					  ,help="Generate only the first kappagraph")
	(options, args) = parser.parse_args()
##---------------------------------------------------------------
##	def usage():
##		usage_string = ' '.join(('usage:', sys.argv[0], 'KAPPA_FILE\n',
##								 ' '* ((len('usage:'))-1), sys.argv[0],
##								 'KAPPA_FILE PARAMETER_STRING\n',
##								 '\n-o/--output FILE : output to FILE in eps format'
##								 '\n-p/--parameter:    print parameter string on plot'
##								 '\n-s/--single:       only print the first kappagraph\n'
####								 '-n/--no_strand: Strand is meaningless for the',
####								 'kappa function, and therefore all features',
####								 'should have Start <= Stop'
##								 ))
##		print >>sys.stderr, usage_string
####----------------------------------------------------------
##	try:
##		opts, args = getopt.getopt(sys.argv[1:], "hpso:", ["help", "parameter", "output=", 'single'])
##	except getopt.GetoptError:
##		# print help information and exit:
##		usage()
##		sys.exit(2)
##	output = None
##	single = False
##	parameter = False
##	for o, a in opts:
##		if o in ("-h", "--help"):
##			usage()
##			sys.exit()
##		if o in ("-o", "--output"):
##			output = a
##		if o in ("-p", "--parameter"):
##			parameter = True
##		if o in ("-s", "--single"):
##			single = True

	if len(args) == 1:
		kappa_filename = args[0]
		print_kappa_help(kappa_filename)
	elif len(args) == 2:
		if not biggles:
			print >>sys.stderr, 'Sorry, biggles module is required for kappagraphs, but it could not be found.'
			sys.exit(3)
		kappa_filename = args[0]
		parameter_string = args[1]
		test_kappa(kappa_filename, parameter_string, options.output, options.parameter, options.single, options.data)
	else:
		parser.print_help()
		sys.exit()
	

if __name__ == '__main__':
	main()


	

#@-body
#@-node:9::if __name__ == __main__
#@-others


#@-body
#@-node:0::@file test_reg_region_kappa.py
#@-leo
