
import sys
import glob
import os
from parse_gomer_output import ParseGomerOutput
from regulated_feature_parser_gomer import ParseRegulatedFeatures
from cerevisiae_feature_parser_gomer import FeatureFileParse

free_conc_ratio_label = 'Free Concentration Ratios'
free_conc_list_label = 'Free Concentration List'


def usage():
	print >>sys.stderr, '\nusage:', os.path.basename(sys.argv[0]), 'CHROM_FEATURE_FILE REGULATED_FEATURES GOMER_OUTPUT_FILE[s]\n\n'

output_handle = sys.stdout
def main():
	if len(sys.argv) >=4:
		chrom_feature_filename = sys.argv[1]
		regulated_filename = sys.argv[2]
		gomer_output_globs = sys.argv[3:]
		gomer_output_filenames = []
		for cur_glob in gomer_output_globs:
			gomer_output_filenames.extend(glob.glob(cur_glob))
	else:
		usage()
		sys.exit(2)


	chromosome_hash, feature_dictionary, \
					 ambiguous_feature_names, \
					 feature_type_dict =    \
					 FeatureFileParse(chrom_feature_filename,{},{},{},{})

	(regulated_feature_names,
	 excluded_feature_names,
	 unregulated_feature_names) = ParseRegulatedFeatures(regulated_filename)


	output_tuples = []
	for filename in gomer_output_filenames:
		handle = file(filename)
		(all_feature_hash,
		 regulated_hash,
		 unknown_feature_hash,
		 gomer_output_skipped_lines,
		 param_hash) = ParseGomerOutput(handle)
		output_tuples.append((param_hash[free_conc_ratio_label],
							 param_hash[free_conc_list_label],
							 all_feature_hash))

	output_tuples.sort()
	for feature_name in regulated_feature_names:
		feature = feature_dictionary[feature_name]
		ref_feature_name = (feature.CommonName or feature.Name)
		print >>output_handle, '##', ref_feature_name
		for free_conc_ratio, free_conc, all_feature_hash in output_tuples:
			print >>output_handle, '\t'.join((str(free_conc_ratio),
											  str(all_feature_hash[ref_feature_name].score)))
		print >>output_handle, '\n'
		
		

if __name__ == "__main__":
	main()
