#! /usr/bin/env python

import re
import sys

SINGLE_AMPLIMER = 'Single Amplimer Section'
MULTIPLE_AMPLIMER = 'Multiple Amplimer Section'
NO_AMPLIMER = 'No Amplimer Section'


empty_line_re = re.compile('^\s*$')
separator_re = re.compile('#----------------------------------------')
single_amplimer_tag_re = re.compile('#Amplimers:')
multiple_amplimer_tag_re = re.compile('#Multiple Amplimers:')
no_amplimer_tag_re = re.compile('#Primers with no amplimers:')

amplimer_re = re.compile('^([^\t]+)[\t]+(\w+)[\t]+(\d+)[\t]+(\d+)\s*$')
##feature = amplimer_match.group(1)
##chrom = amplimer_match.group(2)
##start = int(amplimer_match.group(3))
##stop = int(amplimer_match.group(4))

multiple_amplimer_re = re.compile('^#(\S+):[\t]+(\w+)[\t]+(\d+)[\t]+(\d+)[\t]+(\d+)[\t]+(\d+)[\t]+(\d+)\s*$')
##feature = multiple_amplimer_match.group(1)
##chrom = multiple_amplimer_match.group(2)
##start = int(multiple_amplimer_match.group(3))
##stop = int(multiple_amplimer_match.group(4))
##size = int(multiple_amplimer_match.group(5))
##for_mismatch = int(multiple_amplimer_match.group(6))
##rev_mismatch = int(multiple_amplimer_match.group(6))

no_amplimer_re = re.compile('^#%\t(\S+)\s*$')
##feature = no_amplimer_match.group(1)

def ParseRawCoordinates(filehandle):
	"""
	hash keys are all uppercase
	"""
	single_amplimer_hash = {}
	multiple_amplimer_hash = {}
	no_amplimer_hash = {}
	skipped_lines = []

	current_section = None
	two_back = last = current = ''
	for line in filehandle:
		two_back, last, current = last, current, line
		amplimer_match = amplimer_re.match(current)
		multiple_amplimer_match = multiple_amplimer_re.match(current)
		no_amplimer_match = no_amplimer_re.match(current)

		if amplimer_match:
			if current_section <> SINGLE_AMPLIMER:
				raise 'File Format error: found amplimers in section:', current_section
			feature = amplimer_match.group(1)
			chrom = amplimer_match.group(2)
			start = int(amplimer_match.group(3))
			stop = int(amplimer_match.group(4))
			single_amplimer_hash[feature.upper()] = ((feature, chrom, start, stop))
			
		elif multiple_amplimer_match:
			if current_section <> MULTIPLE_AMPLIMER:
				print >>sys.stderr, line
				raise 'File Format error: found "multiple amplimers" in section:', current_section
			feature = multiple_amplimer_match.group(1)
			chrom = multiple_amplimer_match.group(2)
			start = int(multiple_amplimer_match.group(3))
			stop = int(multiple_amplimer_match.group(4))
			size = int(multiple_amplimer_match.group(5))
			for_mismatch = int(multiple_amplimer_match.group(6))
			rev_mismatch = int(multiple_amplimer_match.group(6))
			
			multiple_amplimer_hash.setdefault(feature.upper(), []).append((feature,
																   chrom,
																   start,
																   stop,
																   size,
																   for_mismatch,
																   rev_mismatch))
		elif no_amplimer_match:
			if current_section <> NO_AMPLIMER:
				print >>sys.stderr, line
				raise 'File Format error: found in "no amplimers" in section:', current_section
			feature = no_amplimer_match.group(1)
			no_amplimer_hash[feature.upper()] = feature

		
		elif (separator_re.match(two_back) and
			single_amplimer_tag_re.match(last) and
			separator_re.match(current)):
			current_section = SINGLE_AMPLIMER
		elif (separator_re.match(two_back) and
			multiple_amplimer_tag_re.match(last) and
			separator_re.match(current)):
			current_section = MULTIPLE_AMPLIMER
		elif (separator_re.match(two_back) and
			no_amplimer_tag_re.match(last) and
			separator_re.match(current)):
			current_section = NO_AMPLIMER
		elif empty_line_re.match(current):
			continue
		else:
			skipped_lines.append(current.rstrip())

	return single_amplimer_hash, multiple_amplimer_hash, no_amplimer_hash, skipped_lines


if __name__ == '__main__':
	import sys
	raw_coord_filename = sys.argv[1]
	raw_coord_filehandle = file(raw_coord_filename)
	(single_amplimer_hash,
	 multiple_amplimer_hash,
	 no_amplimer_hash,
	 skipped_lines) = ParseRawCoordinates(raw_coord_filehandle)

	# print single_amplimer_hash, multiple_amplimer_hash, no_amplimer_hash, skipped_lines

	print '-'*60, '\nSKIPPED LINES:\n', '-'*60, '\n', '\n'.join([line for line in  skipped_lines])

	print '-'*60, '\nSingle Amplimer Hash:\n', '-'*60
	for key in single_amplimer_hash:
		print key, single_amplimer_hash[key]

	print '-'*60, '\nMultiple Amplimer Hash:\n', '-'*60
	for key in multiple_amplimer_hash:
		print key, ':'
		for data in multiple_amplimer_hash[key]:
			print data

	print '-'*60, '\nNo Amplimer Hash:\n', '-'*60
	for key in no_amplimer_hash:
		print key, no_amplimer_hash[key]


## In order to deal with silent cassette, loo in good (single amplimer) set first,  if the feature is found there, don't worry (i.e. don't look) if it is found in the multiple amplimer set.

## Don't forget to strip "#" from feature names in multiple and no amplimer sections

##------------------------------------------------------
## ~/GOMER/intergenic_primers/primersearch/0_mismatches/parse.0mis
##------------------------------------------------------

"""
# DUPLICATE PRIMER: iYBLWdelta3
#Primer Count: 6821
%SEQUENCE       NC_001133       1       edf5563344e9d461f41327d5c667e356
%SEQUENCE       NC_001134       2       d8567f8f783057eb0731f8edd0d32f74
%SEQUENCE       NC_001135       3       604e89e58881845f8f25de905fe485b3
%SEQUENCE       NC_001136       4       3c2c09c81940a0cfe40f69b4959862d2
%SEQUENCE       NC_001137       5       2b9aeb682e653932db18aa15141a894f
%SEQUENCE       NC_001138       6       fd3779b3bbe087e37069db421a1247c0
%SEQUENCE       NC_001139       7       1a353ce6aba3f3532a95048d8f06ad64
%SEQUENCE       NC_001140       8       2f9ae3f9e9190c744498cf76bf270ff7
%SEQUENCE       NC_001141       9       2fd34473d35a98406b22cf1acc7e4272
%SEQUENCE       NC_001142       10      9ba2b893970288a9155c3f50585ee794
%SEQUENCE       NC_001143       11      363e20bd3b012be3d274e16da565fb99
%SEQUENCE       NC_001144       12      067e857685b396768fae540c4c7a2390
%SEQUENCE       NC_001145       13      f054292ca762f4437c4d5d2d1fc24afe
%SEQUENCE       NC_001146       14      1263a38ecfd09590ffb83acb714d5542
%SEQUENCE       NC_001147       15      1f4eb562e9f05e3edc9e1ce9e3c08683
%SEQUENCE       NC_001148       16      62dc661cfae03971c41c29fe9ff4e08e
%SEQUENCE       NC_001224       17      71c39cf065b8d574f636b654c274cf1b
                                                                                                                                                         
#Total Number of Unique Primers:6820
#Number of Primers with single amplimer:6574
#Number of Primers with no amplimer:40
#Number of Primers with multiple amplimers:206
#----------------------------------------
#Amplimers:
#----------------------------------------
#name   chrom   start   stop
iYAL068C-0      1       2162    3449
iYAL068C-1      1       3430    4699
iYAL068C-2      1       4696    5980
iYAL068C-3      1       5953    7231
iYAL067C        1       9034    10087
iYAL066W        1       10421   11566
iYAL065C        1       11958   12058
iYAL064W-B      1       12431   13374
iYAL064C-A-2    1       16346   17640
iYAL064C-A-3    1       17633   18926
iYAL064C-A-5    1       20231   21538
iYAL064W        1       21854   22224
iYALWdelta1     1       22540   23996
"""

"""
Q0085   17      73803   74466
Q0090   17      74607   74687
Q0100   17      77440   77503
Q0105   17      78084   78166
Q0140   17      85035   85110
9S rRNA 17      85311   85778
                                                                                                                                                         
#----------------------------------------
#Multiple Amplimers:
#----------------------------------------
#name   chrom   start   stop    size    for_mis rev_mis
#YBLWtau1:      2       35869   36213   345     0       0
#YBLWtau1:      7       110857  111201  345     0       0
#YBLWtau1:      8       91386   91730   345     0       0
#YBLWtau1:      9       196676  197020  345     0       0
#YBLWtau1:      10      203099  203443  345     0       0
#YBLWtau1:      10      197244  197588  345     0       0
#YBLWtau1:      10      197244  203443  6200    0       0
#YBLWtau1:      13      503770  504114  345     0       0
#YBLWtau1:      16      442763  443107  345     0       0
#YBLWtau1:      16      436911  437255  345     0       0
#YBLWtau1:      16      436911  443107  6197    0       0
                                                                                                                                                         
#iYHRCdelta16-B:        1       224219  225461  1243    0       0
#iYHRCdelta16-B:        8       550866  552110  1245    0       0
                                                                                                                                                         
#RDN37-1:       12      464723  465086  364     0       0
#RDN37-1:       12      455586  455949  364     0       0
"""

"""
#iYLRCdelta8:   12      485561  486210  650     0       0
#iYLRCdelta8:   12      481909  482558  650     0       0
#iYLRCdelta8:   12      481909  486210  4302    0       0
#iYLRCdelta8:   12      472329  472978  650     0       0
                                                                                                                                                         
#iYPL283C-1:    15      1083204 1084168 965     0       0
#iYPL283C-1:    16      6970    7934    965     0       0
 
 
#----------------------------------------
#Primers with no amplimers:
#----------------------------------------
#iYCLWomega2-0
#iYCLWomega2-1
#itG(GCC)C
#iYCR016W
#iYCR025C
#itQ(UUG)C
#iYCRWdelta11
#iYCR028C-A
#iSNR65
#iYCR032W
#iYCR054C

"""
##----------------------------------------------
## ~/GOMER/intergenic_primers/ORF_primers/Yeast_Gene_Pairs_Coordinates.0mis
##----------------------------------------------
"""
# DUPLICATE PRIMER: YCL021W-A
# DUPLICATE PRIMER: YCL027C-A
#Primer Count: 6337
%SEQUENCE       NC_001133       1       edf5563344e9d461f41327d5c667e356
%SEQUENCE       NC_001134       2       d8567f8f783057eb0731f8edd0d32f74
%SEQUENCE       NC_001135       3       604e89e58881845f8f25de905fe485b3
%SEQUENCE       NC_001136       4       3c2c09c81940a0cfe40f69b4959862d2
%SEQUENCE       NC_001137       5       2b9aeb682e653932db18aa15141a894f
%SEQUENCE       NC_001138       6       fd3779b3bbe087e37069db421a1247c0
%SEQUENCE       NC_001139       7       1a353ce6aba3f3532a95048d8f06ad64
%SEQUENCE       NC_001140       8       2f9ae3f9e9190c744498cf76bf270ff7
%SEQUENCE       NC_001141       9       2fd34473d35a98406b22cf1acc7e4272
%SEQUENCE       NC_001142       10      9ba2b893970288a9155c3f50585ee794
%SEQUENCE       NC_001143       11      363e20bd3b012be3d274e16da565fb99
%SEQUENCE       NC_001144       12      067e857685b396768fae540c4c7a2390
%SEQUENCE       NC_001145       13      f054292ca762f4437c4d5d2d1fc24afe
%SEQUENCE       NC_001146       14      1263a38ecfd09590ffb83acb714d5542
%SEQUENCE       NC_001147       15      1f4eb562e9f05e3edc9e1ce9e3c08683
%SEQUENCE       NC_001148       16      62dc661cfae03971c41c29fe9ff4e08e
 
#Total Number of Unique Primers:6335
#Number of Primers with single amplimer:6049
#Number of Primers with no amplimer:48
#Number of Primers with multiple amplimers:238
#----------------------------------------
#Amplimers:
#----------------------------------------
#name   chrom   start   stop
YAL069W 1       335     649
YAL067C 1       7236    9017
YAL066W 1       10092   10400
YAL065C 1       11566   11952
YAL064C-A       1       13364   13744
YAL064W 1       21526   21852
YAL063C 1       24001   27969
YAL062W 1       31568   32941
YAL061W 1       33449   34702
YAL060W 1       35156   36304
YAL059W 1       36510   37148
YAL058W 1       37465   38973
"""

"""
YPR194C 16      924299  926932
YPR195C 16      927960  928289
YPR196W 16      931371  932783
YPR197C 16      933893  934456
YPR198W 16      934029  935660
YPR199C 16      938143  939027
YPR200C 16      939274  939666
YPR201W 16      939917  941131
                                                                                                                      
#----------------------------------------
#Multiple Amplimers:
#----------------------------------------
#name   chrom   start   stop    size    for_mis rev_mis
#YMR323W:       13      920087  921400  1314    0       0
#YMR323W:       15      1080269 1081582 1314    0       0
#YMR323W:       16      9557    10870   1314    0       0
                                                                                                                      
#YDR316W-A:     2       259826  261148  1323    0       0
#YDR316W-A:     4       1096056 1097378 1323    0       0
#YDR316W-A:     4       882597  883919  1323    0       0
#YDR316W-A:     5       496797  498119  1323    0       0
#YDR316W-A:     7       821688  823010  1323    0       0
#YDR316W-A:     8       547906  549342  1437    0       0
#YDR316W-A:     12      219387  220709  1323    0       0
#YDR316W-A:     13      184461  185783  1323    0       0
#YDR316W-A:     13      377002  378324  1323    0       0
#YDR316W-A:     15      595111  596433  1323    0       0
#YDR316W-A:     16      844704  846026  1323    0       0
"""

"""
#YDR098C-B:     2       259826  265097  5272    0       0
#YDR098C-B:     4       1096056 1101324 5269    0       0
#YDR098C-B:     4       878651  883919  5269    0       0
#YDR098C-B:     4       645851  651119  5269    0       0
#YDR098C-B:     5       492851  498119  5269    0       0
#YDR098C-B:     7       817742  823010  5269    0       0
#YDR098C-B:     8       543960  549342  5383    0       0
#YDR098C-B:     12      215441  220709  5269    0       0
#YDR098C-B:     13      184461  189729  5269    0       0
#YDR098C-B:     13      373056  378324  5269    0       0
#YDR098C-B:     15      595111  600379  5269    0       0
#YDR098C-B:     15      117999  123267  5269    0       0
#YDR098C-B:     16      844704  849975  5272    0       0
                                                                                                                      
#YGR292W:       2       805306  807060  1755    0       0
#YGR292W:       7       1076596 1078350 1755    0       0
                                                                                                                      
                                                                                                                      
#----------------------------------------
#Primers with no amplimers:
#----------------------------------------
#YAL037W
#YBL004W
#YBL069W
#YBL088C
#YBR006W
#YBR084C-A
#YBR136W
#YBR140C
#YCL004W
"""
