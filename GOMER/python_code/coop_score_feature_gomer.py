from __future__ import division
#@+leo
#@+node:0::@file coop_score_feature_gomer.py
#@+body
#@@first
#@@language python
##import os
##os.environ['PYCHECKER'] = '--allglobals --stdlib'
##import pychecker.checker

##import check_python_version
##check_python_version.CheckVersion()

import sys

from integer_string_compare import sort_numeric_string
from globals_gomer import FORWARD, REVCOMP


#@+others
#@+node:1::CooperativeScoreFeature
#@+body
def CooperativeScoreFeature(feature, site_kappa_model, for_primary_Kas, rev_primary_Kas, primary_tf_free_conc, coop_tf_tuple):
	"""
	I think this function should be usable for all feature types, I need to be
	sure that all features have the required attributes.  If they do, the name
	of the function will probably be changed to "CooperativeScoreFeature", and
	the "feature" paramtere will be changed to "feature"

	PARAMS:
	        feature              : the feature of interest
			site_kappa_model : refered to as "regulatory_region_model" in
			                   function "RegulatoryRegionScoreFeatureSlice", this
							   is the model which can provide the site kappa
			primary_tf       : the name of the primary transcription factor
			free_conc_hash   : a hash (keyed by tf_name) of free concentration
			                   values for the various transcription factors being
							   considered
			coop_tf_list   : a list of the names of the coopary transcription
			                   factors to be considered
			K_dimer_hash     : a "2D hash" of pairwise dimerization constants
	                           between all pairs that will be considered
	        coop_model_hash  : a "2D hash" of cooperativity (kappa_coop) models,
			                   this should have the same structure as the K_dimer_hash
	"""


	"""
	deal with complete or partial overlap of cooperative proteins
	
	primary TF is the TF that everything else is relative.
	
	In the case of simple regulatory model, the primary TF is the only TF
	"""
	##----------------------------------------------------------------
	## Iterate over primary TF sites "i", bound by the protein "X"
	##----------------------------------------------------------------
##	cur_coop_model_hash = coop_model_hash.get(primary_tf_key, {})
##	coop_tf_list = tuple(coop_tf_list)
##	current_chromsome = feature.Chromosome
##	hitlist_hash = {}
##	tf_md5_hash = {}
##	for tf_key in coop_tf_list + (primary_tf_key,):
##		tf_md5 = probability_matrix_hash[tf_key].MD5
##		hitlist_hash[tf_md5] = {}
##		hitlist_hash[tf_md5][FORWARD] = current_chromsome.GetHitList(tf_md5).Forward
##		hitlist_hash[tf_md5][REVCOMP] = current_chromsome.GetHitList(tf_md5).ReverseComplement
##		tf_md5_hash[tf_key] = tf_md5
##	primary_tf_md5 = tf_md5_hash[primary_tf_key]
##	primary_tf_free_conc = free_conc_hash[primary_tf_key]

	i_sites_product = 1

	regulatory_region_ranges = site_kappa_model.GetRegulatoryRegionRanges(feature)
	for first_i_hitindex, last_i_hitindex in regulatory_region_ranges:
		#---------------------------------------------------------------
		stop_i_hitindex = last_i_hitindex + 1
		## for_primary_Ka_list = hitlist_hash[primary_tf_md5][FORWARD][first_i_hitindex :stop_i_hitindex]
		## rev_primary_Ka_list = hitlist_hash[primary_tf_md5][REVCOMP][first_i_hitindex :stop_i_hitindex]
		for_primary_Ka_slice = for_primary_Kas[first_i_hitindex :stop_i_hitindex]
		rev_primary_Ka_slice = rev_primary_Kas[first_i_hitindex :stop_i_hitindex]
		if len(for_primary_Ka_slice) <> len(rev_primary_Ka_slice):
			raise StandardError, 'Length of forward and revcomp slices should be identical!!!'
		i_hitindices = xrange(first_i_hitindex,stop_i_hitindex)
		##-------------------------------------------------------------------
		for i_index in xrange(len(for_primary_Ka_slice)):
			i_hitindex = i_hitindices[i_index]
		 	for i_strand, primary_Ka_list  in ((FORWARD, for_primary_Ka_slice), (REVCOMP,rev_primary_Ka_slice)):
			 	primary_Ka = primary_Ka_list[i_index]
		##-------------------------------------------------------------------
				## FILTER START
				if primary_Ka == 0:
					continue
				## FILTER STOP
				i_site_kappa = site_kappa_model.GetSiteKappa(feature, i_hitindex, i_strand)
				if i_site_kappa == 0:
					continue
				K_a_eff = (primary_Ka * i_site_kappa)
				i_sites_product *= 1 / (1 + (primary_tf_free_conc * K_a_eff))
				##----------------------------------------------------------------
				## Iterate over secondary TF proteins "Y"
				##----------------------------------------------------------------
				# proteins_denom_product = 1
				proteins_product = 1
				# for coop_tf_key in coop_tf_list:
				for K_dimer, coop_tf_free_conc, coop_model, for_coop_Kas, rev_coop_Kas in coop_tf_tuple:
					##----------------------------------------------------------------
					## Iterate over secondary TF sites "j" bound by the protein "Y"
					##----------------------------------------------------------------
					# K_dimer = K_dimer_hash[primary_tf_key][coop_tf_key]
					# coop_tf_free_conc = free_conc_hash[coop_tf_key]
					# coop_model = cur_coop_model_hash[coop_tf_key]
					# coop_tf_md5 = tf_md5_hash[coop_tf_key]

					coop_slice_tuple = coop_model.GetSlices(i_hitindex, i_strand, feature)

					## Iterate over (potential) chunks of regulatory reigons
					for start_coop_slice, stop_coop_slice, for_coop_kappa_slice, rev_coop_kappa_slice in coop_slice_tuple:
						# for_coop_Ka_slice = hitlist_hash[coop_tf_md5][FORWARD][start_coop_slice:stop_coop_slice]
						# rev_coop_Ka_slice = hitlist_hash[coop_tf_md5][REVCOMP][start_coop_slice:stop_coop_slice]
						for_coop_Ka_slice = for_coop_Kas[start_coop_slice:stop_coop_slice]
						rev_coop_Ka_slice = rev_coop_Kas[start_coop_slice:stop_coop_slice]
						
						for j_index in range(stop_coop_slice-start_coop_slice):
							## j_for_rev_denom_product = 1
							for_coop_Ka = for_coop_Ka_slice[j_index]
							for_coop_kappa = for_coop_kappa_slice[j_index]
							rev_coop_Ka = rev_coop_Ka_slice[j_index]
							rev_coop_kappa = rev_coop_kappa_slice[j_index]
							# j_sites_denom_product *= (1 + ((primary_tf_free_conc * # for
							proteins_product *= (1 / (1 + (primary_tf_free_conc *    # for
														   coop_tf_free_conc *       # for
														   for_coop_kappa * K_dimer  # for
														   * K_a_eff * for_coop_Ka)))# for

							# j_sites_denom_product *= (1 + ((primary_tf_free_conc * # rev
							proteins_product *= (1 / (1 + (primary_tf_free_conc *    # rev
														   coop_tf_free_conc *       # rev
														   rev_coop_kappa * K_dimer  # rev
														   * K_a_eff * rev_coop_Ka)))# rev
					# proteins_denom_product *= j_sites_denom_product
				i_sites_product *= proteins_product
	# feature_score = 1 - (1/i_sites_denom_product)
	feature_score = 1 - i_sites_product
	return feature_score

####==============================================================================
####==============================================================================

##def OldCooperativeScoreFeature(feature, site_kappa_model, primary_tf_key, coop_tf_list, 
##							   free_conc_hash,	K_dimer_hash, coop_model_hash, probability_matrix_hash):
##	"""
##	I think this function should be usable for all feature types, I need to be
##	sure that all features have the required attributes.  If they do, the name
##	of the function will probably be changed to "CooperativeScoreFeature", and
##	the "feature" paramtere will be changed to "feature"

##	PARAMS:
##	        feature              : the feature of interest
##			site_kappa_model : refered to as "regulatory_region_model" in
##			                   function "RegulatoryRegionScoreFeatureSlice", this
##							   is the model which can provide the site kappa
##			primary_tf       : the name of the primary transcription factor
##			free_conc_hash   : a hash (keyed by tf_name) of free concentration
##			                   values for the various transcription factors being
##							   considered
##			coop_tf_list   : a list of the names of the coopary transcription
##			                   factors to be considered
##			K_dimer_hash     : a "2D hash" of pairwise dimerization constants
##	                           between all pairs that will be considered
##	        coop_model_hash  : a "2D hash" of cooperativity (kappa_coop) models,
##			                   this should have the same structure as the K_dimer_hash
##	"""


##	"""
##	deal with complete or partial overlap of cooperative proteins
	
##	primary TF is the TF that everything else is relative.
	
##	In the case of simple regulatory model, the primary TF is the only TF
##	"""
#### def RegulatoryRegionScoreOrfSlice(orf, regulatory_region_model, binding_site_model, free_concentration_factor):

##	##----------------------------------------------------------------
##	## Iterate over primary TF sites "i", bound by the protein "X"
##	##----------------------------------------------------------------
##	## print >> sys.stderr, 'Scoring:', feature.Name, '(', feature.Types, ')'
##	# cur_coop_model_hash = coop_model_hash[primary_tf]
##	cur_coop_model_hash = coop_model_hash.get(primary_tf_key, {})
##	coop_tf_list = tuple(coop_tf_list)
##	regulatory_region_ranges = site_kappa_model.GetRegulatoryRegionRanges(feature)
##	current_chromsome = feature.Chromosome
##	hitlist_hash = {}
##	tf_md5_hash = {}
##	for tf_key in coop_tf_list + (primary_tf_key,):
##		tf_md5 = probability_matrix_hash[tf_key].MD5
##		hitlist_hash[tf_md5] = {}
##		hitlist_hash[tf_md5][FORWARD] = current_chromsome.GetHitList(tf_md5).Forward
##		hitlist_hash[tf_md5][REVCOMP] = current_chromsome.GetHitList(tf_md5).ReverseComplement
##		tf_md5_hash[tf_key] = tf_md5
##	## primary_tf_hitlist = current_chromsome.GetHitList(binding_site_model.Name)
##	primary_tf_md5 = tf_md5_hash[primary_tf_key]
##	primary_tf_free_conc = free_conc_hash[primary_tf_key]
##	# i_sites_denom_product = 1
##	i_sites_product = 1
##	for first_i_hitindex, last_i_hitindex in regulatory_region_ranges:
##		#---------------------------------------------------------------
##		stop_i_hitindex = last_i_hitindex + 1
##		for_primary_Ka_list = hitlist_hash[primary_tf_md5][FORWARD][first_i_hitindex :stop_i_hitindex]
##		rev_primary_Ka_list = hitlist_hash[primary_tf_md5][REVCOMP][first_i_hitindex :stop_i_hitindex]
##		## for_primary_Ka_list = hit_list.Forward[first_i_hitindex :stop_i_hitindex]
##		## rev_primary_Ka_list = hit_list.ReverseComplement[first_i_hitindex :stop_i_hitindex]
##		if len(for_primary_Ka_list) <> len(rev_primary_Ka_list):
##			raise StandardError, 'Length of forward and revcomp slices should be identical!!!'
##		i_hitindices = xrange(first_i_hitindex,stop_i_hitindex)
##		##---------------------------------------------------------------
##		## for i_hitindex in xrange(first_i_hitindex, stop_i_hitindex):
##		## 	for i_strand,  in (FORWARD, REVCOMP):
##		##	 	primary_Ka = hitlist_hash[primary_tf][i_strand][i_hitindex]
##		##-------------------------------------------------------------------
##		for i_index in xrange(len(for_primary_Ka_list)):
##			i_hitindex = i_hitindices[i_index]
##		 	for i_strand, primary_Ka_list  in ((FORWARD, for_primary_Ka_list), (REVCOMP,rev_primary_Ka_list)):
##			 	primary_Ka = primary_Ka_list[i_index]
##		##-------------------------------------------------------------------
##				## FILTER START
##				if primary_Ka == 0:
##					continue
##				## FILTER STOP
##				i_site_kappa = site_kappa_model.GetSiteKappa(feature, i_hitindex, i_strand)
##				if i_site_kappa == 0:
##					continue
##				K_a_eff = (primary_Ka * i_site_kappa)
##				# i_denominator = 1 + (primary_tf_free_conc * K_a_eff)
##				# i_denominator = 1 + (primary_tf_free_conc * primary_Ka * i_site_kappa )
##				i_sites_product *= 1 / (1 + (primary_tf_free_conc * K_a_eff))
##				## cooperativity_ranges = coop_model.GetRegulatoryRegionRanges(feature, i_hitindex)
##				##----------------------------------------------------------------
##				## Iterate over secondary TF proteins "Y"
##				##----------------------------------------------------------------
##				# proteins_denom_product = 1
##				proteins_product = 1
##				for coop_tf_key in coop_tf_list:
##					##----------------------------------------------------------------
##					## Iterate over secondary TF sites "j" bound by the protein "Y"
##					##----------------------------------------------------------------
##					K_dimer = K_dimer_hash[primary_tf_key][coop_tf_key]
##					coop_tf_free_conc = free_conc_hash[coop_tf_key]
##					coop_model = cur_coop_model_hash[coop_tf_key]
##					coop_slice_tuple = coop_model.GetSlices(i_hitindex, i_strand, feature)
##					coop_tf_md5 = tf_md5_hash[coop_tf_key]
##					## cooperativity_ranges = coop_model.GetCoopRanges(i_hitindex, i_strand, feature)
##					# j_sites_denom_product = 1
##					# j_sites_product = 1
##					## for start_coop_hitindex, stop_coop_hitindex in cooperativity_ranges:

##					## Iterate over (potential) chunks of regulatory reigons
##					for start_coop_slice, stop_coop_slice, for_coop_kappa_slice, rev_coop_kappa_slice in coop_slice_tuple:
##						for_coop_Ka_slice = hitlist_hash[coop_tf_md5][FORWARD][start_coop_slice:stop_coop_slice]
##						rev_coop_Ka_slice = hitlist_hash[coop_tf_md5][REVCOMP][start_coop_slice:stop_coop_slice]
##						## print >>sys.stderr, 'start:', start_coop_slice, ', stop:', stop_coop_slice, ', len(for_coop_kappa_slice):', len(for_coop_kappa_slice), ', len(rev_coop_kappa_slice):', len(rev_coop_kappa_slice), ', len(for_second_Ka_slice):', len(for_second_Ka_slice)
##						## for j_hitindex in xrange(start_coop_hitindex, (stop_coop_hitindex + 1)):
##						## for j_index in range((stop_coop_slice-start_coop_slice)+1):
##						for j_index in range(stop_coop_slice-start_coop_slice):
##							## j_for_rev_denom_product = 1
##							for_coop_Ka = for_coop_Ka_slice[j_index]
##							for_coop_kappa = for_coop_kappa_slice[j_index]
##							rev_coop_Ka = rev_coop_Ka_slice[j_index]
##							rev_coop_kappa = rev_coop_kappa_slice[j_index]
##							# j_sites_denom_product *= (1 + ((primary_tf_free_conc *  # for
##							proteins_product *= (1 / (1 + ((primary_tf_free_conc *    # for
##															coop_tf_free_conc *       # for
##															for_coop_kappa * K_dimer)**2  # for
##														   * K_a_eff * for_coop_Ka)))# for

##							# j_sites_denom_product *= (1 + ((primary_tf_free_conc * # rev
##							proteins_product *= (1 / (1 + ((primary_tf_free_conc *    # rev
##															coop_tf_free_conc *       # rev
##															rev_coop_kappa * K_dimer)**2  # rev
##														   * K_a_eff * rev_coop_Ka)))# rev
##					# proteins_denom_product *= j_sites_denom_product
##				i_sites_product *= proteins_product
##	# feature_score = 1 - (1/i_sites_denom_product)
##	feature_score = 1 - i_sites_product
##	return feature_score



## def score_ORFs(chromosome_hash, binding_site_model, free_concentration_factor,
##			   regulatory_region_model):



# 
# def CooperativeScoreOrf(orf, site_kappa_model, primary_tf, free_conc_hash,
# 						second_tf_list, K_dimer_hash, coop_model_hash):


##def coop_score_ORFs(chromosome_hash, regulatory_region_model, primary_tf_name,
##					second_tf_name_list, free_conc_hash, K_dimer_hash,
##					coop_model_hash):
##	list_of_score_tuples = []
##	sorted_chromosome_label_list = chromosome_hash.keys()
##	sorted_chromosome_label_list.sort(IntegerStringCmp)
##	for chromosome_label in sorted_chromosome_label_list:
##		chromosome = chromosome_hash[chromosome_label]
##		print >> sys.stderr, '\nCoop Scoring Orfs of Chromosome:', chromosome_label
##		for cur_orf in chromosome.OrfIterator():
##			cur_orf_score = CooperativeScoreOrf(cur_orf,
##													 regulatory_region_model,
##													 primary_tf_name, 
##													 second_tf_name_list, 
##													 free_conc_hash,
##													 K_dimer_hash, 
##													 coop_model_hash)
##			list_of_score_tuples.append(tuple((cur_orf_score,cur_orf)))
##	return list_of_score_tuples
##--------------------------------------------------------------------
def old_coop_score_features(feature_types, chromosome_hash, regulatory_region_model,
							primary_tf_name,coop_tf_name_list,free_conc_hash,
							K_dimer_hash,coop_model_hash, probability_matrix_hash):
	list_of_score_tuples = []
##	sorted_chromosome_label_list = chromosome_hash.keys()
##	sorted_chromosome_label_list.sort(IntegerStringCmp)
	sorted_chromosome_label_list = sort_numeric_string(chromosome_hash.keys())
	if __debug__: print >>sys.stderr, 'Coop Scoring Feature Types:', feature_types
	for chromosome_label in sorted_chromosome_label_list:
		chromosome = chromosome_hash[chromosome_label]
## 		sys.stderr.write ('\r' + ' '*80 + '\r')
## 		print >>sys.stderr, 'Coop Scoring Features of Chromosome:', chromosome_label,
		if __debug__:
			print >>sys.stderr, 'Coop Scoring Features of Chromosome:', chromosome_label
		for cur_feature in chromosome.FeatureIterator(feature_types):
			cur_feature_score = OldCooperativeScoreFeature(cur_feature,
														   regulatory_region_model,
														   primary_tf_name, 
														   coop_tf_name_list, 
														   free_conc_hash,
														   K_dimer_hash, 
														   coop_model_hash,
														   probability_matrix_hash)
			list_of_score_tuples.append(tuple((cur_feature_score,cur_feature)))
	return list_of_score_tuples
##=============================================================================
##=============================================================================
def coop_score_features(feature_types, chromosome_hash, regulatory_region_model,
						primary_tf_key,coop_tf_key_list,free_conc_hash,
						K_dimer_hash,coop_model_hash, probability_matrix_hash):
	list_of_score_tuples = []

	primary_tf_md5 = probability_matrix_hash[primary_tf_key].MD5
	primary_tf_free_conc = free_conc_hash[primary_tf_key]
	cur_coop_model_hash = coop_model_hash[primary_tf_key]
	cur_K_dimer_hash = K_dimer_hash[primary_tf_key]
	
	coop_data_tuple = [((cur_K_dimer_hash[coop_tf_key],
						 free_conc_hash[coop_tf_key],
						 cur_coop_model_hash[coop_tf_key],
						 probability_matrix_hash[coop_tf_key].MD5))
					   for coop_tf_key in coop_tf_key_list]
	## print 'coop_data_tuple:', coop_data_tuple
	
	sorted_chromosome_label_list = sort_numeric_string(chromosome_hash.keys())
	if __debug__: print >>sys.stderr, 'Coop Scoring Feature Types:', feature_types
	for chromosome_label in sorted_chromosome_label_list:
		chromosome = chromosome_hash[chromosome_label]
		if __debug__:
			print >>sys.stderr, 'Coop Scoring Features of Chromosome:', chromosome_label

		for_primary_Kas = chromosome.GetHitList(primary_tf_md5).Forward
		rev_primary_Kas = chromosome.GetHitList(primary_tf_md5).ReverseComplement
		coop_tf_tuple = [((K_dimer,free_conc,coop_model,
						   chromosome.GetHitList(coop_tf_md5).Forward,
						   chromosome.GetHitList(coop_tf_md5).ReverseComplement))
						 for K_dimer,free_conc,coop_model,coop_tf_md5 in coop_data_tuple]
		## ELEMENTS OF coop_tf_tuple: (K_dimer, coop_tf_free_conc, coop_model, for_coop_Kas, rev_coop_Kas in coop_tf_tuple)

		for cur_feature in chromosome.FeatureIterator(feature_types):
			cur_feature_score = CooperativeScoreFeature(cur_feature,
														regulatory_region_model,
														for_primary_Kas, 
														rev_primary_Kas, 
														primary_tf_free_conc,
														coop_tf_tuple)
			list_of_score_tuples.append(tuple((cur_feature_score,cur_feature)))
	return list_of_score_tuples



#@-body
#@-node:1::CooperativeScoreFeature
#@-others


#@-body
#@-node:0::@file coop_score_feature_gomer.py
#@-leo
