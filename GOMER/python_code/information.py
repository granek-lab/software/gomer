#! /usr/bin/env python
from __future__ import division
from __future__ import generators
#@+leo
#@+node:0::@file information.py
#@+body
#@@first
#@@first
#@@first
#@@language python

"""
*Description:

Functions for doing calculations related to information content.  The design of these
functions are distantly derived from studying the code for making sequence logos
"""


#@<<imports>>
#@+node:1::<<imports>>
#@+body
##import os
##os.environ['PYCHECKER'] = '--allglobals --stdlib'
##import pychecker.checker
##import check_python_version
##check_python_version.CheckVersion()

import math
import operator

from exceptions_gomer import GomerError
#@-body
#@-node:1::<<imports>>



#@<<constants>>
#@+node:3::<<constants>>
#@+body
TOLERANCE = 1e-10
ln2 = math.log(2.0)

#@-body
#@-node:3::<<constants>>



#@<<InformationError>>
#@+node:4::<<InformationError>>
#@+body
class InformationError(GomerError):
	def __init__(self, probabilities, message):
		self.message = message + \
					   '\nError encountered in following probabilities:\n' + \
					   ', '.join(map(str, probabilities)) + '\n'

#@-body
#@-node:4::<<InformationError>>




#@+others
#@+node:2::tests
#@+body
#@+doc
# 
# 
# ./information.py 0.2 0.2 0.2 0.4
# ./information.py 0.0 0.0 0.0 1
# ./information.py 1e-10 1e-10 1e-10 1
# ./information.py 1e-12 1e-12 1e-12 1
# ./information.py 0.01 0.01 0.01 0.97
# ./information.py 0.25 0.25 0.25 0.25
# ./information.py 0.001 0.001 0.001 0.997
# ./information.py 0.0001 0.0001 0.0001 0.9997
# ./information.py 0.00001 0.00001 0.00001 0.99997
# ./information.py 1e-12 1e-12 .05 1
# ./information.py 0.1 0.1 0.1 0.2 0.2 0.3
# ./information.py 0.1 0.1 0.1 0.2 0.2 0.1 0.1 0.1
# ./information.py 0.8 0.2
# ./information.py 0.9 0.1
# ./information.py 0.9999 0.0001

#@-doc
#@-body
#@-node:2::tests
#@+node:5::log_base
#@+body
def LogBase (x, base):
	return math.log(x)/math.log(base)

def Log2 (x):
	return math.log(x)/math.log(2)

#@-body
#@-node:5::log_base
#@+node:6::information_content
#@+body
def information_content (probabilities) :
	"""
	calculates information content for a list of probabilities

	Each of the N probabilities represents the likelihood of event x occuring.

	There should be one probability supplied for each of the possible events, and the
	probabilities should sum to 1
	"""
	# prob_sum = reduce(lambda x,y: x + y, probabilities)	
	prob_sum = reduce(operator.add, probabilities)		
	if abs(1 - prob_sum) >	TOLERANCE :
		raise InformationError (probabilities, "Probabilities don't sum to 1")
	info = 0
	for cur_prob in probabilities:
		info += -(cur_prob * log_base(cur_prob, 2))
	info = log_base(len(probabilities), 2) - info
	return info
#@-body
#@-node:6::information_content
#@+node:7::alt_information_content
#@+body

# def _add(x,y): return x+y

def _sum(seq): 
	# def add(x,y): return x+y
	return reduce(operator.add, seq, 0)


def information_content_alt (probabilities) :
	prob_sum = reduce(lambda x,y: x + y, probabilities)	
	if abs(1 - prob_sum) >	TOLERANCE :
		raise InformationError(probabilities, "Probabilities don't sum to 1")
	alphabet_size = len (probabilities)
	hmax = math.log(alphabet_size)
	info = hmax
	info +=	 (_sum (map ((lambda freq: (freq * (math.log(freq)))), probabilities)))
	info = float(info)/float(ln2)
	return info

#@-body
#@-node:7::alt_information_content
#@-others







#@<<if __main__>>
#@+node:8::<<if __main__>>
#@+body
if __name__ == '__main__':
	import sys
	if len(sys.argv) > 1:
		probs = map(float, sys.argv[1:])
	else :
		print 'Need at least one probability'
		sys.exit(1)
	# print probs
	print '1:  ', information_content(probs)
	print 'alt:', information_content_alt(probs)



#@-body
#@-node:8::<<if __main__>>



#@-body
#@-node:0::@file information.py
#@-leo
