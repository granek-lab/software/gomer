#! /usr/bin/env python

"""
This code comes entirely from a post to the "Python-list" mailing list by nbecker@fred.net


Since there is no indication as to the license of this code and since it was posted to a public mailing list, I assume it is in the public domain, and will treat it as such.

At the time of this writing, the post could be found at:
http://mail.python.org/pipermail/python-list/1999-November/015625.html
"""

import popen2

class grace:
	def __init__ (self):
		self.p = popen2.Popen3 ("xmgrace -noask -dpipe 0")

	def Command (self, cmd):
		if (cmd[-1] != '\n'):
			cmd = cmd + '\n'
		self.p.tochild.write (cmd)

	def Flush (self):
		self.p.tochild.flush()

	def Wait (self):
		return self.p.wait()

class grace_no_safe(grace):
	"""
	opening grace in '-nosafe' mode allows writing files (in -safe mode, the default, the filesystem can't be modified
	"""
	def __init__ (self):
		self.p = popen2.Popen3 ("xmgrace -nosafe -noask -dpipe 0")

		
if (__name__ == "__main__"):
	g = grace()

	g.Command ("g0.s0 point 0,0")
	g.Command ("g0.s0 point 1,1")
	g.Flush()
	print "returned:", g.Wait()
