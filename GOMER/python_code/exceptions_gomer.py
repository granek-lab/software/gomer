#! /usr/bin/env python
from __future__ import division
#@+leo
#@+node:0::@file exceptions_gomer.py
#@+body
#@@first
#@@first
#@@language python

"""
*Description:

Basic exception classes.
"""
##import os
##os.environ['PYCHECKER'] = '--allglobals --stdlib'
##import pychecker.checker


#@+others
#@+node:1::Base Error
#@+body
# class GomerError(StandardError):
class GomerError(Exception):
	"""Base class for exceptions in this module."""
	def __init__ (self, message=''):
		## super(GomerError, self).__init__()
		self.message=message
		
	def __str__ (self):
		return	'\n\n' + '*'*60 + '\n' + self.message + '\n' + '*'*60 + '\n'

class FeatureTypeError(GomerError):
	def __init__ (self, message, known_feature_types):
		self.message = message +'\nKnown Feature Types: '+','.join(known_feature_types)
				 

#@-body
#@-node:1::Base Error
#@+node:2::NOT_DONE_YET Error
#@+body
# class NOT_DONE_YET(NotImplementedError):
class NOT_DONE_YET(Exception):
	def __init__ (self):
		# super(NOT_DONE_YET, self).__init__()
		print >>sys.stderr, 'This functionality is not done yet!!!!'
#@-body
#@-node:2::NOT_DONE_YET Error
#@-others



#@+doc
# 
# class InputError(Error):
#	  """Exception raised for errors in the input.
# 
#	  Attributes:
#		  expression -- input expression in which the error occurred
#		  message -- explanation of the error
#	  """
# 
#	  def __init__(self, expression, message):
#		  self.expression = expression
#		  self.message = message
# 
# class TransitionError(Error):
#	  """Raised when an operation attempts a state transition that's not
#	  allowed.
# 
#	  Attributes:
#		  previous -- state at beginning of transition
#		  next -- attempted new state
#		  message -- explanation of why the specific transition is not allowed
#	  """
# 
#	  def __init__(self, previous, next, message):
#		  self.previous = previous
#		  self.next = next
#		  self.message = message
# 

#@-doc
#@-body
#@-node:0::@file exceptions_gomer.py
#@-leo
