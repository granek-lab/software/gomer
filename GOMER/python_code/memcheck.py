

#@<<memcheck>>
#@+node:3::<<memcheck>>
#@+body
def getCommandOutput2(command):
    child = os.popen(command)
    data = child.read()
    err = child.close()
    if err:
        raise RuntimeError, '%s failed with exit code %d' % (command, err)
    return data

memlist = []

def memcheck(label):
	if not memlist:
		command_text = 'ps -o pmem,rss,rssize,sz ' + str(os.getpid()) 
	else :
		command_text = 'ps --no-header -o pmem,rss,rssize,sz ' + str(os.getpid()) 
	mem_usage = getCommandOutput2(command_text).strip()
	memlist.append(mem_usage + '\t-\t' + label)
	return mem_usage

def print_memcheck():
	print '+++++++++++++++++++++++++++++++++++'
	for line in memlist:
		print line	
	print '+++++++++++++++++++++++++++++++++++'



#@-body
#@-node:3::<<memcheck>>




