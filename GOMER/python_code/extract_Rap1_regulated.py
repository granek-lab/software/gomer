#! /usr/bin/env python

from __future__ import division

import os
from sets import Set
from optparse import OptionParser

from regulated_feature_parser_gomer import ParseRegulatedFeatures
from coordinate_feature_parser_gomer import CoordinateFeatureParse

def main():
	usage = 'usage: %prog ALL_REGULATED_FILE ORF_COORDINATE_FILE INTERGENIC_COORDINATE_FILE'
	# usage += '\n	 Compare CACHE_FILE_1 and CACHE_FILE_2'
	parser = OptionParser(usage)
	parser.add_option("-e", "--exclude", action="store_true",
					  help="'In orf section, 'EXCLUDE' all the intergenic features which ARE in ALL_REGULATED_FILE and in intergenic section, 'EXCLUDE' all the orf features which ARE in ALL_REGULATED_FILE and all the features which are in both orf and intergenic coordinate files",
					  default=False)

	parser.add_option("--exclude_others", action="store_true",
					  help="In orf section, 'EXCLUDE' all the intergenic features not in ALL_REGULATED_FILE and In intergenic section, 'EXCLUDE' all the orf features not in ALL_REGULATED_FILE  (NOTE: --exclude and --exclude_others are complimentary)",
					  default=False)
	
	(options, args) = parser.parse_args()
		
	if len(args) == 3:
		all_regulated_filename = args[0]
		orf_coordinate_filename = args[1]
		intergenic_coordinate_filename = args[2]
	else:
		parser.error("incorrect number of arguments")

	(regulated,
	 excluded,
	 unregulated) = ParseRegulatedFeatures(all_regulated_filename)

	chromosome_hash, orf_feature_dictionary, ambiguous_feature_names, seq_info_hash = 	\
					 CoordinateFeatureParse(orf_coordinate_filename,{},{},{})
	chromosome_hash, intergenic_feature_dictionary, ambiguous_feature_names, seq_info_hash = 	\
					 CoordinateFeatureParse(intergenic_coordinate_filename,{},{},{})
		

	if len(excluded) <> 0:
		raise 'len(excluded) <> 0'
	elif len(unregulated) <> 0:
		raise 'len(unregulated) <> 0'

	orf_regulated_coordinates = []
	# orf_excluded_coordinates = []
	intergenic_regulated_coordinates = []
	# intergenic_excluded_coordinates = []
	regulated_found_in_both_coordinates = []
	regulated_missing_from_both_coordinates = []
	unregulated_found_in_both_coordinates = []
	
####------------------------------------------------------------
##	for name in regulated:
##		if (name in orf_feature_dictionary) and (name not in intergenic_feature_dictionary):
##			orf_regulated_coordinates.append(name)
##		elif (name in intergenic_feature_dictionary) and (name not in orf_feature_dictionary):
##			intergenic_regulated_coordinates.append(name)
##		elif (name in orf_feature_dictionary) and (name in intergenic_feature_dictionary):
##			regulated_found_in_both_coordinates.append(name)
##		else:
##			regulated_missing_from_both_coordinates.append(name)
####------------------------------------------------------------
##------------------------------------------------------------
	for name in regulated:
		if (name in orf_feature_dictionary) and (name in intergenic_feature_dictionary):
			regulated_found_in_both_coordinates.append(orf_feature_dictionary[name].Name)
			del orf_feature_dictionary[name]
			del intergenic_feature_dictionary[name]
		elif name in orf_feature_dictionary:
			orf_regulated_coordinates.append(orf_feature_dictionary[name].Name)
			del orf_feature_dictionary[name]
		elif name in intergenic_feature_dictionary:
			intergenic_regulated_coordinates.append(intergenic_feature_dictionary[name].Name)
			del intergenic_feature_dictionary[name]
		else:
			regulated_missing_from_both_coordinates.append(name)

	remaining_orf_keys_set = Set(orf_feature_dictionary.keys())
	remaining_intergenic_keys_set = Set(intergenic_feature_dictionary.keys())
	remaining_intersection = remaining_orf_keys_set.intersection(remaining_intergenic_keys_set)

	for name in remaining_intersection:
		unregulated_found_in_both_coordinates.append(orf_feature_dictionary[name].Name)
		del orf_feature_dictionary[name]
		del intergenic_feature_dictionary[name]

##------------------------------------------------------------
	# print '# Names in', os.path.basename(all_regulated_filename), 'which are also in', os.path.basename(coordinate_filename)
	print '#', '-'*20, '\n', '# ORF SECTION\n', '#', '-'*20
	print '# Names in', all_regulated_filename, 'which are also in', orf_coordinate_filename
	print '\n'.join(orf_regulated_coordinates)

	print '\n# Names in', all_regulated_filename, 'which are also in BOTH', orf_coordinate_filename, 'AND', intergenic_coordinate_filename
	print '\n'.join(regulated_found_in_both_coordinates)

	if options.exclude:
		print '\n# Names in', all_regulated_filename, 'which are in', intergenic_coordinate_filename
		print '\n'.join([feature_name + '\t' + 'EXCLUDED' for feature_name in intergenic_regulated_coordinates])

		print '\n# Names in', all_regulated_filename, 'which are in NEITHER', orf_coordinate_filename, 'NOR', intergenic_coordinate_filename
		print '\n'.join(['# ' + feature_name  for feature_name in regulated_missing_from_both_coordinates])

	if options.exclude_others:
		print '\n# Names in', intergenic_coordinate_filename, 'which are NOT in', all_regulated_filename
		print '\n'.join([feature_name + '\t' + 'EXCLUDED' for feature_name in intergenic_feature_dictionary])

		print '\n# Names in both ', orf_coordinate_filename, 'AND', intergenic_coordinate_filename, 'which are NOT in', all_regulated_filename
		print '\n'.join(['# ' + feature_name + '\t' + 'UNREGULATED' for feature_name in unregulated_found_in_both_coordinates])
		
	#===========================================================================
	print '#' + '-'*79 
	print '#' + '-'*79 
	print '#' + '-'*79 
	#===========================================================================
	print '#', '-'*20, '\n', '# INTERGENIC SECTION\n', '#', '-'*20
	print '# Names in', all_regulated_filename, 'which are also in', intergenic_coordinate_filename
	print '\n'.join(intergenic_regulated_coordinates)

	if options.exclude:
		print '\n# Names in', all_regulated_filename, 'which are also in BOTH', orf_coordinate_filename, 'AND', intergenic_coordinate_filename
		print '\n'.join([feature_name + '\t' + 'EXCLUDED' for feature_name in regulated_found_in_both_coordinates])

		print '\n# Names in', all_regulated_filename, 'which are in', orf_coordinate_filename
		print '\n'.join([feature_name + '\t' + 'EXCLUDED' for feature_name in orf_regulated_coordinates])

		print '\n# Names in', all_regulated_filename, 'which are in NEITHER', intergenic_coordinate_filename, 'NOR', orf_coordinate_filename 
		print '\n'.join(['# ' + feature_name for feature_name in regulated_missing_from_both_coordinates])

	if options.exclude_others:
		print '\n# Names in', orf_coordinate_filename, 'which are NOT in', all_regulated_filename
		print '\n'.join([feature_name + '\t' + 'EXCLUDED' for feature_name in orf_feature_dictionary])

		print '\n# Names in both ', orf_coordinate_filename, 'AND', intergenic_coordinate_filename, 'which are NOT in', all_regulated_filename
		print '\n'.join([feature_name + '\t' + 'EXCLUDED' for feature_name in unregulated_found_in_both_coordinates])
	
if __name__ == "__main__":
	main()
