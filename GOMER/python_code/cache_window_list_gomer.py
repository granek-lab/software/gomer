from __future__ import division
#@+leo
#@+node:0::@file cache_window_list_gomer.py
#@+body
#@@first
#@@language python

"""
*Description:

"""
##import os
##os.environ['PYCHECKER'] = '--allglobals --stdlib'
##import pychecker.checker

##import check_python_version
##check_python_version.CheckVersion()

import Numeric
import struct
import types

#@<<class CacheWindowList>>
#@+node:1::<<class CacheWindowList>>
#@+body
class CacheWindowList(object):

	#@+others
	#@+node:1::__init__
	#@+body
	def __init__(self, cache_file_handle, number_of_windows, offset_in_file):
	##	self._bytes_per_value = struct.calcsize('f')
		self._bytes_per_value = Numeric.array(1,Numeric.Float).itemsize()
		self._num_windows = number_of_windows
		self._first_offset = offset_in_file
	##	self._last_offset = offset_in_file + (self._bytes_per_value * (number_of_windows -1))
		self._last_offset = offset_in_file + (self._bytes_per_value * number_of_windows)
		self._handle = cache_file_handle

	#@+doc
	# 
	# 	self._last_offset_in_file is the last valid offset in the file.
	# 
	# 	In other words, if you do:
	# 
	# 		self._handle.seek(self._last_offset_in_file)
	# 		unpack('f', self._handle.read(self._bytes_per_value))
	# 
	# 	you get the score of the last window
	# 
	# 	the next window is from another hitlist
	# 
	# 
	# 
	# string represenation of file:
	# 
	# 
	# 
	# 	lets say:
	# 
	# 	bytes_per_value = 4
	# 	start_offset = 1000
	# 	number_of_windows = 5
	# 
	# 
	# 
	# 	last_offset = start_offset + (bytes_per_value * (number_of_windows - 1))
	# 
	# 	Therefore:
	# 	last_offset = 1016
	# 
	# 
	# 
	# 	next hitlist offset is 1020
	# 
	# 	     |       |       |       |       |       |       |
	# 	. . .a b c d e f g h i j k l m n o p q r s t u v w x y z . . .
	# 
	# 		 ^                                       ^
	# 		 |                                       |
	# 		 1000                                    1020

	#@-doc
	#@-body
	#@-node:1::__init__
	#@+node:2::__getitem__
	#@+body
	def  __getitem__(self, indexOrSlice):
		if type(indexOrSlice) is int:
			index = indexOrSlice
			if indexOrSlice < 0:
				index += self._num_windows
	
	##		data_offset = self._offset + (index * self._bytes_per_value)
			data_offset = self._first_offset + (index * self._bytes_per_value)
			if self._first_offset <= data_offset <= self._last_offset:
				self._handle.seek(data_offset)
	##			array = unpack('f', self._handle.read(self._bytes_per_value))
				array = Numeric.fromstring(self._handle.read(self._bytes_per_value), Numeric.Float)
				return array[0]
			else:
				raise IndexError, 'index out of range'

	#@+doc
	# 
	# 		if -self._num_windows =< indexOrSlice < self._num_windows:
	# @doc
	# 			if indexOrSlice >= 0:
	# 				data_offset = self._offset + indexOrSlice
	# 			else:
	# 				data_offset = self._offset + indexOrSlice + self._num_windows

	#@-doc
	#@@code
	##-----------------------------------------------------------------------------
		elif type(indexOrSlice) is types.SliceType:
			stop_index = indexOrSlice.stop
			start_index = indexOrSlice.start
	
			if stop_index is None:
	##			stop_index = self._num_windows - 1
				stop_index = self._num_windows
			elif stop_index < 0 :
				stop_index += self._num_windows
	
			if start_index is None:
				start_index = 0
			elif start_index < 0 :
				start_index += self._num_windows
			
			start_offset = self._first_offset + (start_index * self._bytes_per_value)
			if start_offset < self._first_offset:
				start_offset = self._first_offset
			elif start_offset > self._last_offset:
				start_offset = self._last_offset
	
			stop_offset = self._first_offset + (stop_index * self._bytes_per_value)
			if stop_offset < self._first_offset:
				stop_offset = self._first_offset
			elif stop_offset > self._last_offset:
				stop_offset = self._last_offset
	
			bytes_to_read = stop_offset - start_offset
			if bytes_to_read < 0:
				bytes_to_read = 0
			
			self._handle.seek(start_offset)
			binary_string_from_file = self._handle.read(bytes_to_read)
	##		##----------------------------------------------------------------------
	##		try:
	##			array_of_hits = Numeric.fromstring(binary_string_from_file, Numeric.Float)
	##		except ValueError:
	##			print 'binary_string_from_file:', binary_string_from_file
	##			print 'Numeric.Float:', Numeric.Float
	##			print 'bytes_to_read:', bytes_to_read
	##			print 'indexOrSlice.stop:', indexOrSlice.stop
	##			print 'indexOrSlice.start:', indexOrSlice.start
	##			print 'stop_index:', stop_index
	##			print 'start_index:', start_index
	##			print 'self._first_offset:', self._first_offset
	##			print 'self._bytes_per_value:', self._bytes_per_value
	##			print 'start_offset:', start_offset
	##			print 'stop_offset:', stop_offset
	##			raise 
	##		##----------------------------------------------------------------------
			array_of_hits = Numeric.fromstring(binary_string_from_file, Numeric.Float)
			return array_of_hits
			
			
		else:
			raise TypeError, 'sequence index must be integer'
			

	#@+doc
	# 
	# >>> (1,2,3,4,5)[-1]
	# 5
	# >>> (1,2,3,4,5)[:-1]
	# (1, 2, 3, 4)
	# >>> (1,2,3,4,5)[-1:]
	# (5,)
	# >>> (1,2,3,4,5)[-1:2]
	# ()
	# >>> (1,2,3,4,5)[2:-1]
	# (3, 4)
	# >>>

	#@-doc
	#@@code
	#@+doc
	# 
	# >>> (1,2,3,4,5)[5]
	# Traceback (most recent call last):
	#   File "<pyshell#47>", line 1, in ?
	#     (1,2,3,4,5)[5]
	# IndexError: tuple index out of range
	# >>> (1,2,3,4,5)[-1]
	# 5
	# >>> (1,2,3,4,5)[-3]
	# 3
	# >>> (1,2,3,4,5)[-4]
	# 2
	# >>> (1,2,3,4,5)[-5]
	# 1
	# >>> (1,2,3,4,5)[-6]
	# Traceback (most recent call last):
	#   File "<pyshell#52>", line 1, in ?
	#     (1,2,3,4,5)[-6]
	# IndexError: tuple index out of range
	# >>>
	# 
	# @doc
	# def  __getitem__(self, indexOrSlice):
	#     try:
	#         return self._sequence[indexOrSlice]
	#     except TypeError:
	#         return self._sequence[indexOrSlice.start:indexOrSlice.stop]
	# 
	# >>> class SliceTester:
	#     def __init__(self):
	#         self.data = ['zero', 'one', 'two', 'three', 'four']
	# 
	#     def  __getitem__(self, indexOrSlice):
	#         try:
	#             return self.data[indexOrSlice]
	#         except TypeError:
	#             return self.data[indexOrSlice.start:indexOrSlice.stop]
	# 
	# >>>
	# KeyboardInterrupt
	# >>> theSlice = SliceTester()
	# >>> a = theSlice[2]
	# >>> a = theSlice[2]
	# >>> b = theSlice[:3]
	# >>> print a
	# two
	# >>> print b
	# ['zero', 'one', 'two']
	# >>> theSlice[4]
	# 'four'
	# >>> theSlice[2:4]
	# ['two', 'three']
	# >>> class SliceTester(object):
	#     def __init__(self):
	#         self.data = ['zero', 'one', 'two', 'three', 'four']
	# 
	#     def  __getitem__(self, indexOrSlice):
	#         try:
	#             return self.data[indexOrSlice]
	#         except TypeError:
	#             return self.data[indexOrSlice.start:indexOrSlice.stop]
	# 
	# 
	# >>> theSlice[2:4]
	# ['two', 'three']
	# >>> theSlice = SliceTester()
	# >>> theSlice[2:4]
	# ['two', 'three']
	# >>> class Blah(object):
	# 	def __init__(self, string):
	# 		self._string = string
	# 	def __getitem__(self, indexOrSlice):
	# 		print indexOrSlice
	# 		return 10
	# 
	# 
	# >>> a = Blah('abcdefg')
	# >>> a[1]
	# 1
	# 10
	# >>> a[1:3]
	# slice(1, 3, None)
	# 10
	# >>> a[1:]
	# slice(1, None, None)
	# 10
	# >>> a[:]
	# slice(None, None, None)
	# 10
	# >>> a[:3]
	# slice(None, 3, None)
	# 10
	# >>> slice
	# <built-in function slice>
	# >>> slice(1)
	# slice(None, 1, None)
	# >>> type(a)
	# <class '__main__.Blah'>
	# >>> type(a[1])
	# 1
	# <type 'int'>
	# >>> class Blah(object):
	# 	def __init__(self, string):
	# 		self._string = string
	# 	def __getitem__(self, indexOrSlice):
	# 		return indexOrSlice
	# 
	# 
	# >>> a = Blah('abcdefg')
	# >>> type(a[1])
	# <type 'int'>
	# >>> a[1]
	# 1
	# >>> a[1:3]
	# slice(1, 3, None)
	# >>> type(a[1:3])
	# <type 'slice'>
	# >>> (a[1:3])
	# slice(1, 3, None)
	# >>> (a[1:3])[1]
	# Traceback (most recent call last):
	#   File "<pyshell#38>", line 1, in ?
	#     (a[1:3])[1]
	# TypeError: unsubscriptable object
	# >>> (a[1:3]).1
	# SyntaxError: invalid syntax
	# >>> (a[1:3]).start
	# 1
	# >>> (a[1:3]).stop
	# 3
	# >>> (a[1:3]).step
	# >>> dir(slice)
	# ['__call__', '__class__', '__cmp__', '__delattr__', '__doc__', 
	# '__getattribute__', '__hash__', '__init__', '__name__', '__new__', 
	# '__reduce__', '__repr__', '__self__', '__setattr__', '__str__']
	# >>> dir(slice())
	# Traceback (most recent call last):
	#   File "<pyshell#44>", line 1, in ?
	#     dir(slice())
	# TypeError: slice() takes at least 1 argument (0 given)
	# >>> dir(slice(1))
	# ['__class__', '__cmp__', '__delattr__', '__doc__', '__getattribute__', 
	# '__hash__', '__init__', '__new__', '__reduce__', '__repr__', 
	# '__setattr__', '__str__', 'start', 'step', 'stop']
	# >>>

	#@-doc
	#@-body
	#@-node:2::__getitem__
	#@+node:3::__len__
	#@+body
	def __len__(self):
		return self._num_windows
	#@-body
	#@-node:3::__len__
	#@-others
#@-body
#@-node:1::<<class CacheWindowList>>


#@-body
#@-node:0::@file cache_window_list_gomer.py
#@-leo
