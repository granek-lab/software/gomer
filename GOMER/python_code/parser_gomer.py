#! /usr/bin/env python
from __future__ import division
#@+leo
#@+node:0::@file parser_gomer.py
#@+body
#@@first
#@@first
#@@language python


"""
*Description:

A base class for parsers.

I'm pretty sure that this is a virtual superclass, but I'm not positive.
"""

#@<<imports>>
#@+node:1::<<imports>>
#@+body
##import os
##os.environ['PYCHECKER'] = '--allglobals --stdlib'
##import pychecker.checker

##import check_python_version
##check_python_version.CheckVersion()

from exceptions_gomer import GomerError

import sys
#@-body
#@-node:1::<<imports>>


#@<<class Parser>>
#@+node:2::<<class Parser>>
#@+body
class Parser(object):
	"""
	This class is mostly virtual, it provides the the basic functionality such as opening files, 
	and doing checks
	"""

	#@+others
	#@+node:1::__init__
	#@+body
	def __init__ (self):
		self._filehandle = None
	#@-body
	#@-node:1::__init__
	#@+node:2::LoadFile
	#@+body
	def LoadFile (self, filename):
		""" 
		This LoadFile isn't checking anything about the file, or catching any exceptions
		So if there is a problem with opening the file (e.g. it doesn't exist, or it isn't readable)
		the caller needs to catch the IOError and deal with it - probably giving an error to the user
		"""
		self._filehandle = open(filename, 'r')
	#@-body
	#@-node:2::LoadFile
	#@+node:3::Parse
	#@+body
	def Parse(self):
		"""
		This functions is almost entirely virtual, but it checks to be sure that we have a filehandle
		"""
		if not self._filehandle:
			raise FileError, 'Parser must have a file loaded before Parsing (use LoadFile)'
	#@-body
	#@-node:3::Parse
	#@-others
#@-body
#@-node:2::<<class Parser>>



#@<<Parser Errors>>
#@+node:3::<<Parser Errors>>
#@+body
#@+doc
# 
# class InputError(Error):
# 	  """Exception raised for errors in the input.
# 
# 	  Attributes:
# 		  expression -- input expression in which the error occurred
# 		  message -- explanation of the error
# 	  """
# 
# 	  def __init__(self, expression, message):
# 		  self.expression = expression
# 		  self.message = message
# 
# class TransitionError(Error):
# 	  """Raised when an operation attempts a state transition that's not
# 	  allowed.
# 
# 	  Attributes:
# 		  previous -- state at beginning of transition
# 		  next -- attempted new state
# 		  message -- explanation of why the specific transition is not allowed
# 	  """
# 
# 	  def __init__(self, previous, next, message):
# 		  self.previous = previous
# 		  self.next = next
# 		  self.message = message
# 

#@-doc
#@@code
#@+others
#@+node:1::ParseError
#@+body
class ParseError(GomerError):
	"""Exception raised for formatting errors encountered during parsing files.

	Attributes:
		line -- line of file containing error
		message -- explanation of the error
	"""
	def __init__(self, file_name, line, line_number, message):
		self.message = '\n'.join((message,
								  'The problem occured on line ' +str(line_number) +' of ' + file_name + ':', line
								  ))

#@-body
#@-node:1::ParseError
#@+node:2::FileError
#@+body
class FileError(GomerError):
	"""
	Exceptions raised in opening files
	"""
	pass

#@-body
#@-node:2::FileError
#@-others




#@-body
#@-node:3::<<Parser Errors>>


#@-body
#@-node:0::@file parser_gomer.py
#@-leo
