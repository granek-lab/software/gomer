#! /usr/bin/env python
from __future__ import division
from __future__ import generators
#@+leo
#@+node:0::@file roman.py
#@+body
#@@first
#@@first
#@@first
#@@language python

"""
*Description:

Functions for conversion between roman numerals and ints, and vice versa.  This
module was taken, almost exactly, from the Python Cookbook
(chapter 3, section 23)
"""
##import os
##os.environ['PYCHECKER'] = '--allglobals --stdlib'
##import pychecker.checker


#@+doc
# 
# roman.py was taken, almost exactly, from the Python Cookbook (chapter 3, 
# section 23)

#@-doc
#@@code


#@<<int_to_roman>>
#@+node:1::<<int_to_roman>>
#@+body
def int_to_roman(input):
    """ Convert an integer to a Roman numeral. """

    if not isinstance(input, type(1)):
        raise TypeError, "expected integer, got %s" % type(input)
    if not 0 < input < 4000:
        raise ValueError, "Argument must be between 1 and 3999"
    ints = (1000, 900,  500, 400, 100,  90, 50,  40, 10,  9,   5,  4,   1)
    nums = ('M',  'CM', 'D', 'CD','C', 'XC','L','XL','X','IX','V','IV','I')
    result = []
    for i in range(len(ints)):
        count = int(input / ints[i])
        result.append(nums[i] * count)
        input -= ints[i] * count
    return ''.join(result)
#@-body
#@-node:1::<<int_to_roman>>


#@<<roman_to_int>>
#@+node:2::<<roman_to_int>>
#@+body
def roman_to_int(input):
    """ Convert a Roman numeral to an integer. """
    if not isinstance(input, type("")):
        raise TypeError, "expected string, got %s" % type(input)
    input = input.upper()
    nums = {'M':1000, 'D':500, 'C':100, 'L':50, 'X':10, 'V':5, 'I':1}
    sum = 0
    for i in range(len(input)):
        try:
            value = nums[input[i]]
            # If the next place holds a larger number, this value is negative
            if i+1 < len(input) and nums[input[i+1]] > value:
                sum -= value
            else: sum += value
        except KeyError:
            raise ValueError, 'input is not a valid Roman numeral: %s' % input
    # easiest test for validity...
    if int_to_roman(sum) == input:
        return sum
    else:
        raise ValueError, 'input is not a valid Roman numeral: %s' % input

#@-body
#@-node:2::<<roman_to_int>>



#@<<if __main__>>
#@+node:3::<<if __main__>>
#@+body
if __name__ == '__main__':
	print int_to_roman(2002)
	# MMII
	print int_to_roman(1999)
	# MCMXCIX
	roman_to_int('XLII')
	# 42
	roman_to_int('VVVIV')
	# Traceback (most recent call last):
	#  ...
	# ValueError: input is not a valid Roman numeral: VVVIV


#@+doc
# 
# 	import sys
# 	if len(sys.argv) == 3:
# 		hash1 = eval(sys.argv[1])
# 		hash2 = eval(sys.argv[2])
# 	else :
# 		print 'Need at least two hashes'
# 		sys.exit(1)
# 	# print probs
# 	print 'Merged', merge_hashes(hash1, hash2)
# 
# 

#@-doc
#@-body
#@-node:3::<<if __main__>>


#@-body
#@-node:0::@file roman.py
#@-leo
