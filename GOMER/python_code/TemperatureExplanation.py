from __future__ import division

##import os
##os.environ['PYCHECKER'] = '--allglobals --stdlib'
##import pychecker.checker

##import check_python_version
##check_python_version.CheckVersion()

import math
from math import exp

ln = math.log

def LogBase(x,b):
	return math.log(x)/math.log(b)

Log2 = lambda x:LogBase(x,2)

'gamma = 100 * Log2(K)'

the_proof = ('100 * Log2(K) is the calculation used to generate deltaG like values in MEME', 
			 'R*T*ln(K) = 100*Log2(K)', 
			 'T = (100*Log2(K))/(R*ln(K)) =~ 72000')

print '\n\n'
for number in range(len(the_proof)):
	print number + 1, '.', the_proof[number]
print '\n\n'


def blah(K):
	just = 40
	R = 1.9858775229213840e-3
##	R = 0.002
	T1 = 300
	T2 = 72000
	things_to_do = (
		'R',
		'T1',
		'T2',
		'R * T1 * ln(K)', 
		'exp(K/math.exp(K/(R * T1)))', 
		'exp(K/math.exp(K/(R * T2)))',
		'100 * Log2(K)',
		'(R * T1 * ln(K))/(100 * Log2(K))',
		'Log2(K)/ln(K)',
		'144/R',
		'144/0.002',
		'(Log2(K)/ln(K))/R',
		'(100* Log2(K))/(R * ln(K))',
		'(Log2(K))/(R * ln(K))'
		
		)
	for code_string in things_to_do:
		print code_string.ljust(40), eval(code_string)
		

blah(20)



##>>> math.log(10)
##2.3025850929940459
##>>> math.log(10)/math.log(2)
##3.3219280948873626
##>>> math.log(10)/(math.log(10)/math.log(2))
##0.69314718055994529
##>>> (math.log(10)/math.log(2))/math.log(10) 
##1.4426950408889634
##>>> 144/.002
##72000.0
