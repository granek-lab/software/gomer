#! /usr/bin/env python

from __future__ import division

import sys
import os
from optparse import OptionParser

from flat_prob_matrix_printer_gomer import FlatProbMatrixOutput
from probability_matrix_gomer import ProbabilityMatrix
from reverse_complement import reverse_complement

base_list='acgt'
near_zero_prob=1e-20
consensus_prob=1

equal_base_freq_hash = {}
for base in base_list:
	equal_base_freq_hash[base.lower()] = 0.25
	equal_base_freq_hash[base.upper()] = equal_base_freq_hash[base.lower()]

def main():

	usage = 'usage: %prog [options] CONSENSUS_SEQ [NAME]'
	parser = OptionParser(usage=usage)

	parser.add_option('-r', '--revcomp',
					  action="store_true",
					  help="Generate a matrix that is the reverse complement of CONSENSUS_SEQ",
					  default=False)

	(options, args) = parser.parse_args()

	if len(args) == 1:
		consensus_sequence = args[0]
		sequence_name = ''
	elif len(args) == 2:
		consensus_sequence = args[0]
		sequence_name = args[1]
	else:
		parser.print_help()
		sys.exit(2)

	if options.revcomp:
		consensus_sequence = reverse_complement(consensus_sequence)
		
	consensus_hash={}
	for base in base_list:
		consensus_hash[base.lower()] = [near_zero_prob]*len(consensus_sequence)
		# consensus_hash[base.upper()] = consensus_hash[base.lower()]

	for index in range(len(consensus_sequence)):
		consensus_base = consensus_sequence[index]
		consensus_hash[consensus_base.lower()][index] = consensus_prob

	# print '#',
	consensus_matrix = ProbabilityMatrix(consensus_hash, [], sequence_name, 300, equal_base_freq_hash, None)
	print FlatProbMatrixOutput(consensus_matrix)


if __name__ == '__main__':
	main()
