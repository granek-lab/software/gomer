#@+leo
#@+node:0::@file runtime_import.py
#@+body
#@@language python
#@<< runtime_import declarations >>
#@+node:1::<< runtime_import declarations >>
#@+body
##--------------------------------------------------------
## Modified from:
## http://aspn.activestate.com/ASPN/Cookbook/Python/Recipe/159571
##--------------------------------------------------------
import imp
import os.path

extensions = ('.py', '.pyc')


#@-body
#@-node:1::<< runtime_import declarations >>


#@+others
#@+node:2::RuntimeImport
#@+body
def RuntimeImport(filename):
	(path, name) = os.path.split(filename)
	(name, ext) = os.path.splitext(name)
	if ext not in extensions:
		print ext
		raise ImportError, 'Filename must end in .py or .pyc'

	(filehandle, filename, data) = imp.find_module(name, [path])
	try:
		return imp.load_module(name, filehandle, filename, data)
	finally:
		# Since we may exit via an exception, close fp explicitly.
		if filehandle:
			filehandle.close()
#@-body
#@-node:2::RuntimeImport
#@-others



#@<<if __main__ runtime_import >>
#@+node:3::<<if __main__ runtime_import >>
#@+body
if __name__ == '__main__':
	import sys
	import os
	# from runtime_import import RuntimeImport

	if len(sys.argv) <> 2:
		print 'Usage - Must supply:\n\t1. path containing modules'
		sys.exit(2)
	directory = sys.argv[1]

	module_hash = {}
	for file_name in os.listdir(directory):
		file_path = os.path.join(directory, file_name)
		root, extension = os.path.splitext(file_path)
		if extension <> '.py':
			continue
		cur_module = RuntimeImport(file_path)
		print file_name, ':', cur_module.LABEL, '\t\t', file_path
		module_hash[cur_module.LABEL] = cur_module

	print 'Which module should I use?'
	name = ''
	while name not in module_hash:
		name = raw_input('> ')

	module_to_use = module_hash[name]
	handler = module_to_use.Handler()
	handler.handleSomething()
	# file:///usr/share/doc/python2.2/html/lib/examples-imp.html
#@-body
#@-node:3::<<if __main__ runtime_import >>



#@-body
#@-node:0::@file runtime_import.py
#@-leo
