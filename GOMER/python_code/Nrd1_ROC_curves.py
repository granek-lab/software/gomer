#! /usr/bin/env python

import sys
import os
import re
import getopt

from parse_gomer_output import ParseGomerOutput
from regulated_feature_parser_gomer import ParseRegulatedFeatures
from generate_ROC_curve import PlotROCCurve, OutputROCCurveData
from grace_plot import GracePlot

from estimate_p_value import MNCP, ROCAUC

filename_re = re.compile('flank(\d+)_tail(\d+)\.')


def usage():
	print >>sys.stderr, '\nusage:', os.path.basename(sys.argv[0]), 'GOMER_OUTPUT REGULATED_FILE SN_O_RNA_FILE\n'
	print >>sys.stderr, '        ', '-d/--data OUTFILE Output data for generating a ROC Curve to OUTFILE, instead of generating it in Grace'

def main():
	try:
		opts, args = getopt.getopt(sys.argv[1:], "d:", ["data="])
	except getopt.GetoptError:
		# print help information and exit:
		usage()
		sys.exit(2)
	data_filehandle = None
	for o, a in opts:
		if o in ("-d", "--data"):
			data_filehandle = file(a, 'w')

	if len(args) <> 3:
		usage()
		sys.exit(2)
	gomer_output_filename = args[0]
	regulated_filename = args[1]
	sn_o_RNA_filename = args[2]

	gomer_output_filehandle       = file(gomer_output_filename)
	## regulated_filehandle          = file(regulated_filename)
	## sn_o_RNA_filehandle     = file(sn_o_RNA_filename)

	(all_feature_hash,
	 regulated_hash,
	 unknown_feature_hash,
	 gomer_output_skipped_lines,
	 param_hash) = ParseGomerOutput(gomer_output_filehandle)

	(regulated_feature_names,
	 excluded_feature_names,
	 unregulated_feature_names) = ParseRegulatedFeatures(regulated_filename)

	(sn_o_RNA_feature_names,
	 sn_o_RNA_excluded_feature_names,
	 sn_o_RNA_unregulated_feature_names) = ParseRegulatedFeatures(sn_o_RNA_filename)

	unregulated_feature_hash = dict([(name.upper(), None) for name in unregulated_feature_names])
	regulated_feature_hash = dict([(name.upper(), None) for name in regulated_feature_names])
	sn_o_RNA_feature_hash = dict([(name.upper(), None) for name in sn_o_RNA_feature_names])
	for feature_name in regulated_feature_hash:
		del sn_o_RNA_feature_hash[feature_name]



	regulated_ranks = []
	unregulated_ranks = []
	sn_o_RNA_ranks = []  ## exluding regulated
	orf_ranks = []


	## below we use feature.rank, instead of feature.score because the scores
	## GOMER outputs are rounded, so it will for example print a score of 1.000
	## sometimes when the score is actually less than 1.  The ranks however
	## preserve the relative rank order of features since they are based on
	## unrounded scores.
	regulated_check_hash = {}
	sn_o_RNA_check_hash = {}
	
	for key in all_feature_hash:
		feature = all_feature_hash[key]
		if key in regulated_feature_hash:
			regulated_ranks.append(feature.rank)
			if feature.name in regulated_check_hash:
				raise 'Feature from regulated feature list occurs more than once: ' + feature_name
			else:
				regulated_check_hash[feature.name] = feature
		elif key in unregulated_feature_hash:
			## unregulated_feature_hash will be empty unless unregulated features were declared in the "regulated file"
			unregulated_ranks.append(feature.rank)
		else:
			unregulated_ranks.append(feature.rank)
			## raise ', '.join(('Unknown feature name:', key, feature.name))
		## --------------------------------------------------------------
		if key in regulated_feature_hash:
			continue
		elif key in sn_o_RNA_feature_hash:
			sn_o_RNA_ranks.append(feature.rank)
			sn_o_RNA_check_hash[feature.name] = feature
		else:
			orf_ranks.append(feature.rank)

	for feature_name in regulated_feature_hash:
		if feature_name not in regulated_check_hash:
			raise 'Feature from regulated feature list not found: ' + feature_name
	for feature_name in sn_o_RNA_feature_hash:
		if feature_name not in sn_o_RNA_check_hash:
			raise 'Feature from snRNA/snoRNA feature list not found: ' + feature_name

	print 'all_feature_hash', len(all_feature_hash)
	print 'regulated_feature_hash', len(regulated_feature_hash)
	print 'unregulated_feature_hash', len(unregulated_feature_hash)
	print 'regulated_ranks', len(regulated_ranks)
	print 'unregulated_ranks', len(unregulated_ranks)

	print 'sn_o_RNA_ranks', len(sn_o_RNA_ranks)
	print 'orf_ranks', len(orf_ranks)
	
	print 'regulated_hash', len(regulated_hash)
	print 'unknown_feature_hash', len(unknown_feature_hash)
	print 'gomer_output_skipped_lines', len(gomer_output_skipped_lines)
	## print '\n'.join(gomer_output_skipped_lines)


	if data_filehandle:
		## printing data for diagonal
		print >>data_filehandle, '##------------------------------------------'
		print >>data_filehandle, '##', os.path.basename(gomer_output_filename)
		print >>data_filehandle, '## Regulated file:', os.path.basename(regulated_filename)
		print >>data_filehandle, '## sn_o_RNA file:', os.path.basename(sn_o_RNA_filename)
		print >>data_filehandle, '##------------------------------------------'
		print >>data_filehandle, '## Diagonal Line'
		print >>data_filehandle, '0\t0\n', '1\t1\n'
		
		OutputROCCurveData(data_filehandle, regulated_ranks,unregulated_ranks,
						   data_set_name='Regulated vs. Unregulated', higher_is_better=False)
		print >>data_filehandle, ''
		OutputROCCurveData(data_filehandle, sn_o_RNA_ranks, orf_ranks,
						   data_set_name='sn(o)RNA vs. ORF', higher_is_better=False)
		print >>data_filehandle, '\n'

	else:
		plot = GracePlot(square_plot=1)
		plot.XLabel('Fraction of Unregulated')
		plot.YLabel('Fraction of Regulated')
		subtitle = os.path.basename(gomer_output_filename) 
		plot.Subtitle(subtitle)

		plot.AddDiagonal()

		## higher_is_better=False because we want to use the ranks, not the absolute scores (see above for reason)
		PlotROCCurve(plot, regulated_ranks,unregulated_ranks,
					 data_set_name='Regulated vs. Unregulated', higher_is_better=False)
		PlotROCCurve(plot, sn_o_RNA_ranks, orf_ranks,
					 data_set_name='sn(o)RNA vs. ORF', higher_is_better=False)

		plot.Save(gomer_output_filename+'_ROCcurve.eps', 'eps')
		## plot.Exit()

	for function in [ROCAUC, MNCP]:
		print '\t'.join((os.path.basename(gomer_output_filename), 'Regulated vs. Unregulated',
						 function.__name__,
						 str(function(regulated_ranks,
									  unregulated_ranks,
									  higher_is_better=False))))

	for function in [ROCAUC, MNCP]:
		print '\t'.join((os.path.basename(gomer_output_filename), 'sn(o)RNA vs. ORF',
						 function.__name__,
						 str(function(sn_o_RNA_ranks,
									  orf_ranks,
									  higher_is_better=False))))


if __name__ == '__main__':
	main()
