#@+leo
#@comment Created by Leo at Fri Jul 18 16:22:49 2003
#@+node:0::@file generate_coordinate_features_gomer.py
#@+body
"""
This controller is essentially for testing purposes, I can't think of any uses for this other than testing.

This controller takes as input an upstream range (start and stop), a sequence table (from which sequence files can be determined), and a feature file.  From this information, an coordinate feature file is generated for all the orfs in the genome (it shouldn't be too hard to set things up to be able to use any feature, but for the moment orfs are the default).  When GOMER is run in coordinate mode, using this coordinate feature file, it should produce the same results as if GOMER was run using the same parameters, using a single square regulatory region with the same upstream range used to generate the coordinate feature file.
"""

#<imports>>
#<def main>>
#<sort_numeric_strings>>
#<if main>>


#@+others
#@+node:1::imports
#@+body
import sys
import os
from optparse import OptionParser

from gomer_load_functions import load_chromosome_table, load_feature_file, load_chromosome_sequences
from integer_string_compare import sort_numeric_string

#@-body
#@-node:1::imports
#@+node:2::def sort_numeric_strings
#@+body

#@-body
#@-node:2::def sort_numeric_strings
#@+node:3::def main
#@+body
def main():	
	
	#@<<optparse arguments>>
	#@+node:1::<<optparse arguments>>
	#@+body
	version_num = 0.001
	version = '%prog ' + str(version_num)
	usage = 'usage: %prog [options] START STOP ChromTable OUTFILE\n'
	
	parser = OptionParser(usage=usage, version=version)

	parser.add_option('-t', '--tab',
					  action="store_const",
					  help='Produce tab-separated file [default]',
					  const='\t',
					  default='\t',
					  dest='separator')

	parser.add_option('-c', '--comma',
					  action="store_const",
					  help='Produce comma-separated file',
					  const=',',
					  dest='separator')
	(options, args) = parser.parse_args()
	NORM_NUM_ARGS = 4

	sep = options.separator

	if len(args) == (NORM_NUM_ARGS):
		start_range = int(args[0])
		stop_range = int(args[1])
		chromosome_table_file_name = args[2]
		out_filename = args[3]
	else :
		parser.print_help()
		print 'len(args)', len(args)
		print 'args', args
		sys.exit(2)
		
	if start_range < stop_range:
		print 'START must be less than STOP\n\n'
		parser.print_help()
		sys.exit(2)
		
	
	for file_name in [chromosome_table_file_name]:
		if not os.path.exists(file_name):
			print "File doesn't exist:", file_name
			sys.exit(1)
	
	##	for file_name in [out_filename]:
	##		if os.path.exists(file_name):
	##			print "File exists, it would be overwritten:", file_name
	##			sys.exit(1)
	
	out_handle = file(out_filename, 'w')
	
	#@-body
	#@-node:1::<<optparse arguments>>


	(feature_file_list,
	 chromosome_file_hash,
	 chromosome_flags_hash,
	 frequency_hash) = load_chromosome_table(chromosome_table_file_name)

	chromosome_hash, feature_dictionary, ambiguous_feature_names, feature_type_dict = load_feature_file(feature_file_list)
	default_feature = 'ORF'
	feature_types = (default_feature,)

	load_chromosome_sequences(chromosome_file_hash, chromosome_flags_hash,
							  chromosome_hash, feature_dictionary,
							  ambiguous_feature_names)

##===============================================================================
	chrom_keys = chromosome_hash.keys()
	chrom_keys = sort_numeric_string(chrom_keys)

	for chrom_label in chrom_keys:
		# %SEQUENCE       NC_001133       1
		cur_chrom = chromosome_hash[chrom_label]
		out_handle.write(sep.join(('%SEQUENCE',
									cur_chrom.Sequence.Accession,
									chrom_label, cur_chrom.MD5)) + '\n')

	header_string = sep.join(('# Name','Chromosome','StartCoord','StopCoord','Alias(es)','Corresponding','Description'))

	out_handle.write('\n\n' + header_string + '\n\n')
	for chrom_label in chrom_keys:
		cur_chrom = chromosome_hash[chrom_label]
		sequence_length = len(cur_chrom.Sequence)
		for feature in cur_chrom.FeatureIterator(feature_types):
			if feature.Strand == '+':
				left = feature.Start - start_range
				## right = feature.Start - stop_range
				right = feature.Start - stop_range
			elif feature.Strand == '-':
				left = feature.Start + stop_range
				right = feature.Start + start_range
			else:
				raise StandardError, 'Strand of Feature must be "+" or "-"'

			if left < 1:
				left = 1
			elif left >= sequence_length:
				left = sequence_length 

			if right < 1:
				right = 1
			elif right >= sequence_length:
				right = sequence_length
			
			if feature.CommonName:
				feature_name = feature.CommonName
			else:
				feature_name = feature.Name

			for value in feature_name, chrom_label:
				if sep in value:
					print >>sys.stderr, 'ERROR: Element"', value, '" contains separator "'+sep+'"'
					print >>sys.stderr, 'This will make a faulty coordinate feature file (feature', feature_name, ')'
			out_handle.write(sep.join((feature_name,
									   chrom_label,
									   str(left),
									   str(right),
									   '',
									   '',
									   ''))+ '\n')
	out_handle.close()

#@-body
#@-node:3::def main
#@+node:4::if main
#@+body
if __name__ == '__main__':
	main()

#@-body
#@-node:4::if main
#@-others
#@-body
#@-node:0::@file generate_coordinate_features_gomer.py
#@-leo
