#! /usr/bin/env python
from __future__ import division

"""
based on gomer_vision.py
"""
import sys
from reverse_complement import reverse_complement
from optparse import OptionParser

from gomer import load_chromosome_table, load_feature_file, load_chromosome_sequences, load_probability_matrix, base_count
from regulated_feature_parser_gomer import ParseRegulatedFeatures

FORWARD='+'
REVERSE='-'

def main():	
	
	version_num = 0.001
	version = '%prog ' + str(version_num)
	usage = 'usage: %prog [options] ChromTable ProbMatrix FEATURE UPSTREAM_BP DOWNSTREAM_BP\n'
	usage +='        --feature_file ChromTable ProbMatrix         UPSTREAM_BP DOWNSTREAM_BP\n'
	
	parser = OptionParser(usage=usage, version=version)

	parser.add_option('-m', '--probability_matrix',
			  action="append",
			  help='Use additional position weight matrix file PWMFILE.',
			  metavar = 'PWMFILE')

	parser.add_option('-f', '--feature_name',
			  action="append",
			  help='Examine additional FEATURE.',
			  metavar = 'FEATURE')			  

	parser.add_option('--feature_file',
			  help="Use the features found in FILE (in GOMER's regulated feature file format) instead of from the command line.",
			  metavar = 'FILE')			  
	parser.add_option('-o', '--output',
			  help="Save output to FILE.",
			  metavar = 'FILE')			  

	parser.add_option('-c', '--cutoff',
					  type="float",
					  default=0.0,
					  help='Show only scores above CUTOFF value.',
					  metavar = 'CUTOFF')			  


					  
	(options, args) = parser.parse_args()
	NORM_NUM_ARGS = 5



	if len(args) == (NORM_NUM_ARGS):
		chromosome_table_file_name = args[0]
		prob_matrix_filename = args[1]
		first_feature_name = args[2]
		upstream_bp = int(args[3])
		downstream_bp = int(args[4])
	elif len(args) == 4 and options.feature_file:
		chromosome_table_file_name = args[0]
		prob_matrix_filename = args[1]
		upstream_bp = int(args[2])
		downstream_bp = int(args[3])
	else :
		parser.print_help()
		sys.exit(2)

	
	if options.output:
		OUTPUT=file(options.output,'w')
	else:
		OUTPUT=sys.stdout
	
	prob_matrix_filenames = [prob_matrix_filename]
	if options.probability_matrix:
		prob_matrix_filenames.extend(options.probability_matrix)

	if options.feature_file:
		(regulated_feature_names,
		 excluded_feature_names,
		 unregulated_feature_names) = ParseRegulatedFeatures(options.feature_file)
		feature_names = regulated_feature_names.keys()
	else:
		feature_names = [first_feature_name]
		if options.feature_name:
			feature_names.extend(options.feature_name)
	CUTOFF = options.cutoff

	print 'PWM files: ',prob_matrix_filenames
	print 'Feature names: ',feature_names

	(feature_file_list,
	 chromosome_file_hash,
	 chromosome_flags_hash,
	 frequency_hash) = load_chromosome_table(chromosome_table_file_name)

	chromosome_hash, feature_dictionary, ambiguous_feature_names, feature_type_dict = load_feature_file(feature_file_list)

	## feature_dictionary has an entry for every non-ambiguous feature_name in the genome
	## REMEMBER THAT feature_dictionary KEYS MUST BE capitalized!!!!


	## Load chromosome sequences
	load_chromosome_sequences(chromosome_file_hash, chromosome_flags_hash,
				  chromosome_hash, feature_dictionary,
				  ambiguous_feature_names)

	if not frequency_hash:
		print 'NO BASE FREQUENCIES SUPPLIED - CALCULATING . . .'
		frequency_hash = base_count(chromosome_hash)
		print 'FREQUENCIES:', frequency_hash

	## Get PWMs from PWM files, and get overall Kamax.

	probability_matrices = []
	for matrix_filename in prob_matrix_filenames:
		probability_matrices.append(load_probability_matrix(matrix_filename, frequency_hash))


        ## WE NOW DO SCORING FOR EACH FEATURE, ONE AT A TIME:
	# We will store feature info in dictionary features_to_plot
	features_to_plot = {}
	my_features = {}
	sequence_start_indices = {}
	chrom_name_dict = {}
	for feature_name in feature_names:
	        # gomer.py -aFILE/--ambiguous=FILE : Print ambiguous names from feature file FILE
	        #is ambiguous_feature_names all caps?
		if feature_name.upper() in ambiguous_feature_names:
			print >>sys.stderr, 'Ambiguous Name:', feature_name
			print >>sys.stderr, '\nOther Names:\n' + \
			      '\n\n'.join([','.join(feature.AllNames()) for feature in ambiguous_feature_names[feature_name.upper()]])
			sys.exit(1)

		# raise NotImplementedError, 'Test to be sure the feature is in feature_dictionary'
		feature = feature_dictionary[feature_name.upper()]


	        ## Define section of chromosome sequence to examine:

		cur_chrom = feature.Chromosome
		chromosome_sequence = cur_chrom.Sequence.Sequence # Don't ask

		# Section of sequence I care about:
		# check whether start > stop
		# if so, switch upstream with downstream and "stop" with "start"
		if feature.Start > feature.Stop:  # feature.Strand = '-' #(-=crick)
			print "Switching upstream and downstream, and start and stop"
			rel_upstream, rel_downstream = downstream_bp, upstream_bp
			feature_start = feature.Stop
			feature_stop = feature.Start
			print 'upstream bp: ',upstream_bp
			print 'downstream bp: ',downstream_bp
			print 'new feature_start:',feature_start
			print 'new feature_stop:',feature_stop
		else:
			rel_upstream, rel_downstream = upstream_bp, downstream_bp
			feature_start = feature.Start
			print 'Feature Feature_start: ',feature_start
			feature_stop = feature.Stop		
			print 'Feature Stop: ', feature_stop
		
		#-----------------------------------------------------------------------
		begin_seq_index = feature_start - rel_upstream - 1
		if begin_seq_index < 0:
			begin_seq_index = 0
			# print "Upstream  distanceYou asked to look too far upstream. I'll look from the beginning."
		end_seq_index = feature_stop + rel_downstream
		if end_seq_index > len(chromosome_sequence):
			end_seq_index = len(chromosome_sequence)
			# print "You asked to look too far downstream. I'll look until the end."
		print 'begin index: ',begin_seq_index,'  End index: ',end_seq_index
		sequence_to_score = chromosome_sequence[begin_seq_index:end_seq_index]
##		if feature.Strand == '-':  #If feature is on Crick strand
##			# want to score reverse complement of sequence
##			sequence_to_score = reverse_complement(sequence_to_score)
##		## print sequence_to_score
		#-----------------------------------------------------------------------
                ## NOW, FOR EACH TF, SCORE EACH STRAND
		TFs = {}
		for matrix in probability_matrices:  # For each TF
			hitlist = matrix.FilterScoreSequence(sequence_to_score, filter_cutoff_ratio=0)
			strands = {FORWARD : hitlist.Forward,
					   REVERSE : hitlist.ReverseComplement}
			# Store strand score dictionary for this TF in TFs dictionary:
			TFs[matrix.Name] = strands

			## FOR THIS FEATURE, STORE 'TFs' DICTIONARY IN 'features_to_plot' DICTIONARY:
			features_to_plot[feature.Name]=TFs
		my_features[feature.Name] = feature
		sequence_start_indices[feature.Name] = begin_seq_index
		chrom_name_dict[feature.Name] = feature.Chromosome.Name
	##===============================================================================
##		name = ''
##		scale = 1000
##		for matrix in probability_matrices:  # For each TF
##			hitlist = matrix.FilterScoreSequence(sequence_to_score, filter_cutoff_ratio=0)
##			strands = {FORWARD:hitlist.Forward,REVERSE:hitlist.ReverseComplement}
##			for hitindex in range(len(hitlist.Forward)):
##				for_score = hitlist.Forward[hitindex]
##				rev_score = hitlist.ReverseComplement[hitindex]
##				base_index = hitindex + begin_seq_index# first base is 0
##				if for_score >= CUTOFF:
##					print >>OUTPUT, '\t'.join(cur_chrom, str(base_index), str(base_index+1),
##											  name, str(for_score*scale), FORWARD)
##				if rev_score >= CUTOFF:
##					print >>OUTPUT, '\t'.join(cur_chrom, str(base_index), str(base_index+1),
##											  name, str(rev_score*scale), REVERSE)
	##===============================================================================

		
	## NORMALIZATION:
	# For each TF, find the highest Ka value (score) found in 
	# all the sequences scored; call this number Kamax.
	# Normalize each TF to its Kamax.
	Kamaxes = {}
	for matrix in probability_matrices:  # For each TF
		possKamaxes = []
		for featurekey in features_to_plot: # For each feature
			scoretuple = features_to_plot[featurekey][matrix.Name]
			for strandkey in scoretuple:
				possKamaxes.append(max(scoretuple[strandkey]))
	        Kamax = max(possKamaxes)
	        Kamaxes[matrix.Name] = Kamax
	print 'Kamaxes:', Kamaxes

	max_normalized_score_list = []
	for matrix in probability_matrices:
		for featurekey in features_to_plot: # For each feature
			scoretuple = features_to_plot[featurekey][matrix.Name]
			print 'Max raw score:',max(scoretuple[FORWARD]+scoretuple[REVERSE])
			scoretuple[FORWARD] = scoretuple[FORWARD]/Kamaxes[matrix.Name]
			scoretuple[REVERSE] = scoretuple[REVERSE]/Kamaxes[matrix.Name]
			## print 'Max normalized score:',max(scoretuple[FORWARD]+scoretuple[REVERSE])
			max_normalized_score_list.append(max(scoretuple[FORWARD]))
			max_normalized_score_list.append(max(scoretuple[REVERSE]))
	

	## GRAPHING
	max_norm_score = max(max_normalized_score_list)
	max_norm_score += max_norm_score * 1e-10 
	# max_norm_score = max(max_norm_score, 1)
	print 'framearray.yrange = ', (-max_norm_score,max_norm_score)
	print 'framearray.xrange = ', (-upstream_bp, my_features[featurekey].Length+downstream_bp)
	scale=1000
	name='.'
	##--------------------------------------------------------------------

	color_list = (('0,60,120', '100,50,0', '0,0,0'))
	color_count = 0
	for matrix in probability_matrices:
		print matrix.Name
		for feature_name in features_to_plot:
			color = color_list[color_count%len(color_list)]
			chrom_name = 'chr' + chrom_name_dict[feature_name]
			sequence_start = sequence_start_indices[feature_name]
			browser_line = 'browser position ' + chrom_name + ':' \
						   + str(sequence_start) + '-' \
						   + str(sequence_start + len(scoretuple[FORWARD]))
			track_line = ' '.join(('track', 'name=' + matrix.Name,
								   'description=' + matrix.Name,
								   'useScore=1',
								   'color='+color))
			print >>OUTPUT, browser_line
			print >>OUTPUT, track_line
			scoretuple = features_to_plot[feature_name][matrix.Name]
			for strand in ((FORWARD, REVERSE)):
				for hitindex in range(len(scoretuple[strand])):
					score = scoretuple[strand][hitindex]
					if score >= CUTOFF:
						score = str(int(round(score*scale)))
						base_index = hitindex + sequence_start
						print >>OUTPUT, '\t'.join((chrom_name, str(base_index), str(base_index+matrix.Length),
												   name, score, strand))

			color_count += 1

if __name__ == '__main__':
	main()
