#! /usr/bin/env python
from __future__ import division



import sys
import os
import re

import ConfigParser


from exceptions_gomer import GomerError
from chromosome_table_parser import ChromosomeTableParse
from cerevisiae_feature_parser_gomer import FeatureFileParse
## from cerevisiae_sequence_parser_gomer import CerevisiaeSequenceParser
from cerevisiae_sequence_parser_gomer import ParseCerevisiaeSequence
from flat_prob_matrix_parser_gomer import FlatProbMatrixParse
from integer_string_compare import sort_numeric_string
from regulated_feature_parser_gomer import ParseRegulatedFeatures
from coordinate_feature_parser_gomer import CoordinateFeatureParse
from open_file import ExpandPath
from chromosome_gomer import Chromosome

from runtime_import import RuntimeImport
##file_path = '/home/josh/GOMER/python_code/kappa_modules/single_square_reg_region_model_gomer.py'
##module = RuntimeImport(file_path)
##SingleSquareRegulatoryRegionModel = module.RegulatoryRegionModel
##RegulatoryRegionModelName = module.NAME
##print 'Using This Regulatory Region Model:', RegulatoryRegionModelName
##del module



def load_config_file(config_file_name):
## 
## 	if not config_file_name:
## 		config_file_name = '../input_files/config/gomer_config'
	config = ConfigParser.ConfigParser()
	config.read(config_file_name)
	return config


def load_chromosome_table(chromosome_table_file_name):
	## print >>sys.stderr, '============================='
	## sys.stderr.write ('\r' + ' '*80 + '\rLoading Chromosome Table File')
	if __debug__:
		print >>sys.stderr, 'Loading Chromosome Table File:', chromosome_table_file_name
		# print >>sys.stderr, '=========================='
##	chromosome_table_parser = ChromosomeTableParser()
##	chromosome_table_parser.LoadFile(chromosome_table_file_name)
##	feature_file_list, chromosome_file_hash, chromosome_flags_hash, frequency_hash =\
##					   chromosome_table_parser.Parse()

	feature_file_list, chromosome_file_hash, chromosome_flags_hash, frequency_hash =\
					   ChromosomeTableParse(chromosome_table_file_name)

	## memcheck('Chromosome Table Loaded')

	return feature_file_list, chromosome_file_hash, chromosome_flags_hash, frequency_hash


def load_feature_file(feature_file_list):
	# print >>sys.stderr, 'Load Feature File'
	# print >>sys.stderr, '================='
	## sys.stderr.write ('\r' + ' '*80 + '\rLoading Feature File')
	chromosome_hash = {}
	feature_dictionary = {}
	ambiguous_feature_names = {}
	feature_type_dict = {}
	for feature_file_name in feature_file_list:
		if __debug__:
			print >>sys.stderr, 'Loading Feature File:', feature_file_name
			# print >>sys.stderr, '================='

##@+doc
## 		feature_parser = CerevisiaeFeatureParser()
## 		feature_parser.LoadFile(feature_file_name)

##@-doc
##@@code		
		chromosome_hash, feature_dictionary, \
						 ambiguous_feature_names, \
						 feature_type_dict = 	\
						 FeatureFileParse(feature_file_name,
										  chromosome_hash,
										  feature_dictionary,
										  ambiguous_feature_names,
										  feature_type_dict)

##@+doc
## 
## 		chromosome_hash = merge_hashes(chromosome_hash, cur_chromosome_hash)
## 		feature_dictionary = merge_hashes(feature_dictionary, cur_feature_dictionary)

##@-doc
##@@code


##@+doc
## 
## 	print 'Ambiguous Feature Names:'
## 	for orf_key in ambiguous_feature_names:
## 		print orf_key
## 		for cur_orf in ambiguous_feature_names[orf_key]:
## 			print ' ' * 5 + ','.join(cur_orf.AllNames()), ',', cur_orf.ChromosomeName

##@-doc
##@@code
	
	## print >>sys.stderr, 'chromosome_hash.keys()', chromosome_hash.keys() 
##	sum = 0
##	print >>sys.stderr, 'ORF COUNTS'
##	for key in chromosome_hash.keys():
##		count = chromosome_hash[key].OrfCount()
##		sum += count
##		print >>sys.stderr, key, count
##	print 'Total:', sum

	## memcheck('Feature File Loaded')

	return chromosome_hash, feature_dictionary, ambiguous_feature_names, feature_type_dict


def load_regulated_feature_names(regulated_feature_filename, feature_dictionary, ambiguous_names):
	## print >>sys.stderr, 'Load Regulated FEATURE File'
	## print >>sys.stderr, '=============================='
	## sys.stderr.write ('\r' + ' '*80 + '\rLoading Regulated FEATURE File')
	if __debug__:
		print >>sys.stderr, 'Loading Regulated Feature File:', regulated_feature_filename
		# print >>sys.stderr, '=============================='

	(regulated_feature_names,
	 excluded_feature_names,
	 unregulated_feature_names) = ParseRegulatedFeatures(regulated_feature_filename)
##--------------------------------------------------------------
	excluded_features = []
	regulated_features = []
	unregulated_features = []
	unknown_regulated_features = []
##--------------------------------------------------------------
	for cur_featurename in excluded_feature_names:
		if cur_featurename in ambiguous_names:
			error_message = 'Excluded Feature Name: "' + cur_featurename + '" is ambiguous.\n' + \
							'Names in the regulated feature file cannot be ambiguous\nOther Names:\n' + \
							'\n\n'.join([','.join(feature.AllNames()) for feature in ambiguous_names[cur_featurename]])
			raise LoadError(error_message)
		elif cur_featurename not in feature_dictionary:
			unknown_regulated_features.append(cur_featurename)
			print >>sys.stderr, 'Excluded Feature Name: "' + cur_featurename + \
				  '" is not found on any of the chromosomes for which a ' +\
				  ' sequence was loaded.\nIt will be ignored.'
		else:
			excluded_features.append(feature_dictionary[cur_featurename])
##--------------------------------------------------------------
	for cur_featurename in unregulated_feature_names:
		if cur_featurename in ambiguous_names:
			error_message = 'Unregulated Feature Name: "' + cur_featurename + '" is ambiguous.\n' + \
							'Names in the regulated feature file cannot be ambiguous\nOther Names:\n' + \
							'\n\n'.join([','.join(feature.AllNames()) for feature in ambiguous_names[cur_featurename]])
			raise LoadError(error_message)
		elif cur_featurename not in feature_dictionary:
			unknown_regulated_features.append(cur_featurename)
			print >>sys.stderr, 'Unregulated Feature Name: "' + cur_featurename + \
				  '" is not found on any of the chromosomes for which a ' +\
				  ' sequence was loaded.\nIt will be ignored.'
		elif feature_dictionary[cur_featurename] in excluded_features:
			error_message = 'Unregulated Feature is already Excluded:\n' + ','.join(feature.AllNames())
			raise LoadError(error_message)
		else:
			unregulated_features.append(feature_dictionary[cur_featurename])
##--------------------------------------------------------------
	for cur_featurename in regulated_feature_names:
		if cur_featurename in ambiguous_names:
			error_message = 'Regulated Feature Name: "' + cur_featurename + '" is ambiguous.\n' + \
							'Names in the regulated feature file cannot be ambiguous\nOther Names:\n' + \
							'\n\n'.join([','.join(feature.AllNames()) for feature in ambiguous_names[cur_featurename]])
			raise LoadError(error_message)
		elif cur_featurename not in feature_dictionary:
			unknown_regulated_features.append(cur_featurename)
			print >>sys.stderr, 'Regulated Feature Name Ignored (it was not found on any of the chromosomes loaded):', cur_featurename 
		elif feature_dictionary[cur_featurename] in excluded_features:
			error_message = 'Regulated Feature is already Excluded:\n' + ','.join(feature.AllNames())
			raise LoadError(error_message)
		elif feature_dictionary[cur_featurename] in unregulated_features:
			error_message = 'Regulated Feature is already Unregulated:\n' + ','.join(feature.AllNames())
			raise LoadError(error_message)
##			error_message = 'Regulated Feature Name: "' + cur_featurename + \
##							'" is not found on any of the chromosomes for which a sequence was loaded.\n'
##			raise LoadError(error_message)
		else:
			regulated_features.append(feature_dictionary[cur_featurename])
##--------------------------------------------------------------


#@+doc
# 
# 	regulated_features = [feature_dictionary[cur_featurename] for 
# cur_featurename in regulated_feature_names]
# 	excluded_features = [feature_dictionary[cur_featurename] for 
# cur_featurename in excluded_feature_names]
# 	unregulated_features = [feature_dictionary[cur_featurename] for 
# cur_featurename in unregulated_feature_names]

#@-doc
#@@code	
##	for featurename in regulated_features:
##		#feature = feature_dictionary.get(featurename, Null()).IsFeature()
##		#if not feature.IsFeature()
##		if not featurename in feature_dictionary:
##			raise TestControllerError('ERROR: FEATURE in regulated FEATURE file is unknown: ' + featurename + '\n')
##		elif not feature_dictionary[featurename].IsFeature :
##			raise TestControllerError('ERROR: feature in regulated FEATURE file is not an FEATURE: ' + featurename + '\n')
##	for featurename in excluded_features:
##		if not featurename in feature_dictionary:
##			raise TestControllerError('ERROR: FEATURE in regulated FEATURE file is unknown: ' + featurename + '\n')
##		elif not feature_dictionary[featurename].IsFeature :
##			raise TestControllerError('ERROR: feature in regulated FEATURE file is not an FEATURE: ' + featurename + '\n')
##	for featurename in unregulated_features:
##		if not featurename in feature_dictionary:
##			raise TestControllerError('ERROR: FEATURE in regulated FEATURE file is unknown: ' + featurename + '\n')
##		elif not feature_dictionary[featurename].IsFeature :
##			raise TestControllerError('ERROR: feature in regulated FEATURE file is not an FEATURE: ' + featurename + '\n')

	regulated_features         = dict(zip(regulated_features, tuple((None,))*len(regulated_features)))
	excluded_features          = dict(zip(excluded_features, tuple((None,))*len(excluded_features)))
	unregulated_features       = dict(zip(unregulated_features, tuple((None,))*len(unregulated_features)))
	# unknown_regulated_features = dict(zip(unknown_regulated_features, tuple((None,))*len(unknown_regulated_features)))
	
	return regulated_features, excluded_features, unregulated_features, unknown_regulated_features


def load_chromosome_sequences(chromosome_file_hash, chromosome_flags_hash,
							  chromosome_hash, feature_dictionary, ambiguous_feature_names):
	#*********************
	# Load ChromosomeSequences
	#*********************
##	print >>sys.stderr, 'Load Chromosome Sequence Files'
##	print >>sys.stderr, '=============================='
	## sys.stderr.write ('\r' + ' '*80 + '\rLoading Chromosome Sequence Files')
	if __debug__:
		print >>sys.stderr, 'Loading Chromosome Sequence Files'
		# print >>sys.stderr, '=============================='

	## sequence_parser = CerevisiaeSequenceParser()
	ParseSequence = ParseCerevisiaeSequence

	# sorted_chromosome_label_list = sort_numeric_string(chromosome_hash.keys())
	sorted_chromosome_label_list = sort_numeric_string(chromosome_file_hash.keys())
	for chromosome_label in sorted_chromosome_label_list:
		chromosome_sequence_filename = chromosome_file_hash[chromosome_label]
		chromosome_flags = chromosome_flags_hash.get(chromosome_label, '')
		## cur_sequence = sequence_parser.ParseFile(chromosome_sequence_filename,
		## 										 chromosome_label, chromosome_flags)
		cur_sequence = ParseSequence(chromosome_sequence_filename,chromosome_label, chromosome_flags)
	##	if chromosome_label <> cur_sequence.Name:
	##		raise  
		# chromosome_hash[chromosome_label].SetSequence(cur_sequence)
		chromosome_hash.setdefault(chromosome_label,Chromosome(chromosome_label)).SetSequence(cur_sequence)
	
		if __debug__:
			print >>sys.stderr, 'Length of Chromosome', chromosome_label, 'is', len(cur_sequence)


	for chromosome_label in chromosome_hash.keys():
		if not chromosome_hash[chromosome_label].Sequence:
			sys.stderr.write('\n' + '=  ' * 20 +
							 '\nNo sequence file provided for chromosome ' +
							 chromosome_label + '\n')
			sys.stderr.write('This chromosome will be removed from analysis' +
							 '\n' + '=  ' * 20 +'\n')
			chromosome = chromosome_hash[chromosome_label]
			for cur_orf in chromosome.OrfIterator():
				## print 'cur_orf.AllNames', cur_orf.AllNames()
				for cur_name in cur_orf.AllNames():
					cur_name = cur_name.upper()
					if cur_name not in ambiguous_feature_names:
						del(feature_dictionary[cur_name])
			del(chromosome_hash[chromosome_label])
			## features will still be in feature table
	## memcheck('Chromosome Sequence Files Loaded')


def load_probability_matrix(probability_file_name, base_frequency_hash):
	#*********************
	# Load ProbabilityFile
	#*********************
	## sys.stderr.write ('\r' + ' '*80 + '\rLoading Probability File')
	if __debug__:
		print >>sys.stderr, 'Loading Probability Matrix:', probability_file_name
		# print >>sys.stderr, '====================='

##	print >>sys.stderr, 'Load Probability File'
##	print >>sys.stderr, '====================='
##	probability_parser = FlatProbMatrixParser()
##	probability_parser.LoadFile(probability_file_name)
##	probability_matrix = probability_parser.Parse()

	probability_matrix = FlatProbMatrixParse(probability_file_name, base_frequency_hash)
	## print 'Using Binding Site Model(Descriptor):', probability_matrix.Descriptor

	##DEBUG probability_printer = flat_prob_matrix_printer_gomer.FlatProbMatrixPrinter()

	##DEBUG print 'Probabilities formatted for file'
	##DEBUG print probability_printer.Output(probability_matrix)
	##DEBUG print 'Probability Table'
	##DEBUG print probability_printer.OutputTable(probability_matrix, values='probs')
	##DEBUG print 'Bitvalue Table'
	##DEBUG print probability_printer.OutputTable(probability_matrix, values='bits')


	## memcheck('Probability File Loaded')
	return probability_matrix


def load_coordinate_feature_file(feature_file_list):
	## sys.stderr.write ('\r' + ' '*80 + '\rLoading Coordinate Feature File')

##	print >>sys.stderr, 'Load Coordinate Feature File'
##	print >>sys.stderr, '================='
	chromosome_hash = {}
	feature_dictionary = {}
	ambiguous_feature_names = {}
	for feature_file_name in feature_file_list:
		if __debug__:
			print >>sys.stderr, 'Loading Coordinate Feature File:', feature_file_name
			# print >>sys.stderr, '============================'

		chromosome_hash, feature_dictionary, ambiguous_feature_names, seq_info_hash = 	\
						 CoordinateFeatureParse(feature_file_name,
												chromosome_hash,
												feature_dictionary,
												ambiguous_feature_names)
	
	## print >>sys.stderr, chromosome_hash.keys()
##	sum = 0
##	for key in chromosome_hash.keys():
##		count = chromosome_hash[key].FeatureCount()
##		sum += count
##		print key, count
##	print 'Total:', sum
	return chromosome_hash, feature_dictionary, ambiguous_feature_names, seq_info_hash

def load_kappa_function(kappa_file_basename, kappa_search_paths, module_hash={}):
	for cur_path in kappa_search_paths:
		reg_region_kappa_file = os.path.join(cur_path,kappa_file_basename)
		if os.path.exists(reg_region_kappa_file):
			break
		elif os.path.exists(ExpandPath(reg_region_kappa_file)):
			reg_region_kappa_file = ExpandPath(reg_region_kappa_file)
			break
		else:
			reg_region_kappa_file = None
	if not reg_region_kappa_file:
		raise LoadError("Can't find module '" + kappa_file_basename + "' in kappa search path")

	if reg_region_kappa_file in module_hash:
		module = module_hash[reg_region_kappa_file]
	else:
		module = RuntimeImport(reg_region_kappa_file)
		module_hash[reg_region_kappa_file] = module
	return module_hash, module

def load_reg_region_kappas(param_module_pairs, kappa_search_paths):
	module_hash = {}
	reg_region_list = []
	for (file_basename, reg_region_kappa_params) in param_module_pairs:
		module_hash, module = load_kappa_function(file_basename, kappa_search_paths, module_hash)

		reg_region_kappa_param_hash = parse_parameter_string(reg_region_kappa_params)
		reg_region_list.append((reg_region_kappa_param_hash, module))
	return module_hash, reg_region_list



def parse_parameter_string(param_string,
						   parameter_separator=';',
						   assignment_separator='=', **kwargs):
	PARAM_SEP = parameter_separator
	ASSIGN_SEP = assignment_separator
	# PARAM_SEP = '::'
	if re.match('^\s*$', param_string):
		return kwargs
	else :
		param_pairs = param_string.split(PARAM_SEP)
		for pair in param_pairs:
			key, value = pair.split(ASSIGN_SEP)
			## kwargs[key.strip()]=eval(value.strip())
			kwargs[key.strip()]=eval(value.strip(), globals(), locals())
		return kwargs


class LoadError(GomerError):
	pass

