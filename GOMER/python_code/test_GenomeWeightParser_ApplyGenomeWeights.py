#! /usr/bin/env python

from genome_weight_parser import GenomeWeightParse, GenomeWeightInfo
from apply_genome_weights import ApplyGenomeWeights
import sys

def main():
    filename1 = sys.argv[1]
    genome_weight_hash1 = GenomeWeightParse(filename1)
    for key in genome_weight_hash1.keys():
        for i in range(len(genome_weight_hash1[key])):
            GenomeWeightInfo.display(genome_weight_hash1[key][i])

    ka_list=[0.3,0.4,0.5]
    matrix_length=30
    start=2
    default_weight=1.0
    chromId = 1

    weighted_ka_list = ApplyGenomeWeights(genome_weight_hash1,ka_list,start,chromId,matrix_length,default_weight)

    print weighted_ka_list

if __name__ == '__main__':
	main()    
