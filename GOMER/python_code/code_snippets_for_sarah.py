import sys
import os
from optparse import OptionParser

from gomer_load_functions import load_chromosome_table, load_feature_file, load_chromosome_sequences, load_probability_matrix

def main():	
	
	version_num = 0.001
	version = '%prog ' + str(version_num)
	usage = 'usage: %prog [options] ChromTable ProbMatrix FEATURE UPSTREAM_BP DOWNSTREAM_BP\n'
	
	parser = OptionParser(usage=usage, version=version)

	parser.add_option('-p', '--postscript',
					  action="store",
					  help='Save to postscript file.',
					  default='\t',
					  dest='separator')

	(options, args) = parser.parse_args()
	NORM_NUM_ARGS = 5



	if len(args) == (NORM_NUM_ARGS):
		chromosome_table_file_name = args[0]
		prob_matrix_filename = args[1]
		feature_name = args[2]
		upstream_bp = int(args[3])
		downstream_bp = int(args[4])

	else :
		parser.print_help()
		sys.exit(2)
	
	(feature_file_list,
	 chromosome_file_hash,
	 chromosome_flags_hash,
	 frequency_hash) = load_chromosome_table(chromosome_table_file_name)

	chromosome_hash, feature_dictionary, ambiguous_feature_names, feature_type_dict = load_feature_file(feature_file_list)




	## feature_dictionary has an entry for every non-ambiguous feature_name in the genome
	## REMEMBER THAT feature_dictionary KEYS MUST BE captialized!!!!
	## USE feature_name.upper()


	# gomer.py -aFILE/--ambiguous=FILE : Print ambiguous names from feature file FILE


	
	#is ambiguous_feature_names all caps?
	if feature_name.upper() in ambiguous_feature_names:
		print >>sys.stderr, 'Ambiguous Name:', feature_name
		print >>sys.stderr, '\nOther Names:\n' + \
		   '\n\n'.join([','.join(feature.AllNames()) for feature in ambiguous_feature_names[feature_name.upper()]])
		sys.exit(1)

	# raise NotImplementedError, 'Test to be sure the feature is in feature_dictionary'
	feature = feature_dictionary[feature_name.upper()]
	
	load_chromosome_sequences(chromosome_file_hash, chromosome_flags_hash,
							  chromosome_hash, feature_dictionary,
							  ambiguous_feature_names)

	if not frequency_hash:
		print 'NO BASE FREQUENCIES SUPPLIED - CALCULATING . . .'
		frequency_hash = base_count(chromosome_hash)
		print 'FREQUENCIES:', frequency_hash

	probability_matrix = load_probability_matrix(prob_matrix_filename, frequency_hash)

	the_chromosome_I_care_about = feature.Chromosome
	chromsome_sequence = the_chromosome_I_care_about.Sequence.Sequence # Don't ask

	begin_index = 1000; end_index = 3000
	sequence_to_score = chromsome_sequence[begin_index:end_index]
	print sequence_to_score
	# I'm sure you already know all this, but I'm going to be an annoying pain in the ass and say it again, and again, and again:
	# 1. be sure to check for off by one errors
	# 2. remember that string index space starts at ZERO and base pair numbering of sesquences start at ONE
	# 3. remember that the slice my_string[a:b] does NOT include the character at index 'b' - e.g.:
	# >>> range(20)[0:9]
	# [0, 1, 2, 3, 4, 5, 6, 7, 8]

	hitlist = probability_matrix.FilterScoreSequence(sequence_to_score, filter_cutoff_ratio=0)
	forward_scores = hitlist.Forward
	revcomp_scores = hitlist.ReverseComplement

	print forward_scores
	print revcomp_scores

	# 1. Grab section of sequence you care about from the genome (chromosome)
	# 2. Get the scores for that region.
	# 3. Do your stuff - Graph it!!!
##===============================================================================

if __name__ == '__main__':
	main()


"""	
Help on module genome_feature_gomer:

NAME
    genome_feature_gomer

FILE
    /home/josh/GOMER/python_code/genome_feature_gomer.py

CLASSES
    __builtin__.object
        GenomeFeature
    
    class GenomeFeature(__builtin__.object)
     |  Methods defined here:
     |  
     |  AllNames(self)
     |      A list of all of the names for this feature
     |  
     |  Properties defined here:
     |  Chromosome
     |  ChromosomeName
     |  CommonName
     |  IsOrf
     |  Length
     |  Name
     |  Start
     |  Stop
     |  Strand
     |  Types

Help on module chromosome_gomer:

NAME
    chromosome_gomer

FILE
    /home/josh/GOMER/python_code/chromosome_gomer.py

    class Chromosome(__builtin__.object)
     |  Name
     |  Sequence
"""	
