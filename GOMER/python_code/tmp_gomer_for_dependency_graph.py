#! /usr/bin/env python
#@+leo
#@comment Created by Leo at Mon Sep 29 18:28:11 2003
#@+node:0::@file gomer.py
#@+body
#@@first
#@@language python



"""
*Description:

This is the main controller for running a GOMER analysis.  
Its a bit hacky, but it is not necessarily.  I have tried to organize GOMER so
that it could be used through different interfaces: command line, curses-style,
GUI.  So this module is supposed to be temporary until things are working to
the point that a better interface can be designed
"""

#@<<imports>>
#@+node:1::<<imports>>
#@+body
from __future__ import division
from __future__ import generators

##import os
### os.environ['PYCHECKER'] = '--allglobals --stdlib --blacklist Numeric'
##os.environ['PYCHECKER'] = '--allglobals --stdlib --blacklist optik'
### os.environ['PYCHECKER'] = '--allglobals'
##import pychecker.checker

import check_python_version
check_python_version.CheckVersion()

# import profile
import os
import sys
# import getopt
import operator
import bisect
# import fpformat


import cerevisiae_feature_parser_gomer as feature_file_parser
## import flat_prob_matrix_printer_gomer
from merge_hashes import merge_hashes
from globals_gomer import ORGANISM_LIST, CACHE_TABLE_FILENAME, CONFIG_FILENAME, OUTPUT_NAME_JUST # , OUTPUT_RANK_JUST
from optparse import OptionParser

from score_windows import cache_score_all_windows
from open_file import OpenFile, TestFile, ExpandPath
from exceptions_gomer import FeatureTypeError
from runtime_import import RuntimeImport
from integer_string_compare import sort_numeric_string
from chromosome_table_parser import ChromosomeTableParse

from gomer_load_functions import parse_parameter_string
from gomer_load_functions import load_config_file
from gomer_load_functions import load_reg_region_kappas
from gomer_load_functions import load_chromosome_table
from gomer_load_functions import load_coordinate_feature_file
from gomer_load_functions import load_chromosome_sequences
from gomer_load_functions import load_probability_matrix
from gomer_load_functions import load_regulated_feature_names
from gomer_load_functions import load_feature_file
from gomer_load_functions import load_kappa_function


version_2_3_hex = 0x20300f0

##if __name__ == '__main__':
##	print >>sys.stderr, '-'*70
##	try:
##		import psyco
##	except ImportError:
##		print >>sys.stderr, 'Module "psyco" not available'
##	else:
##		print >>sys.stderr, 'USING "psyco"!!'
##		## psyco.log()
##		psyco.profile()
##		## psyco.profile(0.2)




#@+doc
# 
# choose between below for regular or slice scoring

#@-doc
#@@code



##        @doc
##which_one = 'index'

#@+doc
# 
# which_one = 'slice'
# if which_one == 'index':
# 	from score_orf_gomer import RegulatoryRegionScoreOrf
# elif which_one == 'slice':
# 	from score_orf_gomer import RegulatoryRegionScoreOrfSlice as RegulatoryRegionScoreOrf
# else:
# 	raise StandardError

#@-doc
#@@code

# from score_orf_gomer import RegulatoryRegionScoreOrfSlice as RegulatoryRegionScoreOrf
# from score_orf_gomer import RegulatoryRegionScoreOrfSlice
from score_orf_gomer import score_features

## print '%' * 50, '\nUSING:', RegulatoryRegionScoreOrf.__name__, '\n', '%' * 50


##from regulatory_region_model_gomer import SingleSquareRegulatoryRegionModel

#@+doc
# 
# from single_square_reg_region_model_gomer import SingleSquareRegulatoryRegionModel

#@-doc
#@@code

##        @doc

#@@code

from exceptions_gomer import GomerError
## import ROC_AUC
## import MNCP
# from estimate_p_value import MNCP, ROCAUC, ROCAUC_pvalue, MNCP_pvalue, EstimateMNCPProb, MannWhitney
from estimate_p_value import MNCP, ROCAUC, EstimateMNCPProb, MannWhitney



## from coop_score_feature_gomer import CooperativeScoreFeature
from coop_score_feature_gomer import coop_score_features
## from coop_score_feature_gomer_speedup import CooperativeScoreFeature

from fullmodel_score_feature_gomer import fullmodel_score_features

from coordinate_feature_gomer import COORDINATE_TYPE

from sequence_feature_gomer import SEQUENCE_TYPE
from multi_fasta_feature_parser_gomer import ParseFASTASequenceFeatureFile

## os.environ['PYCHECKER'] = '--allglobals'
## import pychecker.checker

#@-body
#@-node:1::<<imports>>


def CounterGenerator(start=0):
    count = start
    while(1):
            yield str(count)
            count += 1

MatrixSerialNumber = CounterGenerator().next



#@<<TestControllerError>>
#@+node:26::<<TestControllerError>>
#@+body
class TestControllerError(GomerError):
	pass

#@+doc
# 
# 	def __init__(self, file_name, line, line_number, message):
# 		# self.line, self.line_number, self.message = tuple
# 		# super(ParseError, self).__init__()
# 		self.file_name = file_name
# 		self.line = line
# 		self.line_number = line_number
# 		self.message = message
# 
# 	def print_message (self):
# 		print '-' * 80
# 		print self.message, '\nError encountered in following line (#' \
# 			  + str(self.line_number) + ')', 'from input file:\n', \
# 			  self.file_name, '\n', self.line
# 		print '-' * 80

#@-doc
#@@code

##	def print_message (self):
##		print >>sys.stderr, '-' * 80
##		print >>sys.stderr, self.message, '\n'
##		print >>sys.stderr, '-' * 80



#@-body
#@-node:26::<<TestControllerError>>



#@<<def get_config_parameter>>
#@+node:5::<<def get_config_parameter>>
#@+body
def get_config_parameter(config, param_name, param_section=''):
	if param_section:
		return config.get(param_section, param_name)
	else:
		sections_with_param = []
		for section in config.sections():
			if param_name in section:
				sections_with_param.append(section)
		if len(sections_with_param) == 1:
			param_section = sections_with_param[0]
			return config.get(param_section, param_name)
		elif len(sections_with_param) < 1:
			raise config.NoOptionError 
		elif len(sections_with_param) > 1:
			raise config.DuplicateSectionError
		return None
#@-body
#@-node:5::<<def get_config_parameter>>


#@<<def get_stats>>
#@+node:22::<<def get_stats>>
#@+body
def pvalue_string(num_positive, num_simulations):
	if num_positive:
		result = ' '.join(('p ~=', str(num_positive/num_simulations)))
	else:
		result = ' '.join(('p <<', str(100/num_simulations)))
	result += ' '.join((' (', str(num_positive), 'of', str(num_simulations), ')'))
	return result

##def get_stats (list_of_score_tuples,
##			   regulated_features, excluded_features, unregulated_features,
##			   num_simulations=None):
def get_stats (list_of_score_tuples,
			   regulated_features, excluded_features, unregulated_features):
	regulated_scores = []
	unregulated_scores = []
	excluded_scores = []
	for score, feature in list_of_score_tuples:
		if feature in excluded_features:
			excluded_scores.append(score)
		elif feature in unregulated_features:
			## needed to maintain order of precedence for status
			unregulated_scores.append(score)
		elif feature in regulated_features:
			regulated_scores.append(score)
		else:
			## needed to catch features without an explicitly declared status
			unregulated_scores.append(score)

	if len(regulated_scores) == 0 or len(unregulated_scores) == 0 :
		return None
	
##	if num_simulations:
##		rocauc, rocauc_ge_count = ROCAUC_pvalue(regulated_scores,
##												 unregulated_scores,
##												 num_simulations,
##												 higher_is_better=1)
##		rocauc_pval = rocauc_ge_count/num_simulations
##		rocauc_pval_ratio = str(rocauc_ge_count)+'/'+str(num_simulations)
##		mncp, mncp_ge_count = MNCP_pvalue(regulated_scores,
##												 unregulated_scores,
##												 num_simulations,
##												 higher_is_better=1)
##		mncp_pval = mncp_ge_count/num_simulations
##		mncp_pval_ratio = str(mncp_ge_count)+'/'+str(num_simulations)
##	else:
##		# rocauc = ROCAUC(regulated_scores, unregulated_scores, higher_is_better=1)
##		# mncp = MNCP(regulated_scores, unregulated_scores, higher_is_better=True)
##		# rocauc_pval = rocauc_pval_ratio = ''
##		# mncp_pval = mncp_pval_ratio = ''
	rocauc = ROCAUC(regulated_scores, unregulated_scores, higher_is_better=True)
	U_score, z_score = MannWhitney(regulated_scores, unregulated_scores, higher_is_better=True)
	estimated_mncp_prob, mncp = EstimateMNCPProb(regulated_scores,
												 unregulated_scores,
												 higher_is_better=True)
		
	# return rocauc, rocauc_pval, rocauc_pval_ratio, mncp, mncp_pval, mncp_pval_ratio
	return rocauc, U_score, z_score, mncp, estimated_mncp_prob

#@-body
#@-node:22::<<def get_stats>>


#@<<def output_best_and_worst>>
#@+node:19::<<def output_best_and_worst>>
#@+body
def output_best_and_worst(list_of_score_tuples, num_best=10, num_worst=10):

	list_of_score_tuples.sort()
	result_string = ''

	result_string += '\tworst:\n'
	for index in range(num_worst):
		score = list_of_score_tuples[index][0]
		feature = list_of_score_tuples[index][1]
		if feature.CommonName:
			feature_name = feature.CommonName
		else:
			feature_name = feature.Name
		result_string += '\t\t' + str(index) + '\t' + feature_name + '=' + str(score) + '\n'

	last_index = len(list_of_score_tuples) - 1
	stop = last_index - num_best
	result_string += '\tbest:\n'
	for index in range(last_index, stop, -1):
		score = list_of_score_tuples[index][0]
		feature = list_of_score_tuples[index][1]
		if feature.CommonName:
			feature_name = feature.CommonName
		else:
			feature_name = feature.Name
		result_string += '\t\t' + str(index) + '\t' + feature_name + '=' + str(score) + '\n'
	return result_string

#@-body
#@-node:19::<<def output_best_and_worst>>


#@<<def output_ranks>>
#@+node:20::<<def output_ranks>>
#@+body
def sorted_score_pairs(score_feature_list):
	score_name_feature_list = [(score, (feature.CommonName or feature.Name), feature) for score,feature in score_feature_list]
	score_name_feature_list.sort()
	return score_name_feature_list

def add_rank(tuple_list, descending=None):
	tuple_list.sort()
	if descending:
		tuple_list.reverse()
	tuple_rank_list = [cur_tup + (str(rank),) for cur_tup, rank
							in zip(tuple_list,
								   range(1,len(tuple_list)+1))]
	return tuple_rank_list


def add_rank_ties_bottom(tuple_list):
	tuple_list.sort()
	tuple_rank_list = []
	rank = count = len(tuple_list)
	cur_score = 1-sys.maxint
	for score, name, feature in tuple_list:
		if score <> cur_score:
			rank = count
			cur_score = score
		tuple_rank_list.append((score, name, feature, rank))
		count -= 1
	return tuple_rank_list

def output_ranks(score_feature_list, regulated_features,
				 unknown_regulated_features,
				 excluded_features,
				 num_best=0, num_worst=0,
				 sort_ascending=0
				 # print_all=False # , feature_only=False
				 ):

	##score_orf_list.sort()
	## score_orf_list = sort_score_pairs(score_orf_list)
	## score_name_orf_list = [(score, orf.CommonName, orf) for score,orf in score_orf_list]
	## score_name_orf_list = [(score, (orf.CommonName or orf.Name), orf) for score,orf in score_orf_list]
	## score_name_orf_list.sort()
	# score_name_orf_list = sorted_score_pairs(score_orf_list)
	saturated = [feature for score, feature in score_feature_list if (score == 1)]

	score_name_feature_list = [(score, (feature.CommonName or feature.Name), feature) for score,feature in score_feature_list]
	rank_style = 'bottom'
	if rank_style == 'regular':
		score_name_feature_rank_list = add_rank(score_name_feature_list)
	elif rank_style == 'descending':
		score_name_feature_rank_list = add_rank(score_name_feature_list, descending=1)
		score_name_feature_rank_list.reverse()
	elif rank_style == 'bottom':
		score_name_feature_rank_list = add_rank_ties_bottom(score_name_feature_list)
	else:
		raise StandardError, 'Unknown rank_style'
	del score_name_feature_list

	## name_just = 20
	## rank_just = 10
	# result_string = ''
	result_string_list = []
	sep_line = '---=-'*15
	# sep_line = ''

	score,name,feature,rank = score_name_feature_rank_list[0]
	OUTPUT_RANK_JUST = len(str(rank))

##	if print_all and regulated_features:
##		result_string_list.append(sep_line)
##		result_string_list.append('Regulated:')
##		for score,name,feature,rank in score_name_feature_rank_list:
##			if feature in regulated_features:
##				result_string_list.append('\t'.join((str(rank).rjust(OUTPUT_RANK_JUST), name.ljust(OUTPUT_NAME_JUST), repr(score))))

##	if num_worst > 0 :
##		result_string_list.append(sep_line)
##		result_string_list.append('Worst:')
##		for index in range(num_worst):
##			score,name,feature,rank = score_name_feature_rank_list[index]
##			result_string_list.append('\t'.join((str(rank).rjust(OUTPUT_RANK_JUST), name.ljust(OUTPUT_NAME_JUST), repr(score))))
	
##	last_index = len(score_feature_list) - 1
##	stop = last_index - num_best
##	if num_best > 0 :
##		result_string_list.append('Best:')
##		for index in range(last_index, stop, -1):
##			score,name,feature,rank = score_name_feature_rank_list[index]
##			result_string_list.append('\t'.join((str(rank).rjust(OUTPUT_RANK_JUST), name.ljust(OUTPUT_NAME_JUST), repr(score))))

##	if print_all and excluded_features:
##		result_string_list.append(sep_line)
##		result_string_list.append('Excluded:')
##		for score,name,feature,rank in score_name_feature_rank_list:
##			if feature in excluded_features:
##				result_string_list.append('\t'.join((str(rank).rjust(OUTPUT_RANK_JUST), name.ljust(OUTPUT_NAME_JUST), repr(score))))

##	if print_all and unknown_regulated_features:
##		result_string_list.append(sep_line)
##		result_string_list.append('Unknown Features:')
##		for unknown_name in unknown_regulated_features:
##			result_string_list.append(unknown_name)
	all_string_list = []
	regulated_string_list = []
	excluded_string_list = []
	best_string_list =[]
	worst_string_list =[]
	if num_best > 0:
		best_string_list = ['\t'.join((str(rank).rjust(OUTPUT_RANK_JUST),
									   name.ljust(OUTPUT_NAME_JUST),
									   repr(score)))
							for score,name,feature,rank in
							score_name_feature_rank_list[-num_best:]]
	if num_worst > 0 :
		worst_string_list = ['\t'.join((str(rank).rjust(OUTPUT_RANK_JUST),
										name.ljust(OUTPUT_NAME_JUST),
										repr(score)))
							 for score,name,feature,rank in
							 score_name_feature_rank_list[:num_worst]]
	## if print_all:
##	result_string_list.append(sep_line)
##	result_string_list.append('All:')
	for score,name,feature,rank in score_name_feature_rank_list:
		cur_feature_string = '\t'.join((str(rank).rjust(OUTPUT_RANK_JUST), name.ljust(OUTPUT_NAME_JUST), repr(score)))
		all_string_list.append(cur_feature_string)
		if feature in regulated_features:
			regulated_string_list.append(cur_feature_string)
		elif feature in excluded_features:
			excluded_string_list.append(cur_feature_string)
	##------------------------------------------------------
	## REVERSE
	##------------------------------------------------------
	if sort_ascending:
		all_string_list.reverse()
		regulated_string_list.reverse()
		excluded_string_list.reverse()
		best_string_list.reverse()
		worst_string_list.reverse()
	##------------------------------------------------------
	if saturated:
		result_string_list.append('NUMBER SATURATED: ' + str(len(saturated)))
	if regulated_string_list:
		result_string_list += [sep_line, 'Regulated:'] + regulated_string_list
	# if num_best > 0:
	if best_string_list:
		result_string_list += [sep_line, 'Best:'] + best_string_list
	# if num_worst > 0 :
	if worst_string_list:
		result_string_list += [sep_line, 'Worst:'] + worst_string_list
	if excluded_string_list:
		result_string_list += [sep_line, 'Excluded:'] + excluded_string_list
	if unknown_regulated_features:
		result_string_list += [sep_line, 'Unknown Features:'] + unknown_regulated_features
	result_string_list += [sep_line, 'All:'] + all_string_list
	result_string_list.append('') # add a trailing newline

	return '\n'.join(result_string_list)

#@-body
#@-node:20::<<def output_ranks>>



#@<<def print_windows>>
#@+node:18::<<def print_windows>>
#@+body
##def print_windows(cur_orf, binding_site_model, range_pair):
##	regulatory_five_prime, regulatory_three_prime = range_pair
##	regulatory_region_model = SingleSquareRegulatoryRegionModel(binding_site_model,
##																regulatory_five_prime,
##																regulatory_three_prime)

##	hit_list = cur_orf.Chromosome.GetHitList(binding_site_model.Descriptor)


##	regulatory_region_ranges = regulatory_region_model.GetRegulatoryRegionRanges(cur_orf)

##	for cur_range in regulatory_region_ranges:
##		(start_hitlist_index, stop_hitlist_index) = cur_range
##		max_window = max(hit_list.Forward[start_hitlist_index:stop_hitlist_index])
##		min_window = min(hit_list.Forward[start_hitlist_index:stop_hitlist_index])
##		print 'MAX:', max_window, 'MIN:', min_window


#@-body
#@-node:18::<<def print_windows>>


#@<<def test_orf_iteration>>
#@+node:23::<<def test_orf_iteration>>
#@+body
def test_orf_iteration(chromosome_hash):
	for chromosome_label in chromosome_hash:
		chromosome = chromosome_hash[chromosome_label]
		print 'Chromosome', chromosome_label, ':'
		for cur_orf in chromosome.OrfIterator():
			if cur_orf.IsOrf:
				print cur_orf.Name

#@-body
#@-node:23::<<def test_orf_iteration>>


#@<<def base_count>>
#@+node:24::<<def base_count>>
#@+body
def base_count(chromosome_hash):
	genome_count_hash = {'A':0, 'C':0, 'G':0, 'T':0,
						 'a':0, 'c':0, 'g':0, 't':0}
	for chrom_name in chromosome_hash:
		cur_sequence = chromosome_hash[chrom_name].Sequence.Sequence
		for base in cur_sequence:
			genome_count_hash[base] += 1
##-----------------------------------------------------
	for base in genome_count_hash.keys():
		if base == base.upper():
			genome_count_hash[base] += genome_count_hash[base.lower()]
			del(genome_count_hash[base.lower()])
##-----------------------------------------------------
	total_counts = reduce(operator.add, [genome_count_hash[base] for base in genome_count_hash])
##	AT_freq = (genome_count_hash['A'] + genome_count_hash['T'])/total_counts
##	GC_freq = (genome_count_hash['G'] + genome_count_hash['C'])/total_counts
	A_freq = ((genome_count_hash['A'] + genome_count_hash['T'])/total_counts)/2
	T_freq = ((genome_count_hash['A'] + genome_count_hash['T'])/total_counts)/2
	G_freq = ((genome_count_hash['G'] + genome_count_hash['C'])/total_counts)/2
	C_freq = ((genome_count_hash['G'] + genome_count_hash['C'])/total_counts)/2

	return {'A':A_freq, 'C':C_freq, 'G':G_freq, 'T':T_freq}

#@-body
#@-node:24::<<def base_count>>


##<#<def old_test_array_index>>



#@<<def output_ambiguous>>
#@+node:27::<<def output_ambiguous>>
#@+body
def output_ambiguous(file_name_list):
	print file_name_list
	chromosome_hash, feature_dictionary, ambiguous_feature_names, feature_type_dict = \
					 load_feature_file(file_name_list)
	first = 12
	second = 8
	third = 10
	print '-' *20, '\nAMBIGUOUS NAMES:\n', '-' *20
	print '\n' + 'Ambiguous'.ljust(first) + 'Chrom-'.center(second), 'Start'.center(third), 'Alternate Names'
	print 'Name'.ljust(first) + 'osome'.center(second), 'bp'.center(third)
	print '-' * 60
	for ambig_name in ambiguous_feature_names:
		name_to_print = ambig_name
		for feature in ambiguous_feature_names[ambig_name]:
			print name_to_print.ljust(first), feature.ChromosomeName.center(second), \
				  `feature.Start`.ljust(third), ', '.join(feature.AllNames())
			name_to_print = ''
		print ''

#@-body
#@-node:27::<<def output_ambiguous>>


#@<<def find_top_hits>>
#@+node:30::<<def find_top_hits>>
#@+body
def find_top_hits(num_hits, chromosome_hash, prob_matrix):
	top_list = [(1-sys.maxint, None, None, None)] * num_hits
	for chrom_label in chromosome_hash:
		cur_chrom = chromosome_hash[chrom_label]
		hit_list = cur_chrom.GetHitList(prob_matrix.Descriptor)

##       @doc
##------------------------------------------------------------------
		strand = 'W'
		forward_list = hit_list.Forward[:]
		for index in range(len(forward_list)):
			hit_score = forward_list[index]
			if hit_score > top_list[0][0]:
				new_entry = (hit_score, chrom_label, index, strand)
				bisect.insort(top_list, new_entry)
				top_list.pop(0)
		del(forward_list)

		strand = 'C'
		revcomp_list = hit_list.ReverseComplement[:]
		for index in range(len(revcomp_list)):
			hit_score = revcomp_list[index]
			if hit_score > top_list[0][0]:
				new_entry = (hit_score, chrom_label, index, strand)
				bisect.insort(top_list, new_entry)
				top_list.pop(0)
		del(revcomp_list)

##------------------------------------------------------------------

#@@code


#@+doc
# 
# ##------------------------------------------------------------------
# 		strand = 'f'
# 		for index in range(len(hit_list.Forward)):
# 			hit_score = hit_list.Forward[index]
# 			if hit_score > top_list[0][0]:
# 				new_entry = (hit_score, chrom_label, index, strand)
# 				bisect.insort(top_list, new_entry)
# 				top_list.pop(0)
# 
# 		strand = 'r'
# 		for index in range(len(hit_list.ReverseComplement)):
# 			hit_score = hit_list.ReverseComplement[index]
# 			if hit_score > top_list[0][0]:
# 				new_entry = (hit_score, chrom_label, index, strand)
# 				bisect.insort(top_list, new_entry)
# 				top_list.pop(0)
# ##------------------------------------------------------------------

#@-doc
#@@code
	top_list.reverse()
	print 'TOP LIST:'
	print 'score'.center(15) + 'Chrom'.center(7) + 'Left'.center(9) + 'Right'.center(9) + 'Strand'.center(7) + 'Nearest Feature'.center(20)
	print '-' * 60
	for hit in top_list:
		score, chrom, left_index, strand = hit
		left_bp = left_index + 1
		right_bp = left_bp + (binding_site_model.Length - 1)
		nearest_features = chromosome_hash[chrom].GetNearest(left_bp, right_bp)
		output_string = str(round(score,3)).rjust(15) + chrom.center(7) + `left_bp`.rjust(9) + `right_bp`.rjust(9) + strand.center(7) + nearest_features[0].Name.center(20) + `nearest_features[0].Start`.rjust(9) + `nearest_features[0].Stop`.rjust(9)
		print output_string

		# if more than one feature is nearest (they are equidistant?) print the information for the remaining Nearest Features
## 		if len(nearest_features)>1:
## 			print "len(nearest_features)", len(nearest_features)
## 			print nearest_features
		for feature in nearest_features[1:]:
			# print ' ' * 46 + feature.Name.center(12) + `feature[0].Start`.rjust(9) + `feature[0].Stop`.rjust(9)
			print ' ' * 47 + feature.Name.center(20) + `feature.Start`.rjust(9) + `feature.Stop`.rjust(9)
	print '\n\n\n'	
	##'\n'.join([str(hit) for hit in top_list])

#@@code



#@+doc
# 
# >>> import random
# >>> import bisect
# >>> a = [10] * 20
# >>> for c in range(500):
# 	new_val = random.randrange(1000)
# 	bisect.insort(a, new_val)
# 	print a.pop(0), '\t', a

#@-doc
#@@code
#@-body
#@-node:30::<<def find_top_hits>>


#@<<def output_feature_types>>
#@+node:28::<<def output_feature_types>>
#@+body
def output_feature_types(feature_file_name):
##	if organism.lower().strip() == 'help':
##		print '-' * 60, '\n', '-' * 60
##		print 'Known Organisms:\n\t', '\n\t'.join(ORGANISM_LIST) 
##		print '-' * 60, '\n', '-' * 60, '\n'
##		sys.exit(0)
##	feature_parser_hash = {'Saccharomyces cerevisiae' : cerevisiae_feature_parser_gomer}
##	matching_organisms = []
##	for cur_organism in ORGANISM_LIST:
##		if re.search(organism.strip(), cur_organism, re.IGNORECASE):
##			matching_organisms.append(cur_organism)
	
##	if len(matching_organisms) == 0 :
##		print 'ORGANISM_LIST', ORGANISM_LIST
##		raise TestControllerError, 'ERROR: "' + organism + \
##			  '" does not match any known organisms:\n' + '\n'.join(ORGANISM_LIST) 
##	elif len(matching_organisms) > 1:
##		raise TestControllerError, 'ERROR: supplied organism is ambiguous. "' + \
##			  organism + '" matches:\n' + '\n'.join(matching_organisms)
##	else:
##		organism = matching_organisms[0]

	# feature_file_parser = cerevisiae_feature_parser_gomer

	
	feature_types, multiple_types, feature_types_string, multiple_types_string =\
				   feature_file_parser.GetFeatureTypes(feature_file_name)
	print '-' * 50
	print 'SINGLE TYPE COUNTS'
	print '(features with more than one type are counted once for each type)'
	print '------------------'
	print feature_types_string
	print '-' * 50, '\n\n', '-' * 50
	print 'MULTIPLE TYPE COUNTS'
	print '(A feature can have more than one type. These are counts of the types for each'
	print ' feature that has more than one type.  All of these features are counted in the single types count)'
	print '--------------------'
	print multiple_types_string
	print '-' * 50, '\n\n'

	print 'Default Features:', feature_file_parser.DEFAULT_FEATURES
	


#@-body
#@-node:28::<<def output_feature_types>>



#@<<def print_kappa_help>>
#@+node:29::<<def print_kappa_help>>
#@+body
def print_kappa_help(kappa_file):
##	if not os.path.exists(kappa_file):
##		kappa_file = os.path.join(gomer_home_directory, kappa_file)

	module = RuntimeImport(kappa_file)
	print >> sys.stderr, '-' * 60 + '\n'
	print >> sys.stderr, 'Parameters:\n\n'
	print >> sys.stderr, 'Name'.ljust(30) + 'Type'.ljust(30)
	print >> sys.stderr, '--------------'.ljust(30) + '--------------'.ljust(30)
	for parameter in module.PARAMETERS:
		print >> sys.stderr, parameter.ljust(30) + str(module.PARAMETERS[parameter]).ljust(30)
	print >> sys.stderr, '-' * 60 + '\n'
	print >> sys.stderr, '\nDescription:\n\n', module.DESCRIPTION	
	print >> sys.stderr, '-' * 60 + '\n'

#@-body
#@-node:29::<<def print_kappa_help>>







#@<<def process_coop_kappa_params>>
#@+node:32::<<def process_coop_kappa_params>>
#@+body
#@+doc
# 
# NOT IN USE AT THE MOMENT, BUT DON'T THROW AWAY YET, MIGHT BE USEFUL LATER.
# 
# def process_coop_kappa_params(options, primary_matrix, frequency_hash,
# 							  scored_matrices, chromosome_hash,
# 							  filter_cutoff_ratio, cache_directory,
# 							  cache_table_filename, gomer_home_directory,
# 							  module_hash):
# #def process_coop_kappa_params(options, primary_matrix, frequency_hash, 
# chromosome_hash, filter_cutoff_ratio, cache_directory, cache_table_filename):
# 	probability_matrix_hash = {}
# 	probability_matrix_hash[primary_matrix.Descriptor] = primary_matrix
# 	free_conc_hash = {}
# 	k_dimer_hash = {primary_matrix.Descriptor:{}}
# 	coop_model_hash = {primary_matrix.Descriptor:{}}
# 	second_tf_name_list = []
# 	print 'COOP KAPPA ARGUMENTS'
# 	for kappa_file, param_string, prob_matrix, free_conc_ratio, k_dimer_ratio 
# in options.coop_kappa:
# 		print 'kappa_file:', kappa_file
# 		print 'param_string:', param_string
# 		print 'prob_matrix:', prob_matrix
# 		print 'free_conc_ratio:', free_conc_ratio
# 		print 'k_dimer_ratio:', k_dimer_ratio
# 		##--------------------------------------------------
# 		cur_matrix = load_probability_matrix(prob_matrix, frequency_hash)
# 		if cur_matrix.Descriptor not in scored_matrices:
# 			matrix_descriptor = cache_score_all_windows(cur_matrix, chromosome_hash, filter_cutoff_ratio,
# 														cache_directory, cache_table_filename)
# 			scored_matrices[matrix_descriptor] = 1
# 		probability_matrix_hash[cur_matrix.Descriptor] = cur_matrix
# 		print 'cur_matrix.Descriptor', cur_matrix.Descriptor
# 		##--------------------------------------------------
# 		second_tf_name_list.append(cur_matrix.Descriptor)
# 		##--------------------------------------------------
# 		(max_Ka, max_Ka_values, max_Ka_sequence,
# 		 min_Ka, min_Ka_values, min_Ka_sequence) = cur_matrix.GetKaRange()
# 		if free_conc_ratio.lower() == 'default' or float(free_conc_ratio) == 1:
# 			free_conc = 1/max_Ka
# 		else:
# 			free_conc = float(free_conc_ratio) * 1/max_Ka
# 		print 'free_conc', free_conc, '(ratio:', free_conc/(1/max_Ka), ')'
# 		free_conc_hash[cur_matrix.Descriptor] = free_conc
# 		##--------------------------------------------------
# 		if k_dimer_ratio.lower() == 'default' or float(k_dimer_ratio) == 1:
# 			k_dimer = max_Ka
# 		else:
# 			k_dimer = float(k_dimer_ratio) * max_Ka
# 		print 'k_dimer', k_dimer, '(ratio:', k_dimer/(max_Ka), ')'
# 		k_dimer_hash[primary_matrix.Descriptor][cur_matrix.Descriptor] = float(k_dimer)
# 		print 'k_dimer_hash[primary_matrix.Descriptor][cur_matrix.Descriptor]', k_dimer_hash[primary_matrix.Descriptor][cur_matrix.Descriptor]
# 		##--------------------------------------------------
# 		if os.path.exists(kappa_file):
# 			kappa_file = kappa_file
# 		else:
# 			kappa_file = os.path.join(gomer_home_directory, kappa_file)
# 
# 		if kappa_file in module_hash:
# 			module = module_hash[kappa_file]
# 		else:
# 			module = RuntimeImport(kappa_file)
# 			module_hash[kappa_file] = module
# 			param_hash = parse_parameter_string(param_string)
# 		coop_model = module.CoopModel(primary_matrix, cur_matrix, **param_hash)
# 		coop_model_hash[primary_matrix.Descriptor][cur_matrix.Descriptor] = coop_model
# 	return coop_model_hash, free_conc_hash, second_tf_name_list, k_dimer_hash

#@-doc
#@-body
#@-node:32::<<def process_coop_kappa_params>>



#@<<def sequence_features_main>>
#@+node:33::<<def sequence_features_main>>
#@+body
#@+doc
# 
# NOT IN USE AT THE MOMENT, BUT DON'T THROW AWAY YET, MIGHT BE USEFUL LATER.
# 
# def sequence_features_main(options, config, gomer_home_directory) :
# 	module_hash, reg_region_list = load_reg_region_kappas(options, config, gomer_home_directory)
# 
# 	filter_cutoff_ratio = options.filter_cutoff_ratio
# 	if filter_cutoff_ratio < 1:
# 		filter_cutoff_ratio = 0
# 	print 'Filter Cutoff Ratio:', filter_cutoff_ratio
# ##---------------------------------------------------------------------
# ##---------------------------------------------------------------------
# 	feature_dictionary = {}
# 	feature_list = []
# 	for sequence_file_name in options.sequence_feature:
# 		(feature_dictionary,
# 		 feature_list,
# 		 frequency_hash) = ParseFASTASequenceFeatureFile(sequence_file_name,
# 														 feature_dictionary,
# 														 feature_list)
# 
# 	if cur_module.TYPE <> 'SequenceFeatureRegulatoryRegionModel':
# 		raise StandardError, "It is only appropriate to use 
# SequenceFeatureRegulatoryRegionModels for 'SequenceFeature' features"
# ##---------------------------------------------------------------------
# ##---------------------------------------------------------------------
# 	primary_matrix = load_probability_matrix(probability_file_name, frequency_hash)
# ##-------------------------------------------------------------------------------
# 	regulated_orfs, excluded_orfs, unregulated_orfs = \
# 					load_regulated_orf_names(regulated_orf_filename,
# 											 feature_dictionary,
# 											 ambiguous_feature_names)
# 	scored_matrices = {}
# 	matrix_descriptor = cache_score_all_windows(primary_matrix, 
# chromosome_hash, filter_cutoff_ratio,
# 												cache_directory, cache_table_filename)
# 	scored_matrices[matrix_descriptor] = 1
# ##-------------------------------------------------------------------------------
# 	if options.coop_kappa:
# 		raise StandardError, "Sequence Features doesn't support coop yet"
# 		(coop_model_hash,
# 		 free_conc_hash,
# 		 second_tf_name_list,
# 		 k_dimer_hash) = process_coop_kappa_params(options, primary_matrix,
# 												   frequency_hash,
# 												   scored_matrices,
# 												   chromosome_hash,
# 												   filter_cutoff_ratio,
# 												   cache_directory,
# 												   cache_table_filename,
# 													gomer_home_directory,
# 												   module_hash)
# 	(max_Ka, max_Ka_values, max_Ka_sequence,
# 	 min_Ka, min_Ka_values, min_Ka_sequence) = primary_matrix.GetKaRange()
# 	# max_Ka, max_Ka_values, max_Ka_sequence = probability_matrix.GetBestKa()
# 	print 'Max Ka:', max_Ka, '\nMax Ka Values:', max_Ka_values, '\nMax Ka 
# Seqeunce:', max_Ka_sequence
# 	print 'Min Ka:', min_Ka, '\nMin Ka Values:', min_Ka_values, '\nMin Ka 
# Seqeunce:', min_Ka_sequence
# 	print 'MaxKa/MinKa:', max_Ka/min_Ka
# 	if options.free_conc_ratio:
# 		free_conc_list = [ratio*(1/max_Ka) for ratio in options.free_conc_ratio]
# 	else:
# 		free_conc = 1/max_Ka
# 		free_conc_list = tuple((free_conc,))
# 	print 'Free Concentration Ratios:', options.free_conc_ratio
# 	print 'Free Concentration List:', free_conc_list
# ##===============================================================================
# 	result_table = ''
# 	result_table += '1/max_Ka:\t' + str(1/max_Ka) + '\n'
# 	result_table +=  'pseudoRT: ' + str(primary_matrix.GetPseudoRT()) + '\n'
# 	result_table +=  'pseudoTemperature: ' + 
# str(primary_matrix.GetPseudoTemp()) + '\n'
# 	for free_conc in free_conc_list:
# 		print >> sys.stderr, 'Free Concentration:', free_conc, '(ratio:', 
# free_conc/(1/max_Ka), ')'
# 		result_table += ' '.join(('[', str(free_conc), ']')).ljust(22) + 
# 'Normalized ROC-AUC'.ljust(20) + 'MNCP'.ljust(20) + '\n'

#@-doc
#@@code
#@+doc
# 
# ##===============================================================================
# 		for param_hash, cur_module in reg_region_list:
# 			regulatory_string = cur_module.NAME + str(param_hash)
# 			regulatory_region_model = \
# 									cur_module.RegulatoryRegionModel(primary_matrix,
# 																	 **param_hash)
# ##===============================================================================
# 			if options.coop_kappa:
# 				free_conc_hash[primary_matrix.Descriptor] = free_conc
# 				feature_score_list = coop_score_ORFs(chromosome_hash,
# 												 regulatory_region_model,
# 												 primary_matrix.Descriptor,
# 												 second_tf_name_list,
# 												 free_conc_hash,
# 												 k_dimer_hash,
# 												 coop_model_hash)
# 			else:
# 				feature_score_list = score_sequence_features(feature_types,
# 													chromosome_hash,
# 													primary_matrix,
# 													free_conc,
# 													regulatory_region_model)
# 
# 			regulated_ranks = output_regulated_ranks(feature_score_list, regulated_orfs,
# 													 num_best=1,
# 													 num_worst=1,
# 													 ## print_all=1)
# 													 print_all=1,
# 													 orf_only=0)
# 			norm_roc_auc, mncp = get_stats(feature_score_list, regulated_orfs,
# 										   excluded_orfs, unregulated_orfs)
# 			print 'NUMBER OF ORF SCORES:', len(feature_score_list)
# 			print 'Free Concentration:', free_conc
# 			print 'pseudoRT: ' + str(primary_matrix.GetPseudoRT())
# 			print 'pseudoTemperature: ' + str(primary_matrix.GetPseudoTemp())
# 			print regulatory_string, norm_roc_auc, mncp, '\n', regulated_ranks
# 			result_table += regulatory_string.ljust(22)
# 			result_table += str(norm_roc_auc).ljust(20) + str(mncp).ljust(20) + '\n'
# 	print 'Probability File:', probability_file_name
# 	print result_table

#@-doc
#@-body
#@-node:33::<<def sequence_features_main>>


#@<<def score_sequence_features>>
#@+node:16::<<def score_sequence_features>>
#@+body
#@+doc
# 
# def score_sequence_features(features_list, binding_site_model, free_concentration_factor,
# 			   regulatory_region_model):
# 	list_of_score_tuples = []
# 	for cur_feature in features_list:
# 		cur_orf_score = RegulatoryRegionScoreOrf(cur_orf,
# 												 regulatory_region_model,
# 												 binding_site_model,
# 												 free_concentration_factor)
# 			list_of_score_tuples.append(tuple((cur_orf_score,cur_orf)))
# 	return list_of_score_tuples

#@-doc
#@-body
#@-node:16::<<def score_sequence_features>>


#@<<def output_base_frequencies>>
#@+node:7::<<def output_base_frequencies>>
#@+body
def output_base_frequencies(chromosome_table_file_name):

	feature_file_list, chromosome_file_hash, chromosome_flags_hash, file_frequency_hash =\
					   ChromosomeTableParse(chromosome_table_file_name)

	chromosome_hash, feature_dictionary, ambiguous_feature_names, feature_type_dict = load_feature_file(feature_file_list)
		
	load_chromosome_sequences(chromosome_file_hash, chromosome_flags_hash,
							  chromosome_hash, feature_dictionary,
							  ambiguous_feature_names)

	frequency_hash = base_count(chromosome_hash)
##	chromosome_labels = chromosome_hash.keys()
##	chromosome_labels.sort(IntegerStringCmp)
	chromosome_labels = sort_numeric_string(chromosome_hash.keys())
	print 'Calculating base frequencies using these chromosomes:\n', chromosome_labels, '\n'
	if file_frequency_hash:
		print 'Base Frequencies Provided in Chromosome Table:'
		print generate_frequency_table(file_frequency_hash)
	print 'Calculated Base Frequencies:'
	print generate_frequency_table(frequency_hash)
	

def generate_frequency_table(frequency_hash):
	FREQUENCY_TOKEN = 'Frequency:'
	frequency_header = ''
	for base_name in frequency_hash:
		frequency_header += '\t'.join((FREQUENCY_TOKEN, base_name, repr(frequency_hash[base_name]))) + '\n'
	return frequency_header

#@-body
#@-node:7::<<def output_base_frequencies>>



#@<<def output_regulated_ranks>>
#@+node:21::<<def output_regulated_ranks>>
#@+body
def output_regulated_ranks(score_feature_list, regulated_features,
						   unknown_regulated_features):
	## name_just = 20
	## rank_just = 10
	
	saturated = [feature for score, feature in score_feature_list if (score == 1)]
	
	score_name_feature_list = [(score, (feature.CommonName or feature.Name), feature) for score,feature in score_feature_list]
	score_name_feature_rank_list = add_rank_ties_bottom(score_name_feature_list)
	del score_name_feature_list
	score_name_feature_rank_list.reverse()
	
	result_string = ''

	score,name,feature,rank = score_name_feature_rank_list[-1]
	OUTPUT_RANK_JUST = len(str(rank))
	for score,name,feature,rank in score_name_feature_rank_list:
		if feature in regulated_features:
			result_string += '\t'.join((str(rank).rjust(OUTPUT_RANK_JUST), name.ljust(OUTPUT_NAME_JUST), repr(score))) + '\n'
	if unknown_regulated_features:
		result_string += 'Unknown Features:\n'
		for unknown_name in unknown_regulated_features:
			result_string += ''.join((''.ljust(OUTPUT_RANK_JUST), unknown_name.ljust(OUTPUT_NAME_JUST))) + '\n'
	if saturated:
		## result_string += '\n*********'
		result_string += '\nNUMBER SATURATED: ' + str(len(saturated)) + '\n'
		## result_string += '\n*********\n'
	return result_string

#@-body
#@-node:21::<<def output_regulated_ranks>>


#@<<def david output>>
#@+node:34::<<def david output>>
#@+body
##def short_output(file_name, feature_score_list, regulated_orfs,
##				 unknown_regulated_features,
##				 roc_auc, rocauc_pval,
##				 mncp, mncp_pval,
##				 KaMAX, KaMAX_sequence,
##				 information_list,
##				 command_line):
def short_output(feature_score_list, regulated_features,
				 unknown_regulated_features,
				 roc_auc, U_score,z_score,
				 mncp, estimated_mncp_prob,
				 KaMAX, KaMAX_sequence,
				 information_list,
				 command_line):
	decimals = 6
##	mncpjust = 12
##	rocjust = 12
##	kamax_just = 12
	mncpjust = rocjust = justify = 13
	common_tag = '$'
	score_tag    = common_tag+'@'+' '
	sequence_tag = common_tag+'%'+' '
	info_tag     = common_tag+'&'+' '
	interp_string = '%.' + str(decimals) + 'g'
	## fix = fpformat.fix
	
	regulated_ranks = output_regulated_ranks(feature_score_list, regulated_features, unknown_regulated_features)
	output_string = command_line + '\n'
	output_string += ' '.join(('Scored:', str(len(feature_score_list)))) + '\n'
	output_string += '\n' + regulated_ranks + '\n'
	#output_string += ' '.join(str(mncp).ljust(20) + str(roc_auc).ljust(20) + '\n'
	output_string += ''.join((' '*len(score_tag), 'MNCP'.ljust(mncpjust),
							  'est_mncp_p'.ljust(mncpjust),
							  'ROC_AUC'.ljust(rocjust), 'U'.ljust(rocjust),'z'.ljust(rocjust),
							  'KaMAX'.ljust(rocjust)))+ '\n'
##	output_string += ''.join((tag1, fix(mncp,decimals).ljust(mncpjust),
##							  fix(estimated_mncp_prob,decimals).ljust(mncpjust),
##							  fix(roc_auc,decimals).ljust(rocjust),
##							  fix(U_score,decimals).ljust(rocjust),
##							  fix(z_score,decimals).ljust(rocjust),
##							  fix(KaMAX,decimals).ljust(kamax_just))) + '\n'
	output_string += score_tag + ''.join([(interp_string %val).ljust(justify)
										  for val in [mncp, estimated_mncp_prob,
													  roc_auc, U_score, z_score,
												 KaMAX]]) + '\n'
	output_string += sequence_tag + ''.join(KaMAX_sequence).upper() + '\n'
	output_string += info_tag + ' '.join(['%.2f'%val for val in information_list])+'\n'
	return output_string

#@-body
#@-node:34::<<def david output>>




#@<<command line>>
#@+node:35::<<command line>>
#@+body
#@<<def init_option_parser>>
#@+node:2::<<def init_option_parser>>
#@+body
def init_option_parser():
	"""
	Initializes the "optparse" command line option (flag) parser.
	"""
	version_num = 0.001
	version = '%prog ' + str(version_num)
	usage = 'usage: %prog [options] ChromTable ProbMatrix RegFeature\n' +\
			(' ' * len('usage: ')) + \
			'%prog -t, --top NUM ChromTable ProbMatrix\n' + \
			(' ' * len('usage: ')) + \
			'%prog --coordinate_feature CoordinateFeature ChromTable ProbMatrix RegFeature\n' + \
			(' ' * len('usage: ')) + \
			'%prog --sequence_feature SequenceFeatureFile ProbMatrix RegFeature\n' +\
			(' ' * len('usage: ')) + \
			'%prog --frequency ChromTable\n'
	
	parser = OptionParser(usage=usage, version=version)
	
	
	##parser.add_option("--EXCLUDE_MITO", action="store_false", dest="include_mito",
	##				  default=0, help="exclude")
	##parser.add_option("--INCLUDE_MITO", action="store_true", dest="include_mito")
	
		
	
	parser.add_option('-c', '--config', 
					  metavar="FILE",
					  help="Use FILE for gomer config, instead of default")
	parser.add_option('-t', '--top', type='int', dest="top_hits",
					  metavar="NUM",
					  help="Print the top NUM hits in genome, and the feature closest to that hit")
	parser.add_option('-r', '--reg_region_kappa',
					  action="append",
					  metavar="KAPPA_FILE  PARAM_STRING",
					  help="",
					  nargs = 2)
	parser.add_option('--coop_kappa',
					  action="append",
					  metavar="KAPPA_FILE  PARAM_STRING PROB_MATRIX FREE_CONC_RATIO K_DIMER_RATIO",
					  help='see documentation',
					  nargs = 5)
	parser.add_option('--comp_kappa',
					  action="append",
					  metavar="KAPPA_FILE PARAM_STRING PROB_MATRIX FREE_CONC_RATIO",
					  help='see documentation',
					  nargs = 4)
	parser.add_option('-k', '--kappa_help',
					  metavar="KAPPA_FILE",
					  help="List the params required by KAPPA_FILE, an example, and a description",
					  nargs = 1)
	parser.add_option('--free_conc_ratio',
					  type='float',
					  ## action="append",
					  metavar="CONC_RATIO",
					  ## help="Calculations will be done using (CONC_RATIO * 1/max_Ka, where max_Ka is the best possible Ka given by the PROB_MATRIX) as the free concentration. If this option is supplied more than once, a separate run will be done for each CONC_RATIO value",
					  help="Calculations will be done using (CONC_RATIO * 1/max_Ka, where max_Ka is the best possible Ka given by the PROB_MATRIX) as the free concentration.",
					  nargs = 1)
	parser.add_option('--filter_cutoff_ratio',
					  type='float',
					  metavar="CUTOFF",
					  help="A CUTOFF less than 1 turns off filtering",
					  default=0,
					  nargs = 1)
	##	parser.add_option('--pseudo_temp',
	##					  type='float',
	##					  action="store",
	##					  metavar="TEMP",
	##					  help="Use TEMP as the pseudo-temp",
	##					  nargs = 1)
	parser.add_option('-a', '--ambiguous',
					  metavar="FILE",
					  action="append",
					  help="Print ambiguous names from feature file FILE",
					  nargs = 1)
	parser.add_option('--feature_types',
					  metavar="FEATURE FILE",
					  help="Print types of features found in FEATURE FILE",
					  nargs = 1)
	parser.add_option('-f', '--feature',
					  metavar="FEATURE",
					  action="append",
					  help="Genome features of type FEATURE will be considered",
					  nargs = 1)
	
	parser.add_option('--coordinate_feature',
					  action="append",
					  metavar="CoordinateFeature",
					  help="This option can be given more than once, so that multiple CoordinateFeature files can be supplied",
					  nargs = 1)
	
	parser.add_option('--sequence_feature',
					  action="append",
					  metavar="SequenceFeatureFile",
					  help="This option can be given more than once, so that multiple SequenceFeatureFile files can be supplied",
					  nargs = 1)
	parser.add_option('--frequency',
					  default=0,
					  metavar="ChromTable",
					  help="Calculate the base frequencies of the sequences in ChromTable")
	parser.add_option('-s', '--short_output',
					  metavar="SHORT_OUTPUT_FILE",
					  help="Save a short version of the output to the SHORT_OUTPUT_FILE",
					  nargs = 1)
	parser.add_option('-o', '--output',
					  metavar="OUTPUT_FILE",
					  help="Save output to the OUTPUT_FILE",
					  nargs = 1)
	parser.add_option('-C', '--compression',
					  type='int',
					  metavar="LEVEL",
					  help="Use compression on the cache file.  LEVEL is an integer from 1 to 9 setting the level of compression; 1 is fastest but compresses the least, 9 is slowest but compresses maximally. Any value not between 1 and 9 defaults to 9",
					  default=None,
					  nargs = 1)
	##parser.add_option("--INCLUDE_MITO", action="store_true", dest="include_mito")
	parser.add_option('-n', '--no_long',
					  dest='long_output',
					  action="store_false",
					  help="Do not generate long output (overriden by -o/--output)",
					  default=True)
## 	parser.add_option('-P', '--pvalue',
## 					  type='float',
## 					  action="store",
## 					  metavar="NUM",
## 					  help="Run NUM simulations to estimate the p-value of the ROCAUC and MNCP values for the results.  Note: this is only available with versions of Python >=2.3, and can take a long time, depending on the number of simulations (1e5 -- 100000 is a good starting point).")
	parser.add_option('--cache_directory',
					  metavar="DIR",
					  help="Use directory DIR as the cache directory (this command line option overrides configuration file values).",
					  default=None,
					  nargs = 1)
	parser.add_option('--fast_cache',
					  metavar="DIR",
					  help="Use directory DIR as a fast cache directory (this command line option overrides configuration file values).",
					  default=None,
					  nargs = 1)
	parser.add_option('-D', '--delete_cache',
					  action="store_true",
					  help="Delete the cache file when execution is complete",
					  default=False)
	parser.add_option('--test_full_model',
					  action="store_true",
					  help="If you don't know what it is, don't use it!",
					  default=False)
	parser.add_option('--test_coop_model',
					  action="store_true",
					  help="If you don't know what it is, don't use it!",
					  default=False)

	return parser


#@-body
#@-node:2::<<def init_option_parser>>


#@<<def main>>
#@+node:1::<<def main>>
#@+body
## def main (chromosome_table_file_name=None, probability_file_name=None):
def main() :	
	## MITOCHONDRIAL_LABEL = '17'
	##<process arguments>
	
	#@<<optparse arguments>>
	#@+node:1::<<optparse arguments>>
	#@+body
	command_line = ' '.join(sys.argv)

	print >>sys.stderr, command_line
	
	parser = init_option_parser()
	
	(options, args) = parser.parse_args()
	
	long_output = options.long_output
	if options.output:
		output_filename = ExpandPath(options.output)
		output_directory = os.path.dirname(output_filename)
		if not os.path.exists(output_directory):
			print >>sys.stderr, 'Output directory does not exist:', output_directory
			print >>sys.stderr, 'Attempting to create output directory:', output_directory
			try:
				os.makedirs(output_directory)
			except OSError, (errno, strerror):
				if errno==17: pass
				else: raise
			print >>sys.stderr, 'Created directory:', output_directory

			
		OUT = OpenFile(output_filename, 'Output File','w')
		long_output = True
	else:
		OUT = sys.stdout
		
	if options.ambiguous:
		output_ambiguous(options.ambiguous)
		sys.exit(0)
	elif options.feature_types:
		output_feature_types(options.feature_types)
		sys.exit(0)
	elif options.kappa_help:
		print_kappa_help(options.kappa_help)
		sys.exit(0)
	elif options.frequency:
		output_base_frequencies(options.frequency)
		sys.exit(0)
	##---------------------------------------------------
	TOP_HITS_NUM_ARGS = 2
	SEQUENCE_FEATURE_NUM_ARGS = 2
	NORM_NUM_ARGS = 3
  	info_just = 29

	if options.top_hits and TOP_HITS_NUM_ARGS == len(args):
		chromosome_table_file_name = args[0]
		probability_file_name = args[1]
	elif options.sequence_feature:
		if SEQUENCE_FEATURE_NUM_ARGS <> len(args):
			parser.error('Incorrect number of arguments for "--sequence_feature" mode.')
			sys.exit(2)
		else:
			probability_file_name = args[0]
			regulated_feature_filename = args[1]
			if __debug__:
				for sequence_feature_file in options.sequence_feature:
					print >>sys.stderr, 'Sequence Feature File:'.ljust(info_just), sequence_feature_file
				print >>sys.stderr, 'Primary Matrix File:'.ljust(info_just), probability_file_name
				print >>sys.stderr, 'Regulated Feature File:'.ljust(info_just), regulated_feature_filename
	elif len(args) == (NORM_NUM_ARGS):
		chromosome_table_file_name = args[0]
		probability_file_name = args[1]
		regulated_feature_filename = args[2]
		if __debug__:
			print >>sys.stderr, 'Chromosome Table File:'.ljust(info_just), chromosome_table_file_name
			print >>sys.stderr, 'Primary Matrix File:'.ljust(info_just), probability_file_name
			print >>sys.stderr, 'Regulated Feature File:'.ljust(info_just), regulated_feature_filename
	else :
		parser.print_help()
		print >>sys.stderr , 'Command Line Arguments:', args
		# print >>sys.stderr, 'len(args)', len(args)
		# print >>sys.stderr, 'args', args
		sys.exit(2)
	##---------------------------------------------------
## 	if options.pvalue:
## 		if sys.hexversion < version_2_3_hex:
## 			print >>sys.stderr, 'Cannot use "pvalue" option, it requires at least version 2.3 of python.'
## 			print >>sys.stderr, 'Your version is:', sys.version
## 			sys.exit(2)
## 			options.pvalue = None
## 		else:
## 			options.pvalue = int(options.pvalue)
	##---------------------------------------------------
	if 'GOMER_HOME' in os.environ:
		gomer_home_directory = os.environ['GOMER_HOME']
		gomer_home_directory = os.path.expanduser(gomer_home_directory)
		search_path = ((os.getcwd(), gomer_home_directory))
	else:
		search_path = ((os.getcwd(), ))
		gomer_home_directory = os.getcwd()
	##---------------------------------------------------
	paths_to_check = (('',
					   'input_files/config/',
					   'input_files/',
					   '../input_files/config/',
					   '../input_files/'))
	for cur_path in paths_to_check:
		config_default = os.path.join(gomer_home_directory, cur_path, CONFIG_FILENAME)
		config_default = os.path.normpath(config_default)
		if os.path.exists(config_default):
			break
	##-------------------------------------------------------------------------
	if options.config and os.path.exists(options.config):
		config = load_config_file(options.config)
	elif os.path.exists(config_default):
		print >>sys.stderr, 'Config file found at:', config_default
		config = load_config_file(config_default)
	else:
		error_message = """
	Config file must either:
	   1. Be supplied by the -c/--config command line option. Or,
	   2. Be at $GOMER_HOME/input_files/config/gomer_config 
	
	   $GOMER_HOME is taken from the environment,if the "GOMER_HOME" environment
		  variable is defined.
	
	   Otherise, $GOMER_HOME is set to the current working directory."""
		raise TestControllerError(error_message)
	
	if config.has_option('directories', 'gomer_home'):
		gomer_home_directory = config.get('directories', 'gomer_home')
	##---------------------------------------------------
	if options.cache_directory:
		cache_directory = options.cache_directory
	else:
		cache_directory = get_config_parameter(config, 'cache_directory', 'directories')
	# print >>sys.stderr, 'cache_directory:', cache_directory
	cache_directory = ExpandPath(cache_directory)
	##---------------------------------------------------
	## cache_table_filename = get_config_parameter(config, 'cache_table', 'files')
	cache_table_filename = CACHE_TABLE_FILENAME
	cache_table_full_filename = os.path.join(cache_directory, cache_table_filename)
	if not os.path.exists(cache_directory):
		print >>sys.stderr, 'Cache directory: ' + cache_directory + " Doesn't exist."
		print >>sys.stderr, '\nAttempting to create:', cache_directory
		try:
			os.makedirs(cache_directory)
		except OSError, (errno, strerror):
			# if re.search('File exists', strerror, re.IGNORECASE):
			if errno==17:
				pass
			else:
				raise
		print >>sys.stderr, '\nCreated:', cache_directory

	if not os.path.exists(cache_table_full_filename):
		print >>sys.stderr, 'Cache table file was not found.'
		print >>sys.stderr, 'It is expected to be at:', cache_table_full_filename
		print >>sys.stderr, '\nAttempting to create cache table file: ', cache_table_full_filename
		a = file(cache_table_full_filename, 'w')
		a.close()
	##---------------------------------------------------
	

	#@-doc
	#@-body
	#@-node:1::<<optparse arguments>>

	#<load reg region kappas>>

	##---------------------------------------------------
	short_output_handle = None
	if options.short_output:
		if options.short_output.strip() == '-':
			short_output_handle = sys.stdout
		else:
			short_output_filename = ExpandPath(options.short_output)
			short_output_directory = os.path.dirname(short_output_filename)
			if not os.path.exists(short_output_directory):
				print >>sys.stderr, 'Output directory does not exist:', short_output_directory
				print >>sys.stderr, 'Attempting to create output directory:', short_output_directory
				try:
					os.makedirs(short_output_directory)
				except OSError, (errno, strerror):
					if errno==17: pass
					else: raise
			short_output_handle = OpenFile(options.short_output, 'Short Output File','w')
	##---------------------------------------------------
	fast_cache_directory = None
	if options.fast_cache:
		fast_cache_directory = options.fast_cache
	elif config.has_option('directories', 'fast_cache'):
		fast_cache_directory = config.get('directories', 'fast_cache')
## 	if fast_cache_directory and (not os.path.exists(fast_cache_directory)):
## 		raise TestControllerError("Fast Cache Directory doesn't exist: '" + fast_cache_directory + "'\n")
 	if fast_cache_directory:
		TestFile(fast_cache_directory, "Fast Cache Directory")
	##---------------------------------------------------
	if config.has_option('directories', 'kappa_directory'):
		kappa_directory = config.get('directories', 'kappa_directory')
		kappa_search_paths = (('', kappa_directory, gomer_home_directory,
							   os.path.join(gomer_home_directory, kappa_directory)))
	else:
		kappa_search_paths = (('',gomer_home_directory))
	##-------------------------------------------------------------------------
	if options.reg_region_kappa:
		reg_region_kappa_pairs = options.reg_region_kappa
		## print 'DEBUG:', reg_region_kappa_pairs
	elif (options.coordinate_feature and
		  config.has_option('kappas', 'coordinate_regulatory_region') and
		  config.has_option('kappas', 'coordinate_regulatory_region_params')):
		reg_region_kappa_pairs = ((( config.get('kappas', 'coordinate_regulatory_region'),
									config.get('kappas', 'coordinate_regulatory_region_params'))),)
	elif (options.sequence_feature and
		  config.has_option('kappas', 'sequence_regulatory_region') and
		  config.has_option('kappas', 'sequence_regulatory_region_params')):
		reg_region_kappa_pairs = ((( config.get('kappas', 'sequence_regulatory_region'),
									config.get('kappas', 'sequence_regulatory_region_params'))),)
	elif (config.has_option('kappas', 'regulatory_region') and
		  config.has_option('kappas', 'regulatory_region_params')):
		reg_region_kappa_pairs = ((( config.get('kappas', 'regulatory_region'),
									config.get('kappas', 'regulatory_region_params'))),)
	else:
		error_message = 'No Regulatory Region Kappa supplied\n' +\
						'An appropriate Regulatory Region Kappa must be supplied ' +\
						'In the GOMER configuration file, or using the\n' +\
						'-r/--reg_region_kappa option\n'
		raise TestControllerError(error_message)

	module_hash, reg_region_list = load_reg_region_kappas(reg_region_kappa_pairs, kappa_search_paths)
	
	if long_output: print >>OUT, command_line
	filter_cutoff_ratio = options.filter_cutoff_ratio
	if filter_cutoff_ratio < 1:
		filter_cutoff_ratio = 0
##---------------------------------------------------------------------
	if options.sequence_feature:
		feature_dictionary = {}
		feature_list = []
		frequency_hash_list = []
		pseudo_chrom_hash = {}
		## print >>sys.stderr, 'options.sequence_feature:', options.sequence_feature
		for sequence_file_name in options.sequence_feature:
			(feature_dictionary,
			 feature_list,
			 frequency_hash,
			 pseudo_chrom_hash,
			 ambiguous_feature_names) = ParseFASTASequenceFeatureFile(sequence_file_name,
																	  feature_dictionary,
																	  feature_list,
																	  pseudo_chrom_hash)
			frequency_hash_list.append(frequency_hash)
			
		if not frequency_hash:
			print '=' * 50, '\n', '=' * 50, '\n'
			print 'NO FREQUENCY HASH SUPPLIED IN SEQUENCES!'
			print 'IT IS RECOMMENDED THAT A FREQUENCY HASH IS SUPPLIED!'
			print 'FREQUENCY HASH WILL BE CALCULATED FROM SUPPLIED SEQUENCES!'
			print '=' * 50, '\n', '=' * 50, '\n'
			
		if len(options.sequence_feature) > 1 :
			for index in range(1, len(frequency_hash_list)):
				if frequency_hash_list[0] <> frequency_hash_list[index]:
					print '=' * 50, '\n', '=' * 50, '\n'
					print 'MULTIPLE SEQUENCES SUPPLIED, WITH DIFFERENT FREQUENCY HASHES'
					print 'FREQUENCY HASH WILL BE CALCULATED FROM SUPPLIED SEQUENCES'
					print '=' * 50, '\n', '=' * 50, '\n'
					frequency_hash = None
					break
					
		## default_features = (SEQUENCE_TYPE,)
		options.feature = (SEQUENCE_TYPE,)
		feature_type_dict = {SEQUENCE_TYPE:1}
		for param_hash, cur_module in reg_region_list:
			if cur_module.TYPE <> 'SequenceFeatureRegulatoryRegionModel':
				raise StandardError, "It is only appropriate to use SequenceFeatureRegulatoryRegionModels for 'SequenceFeature' features"
		chromosome_hash = pseudo_chrom_hash
		##for feature_name in feature_dictionary:
		##	feature = feature_dictionary[feature_name]
		##	print feature_name, len(feature), len(feature.Sequence.Sequence), feature.Sequence.Sequence
		chromosome_table_file_name = None
##---------------------------------------------------------------------
	else: # if not options.sequence_feature:
		(feature_file_list,
		 chromosome_file_hash,
		 chromosome_flags_hash,
		 frequency_hash) = load_chromosome_table(chromosome_table_file_name)

		if options.coordinate_feature:
			(chromosome_hash, feature_dictionary,
			 ambiguous_feature_names, seq_info_hash) = load_coordinate_feature_file(options.coordinate_feature)
			##  default_features = (COORDINATE_TYPE,)
			options.feature = default_features = (COORDINATE_TYPE,)
			feature_type_dict = {COORDINATE_TYPE:1}
			for param_hash, cur_module in reg_region_list:
				if cur_module.TYPE <> 'CoordinateRegulatoryRegionModel':
					raise StandardError, "It is only appropriate to use CoordinateRegulatoryRegionModels for 'Coordinate' features"
			# print >>sys.stderr, feature_dictionary.keys()
		else:
			chromosome_hash, feature_dictionary, ambiguous_feature_names, feature_type_dict = load_feature_file(feature_file_list)
			default_features = feature_file_parser.DEFAULT_FEATURES

		load_chromosome_sequences(chromosome_file_hash, chromosome_flags_hash,
								  chromosome_hash, feature_dictionary,
								  ambiguous_feature_names)

		if options.coordinate_feature:
			for label in seq_info_hash:
				if seq_info_hash[label].seqID  <> chromosome_hash[label].Sequence.Accession:
					error_message = ' '.join(('Sequence ID for chromosome', label,
											 'in coordinate feature file(' +
											 seq_info_hash[label].seqID +') does',
											 'not match that found in the sequence',
											 'loaded (' + chromosome_hash[label].Sequence.Accession
											 + ')'))
					raise TestControllerError, error_message
				elif seq_info_hash[label].md5sum <> chromosome_hash[label].MD5:
					error_message = ' '.join(('MD5 sum for chromosome', label,
											  'in coordinate feature file(' +
											  seq_info_hash[label].md5sum +') does',
											  'not match that calculated for loaded',
											  'sequence (' + chromosome_hash[label].MD5
											  + ')'))
					raise TestControllerError, error_message
					
    ##----------------------------------------------------------------------------------------		
	## Select Feature Types
	##---------------------
	if options.feature:
		feature_types = options.feature
	elif config.has_section('parameters') and config.has_option('parameters', 'feature_types'):
		feature_types = [cur_type.strip() for cur_type in 
						 config.get('parameters', 'feature_types').split(',')]
	else:
##		feature_types = (default_feature,)
		feature_types = default_features
	for cur_type in feature_types:
		if cur_type not in feature_type_dict:
			raise FeatureTypeError('\nFeature type is not found in feature file(s): '
								   + cur_type + '\n', feature_type_dict.keys())
    ##----------------------------------------------------------------------------------------		
	if not frequency_hash:
		print 'NO BASE FREQUENCIES SUPPLIED - CALCULATING . . .'
		frequency_hash = base_count(chromosome_hash)
		print 'FREQUENCIES:', frequency_hash
	primary_matrix = load_probability_matrix(probability_file_name, frequency_hash)
	## if options.pseudo_temp:
	##	probability_matrix.ResetPseudoTemp(options.pseudo_temp)
	# probability_matrix.TestFloatError(20)
	# probability_matrix.SetBaseFrequencies(frequency_hash)

	if options.top_hits:
		cache_score_all_windows(primary_matrix, chromosome_hash,filter_cutoff_ratio,
								cache_directory, cache_table_filename,
								fast_cache_directory,
								frequency_hash,
								debug=False,
								compression=options.compression,
								delete_cache=options.delete_cache)
		find_top_hits(options.top_hits, chromosome_hash, primary_matrix)

		sys.exit(2)
##-------------------------------------------------------------------------------
	regulated_features, excluded_features, unregulated_features, unknown_regulated_features = \
					load_regulated_feature_names(regulated_feature_filename,
											 feature_dictionary,
											 ambiguous_feature_names)
	scored_matrices = {}
	matrix_md5 = cache_score_all_windows(primary_matrix, chromosome_hash,
										 filter_cutoff_ratio,
										 cache_directory,cache_table_filename,
										 fast_cache_directory,
										 frequency_hash,
										 debug=False,
										 compression=options.compression,
										 delete_cache=options.delete_cache)
	scored_matrices[matrix_md5] = 1

##-------------------------------------------------------------------------------
	probability_matrix_hash = {}

	primary_matrix_key = ' '.join((primary_matrix.Descriptor, MatrixSerialNumber()))
	probability_matrix_hash[primary_matrix_key] = primary_matrix
	probability_matrix_types = {}
	probability_matrix_types[primary_matrix_key] = 'primary'
	free_conc_hash = {}
	competitor_tf_name_list = []
	coop_tf_name_list = []
	coop_model_hash = {}
	comp_model_hash = {}
	k_dimer_hash = {}
	info_just = 29
	if long_output:
		print >>OUT, '-' * 60
		if chromosome_table_file_name:
			print >>OUT, 'Chromosome Table File:'.ljust(info_just), chromosome_table_file_name
		if options.sequence_feature:
			for sequence_feature_file in options.sequence_feature:
				print >>OUT, 'Sequence Feature File:'.ljust(info_just), sequence_feature_file
		if options.coordinate_feature:
			for coordinate_feature_file in options.coordinate_feature:
				print >>OUT, 'Coordinate Feature File:'.ljust(info_just), coordinate_feature_file
		for param_hash, cur_module in reg_region_list:
			print >>OUT, 'Regulatory Region Model:'.ljust(info_just), cur_module.NAME
			if param_hash:
				print >>OUT, 'Regulatory Region Parameters:'.ljust(info_just), \
					  ','.join([str(key) + ':' + str(param_hash[key]) for key in param_hash])
		if not (options.coordinate_feature or options.sequence_feature):
			print >>OUT, 'Feature Types:'.ljust(info_just), ', '.join(feature_types)
		print >>OUT, 'Primary Matrix File:'.ljust(info_just), probability_file_name
		print >>OUT, 'Primary Matrix Name:'.ljust(info_just), primary_matrix.Name
		print >>OUT, 'Regulated Feature File:'.ljust(info_just), regulated_feature_filename
		
		if options.filter_cutoff_ratio and filter_cutoff_ratio:
			print >>OUT, 'Filter Cutoff Ratio:'.ljust(info_just), filter_cutoff_ratio
	## print 'Primary Matrix pseudoTemp:', primary_matrix.GetPseudoTemp()
	## END COOP OR COMP
##-------------------------------------------------------------------------------
	## BEGIN COOP
	if options.coop_kappa:
		#@<<process coop_kappa parameters>>
		#@+node:2::<<process coop_kappa parameters>>
		#@+body
		#                            @doc
		# if options.coop_kappa:
		coop_matrix_hash = {}
		k_dimer_hash = {primary_matrix_key:{}}
		coop_model_hash = {primary_matrix_key:{}}
		## coop_tf_name_list = []
		long_sep = '-=-' * 25
		under_sep = '-=-' * 7
		coop_just = 18
		if long_output: print >>OUT, long_sep, '\nCooperativity Parameters\n', under_sep
		for coop_kappa_file, param_string, prob_matrix, free_conc_ratio, k_dimer_ratio in options.coop_kappa:
			param_hash = parse_parameter_string(param_string)
			cur_matrix = load_probability_matrix(prob_matrix, frequency_hash)
			if cur_matrix.MD5 not in scored_matrices:
				matrix_md5 = cache_score_all_windows(cur_matrix,
													 chromosome_hash,
													 filter_cutoff_ratio,
													 cache_directory,
													 cache_table_filename,
													 fast_cache_directory,
													 frequency_hash,
													 debug=False,
													 compression=options.compression,
													 delete_cache=options.delete_cache)
				scored_matrices[matrix_md5] = 1
			cur_matrix_key = ' '.join((cur_matrix.Descriptor, MatrixSerialNumber()))
			probability_matrix_hash[cur_matrix_key] = cur_matrix
			probability_matrix_types[cur_matrix_key] = 'coop'
			# coop_matrix_hash[cur_matrix_key] = cur_matrix
			##--------------------------------------------------
			coop_tf_name_list.append(cur_matrix_key)
			##--------------------------------------------------
			(max_Ka, max_Ka_values, max_Ka_sequence,
			 min_Ka, min_Ka_values, min_Ka_sequence) = cur_matrix.GetKaRange()
			if free_conc_ratio.lower() == 'default':
				free_conc_ratio = 1.0
			else:
				free_conc_ratio = float(free_conc_ratio)
			free_conc = free_conc_ratio/max_Ka
			## print 'free_conc', free_conc, '(ratio:', free_conc/(1/max_Ka), ')'
			free_conc_hash[cur_matrix_key] = free_conc
			##--------------------------------------------------
			if k_dimer_ratio.lower() == 'default' :
				k_dimer_ratio = 1.0
			else:
				k_dimer_ratio = float(k_dimer_ratio)
			k_dimer = k_dimer_ratio * max_Ka
			## print 'cur_matrix.Descriptor', cur_matrix.Descriptor
			## print 'k_dimer', k_dimer, '(ratio:', k_dimer/(max_Ka), ')'
			k_dimer_hash[primary_matrix_key][cur_matrix_key] = float(k_dimer)
			## print 'k_dimer_hash[primary_matrix.Descriptor][cur_matrix.Descriptor]', k_dimer_hash[primary_matrix.Descriptor][cur_matrix.Descriptor]
			##--------------------------------------------------
			module_hash, module = load_kappa_function(coop_kappa_file, kappa_search_paths, module_hash)
			##--------------------------------------------------
			if long_output:
				print >>OUT, 'Coop Kappa File:'.ljust(coop_just), coop_kappa_file
				# print >>OUT, 'parameter string:'.ljust(coop_just), param_string
				print >>OUT, 'Coop Parameters:'.ljust(coop_just), \
					  ','.join([str(key) + ':' + str(param_hash[key]) for key in param_hash])
				print >>OUT, 'Coop Matrix File:'.ljust(coop_just), prob_matrix
				print >>OUT, 'Coop Matrix Name:'.ljust(coop_just), cur_matrix.Name
				## print 'free conc ratio:'.ljust(coop_just), free_conc_ratio
				print >>OUT, 'Kdimer ratio:'.ljust(coop_just), k_dimer_ratio
				print >>OUT, 'Kdimer:'.ljust(coop_just), k_dimer
				print >>OUT, long_sep
			##--------------------------------------------------
			coop_model = module.CoopModel(primary_matrix, cur_matrix, **param_hash)
			coop_model_hash[primary_matrix_key][cur_matrix_key] = coop_model
		
		
		#@-body
		#@-node:2::<<process coop_kappa parameters>>
	## END COOP
	## END if options.coop_kappa:
##-------------------------------------------------------------------------------


##-------------------------------------------------------------------------------
	## BEGIN COMP

	if options.comp_kappa:
		comp_model_hash = {primary_matrix_key:{}}
		## competitor_tf_name_list = []
		long_sep = '=-=' * 25
		under_sep = '=-=' * 7
		comp_just = 18
		if long_output: print >>OUT, long_sep, '\nCompetition Parameters\n', under_sep
		for comp_kappa_file, param_string, prob_matrix, free_conc_ratio in options.comp_kappa:
			param_hash = parse_parameter_string(param_string)
			cur_matrix = load_probability_matrix(prob_matrix, frequency_hash)
			if cur_matrix.MD5 not in scored_matrices:
				matrix_md5 = cache_score_all_windows(cur_matrix,
													 chromosome_hash,
													 filter_cutoff_ratio,
													 cache_directory,
													 cache_table_filename,
													 fast_cache_directory,
													 frequency_hash,
													 debug=False,
													 compression=options.compression,
													 delete_cache=options.delete_cache)
				scored_matrices[matrix_md5] = 1
			cur_matrix_key = ' '.join((cur_matrix.Descriptor, MatrixSerialNumber()))
			probability_matrix_hash[cur_matrix_key] = cur_matrix
			probability_matrix_types[cur_matrix_key] = 'comp'
			## print 'cur_matrix.Descriptor', cur_matrix.Descriptor
			##--------------------------------------------------
			competitor_tf_name_list.append(cur_matrix_key)
			##--------------------------------------------------
			(max_Ka, max_Ka_values, max_Ka_sequence,
			 min_Ka, min_Ka_values, min_Ka_sequence) = cur_matrix.GetKaRange()
			if free_conc_ratio.lower() == 'default':
				free_conc_ratio = 1.0
			else:
				free_conc_ratio = float(free_conc_ratio)
			free_conc = free_conc_ratio/max_Ka
			## print 'free_conc', free_conc, '(ratio:', free_conc/(1/max_Ka), ')'
			free_conc_hash[cur_matrix_key] = free_conc
			##--------------------------------------------------
			module_hash, module = load_kappa_function(comp_kappa_file, kappa_search_paths, module_hash)

			comp_model = module.CompModel(primary_matrix, cur_matrix, **param_hash)
			comp_model_hash[primary_matrix_key][cur_matrix_key] = comp_model
			##--------------------------------------------------
			if long_output:
				print >>OUT, 'Comp Kappa File:'.ljust(comp_just), comp_kappa_file
				print >>OUT, 'Comp Parameters:'.ljust(comp_just), \
					  ','.join([str(key) + ':' + str(param_hash[key]) for key in param_hash])
				print >>OUT, 'Comp Matrix File:'.ljust(comp_just), prob_matrix
				print >>OUT, 'Comp Matrix Name:'.ljust(comp_just), cur_matrix.Name
				## print 'free conc ratio:'.ljust(comp_just), free_conc_ratio
				print >>OUT, long_sep
			##--------------------------------------------------
	## END COMP
	## END if options.comp_kappa:
##-------------------------------------------------------------------------------

			
##-------------------------------------------------------------------------------
	#<old mito>>
	#<old cache>>
	## listof_regulatory_region_ranges = ((600,0),)

#@@code
##-------------------------------------------------------------------------------
##	start_list = range(600, 2100, 100)
##	stop_list = range(0, 1500, 100)

#@+doc
# 
# 	start_list = range(600, 1500, 500)
# 	stop_list = range(0, 1000, 400)
# 	listof_regulatory_region_ranges = []
# 	for start in start_list:
# 		for stop in stop_list:
# 			if start > stop:
# 				listof_regulatory_region_ranges.append((start, stop))

#@-doc
#@@code
	# print 'Regulatory Ranges:', listof_regulatory_region_ranges
##===============================================================================
 	(max_Ka, max_Ka_values, max_Ka_sequence,
 	 min_Ka, min_Ka_values, min_Ka_sequence) = primary_matrix.GetKaRange()
	if options.free_conc_ratio:
		free_conc_list = tuple((options.free_conc_ratio/max_Ka,))
	else:
		free_conc_list = tuple((1/max_Ka,))

## 	per_position_information = primary_matrix.GetPositionInformation()
## 	# max_Ka, max_Ka_values, max_Ka_sequence = probability_matrix.GetBestKa()
## 	print 'Max Ka:', max_Ka, '\nMax Ka Values:', max_Ka_values, '\nMax Ka Seqeunce:', ''.join([base.upper() for base in max_Ka_sequence])
## 	print 'Min Ka:', min_Ka, '\nMin Ka Values:', min_Ka_values, '\nMin Ka Seqeunce:', ''.join([base.upper() for base in min_Ka_sequence])
## 	print 'MaxKa/MinKa:', max_Ka/min_Ka
## 	print 'Information:', per_position_information
## 	print 'Total Matrix Information:', reduce(operator.add, per_position_information)
	## print 'Free Concentration Ratios:', options.free_conc_ratio
	## print 'Free Concentration List:', free_conc_list
##===============================================================================
##	score_ORFs(chromosome_hash, probability_matrix, free_concentration_factor=0.01)
	result_table = ''
	# result_table += '1/max_Ka:\t' + str(1/max_Ka) + '\n'
	# result_table +=  'pseudoRT: ' + str(primary_matrix.GetPseudoRT()) + '\n'
	## result_table +=  'pseudoTemperature: ' + str(primary_matrix.GetPseudoTemp()) + '\n'
	for free_conc in free_conc_list:
		## print >> sys.stderr, 'Free Concentration:', free_conc, '(ratio:', free_conc/(1/max_Ka), ')'
		## result_table += ' '.join(('[', str(free_conc), ']')).ljust(22) + 'Normalized ROC-AUC'.ljust(20) + 'MNCP'.ljust(20) + '\n'
		## result_table += ' '.join(('[', str(free_conc), ']')).ljust(22) + '\n'

#@+doc
# 
# ##===============================================================================
# 		for regulatory_region_range in listof_regulatory_region_ranges:
# 			print >> sys.stderr, 'Regulatory Region Rangs:', regulatory_region_range
# 			##old_score_ORFs(chromosome_hash, probability_matrix, free_conc, regulatory_region_ranges)
# 			regulatory_string = 'Square' + str(regulatory_region_range[0]) + '-' +\
# 								str(regulatory_region_range[1])
# 			regulatory_five_prime, regulatory_three_prime = regulatory_region_range
# 			regulatory_region_model = RegulatoryRegionModel(primary_matrix,
# 															regulatory_five_prime=regulatory_five_prime,
# 															regulatory_three_prime=regulatory_three_prime)
# ##===============================================================================

#@-doc
#@@code
##===============================================================================
		for param_hash, cur_module in reg_region_list:
##			print 'Regulatory Region Model:', cur_module.NAME
##			print 'Parameters:', param_hash
			## regulatory_string = cur_module.NAME + str(param_hash)
			## print "DEBUG:", param_hash, cur_module.RegulatoryRegionModel.__name__
			regulatory_region_model = \
									cur_module.RegulatoryRegionModel(primary_matrix,
																	 **param_hash)

			free_conc_hash[primary_matrix_key] = free_conc

			sep_line = '-==' * 25
			conc_just = 22
			if long_output:
				print >>OUT, sep_line
				print >>OUT, ' '.join([x.ljust(conc_just) for x in ['Information', '[Free]', '[Free] ratio',
																	'MaxKa', 'MinKa', 'MaxKa/MinKa',
																	'MaxKa Seq', 'MinKa Seq',
																	'Pseudo Temp', 'Type', 'Matrix']])
			type_key_tuple = [((probability_matrix_types[matrix_key], matrix_key)) for matrix_key in probability_matrix_hash]
			type_key_tuple.sort()
			type_key_tuple.reverse()
			sorted_matrix_keys = [matrix_key for matrix_type, matrix_key in type_key_tuple]
			## for matrix_key in probability_matrix_hash:
			for matrix_key in sorted_matrix_keys:
				matrix = probability_matrix_hash[matrix_key]
				per_position_information = matrix.GetPositionInformation()
				(max_Ka, max_Ka_values, max_Ka_sequence,
				 min_Ka, min_Ka_values, min_Ka_sequence) = matrix.GetKaRange()
				max_Ka_sequence = ''.join([base.upper() for base in max_Ka_sequence])
				min_Ka_sequence = ''.join([base.upper() for base in min_Ka_sequence])
				total_matrix_info = reduce(operator.add, per_position_information)

				free_conc = free_conc_hash[matrix_key]
				matrix_type = probability_matrix_types[matrix_key]
				if long_output:
					print >>OUT, ' '.join([str(x).ljust(conc_just) for x in [total_matrix_info, free_conc, free_conc*max_Ka,
																			 max_Ka, min_Ka, max_Ka/min_Ka,
																			 max_Ka_sequence, min_Ka_sequence,
																			 matrix.GetPseudoTemp(), matrix_type, matrix.Name]])
			if long_output: print >>OUT, sep_line
##===============================================================================
			if options.test_full_model:
				feature_score_list = fullmodel_score_features(feature_types,
															  chromosome_hash,
															  regulatory_region_model,
															  primary_matrix_key,
															  coop_tf_name_list,
															  competitor_tf_name_list,
															  free_conc_hash,
															  k_dimer_hash,
															  coop_model_hash,
															  comp_model_hash,
															  probability_matrix_hash)
##===============================================================================
			elif options.test_coop_model and (not options.comp_kappa):
				feature_score_list = coop_score_features(feature_types,
														 chromosome_hash,
														 regulatory_region_model,
														 primary_matrix_key,
														 coop_tf_name_list,
														 free_conc_hash,
														 k_dimer_hash,
														 coop_model_hash,
														 probability_matrix_hash)
##===============================================================================
			elif options.comp_kappa:
				feature_score_list = fullmodel_score_features(feature_types,
															  chromosome_hash,
															  regulatory_region_model,
															  primary_matrix_key,
															  coop_tf_name_list,
															  competitor_tf_name_list,
															  free_conc_hash,
															  k_dimer_hash,
															  coop_model_hash,
															  comp_model_hash,
															  probability_matrix_hash)
##===============================================================================
			elif options.coop_kappa:
				feature_score_list = coop_score_features(feature_types,
														 chromosome_hash,
														 regulatory_region_model,
														 primary_matrix_key,
														 coop_tf_name_list,
														 free_conc_hash,
														 k_dimer_hash,
														 coop_model_hash,
														 probability_matrix_hash)
##===============================================================================
			else:
				feature_score_list = score_features(feature_types,
													chromosome_hash,
													primary_matrix,
													free_conc,
													regulatory_region_model)
##===============================================================================
			##best_and_worst = output_best_and_worst(feature_score_list, num_best=10, num_worst=10)

##			if regulated_orfs:
##				(roc_auc,
##				 roc_pval,
##				 roc_pval_ratio,
##				 mncp,
##				 mncp_pval,
##				 mncp_pval_ratio) = get_stats(feature_score_list, regulated_orfs,
##											  excluded_orfs, unregulated_orfs,
##											  options.pvalue)

			rocauc = U_score = z_score = mncp = estimated_mncp_prob = ''
			if regulated_features:
##				stats_tuple = get_stats(feature_score_list,regulated_features,
##										excluded_features,unregulated_features,
##										options.pvalue)
				stats_tuple = get_stats(feature_score_list,regulated_features,
										excluded_features,unregulated_features)
				if stats_tuple:
					rocauc, U_score, z_score, mncp, estimated_mncp_prob = stats_tuple
					## result_table += regulatory_string.ljust(22) + '\n'
					result_table += ' '.join(('ROC AUC:',str(rocauc),
											  '; U='+str(U_score),
											  '; z='+str(z_score)+'\n'))
					result_table += ' '.join(('MNCP   :',str(mncp),
											  '; estimated prob='+str(estimated_mncp_prob) + '\n'))
					if long_output: OUT.write(result_table)
			if long_output:
				ranks_and_scores = output_ranks(feature_score_list, regulated_features,
												unknown_regulated_features,
												excluded_features
												, sort_ascending=1
												## , print_all=1
												## , feature_only=0
												)
				OUT.write(ranks_and_scores)

			if short_output_handle:
				short_output_string = short_output(feature_score_list,
												   regulated_features, unknown_regulated_features,
												   rocauc, U_score, z_score,
												   mncp, estimated_mncp_prob,
												   max_Ka, max_Ka_sequence,
												   per_position_information,
												   command_line)
				short_output_handle.write(short_output_string)
				short_output_handle.close()


#@-body
#@-node:1::<<def main>>





#@<<if main>>
#@+node:4::<<if main>>
#@+body
if __name__ == '__main__':
##	try :
##		main()
##	except GomerError, instance:
##		instance.print_message()
##		raise 
	main()

#@-body
#@-node:4::<<if main>>


#@-body
#@-node:35::<<command line>>


#@-body
#@-node:0::@file gomer.py
#@-leo
