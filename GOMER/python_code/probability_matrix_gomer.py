#! /usr/bin/env python
from __future__ import division
#@+leo
#@comment Created by Leo at Fri Oct  3 16:11:21 2003
#@+node:0::@file probability_matrix_gomer.py
#@+body
#@@first
#@@first
#@@language python

"""
*Description:

A matrix representation of a binding site.  

It can score a sequence, returning a hitlist instance with a scores for each
possible window of the matrix on the sequence (both forward and reverse
complement).

A probability matrix instance is initialized with a table of base probabilities, 
in addition to this probability representation, a bitscore (information content)
based representation is generated.  At the moment, this representation is what is
used to score the sequence.
"""


#@<<imports>>
#@+node:1::<<imports>>
#@+body
##import os
##os.environ['PYCHECKER'] = '--allglobals --stdlib'
##import pychecker.checker

##import check_python_version
##check_python_version.CheckVersion()


import operator
import math
import sys
import Numeric
import random
import md5

# import array

# import information
from exceptions_gomer import NOT_DONE_YET
from exceptions_gomer import GomerError
from hit_list_gomer import HitList
from flat_prob_matrix_printer_gomer import FlatProbMatrixOutput
import globals_gomer
SEQUENCE_ALPHABET = globals_gomer.SEQUENCE_ALPHABET
NULL_SYMBOL = globals_gomer.NULL_SYMBOL
NO_INFO_SYMBOL = globals_gomer.NO_INFO_SYMBOL
#----------------------------------------------------------------------
complementation_mapping = globals_gomer.complementation_mapping.copy()
complement_keys = complementation_mapping.keys()
for base in complement_keys:
	if base.isupper():
		complementation_mapping[base.lower()] = complementation_mapping[base].lower()
	elif base.islower():
		complementation_mapping[base.upper()] = complementation_mapping[base].upper()
del base
#----------------------------------------------------------------------



#@-body
#@-node:1::<<imports>>



ln = math.log
# Log2 = lambda x:math.log(x)/math.log(2) # log base 2
def Log2(x): # log base 2
	return math.log(x)/math.log(2) 

class ProbabilityMatrix(object):

	#@+others
	#@+node:2::__init__
	#@+body
	def __init__ (self, probability_hash, comments_list, name, pseudo_temperature, base_freq_hash, filename):
		self._probability_hash = probability_hash
		self._site_length = -1
		self._comments_list = comments_list
		self._name = name
		self._filename = filename
		self._R = 1.9858775229213840e-3 # kcal/(mol.K)
		self._pseudo_temperature = pseudo_temperature
		self._pseudoRT = pseudo_temperature * self._R
		self._roomtempRT = 300 * self._R
		self._base_frequency_hash = base_freq_hash
		##-------------------------------------------------------
		dummy_temp = math.e ## dummy_temp can be any positive value other than 1 (which creates a division by zero)
		R = self._R
		self._room_temperature_correction_factor = Log2(dummy_temp)/(R * ln(dummy_temp)) # Log2(dummy_temp)/ln(dummy_temp) * (1/R)
		self._temp_corrected_RT = 300/self._room_temperature_correction_factor
		self._temp_corrected_pseudoRT = self._pseudo_temperature/self._room_temperature_correction_factor
		## self._temp_corrected_RT = 1
		## print "TEMP_SCALING_FACTOR:", self._temp_corrected_RT
		##-------------------------------------------------------
		
		#@<<check site length>>
		#@+node:1::<<check site length>>
		#@+body
		# calculate and check self._site_length
		for base in probability_hash.keys():
			if self._site_length  < 0:
				self._site_length = len(probability_hash[base])
			elif not self._site_length == len(probability_hash[base]):
				raise ProbabilityMatrixError ('Number of probabilities for base: '\
											  + base + ' different from other bases\n') 
		
		#@-body
		#@-node:1::<<check site length>>

		# generate bitvalues
		self._generate_deltaG_matrices()
	
		
		#@<<generate reverse complemented matrices>>
		#@+node:2::<<generate reverse complemented matrices>>
		#@+body
		
		#@-body
		#@-node:2::<<generate reverse complemented matrices>>

								
		
		#@<<test reverse complementation>>
		#@+node:3::<<test reverse complementation>>
		#@+body
		#@+doc
		# 
		# deltaG_matrix_rev_comp2 = 
		# self._generate_deltaG_matrix(self._probability_hash_rev_comp, self._site_length)
		# 
		# ##hashes_different = 0
		# ##for key in deltaG_matrix_rev_comp2:
		# ##	for position in range(len(deltaG_matrix_rev_comp2[key])):
		# ##		if not deltaG_matrix_rev_comp2[key][position] == 
		# self._deltaG_matrix_rev_comp[key][position] :
		# ##			print 'ERROR in reverse complement', key, position
		# ##			hashes_different += 1
		# ##if not hashes_different:
		# ##	print 'Two different reverse complements are identical'
		# #---------------------------
		# self._compare_matrices(deltaG_matrix_rev_comp2, 'deltaG_matrix_rev_comp2',
		# 					   self._deltaG_matrix_rev_comp, 'self._deltaG_matrix_rev_comp')
		# # self._both_cases(deltaG_matrix_rev_comp2)
		# 
		# 
		# # ----------------------------------------------------------------------------------
		# 
		# double_rev_comp_probs = self._reverse_complement_hash(self._probability_hash_rev_comp)
		# double_rev_comp_bits = self._reverse_complement_hash(self._deltaG_matrix_rev_comp)
		# 
		# self._compare_matrices(double_rev_comp_bits, 'double_rev_comp_bits',
		# 					   self._deltaG_matrix, 'self._deltaG_matrix')
		# 
		# ##hashes_different = 0
		# ##for key in double_rev_comp_bits.keys():
		# ##	for position in range(len(double_rev_comp_bits[key])):
		# ##		if not double_rev_comp_bits[key][position] == 
		# self._deltaG_matrix[key][position] :
		# ##			print 'ERROR in double revcomp and forward bitvalues', key, position
		# ##			hashes_different += 1
		# ##if not hashes_different:
		# ##	print 'Double revcomp and forward bitvalues are identical'
		# 
		# self._compare_matrices(double_rev_comp_probs, 'double_rev_comp_probs',
		# 					   self._probability_hash, 'self._probability_hash')
		# 
		# ##hashes_different = 0
		# ##for key in double_rev_comp_probs.keys():
		# ##	for position in range(len(double_rev_comp_probs[key])):
		# ##		if not double_rev_comp_probs[key][position] == 
		# self._probability_hash[key][position] :
		# ##			print 'ERROR in double revcomp and forward probabilities', key, position
		# ##			hashes_different += 1
		# ##if not hashes_different:
		# ##	print 'Double revcomp and forward probabilities are identical'

		#@-doc
		#@-body
		#@-node:3::<<test reverse complementation>>
				
	
		
		#@<<ensure that upper and lower cases are in matrices>>
		#@+node:4::<<ensure that upper and lower cases are in matrices>>
		#@+body
		# fast and easy way to be sure that both upper and lower case bases work
		
		#@-body
		#@-node:4::<<ensure that upper and lower cases are in matrices>>

	#@-body
	#@-node:2::__init__
	#@+node:3::Name
	#@+body
	def _get_name(self): return self._name
	def _set_name(self, value): raise AttributeError, "Can't set 'Name' attribute"
	Name = property(_get_name, _set_name)
	
	#@-body
	#@-node:3::Name
	#@+node:4::Descriptor
	#@+body
	def _get_descriptor(self): return self._name + '\t' + str(self._pseudo_temperature)
	def _set_descriptor(self, value): raise AttributeError, "Can't set 'Descriptor' attribute"
	Descriptor = property(_get_descriptor, _set_descriptor)
	
	#@-body
	#@-node:4::Descriptor
	#@+node:5::FileName
	#@+body
	def _get_filename(self): return self._filename
	def _set_filename(self, value): raise AttributeError, "Can't set 'FileName' attribute"
	FileName = property(_get_filename, _set_filename)
	
	#@-body
	#@-node:5::FileName
	#@+node:6::MD5
	#@+body
	def _get_md5(self):
		string_representation= FlatProbMatrixOutput(self)
		md5_value = md5.new(string_representation).hexdigest()
		return md5_value
	def _set_md5(self, value): raise AttributeError, "Can't set 'MD5' attribute"
	MD5 = property(_get_md5, _set_md5)
	
	#@-body
	#@-node:6::MD5
	#@+node:7::Length
	#@+body
	def _get_length(self): return self._site_length
	def _set_length(self, value): raise AttributeError, "Can't set 'Length' attribute"
	Length = property(_get_length, _set_length)

	def __len__(self):
		return self._site_length
	#@-body
	#@-node:7::Length
	#@+node:8::CommentList
	#@+body
	def _get_comment_list(self): return self._comments_list[:]
	def _set_comment_list(self, value): raise AttributeError, "Can't set 'CommentList' attribute"
	CommentList = property(_get_comment_list, _set_comment_list)
	
	#@-body
	#@-node:8::CommentList
	#@+node:9::ResetPseudoTemp
	#@+body
	def ResetPseudoTemp(self, new_pseudo_temp):
		self._pseudo_temperature = new_pseudo_temp
		self._pseudoRT = new_pseudo_temp * self._R
		self._temp_corrected_psuedoRT = new_pseudo_temp/self._room_temperature_correction_factor
		## self._generate_deltaG_matrices() # UNCOMMENT FOR TEST PSEUDO TEMP

	#@-body
	#@-node:9::ResetPseudoTemp
	#@+node:10::GetPseudoTemp
	#@+body
	def GetPseudoRT(self):
		return self._pseudoRT
	
	def GetPseudoTemp(self):
		return self._pseudo_temperature
	
	#@-body
	#@-node:10::GetPseudoTemp
	#@+node:11::GetPositionInformation
	#@+body
	def GetPositionInformation(self):
		info_list = [0] * self.Length
		for position_index in range(self.Length):
			for base in SEQUENCE_ALPHABET:
				base_prior = self._base_frequency_hash[base]
				base_fraction = self._probability_hash[base][position_index] 
				info_list[position_index] +=  base_fraction * Log2(base_fraction/base_prior)
		return info_list
	

	def GetKaSum(self):
		delta_G_sum = 0
		all_bases = self._deltaG_matrix.keys()
		upper_bases = [base.upper() for base in all_bases]
		bases_dict = dict([(base, None) for base in upper_bases])
		unique_bases =  bases_dict.keys()
		for matrix_position in range(self._site_length):
			for cur_base in unique_bases:
				delta_G_sum += self._deltaG_matrix[cur_base][matrix_position]
		Ka_sum = self.calc_Ka(delta_G_sum) 
		return Ka_sum
		
	
	#@-body
	#@-node:11::GetPositionInformation
	#@+node:12::ResetBaseFrequencies
	#@+body
	def ResetBaseFrequencies(self, base_freq_hash):
		self._base_frequency_hash = base_freq_hash
		self._generate_deltaG_matrices()
	
	#@-body
	#@-node:12::ResetBaseFrequencies
	#@+node:13::SillySortsRemoved
	#@+node:1::GetBestKa
	#@+body
	def GetBestKa(self):
		"""
		returns: a tuple:(best_Ka, best_values, best_sequence)
	
		best_Ka - The highest Ka that is possible for this binding_matrix
		best_values - the individual best value for each position in the matrix (i.e. the per base deltaG values that contribute to the best_Ka)
		best_sequence - the sequence which achieves the best_Ka
		"""
		best_values = [1 - sys.maxint] * self.Length
		best_sequence = [None] * self.Length
		for cur_base in self._deltaG_matrix:
			for index in range(len(self._deltaG_matrix[cur_base])):
				if self._deltaG_matrix[cur_base][index] > best_values[index]:
					best_values[index] = self._deltaG_matrix[cur_base][index]
					best_sequence[index] = cur_base
	
		deltaG_score = reduce(operator.add, best_values, 0)
		best_Ka = self.calc_Ka(deltaG_score)
		return best_Ka, best_values, best_sequence
	#@-body
	#@-node:1::GetBestKa
	#@+node:2::GetKaRange
	#@+body
	def GetKaRange(self):
		
		max_Ka_deltaG_scores = [1 - sys.maxint] * self.Length
		max_Ka_sequence = [None] * self.Length
		
		min_Ka_deltaG_scores = [sys.maxint] * self.Length
		min_Ka_sequence = [None] * self.Length
		
		for cur_base in self._deltaG_matrix:
			for index in range(len(self._deltaG_matrix[cur_base])):
	
				if self._deltaG_matrix[cur_base][index] > max_Ka_deltaG_scores[index]:
					max_Ka_deltaG_scores[index] = self._deltaG_matrix[cur_base][index]
					max_Ka_sequence[index] = cur_base
	
				if self._deltaG_matrix[cur_base][index] < min_Ka_deltaG_scores[index]:
					min_Ka_deltaG_scores[index] = self._deltaG_matrix[cur_base][index]
					min_Ka_sequence[index] = cur_base
	
	
		deltaG_score = reduce(operator.add, max_Ka_deltaG_scores, 0)
		max_Ka = self.calc_Ka(deltaG_score)

		deltaG_score = reduce(operator.add, min_Ka_deltaG_scores, 0)
		min_Ka = self.calc_Ka(deltaG_score)
	
		return max_Ka, max_Ka_deltaG_scores, max_Ka_sequence, min_Ka, min_Ka_deltaG_scores, min_Ka_sequence
	##--------------------------------------------------------------------------------------------	
	def TestFloatError(self, num_tests):
		
		(max_Ka, max_Ka_values, max_Ka_sequence,
		 min_Ka, min_Ka_values, min_Ka_sequence) = self.GetKaRange()
	
		max_errors = []
		min_errors = []
		for count in range(num_tests):
			random.shuffle(max_Ka_values)
			deltaG_score = reduce(operator.add, max_Ka_values, 0)
			ran_max_Ka = self.calc_Ka(deltaG_score)
			max_errors.append(max_Ka - ran_max_Ka)
			#---------------------------------------------
			random.shuffle(min_Ka_values)
			deltaG_score = reduce(operator.add, min_Ka_values, 0)
			ran_min_Ka = self.calc_Ka(deltaG_score)
			min_errors.append(min_Ka - ran_min_Ka)
	
		print 'Max:'
		for error in max_errors:
			print repr(error)
		print 'Min:'
		for error in min_errors:
			print repr(error)
			
		return max_errors, min_errors
	
	#@-body
	#@-node:2::GetKaRange
	#@+node:3::ScoreSequence
	#@+body
##	def ScoreSequence(self, sequence):
##		print '%' * 20, 'ScoreSequence --- SORT', '%' * 20
##		pseudoRT = self._pseudoRT
##		sequence_length = len(sequence)
##		number_of_windows = (sequence_length - self._site_length) + 1
	
##		forward_score_list = Numeric.zeros((number_of_windows,), Numeric.Float)
##		rev_comp_score_list = Numeric.zeros((number_of_windows,), Numeric.Float)
	
##		for cur_window_number in xrange(number_of_windows):
##			for_deltaG_score = 0
##			rev_comp_deltaG_score = 0
##			for matrix_position in range(self._site_length):
##				cur_base = (sequence[(matrix_position + cur_window_number)])
##				for_deltaG_score += self._forward_scoring_hash[cur_base][matrix_position]
##				rev_comp_deltaG_score += self._reverse_scoring_hash[cur_base][matrix_position]
	
##			Ka = math.exp(for_deltaG_score/pseudoRT) # save a function call
##			rev_comp_Ka = math.exp(rev_comp_deltaG_score/pseudoRT)# save a function call
##			forward_score_list[cur_window_number] = Ka
##			rev_comp_score_list[cur_window_number] = rev_comp_Ka
##		return HitList(forward_score_list, rev_comp_score_list, self)
	
	#@-body
	#@-node:3::ScoreSequence
	#@+node:4::FilterScoreSequence
	#@+body
	def FilterScoreSequence(self, sequence, filter_cutoff_ratio=0):
		# print '%' * 20, 'FilterScoreSequence --- SORT', '%' * 20
		pseudoRT = self._pseudoRT # UNCOMMENT FOR OLD PSEUDO TEMP
		## pseudoRT = self._roomtempRT # UNCOMMENT FOR TEST PSEUDO TEMP
		if  filter_cutoff_ratio == 0:
			gamma_cutoff = None
		elif filter_cutoff_ratio < 1:
			raise ProbabilityMatrixError ('filter_cutoff_ratio must be >=1 or 0') 
		else:
			max_Ka, best_values, best_sequence = self.GetBestKa()
			cutoff_Ka = max_Ka/filter_cutoff_ratio
			gamma_cutoff = pseudoRT * math.log(cutoff_Ka)
			if __debug__: print >>sys.stderr, 'Cutoff_Ka', cutoff_Ka, '\nGamma_cutoff', gamma_cutoff
		sequence_length = len(sequence)
		number_of_windows = (sequence_length - self._site_length) + 1
		# forward_list = [0.0] * self._site_length
		# revcomp_list = [0.0] * self._site_length
		forward_score_list = Numeric.zeros((number_of_windows,), Numeric.Float)
		rev_comp_score_list = Numeric.zeros((number_of_windows,), Numeric.Float)
		if filter_cutoff_ratio:
			
			#@<<filter>>
			#@+node:1::<<filter>>
			#@+body
			for cur_window_number in xrange(number_of_windows):
				for_deltaG_score = 0
				rev_comp_deltaG_score = 0

				try:
					for matrix_position in range(self._site_length):
						cur_base = (sequence[(matrix_position + cur_window_number)])
						for_deltaG_score += self._deltaG_matrix[cur_base][matrix_position]
						rev_comp_deltaG_score += self._deltaG_matrix_rev_comp[cur_base][matrix_position]
				except KeyError:
					if cur_base.upper() == NULL_SYMBOL: 
						Ka = rev_comp_Ka = 0
					else:
						raise
				else:
					if for_deltaG_score < gamma_cutoff:
						Ka = 0
					else:		
						Ka = math.exp(for_deltaG_score/pseudoRT) # save a function call

					if rev_comp_deltaG_score < gamma_cutoff:
						rev_comp_Ka = 0
					else:
						rev_comp_Ka = math.exp(rev_comp_deltaG_score/pseudoRT)
			
				forward_score_list[cur_window_number] = Ka
				rev_comp_score_list[cur_window_number] = rev_comp_Ka
			
			#@-body
			#@-node:1::<<filter>>

		else:
			
			#@<<don't filter>>
			#@+node:2::<<don't filter>>
			#@+body
			for cur_window_number in xrange(number_of_windows):
				for_deltaG_score = 0
				rev_comp_deltaG_score = 0
				try:
					for matrix_position in range(self._site_length):
						cur_base = (sequence[(matrix_position + cur_window_number)])
						for_deltaG_score += self._deltaG_matrix[cur_base][matrix_position]
						rev_comp_deltaG_score += self._deltaG_matrix_rev_comp[cur_base][matrix_position]
				except KeyError:
					if cur_base.upper() == NULL_SYMBOL: 
						Ka = rev_comp_Ka = 0
					else:
						raise
				else:
					Ka = math.exp(for_deltaG_score/pseudoRT) # save a function call
					rev_comp_Ka = math.exp(rev_comp_deltaG_score/pseudoRT)
			
				forward_score_list[cur_window_number] = Ka
				rev_comp_score_list[cur_window_number] = rev_comp_Ka
			
			#@-body
			#@-node:2::<<don't filter>>

		return HitList(forward_score_list, rev_comp_score_list, self)
	ScoreSequence = FilterScoreSequence
	#@-body
	#@-node:4::FilterScoreSequence
	#@-node:13::SillySortsRemoved
	#@+node:14::_generate_deltaG_matrix_bits
	#@+body
	#@+doc

	#@-doc
	#@@code
	#--------------------------------------------------------------------------------------------
	def _generate_deltaG_matrices(self):
		probability_hash = self._probability_hash
		site_length = self._site_length
		deltaG_matrix = {}
		for base in probability_hash.keys():
			deltaG_matrix[base] = [None] * site_length
		deltaG_matrix[NO_INFO_SYMBOL] = [None] * site_length
		
		for index in range (site_length):
			deltaG_matrix[NO_INFO_SYMBOL][index] = 0
			for base in probability_hash.keys():
				deltaG_matrix[base][index] = Log2(probability_hash[base][index]/self._base_frequency_hash[base]) * self._temp_corrected_RT # UNCOMMENT FOR OLD PSEUDO TEMP
				## deltaG_matrix[base][index] = Log2(probability_hash[base][index]/self._base_frequency_hash[base]) * self._temp_corrected_pseudoRT # UNCOMMENT FOR TEST PSEUDO TEMP
				## deltaG_matrix[base][index] = ln(probability_hash[base][index]/self._base_frequency_hash[base]) * self._roomtempRT
				## deltaG_matrix[base][index] = ln(probability_hash[base][index]/self._base_frequency_hash[base]) * self._pseudoRT
		# return deltaG_matrix
		##----------------------------------------------------------------------
		##----------------------------------------------------------------------
		self._deltaG_matrix = deltaG_matrix
		self._probability_hash_rev_comp = self._reverse_complement_hash(self._probability_hash)
		self._deltaG_matrix_rev_comp = self._reverse_complement_hash(self._deltaG_matrix)
		self._both_cases(self._deltaG_matrix)
		self._both_cases(self._deltaG_matrix_rev_comp)
	
	#@-body
	#@-node:14::_generate_bitvalue_hash_bits
	#@+node:15::_generate_bitvalue_hash_nats
	#@+body
	#@+doc

	#@-doc
	#@-body
	#@-node:15::_generate_bitvalue_hash_nats
	#@+node:16::_reverse_complement_hash
	#@+body
	def _reverse_complement_hash(self, cur_hash):
		revcomp_hash = {}
		for base in cur_hash.keys():
			# complement_base = complementation_mapping[base]

			# IF a base isn't in the complementation_mapping, it is its own complement
			complement_base = complementation_mapping.get(base, base) 
			revcomp_hash[complement_base] = cur_hash[base][:]
			revcomp_hash[complement_base].reverse()
		return revcomp_hash
	#@-body
	#@-node:16::_reverse_complement_hash
	#@+node:17::_both_cases
	#@+body
	def _both_cases (self, current_hash):
		"""
		_both_cases takes the current hash, and makes sure that the same value is returned, whether
			an uppercase or lowercase base is used to key the hash, it does this by figuring out what
			case the keys are in, and creating duplicate entries with the opposite case
		
		requirements: 1. hash keys must be alphabetical
		              2. The lower case and uppercase versions of the key must have the same meaning				  
		"""
		upper_case = lower_case = 0
		for base in current_hash.keys():
			if not base.isalpha():
				raise ValueError, 'Probability Matrix must be keyed by alphabetic characters'
			elif base.islower():
				lower_case += 1
			elif base.isupper():
				upper_case += 1
			else:
				raise ValueError, 'Probability Matrix keys must be uppercase or lowercase'
				
		if upper_case and lower_case :
			raise ValueError, 'Probability Matrix keys must be uppercase or lowercase'
		elif upper_case:
			for base in current_hash.keys():
				current_hash[base.lower()] = current_hash[base]
		elif lower_case:
			for base in current_hash.keys():
				current_hash[base.upper()] = current_hash[base]
	
	#@-body
	#@-node:17::_both_cases
	#@+node:18::_compare_matrices
	#@+body
	def _compare_matrices(self, matrix_one, matrix_one_name, matrix_two, matrix_two_name):
		hashes_different = 0
		for base in matrix_one.keys():
			for position in range(len(matrix_one[base])):
				if not matrix_one[base][position] == matrix_two[base][position] :
					print 'ERROR:', matrix_one_name, 'and', matrix_two_name, 'are DIFFERENT', base, position, matrix_one[base][position] - matrix_two[base][position]
					hashes_different += 1
		if not hashes_different:
			print matrix_one_name, 'and', matrix_two_name, 'are IDENTICAL'
	
	
	#@-body
	#@-node:18::_compare_matrices
	#@+node:19::calc_Ka
	#@+body
	def calc_Ka(self, gamma):
		## temp_gas_constant - equvalent of RT, R is the gas law constant 1.987 cal/(mol * K) or 8.315 J/(mol * K)
		## T is the temperature
		return math.exp(gamma/self._pseudoRT)  # UNCOMMENT FOR OLD PSEUDO TEMP
		## return math.exp(gamma/self._roomtempRT)  # UNCOMMENT FOR TEST PSEUDO TEMP
	
	#@-body
	#@-node:19::calc_Ka
	#@+node:20::GetProbability
	#@+body
	def GetProbability(self, base, index):
		"""
		returns : the bit value of having "base" in position "index" of a site
		"""
		return self._probability_hash[base][index]
	#@-body
	#@-node:20::GetProbability
	#@+node:21::GetBitValue
	#@+body
	def GetBitValue(self, base, index):
		"""
		returns : the bit value of having "base" in position "index" of a site
		"""
		return self._deltaG_matrix[base][index]
	#@-body
	#@-node:21::GetBitValue
	#@-others



#@<<Matrix Errors>>
#@+node:22::<<Matrix Errors>>
#@+body
#@@code
#@+others
#@+node:1::ProbabilityMatrixError
#@+body
class ProbabilityMatrixError(GomerError):
	pass

#@-body
#@-node:1::ProbabilityMatrixError
#@-others




#@-body
#@-node:22::<<Matrix Errors>>


#@-body
#@-node:0::@file probability_matrix_gomer.py
#@-leo
