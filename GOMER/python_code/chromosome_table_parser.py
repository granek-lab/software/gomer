#! /usr/bin/env python
from __future__ import division
#@+leo
#@comment Created by Leo at Thu Mar  6 17:32:39 2003
#@+node:0::@file chromosome_table_parser.py
#@+body
#@@first
#@@first
#@@language python

"""
*Description:

A Parser for chromosome table files.

That leads to the question - what is a chromosome table file?

The chromsome table file is a way to supply the program with several pieces of information, in an 
organized way.  The chromosome table file supplies the chromosome feature file and the 
chromosome sequence files, and the directory where these files can be found
"""

#@<<imports>>
#@+node:3::<<imports>>
#@+body
##import os
##os.environ['PYCHECKER'] = '--allglobals --stdlib'
##import pychecker.checker

##import check_python_version
##check_python_version.CheckVersion()

import os
import string
import sys
import re

from parser_gomer import Parser
from parser_gomer import ParseError
from indexed import Indexed
from open_file import OpenFile
#@-body
#@-node:3::<<imports>>


#@<<regular expressions>>
#@+node:5::<<regular expressions>>
#@+body
COMMENT_TOKEN = '#'
DIRECTORY_TOKEN = 'Directory:'
FEATURE_FILE_TOKEN = 'Feature file:'
CHROMSOME_TOKEN = 'Chromosome:'
FREQUENCY_TOKEN = 'Frequency:'

comment_re = re.compile('^\s*' + COMMENT_TOKEN + '(.+)')
empty_line_re = re.compile('^\s*$')

directory_re = re.compile('^\s*' + DIRECTORY_TOKEN + '\s*(.+)\s*')
feature_file_re = re.compile('^\s*' + FEATURE_FILE_TOKEN + '\s*(.+)\s*')
chromosome_file_re = re.compile('^\s*' + CHROMSOME_TOKEN + '\s+(\S+)\s+(\S+)\s*(.*)\s*$')
frequency_re = re.compile('^\s*' + FREQUENCY_TOKEN + '\s+(\S+)\s+(\S+)\s*$')





#@-body
#@-node:5::<<regular expressions>>



#@<<doc string>>
#@+node:1::<<doc string>>
#@+body
"""
File Format:
any line which is empty, or contains only white space is ignored
# Comment line 


returns: 

"""
#@-body
#@-node:1::<<doc string>>




#@<<Mitochondrial DNA>>
#@+node:4::<<Mitochondrial DNA>>
#@+body
#@+doc
# 
# 
# At the moment Mitochondrial DNA is being included, this should probably be 
# an option
# 
# In Saccharomyces, Mitochondrial DNA is chromosome 17

#@-doc
#@-body
#@-node:4::<<Mitochondrial DNA>>



#@<<ChromosomeTableParser>>
#@+node:6::<<ChromosomeTableParser>>
#@+body
#@+doc
# 
# class ChromosomeTableParser(Parser):

#@-doc
#@@code
	#@+others
	#@+node:1::__init__
	#@+body
	#@+doc
	# 
	# def __init__ (self):
	# 	Parser.__init__(self)
	# 	self._filehandle = None
	#@-doc
	#@-body
	#@-node:1::__init__
	#@+node:2::def Parse
	#@+body
	#@+doc
	# 
	# def Parse(self):
	# 	"""
	# 	file is closed after parsing
	# 
	# 	returns: a hash, keyed by chromosome_identifier, of Chromosome sequence 
	# file names,
	# 
	# 			and a list of genome feature file names
	# 	"""
	# 
	# 	Parser.Parse(self)  # call the base class method
	# 	directory = ''
	# 	chromosome_file_hash = {}
	# 	chromosome_flags_hash = {}
	# 	feature_file_list = []
	# 	comment_list = []
	# 	frequency_hash = {}
	# 	# for line, line_number in Indexed(self._filehandle):
	# 	# 	split_line = line.rstrip('\n').split('\t')

	#@-doc
	#@@code
		
		#@<<parse table>>
		#@+node:1::<<parse table>>
		#@+body
		#@+doc
		# 
		# line_number = 0
		# for line in self._filehandle:
		# 	line_number += 1
		# 	if empty_line_re.search (line):
		# 		continue
		# 	elif comment_re.search (line) :
		# 		comment_list.append (comment_re.search(line).group(1).rstrip())
		# 	elif directory_re.search(line):
		# 		directory = directory_re.search(line).group(1)
		# 		directory = directory.strip()
		# 	elif feature_file_re.search(line):
		# 		feature_file = feature_file_re.search(line).group(1)
		# 		feature_file = feature_file.strip()
		# 		feature_file_list.append(feature_file)
		# 	elif frequency_re.search(line):
		# 		frequency_match = frequency_re.search(line)
		# 		frequency_base = frequency_match.group(1).strip()
		# 		frequency_value = float(frequency_match.group(2).strip())
		# 		frequency_hash[frequency_base.upper()] = frequency_value
		# 	elif chromosome_file_re.search(line):
		# 		chromosome_file_match = chromosome_file_re.search(line)
		# 		chromosome_label = chromosome_file_match.group(1).strip()
		# 		chromosome_file = chromosome_file_match.group(2).strip()
		# 		chromosome_flags = chromosome_file_match.group(3).strip()
		# 		chromosome_file_hash[chromosome_label] = chromosome_file
		# 		if not empty_line_re.search (chromosome_flags):
		# 			chromosome_flags_hash[chromosome_label] = chromosome_flags.split()
		# 	else :
		# 		raise ChromosomeTableParseError(self._filehandle.name, line, 
		# line_number, 'Line is not valid in a Chromosome Table file')
		# 
		# 

		#@-doc
		#@-body
		#@-node:1::<<parse table>>

		
		#@<<check>>
		#@+node:2::<<check>>
		#@+body
		#@+doc
		# 
		# #if not directory:
		# #	raise ChromosomeTableParseError, 'Chromosome Table must contain a 
		# directory path'
		# 
		# if not feature_file_list:
		# 	raise ChromosomeTableParseError(self._filehandle.name, '', '', 
		# 'Chromsome Table must contain the name of the chromosome feature file')
		# 
		# if not chromosome_file_hash:
		# 	raise ChromosomeTableParseError(self._filehandle.name, '', '', 
		# 'Chromsome Table must contain the names of chromosome sequence files')
		# 
		# 
		# 

		#@-doc
		#@-body
		#@-node:2::<<check>>

	
		
		#@<<generate full path names>>
		#@+node:3::<<generate full path names>>
		#@+body

		##for index in range(len(feature_file_list)):
		##	feature_file_name = os.path.join(directory,  feature_file_list[index])
		####	if os.path.exists(feature_file_name):
		####		feature_file_list[index] = feature_file_name
		####	elif os.path.exists(feature_file_list[index]):
		####		feature_file_name = feature_file_list[index]
		####	else:
		####		raise ChromosomeTableParseError('Problem with feature file path: ' + str(feature_file_list[index] + '\n')	
		##	feature_file_list[index] = feature_file_name
		##@doc
		##This should allow for feature files in different directorys from the chromosomal sequence, although
		##a feature file in the same directory as the sequence, with the same name takes precedence
		##@code
		
		##for chrom in chromosome_file_hash:
		##	chromosome_file_hash[chrom] = os.path.join (directory, chromosome_file_hash[chrom])
			
		
		#@-body
		#@-node:3::<<generate full path names>>


	#@+doc
	# 
	# 	print 'check for a valid path???????'
	# 	self._filehandle.close()
	# 	return feature_file_list, chromosome_file_hash, chromosome_flags_hash, frequency_hash
	# 
	# 
	# 
	# 
	# 

	#@-doc
	#@-body
	#@-node:2::def Parse
	#@-others


#@-body
#@-node:6::<<ChromosomeTableParser>>



#@<<ChromosomeTableParse>>
#@+node:7::<<ChromosomeTableParse>>
#@+body
def ChromosomeTableParse(filename):
	"""
	file is closed after parsing
	
	returns: a hash, keyed by chromosome_identifier, of Chromosome sequence file names,

			and a list of genome feature file names	
	"""
	filehandle = OpenFile(filename, 'Parse Chromosome Table','r')
	directory = ''
	chromosome_file_hash = {}
	chromosome_flags_hash = {}
	feature_file_list = []
	comment_list = []
	frequency_hash = {}
	# for line, line_number in Indexed(self._filehandle):
	# 	split_line = line.rstrip('\n').split('\t')
	
	#@<<parse table>>
	#@+node:1::<<parse table>>
	#@+body
	line_number = 0
	for line in filehandle:
		line_number += 1
		if empty_line_re.search (line):
			continue
		elif comment_re.search (line) :
			comment_list.append (comment_re.search(line).group(1).rstrip())
		elif directory_re.search(line):
			directory = directory_re.search(line).group(1)
			directory = directory.strip()
		elif feature_file_re.search(line):
			feature_file = feature_file_re.search(line).group(1)
			feature_file = feature_file.strip()
			feature_file_list.append(feature_file)
		elif frequency_re.search(line):
			frequency_match = frequency_re.search(line)
			frequency_base = frequency_match.group(1).strip()
			frequency_value = float(frequency_match.group(2).strip())
			frequency_hash[frequency_base.upper()] = frequency_value
		elif chromosome_file_re.search(line):
			chromosome_file_match = chromosome_file_re.search(line)
			chromosome_label = chromosome_file_match.group(1).strip()
			chromosome_file = chromosome_file_match.group(2).strip()
			chromosome_flags = chromosome_file_match.group(3).strip()
			chromosome_file_hash[chromosome_label] = chromosome_file
			if not empty_line_re.search (chromosome_flags):
				chromosome_flags_hash[chromosome_label] = chromosome_flags.split()
		else :
			raise ChromosomeTableParseError(filehandle.name, line, line_number, 'Line is not valid in a Chromosome Table file')
	
	
	
	#@-body
	#@-node:1::<<parse table>>

	
	#@<<check>>
	#@+node:2::<<check>>
	#@+body
	#if not directory:
	#	raise ChromosomeTableParseError, 'Chromosome Table must contain a directory path'
	
	if not feature_file_list:
		# raise ChromosomeTableParseError(filehandle.name, '', '', 'Chromsome Table must contain the name of the chromosome feature file')
		print >>sys.stderr, 'No Chromosome Feature File was found in Chromosome Table'
		
	if not chromosome_file_hash:
		raise ChromosomeTableParseError(filehandle.name, '', '', 'Chromsome Table must contain the names of chromosome sequence files')
		
		
	
	
	#@-body
	#@-node:2::<<check>>


	
	#@<<generate full path names>>
	#@+node:3::<<generate full path names>>
	#@+body
	for index in range(len(feature_file_list)):
		feature_file_name = os.path.join(directory,  feature_file_list[index])
	##	if os.path.exists(feature_file_name):
	##		feature_file_list[index] = feature_file_name
	##	elif os.path.exists(feature_file_list[index]):
	##		feature_file_name = feature_file_list[index]
	##	else:
	##		raise ChromosomeTableParseError('Problem with feature file path: ' + str(feature_file_list[index] + '\n')	
		feature_file_list[index] = feature_file_name

	#@+doc
	# 
	# This should allow for feature files in different directorys from the 
	# chromosomal sequence, although
	# a feature file in the same directory as the sequence, with the same name 
	# takes precedence

	#@-doc
	#@@code

	for chrom in chromosome_file_hash:
		chromosome_file_hash[chrom] = os.path.join (directory, chromosome_file_hash[chrom])
		
	
	#@-body
	#@-node:3::<<generate full path names>>

	
	## print 'check for a valid path???????'
	filehandle.close()
	return feature_file_list, chromosome_file_hash, chromosome_flags_hash, frequency_hash





#@-body
#@-node:7::<<ChromosomeTableParse>>


#@<<Parse Errors>>
#@+node:9::<<Parse Errors>>
#@+body
#@@code
#@+others
#@+node:1::ChromosomeTableParseError
#@+body
class ChromosomeTableParseError(ParseError):
	pass
#@-body
#@-node:1::ChromosomeTableParseError
#@-others




#@-body
#@-node:9::<<Parse Errors>>


#@<<command line>>
#@+node:8::<<command line>>
#@+body
#@<<def run>>
#@+node:1::<<def run>>
#@+body
def run (chromosome_table_file_name=None):
	EXPECTED_ARGUMENTS = 1
	if chromosome_table_file_name :
		print 'run called with parameters'
	elif len(sys.argv) == (EXPECTED_ARGUMENTS + 1):
			chromosome_table_file_name = sys.argv[1]
	else :
		usage()

	if not os.path.exists(chromosome_table_file_name):
		print "File doesn't exist:", chromosome_table_file_name
		sys.exit(1)

	parser = ChromosomeTableParser()
	parser.LoadFile(chromosome_table_file_name)
	feature_file_list, chromosome_file_hash, chromosome_flags_hash = parser.Parse()
	
	print 'Feature Files:', feature_file_list
	print 'Chromosome Files:'
	for label in chromosome_file_hash:
		print label, '\t', '"' + chromosome_file_hash[label] + '"'
		if label in chromosome_flags_hash:
			print label, '\t', chromosome_flags_hash[label]
	


#@-body
#@-node:1::<<def run>>


#@<<def usage>>
#@+node:2::<<def usage>>
#@+body
def usage () :
	print '-'*70 + '\n', 'Arguments given (', len(sys.argv), '):', sys.argv,  """
	1. filename - chromosome table file
	""" + '\n' + '-'*70
	sys.exit(1)

#@-body
#@-node:2::<<def usage>>



#@<<if main>>
#@+node:3::<<if main>>
#@+body
if __name__ == '__main__':
	print os.getcwd()
	run()
	
#@-body
#@-node:3::<<if main>>


#@-body
#@-node:8::<<command line>>


#@<<how to run>>
#@+node:2::<<how to run>>
#@+body
#@+doc
# 
# python chromosome_table_parser.py ~/GOMER/test_files/sequence/cerevisiae_chromosome_table

#@-doc
#@-body
#@-node:2::<<how to run>>
#@-body
#@-node:0::@file chromosome_table_parser.py
#@-leo
