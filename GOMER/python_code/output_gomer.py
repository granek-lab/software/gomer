from __future__ import division
#@+leo
#@comment Created by Leo at Tue Mar 18 14:47:03 2003
#@+node:0::@file output_gomer.py
#@+body
#@@first
#@@language python


"""
*Description:

A base class for parsers.

I'm pretty sure that this is a virtual superclass, but I'm not positive.
"""

#@<<imports>>
#@+node:1::<<imports>>
#@+body
##import os
##os.environ['PYCHECKER'] = '--allglobals --stdlib'
##import pychecker.checker

##import check_python_version
##check_python_version.CheckVersion()

from exceptions_gomer import GomerError

#@-body
#@-node:1::<<imports>>



#@<<Output Errors>>
#@+node:2::<<Output Errors>>
#@+body
#@+doc
# 
# class InputError(Error):
# 	  """Exception raised for errors in the input.
# 
# 	  Attributes:
# 		  expression -- input expression in which the error occurred
# 		  message -- explanation of the error
# 	  """
# 
# 	  def __init__(self, expression, message):
# 		  self.expression = expression
# 		  self.message = message
# 
# class TransitionError(Error):
# 	  """Raised when an operation attempts a state transition that's not
# 	  allowed.
# 
# 	  Attributes:
# 		  previous -- state at beginning of transition
# 		  next -- attempted new state
# 		  message -- explanation of why the specific transition is not allowed
# 	  """
# 
# 	  def __init__(self, previous, next, message):
# 		  self.previous = previous
# 		  self.next = next
# 		  self.message = message
# 

#@-doc
#@@code
#@+others
#@+node:1::OutputError
#@+body
class OutputError(GomerError):
	pass

#@+doc
# 
# 		self.file_name = file_name
# 		self.line = line
# 		self.line_number = line_number

#@-doc
#@@code


#@+doc
# 		print self.message, '\nError encountered in following line (#' \
# 			  + str(self.line_number) + ')', 'from input file:\n', \
# 			  self.file_name, '\n', self.line

#@-doc
#@@code


#@-body
#@-node:1::OutputError
#@-others




#@-body
#@-node:2::<<Output Errors>>


#@-body
#@-node:0::@file output_gomer.py
#@-leo
