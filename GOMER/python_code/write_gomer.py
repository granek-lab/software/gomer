from __future__ import division
#@+leo
#@comment Created by Leo at Tue Mar 18 14:49:37 2003
#@+node:0::@file write_gomer.py
#@+body
#@@first
#@@language python


"""
*Description:

A base class for parsers.

I'm pretty sure that this is a virtual superclass, but I'm not positive.
"""

#@<<imports>>
#@+node:1::<<imports>>
#@+body
##import os
##os.environ['PYCHECKER'] = '--allglobals --stdlib'
##import pychecker.checker

##import check_python_version
##check_python_version.CheckVersion()

from exceptions_gomer import GomerError

#@-body
#@-node:1::<<imports>>



#@<<Write Errors>>
#@+node:2::<<Write Errors>>
#@+body
#@+others
#@+node:1::WriteError
#@+body
class WriteError(GomerError):
	"""Exception raised for formatting errors encountered during parsing files.

	Attributes:
		line -- line of file containing error
		message -- explanation of the error
	"""
	pass

#@+doc
# 
# 		self.file_name = file_name
# 		self.line = line
# 		self.line_number = line_number

#@-doc
#@@code


#@+doc
# 		print self.message, '\nError encountered in following line (#' \
# 			  + str(self.line_number) + ')', 'from input file:\n', \
# 			  self.file_name, '\n', self.line

#@-doc
#@@code
		

#@-body
#@-node:1::WriteError
#@-others




#@-body
#@-node:2::<<Write Errors>>


#@-body
#@-node:0::@file write_gomer.py
#@-leo
