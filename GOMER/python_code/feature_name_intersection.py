#! /usr/bin/env python

import sys
import re
import os
from optparse import OptionParser


from cerevisiae_feature_parser_gomer import FeatureFileParse

comment_line_re = re.compile('^#')
empty_line_re = re.compile('^\s*$')

def parse_file(file_name):
	file_obj = file(file_name)
	values_list = []
	for line in file_obj:
		if not (comment_line_re.match(line) or empty_line_re.match(line)):
			values_list.append(line.strip())
	file_obj.close()
	return values_list

def lookup_names(name_list, feature_dictionary, ambiguous_feature_names):
	names_missing_from_dictionary = []
	ambiguous_names = []
	feature_list = []
	original_name_dict = {}

	for cur_name in name_list:
		cap_cur_name = cur_name.upper()
		if cap_cur_name in feature_dictionary:
			cur_feature = feature_dictionary[cap_cur_name]
			original_name_dict[cur_feature] = cur_name
			feature_list.append(cur_feature)
		elif cap_cur_name in ambiguous_feature_names:
			ambiguous_names.append(cur_name)
		else:
			names_missing_from_dictionary.append(cur_name)
	return feature_list, original_name_dict, names_missing_from_dictionary, ambiguous_names

def features_to_names(feature_list, original_name_dictionary, second_original_name_dictionary={}):
	name_list = []
	for cur_feature in feature_list:
		if cur_feature in original_name_dictionary:
			name_list.append(original_name_dictionary[cur_feature])
		elif cur_feature in second_original_name_dictionary:
			name_list.append(second_original_name_dictionary[cur_feature])
	return name_list
	
def main():

	usage = 'usage: %prog [-h/--help] FILE1 FILE2\n\nFILE1 and FILE2 should have one feature per line.  Leading and trailing whitespace is stripped.'
	parser = OptionParser(usage)
	parser.add_option("-l", "--lookup",metavar="FEATURE_TABLE",
					  help="Use FEATURE_TABLE to look up alternative names for features given in FILE1 and FILE2")

	(options, args) = parser.parse_args()
	if len(args) == 0:
		parser.print_help()
		sys.exit(1)
	if len(args) != 2:
		print >>sys.stderr, ' '.join(sys.argv)
		parser.error("Incorrect number of arguments. Use -h/--help for details and options.")
	
	filename_A = args[0]
	filename_B = args[1]

	basename_A = os.path.basename(filename_A)
	basename_B = os.path.basename(filename_B)
	
	name_list_A = parse_file(filename_A)
	name_list_B = parse_file(filename_B)

	##------------------------------------------------------
	if options.lookup:
		chromosome_hash, feature_dictionary, ambiguous_feature_names,\
						 feature_type_dict = FeatureFileParse(options.lookup,{},{},{},{})


		feature_list_A, original_name_dict_A, names_missing_from_dictionary_A, ambiguous_names_A = lookup_names(name_list_A, feature_dictionary, ambiguous_feature_names)
		feature_list_B, original_name_dict_B, names_missing_from_dictionary_B, ambiguous_names_B = lookup_names(name_list_B, feature_dictionary, ambiguous_feature_names)

		print 'Ambiguous names in', basename_A, ':\n', '\n'.join(ambiguous_names_A), '\n'
		print 'Ambiguous names in', basename_B, ':\n', '\n'.join(ambiguous_names_B), '\n'

		print 'Names in', basename_A, 'not found in feature file:\n', '\n'.join(names_missing_from_dictionary_A), '\n'
		print 'Names in', basename_B, 'not found in feature file:\n', '\n'.join(names_missing_from_dictionary_B), '\n'
	else:
		feature_list_A = name_list_A
		feature_list_B = name_list_B
	##------------------------------------------------------


	## set_A = set(name_list_A)
	## set_B = set(name_list_B)
	set_A = set(feature_list_A)
	set_B = set(feature_list_B)


	feature_intersect = set_A.intersection(set_B)
	feature_union = set_A.union(set_B)
	feature_symmetric_diff = set_A.symmetric_difference(set_B)

	in_A_not_B = set_A.intersection(feature_symmetric_diff)
	in_B_not_A = set_B.intersection(feature_symmetric_diff)

	just = 10

	print '#', str(len(set_A)).ljust(just), 'Total in', basename_A
	print '#', str(len(set_B)).ljust(just), 'Total in', basename_B
	print '' 
	print '#', str(len(feature_intersect)).ljust(just), 'In', 'intersection of', basename_A, 'AND', basename_B
	print '#', 'Fraction of', basename_A, 'in intersection:', len(feature_intersect), '/', len(set_A)
	print '#', 'Fraction of', basename_B, 'in intersection:', len(feature_intersect), '/', len(set_B)
	print '' 
	print '#', str(len(feature_union)).ljust(just), 'In', 'union of', basename_A, 'AND', basename_B
	print '' 
	print '#', str(len(in_A_not_B)).ljust(just), 'In', basename_A, 'NOT', basename_B
	print '#', str(len(in_B_not_A)).ljust(just), 'In', basename_B, 'NOT', basename_A
	print '' 
	

	print '\n# INTERSECTION of', basename_A, 'AND', basename_B, len(feature_intersect)
	name_intersect = features_to_names(feature_intersect, original_name_dict_A)
	print '\n'.join(name_intersect)

	print '\n# UNION of', basename_A, 'AND', basename_B, len(feature_union)
	name_union = features_to_names(feature_union, original_name_dict_A, original_name_dict_B)
	print '\n'.join(name_union)

	print '\n# In', basename_A, 'NOT', basename_B, len(in_A_not_B)
	in_A_not_B_names = features_to_names(in_A_not_B, original_name_dict_A)
	print '\n'.join(in_A_not_B_names)


	print '\n# In', basename_B, 'NOT', basename_A, len(in_B_not_A)
	in_B_not_A_names = features_to_names(in_B_not_A, original_name_dict_B)
	print '\n'.join(in_B_not_A_names)


if __name__ == "__main__":
	main()
