#! /usr/bin/env python
"""
commandline arguements:
     - COORDINATE_FEATURE_FILE
	 - SUBSET_FILE

Generates a new coordinate features file containing only those features in COORDINATE_FEATURE_FILE found in the SUBSET_FILE (the intersection of features whose coordinates are in COORDINATE_FEATURE_FILE and features listed in SUBSET_FILE).

It also outputs the features listed in SUBSET_FILE which are missing from COORDINATE_FEATURE_FILE.
"""

from __future__ import division; division

import sys
import re
from optparse import OptionParser
from integer_string_compare import sort_numeric_string
from coordinate_intersection import parse_coordinate_file

SEQUENCE_TOKEN = '%SEQUENCE'
comment_line_re = re.compile('^#')
tag_line_re = re.compile('^%')
empty_line_re = re.compile('^\s*$')
seq_info_re = re.compile('^' + SEQUENCE_TOKEN + '\t' +
						 '(?P<seq_id>[^\t]+)' + '\t' +
						 '(?P<chrom_label>[^\t]+)' + '\t' +
						 '(?P<md5sum>[^\t]+)' +
						 '\s*$')


def parse_subset_file(filehandle):
	subset_list = []
	subset_dict = {}
	for line in filehandle:
		# for feature_name in line.split(' + '):
		# subset_list.append(feature_name.strip())
		if comment_line_re.match(line) or empty_line_re.match(line):
			continue
		feature_name = line.strip()
		feature_key = feature_name.upper()
		if feature_key not in subset_dict:
			subset_dict[feature_key] = feature_name
			subset_list.append(feature_name)
		else:
			print >>sys.stderr, 'Duplicate found in subset file:', feature_name
	return subset_list

def main():
	version_num = 0.001
	version = '%prog ' + str(version_num)
	usage = 'usage: %prog [options] COORDINATE_FEATURE_FILE SUBSET_FILE\n'
	if __doc__:
		 usage += __doc__
	
	parser = OptionParser(usage=usage, version=version)
	parser.add_option('-o', '--output',
					  action="store",
					  help='Print ')

	parser.add_option("-m", '--missing', action="store_true", default=False,
					  help='Print features in SUBSET_FILE missing from COORDINATE_FEATURE_FILE')


	(options, args) = parser.parse_args()
	NUM_ARGS = 2



	if len(args) == (NUM_ARGS):
		coordinate_feature_filehandle = file(args[0])
		subset_filehandle = file(args[1])
	else :
		parser.print_help()
		sys.exit(2)

	if options.output:
		output_handle = file(options.output, 'w')
	else:
		output_handle = sys.stdout
		
	
	coordinate_dict, coordinate_seqinfo = parse_coordinate_file(coordinate_feature_filehandle)
	subset_list = parse_subset_file(subset_filehandle)

	
	sequence_info_keys = sort_numeric_string(coordinate_seqinfo.keys())

	subset_coordinate_list = []
	subset_missing_from_coordinates = []

	for feature_name in subset_list:
		if feature_name in coordinate_dict:
			subset_coordinate_list.append(coordinate_dict[feature_name])
		else:
			subset_missing_from_coordinates.append(feature_name)


	if options.missing:
		for missing_coord in subset_missing_from_coordinates:
			print >>output_handle, missing_coord

	else:
		for sequence_label in sequence_info_keys:
			print >>output_handle, coordinate_seqinfo[sequence_label]
		print >>output_handle, '\n'

		for coord_line in subset_coordinate_list:
			print >>output_handle, coord_line
		print >>output_handle, '\n'

		
		print >>output_handle, '#', '-'*60
		print >>output_handle, '# Features in subset list, which are missing from coordinate file:'
		print >>output_handle, '#', '-'*60
		for missing_coord in subset_missing_from_coordinates:
			print >>output_handle, '#', missing_coord

if __name__ == '__main__':
	main()
