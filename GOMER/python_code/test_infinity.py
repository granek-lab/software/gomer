#! /usr/bin/env python

from __future__ import division
import re

nan_re = re.compile('nan',re.IGNORECASE)

INFINITY=float('inf')


x = 1e3000
for count in range(10000):
	x *= x


if (INFINITY == x) and (INFINITY <> 10) and (10 <> x):
	detected_infinity = True
else:
	detected_infinity = False

not_a_number = x/x

if nan_re.search(str(not_a_number)):
	detected_nan = True
else:
	detected_nan = False

if detected_infinity and detected_nan:
	print 'Sucessfully detected infinity and NaN.\n\nGOMER should run fine!'
elif detected_infinity:
	print "Sucessfully detected infinity, but didn't detect NaN.\n\nGOMER will probably run fine, but I'm not absolutely sure"
else:
	print "Unable to detected infinity.\n\nGOMER might produce undefined values."
	
