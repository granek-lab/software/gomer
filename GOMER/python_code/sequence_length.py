#! /usr/bin/env python
from __future__ import division
#@+leo
#@+node:0::@file sequence_length.py
#@+body
#@@first
#@@first
#@@language python


"""
*Description:

A small hack - computes the length of the sequence in a FASTA file - probably
only works for FASTA files containing a single sequence
"""

#@<<imports>>
#@+node:1::<<imports>>
#@+body
##import os
##os.environ['PYCHECKER'] = '--allglobals --stdlib'
##import pychecker.checker

import re
import sys

from globals_gomer import SEQUENCE_ALPHABET, NULL_SYMBOL, NO_INFO_SYMBOL
#@-body
#@-node:1::<<imports>>


bases_only_re = re.compile('^[' + SEQUENCE_ALPHABET + NO_INFO_SYMBOL + NULL_SYMBOL + ']+\s*$', re.IGNORECASE)
# empty_line_re = re.compile('^\s*$')

def parse_sequence_read(sequence_file_name):
	sequence_file_handle = file(sequence_file_name)
	header_line = sequence_file_handle.readline()
	sequence = sequence_file_handle.read()
	sequence = sequence.replace('\n','')

	if not bases_only_re.search(sequence):
		raise ValueError, 'Error: Only sequence and empty lines may follow the comment line'
	return sequence, header_line


if __name__ == '__main__':
	sequence_file_name = sys.argv[1]
	sequence, header_line = parse_sequence_read(sequence_file_name)
	print header_line, '\nSequence has', len(sequence), 'base pairs'


#@-body
#@-node:0::@file sequence_length.py
#@-leo
