#! /usr/bin/env python

from __future__ import division

import sys
import os
from optparse import OptionParser
import fpformat

from globals_gomer import OUTPUT_NAME_JUST # , OUTPUT_RANK_JUST
from parse_gomer_output import ParseGomerOutput
from gomer import add_rank_ties_bottom
# from estimate_p_value import MNCP, ROCAUC

handle = sys.stderr

class Feature(object):
	def __init__(self, name, score):
		self.name = name
		self.score = score

def main():
	usage = 'usage: %prog [-h/--help] [-n] GOMER_OUTPUT_1 GOMER_OUTPUT_2'
	usage += '\n       %prog --test GOMER_OUTPUT'
	usage += '\n\n\tFor each feature in the output files, calculate the ratio between values in the two files (unless normalized, ratio is GOMER_OUTPUT_1/GOMER_OUTPUT_2)'
	parser = OptionParser(usage)
	parser.add_option("-n", "--normalize", action="store_true", default=False,
					  help="Normalize change ratios, so that all changes are greater than 1.0 (for values less than 1.0, the reciprocal is used)")


	parser.add_option("-d", "--difference", action="store_true", default=False,
					  help="Calculate the differences between scores, instead of ratio (if -n/--normalize option is given the absolute value of the differnce is calculated)")


	parser.add_option("--test", metavar='GOMER_OUTPUT',
					  help="This outputs the data from GOMER_OUTPUT, without altering it.  This is a test to be sure that generate_ROC_curve.py can correctly parse the output.")
	
	(options, args) = parser.parse_args()
	
	if options.test:
		gomer_output_one = options.test
		gomer_output_handle_one = file(gomer_output_one)

		(all_feature_hash_one,
		 regulated_hash_not_used_one,
		 unknown_feature_hash_not_used_one,
		 gomer_output_skipped_lines_one,
		 param_hash_one) = ParseGomerOutput(gomer_output_handle_one)

		result_string =  generate_feature_data_output(all_feature_hash_one)
		print 'GOMER Output File:     ', gomer_output_one
		print result_string
		sys.exit(2)
	
	elif len(args) != 2:
		print >>sys.stderr, ' '.join(sys.argv)
		parser.error("incorrect number of arguments")

	gomer_output_one = args[0]
	gomer_output_two = args[1]
	gomer_output_handle_one = file(gomer_output_one)
	gomer_output_handle_two = file(gomer_output_two)

	(all_feature_hash_one,
	 regulated_hash_not_used_one,
	 excluded_hash_not_used_one,
	 unknown_feature_hash_not_used_one,
	 gomer_output_skipped_lines_one,
	 param_hash_one) = ParseGomerOutput(gomer_output_handle_one)

	(all_feature_hash_two,
	 regulated_hash_not_used_two,
	 excluded_hash_not_used_two,
	 unknown_feature_hash_not_used_two,
	 gomer_output_skipped_lines_two,
	 param_hash_two) = ParseGomerOutput(gomer_output_handle_two)

	feature_ratio_hash = {}
	##--------------------------------------------------------------------------
	if options.difference:
		for name in all_feature_hash_one:
			feature_one = all_feature_hash_one[name]
			feature_two = all_feature_hash_two[name]
			difference = feature_one.score - feature_two.score
			if options.normalize:
				difference = abs(difference)
			feature_ratio_hash[feature_one.name] = Feature(feature_one.name,difference)
	else:
		for name in all_feature_hash_one:
			feature_one = all_feature_hash_one[name]
			feature_two = all_feature_hash_two[name]
			if options.normalize and (feature_one.score < feature_two.score):
				numerator = feature_two.score
				denominator = feature_one.score
			else:
				numerator = feature_one.score
				denominator = feature_two.score
			if denominator == 0:
				ratio = 0
			else:
				ratio = (numerator/denominator)
			feature_ratio_hash[feature_one.name] = Feature(feature_one.name,ratio)
	##--------------------------------------------------------------------------
	result_string =  generate_feature_data_output(feature_ratio_hash)
	if options.difference:
		if options.normalize:
			print feature_one.score - feature_two.score
			print ''.join(('abs(','(', os.path.basename(gomer_output_one), ')',
						   '-',
						   '(', os.path.basename(gomer_output_two), '))'))
		else:
			print feature_one.score - feature_two.score
			print ''.join(('(', os.path.basename(gomer_output_one), ')',
						   '-',
						   '(', os.path.basename(gomer_output_two), ')'))
	else:
		if options.normalize:
			print 'Normalized'
		else:
			print ''.join(('(', os.path.basename(gomer_output_one), ')',
						   '/',
						   '(', os.path.basename(gomer_output_two), ')'))
	print 'GOMER Output File 1:     ', gomer_output_one
	print 'GOMER Output File 2:     ', gomer_output_two
	print result_string
	
def generate_feature_data_output(feature_hash):
	score_name_feature_list = [(feature.score, feature.name, feature) for feature in feature_hash.values()]
	score_name_feature_rank_list = add_rank_ties_bottom(score_name_feature_list)
	separator = '---=-'*15 + '\n'

	result_string = separator
	result_string += 'All:\n'

	score,name,feature,rank = score_name_feature_rank_list[0]
	OUTPUT_RANK_JUST = len(str(rank))
	for score,name,feature,rank in score_name_feature_rank_list:
		result_string += '\t'.join((str(rank).rjust(OUTPUT_RANK_JUST), name.ljust(OUTPUT_NAME_JUST), repr(score))) + '\n'
	return result_string

		

if __name__ == '__main__':
	main()
