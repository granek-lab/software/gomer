#! /usr/bin/env python
from __future__ import division
#@+leo
#@+node:0::@file flat_prob_matrix_parser_gomer.py
#@+body
#@@first
#@@first
#@@language python

"""
*Description:

Parses a file containing a flat (simple text) matrix of base probabilities, and
returns a ProbabilityMatrix instance representing the values found in the flat
file
"""

#@<<imports>>
#@+node:1::<<imports>>
#@+body
##import os
##os.environ['PYCHECKER'] = '--allglobals --stdlib'
##import pychecker.checker

##import check_python_version
##check_python_version.CheckVersion()

import os
import re
import sys
import operator

# from parser_gomer import Parser
from parser_gomer import ParseError

from indexed import Indexed
from probability_matrix_gomer import ProbabilityMatrix
from flat_prob_matrix_tokens import *
import flat_prob_matrix_printer_gomer
from information import TOLERANCE
from open_file import OpenFile
#@-body
#@-node:1::<<imports>>



#@<<regular expressions>>
#@+node:2::<<regular expressions>>
#@+body
comment_match = re.compile('^\s*' + COMMENT_TOKEN + '(.+)')
name_match = re.compile('^\s*' + NAME_TOKEN + '\s+(.+)')
pseudo_temp_match = re.compile('^\s*' + PSEUDO_TEMP_TOKEN + '\s+(.+)')
base_header_match = re.compile('^[' + BASE_LABEL_STRING + '\s]+$', re.IGNORECASE)
gap_probabilities_match = re.compile('^[' + GAP_PROB_TOKEN + '\s]+$', re.IGNORECASE)
empty_line_match = re.compile('^\s*$')
rough_probabilities_match = re.compile('^\s*\d+[\d\.\-\se]+$')
#@-body
#@-node:2::<<regular expressions>>



#@<<FlatProbMatrixParse>>
#@+node:3::<<FlatProbMatrixParse>>
#@+body
def FlatProbMatrixParse(filename, base_frequency_hash):
	
	#@<<doc string>>
	#@+node:1::<<doc string>>
	#@+body
	"""
	File Format:
	any line which is empty, or contains only white space is ignored
	# Comment line 
	%NAME - Name line
	%PSEUDO_TEMP - pseudo-temperature value - this is the value that is equivalent to RT (Gas law contant * temperature)
	all comment lines are tracked, but if there are multiple %NAME lines,
	, all but the last one are ignored
	
	above are optional, but must come before the base header:
	a   c  g  t   - the order of the bases in the header does not matter,
	                but the order corresponds with the order of the
					probabilities
	.2  .4 .1 .3  - probabilities should sum to one - but they
	                will be rescaled to sum to one, regardless
	
	N   N  N  N   - this is a gap position, the bases don't matter,
	                and don't contribute to the score
	
	returns: 
	
	"""
	#@-body
	#@-node:1::<<doc string>>
	try:
		filehandle = OpenFile(filename, 'Parse Probability Matrix','rU')	
	except IOError:
		filehandle = OpenFile(filename, 'Parse Probability Matrix','r')	
		
	probability_matrix = {}
	comment_list = []
	name = None
	
	#@<<parse header>>
	#@+node:2::<<parse header>>
	#@+body
	## Parse the header section
	pseudo_temp = None
	line_number = 0
	while 1:
		line = filehandle.readline()
		line_number += 1
		if not line :
			raise FlatMatrixParseError \
				  (filehandle.name, line, line_number,\
				   'Parse Error: Reached end of file prematurely:\nno Base Header was found')
		if comment_match.search (line) :
			comment_list.append (comment_match.search(line).group(1).rstrip())
		elif name_match.search (line) :
			name = name_match.search(line).group(1).rstrip()
		elif	pseudo_temp_match.search (line) :
			pseudo_temp = float(pseudo_temp_match.search(line).group(1).rstrip())
		elif empty_line_match.search (line):
			continue
		elif base_header_match.search (line):
			base_label_header = line.rstrip()
			base_label_array = base_label_header.upper().split()
			if not len (base_label_array) == len(BASE_LABEL_STRING) :
				raise FlatMatrixParseError \
					  (filehandle.name, line, line_number,\
					   'Base Header Should contain each of the ' + \
					   str(len(BASE_LABEL_STRING)) + ' bases: ' + ','.join(BASE_LABEL_STRING) +'\n')
			for base in BASE_LABEL_STRING:
				if (base not in base_label_array) :
					raise FlatMatrixParseError \
						  (filehandle.name, line, line_number, \
					 	  'Base Header is  missing the base:' + base + '\n')
			break
		elif rough_probabilities_match.search (line) :
			raise FlatMatrixParseError \
				  (filehandle.name, line, line_number, \
				   'Probabilities Found before Base Header')
		else:
			raise FlatMatrixParseError \
				  (filehandle.name, line, line_number, \
				   'Incorrect line format')
	if empty_line_match.search(name):
		name = 'FILENAME_' + os.path.basename(filehandle.name)
	
	
	#@-body
	#@-node:2::<<parse header>>


	
	#@<<parse probabilities>>
	#@+node:3::<<parse probabilities>>
	#@+body
	## Parse the probabilities section
	
	if not pseudo_temp:
		raise FlatMatrixParseError \
			(filehandle.name, line, line_number, 'PseudoTemperature value is missing from file')
	
	trailing_empty_lines = 0
	line_number = 0
	while 1:
		line = filehandle.readline()
		line_number += 1
		if not line :
			if not probability_matrix:
				raise FlatMatrixParseError \
					  (filehandle.name, line, line_number, \
					   'Parse Error: Reached end of file prematurely:\nno probabilities were found')
			else :
				break
		split_line = line.rstrip('\n').split()
	
		# any empty lines should only occur at the end of the file
		# so we need to keep track to be sure to report an error if
		# empty lines occur in the middle of the file
		if empty_line_match.search (line):
			trailing_empty_lines += 1
			continue
		elif trailing_empty_lines :
			raise FlatMatrixParseError \
					  (filehandle.name, line, line_number, \
					   'Empty lines found in probabilities' + \
					   '\nEmpty lines are only allowed in the header section, or' + \
					   '\ntrailing after all the probabilities, at the end of the file')
		if not (len (split_line) == 4) :
			raise FlatMatrixParseError \
				  (filehandle.name, line, line_number,\
				   'There should be four probabilities per line')
	
		if gap_probabilities_match.search(line) :
			for base in base_label_array:
				probability_matrix.setdefault(base, []).append(0)
		else:
			total_prob = reduce(operator.add, map(float, split_line))
			if abs(1 - total_prob) > TOLERANCE:
				raise FlatMatrixParseError (filehandle.name, line, line_number,
											'Probabilities MUST sum to 1 (within a tolerance of '
											+ str(TOLERANCE) + ')')
			for probability, base_index in Indexed(split_line) :
				try:
					float_probability = float(probability)
				except ValueError:
					raise FlatMatrixParseError \
						  (filehandle.name, line, line_number,\
						   'Probabilities must be floating point numbers')
				if float_probability < 0 :
					raise FlatMatrixParseError \
						  (filehandle.name, line, line_number,\
						   'Probabilities cannot be negative')
				elif float_probability == 0 :
					raise FlatMatrixParseError \
						  (filehandle.name, line, line_number,\
						   'Probabilities cannot be zero')
				cur_base = base_label_array[base_index]
				probability_matrix.setdefault(cur_base, []).append(float_probability)
	
	
	#@-body
	#@-node:3::<<parse probabilities>>


	filehandle.close()
	return ProbabilityMatrix(probability_matrix, comment_list, name,
							 pseudo_temp, base_frequency_hash, filehandle.name)




#@-body
#@-node:3::<<FlatProbMatrixParse>>



#@<<Parser Errors>>
#@+node:4::<<Parser Errors>>
#@+body
#@@code
#@+others
#@+node:1::FlatMatrixParseError
#@+body
class FlatMatrixParseError(ParseError):
	def __init__(self, file_name, line, line_number, message):
		self.message = '\n'.join(('There was a problem parsing the Probability Matrix File (' + file_name + '):',
								  message,
								  'The problem occured on line ' +str(line_number) +':', line
								  ))

##	def __init__(self, file_name, line, line_number, message):
##		self.message = '\n'.join((message,
##								  'Error encountered in following line (#'
##								  +str(line_number) +')'+'from input file:',
##								  file_name, line
##								  ))

#@-body
#@-node:1::FlatMatrixParseError
#@-others




#@-body
#@-node:4::<<Parser Errors>>


#@<<command line>>
#@+node:5::<<command line>>
#@+body
#@<<def run>>
#@+node:1::<<def run>>
#@+body
def run (probability_file_name=None):
	EXPECTED_ARGUMENTS = 1
	if probability_file_name :
		print 'run called with parameters'
	elif len(sys.argv) == (EXPECTED_ARGUMENTS + 1):
			probability_file_name = sys.argv[1]
	else :
		usage()

	if not os.path.exists(probability_file_name):
		print "File doesn't exist:", probability_file_name
		sys.exit(1)

	# parser = FlatProbMatrixParser()
	# parser.LoadFile(probability_file_name)
	# probability_matrix = parser.Parse()

	base_freq_hash = {'A':0.25, 'C':0.25,'G':0.25,'T':0.25}
	probability_matrix = FlatProbMatrixParse(probability_file_name, base_freq_hash)

	printer = flat_prob_matrix_printer_gomer

	print 'Probabilities formatted for file'
	print printer.FlatProbMatrixOutput(probability_matrix)
	print 'Probability Table'
	print printer.FlatProbMatrixOutputTable(probability_matrix, values='probs')
	print 'Bitvalue Table'
	print printer.FlatProbMatrixOutputTable(probability_matrix, values='bits')
	
##	string_array = ['TGgGtTTggacTaACaCTgtgctcctttgactTCgtgCtTGTtGccAatG', 
##		'TgaTGtGCgcTCcTTaatGTTGtCAtaaaggCtcGCTagATtAaAtTGgA',
##		'gtCaGGACgAccCCGTtttTTACtatcAcGTAAccAaTgcAactgTaGAt',
##		'CgaGgcgcTTaaGATggaggtCcaAtaGGAGATtAAcgcaCCCCCgTatg',
##		'gGCCacACAtATtCAGtacccGccCCtaGATaAgAgTGAGggAAgTTgAc',
##		'ccatCCTCCacCGaCaGGGaAcCCAGtatTAGtTTcGCTCGTcaggAaCg',
##		'cATAGcccgAtGAaACtaCAaAAGAcGGTaaaCGgCaCGCATATgGCCcG',
##		'atTgcTcaAGgGggtAgaCtctAgcAatAcaAGcTACCAcCGatGtGGtC',
##		'TAAaAaTgTccCcgTcCcaTGGTACtAACaGGGttcaccTGcaaAaGagT',
##		'AacaCAcgGaACCcGgTagCagTaTagCtcGGTATTtGtcgccaCgTAtt',
##		'GGcGgGtAGCtCtcccaAaGtCtatATcAcTacTGCCGGCaTCTCtaAGt',
##		'TctTGtgTgcGggCGtTCAcTcgtTGcaCTccataGgCgtGaCAAGTAaA',
##		'CTATTAcTTtGAtTcCaTGACtAagCGcAGattACTGcAcaCTAgGTtga']

	string_array = ['TGgGtTTggacTaACaCTgtgctcctttgactTCgtgCtTGTtGccAatG']

	
	for string in string_array:
		# print probability_matrix.ScoreSequence(string), string
		forward, reverse = probability_matrix.ScoreSequence(string)
		forward_string = ' '.join([str(round(val, 3)) for val in forward])
		reverse_string = ' '.join([str(round(val, 3)) for val in reverse])
		print string, '\n', 'forward: ', forward_string, '\n', 'reverse: ', reverse_string, 
	
##	print probability_hash.keys()
##	hash_keys = probability_hash.keys()
##	hash_keys.sort()
##	for key in hash_keys:
##		print key,
##		for probability in probability_hash[key]:
##			print str(round (probability, 3)).ljust(5),
##		print

#@-body
#@-node:1::<<def run>>


#@<<def usage>>
#@+node:2::<<def usage>>
#@+body
def usage () :
	print '-'*70 + '\n', 'Arguments given (', len(sys.argv), '):', sys.argv,  """
	1. filename - flat probability matrix file
	""" + '\n' + '-'*70
	sys.exit(1)

#@-body
#@-node:2::<<def usage>>



#@<<if main>>
#@+node:3::<<if main>>
#@+body
if __name__ == '__main__':
	
	#@<<import>>
	#@+node:1::<<import>>
	#@-node:1::<<import>>

	print os.getcwd()
	run()
#@-body
#@-node:3::<<if main>>


#@-body
#@-node:5::<<command line>>
#@-body
#@-node:0::@file flat_prob_matrix_parser_gomer.py
#@-leo
