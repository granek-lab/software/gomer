#! /usr/bin/env python
from __future__ import division


from global_dictionary import GlobalDictionary
import global_dictionary 

myblah = global_dictionary.BLAH

def dostuff():
	print __name__, 'GlobalDictionary.ONE', GlobalDictionary.ONE, '\n'

def dostuff2():
	print __name__, 'global_dictionary.BLAH', global_dictionary.BLAH, '\n'

def dostuff3():
	print __name__, 'myblah', myblah, '\n'

print ''
print __name__, 'GlobalDictionary.ONE', GlobalDictionary.ONE
print __name__, 'global_dictionary.BLAH', global_dictionary.BLAH
print __name__, 'myblah', myblah
print __name__, 'global_dictionary.RUNTIME', global_dictionary.RUNTIME
print ''
