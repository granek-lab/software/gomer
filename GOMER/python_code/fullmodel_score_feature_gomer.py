from __future__ import division

from globals_gomer import FORWARD, REVCOMP
from integer_string_compare import sort_numeric_string
import sys
import re

nan_re = re.compile('nan',re.IGNORECASE)
INFINITY=float('inf')


def FullModelScoreFeature(feature, site_kappa_model, primary_tf_key, coop_tf_list,
						  competitor_tf_list, free_conc_hash, K_dimer_hash,
						  coop_model_hash, comp_model_hash, probability_matrix_hash):
	##----------------------------------------------------------------
	## Iterate over primary TF sites "i", bound by the protein "X"
	##----------------------------------------------------------------
	# print >> sys.stderr, 'Scoring:', feature.Name, '(', feature.Types, ')'
	cur_coop_model_hash = coop_model_hash.get(primary_tf_key, {})
	cur_comp_model_hash = comp_model_hash.get(primary_tf_key, {})
	coop_tf_list = tuple(coop_tf_list)
	competitor_tf_list = tuple(competitor_tf_list)
	regulatory_region_ranges = site_kappa_model.GetRegulatoryRegionRanges(feature)
	current_chromsome = feature.Chromosome
	hitlist_hash = {}
	tf_md5_hash = {}
	for tf_key in coop_tf_list + competitor_tf_list + (primary_tf_key,):
		tf_md5 = probability_matrix_hash[tf_key].MD5
		hitlist_hash[tf_md5] = {}
		hitlist_hash[tf_md5][FORWARD] = current_chromsome.GetHitList(tf_md5).Forward
		hitlist_hash[tf_md5][REVCOMP] = current_chromsome.GetHitList(tf_md5).ReverseComplement
		tf_md5_hash[tf_key] = tf_md5
	## primary_tf_hitlist = current_chromsome.GetHitList(binding_site_model.Name)
	primary_tf_md5 = tf_md5_hash[primary_tf_key]
	primary_tf_free_conc = free_conc_hash[primary_tf_key]
	# i_sites_denom_product = i_sites_numer_product = 1
	i_sites_product = 1
	for first_i_hitindex, last_i_hitindex in regulatory_region_ranges:
		#---------------------------------------------------------------
		stop_i_hitindex = last_i_hitindex + 1
		for_primary_Ka_list = hitlist_hash[primary_tf_md5][FORWARD][first_i_hitindex :stop_i_hitindex]
		rev_primary_Ka_list = hitlist_hash[primary_tf_md5][REVCOMP][first_i_hitindex :stop_i_hitindex]
		if len(for_primary_Ka_list) <> len(rev_primary_Ka_list):
			raise StandardError, 'Length of forward and revcomp slices should be identical!!!'
		i_hitindices = xrange(first_i_hitindex,stop_i_hitindex)
		for i_index in xrange(len(for_primary_Ka_list)):
			i_hitindex = i_hitindices[i_index]
			# i_for_rev_denom_product = i_for_rev_numer_product = 1
		 	for i_strand, primary_Ka_list  in ((FORWARD, for_primary_Ka_list), (REVCOMP,rev_primary_Ka_list)):
			 	primary_Ka = primary_Ka_list[i_index]
		##-------------------------------------------------------------------
				## FILTER START
				if primary_Ka == 0:
					continue
				## FILTER STOP
				i_site_kappa = site_kappa_model.GetSiteKappa(feature, i_hitindex, i_strand)
				if i_site_kappa == 0:
					continue
				K_a_eff = (primary_Ka * i_site_kappa)
				##==============================================================
				##==============================================================
				##==============================================================
				## COMPETITION TERM!!!!!
				k_competitor_sum = 0
				for comp_tf_key in competitor_tf_list:
					comp_free_conc = free_conc_hash[comp_tf_key]
					comp_model = cur_comp_model_hash[comp_tf_key]
					comp_tf_md5 = tf_md5_hash[comp_tf_key]
					k_sites_sum = 0
					## competitive_region_ranges = site_kappa_model.GetCompetitiveRegionRanges(feature)
## 					competitive_region_ranges = comp_model.GetCompetitiveRegionRanges(i_hitindex,
## 																					  i_strand,
## 																					  feature)
					competitive_slice_tuple = comp_model.GetSlices(i_hitindex, i_strand, feature)
## 					for start_comp_hitindex, stop_comp_hitindex in competitive_region_ranges:
					for start_comp_slice, stop_comp_slice, for_comp_kappa_slice, rev_comp_kappa_slice in competitive_slice_tuple:
						#---------------------------------------------------------------------
						for_comp_Ka_slice = hitlist_hash[comp_tf_md5][FORWARD][start_comp_slice:stop_comp_slice]
						rev_comp_Ka_slice = hitlist_hash[comp_tf_md5][REVCOMP][start_comp_slice:stop_comp_slice]
						#---------------------------------------------------------------------
##--------------------------------------------------------------------------------------------------
## 						for k_hitindex in xrange(start_comp_hitindex, (stop_comp_hitindex + 1)):
## 							k_for_rev_sum = 0
## 							for k_strand in (FORWARD, REVCOMP):
## 								competitor_Ka = hitlist_hash[competitor][k_strand][k_hitindex]
## 								comp_kappa = comp_model.GetSiteKappa(i_hitindex, i_strand,
## 																	 k_hitindex, k_strand,
## 																	 feature)
## 								k_for_rev_sum += (comp_free_conc *
## 												  competitor_Ka *
## 												  comp_kappa)
## 							k_sites_sum += k_for_rev_sum
##--------------------------------------------------------------------------------------------------
						for k_index in range(stop_comp_slice-start_comp_slice):
							for_comp_Ka = for_comp_Ka_slice[k_index]
							for_comp_kappa = for_comp_kappa_slice[k_index]
							k_sites_sum += (comp_free_conc *
											for_comp_Ka *
											for_comp_kappa)
							rev_comp_Ka = rev_comp_Ka_slice[k_index]
							rev_comp_kappa = rev_comp_kappa_slice[k_index]
							k_sites_sum += (comp_free_conc *
											rev_comp_Ka *
											rev_comp_kappa)
##--------------------------------------------------------------------------------------------------
					k_competitor_sum += k_sites_sum
				competition_term = k_competitor_sum
				##==============================================================
				##==============================================================
				##--------------------------------------------------------------
				# i_numerator = one_plus_competition
				# i_denominator = one_plus_competition + (primary_tf_free_conc * K_a_eff)
				one_plus_competition = 1 + competition_term
				# i_product = one_plus_competition/(one_plus_competition + (primary_tf_free_conc * K_a_eff))
				i_sites_product *= one_plus_competition/(one_plus_competition + (primary_tf_free_conc * K_a_eff))
				## cooperativity_ranges = coop_model.GetRegulatoryRegionRanges(feature, i_hitindex)
				##----------------------------------------------------------------
				## Iterate over secondary TF proteins "Y"
				##----------------------------------------------------------------
				# proteins_denom_product = proteins_numer_product = 1
				proteins_product = 1
				for coop_tf_key in coop_tf_list:
					##----------------------------------------------------------------
					## Iterate over secondary TF sites "j" bound by the protein "Y"
					##----------------------------------------------------------------
					K_dimer = K_dimer_hash[primary_tf_key][coop_tf_key]
					coop_tf_free_conc = free_conc_hash[coop_tf_key]
					coop_model = cur_coop_model_hash[coop_tf_key]
					coop_slice_tuple = coop_model.GetSlices(i_hitindex, i_strand, feature)
					coop_tf_md5 = tf_md5_hash[coop_tf_key]
					## cooperativity_ranges = coop_model.GetCoopRanges(i_hitindex, i_strand, feature)
					# j_sites_denom_product = j_sites_numer_product = 1
					# j_sites_product = 1
					## for start_coop_hitindex, stop_coop_hitindex in cooperativity_ranges:
					for start_coop_slice, stop_coop_slice, for_coop_kappa_slice, rev_coop_kappa_slice in coop_slice_tuple:
						for_coop_Ka_slice = hitlist_hash[coop_tf_md5][FORWARD][start_coop_slice:stop_coop_slice]
						rev_coop_Ka_slice = hitlist_hash[coop_tf_md5][REVCOMP][start_coop_slice:stop_coop_slice]
						## print >>sys.stderr, 'start:', start_coop_slice, ', stop:', stop_coop_slice, ', len(for_coop_kappa_slice):', len(for_coop_kappa_slice), ', len(rev_coop_kappa_slice):', len(rev_coop_kappa_slice), ', len(for_second_Ka_slice):', len(for_second_Ka_slice)
						## for j_hitindex in xrange(start_coop_hitindex, (stop_coop_hitindex + 1)):
						## for j_index in range((stop_coop_slice-start_coop_slice)+1):
						for j_index in range(stop_coop_slice-start_coop_slice):
							## j_for_rev_denom_product = 1
							for_coop_Ka = for_coop_Ka_slice[j_index]
							for_coop_kappa = for_coop_kappa_slice[j_index]
							rev_coop_Ka = rev_coop_Ka_slice[j_index]
							rev_coop_kappa = rev_coop_kappa_slice[j_index]
							##------------------------------------------------------------------------
							# j_sites_product *= (one_plus_competition /
							proteins_product *= (one_plus_competition /
												 (one_plus_competition +      # for
												  (primary_tf_free_conc *     # for
												   coop_tf_free_conc *        # for
												   for_coop_kappa * K_dimer   # for
												   * K_a_eff * for_coop_Ka))) # for
							# j_sites_product *= (one_plus_competition /
							proteins_product *= (one_plus_competition /
												 (one_plus_competition +      # rev
												  (primary_tf_free_conc *     # rev
												   coop_tf_free_conc *        # rev
												   rev_coop_kappa * K_dimer   # rev
												   * K_a_eff * rev_coop_Ka))) # rev
							##------------------------------------------------------------------------
							# j_sites_numer_product *= (one_plus_competition)**2 # squared because this is for the for and rev strand
					# proteins_numer_product *= j_sites_numer_product
					# proteins_denom_product *= j_sites_denom_product
					# proteins_product *= j_sites_product
			##------------------------------------------------------------------------
## 			## OLD STYLE
## 				i_for_rev_numer_product *= i_numerator * proteins_numer_product
## 				i_for_rev_denom_product *= i_denominator * proteins_denom_product
## 			i_sites_numer_product *= i_for_rev_numer_product
## 			i_sites_denom_product *= i_for_rev_denom_product
			##------------------------------------------------------------------------
			## NEW STYLE
 				# i_sites_numer_product *= i_numerator * proteins_numer_product
 				# i_sites_denom_product *= i_denominator * proteins_denom_product
				# i_sites_product *= i_product * proteins_product
				i_sites_product *= proteins_product 
			##------------------------------------------------------------------------

## 	if INFINITY == i_sites_numer_product:
## 		if nan_re.search(str(i_sites_numer_product)):
## 			feature_score = float('nan')
## 		else:
## 			feature_score = 0
## 	else:
## 		# feature_score = 1 - (i_sites_numer_product/i_sites_denom_product)
## 		feature_score = 1 - product
	## feature_score = 1 - (i_sites_numer_product/i_sites_denom_product)
	feature_score = 1 - i_sites_product
	return feature_score



##--------------------------------------------------------------------
def fullmodel_score_features(feature_types, chromosome_hash,
							 regulatory_region_model, primary_tf_name,
							 coop_tf_name_list, competitor_tf_list,
							 free_conc_hash, K_dimer_hash, coop_model_hash,
							 comp_model_hash, probability_matrix_hash):
	list_of_score_tuples = []
	sorted_chromosome_label_list = sort_numeric_string(chromosome_hash.keys())
	if __debug__: print >>sys.stderr, 'Full Model Scoring Feature Types:', feature_types
	for chromosome_label in sorted_chromosome_label_list:
		chromosome = chromosome_hash[chromosome_label]
## 		sys.stderr.write ('\r' + ' '*80 + '\r')
## 		print >> sys.stderr, 'Full Model Scoring Features of Chromosome:', chromosome_label,
		if __debug__:
			print >> sys.stderr, 'Full Model Scoring Features of Chromosome:', chromosome_label
		for cur_feature in chromosome.FeatureIterator(feature_types):
			cur_feature_score = FullModelScoreFeature(cur_feature,
													  regulatory_region_model,
													  primary_tf_name, 
													  coop_tf_name_list, 
													  competitor_tf_list,
													  free_conc_hash,
													  K_dimer_hash, 
													  coop_model_hash,
													  comp_model_hash,
													  probability_matrix_hash)
			list_of_score_tuples.append(tuple((cur_feature_score,cur_feature)))
	return list_of_score_tuples
