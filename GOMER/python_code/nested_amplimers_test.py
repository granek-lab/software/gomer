"""
An array of code for the testing the code in parse_primersearch.py which detects and deals with multiple amplimers which are nested (and therefore the innermost nested aplimer is expected to be the only detectable PCR product.
"""
from __future__ import division


import random
import sys
from parse_primersearch import get_nested_multiples, nested_sort
##------------------------------------------------------------------------
##------------------------------------------------------------------------
##------------------------------------------------------------------------

class Blah:
	def __init__(self, left, right, chrom='10'):
		self.left = left
		self.right = right
		self.chrom = chrom
		

def generate_normal_length(n, mean, sd, max_x, min_len=10):
	ends_list = []
	for count in range(n):
		length = 0
		while not (min_len < length < (0.70 * max_x)):
			length = int(random.normalvariate(mean, sd))
		left = random.randint(0, max_x-length)
		right = left + length
		ends_list.append(Blah(left,right))
	return ends_list

def generate_ends(n, min, max, min_len):
	ends_list = []
	for count in range(n):
		left = random.randint(min, max-min_len)
		right = random.randint(left, max)
		ends_list.append(Blah(left,right))
	return ends_list

def generate_nested_ends(max, min_len, max_shortening):
	ends_list = []
	left = 1
	right = max	
	while (right - left > min_len):
		ends_list.append(Blah(left,right))
		left += random.randint(0, max_shortening)
		right -=random.randint(0, max_shortening)
	return ends_list

def print_ends(ends_list):
	return_string = ''
	n = len(ends_list)
	for i in range(len(ends_list)):
		count = n-i
		return_string += str(ends_list[i].left) + '\t' + str(count) + '\n'
		return_string += str(ends_list[i].right) + '\t' + str(count) + '\n\n'
	return return_string

def test_nested_multiples_code(outname_base=None):
	if outname_base:
		unlikely_handle = file(outname_base+'.unlikely', 'w')
		sparse_unlikely_handle = file(outname_base+'.sparse_unlikely', 'w')
		nested_handle = file(outname_base+'.nested', 'w')
	else:
		unlikely_handle = sys.stdout
		sparse_unlikely_handle = sys.stdout
		nested_handle = sys.stdout

	print >>unlikely_handle, '#'*60, '\n# unlikely to be nested\n', '#'*60
	ends_list = generate_ends(100, 1, 50, 10)
	innermost, remaining_multiples = get_nested_multiples(ends_list)
	if innermost:
		print >>unlikely_handle, "## NESTED NESTED NESTED NESTED"
		print >>unlikely_handle, "## innermost"
		print >>unlikely_handle, print_ends([innermost])
		print >>unlikely_handle, "## remaining multiples"
		print >>unlikely_handle, print_ends(remaining_multiples)
	else:
		ends_list = nested_sort(ends_list)
		print >>unlikely_handle, "## not"
		print >>unlikely_handle, print_ends(ends_list)

	print >>sparse_unlikely_handle, '#'*60, '\n# sparse unlikely to be nested\n', '#'*60
	ends_list = generate_ends(100, 1, 500, 10)
	innermost, remaining_multiples = get_nested_multiples(ends_list)
	if innermost:
		print >>sparse_unlikely_handle, "## NESTED NESTED NESTED NESTED"
		print >>sparse_unlikely_handle, "## innermost"
		print >>sparse_unlikely_handle, print_ends([innermost])
		print >>sparse_unlikely_handle, "## remaining multiples"
		print >>sparse_unlikely_handle, print_ends(remaining_multiples)
	else:
		ends_list = nested_sort(ends_list)
		print >>sparse_unlikely_handle, "## not"
		print >>sparse_unlikely_handle, print_ends(ends_list)


	print >>nested_handle, '#'*60, '\n# should be nested\n', '#'*60
	ends_list = generate_nested_ends(100, 5, 3)
	innermost, remaining_multiples = get_nested_multiples(ends_list)
	if innermost:
		print >>nested_handle, "## NESTED NESTED NESTED NESTED"
		print >>nested_handle, "## innermost"
		print >>nested_handle, print_ends([innermost])
		print >>nested_handle, "## remaining multiples"
		print >>nested_handle, print_ends(remaining_multiples)
	else:
		ends_list = nested_sort(ends_list)
		print >>nested_handle, "## not"
		print >>nested_handle, print_ends(ends_list)

	unlikely_handle.close()
	sparse_unlikely_handle.close()
	nested_handle.close()
##------------------------------------------------------------------------
def test_specified_multiples(tuple_list):
	ends_list = []
	for left, right in tuple_list:
		ends_list.append(Blah(left,right))
	sorted_ends_list = nested_sort(ends_list)
	## print '# nested_sort:\n', print_ends(sorted_ends_list)
	dum = sorted_ends_list[0].left
	
	innermost, remaining_multiples = get_nested_multiples(ends_list)
	if innermost:
		print '# innermost:\n', print_ends([innermost])
		print '# remaining_multiples:\n', print_ends([Blah(dum,dum)]+remaining_multiples+[Blah(dum,dum)])
		print >>sys.stderr, 'NESTED'
	else:
		print '# remaining_multiples:\n', print_ends([Blah(dum,dum)]+sorted_ends_list+[Blah(dum,dum)])
		print >>sys.stderr, 'NOT NESTED'
##------------------------------------------------------------------------
##------------------------------------------------------------------------
##------------------------------------------------------------------------
def test_random(n, mean, sd, max_x):
	innermost = None
	count = 0
	while not (innermost):
		endslist = generate_normal_length(n, mean, sd, max_x)
		innermost, remaining_multiples = get_nested_multiples(endslist)
		## innermost = 1
		## count += 1
		## print >>sys.stderr, '\r', count, 
	sorted_endslist = nested_sort(endslist)
	
	print print_ends([Blah(0,0)]+sorted_endslist+[Blah(0,0)])

	if innermost:
		print >>sys.stderr, 'NESTED!!!!'
	else:
		print >>sys.stderr, 'NOT NESTED!!!!'
	##------------------------------------------------
	


if __name__ == '__main__':
	##------------------------------------------------
	argument_string = sys.argv[1]
	test_specified_multiples(eval(argument_string))
	##------------------------------------------------
	## test_random(6, 100, 300, 1000)
	##------------------------------------------------
	## test_nested_multiples_code('/tmp/testnest')
	##------------------------------------------------


"""
  527  ls -1 ../../intergenic_primers/*Coordinates | xargs -n1 -i basename {} | xargs -n1 -i tkdiff {}__13_42__Feb03 {}__14_27__Feb03 &
  528  ls -ltr
  529  rm *__14_27__Feb03
  530  clear
  531  ls -ltr
  532  ls -1 ../../intergenic_primers/*Coordinates | xargs -n1 -i basename {} | xargs -n1 -i tkdiff {}__13_42__Feb03 {}__14_30__Feb03 &
  533  clear
  534  history


  526  python nested_amplimers_test.py "((534564, 535199), (530679,535199), (530679, 531314))"
  527  python nested_amplimers_test.py "((534564, 535199), (530679,535199), (530679, 531314))"
  528  python nested_amplimers_test.py "((2,30),(3,7),(5,19),(9,16))"
 
"""
