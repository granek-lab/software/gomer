#! /usr/bin/env python

from __future__ import division
## from gracePlot import gracePlot
## from grace_plot import GracePlot

"""
The ROC curve:
For two groups of data, group A and group B, The ROC curve shows, the number (or fraction) of members of each group that meet or exceed the threshold.
"""

import re
import sys
import os
import getopt
from optparse import OptionParser
from sets import Set

from grace_plot import GracePlot
from estimate_p_value import MNCP, ROCAUC
from regulated_feature_parser_gomer import ParseRegulatedFeatures
from parse_gomer_output import ParseGomerOutput
from cerevisiae_feature_parser_gomer import FeatureFileParse

handle = sys.stderr

## raise NotImplementedError, "Should be sure that features that are EXCLUDED in the RegulatedFeatures list are actually excluded from the ROC curve"

##--------------------------------------------------------------------
from Numeric import searchsorted,sort


class Feature(object):
	def __init__(self, name):
		self.count = 0
		self.name = name
##--------------------------------------------------------------------
def ThresholdFractions(groupA, groupB, higher_is_better=True):
	"""
	The values in groupA are used as threshold values.

	For each threshold value (unique value in groupA), count the members of each group that meet (or exceed) that threshold.  Then, convert these counts to fraction of members of each group that meet (or beat) the threshold.

	returns:
	    Lists of fractions: groupA_fractions, groupB_fractions.
		Theses lists orrespond, i.e. groupA_fractions[3] is the fraction of members of groupA that meet threshold[3], as groupB_fractions[3] is the fraction of members of groupB that meet threshold[3].
	"""
	
	len_groupA = len(groupA)
	len_groupB = len(groupB)

	## generate a duplicate free list of thresholds from the values in groupA
	threshold_hash = dict(zip(groupA, [None]*len(groupA)))
	threshold_list = threshold_hash.keys()
	threshold_list.sort()
	## threshold_list.reverse()

	groupA_counts = []
	groupB_counts = []

	if higher_is_better:
		for cur_thresh in threshold_list:
			## count the members of each set that are greater than or equal to each threshold value
			groupA_counts.append(len([ x for x in groupA if x >= cur_thresh]))
			groupB_counts.append(len([ x for x in groupB if x >= cur_thresh]))
			## groupA_counts.append(len(filter(lambda x:operator.ge(x,cur_thresh), groupA)))
			## groupB_counts.append(len(filter(lambda x:operator.ge(x,cur_thresh), groupB)))
	else:
		for cur_thresh in threshold_list:
			groupA_counts.append(len([ x for x in groupA if x <= cur_thresh]))
			groupB_counts.append(len([ x for x in groupB if x <= cur_thresh]))

	groupA_counts_with_steps = []
	for count in  groupA_counts:
		groupA_counts_with_steps.append(count)
		groupA_counts_with_steps.append(count)

	groupB_counts_with_steps = []
	# always want to pair the same y value with one less than the next x value
	# we also don't duplicate the last member of groupA, so we skip it below, using a slice
	groupB_counts_with_steps.append(groupB_counts[0])
	for count in groupB_counts[1:]:
		if count == 0:
			groupB_counts_with_steps.append(0)
		else:
			groupB_counts_with_steps.append(count-1)
		groupB_counts_with_steps.append(count)
	groupA_counts_with_steps = groupA_counts_with_steps[:-1]
		
		
	## convert the counts of values meeting the thresholds to fractions of sets
	## that meet the thresholds
	groupA_fractions = [x/len_groupA for x in groupA_counts]
	groupB_fractions = [x/len_groupB for x in groupB_counts]

	groupA_fractions_with_steps = [x/len_groupA for x in groupA_counts_with_steps]
	groupB_fractions_with_steps = [x/len_groupB for x in groupB_counts_with_steps]

	groupA_fractions.sort()
	groupB_fractions.sort()
	groupA_fractions_with_steps.sort()
	groupB_fractions_with_steps.sort()

							  
	return groupA_fractions, groupB_fractions, groupA_fractions_with_steps, groupB_fractions_with_steps
##--------------------------------------------------------------------


def PlotROCCurve(plot, groupA, groupB, stair_step, data_set_name='', higher_is_better=True):
	groupA_fractions, groupB_fractions, groupA_fractions_with_steps, groupB_fractions_with_steps = ThresholdFractions(groupA, groupB, higher_is_better)
	plot.AddDataSetLists([0]+groupB_fractions+[1], [0]+groupA_fractions+[1], data_set_name)

##def OutputROCCurveData(out_handle, groupA, groupB, stair_step, data_set_name='', higher_is_better=True, plot_data_points=True):
##	# groupA is usually the regulated set
##	# groupB is usually the unregulated set
	
##	groupA_fractions, groupB_fractions,\
##					  groupA_fractions_with_steps, \
##					  groupB_fractions_with_steps = ThresholdFractions(groupA, groupB, higher_is_better)
##	print >>out_handle, '##', data_set_name
##	if stair_step:
##		print >>out_handle, '\n'.join([str(x) + '\t' + str(y)
##									   for x,y in
##									   zip([0]+groupB_fractions_with_steps+[1],
##										   [0]+groupA_fractions_with_steps+[1])])
		
##	else:
##		print >>out_handle, '\n'.join([str(x) + '\t' + str(y)
##									   for x,y in
##									   zip([0]+groupB_fractions+[1],
##										   [0]+groupA_fractions+[1])])

##	if plot_data_points:
##		## Include a data set with only the regulated points (the data that forms the curve has other points to make the line pretty)
##		print >>out_handle, '\n', '\n'.join([str(x) + '\t' + str(y)
##											 for x,y in
##											 zip(groupB_fractions,
##												 groupA_fractions)])
##	print >>out_handle, '\n'
	


def NewOutputROCCurveData(out_handle, reg_group, unreg_group,
						  stair_step, plot_points, plot_curve, plot_both,
						  data_set_name=''):
	# groupA is usually the regulated set
	# groupB is usually the unregulated set
	# reg_group and unreg_group are both lists of scores
	if not (plot_points or plot_curve or plot_both):
		plot_both = True

	reg_group = sort(reg_group)
	unreg_group = sort(unreg_group)

	num_reg_lt = searchsorted(reg_group, reg_group)
	num_unreg_lt = searchsorted(unreg_group, reg_group)
	reg_count = len(reg_group)
	unreg_count = len(unreg_group)

	num_reg_ge = [reg_count - num_lt for num_lt in num_reg_lt]
	num_unreg_ge = [unreg_count - num_lt for num_lt in num_unreg_lt]

	
	fraction_reg_ge = [num_ge/reg_count for num_ge in num_reg_ge]
	fraction_unreg_ge = [num_ge/unreg_count for num_ge in num_unreg_ge]

	print >>out_handle, '##', data_set_name
	if stair_step:
		num_unreg_equal = []
		for reg_score, index_in_unreg in zip(reg_group, num_unreg_lt):
			num_equal = 0
			while (index_in_unreg < len(unreg_group)) and (reg_score == unreg_group[index_in_unreg]):
				num_equal += 1
				index_in_unreg += 1
			num_unreg_equal.append(num_equal)


		num_unreg_gt = [num_ge-num_equal for num_ge, num_equal in zip(num_unreg_ge, num_unreg_equal)]
		fraction_unreg_gt = [num_gt/unreg_count for num_gt in num_unreg_gt]

		fraction_unreg_gt.reverse()
		fraction_reg_ge.reverse()
		fraction_unreg_ge.reverse()

		fraction_reg_for_bottom = [0] + fraction_reg_ge[:-1]

		##----------------------------------------------------------------------
		## these lists should be sorted, because the ROC curve is "sorted", if they aren't, there's a problem somewhere
		wrong_order_problem = 0
		for i in range(1, len(fraction_unreg_gt)):
			if fraction_unreg_gt[i-1] > fraction_unreg_gt[i]:
				print 'fraction_unreg_gt[i-1] > fraction_unreg_gt[i]:', fraction_unreg_gt[i-1], fraction_unreg_gt[i]
				wrong_order_problem += 1
				
		for i in range(1, len(fraction_reg_ge)):
			if fraction_reg_ge[i-1] > fraction_reg_ge[i]:
				print 'fraction_reg_ge[i-1] > fraction_reg_ge[i]:', fraction_reg_ge[i-1], fraction_reg_ge[i]
				wrong_order_problem += 1

		for i in range(1, len(fraction_unreg_ge)):
			if fraction_unreg_ge[i-1] > fraction_unreg_ge[i]:
				print 'fraction_unreg_ge[i-1] > fraction_unreg_ge[i]:', fraction_unreg_ge[i-1], fraction_unreg_ge[i]
				wrong_order_problem += 1

		for i in range(1, len(fraction_reg_for_bottom)):
			if fraction_reg_for_bottom[i-1] > fraction_reg_for_bottom[i]:
				print >>sys.stderr, 'fraction_reg_for_bottom[i-1] > fraction_reg_for_bottom[i]:', fraction_reg_for_bottom[i-1], fraction_reg_for_bottom[i]
				wrong_order_problem += 1
		##----------------------------------------------------------------------
		for i in range(len(fraction_unreg_gt)):
			if fraction_unreg_gt[i] > fraction_unreg_ge[i]:
				print 'fraction_unreg_gt[i] > fraction_unreg_ge[i]:', fraction_unreg_gt[i], fraction_unreg_ge[i]
				wrong_order_problem += 1
			if ((i+1) < len(fraction_unreg_gt)) and (fraction_unreg_ge[i] > fraction_unreg_gt[i+1]):
				print 'fraction_unreg_ge[i-1], fraction_unreg_gt[i]:', fraction_unreg_ge[i-1], fraction_unreg_gt[i]
				print 'fraction_unreg_ge[i] > fraction_unreg_gt[i+1]:', fraction_unreg_ge[i], fraction_unreg_gt[i+1], '\n'
				# print 'fraction_unreg_ge[i+1], fraction_unreg_gt[i+2]:', fraction_unreg_ge[i+1], fraction_unreg_gt[i+2], '\n'
				wrong_order_problem += 1
				
		for i in range(len(fraction_reg_for_bottom)):
			if fraction_reg_for_bottom[i] > fraction_reg_ge[i]:
				print >>sys.stderr, 'fraction_reg_for_bottom[i] > fraction_reg_ge[i]:', fraction_reg_for_bottom[i], fraction_reg_ge[i]
				wrong_order_problem += 1
			if ((i+1) < len(fraction_reg_for_bottom)) and  (fraction_reg_ge[i] > fraction_reg_for_bottom[i+1]):
				print >>sys.stderr, 'fraction_reg_ge[i-1], fraction_reg_for_bottom[i]', fraction_reg_ge[i-1], fraction_reg_for_bottom[i]
				print >>sys.stderr, 'fraction_reg_ge[i] > fraction_reg_for_bottom[i+1]', fraction_reg_ge[i], fraction_reg_for_bottom[i+1], '\n'
				# print >>sys.stderr, 'fraction_reg_ge[i+1], fraction_reg_for_bottom[i+2]', fraction_reg_ge[i+1], fraction_reg_for_bottom[i+2], '\n'
				wrong_order_problem += 1

		if wrong_order_problem <> 0 :
			raise "There is a problem with the order, I think its a problem with dealing with ties!!"
		##----------------------------------------------------------------------


		if plot_curve or plot_both:
			print >>out_handle, '\n## stair step ROC curve starting with (0,0) and ending with (1,1)'
			print >>out_handle, '0\t0'
			for index in range(len(fraction_unreg_gt)):
				print >>out_handle, fraction_unreg_gt[index], '\t', fraction_reg_for_bottom[index]
				print >>out_handle, fraction_unreg_ge[index], '\t', fraction_reg_ge[index]
			print >>out_handle, '1\t1'
		##-------------------------------------------------------
		num_reg_ge.reverse()
		num_unreg_ge.reverse()
		num_unreg_gt.reverse()
		print '=' *60
		print "Number of Regulated GE"
##		for num in num_reg_ge:
##			print num, '\t', num/reg_count
		print "Number of Unregulated GE"
##		for num in num_unreg_ge:
##			print num, '\t', num/unreg_count
##		print '=' *60
		print "Number of Unregulated GT"
##		for num in num_unreg_gt:
##			print num, '\t', num/unreg_count
		print '=' *60
		##-------------------------------------------------------

	else:

		fraction_reg_ge.reverse()
		fraction_unreg_ge.reverse()

		if plot_curve or plot_both:
			print >>out_handle, '\n## ROC curve starting with (0,0) and ending with (1,1)'
			print >>out_handle, '\n'.join([str(x) + '\t' + str(y)
										   for x,y in
										   zip([0]+fraction_unreg_ge+[1],
											   [0]+fraction_reg_ge+[1])])
		##-------------------------------------------------------
		num_reg_ge.reverse()
		num_unreg_ge.reverse()
		print '=' *60
		print "Number of Regulated GE"
##		for num in num_reg_ge:
##			print num, '\t', num/reg_count
		print "Number of Unregulated GE"
##		for num in num_unreg_ge:
##			print num, '\t', num/unreg_count
		print '=' *60
		##-------------------------------------------------------

	if plot_points or plot_both:
		## Include a data set with only the regulated points (the data that forms the curve has other points to make the line pretty)
		print >>out_handle, '\n## Actual data points'
		print >>out_handle, '\n'.join([str(x) + '\t' + str(y)
									   for x,y in
									   zip(fraction_unreg_ge,
										   fraction_reg_ge)])
	print >>out_handle, '\n'
	
##  things to check: fraction_reg_ge, fraction_unreg_ge, fraction_unreg_gt, fraction_reg_for_bottom


##------------------------------------------------------------
##------------------------------------------------------------

# from parse_raw_coordinate_file import  ParseRawCoordinates
# from generate_ROC_curve import PlotROCCurve, OutputROCCurveData


filename_re = re.compile('flank(\d+)_tail(\d+)\.')

def upcase_dict_keys(old_dict):
	new_dict = {}
	for key in old_dict:
		new_dict[key.upper()] = old_dict[key]
	return new_dict

def populate_dictionary (feature_dictionary,
						 ambiguous_feature_names,
						 names_to_add):
	"""
	generate a dictionary populated with all the unambiguous aliases for the names in names_to_add
	"""
	alias_dictionary = {}
	missing_names = []
	for name in names_to_add:
		cap_name = name.upper()
		if cap_name in feature_dictionary:
			feature_instance = feature_dictionary[cap_name]
## 			if feature_instance.CommonName:
## 				print feature_instance.CommonName, feature_instance.count
## 			else:
## 				print feature_instance.Name, feature_instance.count
			feature_instance.count = 0
			feature_instance.name = feature_instance.CommonName or feature_instance.Name
			for cur_name in feature_instance.AllNames():
				cap_cur_name = cur_name.upper()
				if cap_cur_name not in ambiguous_feature_names:
					alias_dictionary[cap_cur_name] = feature_instance
		else:
			missing_names.append(name)
	return alias_dictionary, missing_names

def main():
	##--------------------------------------------------------------------------
	usage = 'usage: %prog [-h/--help] [-ds] [-r] GOMER_OUTPUT REGULATED_FILE'
	parser = OptionParser(usage)
	parser.add_option("-d", "--data",metavar="OUTFILE",
					  help="Output data for generating a ROC Curve to OUTFILE, instead of generating it in Grace")
	parser.add_option("-s", "--step", action="store_true", default=False,
					  help="Generate a stair-step style ROC curve")
	parser.add_option("-p", "--points", action="store_true", default=False,
					  help="Show only data points")
	parser.add_option("-c", "--curve", action="store_true", default=False,
					  help="Show only curve (including 0,0 1,1)")
	parser.add_option("-b", "--both", action="store_true", default=False,
					  help="Do both points and curve (default)")
	parser.add_option("--statistics", action="store_true", default=False,
					  help="Only calculate ROCAUC and MNCP, don't generate ROC curve")
	parser.add_option("-a", "--alias",metavar="FEATURE_TABLE",
					  help="Use FEATURE_TABLE to look up aliases for features named in regulated feature file")
	parser.add_option("-n", "--no_diagonal", action="store_true", default=False,
					  help="Don't include the diagonal points (0,0),(1,1) - only applies to --data option")
	
	(options, args) = parser.parse_args()
	if len(args) == 0:
		parser.print_help()
		sys.exit(1)
	if len(args) != 2:
		print >>sys.stderr, ' '.join(sys.argv)
		parser.error("Incorrect number of arguments. Use -h/--help for details and options.")
	gomer_output_filename = args[0]
	regulated_filename = args[1]

##	if options.step:
##		raise NotImplementedError, """There seems to be some problem with this.  When running
		
##		python generate_ROC_curve.py  -s -d /tmp/CAD1_1_12.ROCdata  ~/GOMER/input_files/tests/generate_ROC_curve/CAD1_1_12.long ~/GOMER/input_files/tests/generate_ROC_curve/CAD1_1.gomerset

##		I think there are less points than you would expect for the number of regulated features
##		"""
	##--------------------------------------------------------------------------
	gomer_output_filehandle = file(gomer_output_filename)

	(all_feature_hash,
	 regulated_hash_not_used,
	 excluded_hash,
	 unknown_feature_hash_not_used,
	 gomer_output_skipped_lines,
	 param_hash) = ParseGomerOutput(gomer_output_filehandle)

	(regulated_feature_names,
	 excluded_feature_names,
	 unregulated_feature_names) = ParseRegulatedFeatures(regulated_filename)

##	all_feature_hash = upcase_dict_keys(all_feature_hash)
##	regulated_hash = upcase_dict_keys(regulated_hash)
##	unknown_feature_hash = upcase_dict_keys(unknown_feature_hash)
	##--------------------------------------------------------------------------
	if options.alias:
		chromosome_hash, feature_dictionary, ambiguous_feature_names,\
						 feature_type_dict = FeatureFileParse(options.alias,{},{},{},{})

		for key in all_feature_hash:
			feature = all_feature_hash[key]
			if feature.name in feature_dictionary:
				feature_dictionary[feature.name].rank = feature.rank
				feature_dictionary[feature.name].score = feature.score
				feature_dictionary[feature.name].name = feature.name
				feature_dictionary[feature.name].count = 0
				all_feature_hash[key] = feature_dictionary[feature.name]
			else:
				print >>handle,'Feature in GOMER output not found in feature dictionary:', feature.name
		#-------------------------------------------------------------------
		unregulated_feature_hash, unreg_missing = populate_dictionary(feature_dictionary,
													   ambiguous_feature_names,
													   unregulated_feature_names)
		regulated_feature_hash, reg_missing = populate_dictionary(feature_dictionary,
													 ambiguous_feature_names,
													 regulated_feature_names)
		excluded_feature_hash, exclude_missing = populate_dictionary(feature_dictionary,
													ambiguous_feature_names,
													excluded_feature_names)
		#-------------------------------------------------------------------
		for feature_status, missing_list in [('Regulated',reg_missing),
											 ('Unregulated',unreg_missing),
											 ('Excluded',exclude_missing)]:
			if missing_list:
				print >>handle, feature_status, 'features in regulated feature file, missing from feature table:'
				for name in missing_list:
					print >>handle, name
		#-------------------------------------------------------------------
	else:
		unregulated_feature_hash = dict([(name.upper(), Feature(name.upper())) for name in unregulated_feature_names])
		regulated_feature_hash = dict([(name.upper(), Feature(name.upper())) for name in regulated_feature_names])
		excluded_feature_hash = dict([(name.upper(), Feature(name.upper())) for name in excluded_feature_names])
		
	##--------------------------------------------------------------------------

	regulated_ranks = []
	unregulated_ranks = []
	excluded_ranks = []

	## below we use feature.rank, instead of feature.score because the scores
	## GOMER outputs are rounded, so it will for example print a score of 1.000
	## sometimes when the score is actually less than 1.  The ranks however
	## preserve the relative rank order of features since they are based on
	## unrounded scores.
	
	regulated_check_hash = {}

	for name in all_feature_hash:
		feature = all_feature_hash[name]
		if name in excluded_feature_hash:
			excluded_ranks.append(feature.score)
			excluded_feature_hash[name].count = 1 + excluded_feature_hash[name].count
		elif name in regulated_feature_hash:
			regulated_ranks.append(feature.score)
			regulated_feature_hash[name].count = 1 + regulated_feature_hash[name].count
## 			if feature.name in regulated_check_hash:
## 				raise 'Feature from regulated feature list occurs more than once: ' + feature_name
## 			else:
## 				regulated_check_hash[feature.name.upper()] = feature
		elif name in unregulated_feature_hash:
			## unregulated_feature_hash will be empty unless unregulated features were declared in the "regulated file"
			unregulated_ranks.append(feature.score)
			unregulated_feature_hash[name].count = 1 + unregulated_feature_hash[name].count
		else:
			unregulated_ranks.append(feature.score)
			## raise ', '.join(('Unknown feature name:', name, feature.name))

	
	for feature_status, cur_hash in [('Regulated',regulated_feature_hash),
									 ('Unregulated',unregulated_feature_hash),
									 ('Excluded',excluded_feature_hash)]:
		feature_set = Set(cur_hash.values())
		if [feature for feature in feature_set if feature.count == 0]:
			print >>handle, feature_status, 'Features missing from GOMER output:'
			for feature in feature_set:
				if feature.count == 0:
					print >>handle, feature.name

		if [feature for feature in feature_set if feature.count > 1]:
			print >>handle, feature_status, 'Features occuring multiple times in GOMER output:'
			for feature in feature_set:
				if feature.count > 1:
					print >>handle, feature.name, feature.count


	print >>handle, ' '.join(sys.argv)
	print >>handle, '----------------------------------------------'
	print >>handle, 'Number of All Features:     ', len(all_feature_hash)
	print >>handle, '----------------------------------------------'
	print >>handle, 'Regulated Features Named:   ', len(regulated_feature_hash)
	print >>handle, 'Excluded Features Named:    ', len(excluded_feature_hash)
	print >>handle, 'Unregulated Features Named: ', len(unregulated_feature_hash)
	print >>handle, '----------------------------------------------'
	print >>handle, 'Actual Regulated Features:  ', len(regulated_ranks)
	print >>handle, 'Actual Excluded Featues:    ', len(excluded_ranks)
	print >>handle, 'Actual Unregulated Features:', len(unregulated_ranks)

	# print 'regulated_hash', len(regulated_hash)
	# print 'unknown_feature_hash', len(unknown_feature_hash)
	# print 'gomer_output_skipped_lines', len(gomer_output_skipped_lines)
	## print '\n'.join(gomer_output_skipped_lines)


	if options.statistics:
		pass
	elif options.data:
		if options.data.strip() == "-":
			data_filehandle = sys.stdout
		else:
			data_filehandle = file(options.data, 'w')
		## print "OUTPUT HANDLE:", data_filehandle.name
		## printing data for diagonal
		commandline = ' '.join(sys.argv)
		print >>data_filehandle, '##', commandline
		print >>data_filehandle, '##------------------------------------------'
		print >>data_filehandle, '##', os.path.basename(gomer_output_filename)
		print >>data_filehandle, '## Regulated file:', os.path.basename(regulated_filename)
		print >>data_filehandle, '##------------------------------------------'
		if not options.no_diagonal:
			print >>data_filehandle, '## Diagonal Line'
			print >>data_filehandle, '0\t0\n', '1\t1\n'
		
##		OutputROCCurveData(data_filehandle, regulated_ranks,unregulated_ranks,
##						   options.step, data_set_name='Regulated vs. Unregulated', 
##						   higher_is_better=False)
		NewOutputROCCurveData(data_filehandle, regulated_ranks,unregulated_ranks,
							  options.step, options.points, options.curve, options.both,
							  data_set_name='Regulated vs. Unregulated')
		print >>data_filehandle, '\n'

	else:
		plot = GracePlot(square_plot=1)
		plot.XLabel('Fraction of Unregulated')
		plot.YLabel('Fraction of Regulated')
		subtitle = os.path.basename(gomer_output_filename) 
		plot.Subtitle(subtitle)

		plot.AddDiagonal()

		## higher_is_better=False because we want to use the ranks, not the absolute scores (see above for reason)
		PlotROCCurve(plot, regulated_ranks,unregulated_ranks, options.step,
					 data_set_name='Regulated vs. Unregulated',
					 higher_is_better=False)
		plot.Save(gomer_output_filename+'_ROCcurve.eps', 'eps')
		## plot.Exit()

	print 'GOMER Output File:     ', gomer_output_filename
	print 'Regulated Feature File:', regulated_filename
	# for function in [ROCAUC, MNCP]:
##		print '\t'.join((os.path.basename(gomer_output_filename), 'Regulated vs. Unregulated',
##						 function.__name__,
##						 str(function(regulated_ranks,
##									  unregulated_ranks,
##									  higher_is_better=False))))

##		print '\t'.join((function.__name__,
##						 str(function(regulated_ranks,
##									  unregulated_ranks,
##									  higher_is_better=False))))

	print '\t'.join(('ROCAUC', str(ROCAUC(regulated_ranks, unregulated_ranks, higher_is_better=True))))
	print '\t'.join(('MNCP', str(MNCP(regulated_ranks, unregulated_ranks, higher_is_better=True))))


if __name__ == '__main__':
	main()
