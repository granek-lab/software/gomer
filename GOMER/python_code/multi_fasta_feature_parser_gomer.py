from __future__ import division
#@+leo
#@+node:0::@file multi_fasta_feature_parser_gomer.py
#@+body
#@@first
#@@language python


#@<<imports>>
#@+node:1::<<imports>>
#@+body
import re
import sys
from parser_gomer import ParseError
from sequence_feature_gomer import SequenceFeature
from globals_gomer import SEQUENCE_ALPHABET, NULL_SYMBOL, NO_INFO_SYMBOL
#@+doc
# 
# import os
# os.environ['PYCHECKER'] = '--allglobals --stdlib'
# import pychecker.checker
# 
# 
# import check_python_version
# check_python_version.CheckVersion()
# 
# from parser_gomer import Parser
# from parser_gomer import ParseError
# from globals_gomer import SEQUENCE_ALPHABET
# from sequence_gomer import Sequence
# import roman

#@-doc
#@-body
#@-node:1::<<imports>>


#@<<regular expressions>>
#@+node:2::<<regular expressions>>
#@+body
# SEQUENCE_ALPHABET = 'acgtACGT'

COMMENT_TOKEN = '>'
COMMENT_SEPARATOR = ';'
PAIR_SEPARATOR = '='
comment_re = re.compile('^' + COMMENT_TOKEN + '(.+)$')
empty_line_re = re.compile('^\s*$')
#sequence_re = re.compile('^([' + SEQUENCE_ALPHABET + ']+)\s*$')
sequence_re = re.compile('^[' + SEQUENCE_ALPHABET + NULL_SYMBOL + NO_INFO_SYMBOL + ']+\s*$', re.IGNORECASE)

FREQUENCY_TOKEN = 'Frequency:'
frequency_re = re.compile('^\s*' + FREQUENCY_TOKEN + '\s+(\S+)\s+(\S+)\s*$')


#@-body
#@-node:2::<<regular expressions>>



#@+others
#@+node:3::def ParseFASTASequenceFeatureFile
#@+body
def ParseFASTASequenceFeatureFile(sequence_file_name, feature_hash={}, feature_list=[], pseudo_chrom_hash={}):
	file_handle = file(sequence_file_name)
	cur_line = ' '
	line_count = 0
	frequency_hash = {}
	## print >> sys.stderr, 'FILE NAME', file_handle.name, '\nSUPPLIED NAME', sequence_file_name
	while cur_line:
		previous_file_pos = file_handle.tell()
		cur_line = file_handle.readline()
		line_count += 1
##******************************************************************************
		if frequency_re.search(cur_line):
			frequency_match = frequency_re.search(cur_line)
			frequency_base = frequency_match.group(1).strip()
			frequency_value = float(frequency_match.group(2).strip())
			frequency_hash[frequency_base.upper()] = frequency_value
##******************************************************************************
		elif empty_line_re.match(cur_line):
			continue
		elif comment_re.match(cur_line):
			comment = comment_re.search(cur_line).group(1)
			feature_name, info_hash = _parse_comment(comment)
			sequence_list = []
			# start parsing sequence that goes with the comment line just found
			while cur_line:
				previous_file_pos = file_handle.tell()
				cur_line = file_handle.readline()
				line_count += 1
				if sequence_re.match(cur_line):
					sequence_list.append(cur_line.strip())
					
					#@<<Faster?ParseSequence>>
					#@+node:1::<<Faster?ParseSequence>>
					#@+body
					#@+doc
					# 
					# index = 0
					# NUM_BLOCKS = 50
					# sequence = ['']*NUM_BLOCKS
					# while cur_line:
					# 	sequence_match = sequence_re.search(cur_line)
					# 	if sequence_match:
					# 		if index >= len(sequence):
					# 			sequence.extend(['']*NUM_BLOCKS)
					# 		sequence[index] =  sequence_match.group(1)
					# 		index += 1
					# 		cur_line = sequence_file_iterator.readline()
					# 
					# 	elif empty_line_re.search(cur_line): # allow for empty 
					# lines at end of file
					# 		cur_line = sequence_file_iterator.readline()
					# 		break
					# 	else:
					# 		raise SequenceParseError (sequence_file_iterator.filename(),
					# 							  cur_line,
					# 							  sequence_file_iterator.lineno(),
					# 							  'Error: Only sequence and empty lines may 
					# follow the comment line')

					#@-doc
					#@-body
					#@-node:1::<<Faster?ParseSequence>>

				elif empty_line_re.match(cur_line) or comment_re.match(cur_line):
					if not sequence_list:
						raise SequenceFeatureParseError(sequence_file_name,
												 cur_line, line_count,
												 'ERROR: no sequence provided for:' + feature_name)
					elif feature_name.upper() in feature_hash:
						## print >>sys.stderr, 'feature_hash:', feature_hash
						raise SequenceFeatureParseError(sequence_file_name,
												 cur_line, line_count,
												 'ERROR: duplicated feature name "' + feature_name + '"')
					sequence = ''.join(sequence_list)
					new_feature = SequenceFeature(feature_name, info_hash, sequence)
					feature_hash[feature_name.upper()] = new_feature
					pseudo_chrom_hash[feature_name] = new_feature
					feature_list.append(new_feature)
					file_handle.seek(previous_file_pos)
					line_count -= 1
					break
				else:
					raise SequenceFeatureParseError(sequence_file_name,
													cur_line, line_count,
													"ERROR: File doesn't match format")
		else:
			raise SequenceFeatureParseError(sequence_file_name,
												 cur_line, line_count,
												 "ERROR: File doesn't match format")
	file_handle.close()
	ambiguous_feature_names = {} # there shouldn't be any ambiguous names, the parser won't allow it
	return feature_hash, feature_list, frequency_hash, pseudo_chrom_hash, ambiguous_feature_names
#@-body
#@-node:3::def ParseFASTASequenceFeatureFile
#@+node:4::def _parse_comment
#@+body
def _parse_comment(comment_string):
	split_string = comment_string.split(COMMENT_SEPARATOR)
	name = split_string.pop(0).strip()
	info_hash = {}
	for info_pair in split_string:
		info_pair = info_pair.strip()
		label, value = info_pair.split(PAIR_SEPARATOR)
		info_hash[label.lower()] = value
	return name, info_hash

#@-body
#@-node:4::def _parse_comment
#@+node:5::SequenceFeatureParseError
#@+body
class SequenceFeatureParseError(ParseError):
	pass
#@-body
#@-node:5::SequenceFeatureParseError
#@-others


#@-body
#@-node:0::@file multi_fasta_feature_parser_gomer.py
#@-leo
