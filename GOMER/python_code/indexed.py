#! /usr/bin/env python
from __future__ import division
from __future__ import generators
#@+leo
#@+node:0::@file indexed.py
#@+body
#@@first
#@@first
#@@first
#@@language python

"""
*Description:

This was taken from the Python Cookbook.  It allows one to iterate over elements of a list,
in parallel with iterating over an index.
"""
##import os
##os.environ['PYCHECKER'] = '--allglobals --stdlib'
##import pychecker.checker

##import check_python_version
##check_python_version.CheckVersion()


import sys

#BEGIN - from Python Cookbook
# indices =xrange(sys.maxint)

def Indexed(sequence):
    indices =xrange(sys.maxint)
    iterator = iter(sequence)
    for index in indices:
        yield iterator.next(), index
    # Note that we exit by propagating StopIteration when .next raises it!
#END - from Python Cookbook
#@-body
#@-node:0::@file indexed.py
#@-leo
