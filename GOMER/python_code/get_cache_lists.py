#@+leo
#@comment Created by Leo at Thu Jul 10 14:36:10 2003
#@+node:0::@file get_cache_lists.py
#@+body
#@+doc
# 
# $ grep RME1 chromosomal_feature.tab
# YGR044C RME1    CSP1    ORF     7       583886  582984  C       
# S0003276        L0001648        zinc finger prote
# # cache_file_handler =  CacheFileHandler('C:/josh/GOMER_data/window_cache/paper_gold.probs_cache_0')
# 
# >>> from get_cache_lists import get_cache_lists
# >>> forward, reverse = 
# get_cache_lists('C:/josh/GOMER_data/window_cache/paper_gold.probs_cache_0', '7')
# 
# >>> for_range = forward[580000:588000]
# >>> rev_range = reverse[580000:588000]
# >>> for_list = [(for_range[index], index + 580001, 'f') for index in xrange(len(for_range))]
# >>> rev_list = [(rev_range[index], index + 580001, 'r') for index in xrange(len(rev_range))]
# >>> joint_list = for_list + rev_list
# >>> joint_list.sort()
# >>> print '\n'.join([str(triple) for triple in joint_list[-20:]])
# (1.3293002730630223, 582112, 'f')
# (1.3522912186264817, 582296, 'r')
# (1.4351006613330886, 581433, 'r')
# (1.696963053070385, 580700, 'f')
# (2.0655074601070145, 583219, 'f')
# (2.9181302484564493, 584033, 'f')
# (4.0572333217374128, 584926, 'r')
# (6.2982680299912577, 585480, 'r')
# (6.3990811221198793, 584840, 'r')
# (7.5225671265448621, 584494, 'f')
# (13.866158684694343, 582382, 'r')
# (14.310930286352212, 584474, 'r')
# (27.821622973339725, 581439, 'f')
# (28.393647180371712, 584639, 'f')
# (42.623780984721542, 580446, 'f')
# (45.986378315269263, 581964, 'r')
# (167.08123153415812, 584963, 'f')
# (173.48944746518646, 585710, 'r')
# (31923.151737454198, 583016, 'f')
# (166759.08923391954, 584568, 'f')
# >>>
# 

#@-doc
#@@code

from cache_window_list_gomer import CacheWindowList
from cache_file_handler_gomer import CacheFileHandler

def get_cache_lists(cache_file_name, chrom_label, fast_cache_directory):
	cache_file_handler = CacheFileHandler(cache_file_name, fast_cache_directory)
	cache_handle = cache_file_handler.GetHandle()
	
	cur_cache_chromosome = cache_file_handler.GetChrom(chrom_label)

	num_windows = cur_cache_chromosome.GetNumWindows()
	forward_offset = cur_cache_chromosome.GetForwardOffset()
	revcomp_offset = cur_cache_chromosome.GetRevCompOffset()
		
	foward_cache_list = CacheWindowList(cache_handle, num_windows, forward_offset)
	revcomp_cache_list = CacheWindowList(cache_handle, num_windows, revcomp_offset)
	return foward_cache_list, revcomp_cache_list
#@-body
#@-node:0::@file get_cache_lists.py
#@-leo
