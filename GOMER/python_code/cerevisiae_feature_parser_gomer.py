#! /usr/bin/env python
from __future__ import division
#@+leo
#@+node:0::@file cerevisiae_feature_parser_gomer.py
#@+body
#@@first
#@@first
#@@language python


"""
*Description:

A class for parsing feature files for the Saccharomyces cerevisiae genomes.  
Perhaps this shouldn't be a class, but I guess its too late now.

The main method of this class is, of course the Parse method, which returns a
hash of chromosomes (keyed by their labels), and a feature dictionary.  The
Parse function adds an instance for each of the features found in the feature
file into the appropriate chromosome.
The feature dictionary is used to find any of the features, by any of its names (many features have
several different names, and several different types of names.)
"""

#@<<imports>>
#@+node:4::<<imports>>
#@+body
##import os
##os.environ['PYCHECKER'] = '--allglobals --stdlib'
##import pychecker.checker

##import check_python_version
##check_python_version.CheckVersion()

import os
import string
import sys

from parser_gomer import Parser
from parser_gomer import ParseError
from indexed import Indexed
from chromosome_gomer import Chromosome
from genome_feature_gomer import GenomeFeature
from orf_feature_gomer import OrfFeature
from open_file import OpenFile



# import const
#@-body
#@-node:4::<<imports>>



#@<<constants>>
#@+node:5::<<constants>>
#@+body
NUMBER_CEREVISIAE_FEATURE_FILE_ELEMENTS_PER_LINE = 17
## KNOWN_FEATURES = ['LTR', 'rRNA', 'snRNA', 'Transposon', 'RNA', 'ARS', 'Pseudogene', 'CEN', 'ORF', 'snoRNA', 'tRNA', 'Ty ORF']
## DEFAULT_FEATURE = 'ORF'
## DEFAULT_FEATURES = ['Uncharacterized', 'Verified']
DEFAULT_FEATURES = ['ORF']
ORGANISM = 'Saccharomyces cerevisiae'
strand_mapping = {'W':'+', 'w':'+', 'C':'-', 'c':'-', '':''}

#@-body
#@-node:5::<<constants>>



#@+doc
# 
# class CerevisiaeFeatureParser(Parser):
# 	@others

#@-doc
#@@code
#@+others
#@+node:1::Mitochondrial DNA
#@+body
#@+doc
# 
# 
# At the moment Mitochondrial DNA is being included, this should probably be 
# an option
# 
# In Saccharomyces, Mitochondrial DNA is chromosome 17

#@-doc
#@-body
#@-node:1::Mitochondrial DNA
#@+node:2::CerevisiaeFeatureParse
#@+body
def FeatureFileParse(filename, chromosome_hash, feature_dictionary,
						   ambiguous_feature_names, feature_type_dict):
	"""
	file is closed after parsing
	
	returns: a hash, keyed by chromosome_identifier, of Chromosome instances,
	         to which all the features encoded in the gene feature file have
			 been added
			 
			 a dictionary - containing references to all the features in the 
			 				genome, keyed by all the possible names for the feature.
							It should be noted that all the keys are converted to uppercase
							to make it easier to find entries
	
	"""
	## print ((('*' * 50) + '\n') * 8), 'MITOCONDRIAL DNA IS CHROMOSOME 17', ((('*' * 50) + '\n') * 8)
	## filehandle = file(filename, 'r')	
	filehandle = OpenFile(filename, 'Feature File', 'r')	
	##Parser.Parse(self)  # call the base class method
	# genome_features = {}

#@+doc
# 
# 	chromosome_hash = {}
# 	feature_dictionary = {}
# 	ambiguous_feature_names = {}

#@-doc
#@@code	
	# feature_dict = {
	# 	"ORF": OrfFeature
	# 	}
	for line, line_number in Indexed(filehandle):
		split_line = line.rstrip('\n').split('\t')
		if not (len (split_line) == NUMBER_CEREVISIAE_FEATURE_FILE_ELEMENTS_PER_LINE):
			raise FeatureParseError (filehandle.name, line, line_number, \
									  'Cerevisiae feature file should have ' \
									  + str(NUMBER_CEREVISIAE_FEATURE_FILE_ELEMENTS_PER_LINE) + \
									  ' elements, not ' + str(len(split_line)) + ' elements')
		
		
		(feature, gene, alias, feature_type, chromosome_identifier,\
		startcoord, stopcoord, strand, sgdid, secondary_sgdid,\
		description, date, feature_version, genetic_position,\
		enzyme, gene_name_reservation_date,\
		is_gene_name_standard_gene_name) = split_line

		# The following is a check for correctness of the feature file
		# all of the following are mandatory as defined by SGD
		if feature_type == 'Not physically mapped':
			continue
		elif feature_type == 'Not in systematic sequence of S288C':
			continue
		elif not feature :
			raise FeatureParseError (filehandle.name, line, line_number,\
									 'Mandatory Element: Feature Missing')
		elif not feature_type :
			raise FeatureParseError (filehandle.name, line, line_number,\
									 'Mandatory Element: FeatureType Missing')
		elif not chromosome_identifier :
			raise FeatureParseError (filehandle.name, line, line_number,\
									 'Mandatory Element: Chromosome_identifier Missing')
		elif not startcoord :
			raise FeatureParseError (filehandle.name, line, line_number,\
									 'Mandatory Element: StartCoord Missing')
		elif not stopcoord :
			raise FeatureParseError (filehandle.name, line, line_number,\
									 'Mandatory Element: StopCoord Missing')
		elif not strand and feature_type not in ('ARS', 'CEN'):
			raise FeatureParseError (filehandle.name, line, line_number,\
									 'Mandatory Element: Strand Missing')
		elif not sgdid :
			raise FeatureParseError (filehandle.name, line, line_number,\
									 'Mandatory Element: SGDID Missing')
		elif not date :
			raise FeatureParseError (filehandle.name, line, line_number,\
									 'Mandatory Element: Date Missing')
		elif not feature_version :
			raise FeatureParseError (filehandle.name, line, line_number,\
									 'Mandatory Element: Feature_version Missing')


		#*******
		# We need to further process some of the raw text from the feature table so that it is
		# suitable for use in the feature class
		#*******

		# We need to split up all the elements that can have multiple values
		# into lists of strings
		alias_list = alias.split('|')
		feature_type_list = feature_type.split('|')
		secondary_sgdid_list = secondary_sgdid.split('|')
		current_chromosome = chromosome_hash.setdefault(chromosome_identifier, \
														Chromosome(chromosome_identifier))
		# Convert strand from W to + and C to -
		strand = strand_mapping[strand]
		
##---------------------------------------------------------------
##		for feature_type in feature_type_list:
##			if feature_type not in KNOWN_FEATURES:
##				raise FeatureParseError, (filehandle.name, line, \
##										  line_number, 'Feature Type Unknown: ' + feature_type)
##---------------------------------------------------------------

		if 'ORF' in map(string.upper, feature_type_list):
			new_feature = OrfFeature(feature, gene, alias_list,\
						   sgdid, secondary_sgdid_list,\
						   feature_type_list,\
						   startcoord, stopcoord, strand,\
						   description, chromosome_identifier, current_chromosome)
		else:
			new_feature = GenomeFeature(feature, gene, alias_list,\
						   sgdid, secondary_sgdid_list,\
						   feature_type_list,\
						   startcoord, stopcoord, strand,\
						   description, chromosome_identifier, current_chromosome)

##			self._add_names_to_dictionary (feature_dictionary,
##										   ambiguous_feature_names,
##										   new_feature,
##										   feature, gene, alias_list, sgdid,
##										   secondary_sgdid_list)
		for feat_type in feature_type_list:
			feature_type_dict[feat_type] = feature_type_dict.get(feat_type, 0) + 1

		_add_names_to_dictionary (feature_dictionary,
								  ambiguous_feature_names,
								  new_feature)
		current_chromosome.AddFeature(new_feature)
	filehandle.close()
	return chromosome_hash, feature_dictionary, ambiguous_feature_names, feature_type_dict

#@+doc
# 
# The columns are:
# 
# 1) Feature (mandatory)
# 2) Gene (optional)
# 3) Alias (optional, multiples separated by |)
# 4) FeatureType (mandatory, multiples separated by |)
# 5) Chromosome (mandatory)
# 6) StartCoord (mandatory)
# 7) StopCoord (mandatory)
# 8) Strand (mandatory)
# 9) SGDID (mandatory)
# 10) SecondarySGDID (optional, multiples separated by |)
# 11) Description (optional)
# 12) Date (mandatory)
# 13) Feature_version (mandatory)
# 14) Genetic position (optional)
# 15) Enzyme (optional)
# 16) Gene name reservation date (optional)
# 17) Is the gene name the standard gene name? (optional, either Y or N)

#@-doc
#@-body
#@-node:2::CerevisiaeFeatureParse
#@+node:3::_add_names_to_dictionary
#@+body
##-------------------------------------------------------------------------
def _add_names_to_dictionary (feature_dictionary,
							  ambiguous_feature_names,
							  feature_instance):
	for cur_name in feature_instance.AllNames():
		cap_cur_name = cur_name.upper()
##			feature_dictionary[cap_cur_name] = feature_instance
		if cap_cur_name in ambiguous_feature_names:
			## if one of the names of the instance is already in the ambiguous hash
			ambiguous_feature_names[cap_cur_name].append(feature_instance)
			feature_instance.SetUnambiguousNames(ambiguous_feature_names)
		elif cap_cur_name in feature_dictionary:
			if feature_dictionary[cap_cur_name] == feature_instance:
				## Sometimes one of the alternate names is the same as the common name, that's OK 
				continue
			else:
				## Otherwise, if the current name is in the feature dictionary, we have a new ambiguous name
				value = feature_dictionary[cap_cur_name]
				del feature_dictionary[cap_cur_name]
				ambiguous_feature_names[cap_cur_name] = [value, feature_instance]
				
				value.SetUnambiguousNames(ambiguous_feature_names)
				feature_instance.SetUnambiguousNames(ambiguous_feature_names)

		else:
			feature_dictionary[cap_cur_name] = feature_instance

##-------------------------------------------------------------------------
##	def _add_names_to_dictionary (self, feature_dictionary,
##								  ambiguous_feature_names,
##								  feature_instance,
##								  primary_name, gene, alias_list, sgdid,
##								  secondary_sgdid_list):
##		feature_dictionary[primary_name.upper()] = feature_instance
##		if gene:
##			feature_dictionary[gene.upper()] = feature_instance
##		for alias in alias_list:
##			feature_dictionary[alias.upper()] = feature_instance
##		feature_dictionary[sgdid.upper()] = feature_instance
##		for second_sgdid in secondary_sgdid_list:
##			feature_dictionary[second_sgdid.upper()] = feature_instance
##-------------------------------------------------------------------------
	"""
	all keys are converted to uppercase to make it easier to find entries in the 
	dictionary
	"""
	# ***************************************************************
	# ***************************************************************
##	feature_dictionary[primary_name] = feature_instance
##	if gene:
##		feature_dictionary[gene] = feature_instance
##	for alias in alias_list:
##		feature_dictionary[alias] = feature_instance
##	feature_dictionary[sgdid] = feature_instance
##	for second_sgdid in secondary_sgdid_list:
##		feature_dictionary[second_sgdid] = feature_instance
#@-body
#@-node:3::_add_names_to_dictionary
#@-others


#@<<Parse Errors>>
#@+node:7::<<Parse Errors>>
#@+body
#@@code
#@+others
#@+node:1::FeatureParseError
#@+body
class FeatureParseError(ParseError):
	pass
#@-body
#@-node:1::FeatureParseError
#@-others




#@-body
#@-node:7::<<Parse Errors>>

	

#@<<test_functions>>
#@+node:6::<<test_functions>>
#@+body
##def list_feature_types ():
##	feature_list = {}
##	file = open('chromosomal_feature.tab')
##	for line in file:
##		split_line = line.rstrip('\n').split('\t')
##		feature_type_list = split_line[3].split('|')
##		for featuretype in feature_type_list:
##			feature_list[featuretype] = ''
##	print feature_list.keys()
##	file.close()
#@-body
#@-node:6::<<test_functions>>



#@<<command line>>
#@+node:8::<<command line>>
#@+body
#@<<def run>>
#@+node:1::<<def run>>
#@+body
def GetFeatureTypes(feature_file_name):
	chromosome_hash, feature_dictionary, \
					 ambiguous_feature_names, \
					 feature_type_dict = \
					 FeatureFileParse(feature_file_name, {}, {}, {}, {})
	feature_types = {}
	multiple_types = {}
	for key in chromosome_hash.keys():
		for feature in chromosome_hash[key].FeatureIterator():
			for type in feature.Types:
				feature_types[type] = feature_types.get(type, 0) + 1
			if len(feature.Types) > 1:
				multitype = ','.join(feature.Types)
				multiple_types[multitype] = multiple_types.get(multitype, 0) + 1

	label_just = 40
	count_just = 6
	feature_types_string = '\n'.join([type.ljust(label_just) +
									 str(feature_types[type]).rjust(count_just)
									 for type in feature_types])
	multiple_types_string = '\n'.join([type.ljust(label_just) +
									 str(multiple_types[type]).rjust(count_just)
									 for type in multiple_types])

	return feature_types, multiple_types, feature_types_string, multiple_types_string



def run (feature_file_name=None):
	EXPECTED_ARGUMENTS = 1
	if feature_file_name :
		print 'run called with parameters'
	elif len(sys.argv) == (EXPECTED_ARGUMENTS + 1):
			feature_file_name = sys.argv[1]
	else :
		usage()

	if not os.path.exists(feature_file_name):
		print "File doesn't exist:", feature_file_name
		sys.exit(1)


	chromosome_hash, feature_dictionary, ambiguous_feature_names = \
					 FeatureFileParse(feature_file_name, {}, {}, {})
	print chromosome_hash.keys()
	sum = 0
##------------------------------------------------------------
	print 'ORF COUNTS'
	for key in chromosome_hash.keys():
		count = chromosome_hash[key].OrfCount()
		sum += count
		print key, count
	print 'Total:', sum
##------------------------------------------------------------
	feature_types, multiple_types, feature_types_string, multiple_types_string =\
				   GetFeatureTypes(feature_file_name)
	print '-' * 50
	print 'SINGLE TYPE COUNTS'
	print '------------------'
	print feature_types_string
	print '-' * 50, '\n\n', '-' * 50
	print 'MULTIPLE TYPE COUNTS'
	print '--------------------'
	print multiple_types_string
	print '-' * 50, '\n\n'
##------------------------------------------------------------
##	for key in feature_dictionary.keys():
##		if not key.isupper():
##			print key
	feature_list = ['FUN35', 'YGLCdelta5', 'YKLCsigma1', 'snR76', 'YERCdelta26']
	feature_list = [ name.upper() for name in feature_list]
	for feature_name in feature_list:
		feature = feature_dictionary[feature_name]
		print feature_name, ':', feature.AllNames()
#********************************************
##		print 'reference_name:', feature._reference_name
##		print 'common_name:', feature._common_name
##		print '_alternate_names:', feature._alternate_names
##		print '_id:', feature._id
##		print '_alt_id_list:', feature._alt_id_list
##		print '_alternate_names:', feature._alternate_names
#*******************************************		
	return chromosome_hash, feature_dictionary

#@-body
#@-node:1::<<def run>>


#@<<def usage>>
#@+node:2::<<def usage>>
#@+body
def usage () :
	print '-'*70 + '\n', 'Arguments given (', len(sys.argv), '):', sys.argv, """
	1. filename - cerevisiae feature file
	""" + '\n' + '-'*70
	sys.exit(1)

#@-body
#@-node:2::<<def usage>>



#@<<if main>>
#@+node:3::<<if main>>
#@+body
if __name__ == '__main__':
	print os.getcwd()
##	try :
##		run()
##	except FeatureParseError, instance:
##		instance.print_message()
##		raise 
	run()

#@-body
#@-node:3::<<if main>>


#@-body
#@-node:8::<<command line>>


#@-body
#@-node:0::@file cerevisiae_feature_parser_gomer.py
#@-leo
