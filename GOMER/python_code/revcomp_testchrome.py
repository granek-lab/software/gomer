#! /usr/bin/env python
from __future__ import division
#@+leo
#@+node:0::@file revcomp_testchrome.py
#@+body
#@@first
#@@first
#@@language python


"""
*Description:

A specialty hack - this is for generating the reverse complement of a FASTA
format file.  It is specifically for reverse complementing a synthetic yeast
chromosome test sequence, because of the changes it makes to the header, and
the name of the file it outputs
"""

#@<<imports>>
#@+node:1::<<imports>>
#@+body
##import os
##os.environ['PYCHECKER'] = '--allglobals --stdlib'
##import pychecker.checker

import sys
import re

# import array

import reverse_complement
#@-body
#@-node:1::<<imports>>


empty_line_re = re.compile('^\s*$')


def pretty_print(line_length, string, output_handle):
	for index in range(0,len(string),line_length):
		output_handle.write(string[index:(index + line_length)] + '\n')


def parse_file_readline(sequence_file_name):
	# sequence_file_handle = fileinput.input(sequence_file_name)
	sequence_file_handle = file(sequence_file_name)
	header_line = sequence_file_handle.readline()
	new_header_line = header_line.rstrip().replace('[chromosome=XX]', '[chromosome=XL]')
	## sub(pattern, repl, string[, count])
	line_length = -1
	forward_string = ''
	cur_line = 'PRIME THE PUMP'
	while not empty_line_re.search(cur_line):
		cur_line = sequence_file_handle.readline().rstrip()
		line_length = max((line_length, len(cur_line)))
		forward_string += cur_line
	return forward_string, new_header_line, line_length


if __name__ == '__main__':
	sequence_file_name = sys.argv[1]
	forward_string, new_header_line, line_length = parse_file_readline(sequence_file_name)
	revcomp_string = reverse_complement.complement(reverse_complement.reverse(forward_string))

	output_file_name = sequence_file_name.replace('.fsa', '_revcomp.fsa')
	output_handle = file(output_file_name, 'w')
	output_handle.write(new_header_line + '\n')
	pretty_print(line_length, revcomp_string, output_handle)


#@+doc
# 
# def parse_file_read(sequence_file_name):
# 	sequence = sequence_file_handle.read()
# 	sequence = sequence.replace('\n','')
# 
# 	if not bases_only_re.search(sequence):
# 		raise SequenceParseError (sequence_file_handle.filename(),
# 								  cur_line,
# 								  sequence_file_handle.lineno(),
# 							  'Error: Only sequence and empty lines may follow the comment line')

#@-doc
#@@code


#@-body
#@-node:0::@file revcomp_testchrome.py
#@-leo
