#! /usr/bin/env python

import sys
import math
import os
from Numeric import searchsorted,sort,concatenate,argmin,argmax,size,zeros,add,arange,Float
from cache_window_list_gomer import CacheWindowList
from cache_file_handler_gomer import CacheFileHandler
from optparse import OptionParser



def get_min_max(cache_file_handler):
	max_val = 1 - sys.maxint
	min_val = sys.maxint
	# cache_file_handler = CacheFileHandler(cache_file_name, fast_cache_directory)
	cache_handle = cache_file_handler.GetHandle()

	for chrom_label in cache_file_handler.Chromosomes:
		chrom = cache_file_handler.GetChrom(chrom_label)
		num_windows = chrom.GetNumWindows()
		for_offset = chrom.GetForwardOffset()
		rev_offset = chrom.GetRevCompOffset()
		for offset in (for_offset, rev_offset):
			cur_cache_list = CacheWindowList(cache_handle, num_windows, offset)
			cur_cache_array = cur_cache_list[:]
			min_val = min((min_val, cur_cache_array[argmin(cur_cache_array)]))
			max_val = max((max_val, cur_cache_array[argmax(cur_cache_array)]))
	bottom_exponent = int(math.floor(math.log10(min_val))) - 1
	top_exponent = int(math.ceil(math.log10(max_val))) + 1
	return min_val, max_val, bottom_exponent, top_exponent


def main():
	usage = 'usage: %prog CACHE_FILE_NAME [FAST_CACHE_DIRECTORY] [-s] [-f] [-b] [-d]'
	usage += '\n   The bin cutoffs are determined by BASE**POW, where the increments of POW is dtermined by -s/--step (default is 0.25)'
	parser = OptionParser(usage)
	parser.add_option("-f", "--fast", type="string", dest="fast_cache_directory",
					  metavar="FAST_CACHE",
					  help="use FAST_CACHE as the fast cache directory")
	parser.add_option("-s", "--step", type="float", default=0.25, help="exponent STEP size", metavar="STEP")
	parser.add_option("-b", "--base", type="string", default="10", help="BASE for bins ('e' or a number", metavar="BASE")
	parser.add_option("-d", "--data", action="store_true", default=False, help="output data for histogram")

	(options, args) = parser.parse_args()
	if len(args) != 1:
		parser.error("incorrect number of arguments")
		
	cache_file_name = args[0]
	fast_cache_directory = options.fast_cache_directory
	##-----------------------------------------------------------------
	handler = CacheFileHandler(cache_file_name, fast_cache_directory)
	min_val, max_val, bottom_exp, top_exp = get_min_max(handler)
	print 'min_val', min_val
	print 'max_val', max_val
	print 'bottom_exp', bottom_exp
	print 'top_exp', top_exp
	print 'exponent range:', range(bottom_exp, top_exp)
	base = options.base
	# log_base = 'e'
	exponent_step_size = options.step
	bin_exponents = arange(bottom_exp,top_exp,exponent_step_size)
	if base == 'e' or base == 'E':
		bins = [math.exp(expo) for expo in bin_exponents]
	else:
		bins = [10**expo	   for expo in bin_exponents]
	if options.data:
		calculate_sums = True
	else:
		calculate_sums = False
		
	bin_counts, bin_sums = generate_histogram(handler, bins, calculate_sums)
	# filtered_histo = choose(greater(histo,max_count), (histo, 0))
	# p = gracePlot.gracePlot()
	if options.data:
		ju=16
		print ''.join(["bin #".ljust(ju), "bin bottom".ljust(ju), "bottom".ljust(ju), "bin count".ljust(ju), "bin sum".ljust(ju)])
		for bin_num in range(len(bins)):
			bin_bottom = '%.4g' % bins[bin_num]
			bottom = base+'**'+ str(bin_exponents[bin_num])
			if bin_num < len(bins) -1:
				top = str(bins[bin_num+1]).ljust(ju)
			else:
				top = '+'.ljust(ju)
			count = str(bin_counts[bin_num])
			sum = str(bin_sums[bin_num])
			print ''.join([str(bin_num).ljust(ju),
						   bin_bottom.ljust(ju),
						   bottom.ljust(ju),
						   count.ljust(ju),
						   sum.ljust(ju) ])
	else:
		from gracePlot import gracePlot
		p = gracePlot()
		x_min = bottom_exp
		x_max = top_exp
		p.histoPlot( histo, x_min=x_min, x_max=x_max, edges=1)
		p.ylabel('Number of Sites')
		p.xlabel('Ka (Log10)')
	# return histo, bins, p
	## ***************************************************************
	## After running, in xmgrace: 
	## Set the Y-axis to Logarithmic, and change the Start value to 0.1
	## ***************************************************************


##	g = p.grace
##	g('world ymin 0.1')
##	g('yaxes scale logarithmic')
##	g('redraw')

"""
>>> histogram([-1], [0,1,2])
array([0, 0, 0])
>>> histogram([0], [0,1,2])
array([1, 0, 0])
>>> histogram([.5], [0,1,2])
array([1, 0, 0])
>>> histogram([1], [0,1,2])
array([0, 1, 0])
>>> histogram([1.5], [0,1,2])
array([0, 1, 0])
>>> histogram([2], [0,1,2])
array([0, 0, 1])
>>> histogram([100], [0,1,2])
array([0, 0, 1])
"""
def histogram(data, bins, calculate_sums=False):
	"""
	This code is strongly based on the Numeric documentation
	"""
	# searchsorted tells the indices where the "bin" values would fit into the array
	sorted_data = sort(data)
	indices = searchsorted(sorted_data, bins)
	##---------------------------------------------------------
	if calculate_sums:
		# raise NotImplementedError, "Need to check indices"
		bin_sums = zeros(len(indices), Float)
		last_index = len(sorted_data) + 1
		slice_pairs = zip(indices, concatenate([indices[1:], [last_index]]))
		## print 'slice_pairs', slice_pairs
		for i in range(0,len(indices)):
			start, stop = slice_pairs[i]
			bin_sums[i] = add.reduce(sorted_data[start:stop])
	else:
		bin_sums = None
	##---------------------------------------------------------
	# append the N of the data array for use in the next step
	n = concatenate([indices, [len(data)]])
	# subtracting the array from itself shifted by one provides the counts
	bin_counts = n[1:]-n[:-1]
	# print 'indices', indices
	return bin_counts, bin_sums


##def get_matrix_min_max(prob_matrix_file):
##	prob_matrix = FlatProbMatrixParse(probability_file_name, base_frequency_hash)
##	(max_Ka, max_Ka_values, max_Ka_sequence,
##	 min_Ka, min_Ka_values, min_Ka_sequence) = prob_matrix.GetKaRange()
##	return min_Ka, max_Ka

def generate_histogram(cache_file_handler, bins, calculate_sums):
	## cache_file_handler = CacheFileHandler(cache_file_name, fast_cache_directory)
	cache_handle = cache_file_handler.GetHandle()
	bin_count_totals = zeros(size(bins))
	bin_sum_totals = zeros(size(bins), Float)
	for chrom_label in cache_file_handler.Chromosomes:
		chrom = cache_file_handler.GetChrom(chrom_label)
		num_windows = chrom.GetNumWindows()
		for_offset = chrom.GetForwardOffset()
		rev_offset = chrom.GetRevCompOffset()
		for offset in (for_offset, rev_offset):
			cur_cache_list = CacheWindowList(cache_handle, num_windows, offset)
			cur_cache_array = cur_cache_list[:]
			cur_bin_counts, cur_bin_sums = histogram(cur_cache_array, bins, calculate_sums)
			bin_count_totals += cur_bin_counts
			if calculate_sums:
				bin_sum_totals += cur_bin_sums
	return bin_count_totals, bin_sum_totals

if __name__ == '__main__':

	## main(cache_file_name, fast_cache_directory, print_data)
	main()


##---------------------------------------------
"""

searchsorted(a, values)

Called with a rank-1 array sorted in ascending order, searchsorted() will return the indices of the positions in a where the corresponding values would fit.

>>> print bin_boundaries

[ 0. 0.1 0.2 0.3 0.4 0.5 0.6 0.7 0.8 0.9 1. ]

>>> print data

[ 0.3029573 0.79585496 0.82714031 0.77993884 0.55069605 0.76043182

0.28511823 0.29987358 0.40286206 0.68617903]

>>> print searchsorted(bin_boundaries, data)

[4 8 9 8 6 8 3 3 5 7]

This can be used for example to write a simple histogramming function:

...

>>> print histogram([0,0,0,0,0,0,0,.33,.33,.33], arange(0,1.0,.1))

[7 0 0 3 0 0 0 0 0 0]

>>> print histogram(sin(arange(0,10,.2)), arange(-1.2, 1.2, .1))

[0 0 4 2 2 2 0 2 1 2 1 3 1 3 1 3 2 3 2 3 4 9 0 0]
---------------------------------------------
sort(a, axis=-1)

This function returns an array containing a copy of the data in a , with the same shape as a , but with the order of the elements along the specified axis sorted. The shape of the returned array is the same as a 's. Thus, sort(a, 3) will be an array of the same shape as a, where the elements of a have been sorted along the fourth axis.

---------------------------------------------
argmax(a, axis=-1), argmin(a, axis=-1)

The argmax() function returns an array with the arguments of the maximum values of its input array a along the given axis. The returned array will have one less dimension than a. argmin() is just like argmax() , except that it returns the indices of the minima along the given axis.
"""

#@-body
#@-node:0::@file histogram_scores.py
#@-leo
