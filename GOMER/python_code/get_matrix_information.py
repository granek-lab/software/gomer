#! /usr/bin/env python
from __future__ import division

import os
import sys
import operator
from optparse import OptionParser

from flat_prob_matrix_parser_gomer import FlatProbMatrixParse


def main ():
	version_num = 0.001
	version = '%prog ' + str(version_num)
	usage = 'usage: %prog [options] PROB_MATRIX_FILE\n'

	parser = OptionParser(usage=usage, version=version)
	parser.add_option("-t", "--temperature", type="float", metavar="TEMP",
					   help="Change the pseudotemperature of the matrix to TEMP (default is 300K)")

	(options, args) = parser.parse_args()
	if (len(args) == 1):
		probability_file_name = args[0]
	else :
		parser.print_help()
		sys.exit(2)

	base_frequency_hash = {# genomes/saccharomyces_cerevisiae/jan_29_2004/
		'A':0.3092605972321702,
		'C':0.1907394027678298,
		'T':0.3092605972321702,
		'G':0.1907394027678298}
	
	prob_matrix = FlatProbMatrixParse(probability_file_name, base_frequency_hash)
	if options.temperature:
		prob_matrix.ResetPseudoTemp(options.temperature)
	per_position_information = prob_matrix.GetPositionInformation()

	print 'Matrix:', os.path.basename(probability_file_name)
	print 'base_frequency_hash:\n', '\n'.join([str(key) + ':' + str(base_frequency_hash[key])
											 for key in base_frequency_hash])
	print 'Matrix Temperature:', prob_matrix.GetPseudoTemp()
	print 'Information:', per_position_information
	print 'Total Matrix Information:', reduce(operator.add, per_position_information)
	(max_Ka, max_Ka_values, max_Ka_sequence,
	 min_Ka, min_Ka_values, min_Ka_sequence) = prob_matrix.GetKaRange()
	print 'Max Ka:', max_Ka
	print 'Total Matrix Ka:', prob_matrix.GetKaSum()
	

	

if __name__ == '__main__':
	main()
	
