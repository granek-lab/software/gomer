from __future__ import division
#@+leo
#@comment Created by Leo at Sun Aug 24 23:08:59 2003
#@+node:0::@file coordinate_single_square_reg_region_model_gomer.py
#@+body
#@@first
#@@language python
#@@path kappa_modules/ 


"""
CoordinateSingleSquareRegulatoryRegionModel
"""

#@<<imports>>
#@+node:1::<<imports>>
#@+body
# import sys
##import os
##os.environ['PYCHECKER'] = '--allglobals --stdlib'
##import pychecker.checker

##import check_python_version
##check_python_version.CheckVersion()

import types
from exceptions_gomer import GomerError
#@-body
#@-node:1::<<imports>>



#@<<Name>>
#@+node:2::<<Name>>
#@+body
NAME = 'CoordinateSingleSquareRegulatoryRegionModel'

#@-body
#@-node:2::<<Name>>


#@<<Type>>
#@+node:3::<<Type>>
#@+body
TYPE = 'CoordinateRegulatoryRegionModel'

#@-body
#@-node:3::<<Type>>



#@<<Parameters>>
#@+node:4::<<Parameters>>
#@+body
PARAMETERS = {}


#@-body
#@-node:4::<<Parameters>>


#@<<Description>>
#@+node:5::<<Description>>
#@+body
DESCRIPTION = \
"""


Parameter String Example:
	""
"""
#@-body
#@-node:5::<<Description>>


#@<<Parameter Example>>
#@+node:6::<<Parameter Example>>
#@+body
PARAMETER_STRING_EXAMPLE = ("",)

#@-body
#@-node:6::<<Parameter Example>>


#@<<Requires Strand>>
#@+node:7::<<Requires Strand>>
#@+body
REQUIRES_STRAND = False
#@-body
#@-node:7::<<Requires Strand>>



#@<<class CoordinateSingleSquareRegulatoryRegionModel>>
#@+node:8::<<class CoordinateSingleSquareRegulatoryRegionModel>>
#@+body
class RegulatoryRegionModel(object):

	#@+others
	#@+node:1::__init__
	#@+body
	def __init__(self,
				 binding_site_model):
		# self._genome = genome
		self._binding_site_model = binding_site_model
	
		## In order to save computation, the most recently calculated regulatory region
		## is cached, this is stored in self._cached_regulatory_region, the feature that this 
		## corresponds to is saved in self._cached_feature, so if this is requested again, it doesn't
		## need to be recalculated
		self._cached_regulatory_region = None
		self._cached_coordinate_feature = None

	#@+doc
	# 
	# the regulatory region is inclusive :

	#@-doc
	#@@code
	#@-body
	#@-node:1::__init__
	#@+node:2::GetRegulatoryRegionRanges
	#@+body
	# def GetRegulatoryRegionRanges(self, ORF_name):
	def GetRegulatoryRegionRanges(self, coordinate_feature):

	#@+doc
	# 
	# IMPORTANT!!!!
	# Ranges returned by this function are ranges in "hitlist space" - they 
	# are represent the smallest
	# and largest indices of the hitlist that define the range of binding 
	# sites for which the site
	# kappa is non-zero - they are NOT base pair numbers (because the hitlist 
	# array is numbered starting at 0, and bp's are numbered starting at 1, 
	# (hitlist index) = bp number - 1).

	#@-doc
	#@@code

		# cur_ORF = self._genome.GetFeature(ORF_name)
		if 	coordinate_feature == self._cached_coordinate_feature:
			return self._cached_regulatory_region
	
		cur_chrom = coordinate_feature.Chromosome
		hitlist_size = cur_chrom.GetHitListSize(self._binding_site_model.MD5)
		binding_site_length = self._binding_site_model.Length
	

	#@+doc
	# 
	# 	if ((self._regulatory_five_prime - self._regulatory_three_prime) + 1) < binding_site_length:
	# 		raise SingleSquareRegulatoryRegionError(
	# 			'The regulatory region is smaller than the binding site:' +
	# 			'\nregulatory_five_prime: ' + str(self._regulatory_five_prime) +
	# 			'\nregulatory_three_prime: '+ str(self._regulatory_three_prime) +
	# 			'\nbinding_site_length: ' + str(binding_site_length) + '\n\n'
	# 			)

	#@-doc
	#@@code

	##----------------------------------------------------------------------
	##
	##     1                                     1     
	##     0                                     3   BP
	##     0                                     8
	## ttttaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaatttt
	## 9   9                                     1
	## 5   9                                     3   hitindex    kappa
	##                                           7
	## cccccccccc                                        95        0
	##  cccccccccc                                       96        0
	##   cccccccccc                                      97        0
	##    cccccccccc                                     98        0
	##     cccccccccc                                    99        1
	##      cccccccccc                                   100       1
	##                                 cccccccccc        127       1
	##                                  cccccccccc       128       1
	##                                   cccccccccc      129       0
	##                                    cccccccccc     130       0
	## 
	## binding site length = 10
	## intergenic region: 100bp to 138bp
	## regultory region: 99 to 128 (hitindex)
	##----------------------------------------------------------------------
	
		start_hitlist_index = coordinate_feature.Start - 1
		# subtract one to account for the fact the the hitlist index starts at zero 
		stop_hitlist_index = (coordinate_feature.Stop - binding_site_length) # + 1) - 1)
		# plus one for the binding site length, minus one for the hitlist offset 

	#@+doc
	# 
	# 	the regulatory region is inclusive :
	# 		if the Intergenic region runs from 100bp to 138bp
	# 		that means that any binding site that starts at bp #100 or after,
	# 		and ends at bp #138 or before is in the regulatory region
	# 
	# 	if the binding site is 10bp, then the hitlist index range should be (99,128)

	#@-doc
	#@@code

		if stop_hitlist_index < start_hitlist_index:
			raise CoordinateRegulatoryRegionError(
				'The stop_index cannot be smaller than the start_index:' +
				'\nstart_index: ' +	str(start_hitlist_index) +
				'\nstop_index:  ' + str(stop_hitlist_index) +
				'\nbinding_site_length: ' + str(binding_site_length) + '\n\n'
				)
		elif stop_hitlist_index >= hitlist_size:
			raise CoordinateRegulatoryRegionError(
				'The stop_index cannot be greater than or equal to the size of the hitlist:' +
				'\nstart_index: ' +	str(start_hitlist_index) +
				'\nstop_index:  ' + str(stop_hitlist_index) +
				'\nhitlist size:  ' + str(hitlist_size) +
				'\nbinding_site_length: ' + str(binding_site_length) + '\n\n'
				)
			
			
		if stop_hitlist_index < 0 :
			# return tuple((tuple(()),))
			return tuple()
		elif start_hitlist_index < 0:
			start_hitlist_index = 0
	
		if start_hitlist_index >= hitlist_size:
			# return tuple((tuple(()),))
			return tuple()
		elif stop_hitlist_index >= hitlist_size:
			stop_hitlist_index = hitlist_size - 1
	
	
		self._cached_feature = coordinate_feature
		self._cached_regulatory_region = tuple((tuple((start_hitlist_index, stop_hitlist_index)),))
			
		return self._cached_regulatory_region
	
	
	
	
	#@-body
	#@-node:2::GetRegulatoryRegionRanges
	#@+node:3::GetSiteKappa
	#@+body
	# def GetSiteKappa(self, Orf, base_position):
	def GetSiteKappa(self, coordinate_feature, hitlist_index, site_strand):

	#@+doc
	# 
	# Since this is a square function, the kappa is one if the binding site is 
	# in the regulatory region,
	# otherwise the kappa is zero

	#@-doc
	#@@code
		if 	coordinate_feature == self._cached_feature:
			((start_hitlist_index, stop_hitlist_index),) = self._cached_regulatory_region
		else:
			((start_hitlist_index, stop_hitlist_index),) = self.GetRegulatoryRegionRanges(coordinate_feature)	
		# if start_hitlist_index <= (base_position - 1) <= stop_hitlist_index :
		if start_hitlist_index <= hitlist_index <= stop_hitlist_index :
			kappa_value = 1
		else :
			kappa_value = 0
			
		return kappa_value
	
	
	
	
	#@-body
	#@-node:3::GetSiteKappa
	#@-others

	

#@<<CoordinateRegulatoryRegionError>>
#@+node:4::<<CoordinateRegulatoryRegionError>>
#@+body
class CoordinateRegulatoryRegionError(GomerError):
	pass

#@-body
#@-node:4::<<CoordinateRegulatoryRegionError>>
#@-body
#@-node:8::<<class CoordinateSingleSquareRegulatoryRegionModel>>
#@-body
#@-node:0::@file coordinate_single_square_reg_region_model_gomer.py
#@-leo
