from __future__ import division
#@+leo
#@comment Created by Leo at Sun Aug 24 23:08:58 2003
#@+node:0::@file sequencefeature_single_square_reg_region_model_gomer.py
#@+body
#@@first
#@@language python
#@@path kappa_modules/ 


"""
SequenceFeatureRegulatoryRegionModel
"""

#@<<imports>>
#@+node:1::<<imports>>
#@+body
# import sys
##import os
##os.environ['PYCHECKER'] = '--allglobals --stdlib'
##import pychecker.checker

##import check_python_version
##check_python_version.CheckVersion()

import types
from exceptions_gomer import GomerError
#@-body
#@-node:1::<<imports>>



#@<<Name>>
#@+node:2::<<Name>>
#@+body
NAME = 'SequenceFeatureSingleSquareRegulatoryRegionModel'
#@-body
#@-node:2::<<Name>>


#@<<Type>>
#@+node:3::<<Type>>
#@+body
TYPE = 'SequenceFeatureRegulatoryRegionModel'

#@-body
#@-node:3::<<Type>>



#@<<Parameters>>
#@+node:4::<<Parameters>>
#@+body
PARAMETERS = {}


#@-body
#@-node:4::<<Parameters>>


#@<<Description>>
#@+node:5::<<Description>>
#@+body
DESCRIPTION = \
"""


Parameter String Example:
	""
	(no parameters are expected)
"""
#@-body
#@-node:5::<<Description>>


#@<<Parameter Example>>
#@+node:6::<<Parameter Example>>
#@+body
PARAMETER_STRING_EXAMPLE = ("",)

#@-body
#@-node:6::<<Parameter Example>>


#@<<Requires Strand>>
#@+node:7::<<Requires Strand>>
#@+body
REQUIRES_STRAND = False
#@-body
#@-node:7::<<Requires Strand>>



#@<<class SequenceFeatureSingleSquareRegulatoryRegionModel>>
#@+node:8::<<class SequenceFeatureSingleSquareRegulatoryRegionModel>>
#@+body
class RegulatoryRegionModel(object):

	#@+others
	#@+node:1::__init__
	#@+body
	def __init__(self, binding_site_model):
		# self._genome = genome
		self._binding_site_model = binding_site_model
	
		## In order to save computation, the most recently calculated regulatory region
		## is cached, this is stored in self._cached_regulatory_region, the feature that this 
		## corresponds to is saved in self._cached_feature, so if this is requested again, it doesn't
		## need to be recalculated
		self._cached_regulatory_region = None
		self._cached_feature = None

	#@+doc
	# 
	# the regulatory region is inclusive :

	#@-doc
	#@@code
	#@-body
	#@-node:1::__init__
	#@+node:2::GetRegulatoryRegionRanges
	#@+body
	# def GetRegulatoryRegionRanges(self, ORF_name):
	def GetRegulatoryRegionRanges(self, sequence_feature):

	#@+doc
	# 
	# IMPORTANT!!!!
	# Ranges returned by this function are ranges in "hitlist space" - they 
	# are represent the smallest
	# and largest indices of the hitlist that define the range of binding 
	# sites for which the site
	# kappa is non-zero - they are NOT base pair numbers (because the hitlist 
	# array is numbered starting at 0, and bp's are numbered starting at 1, 
	# (hitlist index) = bp number - 1).
	# 
	# Since a sequence feature is pretty much just a sequence, this regulatory 
	# region model represents the whole sequence

	#@-doc
	#@@code

		# cur_ORF = self._genome.GetFeature(ORF_name)
		if 	sequence_feature == self._cached_feature:
			return self._cached_regulatory_region
	

	#@+doc
	# 
	# 	if ((self._regulatory_five_prime - self._regulatory_three_prime) + 1) < binding_site_length:
	# 		raise SingleSquareRegulatoryRegionError(
	# 			'The regulatory region is smaller than the binding site:' +
	# 			'\nregulatory_five_prime: ' + str(self._regulatory_five_prime) +
	# 			'\nregulatory_three_prime: '+ str(self._regulatory_three_prime) +
	# 			'\nbinding_site_length: ' + str(binding_site_length) + '\n\n'
	# 			)

	#@-doc
	#@@code
		# the regulatory region is the whole sequence
	##----------------------------------------------------------------------
	##           1         2         3        
	## 0123456789012345678901234567890123456789
	## AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA     hitindex
	## cccccccccc                                       0  <---start_hitindex
	##  cccccccccc                                      1
	##                              cccccccccc         29
	##                               cccccccccc        30  <---stop_hitindex
	##
	## reg_region_length = 40
	## site_length    = 10
	## start_hitindex =  0
	## stop_hitindex  = reg_region_length - site_length = 30
	##----------------------------------------------------------------------
	
	
		start_hitlist_index = 0
		stop_hitlist_index = len(sequence_feature) - self._binding_site_model.Length # the last index in the hitlist
	
		self._cached_feature = sequence_feature
		self._cached_regulatory_region = tuple((tuple((start_hitlist_index, stop_hitlist_index)),))
			
		return self._cached_regulatory_region
	
	
	
	
	#@-body
	#@-node:2::GetRegulatoryRegionRanges
	#@+node:3::GetSiteKappa
	#@+body
	# def GetSiteKappa(self, Orf, base_position):
	def GetSiteKappa(self, sequence_feature, hitlist_index, site_strand):

	#@+doc
	# 
	# Since this is a square function, the kappa is one if the binding site is 
	# in the regulatory region,
	# otherwise the kappa is zero

	#@-doc
	#@@code
		if 	sequence_feature == self._cached_feature:
			((start_hitlist_index, stop_hitlist_index),) = self._cached_regulatory_region
		else:
			((start_hitlist_index, stop_hitlist_index),) = self.GetRegulatoryRegionRanges(sequence_feature)	
		# if start_hitlist_index <= (base_position - 1) <= stop_hitlist_index :
		if start_hitlist_index <= hitlist_index <= stop_hitlist_index :
			kappa_value = 1
		else :
			kappa_value = 0
			
		return kappa_value
	
	
	
	
	#@-body
	#@-node:3::GetSiteKappa
	#@-others

	

#@<<RegulatoryRegionError>>
#@+node:4::<<RegulatoryRegionError>>
#@+body
class RegulatoryRegionError(GomerError):
	pass

#@-body
#@-node:4::<<RegulatoryRegionError>>
#@-body
#@-node:8::<<class SequenceFeatureSingleSquareRegulatoryRegionModel>>
#@-body
#@-node:0::@file sequencefeature_single_square_reg_region_model_gomer.py
#@-leo
