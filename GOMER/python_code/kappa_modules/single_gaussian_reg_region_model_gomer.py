#! /usr/bin/env python
#@+leo
#@comment Created by Leo at Sun Aug 24 22:57:54 2003
#@+node:0::@file single_gaussian_reg_region_model_gomer.py
#@+body
#@@first
#@@language python
#@@path kappa_modules/ 

"""
SingleGaussianRegulatoryRegionModel - 
	Sites whose farthest end are more than max_dist from 
"""

#@<<imports>>
#@+node:1::<<imports>>
#@+body
from __future__ import division

import types
import math

import sys
sys.path.append('..')
from exceptions_gomer import GomerError
#@-body
#@-node:1::<<imports>>



#@<<Name>>
#@+node:2::<<Name>>
#@+body
NAME = 'SingleGaussianRegulatoryRegionModel'

#@-body
#@-node:2::<<Name>>


#@<<Type>>
#@+node:3::<<Type>>
#@+body
TYPE = 'RegulatoryRegionModel'

#@-body
#@-node:3::<<Type>>


#@<<Parameters>>
#@+node:5::<<Parameters>>
#@+body
PARAMETERS = {'mean'     : types.IntType,
			  'std_dev'  : types.FloatType, 
			  'max_dist' : types.IntType, 
			  'cutoff'	 : types.FloatType}
#@-body
#@-node:5::<<Parameters>>


#@<<Description>>
#@+node:4::<<Description>>
#@+body
DESCRIPTION = \
"""
SingleGaussianRegulatoryRegionModel is a regulatory region model

"mean" and "std_dev" are required. "mean" is the base pair around which the gaussian distribution is centered, the supplied value is the number of base pairs relative to the start of the feature (the "A" of the ATG if the feature is an ORF) in the 5' direction.  So with a mean of 0, the distribution will be centered on the start of the feature, positive mean values are centered 5' of the feature start (upstream) and negative features are centered downstream of the feature start (the distribution will be centered with the feature, if the mean is negative, and the absolute value of the mean is less than the length of the feature). Either "max_dist" or "cutoff" must be supplied.  max_dist is the distance from the mean of the most distant site to be considered (the distance of the closest edge, or in other words, the number of bases between the mean bp and the closest base of the most distant site.)  If max_dist is not supplied, cutoff must be supplied.  Cutoff is the lowest value of the kappa function.  If cutoff is supplied, max_dist is calculated from cutoff.  Note, max_dist is the distance upstream and downstream from the mean bp.
"""
#@-body
#@-node:4::<<Description>>


#@<<Parameter Example>>
#@+node:6::<<Parameter Example>>
#@+body
PARAMETER_STRING_EXAMPLE = ("mean=300; std_dev=50; max_dist=250",
							"mean=300; std_dev=50; cutoff=0.0001")

#@-body
#@-node:6::<<Parameter Example>>


#@<<Requires Strand>>
#@+node:8::<<Requires Strand>>
#@+body
REQUIRES_STRAND = True
#@-body
#@-node:8::<<Requires Strand>>



#@<<Functions>>
#@+node:7::<<Functions>>
#@+body
def gaussian(x,mean,stdev):
	coeff = 1/(math.sqrt(2*math.pi)*stdev)
	return coeff * math.exp(-(((x-mean)**2)/(2*(stdev**2))))

def inverse_gaussian(y,mean,stdev):
	return math.sqrt(2*(stdev**2)* math.log(1/(math.sqrt(2*math.pi) * stdev*y))) + mean


def gaussian_like(x,mean,stdev):
	coeff = 1 # /(math.sqrt(2*math.pi)*stdev)
	return coeff * math.exp(-(((x-mean)**2)/(2*(stdev**2))))

def inverse_gaussian_like(y,mean,stdev):
	return math.sqrt(-math.log(y) * 2*(stdev**2)) + mean

#@-body
#@-node:7::<<Functions>>



#@<<class SingleGaussianRegulatoryRegionModel>>
#@+node:10::<<class SingleGaussianRegulatoryRegionModel>>
#@+body
class RegulatoryRegionModel(object):

	#@+others
	#@+node:1::__init__
	#@+body
	def __init__(self,
				 binding_site_model, 
				 mean,
				 std_dev,
				 max_dist=None,
				 cutoff=None):
		self._binding_site_model = binding_site_model
		self._site_model_length = binding_site_model.Length
		self._mean = mean
		self._std_dev = std_dev
	
		
		if (not max_dist) and (not cutoff):
			raise RegulatoryRegionError, 'Must supply either a "max_dist" or "cutoff"'	
		elif not max_dist:
			# max_val = gaussian(mean,mean,std_dev)
			max_val = gaussian_like(mean,mean,std_dev)
			if max_val < cutoff:
				raise RegulatoryRegionError, 'Cutoff is greater than the maximum value of the gaussian function for the given mean and standard deviation'
			## max_center = inverse_gaussian(cutoff, mean, std_dev)
			# max_center = inverse_gaussian(cutoff, 0, std_dev)
			max_center = inverse_gaussian_like(cutoff, 0, std_dev)
			self._max_dist = int(math.ceil
								 (max_center +
								  (self._site_model_length-1)/2))
		else:
			self._max_dist = int(max_dist)
	
		if ((2 * self._max_dist) + 1) < self._site_model_length:
			raise RegulatoryRegionError(
				'The regulatory region is smaller than the binding site:' +
				'\nmax_dist: ' + str(self._max_dist) +
				'\nbinding_site_length: ' + str(self._site_model_length) + '\n\n'
				)
		## In order to save computation, the most recently calculated regulatory region
		## is cached, this is stored in self._cached_regulatory_region, the feature that this 
		## corresponds to is saved in self._cached_feature, so if this is requested again, it doesn't
		## need to be recalculated
		self._cached_regulatory_region = None
		self._cached_mean_hitindex = None
		self._cached_feature = None

	#@+doc
	# 
	# the regulatory region is inclusive :
	# 	if the feature is one the '+' strand, and starts at base pair #30
	# 	and the regulatory region is (20, 10),
	# 	that means that any binding site that starts at bp #10 or after,
	# 	and ends at bp #20 or before is in the regulatory region

	#@-doc
	#@@code

	
	##----------------------------------------------------------------------
	##     1         1         1         1        1     
	##     0         1         2         3        3   BP
	##     0         0         0         0        9
	## ttttaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaATGtt
	## 9   9                           1         1
	## 5   9                           2         3       hitindex    kappa
	##                                 7         7
	##        cccccccccc                                      102       0
	##         cccccccccc                                     103       0
	##          cccccccccc                                    104       gauss()
	##           cccccccccc                                   105       gauss()
	##          ^                   cccccccccc                124       gauss()
	##                               cccccccccc               125       gauss()
	##                                cccccccccc              126       0
	##                                 cccccccccc             127       0
	##                                        ^
	## feature start = 140bp
	## binding site length = 10
	## mean = 20
	## mean_bp    = 120 = (140 - 20)
	## mean_index = 119 = (120 - 1)
	## max_dist = 15
	## min_hitindex = 104 = (119 - 15)
	## max_hitindex = 125 = ((119 + 15) - 10) + 1
	##----------------------------------------------------------------------
	
	##----------------------------------------------------------------------
	##
	##     1         1         1         1       1    
	##     0         1         2         3       3  BP
	##     0         0         0         0       8
	## aCATttttttttttttttttttttttttttttttttttttttttaaaaa
	## tGTAaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaattttt
	## 9   9          1         1         1      1   
	## 5   9          1         2         3      3 hitindex    kappa
	##                0         0         0      7    
	## cccccccccc                                        95        0
	##  cccccccccc                                       96        0
	##   cccccccccc                                      97        0
	##    cccccccccc                                     98        0
	##     cccccccccc                                    99        1
	##      cccccccccc                                   100       1
	##                                 cccccccccc        127       1
	##                                  cccccccccc       128       1
	##                                   cccccccccc      129       1
	##                                    cccccccccc     130       0
	## 
	## feature start = 99bp (minus strand)
	## binding site length = 10
	## mean = 20
	## mean_bp    = 119 = (100 + 20) - 1
	## mean_index = 118 = (120 - 1)
	## max_dist = 15
	## min_hitindex = 103 = (118 - 15)
	## max_hitindex = 124 = ((118 + 15) - 10) + 1
	##----------------------------------------------------------------------
	
	
	
	
	
	#@-body
	#@-node:1::__init__
	#@+node:2::GetRegulatoryRegionRanges
	#@+body
	# def GetRegulatoryRegionRanges(self, ORF_name):
	def GetRegulatoryRegionRanges(self, feature):

	#@+doc
	# 
	# IMPORTANT!!!!
	# Ranges returned by this function are ranges in "hitlist space" - they 
	# are represent the smallest
	# and largest indices of the hitlist that define the range of binding 
	# sites for which the site
	# kappa is non-zero - they are NOT base pair numbers (because the hitlist 
	# array is numbered starting at 0, and bp's are numbered starting at 1, 
	# (hitlist index) = bp number - 1).

	#@-doc
	#@@code

		if 	feature == self._cached_feature:
			return self._cached_regulatory_region
	
		cur_chrom = feature.Chromosome
		strand = feature.Strand
		hitlist_size = cur_chrom.GetHitListSize(self._binding_site_model.MD5)
		
		if strand == '+':
			mean_bp = feature.Start - self._mean

	#@+doc
	# 
	# 	the regulatory region is inclusive :
	# 		if the feature is one the '+' strand, and starts at base pair #30
	# 		and the regulatory region is (20, 10),
	# 		that means that any binding site that starts at bp #10 or after,
	# 		and ends at bp #20 or before is in the regulatory region
	# 
	# 	if the binding site is 7bp, then the hitlist index range should be (9,13)

	#@-doc
	#@@code

		elif strand == '-':
			mean_bp = (feature.Start + self._mean) - 1

	#@+doc
	# 
	# 	the regulatory region is inclusive :
	# 		if the feature is one the '-' strand, and starts at base pair #20
	# 		and the regulatory region is (20, 10),
	# 		that means that any binding site that starts at bp #30 or after,
	# 		and ends at bp #40 or before is in the regulatory region
	# 
	# 	if the binding site is 7bp, then the hitlist index range should be (29,33)

	#@-doc
	#@@code
		else:
			raise ValueError, 'Strand must be "+" or "-". "' + str(strand) + '" is not acceptable\n' 
	
		min_hitindex = (mean_bp - self._max_dist) - 1
		max_hitindex = ((mean_bp + self._max_dist) - self._site_model_length)
	
	## 1   5    1    1     2                       bp
	##          0    5     1 
	## aaaaaaaaaaaaaaaaaaaaTaaaaaaaaaaaaaaaaaaaa   mean = 21
	##  1   5    1    1    2                      
	##           0    5    0                       hitindex
	##
	##                 cccccccccc		           len=10, mean_hitindex = 15.5
	##                cccccccccc		           len=10, mean_hitindex = 15.5
	##                ggggggggggg                  len=11, mean_hitindex = 15
		mean_hitindex = (mean_bp - (self._site_model_length - 1)/2) - 1
	
		if max_hitindex < 0 :
			# return tuple((tuple(()),))
			return tuple()
		elif min_hitindex < 0:
			min_hitindex = 0
	
		if min_hitindex >= hitlist_size:
			# return tuple((tuple(()),))
			return tuple()
		elif max_hitindex >= hitlist_size:
			max_hitindex = hitlist_size - 1
	
		self._cached_feature = feature
		self._cached_regulatory_region = tuple((tuple((min_hitindex, max_hitindex)),))
		self._cached_mean_hitindex = mean_hitindex
		return self._cached_regulatory_region
	
	#@-body
	#@-node:2::GetRegulatoryRegionRanges
	#@+node:3::GetSiteKappa
	#@+body
	# def GetSiteKappa(self, Orf, base_position):
	def GetSiteKappa(self, feature, hitindex, site_strand):

	#@+doc
	# 
	# Since this is a square function, the kappa is one if the binding site is 
	# in the regulatory region,
	# otherwise the kappa is zero

	#@-doc
	#@@code
		if 	feature == self._cached_feature:
			((min_hitindex, max_hitindex),) = self._cached_regulatory_region
			mean_hitindex = self._cached_mean_hitindex
		else:
			((min_hitindex, max_hitindex),) = self.GetRegulatoryRegionRanges(feature)	
			mean_hitindex = self._cached_mean_hitindex
	
		if min_hitindex <= hitindex <= max_hitindex :
			# kappa_value = gaussian(hitindex, mean_hitindex, self._std_dev)
			kappa_value = gaussian_like(hitindex, mean_hitindex, self._std_dev)
		else :
			kappa_value = 0
			
		return kappa_value
	
	
	
	
	#@-body
	#@-node:3::GetSiteKappa
	#@-others

	

#@<<SingleGaussianRegulatoryRegionError>>
#@+node:4::<<SingleGaussianRegulatoryRegionError>>
#@+body
class RegulatoryRegionError(GomerError):
	pass

#@-body
#@-node:4::<<SingleGaussianRegulatoryRegionError>>



#@-body
#@-node:10::<<class SingleGaussianRegulatoryRegionModel>>

def main1():
	import Numeric
	import biggles
	import os

	def usage():
		print >>sys.stderr, 'usage:', os.path.basename(sys.argv[0]), 'MEAN SD'
	if len(sys.argv) == 3:
		mean = float(sys.argv[1])
		SD = float(sys.argv[2])
	else:
		usage()
		sys.exit(2)
##	mean = 0
##	SD = 1
	max_val = gaussian(mean,mean,SD)
	print 'Maximum y:', max_val
	cutoff = max_val/100
	max_x = inverse_gaussian(cutoff,mean,SD)
	min_x = mean - (max_x - mean)
	step = (max_x - min_x)/1000
	x_list = Numeric.arange(min_x,max_x,step)
	y_list = [gaussian(x,mean,SD) for x in x_list]
	p = biggles.FramedPlot()
	user_curve = biggles.Curve(x_list, y_list, color="red")
	user_curve.label = 'mean = ' + str(mean) + ', SD = ' + str(SD)

	standard_curve = biggles.Curve(x_list, [gaussian(x,0,1) for x in x_list],
								   color="black", linetype='dotted')
	standard_curve.label = 'Standard Curve'
	legend = biggles.PlotKey(0.1,0.9,(user_curve, standard_curve))
	p.add(legend, user_curve, standard_curve)
	p.show()
####----------------------------------------------------------
####	print 'x', 'neil_0_1', 'gaussian(x,0,1)', 'gaussian(x,1.5,2)', 'gaussian(x,0,0.4)','gaussian(x,0,0.5)','gaussian(x,0,0.6)','inverse_gaussian(g0_1,0,1)-abs(x)'
##	print 'x', 'n_0_1','n_0_2','n_0_3','g_0_1','g_0_0_4'
##	for x in Numeric.arange(-4,4,0.01):
##		n_0_1 = neil(x,0,1)
##		n_0_2 = neil(x,0,2)
##		n_0_3 = neil(x,0,3)
##		g0_1 = gaussian(x,0,1)
##		## g1_5__2 = gaussian(x,1.5,2)
##		g0_0_4 = gaussian(x,0,0.4)
##		## g0_0_5 = gaussian(x,0,0.5)
##		## g0_0_6 = gaussian(x,0,0.6)
##		## inv0_1 = inverse_gaussian(g0_1, 0,1)
##		## print x, nval, g0_1, g1_5__2, g0_0_4, g0_0_5, g0_0_6,inv0_1 - abs(x)
##		print x, n_0_1, n_0_2, n_0_3, g0_1, g0_0_4
####----------------------------------------------------------
##	import gracePlot
##	xlist   = Numeric.arange(-4,4,0.01)
##	nval    = [neil(x,0,1) for x in xlist]
##	p = gracePlot.gracePlot()  # A grace session begins
##	p.plot(xlist,nval)#, symbols=1)

##	g0_1    = [gaussian(x,0,1) for x in xlist]
##	p.plot(xlist,g0_1)#, symbols=1)
	
##	g1_5__2 = [gaussian(x,1.5,2) for x in xlist]
##	g0_0_4  = [gaussian(x,0,0.4) for x in xlist]
##	g0_0_5  = [gaussian(x,0,0.5) for x in xlist]
##	g0_0_6  = [gaussian(x,0,0.6) for x in xlist]
##	inv0_1  = [inverse_gaussian(y, 0,1) for y in g0_1]
##	# print x, nval, g0_1, g1_5__2, g0_0_4, g0_0_5, g0_0_6,inv0_1 - abs(x)

def main2():
	mean = -100
	sd = 20

	print ''.join([str(val).ljust(20) for val in ['x', 'gauss_y', 'inv_gauss_y', 'gausslike_y', 'inv_gausslike_y']])
	for x in range(-200, 200, 10):
		gauss_y = gaussian(x, mean, sd)
		gausslike_y = gaussian_like(x, mean, sd)

		inv_gauss_y = inverse_gaussian(gauss_y, mean, sd)
		inv_gausslike_y = inverse_gaussian_like(gausslike_y, mean, sd)

		
		print ''.join([str(val).ljust(20) for val in [x, gauss_y, inv_gauss_y, gausslike_y, inv_gausslike_y]])

#@<<if __main__>>
#@+node:9::<<if __main__>>
#@+body
if __name__ == '__main__':
	# main1()
	main2()


#@-body
#@-node:9::<<if __main__>>



#@-body
#@-node:0::@file single_gaussian_reg_region_model_gomer.py
#@-leo
