#! /usr/bin/env python
from __future__ import division

import types
from exceptions_gomer import GomerError

NAME = 'SimpleSquareCompModel'
TYPE = 'CompetitiveModel'
PARAMETERS = {'max_distance' : types.IntType}

DESCRIPTION = NAME + \
"""
is a competitive kappa model.


It represents a simple square function.


Parameter String Example:
	"max_distance=10"

"""
REQUIRES_STRAND = False
PARAMETER_STRING_EXAMPLE = ("max_distance=10",)




class CompModel(object):
	def __init__(self,
				 primary_TF_binding_model, 
				 secondary_TF_binding_model, 
				 max_distance):

	# It is very important that primary_TF_binding_model and 
	# secondary_TF_binding_model are not switched!!
	# 

		## if max_distance < 0:
		##	raise CompError, 'max_distance must be greater than ZERO'	
		self._primary_TF_binding_model = primary_TF_binding_model
		self._primary_TF_length = primary_TF_binding_model.Length
		self._secondary_TF_binding_model = secondary_TF_binding_model
		self._secondary_TF_length = secondary_TF_binding_model.Length
		self._max_distance = max_distance
	
		## In order to save computation, the value of coop_ranges most is cached, 
		## this is stored in self._cached_coop_ranges, so if this is requested again, it doesn't
		## need to be recalculated
		#self._cached_primary_TF_name = None
		self._cached_primary_site_hitindex = None
		self._cached_comp_ranges = None
		self._cached_feature = None

	def GetSlices(self, primary_site_hitindex, primary_site_strand, feature):
		comp_ranges = self.GetCompetitiveRegionRanges(primary_site_hitindex, primary_site_strand, feature)
		## need to add 1 to the "slice stop" value, since these values are to be used directly for slices,
		## the stop value is the index after the last element included in the slice, not the last element included in the slice
		comp_ranges = [(start, stop+1) for start, stop in comp_ranges]
		tuple_list = []
		for start, stop in comp_ranges:
			slice_length = stop - start
			ones   = (1,) * slice_length
			kappa_slice = ones
			tuple_list.append((start, stop, kappa_slice, kappa_slice))
		## return list of tuples of (start_coop_slice, stop_coop_slice, for_kappa_slice, rev_kappa_slice)
		return tuple_list
	##--------------------------------------------------------------------------


	def GetCompetitiveRegionRanges(self, primary_site_hitindex, primary_site_strand, feature):

	# IMPORTANT!!!!
	# Ranges returned by this function are ranges in "hitlist space" - they 
	# are represent the smallest
	# and largest indices of the hitlist that define the range of binding 
	# sites for which the site
	# kappa is non-zero - they are NOT base pair numbers (because the hitlist 
	# array is numbered starting at 0, and bps are numbered starting at 1, 
	# (hitlist index) = bp number - 1).

		if (self._cached_primary_site_hitindex == primary_site_hitindex) and \
			(self._cached_feature == feature):
			return self._cached_coop_ranges
		
		#----------------------------------------------
		range_start = ((primary_site_hitindex - self._max_distance) - self._secondary_TF_length)
		range_stop = (primary_site_hitindex + self._max_distance + self._primary_TF_length)
		#----------------------------------------------
		chrom = feature.Chromosome
		hitlist_size = chrom.GetHitListSize(self._secondary_TF_binding_model.MD5)
	
		if (range_start >= hitlist_size) or (range_stop < 0):
			pass
		else:
			if range_start < 0:
				range_start = 0
			if range_stop >= hitlist_size:
				range_stop = hitlist_size - 1
			range = (range_start, range_stop)
			range_list = [range]
		#----------------------------------------------
		
		self._cached_primary_site_hitindex = primary_site_hitindex
		self._cached_feature = feature
		self._cached_coop_ranges = tuple(range_list)
		return self._cached_coop_ranges
	
	# Secondary = seconds
	# Primary =   primarypr
	# 
	# primary_TF_length = 9
	# secondary_TF_length = 7
	# #===============================================================================
	#           1         2         3         4         5         6         7
	# 01234567890123456789012345678901234567890123456789012345678901234567890 hitindex
	#    seconds                    primarypr
	# 
	# primary_TF_hitindex = 30
	# secondary_TF_hitindex = 3          <-------- range_start
	# #------------------------------
	# primary_TF_end_hitindex = 38
	# secondary_TF_end_hitindex = 9
	# #------------------------------
	# distance = 20
	# #===============================================================================
	#           1         2         3         4         5         6         7
	# 01234567890123456789012345678901234567890123456789012345678901234567890 hitindex
	#              seconds          primarypr
	# 
	# primary_TF_hitindex = 30
	# secondary_TF_hitindex = 13          <-------- left_range_stop
	# #------------------------------
	# primary_TF_end_hitindex = 38
	# secondary_TF_end_hitindex = 19
	# #------------------------------
	# distance = 20
	# #===============================================================================
	#           1         2         3         4         5         6         7
	# 01234567890123456789012345678901234567890123456789012345678901234567890 hitindex
	#                               primarypr          seconds
	# 
	# primary_TF_hitindex = 30
	# secondary_TF_hitindex = 49          <-------- right_range_start
	# #------------------------------
	# primary_TF_end_hitindex = 38
	# secondary_TF_end_hitindex = 55
	# #------------------------------
	# distance = 20
	# #===============================================================================
	#           1         2         3         4         5         6         7
	# 01234567890123456789012345678901234567890123456789012345678901234567890 hitindex
	#                               primarypr                    seconds
	# 
	# primary_TF_hitindex = 30
	# secondary_TF_hitindex = 59          <-------- range_stop
	# #------------------------------
	# primary_TF_end_hitindex = 38
	# secondary_TF_end_hitindex = 65
	# #------------------------------
	# distance = 20
	# #===============================================================================
	#           1         2         3         4         5         6         7
	# 01234567890123456789012345678901234567890123456789012345678901234567890 hitindex
	#                               primarypr  seconds
	# 
	# primary_TF_hitindex = 30
	# secondary_TF_hitindex = 41          <-------- range_stop
	# #------------------------------
	# primary_TF_end_hitindex = 38
	# secondary_TF_end_hitindex = 47
	# #------------------------------
	# distance = 2
	# #===============================================================================
	#           1         2         3         4         5         6         7
	# 01234567890123456789012345678901234567890123456789012345678901234567890 hitindex
	#                               primarypr
	#                                     seconds
	# 
	# primary_TF_hitindex = 30
	# secondary_TF_hitindex = 36          <-------- range_stop
	# #------------------------------
	# primary_TF_end_hitindex = 38
	# secondary_TF_end_hitindex = 42
	# #------------------------------
	# distance = -3
	# #===============================================================================

	def GetSiteKappa(self, primary_site_hitindex, primary_site_strand,
					 secondary_site_hitindex, secondary_site_strand, feature):

	# Since this is a square function, the kappa is one if the binding site is 
	# in the regulatory region,
	# otherwise the kappa is zero.  So all we have to do is see if the site 
	# falls in the one of the ranges returned by GetCoopRanges.
		if (self._cached_primary_site_hitindex == primary_site_hitindex) and \
			(self._cached_feature == feature):
			range_pairs = self._cached_coop_ranges
		else:
			range_pairs = self.GetCoopRanges(primary_site_hitindex, primary_site_strand, feature)
	
		kappa_value = 0
		for start_hitindex, stop_hitindex in range_pairs: 
			if start_hitindex <= secondary_site_hitindex <= stop_hitindex :
				kappa_value = 1
				break
		return kappa_value
	
	
	def _distance(self, primary_TF_hitindex, secondary_TF_hitindex):

	# It is very important that primary_TF_binding_model and 
	# secondary_TF_binding_model are not switched!!


		primary_TF_end_hitindex =  (primary_TF_hitindex + self._primary_TF_length) - 1
		secondary_TF_end_hitindex =  (secondary_TF_hitindex + self._secondary_TF_length) - 1
		
		distance = (max((primary_TF_hitindex - secondary_TF_end_hitindex), (secondary_TF_hitindex - primary_TF_end_hitindex))) - 1
		## distance is the number of bp between the two sites
	    ## Therefore if last base of site X is 10,
		## and first base of site Y is 11, distance is 0
	
		return distance
	
	

	

class CompError(GomerError):
	pass
