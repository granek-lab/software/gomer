#! /usr/bin/env python
from __future__ import division
#@+leo
#@comment Created by Leo at Wed Jun 11 18:41:39 2003
#@+node:0::@file regulatory_region_model_gomer.py
#@+body
#@@first
#@@first
#@@language python
#@@path kappa_modules/

"""
* Description:

RegulatoryRegionModel - This doesn't do anything right now, it might end up being
	used as a (virtual?) superclass for Regulatory Region Models.   The purpose
	of a regulatory model is to return a site_kappa value for a given binding
	site.  The site_kappa value for a given site depends solely on the site's
	position relative to the FEATURE start site, and on the characteristics of the
	model.  

	Theoretically, a site_kappa is defined for all binding sites on a
	chromosome, no matter how distant it is from the FEATURE.  In reality, beyond a
	certain range, the site_kappa is zero for the vast majority of sites in the
	genome (again this depends on the characteristics of the specific regulatory
	region model, but basically only sites within a relatively small range of the
	FEATURE start will have non-zero site_kappas).  

	With this in mind, the regulatory region model has a second purpose - in
	order to speed things up, it can return a list of one or more ranges of
	distances (relative to the FEATURE start site) for which the site_kappa is
	non-zero, so only binding sites in those ranges need to be considered in
	calculating the score for the regulatory region.

	It should be noted that ranges returned by the function GetRegulatoryRegions
	are ranges in "hitlist space" -

	they are represent the smallest and largest indices of the hitlist that
	define the range of binding sites for which the site kappa is non-zero - they
	are NOT base pair numbers. 
	
	It should also be noted that hits are numbered from their left end.  For hits 
	on the Forward strand, this means from the 5' end (so hit #10 on the Forward 
	strand is the site starting at bp 11), and the 3' end on the Reverse Complement 
	strand.  An advantage of this is that hit #10 on the Forward strand completely 
	overlaps (it is the reverse complement of) hit #10 on the Reverse Complement 
	strand.  A disadvantage is that this can be a bit confusing.


SingleSquareRegulatoryRegionModel - This is the first Regulatory Region Model I
	have made.  This regulatory region is about as simple as they come, it is a
	single square function - any site between the start and end of the regulatory
	region has a site_kappa  of 1, all sites outside of this regulatory region
	have site_kappas of zero.
"""

#@<<imports>>
#@+node:1::<<imports>>
#@+body
# import sys
##import os
##os.environ['PYCHECKER'] = '--allglobals --stdlib'
##import pychecker.checker

##import check_python_version
##check_python_version.CheckVersion()

#@-body
#@-node:1::<<imports>>



#@<<class RegulatoryRegionModel>>
#@+node:2::<<class RegulatoryRegionModel>>
#@+body
class RegulatoryRegionModel(object):

	#@+others
	#@+node:1::__init__
	#@+body
	def __init__(self, genome, config_file_name = None):
		if config_file_name:
			self._config_file_name = config_file_name
		self._genome = genome
	#@-body
	#@-node:1::__init__
	#@+node:2::GetRegulatoryRegions
	#@+body
	def GetRegulatoryRegionRanges(self, feature_name):
		raise NotImplementedError
	
	#@-body
	#@-node:2::GetRegulatoryRegions
	#@-others
#@-body
#@-node:2::<<class RegulatoryRegionModel>>


#@-body
#@-node:0::@file regulatory_region_model_gomer.py
#@-leo
