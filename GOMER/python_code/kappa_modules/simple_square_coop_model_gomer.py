#! /usr/bin/env python
from __future__ import division
#@+leo
#@+node:0::@file simple_square_coop_model_gomer.py
#@+body
#@@first
#@@first
#@@language python
#@@path kappa_modules/ 


#@<<imports>>
#@+node:1::<<imports>>
#@+body
##import os
##os.environ['PYCHECKER'] = '--allglobals --stdlib'
##import pychecker.checker

##import check_python_version
##check_python_version.CheckVersion()

import types
from exceptions_gomer import GomerError
#@-body
#@-node:1::<<imports>>



#@<<Name>>
#@+node:2::<<Name>>
#@+body
NAME = 'SimpleSquareCoopModel'

#@-body
#@-node:2::<<Name>>


#@<<Type>>
#@+node:3::<<Type>>
#@+body
TYPE = 'CooperativityModel'

#@-body
#@-node:3::<<Type>>



#@<<Parameters>>
#@+node:4::<<Parameters>>
#@+body
PARAMETERS = {'max_distance' : types.IntType, 'min_distance' : types.IntType}


#@-body
#@-node:4::<<Parameters>>


#@<<Description>>
#@+node:5::<<Description>>
#@+body
DESCRIPTION = \
"""
SimpleSquareCoopModel is a cooperativity model.



It represents a simple square function.

The only thing that determines the Kappa value of this model is distance.  There are three criteria involved in determining the Kappa value of this model:
	1. If the sites overlap (clash) the kappa value is zero.
	2. If the distance between the two binding sites is less than or equal to max_distance, the kappa value is one.
	3. Otherwise, the kappa value is zero.
	
The distance between two sites is determined by the distance between their two closest ends.  Strand is not taken into consideration.  This table should explaing the SimpleSquareCoopModel using two transcription factors, X and Y, where the max_distance is 10.

It should be noted that the SimpleSquareCoopModel allows for secondary sites on either side of the primary site.


max_distance = 10   				 
					 									 
    X     |    Y     | 	   	  |		   |
----------|----------| 	   	  |	    |
left|right|left|right|Distance|Kappa|
----------|----------|--------|-----|
 20 | 30  | 31 | 43  | 	  0	  |	 1  |
----------|----------|--------|-----|
 25 | 35  | 31 | 43  | overlap|	 0  |
----------|----------|--------|-----|
 20 | 30  | 30 | 42  | overlap|	 0  |
----------|----------|--------|-----|
 20 | 30  |  5 | 17  | 	  2	  |	 1  |
----------|----------|--------|-----|
 20 | 30  | 45 | 57  | 	  14  |	 0  |


Parameter String Example:
	"max_distance=50;min_distance=10"

"""
#@-body
#@-node:5::<<Description>>

REQUIRES_STRAND = False
PARAMETER_STRING_EXAMPLE = ("max_distance=50; min_distance=10",)




#@<<class SimpleSquareCoopModel>>
#@+node:6::<<class SimpleSquareCoopModel>>
#@+body
class CoopModel(object):

	#@+others
	#@+node:1::__init__
	#@+body
	def __init__(self,
				 primary_TF_binding_model, 
				 secondary_TF_binding_model, 
				 max_distance, min_distance):

	#@+doc
	# 
	# It is very important that primary_TF_binding_model and 
	# secondary_TF_binding_model are not switched!!
	# 

	#@-doc
	#@@code

		# self._genome = genome
		if max_distance < 0:
			raise CoopError, ' ZERO'	
		elif min_distance < 0:
			raise CoopError, 'min_distance must be greater than ZERO'	
		elif max_distance < min_distance :
			raise CoopError, 'max_distance must be greater than or equal to min_distance'	
		self._primary_TF_binding_model = primary_TF_binding_model
		self._primary_TF_length = primary_TF_binding_model.Length
		self._secondary_TF_binding_model = secondary_TF_binding_model
		self._secondary_TF_length = secondary_TF_binding_model.Length
	
		self._max_distance = max_distance
		self._min_distance = min_distance
	
		## In order to save computation, the value of coop_ranges most is cached, 
		## this is stored in self._cached_coop_ranges, so if this is requested again, it doesn't
		## need to be recalculated
		#self._cached_primary_TF_name = None
		self._cached_primary_site_hitindex = None
		self._cached_coop_ranges = None
		self._cached_feature = None
	#@-body
	#@-node:1::__init__
	#@+node:2::GetCoopRanges
	#@+body

	##--------------------------------------------------------------------------
	def GetSlices(self, primary_site_hitindex, primary_site_strand, feature):
		coop_ranges = self.GetCoopRanges(primary_site_hitindex, primary_site_strand, feature)
		## need to add 1 to the "slice stop" value, since these values are to be used directly for slices,
		## the stop value is the index after the last element included in the slice, not the last element included in the slice
		coop_ranges = [(start, stop+1) for start, stop in coop_ranges]
		tuple_list = []
		for start, stop in coop_ranges:
			slice_length = stop - start
			ones   = (1,) * slice_length
			kappa_slice = ones
			tuple_list.append((start, stop, kappa_slice, kappa_slice))
		## return list of tuples of (start_coop_slice, stop_coop_slice, for_kappa_slice, rev_kappa_slice)
		return tuple_list
	##--------------------------------------------------------------------------


	def GetCoopRanges(self, primary_site_hitindex, primary_site_strand, feature):

	#@+doc
	# 
	# IMPORTANT!!!!
	# Ranges returned by this function are ranges in "hitlist space" - they 
	# are represent the smallest
	# and largest indices of the hitlist that define the range of binding 
	# sites for which the site
	# kappa is non-zero - they are NOT base pair numbers (because the hitlist 
	# array is numbered starting at 0, and bps are numbered starting at 1, 
	# (hitlist index) = bp number - 1).
	# 

	#@-doc
	#@@code

		if (self._cached_primary_site_hitindex == primary_site_hitindex) and \
			(self._cached_feature == feature):
			return self._cached_coop_ranges
		
		#----------------------------------------------
		left_range_start = ((primary_site_hitindex - self._max_distance) - self._secondary_TF_length)
		left_range_stop = ((primary_site_hitindex - self._min_distance) - self._secondary_TF_length)
		right_range_start = (primary_site_hitindex + self._min_distance + self._primary_TF_length)
		right_range_stop = (primary_site_hitindex + self._max_distance + self._primary_TF_length)
		#----------------------------------------------
		chrom = feature.Chromosome
		hitlist_size = chrom.GetHitListSize(self._secondary_TF_binding_model.MD5)
	
		range_list = [] 
	
		if (left_range_stop < 0) or (left_range_start >= hitlist_size):
			pass
		else:
			if left_range_start < 0:
				left_range_start = 0
			if left_range_stop >= hitlist_size:
				left_range_stop = hitlist_size - 1
			left_range = (left_range_start, left_range_stop)
			range_list.append(left_range)
		#----------------------------------------------
		#----------------------------------------------
		if (right_range_stop < 0) or (right_range_start >= hitlist_size):
			pass
		else:
			if right_range_start < 0:
				right_range_start = 0
			if right_range_stop >= hitlist_size:
				right_range_stop = hitlist_size - 1
			right_range = (right_range_start, right_range_stop)
			range_list.append(right_range)
		#----------------------------------------------
		
		self._cached_primary_site_hitindex = primary_site_hitindex
		self._cached_feature = feature
		self._cached_coop_ranges = tuple(range_list)
		return self._cached_coop_ranges
	
	#@-body
	#@+node:1::test_coop_ranges
	#@+body
	#@+doc
	# 
	# print """
	# secondary_TF_hitindex = 3          # <-------- left_range_start
	# secondary_TF_hitindex = 59         #  <-------- right_range_stop
	# secondary_TF_hitindex = 13         #  <-------- left_range_stop
	# secondary_TF_hitindex = 49         #  <-------- right_range_start
	# """
	# class Blah:
	# 	pass
	# 
	# self = Blah()
	# self.max_distance = 20
	# self.min_distance = 10
	# self._primary_TF_length = 9
	# self._secondary_TF_length = 7
	# primary_site_hitindex = 30
	# 
	# left_range_start = ((primary_site_hitindex - self.max_distance) - self._secondary_TF_length)
	# left_range_stop = ((primary_site_hitindex - self.min_distance) - self._secondary_TF_length)
	# right_range_start = (primary_site_hitindex + self.min_distance + self._primary_TF_length)
	# right_range_stop = (primary_site_hitindex + self.max_distance + self._primary_TF_length)
	# 
	# print 'left_range_start', left_range_start
	# print 'left_range_stop', left_range_stop
	# print 'right_range_start', right_range_start
	# print 'right_range_stop', right_range_stop

	#@-doc
	#@-body
	#@-node:1::test_coop_ranges
	#@+node:2::test_conditions
	#@+body
	#@+doc
	# 
	# Secondary = seconds
	# Primary =   primarypr
	# 
	# primary_TF_length = 9
	# secondary_TF_length = 7
	# #===============================================================================
	#           1         2         3         4         5         6         7
	# 01234567890123456789012345678901234567890123456789012345678901234567890 hitindex
	#    seconds                    primarypr
	# 
	# primary_TF_hitindex = 30
	# secondary_TF_hitindex = 3          <-------- left_range_start
	# #------------------------------
	# primary_TF_end_hitindex = 38
	# secondary_TF_end_hitindex = 9
	# #------------------------------
	# distance = 20
	# #===============================================================================
	#           1         2         3         4         5         6         7
	# 01234567890123456789012345678901234567890123456789012345678901234567890 hitindex
	#              seconds          primarypr
	# 
	# primary_TF_hitindex = 30
	# secondary_TF_hitindex = 13          <-------- left_range_stop
	# #------------------------------
	# primary_TF_end_hitindex = 38
	# secondary_TF_end_hitindex = 19
	# #------------------------------
	# distance = 20
	# #===============================================================================
	#           1         2         3         4         5         6         7
	# 01234567890123456789012345678901234567890123456789012345678901234567890 hitindex
	#                               primarypr          seconds
	# 
	# primary_TF_hitindex = 30
	# secondary_TF_hitindex = 49          <-------- right_range_start
	# #------------------------------
	# primary_TF_end_hitindex = 38
	# secondary_TF_end_hitindex = 55
	# #------------------------------
	# distance = 20
	# #===============================================================================
	#           1         2         3         4         5         6         7
	# 01234567890123456789012345678901234567890123456789012345678901234567890 hitindex
	#                               primarypr                    seconds
	# 
	# primary_TF_hitindex = 30
	# secondary_TF_hitindex = 59          <-------- right_range_stop
	# #------------------------------
	# primary_TF_end_hitindex = 38
	# secondary_TF_end_hitindex = 65
	# #------------------------------
	# distance = 20
	# #=====================================================================================================
	# self.max_distance = 20
	# self.min_distance = 10
	# self._primary_TF_length = 9
	# self._secondary_TF_length = 7
	# primary_site_hitindex = 30

	#@-doc
	#@-body
	#@-node:2::test_conditions
	#@-node:2::GetCoopRanges
	#@+node:3::GetSiteKappa
	#@+body
	def GetSiteKappa(self, primary_site_hitindex, primary_site_strand,
					 secondary_site_hitindex, secondary_site_strand, feature):

	#@+doc
	# 
	# Since this is a square function, the kappa is one if the binding site is 
	# in the regulatory region,
	# otherwise the kappa is zero.  So all we have to do is see if the site 
	# falls in the one of the ranges returned by GetCoopRanges.

	#@-doc
	#@@code
		if (self._cached_primary_site_hitindex == primary_site_hitindex) and \
			(self._cached_feature == feature):
			range_pairs = self._cached_coop_ranges
		else:
			range_pairs = self.GetCoopRanges(primary_site_hitindex, primary_site_strand, feature)
	
		kappa_value = 0
		for start_hitindex, stop_hitindex in range_pairs: 
			if start_hitindex <= secondary_site_hitindex <= stop_hitindex :
				kappa_value = 1
				break
		return kappa_value
	
	
	
	
	#@-body
	#@-node:3::GetSiteKappa
	#@+node:4::_distance
	#@+body
	def _distance(self, primary_TF_hitindex, secondary_TF_hitindex):

	#@+doc
	# 
	# It is very important that primary_TF_binding_model and 
	# secondary_TF_binding_model are not switched!!
	# 

	#@-doc
	#@@code
		primary_TF_end_hitindex =  (primary_TF_hitindex + self._primary_TF_length) - 1
		secondary_TF_end_hitindex =  (secondary_TF_hitindex + self._secondary_TF_length) - 1
		
		distance = (max((primary_TF_hitindex - secondary_TF_end_hitindex), (secondary_TF_hitindex - primary_TF_end_hitindex))) - 1
		## distance is the number of bp between the two sites
	    ## Therefore if last base of site X is 10,
		## and first base of site Y is 11, distance is 0
	
		if distance < 0:
			return None
		else:
			return distance
	
	

	#@+doc
	# 
	# ALL THE TESTS BELOW WORK CORRECTLY
	# #=====================================================================================================
	# #=====================================================================================================
	# Secondary = seconds
	# Primary =   primarypr
	# 
	# primary_TF_length = 9
	# secondary_TF_length = 7
	# #=====================================================================================================
	# 
	#           1         2         3         4         5         6         
	# 7         8         9
	# 0123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890 hitindex
	#              seconds                    primarypr
	# 
	# primary_TF_hitindex = 40
	# secondary_TF_hitindex = 13
	# #------------------------------
	# primary_TF_end_hitindex = 48
	# secondary_TF_end_hitindex = 19
	# #------------------------------
	# distance = 20
	# #=====================================================================================================
	#           1         2         3         4         5         6         
	# 7         8         9
	# 0123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890 hitindex
	#                       primarypr                 seconds
	# 
	# primary_TF_hitindex = 22
	# secondary_TF_hitindex = 48
	# #------------------------------
	# primary_TF_end_hitindex = 30
	# secondary_TF_end_hitindex = 54
	# #------------------------------
	# distance = 17
	# #=====================================================================================================
	#           1         2         3         4         5         6         
	# 7         8         9
	# 0123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890 hitindex
	#                       primarypr seconds
	# 
	# primary_TF_hitindex = 22
	# secondary_TF_hitindex = 32
	# #------------------------------
	# primary_TF_end_hitindex = 30
	# secondary_TF_end_hitindex = 38
	# #------------------------------
	# distance = 1
	# #=====================================================================================================
	#           1         2         3         4         5         6         
	# 7         8         9
	# 0123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890 hitindex
	#                       primarypr
	# 					           seconds
	# 
	# primary_TF_hitindex = 22
	# secondary_TF_hitindex = 31
	# #------------------------------
	# primary_TF_end_hitindex = 30
	# secondary_TF_end_hitindex = 37
	# #------------------------------
	# distance = 0
	# #=====================================================================================================
	#           1         2         3         4         5         6         
	# 7         8         9
	# 0123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890 hitindex
	#                       seconds
	# 					         primarypr
	# 
	# primary_TF_hitindex = 29
	# secondary_TF_hitindex = 22
	# #------------------------------
	# primary_TF_end_hitindex = 37
	# secondary_TF_end_hitindex = 28
	# #------------------------------
	# distance = 0
	# =====================================================================================================
	#           1         2         3         4         5         6         
	# 7         8         9
	# 0123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890 hitindex
	#                       primarypr
	# 					       seconds
	# 
	# primary_TF_hitindex = 22
	# secondary_TF_hitindex = 27
	# #------------------------------
	# primary_TF_end_hitindex = 30
	# secondary_TF_end_hitindex = 33
	# #------------------------------
	# distance = 1-maxint
	# #=====================================================================================================
	#           1         2         3         4         5         6         
	# 7         8         9
	# 0123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890 hitindex
	#                       primarypr
	# 					 seconds
	# 
	# primary_TF_hitindex = 22
	# secondary_TF_hitindex = 21
	# #------------------------------
	# primary_TF_end_hitindex = 30
	# secondary_TF_end_hitindex = 27
	# #------------------------------
	# distance = 1-maxint
	# #=====================================================================================================
	#           1         2         3         4         5         6         
	# 7         8         9
	# 0123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890 hitindex
	#                       primarypr
	# 					  seconds
	# 
	# primary_TF_hitindex = 22
	# secondary_TF_hitindex = 22
	# #------------------------------
	# primary_TF_end_hitindex = 30
	# secondary_TF_end_hitindex = 28
	# #------------------------------
	# distance = 1-maxint
	# #=====================================================================================================
	#           1         2         3         4         5         6         
	# 7         8         9
	# 0123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890 hitindex
	#                       primarypr
	# 					   seconds
	# 
	# primary_TF_hitindex = 22
	# secondary_TF_hitindex = 23
	# #------------------------------
	# primary_TF_end_hitindex = 30
	# secondary_TF_end_hitindex = 29
	# #------------------------------
	# distance = 1-maxint
	# #=====================================================================================================
	#           1         2         3         4         5         6         
	# 7         8         9
	# 0123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890 hitindex
	#                       primarypr
	# 					    seconds
	# 
	# primary_TF_hitindex = 22
	# secondary_TF_hitindex = 24
	# #------------------------------
	# primary_TF_end_hitindex = 30
	# secondary_TF_end_hitindex = 30
	# #------------------------------
	# distance = 1-maxint

	#@-doc
	#@@code

	
	#@-body
	#@-node:4::_distance
	#@-others

	

#@<<SimpleSquareCoopError>>
#@+node:5::<<SimpleSquareCoopError>>
#@+body
class CoopError(GomerError):
	pass

#@-body
#@-node:5::<<SimpleSquareCoopError>>
#@-body
#@-node:6::<<class SimpleSquareCoopModel>>
#@-body
#@-node:0::@file simple_square_coop_model_gomer.py
#@-leo
