#! /usr/bin/env python

"""
SameStrandCodingAndFlanksRegulatoryRegionModel is a regulatory region model

It represents a single square function from "five_prime_of_feature" upstream of the START (of the coding region) to "three_prime_of_feature" downstream of the END (of the coding region) of the Feature in question.  

The values of "five_prime_of_feature" and "three_prime_of_feature" must be integers, they can be zero (i.e. the regulatory region doesn't extend past the feature at the relavant end) and negative (i.e. for a value of -X, the regulatory region excludes the X bp at the relavant end).  The values do not need to be the same.

SameStrandCodingAndFlanksRegulatoryRegionModel considers only those binding sites that are in the range from from "five_prime_of_feature" upstream of the START to "three_prime_of_feature" downstream of the END  the Feature in question, which are on the same strand as the feature.

This is similar to the model DownstreamSameStrandSingleSquareRegulatoryRegionModel
but it includes the "coding region", and "upstream region" of the feature
This regulatory region is a single square function - any site within the regulatory region, on the same strand as the feature, has a site_kappa  of 1, all other sites have site_kappas of zero.
"""

DESCRIPTION = __doc__

from __future__ import division
division ## should raise an exception if the import didn't work


import types
from exceptions_gomer import GomerError
from globals_gomer import PLUS_STRAND, MINUS_STRAND

NAME = 'SameStrandCodingAndFlanksRegulatoryRegionModel'

TYPE = 'RegulatoryRegionModel'

PARAMETERS = {'five_prime_of_feature' : types.IntType,
			  'three_prime_of_feature': types.IntType}

PARAMETER_STRING_EXAMPLE = ("five_prime_of_feature=50; three_prime_of_feature=100",)

REQUIRES_STRAND = True
class RegulatoryRegionModel(object):

	def __init__(self,
				 binding_site_model, 
				 five_prime_of_feature,
				 three_prime_of_feature):
		self._five_prime_of_feature = five_prime_of_feature
		self._three_prime_of_feature = three_prime_of_feature
		self._binding_site_model = binding_site_model
		self._site_model_length = binding_site_model.Length
	
		## In order to save computation, the most recently calculated regulatory region
		## is cached, this is stored in self._cached_regulatory_region, the feature that this 
		## corresponds to is saved in self._cached_feature, so if this is requested again, it doesn't
		## need to be recalculated
		self._cached_regulatory_region = None
		self._cached_feature = None

	def GetRegulatoryRegionRanges(self, feature):

	# IMPORTANT!!!!
	# Ranges returned by this function are ranges in "hitlist space" - they 
	# are represent the smallest
	# and largest indices of the hitlist that define the range of binding 
	# sites for which the site
	# kappa is non-zero - they are NOT base pair numbers (because the hitlist 
	# array is numbered starting at 0, and bp's are numbered starting at 1, 
	# (hitlist index) = bp number - 1).

		# cur_ORF = self._genome.GetFeature(ORF_name)
		if 	feature == self._cached_feature:
			return self._cached_regulatory_region
	
		if (self._three_prime_of_feature + self._five_prime_of_feature + feature.Length) < self._site_model_length:
			raise RegulatoryRegionError(
				'The regulatory region is smaller than the binding site:' +
				'\nfive_prime_of_feature: ' + str(self._five_prime_of_feature) +
				'\nthree_prime_of_feature: '+ str(self._three_prime_of_feature) +
				'\nfeature.Length: '+ str(feature.Length) +
				'\nbinding_site_length: ' + str(self._site_model_length) + '\n\n'
				)

		cur_chrom = feature.Chromosome
		strand = feature.Strand
		hitlist_size = cur_chrom.GetHitListSize(self._binding_site_model.MD5)
		
		
		if strand == PLUS_STRAND:
			start_hitlist_index = (feature.Start - self._five_prime_of_feature) - 1
			# subtract one to account for the fact the the hitlist index starts at zero 
			stop_hitlist_index = ((feature.Stop + self._three_prime_of_feature) - self._site_model_length) # + 1) - 1)
			# plus one for the binding site length, minus one for the hitlist offset 

	
		elif strand == MINUS_STRAND:
			start_hitlist_index = (feature.Stop - self._three_prime_of_feature) - 1
			# subtract one to account for the fact the the hitlist index starts at zero 
			stop_hitlist_index = ((feature.Start + self._five_prime_of_feature) - self._site_model_length) # + 1) - 1)
			# plus one for the binding site length, minus one for the hitlist offset 


		else:
			raise ValueError, 'Strand must be "'+PLUS_STRAND+'" or "'+MINUS_STRAND+'". "' + str(strand) + '" is not acceptable\n' 
	
		if stop_hitlist_index < start_hitlist_index:
			raise RegulatoryRegionError(
				'The stop_index cannot be smaller than the start_index:' +
				'\nstart_index: ' +	str(start_hitlist_index) +
				'\nstop_index:  ' + str(stop_hitlist_index) +
				'\n\nfive_prime_of_feature: ' + str(self._five_prime_of_feature) +
				'\nthree_prime_of_feature: '+ str(self._three_prime_of_feature) +
				'\nbinding_site_length: ' + str(self._site_model_length) + '\n\n'
				)
			
		if stop_hitlist_index < 0 :
			# return tuple((tuple(()),))
			return tuple()
		elif start_hitlist_index < 0:
			start_hitlist_index = 0
	
		if start_hitlist_index >= hitlist_size:
			# return tuple((tuple(()),))
			return tuple()
		elif stop_hitlist_index >= hitlist_size:
			stop_hitlist_index = hitlist_size - 1
	
		self._cached_feature = feature
		self._cached_regulatory_region = tuple((tuple((start_hitlist_index, stop_hitlist_index)),))
			
		return self._cached_regulatory_region
	
	def GetSiteKappa(self, feature, hitlist_index, site_strand):

	# Since this is a square function, the kappa is one if the binding site is 
	# in the regulatory region,
	# otherwise the kappa is zero

		if 	feature == self._cached_feature:
			((start_hitlist_index, stop_hitlist_index),) = self._cached_regulatory_region
		else:
			((start_hitlist_index, stop_hitlist_index),) = self.GetRegulatoryRegionRanges(feature)	
		# if start_hitlist_index <= (base_position - 1) <= stop_hitlist_index :
		if (start_hitlist_index <= hitlist_index <= stop_hitlist_index) and \
			site_strand == feature.Strand :
			kappa_value = 1
		elif (site_strand <> PLUS_STRAND) and (site_strand <> MINUS_STRAND):
			error_message = 'site_strand must be "' + PLUS_STRAND + '" or "' + MINUS_STRAND + '"\n'
			raise RegulatoryRegionError(error_message)
		else :
			kappa_value = 0
			
		return kappa_value
	
	
	
class RegulatoryRegionError(GomerError):
	pass
