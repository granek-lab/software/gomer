#@+leo
#@comment Created by Leo at Thu Oct 23 15:10:35 2003
#@+node:0::@file coordinate_linear_decay_reg_region_model_gomer.py
#@+body
#@@language python
#@@path kappa_modules/ 


"""
CoordinateLinearDecayRegulatoryRegionModel

----------------------------------------------------------
----------------------------------------------------------
Things to test:
   1. Regulatory region and square regions boudaries
   2. Compare to Flank
   3. Compare to regular coordinate regulatory region kappa
----------------------------------------------------------
----------------------------------------------------------

"""


#@<<imports>>
#@+node:1::<<imports>>
#@+body
from __future__ import division

import types
import sys
from exceptions_gomer import GomerError
#@-body
#@-node:1::<<imports>>



#@<<Name>>
#@+node:2::<<Name>>
#@+body
NAME = 'CoordinateLinearDecayRegulatoryRegionModel'

#@-body
#@-node:2::<<Name>>


#@<<Type>>
#@+node:3::<<Type>>
#@+body
TYPE = 'CoordinateRegulatoryRegionModel'

#@-body
#@-node:3::<<Type>>



#@<<Parameters>>
#@+node:4::<<Parameters>>
#@+body
PARAMETERS = {'flank' : types.IntType,
			  'tail' : types.IntType}


#@-body
#@-node:4::<<Parameters>>


#@<<Description>>
#@+node:5::<<Description>>
#@+body
DESCRIPTION = \
"""
This Kappa function is somewhat similar to CoordinateFlankSquareRegulatoryRegionModel in that it allows for "flanks" on feature/regulatory regions defined by coordinates.  However 

flank - an integer
tail - a non-negative integer

The "flank" parameter defines a point we'll call flank_bp, which is flank base pairs relative to the ends of the coordinate feature (so it can technically be negative, which would result in a flank_bp within the coordinate feature).

The "tail" parameter defines a point we'll call tail_bp, which is tail base pairs distal to flank_bp (tail must be zero or positive).

The kappa function has a value of 1 between the two flank_bp points defined by the two ends of the coordinate feature.  At each end, from flank_bp to tail_bp, the value of the kappa function decays linearly from 1 at flank_bp to 0 at tail_bp.

From this description, it should be clear that CoordinateLinearDecayRegulatoryRegionModel can replicate CoordinateFlankSquareRegulatoryRegionModel with a "flank" value the same as flank, and a "tail" value of zero.  Similarly, the CoordinateSingleSquareRegulatoryRegionModel can be modeled with "flank" and "tail" values of zero.

This model is useful for analyzing 
"""


#@-body
#@-node:5::<<Description>>


#@<<Parameter Example>>
#@+node:6::<<Parameter Example>>
#@+body
PARAMETER_STRING_EXAMPLE = ("flank=400; tail=200",)

#@-body
#@-node:6::<<Parameter Example>>


#@<<Requires Strand>>
#@+node:7::<<Requires Strand>>
#@+body
REQUIRES_STRAND = False
#@-body
#@-node:7::<<Requires Strand>>



#@<<class CoordinateLinearDecayRegulatoryRegionModel>>
#@+node:8::<<class CoordinateLinearDecayRegulatoryRegionModel>>
#@+body
class RegulatoryRegionModel(object):

	#@+others
	#@+node:1::__init__
	#@+body
	def __init__(self,
				 binding_site_model,
				 flank, tail):
		# self._genome = genome
		self._binding_site_model = binding_site_model
		self._site_length = binding_site_model.Length
		self._flank = flank
		if tail < 0 :
			raise CoordinateRegulatoryRegionError('Tail must be non-negative.  Supplied value is: ' + str(tail))
		self._tail = tail
		##------------------------------------------------------
		## Decay function
		##------------------------------------------------------
		# y = mx + b
##		# left decay function
##		self._left_b = 0
##		self._left_m = (0 - 1)/(0 - tail) # rise over run
		
##		# right decay function
##		self._right_b = 1
##		self._right_m = (1 - 0)/(0 - tail) # rise over run
		self._b = 1
		if tail == 0 :
			self._m = None
		else:
			self._m = (1 - 0)/(0 - tail) # rise over run
		##------------------------------------------------------

		
		## In order to save computation, the most recently calculated regulatory region
		## is cached, this is stored in self._cached_regulatory_region, the feature that this 
		## corresponds to is saved in self._cached_feature, so if this is requested again, it doesn't
		## need to be recalculated
		self._cached_regulatory_region = None
		self._cached_feature = None

		self._cached_square_region = None
		self._cached_feature__square = None

	#@+doc
	# 
	# the regulatory region is inclusive :

	#@-doc
	#@@code
	#@-body
	#@-node:1::__init__
	#@+node:2::GetRegulatoryRegionRanges
	#@+body
	# def GetRegulatoryRegionRanges(self, ORF_name):
	def GetRegulatoryRegionRanges(self, coordinate_feature):

	#@+doc
	# 
	# IMPORTANT!!!!
	# Ranges returned by this function are ranges in "hitlist space" - they 
	# are represent the smallest
	# and largest indices of the hitlist that define the range of binding 
	# sites for which the site
	# kappa is non-zero - they are NOT base pair numbers (because the hitlist 
	# array is numbered starting at 0, and bp's are numbered starting at 1, 
	# (hitlist index) = bp number - 1).

	#@-doc
	#@@code

		# cur_ORF = self._genome.GetFeature(ORF_name)
		if 	coordinate_feature == self._cached_feature:
			return self._cached_regulatory_region
	
		cur_chrom = coordinate_feature.Chromosome
		hitlist_size = cur_chrom.GetHitListSize(self._binding_site_model.MD5)
	

	#@+doc
	# 
	# 	if ((self._regulatory_five_prime - self._regulatory_three_prime) + 1) < binding_site_length:
	# 		raise SingleSquareRegulatoryRegionError(
	# 			'The regulatory region is smaller than the binding site:' +
	# 			'\nregulatory_five_prime: ' + str(self._regulatory_five_prime) +
	# 			'\nregulatory_three_prime: '+ str(self._regulatory_three_prime) +
	# 			'\nbinding_site_length: ' + str(binding_site_length) + '\n\n'
	# 			)

	#@-doc
	#@@code

	##----------------------------------------------------------------------
	##
	##     1                                     1     
	##     0                                     3   BP
	##     0                                     8
	## ttttaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaatttt
	## 9   9                                     1
	## 5   9                                     3   hitindex    kappa
	##                                           7
	## cccccccccc                                        95        0
	##  cccccccccc                                       96        0
	##   cccccccccc                                      97        0
	##    cccccccccc                                     98        0
	##     cccccccccc                                    99        1
	##      cccccccccc                                   100       1
	##                                 cccccccccc        127       1
	##                                  cccccccccc       128       1
	##                                   cccccccccc      129       0
	##                                    cccccccccc     130       0
	## 
	## binding site length = 10
	## intergenic region: 100bp to 138bp
	## regultory region: 99 to 128 (hitindex)
	##----------------------------------------------------------------------
	
		start_hitlist_index = (coordinate_feature.Start - (self._flank + self._tail)) - 1
		# subtract one to account for the fact the the hitlist index starts at zero 
		stop_hitlist_index = (coordinate_feature.Stop - self._site_length) + self._flank + self._tail # + 1) - 1)
		# plus one for the binding site length, minus one for the hitlist offset 

	#@+doc
	# 
	# 	the regulatory region is inclusive :
	# 		if the Intergenic region runs from 100bp to 138bp
	# 		that means that any binding site that starts at bp #100 or after,
	# 		and ends at bp #138 or before is in the regulatory region
	# 
	# 	if the binding site is 10bp, then the hitlist index range should be (99,128)

	#@-doc
	#@@code

		if stop_hitlist_index < start_hitlist_index:
			if (self._flank + self._tail) <0:
				self._cached_feature = coordinate_feature
				self._cached_regulatory_region = tuple()
				return self._cached_regulatory_region
			else:
				raise CoordinateRegulatoryRegionError(' '.join((
					'The stop_index cannot be smaller than the start_index:',
					'\nstart_index:', str(start_hitlist_index), 
					'\nstop_index:', str(stop_hitlist_index),
					'\nbinding_site_length:', str(self._site_length), 
					'\nFeature:', coordinate_feature.Name,
					', Start:', str(coordinate_feature.Start),
					', Stop:', str(coordinate_feature.Stop),
					'\n\n')))
			
		if stop_hitlist_index < 0 :
			self._cached_feature = coordinate_feature
			self._cached_regulatory_region = tuple()
			return self._cached_regulatory_region
		elif start_hitlist_index < 0:
			start_hitlist_index = 0
	
		if start_hitlist_index >= hitlist_size:
			# return tuple((tuple(()),))
			self._cached_feature = coordinate_feature
			self._cached_regulatory_region = tuple()
			return self._cached_regulatory_region
		elif stop_hitlist_index >= hitlist_size:
			stop_hitlist_index = hitlist_size - 1
	
	
		self._cached_feature = coordinate_feature
		self._cached_regulatory_region = tuple((tuple((start_hitlist_index, stop_hitlist_index)),))
			
		return self._cached_regulatory_region
##------------------------------------------------------------
	def _get_square_region_ranges(self, coordinate_feature):
		cur_chrom = coordinate_feature.Chromosome
		hitlist_size = cur_chrom.GetHitListSize(self._binding_site_model.MD5)
	
		start_square_hitlist_index = (coordinate_feature.Start - self._flank) - 1
		# subtract one to account for the fact the the hitlist index starts at zero 
		stop_square_hitlist_index = (coordinate_feature.Stop - self._site_length) + self._flank# + 1) - 1)

##		if stop_square_hitlist_index < start_square_hitlist_index:
##			if self._flank<0:
##				self._cached_feature__square = coordinate_feature
##				self._cached_square_region = tuple()
##				return self._cached_square_region
##			else:
##				raise CoordinateRegulatoryRegionError(' '.join((
##					'The stop_square_index cannot be smaller than the start_square_index:',
##					'\nstart_square_index:', str(start_square_hitlist_index), 
##					'\nstop_square_index:', str(stop_square_hitlist_index),
##					'\nbinding_site_length:', str(self._site_length), 
##					'\nFeature:', coordinate_feature.Name,
##					', Start:', str(coordinate_feature.Start),
##					', Stop:', str(coordinate_feature.Stop),
##					'\n\n')))
			
##		if stop_square_hitlist_index < 0 :
##			self._cached_feature__square = coordinate_feature
##			self._cached_square_region = tuple()
##			return self._cached_square_region
##		elif start_square_hitlist_index < 0:
##			start_square_hitlist_index = 0
	
##		if start_square_hitlist_index >= hitlist_size:
##			# return tuple((tuple(()),))
##			self._cached_feature__square = coordinate_feature
##			self._cached_square_region = tuple()
##			return self._cached_square_region
##		elif stop_square_hitlist_index >= hitlist_size:
##			stop_square_hitlist_index = hitlist_size - 1
	
	
		self._cached_feature__square = coordinate_feature
		self._cached_square_region = tuple((tuple((start_square_hitlist_index, stop_square_hitlist_index)),))
			
		return self._cached_square_region
##------------------------------------------------------------	
	
	#@-body
	#@-node:2::GetRegulatoryRegionRanges
	#@+node:3::GetSiteKappa
	#@+body
	# def GetSiteKappa(self, Orf, base_position):
	def GetSiteKappa(self, coordinate_feature, hitlist_index, site_strand):

	#@+doc
	# 
	# Since this is a square function, the kappa is one if the binding site is 
	# in the regulatory region,
	# otherwise the kappa is zero

	#@-doc
	#@@code
		if 	coordinate_feature == self._cached_feature:
			((start_hitlist_index, stop_hitlist_index),) = self._cached_regulatory_region
		else:
			((start_hitlist_index, stop_hitlist_index),) = self.GetRegulatoryRegionRanges(coordinate_feature)	

		if 	coordinate_feature == self._cached_feature__square:
			((start_square_hitlist_index, stop_square_hitlist_index),) = self._cached_square_region
		else:
			try:
				((start_square_hitlist_index, stop_square_hitlist_index),) = self._get_square_region_ranges(coordinate_feature)
			except ValueError:
				print self._get_square_region_ranges(coordinate_feature)
				print "(start_hitlist_index, stop_hitlist_index) - (", start_hitlist_index, '), (', stop_hitlist_index, ')'
				raise

		if hitlist_index < start_hitlist_index or stop_hitlist_index < hitlist_index:
			# if the site is outside the regulatory region, the kappa is zero
			kappa_value = 0
		elif start_square_hitlist_index <= hitlist_index <= stop_square_hitlist_index :
			# if the site is within the "square" region of the function, the kappa is one
			# start_square_hitlist_index, stop_square_hitlist_index mark the left and right ends, respectively of the square function
			kappa_value = 1
		# otherwise, the site is in the tail region, and the kappa is determined by the linear decay function

##------------------------------------------------------------------------------
##		elif start_hitlist_index <= hitlist_index < start_square_hitlist_index:
##			# hit is in the left tail
##			tail_x = start_square_hitlist_index - hitlist_index
##			## kappa_value = (tail_x * self._left_m) + self._left_b
##			kappa_value = (tail_x * self._m) + self._b
##		elif stop_square_hitlist_index < hitlist_index <= stop_hitlist_index:
##			# hit is in the right tail
##			tail_x = hitlist_index - stop_square_hitlist_index
##			## kappa_value = (tail_x * self._right_m) + self._right_b
##			kappa_value = (tail_x * self._m) + self._b
##------------------------------------------------------------------------------
		elif start_hitlist_index <= hitlist_index <= stop_hitlist_index:
			# if the site is outside the regulatory region, the kappa is zero
			kappa_value = 0
			l_tail_x = start_square_hitlist_index - hitlist_index
			r_tail_x = hitlist_index - stop_square_hitlist_index
			l_value = (l_tail_x * self._m) + self._b
			r_value = (r_tail_x * self._m) + self._b
			kappa_value = min((l_value, r_value))
		else:
			raise CoordinateRegulatoryRegionError('Regulatory region problem')
			
		return kappa_value
	
	#@-body
	#@-node:3::GetSiteKappa
	#@-others

	

#@<<CoordinateRegulatoryRegionError>>
#@+node:4::<<CoordinateRegulatoryRegionError>>
#@+body
class CoordinateRegulatoryRegionError(GomerError):
	pass

#@-body
#@-node:4::<<CoordinateRegulatoryRegionError>>
#@-body
#@-node:8::<<class CoordinateLinearDecayRegulatoryRegionModel>>
#@-body
#@-node:0::@file coordinate_linear_decay_reg_region_model_gomer.py
#@-leo
