#! /usr/bin/env python

from __future__ import division

import types
from exceptions_gomer import GomerError
## from globals_gomer import FORWARD, REVCOMP
from globals_gomer import PLUS_STRAND, MINUS_STRAND

NAME = 'StrandSquareCoopModel'

TYPE = 'CooperativityModel'

PARAMETERS = {'max_distance' : types.IntType, 'min_distance' : types.IntType, 'strand' : types.StringType}

DESCRIPTION = \
"""
StrandSquareCoopModel is a cooperativity model.

It represents a strand specific simple square function.

There are two criteria that determine the kappa value of this model: distance
between the primary and secondary site, and the strand relationship between the
two sites.  There are four criteria involved in determining the Kappa value of
this model:
        1. Strand:
           a. If strand argument is BOTH, the continue with step 2.
           b. If strand argument is SAME, and the primary and secondary are on
              different strands, the kappa value is zero, otherwise, continue
              with step 2.
           c. If strand argument is OPPOSITE, and the primary and secondary are
              on the same strand, the kappa value is zero, otherwise, continue
              with step 2.
        2. If the sites overlap (clash) the kappa value is zero.
        3. If the distance between the two binding sites is less than or equal
           to max_distance, the kappa value is one.
        4. Otherwise, the kappa value is zero.
        
The distance between two sites is determined by the distance between their two
closest ends.

It should be noted that the SimpleSquareCoopModel allows for secondary sites on
either side of the primary site.

Strand argument can be 'SAME', 'OPPOSITE' or 'BOTH' (these are case insensitive).

NOTE: Strand argument must be quoted, it is likely that one will need to use
      single quotes to avoid a clash with quotes used to quote the parameter
      string.
"""
REQUIRES_STRAND = False

PARAMETER_STRING_EXAMPLE = ("max_distance=50; min_distance=10; strand='SAME'",)


SAME="SAME"
OPPOSITE="OPPOSITE"
BOTH="BOTH"


class CoopModel(object):

	def __init__(self,
				 primary_TF_binding_model, 
				 secondary_TF_binding_model, 
				 max_distance, min_distance, strand):

	# It is very important that primary_TF_binding_model and 
	# secondary_TF_binding_model are not switched!!
	# 
		# self._genome = genome
		if max_distance < 0:
			raise CoopError, ' ZERO'	
		elif min_distance < 0:
			raise CoopError, 'min_distance must be greater than ZERO'	
		elif max_distance < min_distance :
			raise CoopError, 'max_distance must be greater than or equal to min_distance'	
		self._primary_TF_binding_model = primary_TF_binding_model
		self._primary_TF_length = primary_TF_binding_model.Length
		self._secondary_TF_binding_model = secondary_TF_binding_model
		self._secondary_TF_length = secondary_TF_binding_model.Length
	
		self._max_distance = max_distance
		self._min_distance = min_distance
		if (strand.upper() == SAME) or (strand.upper() == OPPOSITE) or (strand.upper() == BOTH): 
			self._strand = strand.upper()
		else:
			raise CoopError, '"strand" must be: ' + SAME + ', ' + OPPOSITE + ', or ' + BOTH
		
	
		## In order to save computation, the value of coop_ranges most is cached, 
		## this is stored in self._cached_coop_ranges, so if this is requested again, it doesn't
		## need to be recalculated
		#self._cached_primary_TF_name = None
		self._cached_primary_site_hitindex = None
		self._cached_coop_ranges = None
		self._cached_feature = None
	##--------------------------------------------------------------------------
	def GetSlices(self, primary_site_hitindex, primary_site_strand, feature):
		coop_ranges = self.GetCoopRanges(primary_site_hitindex, primary_site_strand, feature)
		## need to add 1 to the "slice stop" value, since these values are to be used directly for slices
		## the stop value is the index after the last element included in the slice, not the last element included in the slice
		coop_ranges = [(start, stop+1) for start, stop in coop_ranges]
		tuple_list = []
		for start, stop in coop_ranges:
			slice_length = stop - start
			zeroes = (0,) * slice_length
			ones   = (1,) * slice_length
			if self._strand == BOTH:
				for_kappa_slice = ones
				rev_kappa_slice = ones
			elif self._strand == SAME:
				if primary_site_strand == PLUS_STRAND:
					for_kappa_slice = ones
					rev_kappa_slice = zeroes
				elif primary_site_strand == MINUS_STRAND:
					for_kappa_slice = zeroes
					rev_kappa_slice = ones
			elif self._strand == OPPOSITE:
				if primary_site_strand == MINUS_STRAND:
					for_kappa_slice = ones
					rev_kappa_slice = zeroes
				elif primary_site_strand == PLUS_STRAND:
					for_kappa_slice = zeroes
					rev_kappa_slice = ones
			else:
				raise 'Unknown strand!!'
			tuple_list.append((start, stop, for_kappa_slice, rev_kappa_slice))

		## return list of tuples of (start_coop_slice, stop_coop_slice, for_kappa_slice, rev_kappa_slice)
		return tuple_list
	##--------------------------------------------------------------------------
	def GetCoopRanges(self, primary_site_hitindex, primary_site_strand, feature):

	# IMPORTANT!!!!
	# Ranges returned by this function are ranges in "hitlist space" - they 
	# are represent the smallest
	# and largest indices of the hitlist that define the range of binding 
	# sites for which the site
	# kappa is non-zero - they are NOT base pair numbers (because the hitlist 
	# array is numbered starting at 0, and bps are numbered starting at 1, 
	# (hitlist index) = bp number - 1).
	# 

		if (self._cached_primary_site_hitindex == primary_site_hitindex) and \
			(self._cached_feature == feature):
			return self._cached_coop_ranges
		
		#----------------------------------------------
		left_range_start = ((primary_site_hitindex - self._max_distance) - self._secondary_TF_length)
		left_range_stop = ((primary_site_hitindex - self._min_distance) - self._secondary_TF_length)
		right_range_start = (primary_site_hitindex + self._min_distance + self._primary_TF_length)
		right_range_stop = (primary_site_hitindex + self._max_distance + self._primary_TF_length)
		#----------------------------------------------
		chrom = feature.Chromosome
		hitlist_size = chrom.GetHitListSize(self._secondary_TF_binding_model.MD5)
	
		range_list = [] 
	
		if (left_range_stop < 0) or (left_range_start >= hitlist_size):
			pass
		else:
			if left_range_start < 0:
				left_range_start = 0
			if left_range_stop >= hitlist_size:
				left_range_stop = hitlist_size - 1
			left_range = (left_range_start, left_range_stop)
			range_list.append(left_range)
		#----------------------------------------------
		#----------------------------------------------
		if (right_range_stop < 0) or (right_range_start >= hitlist_size):
			pass
		else:
			if right_range_start < 0:
				right_range_start = 0
			if right_range_stop >= hitlist_size:
				right_range_stop = hitlist_size - 1
			right_range = (right_range_start, right_range_stop)
			range_list.append(right_range)
		#----------------------------------------------
		
		self._cached_primary_site_hitindex = primary_site_hitindex
		self._cached_feature = feature
		self._cached_coop_ranges = tuple(range_list)
		return self._cached_coop_ranges
	
	def GetSiteKappa(self, primary_site_hitindex, primary_site_strand,
					 secondary_site_hitindex, secondary_site_strand, feature):

	# Since this is a square function, the kappa is one if the binding site is 
	# in the regulatory region (depending on the strand), otherwise the kappa is
	# zero. So all we have to do is see if the site falls in the one of the
	# ranges returned by GetCoopRanges.
		if (self._cached_primary_site_hitindex == primary_site_hitindex) and \
			(self._cached_feature == feature):
			range_pairs = self._cached_coop_ranges
		else:
			range_pairs = self.GetCoopRanges(primary_site_hitindex, primary_site_strand, feature)
	
		kappa_value = 0
		for start_hitindex, stop_hitindex in range_pairs: 
			if start_hitindex <= secondary_site_hitindex <= stop_hitindex :
				if self._strand == BOTH:
					kappa_value = 1
				elif (self._strand == SAME) and (primary_site_strand == secondary_site_strand):
					kappa_value = 1
				elif (self._strand == OPPOSITE)	and (primary_site_strand <> secondary_site_strand):
					kappa_value = 1
				else:
					kappa_value = 0
				return kappa_value
		return kappa_value
	
	def _distance(self, primary_TF_hitindex, secondary_TF_hitindex):

	#@+doc
	# 
	# It is very important that primary_TF_binding_model and 
	# secondary_TF_binding_model are not switched!!
	# 

	#@-doc
	#@@code
		primary_TF_end_hitindex =  (primary_TF_hitindex + self._primary_TF_length) - 1
		secondary_TF_end_hitindex =  (secondary_TF_hitindex + self._secondary_TF_length) - 1
		
		distance = (max((primary_TF_hitindex - secondary_TF_end_hitindex), (secondary_TF_hitindex - primary_TF_end_hitindex))) - 1
		## distance is the number of bp between the two sites
	    ## Therefore if last base of site X is 10,
		## and first base of site Y is 11, distance is 0
	
		if distance < 0:
			return None
		else:
			return distance
	
	


class CoopError(GomerError):
	pass
## 	def __init__(self, message):
## 		self.message=message
		# GomerError.__init__(self,message)
		# super(CoopError, self).__init__(message)


