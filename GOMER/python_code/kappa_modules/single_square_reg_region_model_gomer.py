#! /usr/bin/env python
#@+leo
#@comment Created by Leo at Sun Aug 24 22:58:38 2003
#@+node:0::@file single_square_reg_region_model_gomer.py
#@+body
#@@first
#@@language python
#@@path kappa_modules/ 


#@<<Description>>
#@+node:1::<<Description>>
#@+body
"""
SingleSquareRegulatoryRegionModel is a regulatory region model.

It represents a single square function from "regulatory_five_prime" to "regulatory_three_prime" upstream the START of the feature in question.  

The value of "regulatory_five_prime" must be larger than the value of "regulatory_three_prime"

The parameters can be negative.  A negative value means the the location is downstream of the START of the feature. 

In other words, the SingleSquareRegulatoryRegionModel considers only those binding sites that are in the range from "regulatory_five_prime" to "regulatory_three_prime" relative to the start of the feature.



SingleSquareRegulatoryRegionModel - This is the first Regulatory Region Model I
	have made.  This regulatory region is about as simple as they come, it is a
	single square function - any site between the start and end of the regulatory
	region has a site_kappa  of 1, all sites outside of this regulatory region
	have site_kappas of zero.

"""
DESCRIPTION = __doc__

#@-body
#@-node:1::<<Description>>


#@<<imports>>
#@+node:2::<<imports>>
#@+body
from __future__ import division


import types
from exceptions_gomer import GomerError

#@-body
#@-node:2::<<imports>>



#@<<Name>>
#@+node:3::<<Name>>
#@+body
NAME = 'SingleSquareRegulatoryRegionModel'

#@-body
#@-node:3::<<Name>>


#@<<Type>>
#@+node:4::<<Type>>
#@+body
TYPE = 'RegulatoryRegionModel'

#@-body
#@-node:4::<<Type>>


#@<<Parameters>>
#@+node:5::<<Parameters>>
#@+body
PARAMETERS = {'regulatory_five_prime' : types.IntType,
			  'regulatory_three_prime': types.IntType}


#@-body
#@-node:5::<<Parameters>>


#@<<Parameter Example>>
#@+node:6::<<Parameter Example>>
#@+body
PARAMETER_STRING_EXAMPLE = ("regulatory_five_prime=600; regulatory_three_prime=1",)

#@-body
#@-node:6::<<Parameter Example>>


#@<<Requires Strand>>
#@+node:7::<<Requires Strand>>
#@+body
REQUIRES_STRAND = True
#@-body
#@-node:7::<<Requires Strand>>



#@<<class SingleSquareRegulatoryRegionModel>>
#@+node:8::<<class SingleSquareRegulatoryRegionModel>>
#@+body
class RegulatoryRegionModel(object):

	#@+others
	#@+node:1::__init__
	#@+body
	def __init__(self,
				 binding_site_model, 
				 regulatory_five_prime,
				 regulatory_three_prime):
		# self._genome = genome
		if regulatory_five_prime <= regulatory_three_prime:
			raise RegulatoryRegionError, 'First value in regulatory range must be greater than second'	
		self._regulatory_five_prime = regulatory_five_prime
		self._regulatory_three_prime = regulatory_three_prime
		self._binding_site_model = binding_site_model
		self._site_length = binding_site_model.Length
	
		if ((self._regulatory_five_prime - self._regulatory_three_prime) + 1) < self._site_length:
			raise RegulatoryRegionError(
				'The regulatory region is smaller than the binding site:' +
				'\nregulatory_five_prime: ' + str(self._regulatory_five_prime) +
				'\nregulatory_three_prime: '+ str(self._regulatory_three_prime) +
				'\nself._site_length: ' + str(self._site_length) + '\n\n'
				)
		## In order to save computation, the most recently calculated regulatory region
		## is cached, this is stored in self._cached_regulatory_region, the feature that this 
		## corresponds to is saved in self._cached_feature, so if this is requested again, it doesn't
		## need to be recalculated
		self._cached_regulatory_region = None
		self._cached_feature = None

	#@+doc
	# 
	# the regulatory region is inclusive :
	# 	if the feature is on the '+' strand, and starts at base pair #30
	# 	and the regulatory region is (20, 10),
	# 	that means that any binding site that starts at bp #10 or after,
	# 	and ends at bp #20 or before is in the regulatory region

	#@-doc
	#@@code
	#@-body
	#@-node:1::__init__
	#@+node:2::GetRegulatoryRegionRanges
	#@+body
	# def GetRegulatoryRegionRanges(self, ORF_name):
	def GetRegulatoryRegionRanges(self, feature):

	#@+doc
	# 
	# IMPORTANT!!!!
	# Ranges returned by this function are ranges in "hitlist space" - they 
	# are represent the smallest
	# and largest indices of the hitlist that define the range of binding 
	# sites for which the site
	# kappa is non-zero - they are NOT base pair numbers (because the hitlist 
	# array is numbered starting at 0, and bp's are numbered starting at 1, 
	# (hitlist index) = bp number - 1).

	#@-doc
	#@@code

		# cur_ORF = self._genome.GetFeature(ORF_name)
		if 	feature == self._cached_feature:
			return self._cached_regulatory_region
	
		cur_feature = feature
		cur_chrom = cur_feature.Chromosome
		strand = cur_feature.Strand
		hitlist_size = cur_chrom.GetHitListSize(self._binding_site_model.MD5)
		
	
	
	##----------------------------------------------------------------------
	##
	##     1                                      1     
	##     0                                      3   BP
	##     0                                      9
	## ttttaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaATGtt
	## 9   9                           1         1
	## 5   9                           2         3       hitindex    kappa
	##                                 7         7
	## cccccccccc                                        95        0
	##  cccccccccc                                       96        0
	##   cccccccccc                                      97        0
	##    cccccccccc                                     98        0
	##     cccccccccc                                    99        1
	##      cccccccccc                                   100       1
	##                                 cccccccccc        127       1
	##                                  cccccccccc       128       1
	##                                   cccccccccc      129       1
	##                                    cccccccccc     130       0
	## 
	## feature start = 140bp
	## binding site length = 10
	## regultory region: 40 to 1
	## regultory region: 99 to 129 (hitindex)
	## start_hitindex = (140 - 40) - 1 = 99
	## stop_hitindex =  (140 - 1) - 10 = 129
	##----------------------------------------------------------------------
	## As can be seen above, if the regulatory_three_prime is 0, then, assuming the feature we are talking about is an ORF, the "A" of the ATG will overlap with the last base of the binding site.  In other words, the last window before the Feature starts has an offset of 1, the first window that overlaps with the feature is the 0 window.
	
		
		if strand == '+':
			start_hitlist_index = (cur_feature.Start - self._regulatory_five_prime) - 1
			# subtract one to account for the fact the the hitlist index starts at zero 
			stop_hitlist_index = ((cur_feature.Start - self._regulatory_three_prime) - self._site_length) # + 1) - 1)
			# plus one for the binding site length, minus one for the hitlist offset 

	#@+doc
	# 
	# 	the regulatory region is inclusive :
	# 		if the feature is on the '+' strand, and starts at base pair #30
	# 		and the regulatory region is (20, 10),
	# 		that means that any binding site that starts at bp #10 or after,
	# 		and ends at bp #20 or before is in the regulatory region
	# 
	# 	if the binding site is 7bp, then the hitlist index range should be (9,13)

	#@-doc
	#@@code

	##----------------------------------------------------------------------
	##
	##     1         1         1         1       1    
	##     0         1         2         3       3  BP
	##     0         0         0         0       8
	## aCATttttttttttttttttttttttttttttttttttttttttaaaaa
	## tGTAaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaattttt
	## 9   9          1         1         1      1   
	## 5   9          1         2         3      3 hitindex    kappa
	##                0         0         0      7    
	## cccccccccc                                        95        0
	##  cccccccccc                                       96        0
	##   cccccccccc                                      97        0
	##    cccccccccc                                     98        0
	##     cccccccccc                                    99        1
	##      cccccccccc                                   100       1
	##                                 cccccccccc        127       1
	##                                  cccccccccc       128       1
	##                                   cccccccccc      129       1
	##                                    cccccccccc     130       0
	## 
	## feature start = 99bp (minus strand)
	## binding site length = 10
	## regultory region: 40 to 1
	## regultory region: 99 to 129 (hitindex)
	## start_hitindex = (99 + 1) - 1   = 99
	## stop_hitindex  = (99 + 40) - 10 = 129
	##----------------------------------------------------------------------
	## As can be seen above, if the regulatory_three_prime is 0, then, assuming the feature we are talking about is an ORF, the "A" of the ATG will overlap with the last base of the binding site.  In other words, the last window before the Feature starts has an offset of 1, the first window that overlaps with the feature is the 0 window.
	
		elif strand == '-':
			start_hitlist_index = (cur_feature.Start + self._regulatory_three_prime) - 1
			# subtract one to account for the fact the the hitlist index starts at zero 
			stop_hitlist_index = ((cur_feature.Start + self._regulatory_five_prime) - self._site_length) # + 1) - 1)
			# plus one for the binding site length, minus one for the hitlist offset 

	#@+doc
	# 
	# 	the regulatory region is inclusive :
	# 		if the ORF is one the '-' strand, and starts at base pair #20
	# 		and the regulatory region is (20, 10),
	# 		that means that any binding site that starts at bp #30 or after,
	# 		and ends at bp #40 or before is in the regulatory region
	# 
	# 	if the binding site is 7bp, then the hitlist index range should be (29,33)

	#@-doc
	#@@code
		else:
			raise ValueError, 'Strand must be "+" or "-". "' + str(strand) + '" is not acceptable\n' 
	
		if stop_hitlist_index < start_hitlist_index:
			raise RegulatoryRegionError(
				'The stop_index cannot be smaller than the start_index:' +
				'\nstart_index: ' +	str(start_hitlist_index) +
				'\nstop_index:  ' + str(stop_hitlist_index) +
				'\n\nregulatory_five_prime: ' + str(self._regulatory_five_prime) +
				'\nregulatory_three_prime: '+ str(self._regulatory_three_prime) +
				'\nbinding_site_length: ' + str(self._site_length) + '\n\n'
				)
	
			
		if stop_hitlist_index < 0 :
			# return tuple((tuple(()),))
			return tuple()
		elif start_hitlist_index < 0:
			start_hitlist_index = 0
	
		if start_hitlist_index >= hitlist_size:
			# return tuple((tuple(()),))
			return tuple()
		elif stop_hitlist_index >= hitlist_size:
			stop_hitlist_index = hitlist_size - 1
	
	
		self._cached_feature = feature
		self._cached_regulatory_region = tuple((tuple((start_hitlist_index, stop_hitlist_index)),))
			
		return self._cached_regulatory_region
	
	#@-body
	#@-node:2::GetRegulatoryRegionRanges
	#@+node:3::GetSiteKappa
	#@+body
	# def GetSiteKappa(self, Orf, base_position):
	def GetSiteKappa(self, feature, hitlist_index, site_strand):

	#@+doc
	# 
	# Since this is a square function, the kappa is one if the binding site is 
	# in the regulatory region,
	# otherwise the kappa is zero

	#@-doc
	#@@code
		if 	feature == self._cached_feature:
			((start_hitlist_index, stop_hitlist_index),) = self._cached_regulatory_region
		else:
			((start_hitlist_index, stop_hitlist_index),) = self.GetRegulatoryRegionRanges(feature)	
		# if start_hitlist_index <= (base_position - 1) <= stop_hitlist_index :
		if start_hitlist_index <= hitlist_index <= stop_hitlist_index :
			kappa_value = 1
		else :
			kappa_value = 0
			
		return kappa_value
	
	
	
	
	#@-body
	#@-node:3::GetSiteKappa
	#@-others

	

#@<<SingleSquareRegulatoryRegionError>>
#@+node:4::<<SingleSquareRegulatoryRegionError>>
#@+body
class RegulatoryRegionError(GomerError):
	pass

## CHANGED HERE?
#@-body
#@-node:4::<<SingleSquareRegulatoryRegionError>>
#@-body
#@-node:8::<<class SingleSquareRegulatoryRegionModel>>
#@-body
#@-node:0::@file single_square_reg_region_model_gomer.py
#@-leo
