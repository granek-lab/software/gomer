#! /usr/bin/env python

"""
Generates a multi-sequence FASTA file, with one sequence for each coordinate
feature in the COORDINATE_FEATURE_FILE.  The sequences whose start and stop
are defined by the coordinates in the COORDINATE_FEATURE_FILE are extracted
from the appropriate chromosomal sequences which are in the files listed in
CHROM_TABLE_FILE.


By default, the sequences are printed 80 characters per line.
If the -c/--columns option is given, NUMBER of characters of sequence are
printed per line.
If NUMBER is zero, the whole sequence will be printed on one line.

If the -f/--flank LENGTH option is given, the sequences will include the
LENGTH base pairs of sequence upstream and downstream from those defined by
the coordinates.

The --neil option generates output in the format used by neil,
which makes easy a direct comparison of the output with one generated
by neil using diff.

The -o/--output prints the results to OUTPUT_FILE, instead of printing to the
standard output.  The OUTPUT_FILE can be used as input to GOMER's sequence
feature mode.

The -n/--feature_names expects a FEATURE_NAME_FILE consisting of one name per
line.  When this option is used, instead of printing the sequences for every
feature defined in the coordinate file, only those features named in the
FEATURE_NAME_FILE are printed.  The names are case insensitive.

If the --no_frequencies option is given, the background base frequencies of the
genome are not printed in the output.
"""

from __future__ import division


#@+leo
#@+node:0::@file generate_fasta_from_coordinates.py
#@+body
import re
import os
import sys
import getopt
## from test_controller_gomer import load_chromosome_sequences
from gomer_load_functions import load_chromosome_sequences
from coordinate_feature_parser_gomer import CoordinateFeatureParse
from chromosome_table_parser import ChromosomeTableParse
from integer_string_compare import sort_numeric_string

SEPARATOR = '; '
FREQUENCY_TOKEN = 'Frequency:'
COMMENT_TOKEN = '#'
comment_match = re.compile('^\s*' + COMMENT_TOKEN + '(.*)')
empty_line_match = re.compile('^\s*$')


'>NAME	seq=ORIGINAL_SEQUENCE start=START_BP stop=STOP_BP chrom=CHROMOSOME'


def load(coordinate_filename, chromosome_table_file_name):
	chromosome_hash, feature_dictionary, ambiguous_feature_names, seq_info_hash =	\
						 CoordinateFeatureParse(coordinate_filename,{},{},{})

	feature_file_list, chromosome_file_hash, chromosome_flags_hash, frequency_hash =\
					   ChromosomeTableParse(chromosome_table_file_name)

	load_chromosome_sequences(chromosome_file_hash, chromosome_flags_hash,
							  chromosome_hash, feature_dictionary,
							  ambiguous_feature_names)

	for chrom_label in seq_info_hash:
		if seq_info_hash[chrom_label].seqID <> chromosome_hash[chrom_label].Sequence.Accession:
			raise StandardError, 'Accession numbers should be equal: ' + \
					', '.join((chrom_label,	 seq_info_hash[chrom_label].seqID, chromosome_hash[chrom_label].Sequence.Accession))

	return chromosome_hash, frequency_hash, feature_dictionary

def generate_header(feature, chrom_label, accession):
	header = '>' + SEPARATOR.join((
		feature.Name,
		'seq=' + accession,
		'chrom=' + chrom_label,
		'start=' + str(feature.Start),
		'stop=' + str(feature.Stop)
		))
	return header

""">iYAL068C-0 seqmatch5/3 2161 3449 feature5/3 2169 3435"""
""">iYAL068C-0, seq=NC_001133, chrom=1, start=2162, stop=3449"""
def generate_neil_header(feature, chrom_label, accession, primer):
	header = '>' + ' '.join((
		feature.Name, 
		'seqmatch5/3',
		str(feature.Start-1),
		str(feature.Stop),
		'feature5/3',
		str(primer.start),
		str(primer.end)
		))
	return header



def grab_sequence(feature, chrom, char_per_line=80, flank=0):
	start_index = (feature.Start - 1) - flank # convert from bp space to index space
	stop_index = (feature.Stop) + flank # convert from bp space to index space, needs to be inclusive
	if start_index < 0:
		start_index = 0
	if stop_index >= len(chrom.Sequence.Sequence):
		stop_index = len(chrom.Sequence.Sequence) - 1
		
	sequence = chrom.Sequence.Sequence[start_index:stop_index]
	if char_per_line:
		sequence = '\n'.join([sequence[start:start+char_per_line]
							  for start in xrange(0,len(sequence), char_per_line)])
	return sequence
							  

def usage():
	prog = os.path.basename(sys.argv[0])
	print >>sys.stderr, 'usage:', prog, \
		  'COORDINATE_FEATURE_FILE CHROM_TABLE_FILE'
	print >>sys.stderr, ' '* len('usage:'), os.path.basename(sys.argv[0]), \
		  '--neil COORDINATE_FEATURE_FILE CHROM_TABLE_FILE PRIMER_FILE'
	print >>sys.stderr
	print >>sys.stderr, ' '* len('usage:'), '-c/--columns NUMBER - NUMBER of characters per sequence line'
	print >>sys.stderr, ' '* len('usage:'), '-f/--flank LENGTH  - add flanks to coordinate sequences'
	print >>sys.stderr, ' '* len('usage:'), '-o/--output OUTPUT_FILE'
	print >>sys.stderr, ' '* len('usage:'), '-n/--feature_names FEATURE_NAME_FILE'
	print >>sys.stderr, ' '* len('usage:'), '--no_frequencies'

	description = __doc__
	print >>sys.stderr, '\n', description

	
def generate_frequency_header(frequency_hash):
	frequency_header = ''
	for base_name in frequency_hash:
		frequency_header += '\t'.join((FREQUENCY_TOKEN, base_name, repr(frequency_hash[base_name]))) + '\n'
	return frequency_header
		

def main(args, columns, flank, feature_name_list, print_frequencies, output_handle):
	"""
	if feature_name_dict is non-empty, only features found in it will be output
	"""
	if len(args) <> 2:
		usage()
		sys.exit(2)
	coordinate_filename = args[0]
	# sequence_filename_list = args[2:]
	chromosome_table_file_name= args[1]
	
	chrom_hash, frequency_hash, feature_dictionary = load(coordinate_filename, chromosome_table_file_name)
	
	if columns == None:
		columns = 80

	if print_frequencies:
		output_handle.write(generate_frequency_header(frequency_hash))


	if feature_name_list:
		for feature_name in feature_name_list:
			if feature_name.upper() not in feature_dictionary:
				print >>sys.stderr, 'Feature not found in coordinate file, ignoring:', feature_name
				continue
			feature = feature_dictionary[feature_name.upper()]
			cur_chrom = feature.Chromosome
			chrom_label = cur_chrom.Name
			accession = cur_chrom.Sequence.Accession
			header = generate_header(feature, chrom_label, accession)
			sequence = grab_sequence(feature, cur_chrom, columns, flank)
			output_handle.write(header + '\n' + sequence + '\n')
	else:
		chrom_keys = sort_numeric_string(chrom_hash.keys())
		for chrom_label in chrom_keys:
			cur_chrom = chrom_hash[chrom_label]
			accession = cur_chrom.Sequence.Accession
			for feature in cur_chrom.FeatureIterator():
				header = generate_header(feature, chrom_label, accession)
				sequence = grab_sequence(feature, cur_chrom, columns, flank)
				output_handle.write(header + '\n' + sequence + '\n')

def neil_style_main(args, columns, feature_name_list, output_handle):
	from parse_PCR_primers import ParsePrimers
	if len(args) <> 3:
		usage()
		sys.exit(2)
	coordinate_filename = args[0]
	chromosome_table_file_name= args[1]
	primer_file_name= args[2]

	if columns == None:
		columns = 50

	primer_hash = ParsePrimers(primer_file_name)
	chrom_hash, frequency_hash, feature_dictionary = load(coordinate_filename, chromosome_table_file_name)


	if feature_name_list:
		for feature_name in feature_name_list:
			if feature_name.upper() not in feature_dictionary:
				print >>sys.stderr, 'Feature not found in coordinate file, ignoring:', feature_name
				continue
			feature = feature_dictionary[feature_name.upper()]
			cur_chrom = feature.Chromosome
			primer = primer_hash[feature.Name]
			header = generate_neil_header(feature, chrom_label, accession, primer)
			sequence = grab_sequence(feature, cur_chrom, columns)
			output_handle.write(header + '\n' + sequence + '\n')

	else:
		chrom_keys = sort_numeric_string(chrom_hash.keys())
		for chrom_label in chrom_keys:
			cur_chrom = chrom_hash[chrom_label]
			accession = cur_chrom.Sequence.Accession
			for feature in cur_chrom.FeatureIterator():
				primer = primer_hash[feature.Name]
				header = generate_neil_header(feature, chrom_label, accession, primer)
				sequence = grab_sequence(feature, cur_chrom, columns)
				output_handle.write(header + '\n' + sequence + '\n')


def parse_feature_name_file(filehandle):
	feature_name_list = []
	for	line in filehandle:
		if empty_line_match.search(line):
			continue
		elif comment_match.search(line):
			continue
		else:
			feature_name_list.append(line.strip())
	return feature_name_list


if __name__ == "__main__":
	try:
		opts, args = getopt.getopt(sys.argv[1:], "hc:f:o:n:",
								   ["help", "neil", 'columns=', 'flank=',
									'output=', 'feature_names=',
									'no_frequencies'])
	except getopt.GetoptError:
		# print help information and exit:
		usage()
		sys.exit(2)
	neil_style = None
	num_columns = None
	flank = 0
	output_handle = sys.stdout
	feature_name_list = []
	print_frequencies=True

	for o, a in opts:
		if o in ("-h", "--help"):
			usage()
			sys.exit()
		if o in ("--neil",):
			print >>sys.stderr, 'NEIL STYLE'
			neil_style = True
		if o in ("-c", "--columns"):
			num_columns = int(a)
		if o in ("-f", "--flank"):
			flank = int(a)
		if o in ("-o", "--output"):
			output_handle = file(a, 'w')
		if o in ("-n", "--feature_names"):
			feature_name_handle = file(a)
			feature_name_list = parse_feature_name_file(feature_name_handle)
			feature_name_handle.close()
		if o in ("--no_frequencies"):
			print_frequencies=False

	if neil_style:
		print >>sys.stderr, 'NEIL STYLE'
		neil_style_main(args, num_columns, flank, feature_name_list, output_handle)
	else:
		main(args, num_columns, flank, feature_name_list, print_frequencies, output_handle)

	output_handle.close()
#@-body
#@-node:0::@file generate_fasta_from_coordinates.py
#@-leo
