from __future__ import division
#@+leo
#@+node:0::@file cache_file_printer_gomer.py
#@+body
#@@first
#@@language python


#@<<imports>>
#@+node:1::<<imports>>
#@+body
##import os
##os.environ['PYCHECKER'] = '--allglobals --stdlib'
##import pychecker.checker

##import check_python_version
##check_python_version.CheckVersion()

import os
import sys
import time
import md5
# import struct
import Numeric
import shutil
import atexit
import tempfile
from gzip import GzipFile
from stat import ST_SIZE

from flat_prob_matrix_printer_gomer import FlatProbMatrixOutput
from write_gomer import WriteError
from cache_file_tokens import *

from cache_table_gomer import CacheTableEntry
from cache_file_handler_gomer import CacheFileHandlerDirect, ChromosomeHandler
from globals_gomer import MINIMUM_FILE_COMPRESSION
#@-body
#@-node:1::<<imports>>

gzip_extension = '.gz'

#@<<class CacheFilePrinter>>
#@+node:2::<<class CacheFilePrinter>>
#@+body
class CacheFilePrinter(object):

	#@+others
	#@+node:1::def __init__
	#@+body
	def __init__(self, cache_directory, fast_cache_directory, compression, delete_cache):
		self._cache_directory = cache_directory

		if __debug__: print >>sys.stderr, "fast_cache_directory", fast_cache_directory
		self._fast_cache = False
		self._delete_cache = delete_cache
		if fast_cache_directory and os.path.exists(fast_cache_directory):
			self._fast_cache = True
			tempfile.tempdir=fast_cache_directory
			##temp_filename = os.path.join(fast_cache_directory, temp_filename)
			##self._temp_filename = None
			self._filehandle = tempfile.TemporaryFile()
		elif delete_cache:
			tempfile.tempdir=cache_directory
			self._filehandle = tempfile.TemporaryFile()
		else:
			temp_filename = 'cache' + str(time.time())
			temp_filename = os.path.join(cache_directory, temp_filename)
			##self._temp_filename = temp_filename
			self._filehandle = file(temp_filename, 'w+b')
		if compression == None or delete_cache:
			# deleting takes precedence over compressing
			self._compression = None
		else:
			if not( 1 <= compression <= 9):
				compression = 9
			self._compression = compression
		self._data_offset = 0
		self._data_start_position = None
		self._cur_chromosome = None
		self._prob_matrix = None
		self._filter_cutoff_ratio = None
		self._chromosome_hash = {}
		self._bytes_per_value = Numeric.array(1,Numeric.Float).itemsize()
	
		self._base_frequency_hash = None
	
		## self._cache_file_md5 = md5.new()
		## self._chromosome_offsets = {}
	#@-body
	#@-node:1::def __init__
	#@+node:2::def AddFilterCutoff
	#@+body
	def AddFilterCutoff(self, filter_cutoff_ratio):
		if self._data_start_position:
			raise CacheFilePrinterError, 'Cannot add filter cutoff after begining to write window scores'
			
	
		if self._filter_cutoff_ratio:
			raise CacheFilePrinterError, 'Cannot add a second filter cutoff'
		else:
			self._filter_cutoff_ratio = filter_cutoff_ratio
	
		output_string = FILTER_CUTOFF_RATIO_TOKEN + ' ' + repr(filter_cutoff_ratio) + '\n'
		self._filehandle.write(output_string)
	
	#@-body
	#@-node:2::def AddFilterCutoff
	#@+node:3::def AddBaseFrequencies
	#@+body
	def AddBaseFrequencies(self, base_frequency_hash):
		if self._data_start_position:
			raise CacheFilePrinterError, 'Cannot add base frequencies after begining to write window scores'
			
	
		if self._base_frequency_hash <> None:
			raise CacheFilePrinterError, 'Cannot add a second set of base frequencies'
		else:
			self._base_frequency_hash = base_frequency_hash
	
		upper_base_freq_pairs = [(base.upper(), freq) for base, freq in base_frequency_hash.items()]
		upper_base_freq_pairs.sort()
		
		output_string = ''
		for base, freq in upper_base_freq_pairs:
			output_string += ' '.join((FREQUENCY_TOKEN, base, repr(freq))) + '\n'
		self._filehandle.write(output_string)
	
	#@-body
	#@-node:3::def AddBaseFrequencies
	#@+node:4::def AddProbMatrix
	#@+body
	def AddProbabilityMatrix(self, prob_matrix):
		if self._data_start_position:
			raise CacheFilePrinterError, 'Cannot add a probability matrix after begining to write window scores'
			
	
		if self._prob_matrix:
			raise CacheFilePrinterError, 'Cannot add a second probability matrix'
		else:
			self._prob_matrix = prob_matrix
	
		output_string = ''
		output_string += PROB_MATRIX_FILE_TOKEN + ' ' + prob_matrix.FileName + '\n'
		probability_matrix_string = FlatProbMatrixOutput(prob_matrix)
		self._prob_matrix_filename = prob_matrix.FileName
		self._prob_matrix_md5 = prob_matrix.MD5		
		output_string += PROB_MATRIX_MD5_TOKEN + ' ' + self._prob_matrix_md5 + '\n'
		output_string += PROB_MATRIX_BEGIN_TOKEN + '\n'
		output_string += probability_matrix_string
		output_string += PROB_MATRIX_END_TOKEN + '\n'
		self._filehandle.write(output_string)
		##self._cache_file_md5.update(output_string)
	#@-body
	#@-node:4::def AddProbMatrix
	#@+node:5::def AddChromosome
	#@+body
	def AddChromosome(self, chrom_label, chrom_sequence):
		if self._data_start_position:
			raise CacheFilePrinterError, 'Cannot add a probability matrix after begining to write window scores'
		elif not self._prob_matrix:
			raise CacheFilePrinterError, 'Probability matrix must be added before Chromosomes'
		chrom_sequence_md5 = md5.new(chrom_sequence).hexdigest()
		
		number_of_windows = (len(chrom_sequence) - self._prob_matrix.Length) + 1
		forward_offset = self._data_offset
		self._data_offset += number_of_windows * self._bytes_per_value
		reverse_offset = self._data_offset  ## + (number_of_windows * 	self._bytes_per_value)
		self._data_offset += number_of_windows * self._bytes_per_value
		## self._chromosome_offsets[chrom_label]=(forward_offset, reverse_offset)
		
		output_string = ' '.join((CHROMOSOME_TOKEN,str(chrom_label),
								  str(number_of_windows),str(forward_offset),
								  str(reverse_offset),chrom_sequence_md5)) + '\n'
		self._filehandle.write(output_string)
		##self._cache_file_md5.update(output_string)
		self._cur_chromosome = chrom_label
	##------------------------------------------------------------------------------
		self._chromosome_hash[chrom_label] = ChromosomeHandler(chrom_label,
															   number_of_windows,
															   forward_offset,
															   reverse_offset,
															   chrom_sequence_md5)
		
	
	
	#@-body
	#@-node:5::def AddChromosome
	#@+node:6::def AddHitlist
	#@+body
	def AddHitlist(self, hitlist):
		if not self._data_start_position:
			## self._score_segment = 1
			self._filehandle.write(DATA_START_LINE_TOKEN + '- do NOT touch anything below this line' + '-' * 20 + '\n')
			self._data_start_position = self._filehandle.tell()
	
		if not self._prob_matrix:
			raise CacheFilePrinterError, 'Probability matrix must be added before Window Scores'
		elif not self._cur_chromosome:
			raise CacheFilePrinterError, 'Chromosomes must be added before Window Scores'
	
		# hitlist_length = hitlist.Length
		forward_scores = hitlist.Forward
		revcomp_scores = hitlist.ReverseComplement
	##	for window in xrange(hitlist_length):
	##		output_string = struct.pack('f', forward_scores[window]) + \
	##						struct.pack('f', revcomp_scores[window])
	##		self._filehandle.write(output_string)
	##		self._cache_file_md5.update(output_string)

	#@+doc
	# 
	# 	for score in forward_scores:
	# 		self._filehandle.write(struct.pack('f', score))
	# 		##self._cache_file_md5.update(output_string)
	# 	for score in revcomp_scores:
	# 		self._filehandle.write(struct.pack('f', score))
	# 		##self._cache_file_md5.update(output_string)

	#@-doc
	#@@code
		##print 'Forward Offset:', self._filehandle.tell()
		self._filehandle.write(forward_scores.tostring())
		##print 'Revcomp Offset:', self._filehandle.tell()
		self._filehandle.write(revcomp_scores.tostring())
		

	#@+doc
	# 
	# ---------------------
	# 5.9 array -- Efficient arrays of numeric values
	# ---------------------
	# fromfile(f, n)
	#     Read n items (as machine values) from the file object f and append 
	# them to the end of the array. If less than n items are available, 
	# EOFError is raised, but the items that were available are still inserted 
	# into the array. f must be a real built-in file object; something else 
	# with a read() method won't do.
	# 
	# 
	# fromstring(s)
	#     Appends items from the string, interpreting the string as an array 
	# of machine values (as if it had been read from a file using the 
	# fromfile() method).
	# 
	# 
	# 
	# tofile(f)
	#     Write all items (as machine values) to the file object f.
	# 
	# tolist()
	#     Convert the array to an ordinary list with the same items.
	# 
	# tostring()
	#     Convert the array to an array of machine values and return the 
	# string representation (the same sequence of bytes that would be written 
	# to a file by the tofile() method.)
	# 
	# 
	# When an array object is printed or converted to a string, it is 
	# represented as array(typecode, initializer). The initializer is omitted 
	# if the array is empty, otherwise it is a string if the typecode is 'c', 
	# otherwise it is a list of numbers. The string is guaranteed to be able 
	# to be converted back to an array with the same type and value using 
	# reverse quotes (``), so long as the array() function has been imported 
	# using from array import array. Examples:
	# 
	# 
	# 
	# ---------------------
	# Numeric:
	# ---------------------
	# byteswapped()
	# 	The byteswapped method performs a byte swapping operation on all the 
	# elements in the array.
	# 	>>> print a
	# 	[1 2 3]
	# 	>>> print a.byteswapped()
	# 	[16777216 33554432 50331648]
	# 
	# tostring()
	# 	The tostring method returns a string representation of the data portion 
	# of the array it is applied to.
	# 	>>> a = arange(65,100)
	# 	>>> print a.tostring()
	# 	A B C D E F G H I J K L M N O P Q R S T
	# 	U V W X Y Z [ \ ] ^ _ ` a b c
	# 
	# fromstring(string, typecode)
	# 
	# Will return the array formed by the binary data given in string of the 
	# specified typecode. This is mainly used for reading binary data to and 
	# from files, it can also be used to exchange binary data with other 
	# modules that use python strings as storage ( e.g. PIL). Note that this 
	# representation is dependent on the byte order. To find out the byte 
	# ordering used, use the byteswapped() method described on byteswapped().

	#@-doc
	#@-body
	#@-node:6::def AddHitlist
	#@+node:7::def Finish
	#@+body
	def Finish(self):

	#@+doc
	# 
	# probability_matrix_filename | md5sum_of_prob_hash_file | 
	# cache_file_name(based on probability hash) | 	md5sum_of_cache_file
	# 

	#@-doc
	#@@code
		if not self._data_start_position:
			raise CacheFilePrinterError, 'Window Scores must be added before calling Finish'
		if not self._prob_matrix:
			raise CacheFilePrinterError, 'Probability matrix must be added before calling Finish'
		if not self._cur_chromosome:
			raise CacheFilePrinterError, 'Chromosomes must be added before calling Finish'
		if self._filter_cutoff_ratio == None:
			raise CacheFilePrinterError, 'Filter Cutoff must be added before calling Finish'
		if self._base_frequency_hash == None:
			raise CacheFilePrinterError, 'Base Frequencies must be added before calling Finish'
	
		##file_handle_name = self._filehandle.name
		##self._filehandle.close()
		##self._filehandle = file(file_handle_name, 'rb')
		## self._filehandle.flush()
		self._filehandle.seek(0)
	##--------------------------------------------------------------
		probability_matrix_filename = os.path.basename(self._prob_matrix.FileName)
		cache_file_base_name = os.path.basename(probability_matrix_filename) + '_cache_'
		counter = 0
		new_cache_filename = os.path.join(self._cache_directory, cache_file_base_name + str(counter))
		while (os.path.exists(new_cache_filename) or
			   os.path.exists(new_cache_filename + gzip_extension)):
			counter += 1
			new_cache_filename = os.path.join(self._cache_directory, cache_file_base_name + str(counter))
	##--------------------------------------------------------------

	#@+doc
	# 
	# If a fast cache is used:
	# 	1. If compression is OFF, just copy fast cache file to real cache.
	# 	2. If compression is ON,  but fails, do #1
	# 	3. If compresion is ON, compress to real cache

	#@-doc
	#@@code
	##--------------------------------------------------------------
		compressed = False
		if self._compression <> None:
			gzipped_file_name = new_cache_filename + gzip_extension
			print >>sys.stderr, 'Writing Cache File at Compression Level:', self._compression
			gzipped_handle = GzipFile(gzipped_file_name, 'wb', self._compression)
			## print >>sys.stderr, gzipped_handle.mode
			## print >>sys.stderr, self._filehandle.name, self._filehandle.mode, self._filehandle.closed
			shutil.copyfileobj(self._filehandle, gzipped_handle)
			gzipped_handle.close()
	##--------------------------------------------------------------
			zipped_size = os.stat(gzipped_file_name)[ST_SIZE]
			self._filehandle.seek(0,2)
			original_size = self._filehandle.tell()
			## original_size = os.stat(file_handle_name)[ST_SIZE]
			if zipped_size/original_size < MINIMUM_FILE_COMPRESSION:
				compressed = True
				atexit.register(try_to_remove, self._filehandle)
				##atexit.register(os.remove, self._filehandle.name)
				##atexit.register(self._filehandle.close)
				##new_cache_file_name = gzipped_file_name
				handle_for_md5 = file(gzipped_file_name,'rb')
				new_cache_filename = gzipped_file_name
	##-----------------------------------------------
			else:  ## compression failed
				## if the compression doesn't significantly decrease the size of
				## the file, clean up the compressed file and just leave the raw
				## cache file
				compressed = False
				compression_percent = round(100 * zipped_size/original_size, 2)
				print >>sys.stderr, 'Compression of cache file is poor (', compression_percent, '%)' 
				print >>sys.stderr, 'Removing compressed cache, and leaving original cache file'
				os.remove(gzipped_file_name)
				##-------------------------------------------------------
				## windows can't rename an open file, so we close the file, rename it, and reopen it
				## compressed = False
				## self._filehandle.close()
				## os.rename(file_handle_name, new_cache_file_name)
				## self._filehandle = file(new_cache_file_name, 'rb')
	##	else:   #	if self._compression <> None:
		if not compressed:
			## If no compression is desired, or if compression fails
			if self._delete_cache: # if we want to DELETE the cache
				pass
			elif self._fast_cache:
				self._filehandle.seek(0)
				new_cache_filehandle = file(new_cache_filename, 'wb')
				shutil.copyfileobj(self._filehandle, new_cache_filehandle)
				new_cache_filehandle.close()
			else:
				## windows can't rename an open file, so we close the file, rename it, and reopen it
				self._filehandle.close()
				os.rename(self._filehandle.name, new_cache_filename)
				self._filehandle = file(new_cache_filename, 'rb')
			self._filehandle.seek(0)
			handle_for_md5 = self._filehandle

	#@+doc
	# 
	# 		At normal program termination (for instance, if sys.exit() is called 
	# or the main module's execution completes), all functions registered are 
	# called in last in, first out order. The assumption is that lower level 
	# modules will normally be imported before higher level modules and thus 
	# must be cleaned up later.

	#@-doc
	#@@code
	##	print 'RENAMING - old_name:', 	self._temp_filename, 'new_name:', new_cache_file_name
	##	os.rename(self._temp_filename, new_cache_file_name)

	#@+doc
	# 
	# 	1. What CacheFilePrinter should do:
	# 		Finish should return a CacheFileHandler instance, and this handler 
	# should be an interface to the uncompressed version of the file, 
	# regardless of whether or not compression is to be used.
	# 
	# 		If compression is selected, Finish should open a GzipFile, and use 
	# shutil.copyfileobj to copy the cache to a compressed file (this could be 
	# done in a separate thread).  This compressed file should be named with a 
	# .gz, and the name of this file should be entered in the cache table.  
	# The md5sum should be the md5sum of the compressed file.  Finish should 
	# then register an atexit to delete the uncompressed version of the file 
	# on exit.

	#@-doc
	#@@code
		##Calculate MD5 of cache file
		cache_file_md5 = md5.new()
		read_block_size = 5000000
		## filehandle = file(new_cache_file_name, 'rb')
		input = ' '
		handle_for_md5.seek(0)
		while input:
			input = handle_for_md5.read(read_block_size)
			cache_file_md5.update(input)
		del input
		handle_for_md5.seek(0)
		## filehandle.close()
		cache_file_md5_digest = cache_file_md5.hexdigest()
		cache_table_entry_hash = {'probability_filename': probability_matrix_filename,
								  'filter_cutoff_ratio':self._filter_cutoff_ratio,
								  'md5sum_of_prob_file':self._prob_matrix_md5,
								  'cache_file_name':os.path.basename(new_cache_filename),
								  'md5sum_of_cache_file':cache_file_md5_digest}
		table_entry = CacheTableEntry(**cache_table_entry_hash)
	## INITIALIZE a CacheFileHandler
		self._filehandle.seek(0)
		cache_handler = CacheFileHandlerDirect(self._filehandle,
											   self._base_frequency_hash,
											   self._chromosome_hash,
											   cache_file_md5_digest,
											   self._filter_cutoff_ratio,
											   self._prob_matrix_filename,
											   self._prob_matrix_md5,
											   self._data_start_position)
		return table_entry, cache_handler
	#@-body
	#@-node:7::def Finish
	#@-others

	

#@+doc
# 
# cache file format:
# -----------------------
# 1. probability matrix file name
# 2. copy of probability matrix data
# 3. md5sum of probability matrix
# 4. md5sum of each chromosome sequence
# 5. byte number of beginning of each chromosome's values (forward and reverse?)
# 6. BREAKPOINT with WARNING
# 7. window scores (all forward then all reverse?)

#@-doc
#@-body
#@-node:2::<<class CacheFilePrinter>>


#@<<CacheFilePrinterError>>
#@+node:3::<<CacheFilePrinterError>>
#@+body
class CacheFilePrinterError(WriteError):
	pass
#@-body
#@-node:3::<<CacheFilePrinterError>>


def try_to_remove(filehandle):
	name = filehandle.name
	filehandle.close()
	try:
		os.remove(name)
	except OSError:
		pass


#@-body
#@-node:0::@file cache_file_printer_gomer.py
#@-leo
