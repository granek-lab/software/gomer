#! /usr/bin/env python
from __future__ import division


from global_dictionary import GlobalDictionary
import global_dictionary 

# from global_dictionary_tester2 import dostuff, dostuff2


print __name__, 'global_dictionary.BLAH', global_dictionary.BLAH
print 'global_dictionary.BLAH = 1000' ; global_dictionary.BLAH = 1000
print __name__, 'global_dictionary.BLAH', global_dictionary.BLAH
print 'global_dictionary.RUNTIME = 3.14159' ; global_dictionary.RUNTIME = 3.14159
from global_dictionary_tester2 import dostuff, dostuff2, dostuff3

dostuff()
print 'GlobalDictionary.ONE', GlobalDictionary.ONE
print 'dir(GlobalDictionary)', dir(GlobalDictionary)

print 'GlobalDictionary.ONE = 250' ; GlobalDictionary.ONE = 250


print 'GlobalDictionary.ONE', GlobalDictionary.ONE
dostuff()

dostuff2()
dostuff3()
print __name__, 'global_dictionary.BLAH', global_dictionary.BLAH
print 'global_dictionary.BLAH = 123456' ; global_dictionary.BLAH = 123456
print __name__, 'global_dictionary.BLAH', global_dictionary.BLAH
dostuff2()
dostuff3()
