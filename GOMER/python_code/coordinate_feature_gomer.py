#! /usr/bin/env python
from __future__ import division
#@+leo
#@comment Created by Leo at Fri Jul 18 16:35:30 2003
#@+node:0::@file coordinate_feature_gomer.py
#@+body
#@@first
#@@first
#@@language python


"""
*Description:

A subclass of GenomeFeature for representing ORFs.

Only overrides __init__ method of GenomeFeature.
"""

COORDINATE_TYPE = 'COORDINATE'


#@<<imports>>
#@+node:1::<<imports>>
#@+body
##import os
##os.environ['PYCHECKER'] = '--allglobals --stdlib'
##import pychecker.checker

##import check_python_version
##check_python_version.CheckVersion()

from genome_feature_gomer import GenomeFeature

#@-body
#@-node:1::<<imports>>


class CoordinateFeature(GenomeFeature):

	#@+others
	#@+node:2::__init__
	#@+body
	def __init__(self, name, chrom_label, startcoord, stopcoord, alias_list,
				 corresponding_features_list, description, chrom_reference):
	
		self._corresponding_features_list = corresponding_features_list
		unique_id=''
		alt_id_list=()
		feature_type_list=(COORDINATE_TYPE,)
		strand=''
		super(CoordinateFeature, self).__init__(name, name, alias_list,unique_id,
												alt_id_list,feature_type_list,
												startcoord, stopcoord, strand,
												description, chrom_label, chrom_reference)
	
	#@-body
	#@-node:2::__init__
	#@-others


#@-body
#@-node:0::@file coordinate_feature_gomer.py
#@-leo
