#! /usr/bin/env python
from __future__ import division

"""
intersection.py :
Used to compare or merge two coordinate feature files
(e.g. Intergenic_Coordinates and Yeast_Gene_Pairs_Coordinates)

Without options, print lines with feature names that intersect between the two files.

-n/--names - same as without options, but print only the intersecting names,
             not the whole line

-m/--merge - Merge FIRST_FILE and SECOND_FILE.
             For any feature that is found in both files, the feature from
			 FIRST_FILE will be used (in the output, the feature from SECOND_FILE
			 will be commented out (by a comment '#' symbol).

			 If two different sequences (according to md5sum) are used with the
			 same sequence label (e.g. "1"), this program will complain and exit.
			 Otherwise, the union of sequence info from the two files will be
			 included in the merged file.
"""
import sys
import os
import re
import getopt
import textwrap
from integer_string_compare import sort_numeric_string


SEQUENCE_TOKEN = '%SEQUENCE'
comment_line_re = re.compile('^#')
tag_line_re = re.compile('^%')
empty_line_re = re.compile('^\s*$')
seq_info_re = re.compile('^' + SEQUENCE_TOKEN + '\t' +
						 '(?P<seq_id>[^\t]+)' + '\t' +
						 '(?P<chrom_label>[^\t]+)' + '\t' +
						 '(?P<md5sum>[^\t]+)' +
						 '\s*$')



INTERSECTION_COMMENT = '#INTERSECTION'

def main():
	def usage():
		print >>sys.stderr, 'usage:', sys.argv[0], 'FIRST_FILE SECOND_FILE'
		print >>sys.stderr, '\n\t-n/--names : Only print names which intersect'
		print >>sys.stderr, '\t-m/--merge: Merge FIRST_FILE and SECOND_FILE.'
		print >>sys.stderr, '\t--allowed_multiples: .'
		print >>sys.stderr, '\n', __doc__
	try:
		opts, args = getopt.getopt(sys.argv[1:], "hnm", ["help", "names", "merge"])
	except getopt.GetoptError:
		usage()
		sys.exit(2)
	names_only = False
	do_merge = False
	for o, a in opts:
		if o in ("-h", "--help"):
			usage()
			sys.exit()
		if o in ("-n", "--names"):
			names_only = True
		if o in ("-m", "--merge"):
			do_merge = True

	if len(args) <> 2:
		usage()
		sys.exit(2)
	filename_one = args[0]
	filename_two = args[1]

	dict_one, seq_info_one = parse_coordinate_file(file(filename_one))
	dict_two, seq_info_two = parse_coordinate_file(file(filename_two))



	intersection_dict = {}
	for name in dict_two:
		if name in dict_one:
			intersection_dict[name] = (dict_one[name], dict_two[name])

	if do_merge:
		merge(filename_one, seq_info_one, filename_two, seq_info_two, intersection_dict)
	elif names_only:
		raise NotImplementedError
	else:
		for name in intersection_dict:
			cur_intersect = intersection_dict[name]
			print cur_intersect[0]
			print cur_intersect[1]

def merge(filename_one, seq_info_one, filename_two, seq_info_two, intersection_dict):
	handle_one = file(filename_one)
	handle_two = file(filename_two)

	##-------------------------------------------------
	## Merge  Sequence Info Declarations
	##-------------------------------------------------
	sequence_info_union = {}
	for sequence_label in seq_info_one:
		if sequence_label in seq_info_two:
			if seq_info_one[sequence_label] == seq_info_two[sequence_label]:
				sequence_info_union[sequence_label] = seq_info_one[sequence_label]
				del seq_info_two[sequence_label]
			else:
				raise 'Different sequence used to generate two different files:\n'\
					  + seq_info_one[sequence_label] + '\n' \
					  + seq_info_two[sequence_label] + '\n'
		else:
			print >>sys.stderr, 'Sequence present in', filename_one, ', but absent from', filename_two, ':', seq_info_one[sequence_md5]
			sequence_info_union[sequence_label] = seq_info_one[sequence_label]

	for sequence_label in seq_info_two:
		print >>sys.stderr, 'Sequence present in', filename_two, ', but absent from', filename_one, ':', seq_info_two[sequence_label]
		sequence_info_union[sequence_label] = seq_info_two[sequence_label]
	##-------------------------------------------------

	wrapper = textwrap.TextWrapper(initial_indent='# ',subsequent_indent='# ')
	head_string = ' '.join(('Merge of "'+ os.path.basename(filename_one)+'" and "'+
						   os.path.basename(filename_two)+'".  For names found',
							'in both files, the feature',
						   'from "'+os.path.basename(filename_one)+'"is used, ',
						   'and the feature from "'+os.path.basename(filename_two)+
						   '" is commented out with "' + INTERSECTION_COMMENT + '"'))
	print '#' + '='*60, '\n', wrapper.fill(head_string),'\n#' + '=' *60
	print '#' + '-'*60, '\n## File:',os.path.basename(filename_one),'\n#' + '-' *60
	sequence_info_printed = False
	for line in handle_one:
		if seq_info_re.search(line):
			if not sequence_info_printed:
				sequence_info_keys = sort_numeric_string(sequence_info_union.keys())
				print '#'+'-'*60
				for sequence_label in sequence_info_keys:
					print sequence_info_union[sequence_label]
				print '#'+'-'*60
				sequence_info_printed = True
			print '#MERGE:', line.strip()
		else:
			print line.strip()
	print '#' + '-'*60, '\n## File:',os.path.basename(filename_two),'\n#' + '-' *60

	for line in handle_two:
		if comment_line_re.match(line) or empty_line_re.match(line):
			print line.strip()
		elif seq_info_re.search(line):
			print '#MERGE:', line.strip()
		else:
			name = line.split('\t')[0]
			if name in intersection_dict:
				print INTERSECTION_COMMENT, line.strip()
			else:
				print line.strip()
	handle_one.close()
	handle_two.close()

def parse_coordinate_file(handle):
	# handle = file(filename)
	name_dict = {}
	line_dict = {}

##	line_list = []
	seq_info_hash = {}
	for line in handle:
		if seq_info_re.match(line):
			seq_info_match = seq_info_re.match(line)
			chrom_label = seq_info_match.group('chrom_label')
			seq_id = seq_info_match.group('seq_id')
			md5sum = seq_info_match.group('md5sum')
			seq_info_hash[chrom_label] = line.strip()
		elif comment_line_re.match(line) or empty_line_re.match(line):
##			line_list.append('', line)
			pass
		else:
			# raise StandardError, 'ARG!!!!!!!!!!!'
			split_line = line.split('\t')
			##-----------------------------------------------------------
			line_concat = 'JOINT'.join(split_line)
			if line_concat in line_dict:
				print >>sys.stderr, 'Duplicated line ignored:', line
				## raise StandardError
				continue
			else:
				line_dict[line_concat] = 1
			##-----------------------------------------------------------
			# name = line.split('\t')[0]
			name = split_line[0]
			if name in name_dict:
				print 'DUPLICATE in', os.path.basename(filename),':', name
			else:
				name_dict[name] = line.strip()
##				line_list.append(name, line)
##	return name_dict, line_list
	## print 'name_dict', name_dict
	## print 'line_dict', line_dict
	handle.close()
	return name_dict, seq_info_hash

if __name__ == '__main__':
	# raise StandardError, 'Need to be sure that %SEQUENCE declarations are the same, and %SEQUENCE declarations that are missing from the first file are included from the second'
	main()
