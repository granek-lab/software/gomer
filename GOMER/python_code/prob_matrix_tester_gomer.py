#! /usr/bin/env python
from __future__ import division
#@+leo
#@+node:0::@file prob_matrix_tester_gomer.py
#@+body
#@@first
#@@first
#@@language python


"""
*Description:

Not really sure what this does.  I think it is probably outdated
"""


#@<<imports>>
#@+node:1::<<imports>>
#@+body
##import os
##os.environ['PYCHECKER'] = '--allglobals --stdlib'
##import pychecker.checker
##import check_python_version
##check_python_version.CheckVersion()

import os
import re
import sys
import operator
from optparse import OptionParser

from flat_prob_matrix_parser_gomer import FlatProbMatrixParse
from flat_prob_matrix_tokens import *
from flat_prob_matrix_printer_gomer import FlatProbMatrixOutput, FlatProbMatrixOutputTable, PWMOutput


#@-body
#@-node:1::<<imports>>


#@<<regular expressions>>
#@+node:2::<<regular expressions>>
#@+body
# comment_match = re.compile('^\s*' + COMMENT_TOKEN + '(.+)')
comment_match = re.compile('^' + COMMENT_TOKEN + '(.*)$')
empty_line_match = re.compile('^\s*$')
#@-body
#@-node:2::<<regular expressions>>


#@+others
#@-others




#@<<command line>>
#@+node:3::<<command line>>
#@+body
#@<<def run>>
#@+node:1::<<def run>>
#@+body
def main ():
	version_num = 0.001
	version = '%prog ' + str(version_num)
	usage = 'usage: %prog [options] PROB_MATRIX_FILE SEQUENCE\n' +\
			(' ' * len('usage: ')) + \
			'%prog -f/--file SEQUENCE_FILE PROB_MATRIX_FILE\n'
	parser = OptionParser(usage=usage, version=version)
	parser.add_option('-f', '--file', 
					  metavar="SEQUENCE_FILE",
					  help="Use score sequence from SEQUENCE_FILE")
	parser.add_option('-e', '--equal_prob_bases', 
					  action="store_true",
					  default=False,
					  metavar="SEQUENCE_FILE",
					  help="Use equal probability (0.25 each) base frequencies")
	parser.add_option('--oct2002', 
					  action="store_true",
					  default=False,
					  metavar="SEQUENCE_FILE",
					  help="Use probabilities from  saccharomyces_cerevisiae/oct_23_2002")
	parser.add_option('--jan2004', 
					  action="store_true",
					  default=False,
					  metavar="SEQUENCE_FILE",
					  help="Use probabilities from  saccharomyces_cerevisiae/jan_29_2004")
	parser.add_option('--temp', 
					  type='float',
					  metavar="TEMP",
					  help="Use TEMP as the psedotemp for the matrix, instead of the temperature found in the probability file")

	(options, args) = parser.parse_args()
	EXPECTED_ARGUMENTS = 2
	if options.file and (len(args) == 1):
		probability_file_name = args[0]
		sequences_file_name = options.file
		##--------------------------------------------------------------------
		sequences_file_handle = open(sequences_file_name, 'r')
		cur_string = ''
		cur_comment = ''
		comment_list = []
		sequence_list = []
		for line in sequences_file_handle:
			if empty_line_match.search(line):
				continue
			elif comment_match.search(line):
				cur_comment += line
				if not empty_line_match.search(cur_string):
					sequence_list.append(cur_string)
				cur_string = ''
			else:
				cur_string += line.rstrip()
				comment_list.append(cur_comment)
				cur_comment = ''
		if not empty_line_match.search(cur_string):
			sequence_list.append(cur_string)
			cur_string = ''


				# sequence_list.append(line.rstrip())
	##--------------------------------------------------------------------
	elif (len(args) == 2):
		probability_file_name = args[0]
		sequence_list = [args[1]]
	else :
		parser.print_help()
		sys.exit(2)


	if options.equal_prob_bases:
		base_frequency_hash = {'A': 0.25, 'C': 0.25, 'T': 0.25, 'G': 0.25}
	elif options.oct2002:
		base_frequency_hash = {'A': 0.30926101235029785,
							   'C': 0.19073898764970215,
							   'T': 0.30926101235029785,
							   'G': 0.19073898764970215}
	elif options.jan2004:
		base_frequency_hash = {'A': 0.3092605972321702,
							   'C': 0.1907394027678298,
							   'G': 0.1907394027678298,
							   'T': 0.3092605972321702}
	else:
		
		base_frequency_hash = base_count(''.join(sequence_list))
	probability_matrix = FlatProbMatrixParse(probability_file_name, base_frequency_hash)
	if options.temp:
		probability_matrix.ResetPseudoTemp(options.temp)
		print 'TEMP:', options.temp
	print 'Probabilities formatted for file'
	print FlatProbMatrixOutput(probability_matrix)
	print 'Probability Table'
	print FlatProbMatrixOutputTable(probability_matrix)
	print 'Bitvalue Table'
	print PWMOutput(probability_matrix)
	# best_Ka, best_values, best_sequence = probability_matrix.GetBestKa()
	max_Ka, max_Ka_bitscores, max_Ka_sequence, min_Ka, min_Ka_bitscores, min_Ka_sequence = probability_matrix.GetKaRange()
	# print 'GetBestKa:\n', '\n'.join([str(x) for x in probability_matrix.GetBestKa()])
	# print 'GetKaRange:\n', '\n'.join([str(x) for x in probability_matrix.GetKaRange()])
	print 'MaxKa:', max_Ka
	print 'MaxKa sequence:', ''.join(max_Ka_sequence).upper()
	print 'MaxKa bitscores:', ', '.join(['%.4g' % x for x in max_Ka_bitscores])

	print 'MinKa:', min_Ka
	print 'MinKa sequence:', ''.join(min_Ka_sequence).upper()
	print 'MinKa bitscores:', ', '.join(['%.4g' % x for x in min_Ka_bitscores])
	print

	#@<<check reverse complement>>
	#@+node:1::<<check reverse complement>>
	#@+body
	one = two = 0
	for index in range(len(sequence_list)):
		sequence = sequence_list[index]
		# comment = comment_list[index]
		# print comment
		# forward_score, reverse_score = probability_matrix.ScoreSequence(string)
		#-----------------------------------------------------------------------
## 		print 'ScoreSequence'
## 		hitlist = probability_matrix.ScoreSequence(string, 0)
## 		forward_score = hitlist.Forward
## 		reverse_score = hitlist.ReverseComplement
## 		print string
## 		print [round(value, 5) for value in forward_score]
## 		print [round(value, 5) for value in reverse_score]
## 		print 'for: (', ', '.join(['%0.2e' % score for score in forward_score]), ')'
## 		print 'rev: (', ', '.join(['%0.2e' % score for score in reverse_score]), ')'
		#-----------------------------------------------------------------------
		hitlist = probability_matrix.FilterScoreSequence(sequence, 0)
		forward_score = hitlist.Forward
		reverse_score = hitlist.ReverseComplement
		print '\n', sequence
		print [round(value, 5) for value in forward_score]
		print [round(value, 5) for value in reverse_score]
		# print 'for: (', ', '.join(['%0.2e' % score for score in forward_score]), ')'
		# print 'rev: (', ', '.join(['%0.2e' % score for score in reverse_score]), ')'
		# print 'for: (', ', '.join([`score` for score in forward_score]), ')'
		# print 'rev: (', ', '.join([`score` for score in reverse_score]), ')'
		print 'for:', '\n'.join([`score` for score in forward_score])
		print 'rev:', '\n'.join([`score` for score in reverse_score])
		#-----------------------------------------------------------------------
		

		for_max =  max(forward_score)
		rev_max =  max(reverse_score)
		print 'Best For Score:', for_max, 'index:', `list(forward_score).index(for_max)`
		print 'Best Rev Score:', rev_max, 'index:', `list(reverse_score).index(rev_max)`
	
		if (one and two) or ((not one) and (not two)):
			one = forward_score, reverse_score, sequence
			two = 0
		elif one and (not two):
			# forward_score.reverse()
			# reverse_score.reverse()
			forward_score = [forward_score].reverse()
			reverse_score = [reverse_score].reverse()
			two = forward_score, reverse_score, sequence
	##		diff1 = []
	##		diff2 = []
	##		for index in range(len(one[0])):
	##			diff1.append(one[0][index] - two[1][index])
	##			diff2.append(one[1][index] - two[0][index])
	##		# print [round(val, 20) for val in diff1], '\n', \[round(val, 20) for val in diff2]
	##		print diff1, '\n', diff2
	
		# ------------------------------------------------------------
			if one[0] == two[1]:
				print 'Forward', one[2], 'equals Reverse', two[2]
			else :
				print 'NOT EQUAL', 'one[0] == two[1]'
	
			if one[1] == two[0]:
				print 'Reverse', one[2], 'equals Forward', two[2]
			else :
				print 'NOT EQUAL', 'one[1] == two[0]'
	
		else:
			print 'Error: With first and second'
			sys.exit(1)
	
	#@-body
	#@-node:1::<<check reverse complement>>

			
##	print probability_hash.keys()
##	hash_keys = probability_hash.keys()
##	hash_keys.sort()
##	for key in hash_keys:
##		print key,
##		for probability in probability_hash[key]:
##			print str(round (probability, 3)).ljust(5),
##		print

#@-body
#@-node:1::<<def run>>

def base_count(sequence):
	genome_count_hash = {'A':0, 'C':0, 'G':0, 'T':0,
						 'a':0, 'c':0, 'g':0, 't':0}
	for base in sequence:
		genome_count_hash[base] += 1
	#-----------------------------------------------------
	for base in genome_count_hash.keys():
		if base == base.upper():
			genome_count_hash[base] += genome_count_hash[base.lower()]
			del(genome_count_hash[base.lower()])
	##-----------------------------------------------------
	total_counts = reduce(operator.add, [genome_count_hash[base] for base in genome_count_hash])
	## AT_freq = (genome_count_hash['A'] + genome_count_hash['T'])/total_counts
	## GC_freq = (genome_count_hash['G'] + genome_count_hash['C'])/total_counts
	A_freq = ((genome_count_hash['A'] + genome_count_hash['T'])/total_counts)/2
	T_freq = ((genome_count_hash['A'] + genome_count_hash['T'])/total_counts)/2
	G_freq = ((genome_count_hash['G'] + genome_count_hash['C'])/total_counts)/2
	C_freq = ((genome_count_hash['G'] + genome_count_hash['C'])/total_counts)/2

	return {'A':A_freq, 'C':C_freq, 'G':G_freq, 'T':T_freq}




#@<<def usage>>
#@+node:2::<<def usage>>
#@+body

#@-body
#@-node:2::<<def usage>>



#@<<if main>>
#@+node:3::<<if main>>
#@+body
if __name__ == '__main__':
	
	#@<<import>>
	#@+node:1::<<import>>
	#@+body
	import os
	import flat_prob_matrix_printer_gomer
	#@-body
	#@-node:1::<<import>>

	print os.getcwd()
	main()
	
#@-body
#@-node:3::<<if main>>


#@-body
#@-node:3::<<command line>>
#@-body
#@-node:0::@file prob_matrix_tester_gomer.py
#@-leo
