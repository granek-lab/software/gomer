#! /usr/bin/env python

from __future__ import division

import sys
import re
import operator
import bisect
from  open_file import OpenFile
from parser_gomer import ParseError


NUM_ELEMS = 4	##Number of elements per weight line
COMMENT_TOKEN = '#'
DEFAULT_WEIGHT_TOKEN = "%DEFAULT_WEIGHT"
comment_re = re.compile('^' + COMMENT_TOKEN + '.*')
default_weight_re = re.compile('^' + DEFAULT_WEIGHT_TOKEN + '\s+(.+)\s*$')
weight_info_re = re.compile('^\S+\t\d+\t\d+\s+\S+\s*$')
empty_line_re = re.compile('^\s*$')


class GenomeWeight(object):
	def __init__(self, chrom_label, startcoord, stopcoord, weight):
		if int(stopcoord) < int(startcoord):
			raise ('\n'.join((
				'Stop coordinate cannot be less than start coordinate:',
				'chrom_label: ' + chrom_label,
				'start: ' + str(startcoord),
				'stop: ' + str(stopcoord),
				'weight: ' + str(weight)
				)))


		self.chrom_label = chrom_label	# chrom_label is a string

		# bp space coordinates
		self.bp_startcoord = int(startcoord)	# startcoord is an integer
		self.bp_stopcoord = int(stopcoord)		# stopcoord is an integer
		# Ka (array) space coordinates
		self.startcoord = self.bp_startcoord - 1
		self.stopcoord = self.bp_stopcoord - 1

		self.weight = float(weight)			# weight is a float number

	def __str__(self):
		return '\t'.join([str(x) for x in [self.chrom_label,
										   self.bp_startcoord,
										   self.bp_stopcoord,
										   self.weight]])

	## Two genome weight instances are considered equal if their base
	## coordinates overlap at all.  If two genome weights do not overlap,
	## then one must be strictly less than (to the left in the genome) or
	## greater than (to the right of on the genome) the other.


	## ============================================================
	##
	## A < B      A ---------
	##                                B -------
	## 
	## ============================================================
	## 
	## A > B                           A ------
	##          B --------
	## 
	## ============================================================
	## 
	## A = B          A ------
	##          B --------
	## 
	## ============================================================
	## 
	## A = B          A ------
	##                   B --------
	## 
	## ============================================================
	## 
	## A = B          A ------
	##                B ------
	## 
	## ============================================================
	## 
	## A = B              A ------
	##                B ------------
	## B.contains(A)
	## ============================================================
	## 
	## A = B          A -----------
	##                   B ------
	## A.contains(B)
	## ============================================================
	def contains(self,other):
		# other falls entirely within self
		return ((self.startcoord <= other.startcoord) and
				(other.stopcoord <= self.stopcoord))

	def overlap_end(self,other):
		return min ((self.stopcoord, other.stopcoord))

		
	def overlap_amount(self,other):
		if self != other:
			return 0
		elif self.contains(other):
			return len(other)
		elif other.contains(self):
			return len(self)
		else:
			if other.stopcoord < self.stopcoord:
				return (other.stopcoord - self.startcoord) + 1
			else:
				return (self.stopcoord - other.startcoord) + 1
		## ============================================================
		## A = B
		##               1         2
		##     012345678901234567890123456789
		##               A ------
		##          B --------
		## 
		## A=(12,17) 
		## B=(7,14)
		##
		## overlap = (b.stop - a.start) + 1 = (14-12)+1 = 3  <----
		## overlap = (a.stop - b.start) + 1 = (17-7)+1 = 11
		## ============================================================
		## A = B
		##               1         2
		##     012345678901234567890123456789
		##
		##                A -------
		##                   B --------
		## 
		## A=(13,19) 
		## B=(16,23)
		##
		## overlap = (b.stop - a.start) + 1 = (23-11)+1 = 13
		## overlap = (a.stop - b.start) + 1 = (19-16)+1 = 4   <----
		## ============================================================

			

	def overlaps(self,other):
		return self == other

	def __len__(self):
		return (self.stopcoord - self.startcoord) + 1

	def __lt__(self,other):
		# self < other
		return self.stopcoord < other.startcoord
	def __gt__(self,other):
		# self > other
		return other.stopcoord < self.startcoord
##--------------------------------------------------------------------
	def __eq__(self, other):
		# if neither (self < other) nor (self > other), self == other ???? 
		return not ((self.stopcoord < other.startcoord)
					or
					(other.stopcoord < self.startcoord))
	def __ne__(self, other):
		# if (self < other) or (self > other), self != other ???? 
		return ((self.stopcoord < other.startcoord)
				or
				(other.stopcoord < self.startcoord))

	def __le__(self,other):
		# self < other
		return ((self < other) or
				(self == other))
	def __ge__(self,other):
		# self > other
		return ((self > other) or
				(self == other))
##--------------------------------------------------------------------

class ArraySpaceDummyWeight(GenomeWeight):
	def __init__(self, chrom_label, startcoord, stopcoord, weight=None):
		if int(stopcoord) < int(startcoord):
			raise ('\n'.join((
				'Stop coordinate cannot be less than start coordinate:',
				'chrom_label: ' + chrom_label,
				'start: ' + str(startcoord),
				'stop: ' + str(stopcoord),
				'weight: ' + str(weight)
				)))

		self.chrom_label = chrom_label	# chrom_label is a string

		# Ka (array) space coordinates
		self.startcoord = int(startcoord)	# startcoord is an integer
		self.stopcoord = int(stopcoord)		# stopcoord is an integer
		# bp space coordinates
		self.bp_startcoord = self.startcoord + 1
		self.bp_stopcoord = self.stopcoord + 1
		if weight:
			self.weight = float(weight)
		else:
			self.weight = None
			

#======================================================================
#======================================================================

# Except for weight statements, only comments are allowed throughtout the file. Comments are
# demarcated by a line starting with a '#' character.
# Any line which is empty, or contains only white space is ignored.
# The fields of the weight statements are as follows, they must be tab separated:
# CHROMOSOME_LABEL	START	STOP	WEIGHT
# Stop coordinate is inclusive.
# START and STOP are expressed in bp positions
# A default weight may be specified by "%DEFAULT_WEIGHT XXXXX" on its own line, where XXXXX is the default weight




def GenomeWeightParse(filename):
	
	filehandle = OpenFile(filename, 'Parse Genome Weight')
	line_number = 0
	default_weight = 1
	genome_weight_dict = {}

	for line in filehandle:
		line_number += 1
		if comment_re.search(line) or empty_line_re.search(line):
			continue
		elif default_weight_re.match(line):
			default_weight = float(default_weight_re.match(line).group(1).strip())
		elif weight_info_re.match(line):
			split_line = line.rstrip().split('\t')
			if len(split_line) == 4:
				chrom_label, startcoord, stopcoord, weight = split_line
 				## if int(stopcoord) < int(startcoord):
 				##	raise GenomeWeightParseError (filehandle.name, line, line_number, 'Stop coordinate cannot be less than start coordinate')
				new_weight = GenomeWeight(chrom_label, startcoord, stopcoord, weight)
				overlapping_weight =  add_genome_weight(new_weight, genome_weight_dict)
				## overlapping_weight =  test_add_genome_weight(new_weight, genome_weight_dict)
				if overlapping_weight:
					raise GenomeWeightParseError (filehandle.name, line, line_number,
												  'Previous weight (' + str(overlapping_weight) + ') overlaps with weight region read from file')
			else:
				raise GenomeWeightParseError (filehandle.name, line, line_number, 'Weight file should have ' + str(NUM_ELEMS) + \
											  ' elements per line, not ' + str(len(split_line)) + ' elements')
		else:
			raise GenomeWeightParseError (filehandle.name, line, line_number, 'Parse Error: Unrecognized line')

	if not genome_weight_dict:
		print 'No weights in this file.'
		sys.exit(1)

	filehandle.close()
	return genome_weight_dict, default_weight

def add_genome_weight(new_weight, genome_weight_dict):
	sorted_weight_list = genome_weight_dict.setdefault(new_weight.chrom_label, [])

	insert_index = bisect.bisect(sorted_weight_list, new_weight)
	if sorted_weight_list: # if the list isn't empty
		## check element to the left
		if insert_index > 0:
			left_index = insert_index - 1
			if sorted_weight_list[left_index] == new_weight:
				return sorted_weight_list[left_index] ##overlapping weight region
		## check element at the insertion point (will be to the right)
		if insert_index < len(sorted_weight_list):
			if sorted_weight_list[insert_index] == new_weight:
				return sorted_weight_list[insert_index] ##overlapping weight region
	sorted_weight_list.insert(insert_index, new_weight)
	return None




#@<<Parse Errors>>
#@+body
class GenomeWeightParseError(ParseError):
	pass

#@<<command line>>
#@+body
def usage():
	print '-'*70 + '\n', 'Argument given(', len(sys.argv), '):', sys.argv, + '\n' + '-'*70
	sys.exit(1)

#@<<if main>>
#@+body

def main(weight_filename):
	return GenomeWeightParse(weight_filename)

def test_equality(weight_filename):
	test_dict, default_weight = GenomeWeightParse(weight_filename)
	test_tuples = [('1A','1B'),
				   ('2A','2B'),
				   ('3A','3B'),
				   ('4A','4B'),
				   ('5A','5B'),
				   ('6A','6B'),
				   ('7A','7B')]
	
	function_list = [operator.lt,
					 operator.gt,
					 operator.eq, 
					 operator.ne, 
					 operator.le, 
					 operator.ge]

	print ' '*5 , '\t\t', ' '.join([x.__name__ for x in function_list])


	for a_name, b_name in test_tuples:
		a_weight = test_dict[a_name]
		b_weight = test_dict[b_name]

		test_results = [cur_fun(a_weight, b_weight) for cur_fun in function_list]
		print 'A:', a_weight
		print 'B:', b_weight
		print a_weight.chrom_label, b_weight.chrom_label, '\t\t', '  '.join([str(int(x)) for x in test_results])
		print
	##============================================================
	print '='*60
	for a_name, b_name in test_tuples:
		a_weight = test_dict[b_name]
		b_weight = test_dict[a_name]
		
##		test_results = [a_weight < b_weight,
##						a_weight > b_weight,
##						a_weight == b_weight,
##						a_weight != b_weight,
##						a_weight <= b_weight,
##						a_weight >= b_weight]
		test_results = [cur_fun(a_weight, b_weight) for cur_fun in function_list]
		print 'A:', a_weight
		print 'B:', b_weight
		print a_weight.chrom_label, b_weight.chrom_label, '\t\t', '  '.join([str(int(x)) for x in test_results])
		print
	##============================================================

def test_add_genome_weight(new_weight, genome_weight_dict):
	genome_weight_dict[new_weight.chrom_label] = new_weight
	print >>sys.stderr, 'test_add_genome_weight'
	return None

def test_insertion_point(genome_weight_dict, weights_list, bisect_func):
	##------------------------------------------------------------
	sorted_chrom_weights = genome_weight_dict['1']
	print 'len(sorted_chrom_weights)', len(sorted_chrom_weights)
	for test_weight in weights_list:

		insertion_point = bisect_func(sorted_chrom_weights, test_weight)

		print '\ntest_weight:', test_weight
		print 'insertion_point:', insertion_point
		if insertion_point < len(sorted_chrom_weights):
			print 'sorted_chrom_weights[insertion_point]:', sorted_chrom_weights[insertion_point]
			if test_weight == sorted_chrom_weights[insertion_point]:
				print 'test_weight == sorted_chrom_weights[insertion_point]'
			if sorted_chrom_weights[insertion_point].contains(test_weight):
				print 'sorted_chrom_weights[insertion_point].contains(test_weight)'
		if insertion_point >= 0:
			print 'sorted_chrom_weights[insertion_point-1]:', sorted_chrom_weights[insertion_point-1]
			if test_weight == sorted_chrom_weights[insertion_point-1]:
				print 'test_weight == sorted_chrom_weights[insertion_point-1]'
			if sorted_chrom_weights[insertion_point-1].contains(test_weight):
				print 'sorted_chrom_weights[insertion_point-1].contains(test_weight)'
		if insertion_point+1 < len(sorted_chrom_weights):
			print 'sorted_chrom_weights[insertion_point+1]:', sorted_chrom_weights[insertion_point+1]
			if test_weight == sorted_chrom_weights[insertion_point+1]:
				print 'test_weight == sorted_chrom_weights[insertion_point+1]'
			if sorted_chrom_weights[insertion_point+1].contains(test_weight):
				print 'sorted_chrom_weights[insertion_point+1].contains(test_weight)'

def find_transition_zones(genome_weight_dict):
	sorted_chrom_weights = genome_weight_dict['1']
	matrix_len = 10
	site_start_Ka_index = 102
	site_stop_Ka_index = ((site_start_Ka_index + matrix_len) - 1)
	# site_start_Ka_index = 103

	"""
	        1         1         1         1
			0         1         2         3
			0123456789012345678901234567890
            --                                 10
              --                               20
                --                             30
                  --                           40
                    --                         50
                      --                       60
                        --                     70
                          --                   80
                            --                 90
                              --              100
                                                            
              ==========    102 to 111 weight = (2/10*20) + (2/10*30) + (2/10*40) + (2/10*50) + (2/10*60) = 40
              ==========    102 to 111 weight = (2/10*20) + (2/10*30) + (2/10*40) + (2/10*50) + (2/10*60) = 40
	"""
	
	site_dummy_weight = GenomeWeight('1', site_start_Ka_index, site_stop_Ka_index, 1.0)
	site_left_insert_index = bisect.bisect_left(sorted_chrom_weights, site_dummy_weight)
	# site_right_insert_index = bisect.bisect_right(sorted_chrom_weights, site_dummy_weight)
	weight_sum = 0

	while (site_stop_Ka_index >= weight_region_start_index):
		print 'what about'
		
		print 'cur_weight_region:', cur_weight_region
		print 'base_overlap = '
		weight_sum += (base_overlap/matrix_len) * cur_weight_region.weight


		chrom_weight_index += 1
		if chrom_weight_index == len(sorted_chrom_weights):
			# deal with the end of the weight region
			break
		else:
			print 'DO SOMETHING HERE!!!!!!'
		cur_weight_region = sorted_chrom_weights[weight_index]
		weight_region_start_index = cur_weight_region.startcoord


	for weight_index in range (site_left_insert_index, site_right_insert_index):
		cur_weight_region
		print sorted_chrom_weights[weight_index]
		print 'overlap'
		print 'weight_sum += (base_overlap/matrix_len) * '
	

if __name__ == '__main__':
	weight_filename = sys.argv[1]
	# test_equality(weight_filename)
	genome_weight_dict, default_weight = main(weight_filename)
	print 'Default Weight:', default_weight
##	test_insertion_point(genome_weight_dict,
##						 [GenomeWeight(param_tuple) for param_tuple in
##						  [('1', 50, 50, 100),
##						   ('1', 99, 99, 100),
##						   ('1', 100, 100, 100),
##						   ('1', 101, 101, 100),
##						   ('1', 499, 499, 100),
##						   ('1', 500, 500, 100),
##						   ('1', 501, 501, 100),
##						   ('1', 800, 800, 100),
##						   ('1', 950, 950, 100),
##						   ('1', 1100, 1100, 100),
##						   ('1', 2000, 2000, 100),
##						   ('1', 2200, 2200, 100),
##						   ('1', 2600, 2600, 100)]], 
##						 bisect.bisect)
	matlen = 5
	test_insertion_point(genome_weight_dict,
						 [GenomeWeight(chrom, start, (start+matlen)-1, weight) for
						  chrom, start, weight in
						  [('1', 50, 100),
						   ('1', 95, 100),
						   ('1', 96, 100),
						   ('1', 100, 100),
						   ('1', 120, 100),
						   ('1', 495, 100),
						   ('1', 496, 100),
						   ('1', 498, 100),
						   ('1', 500, 100),
						   ('1', 501, 100),
						   ('1', 2200, 100),
						   ('1', 2499, 100),
						   ('1', 2500, 100),
						   ('1', 2501, 100),
						   ('1', 2600, 100)]], 
						 bisect.bisect_left)

	# find_transition_zones(genome_weight_dict)
