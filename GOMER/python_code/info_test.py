def info(*args):
	sum = 0
	for arg in args:
		sum += -(arg * LogBase(arg,2))
	return 2 - sum

info(0.25, 0.25, 0.25, 0.25)
0.0
info(1, 1e-20, 1e-20, 1e-20)
		
