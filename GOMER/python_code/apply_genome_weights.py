#! /usr/bin/env python

from __future__ import division

import bisect
from genome_weight_parser import GenomeWeight, ArraySpaceDummyWeight, GenomeWeightParse
import operator
import Numeric

# genome_weight_hash is the result of genome_weight_parser

"""
The weight on a binding site is a weighted sum of the weights intersecting that site.  So if the first 3bp of a 10bp falls within weight 5 and the last 7bp falls within weight 2, the weight on the site will be (3/10 * 8) + (7/10 * 2) = 3.8

-----------------------
default = 10

matrix_len = 5

start stop  weight
 50   100     20
 200  300     40
 301  350     30
-----------------------

============================================================
Weight Region --> Default Region
--------------------------------

weight run goes through ((stopcoord - matrix_length) + 1)
transition starts at ((stopcoord - matrix_length) + 2)


          1         1         1
          0         1         2
0123456789012345678901234567890        weight
-----                                   20
      -----                             20
      
       -----                            18
        -----                           16
         -----                          14
          -----                         12
                                        
           -----                        10
            -----                       10
             -----
          1         1         1
          0         1         2
0123456789012345678901234567890

============================================================
Default Region --> Weight Region
--------------------------------


default_weight run goes through (next_start - matrix_length)
transition starts at ((next_start - matrix_length) + 1)

          2         2         2
          0         1         2
0123456789012345678901234567890        weight
-----                                  10
     -----                             10
                                         
      -----                            16
       -----                           22
        -----                          28
         -----                         34
                                        
          -----                        40
           -----                       40
            -----
          2         2         2
          0         1         2
0123456789012345678901234567890

============================================================
Weight Region --> Weight Region
--------------------------------
weight run goes through ((stopcoord - matrix_length) + 1)
transition starts at ((stopcoord - matrix_length) + 2)

          3         3         3
          0         1         2
0123456789012345678901234567890        weight
-----                                   40
      -----                             40
                                         
       -----                            38
        -----                           36
         -----                          34
          -----                         32
                                        
           -----                        30
            -----                       30
             -----
          3         3         3
          0         1         2
0123456789012345678901234567890

------------------------------------------------------------
matlen = 5
weight1 = 40 ; weight2 = 30 ;
for x in range(matlen+1):
     (((matlen-x)/matlen) * weight1) + ((x/matlen) * weight2)
------------------------------------------------------------
python genome_weight_parser.py test_weights.txt
------------------------------------------------------------
matlen = 5
mat_index = 290
mat_last_base_index = (mat_index + matlen) - 1
"""


# def ApplyGenomeWeights(slice_of_Kas, Ka_slice_offset, Ka_slice_chrom, matrix_length,
def ApplyGenomeWeights(Ka_slice, Ka_slice_offset, Ka_slice_chrom, matrix_length,
					   genome_weight_dict, default_genome_weight):
	"""
	The slice_of_Kas can consist of all of the Kas on a chromosome, or a subset thereof.  Therefore, we need to know the array index of the first Ka_window in the slice (i.e. the Ka_slice_offset)
	"""
##============================================================
##============================================================


## need to determine the first weight in relation to the slice

## 1. Deal with the begining of the slice.
## 2. Deal with weight-weight, weight-no weight, and no weight-weight interfaces (proportional weights)
## 3. Deal with continuous weight runs (easy)  ---> all Kas from weight_region_start_index through ((weight_region_stop_index - matrix_len) + 1)
## 4. deal with the end of the slice

	# Ka_slice_offset_bp = Ka_slice_offset + 1 # translated from array index to bp position
	# sorted_chromosome_weights = genome_weight_dict[str(Ka_slice_chrom)]
	sorted_chromosome_weights = genome_weight_dict.get(str(Ka_slice_chrom), [])
	weighted_Ka_slice = Numeric.zeros(Ka_slice.shape, Ka_slice.typecode())
	Ka_slice_index = 0
	while Ka_slice_index < len(Ka_slice):
		## for each index figure out what to do, in the case of runs, we will be able to skip a bunch of indices at a time
		offset_Ka_slice_index = Ka_slice_index + Ka_slice_offset

		# slice_index_dummy_weight = ArraySpaceDummyWeight(Ka_slice_chrom, offset_Ka_slice_index, offset_Ka_slice_index, 1)
		site_dummy_weight = ArraySpaceDummyWeight(Ka_slice_chrom, offset_Ka_slice_index, (offset_Ka_slice_index + matrix_length)-1)
		weight_insertion_index = bisect.bisect_left(sorted_chromosome_weights, site_dummy_weight)
		if weight_insertion_index == len(sorted_chromosome_weights): ### if we are past the last weighted region
			# print 'WE ARE PAST THE LAST WEIGHTED REGION.'
			# print 'THIS SECTION TO THE END OF THE SLICE MUST BE DEFAULT WEIGHTED'
			# print 'WE CAN WEIGHT IT, THEN SKIP TO THE END OF THE SLICE'
			# print 'YAY, WE ARE DONE!!!!'
			weighted_Ka_slice[Ka_slice_index:] = Ka_slice[Ka_slice_index:] * default_genome_weight
			Ka_slice_index = len(Ka_slice)
		else:
			insertion_weight = sorted_chromosome_weights[weight_insertion_index]
##			print '\nsite_dummy_weight', site_dummy_weight
##			print 'insertion_weight', insertion_weight
			if site_dummy_weight == insertion_weight:
##				print 'site_dummy_weight == insertion_weight'
##				print 'THIS SITE FALLS (at least partially) WITHIN A WEIGHT REGION'
##				print 'IT IS EITHER:'
				if insertion_weight.contains(site_dummy_weight):
##					print 'insertion_weight.contains(site_dummy_weight)'
					# print 'ENTIRELY WITHIN A WEIGHT REGION'
					# print "    1. FIGURE OUT WHERE THE NEXT TRANSITION IS:"
					insertion_weight_run_end_index = (insertion_weight.stopcoord - matrix_length) + 1 
					next_transition_start_index = insertion_weight_run_end_index + 1
					# print "    2. WEIGHT *UP TO* THE NEXT TRANSITION"
					weighted_Ka_slice[Ka_slice_index:next_transition_start_index] = Ka_slice[Ka_slice_index:next_transition_start_index] * insertion_weight.weight
					# print "    3. SKIP THE INDEX TO THE NEXT TRANSITION"
					Ka_slice_index = next_transition_start_index - Ka_slice_offset
				else:
##					print 'IT IS A TRANSITION'
##					print 'IF IT IS A TRANSITION, IT COULD BE:'
##					print '    1. WEIGHT-WEIGHT TRANSITION'
##					print '    2. WEIGHT-DEFAULT TRANSITION'
##					print '    3. DEFAULT-WEIGHT TRANSITION'
##					print 'DEAL WITH THIS!!!'
					weighted_weight_sum = 0
					offset_index = offset_Ka_slice_index
					# while step < (len(matrix_length)):
					while offset_index <= site_dummy_weight.stopcoord:
##						print 'offset_index', offset_index
						# print 'offset_index', offset_index
						transition_dummy_weight = ArraySpaceDummyWeight(Ka_slice_chrom, offset_index, offset_index)
						transition_weight_insertion_index = bisect.bisect_left(sorted_chromosome_weights, transition_dummy_weight)
						if transition_weight_insertion_index < len(sorted_chromosome_weights):
							transition_insertion_weight = sorted_chromosome_weights[transition_weight_insertion_index]
						else:
							transition_insertion_weight = None
##						if transition_insertion_weight == transition_dummy_weight:
##							# print "THIS STEP OVERLAPS A WEIGHT REGION"
##							# print "DEAL WITH THIS"
##							weighted_weight_sum += (transition_insertion_weight.overlap_amount(site_dummy_weight)/matrix_length) * transition_insertion_weight.weight
##							offset_index = insertion_weight.stopcoord
##						else:
						if (not transition_insertion_weight) or (transition_insertion_weight != transition_dummy_weight):
##							print "THIS TRANSITION OVERLAPS A DEFAULT REGION"
							# default_region_start = transition_insertion_weight.stopcoord + 1
							# default_region_start = next_transition_start


							## next_transition_weight_insertion_index = transition_weight_insertion_index + 1
							next_transition_weight_insertion_index = transition_weight_insertion_index


							if next_transition_weight_insertion_index < len(sorted_chromosome_weights):
								default_region_stop = sorted_chromosome_weights[next_transition_weight_insertion_index].startcoord - 1
							else :
								default_region_stop = (len(Ka_slice) + Ka_slice_offset) - 1
							transition_insertion_weight = ArraySpaceDummyWeight(Ka_slice_chrom, offset_index, default_region_stop, default_genome_weight)
						weighted_weight_sum += (transition_insertion_weight.overlap_amount(site_dummy_weight)/matrix_length) * transition_insertion_weight.weight
						offset_index = transition_insertion_weight.stopcoord + 1
						# print transition_insertion_weight
##					print 'weighted_weight_sum', weighted_weight_sum
					weighted_Ka_slice[Ka_slice_index] = Ka_slice[Ka_slice_index] * weighted_weight_sum
					Ka_slice_index += 1
			else:
				# print "THIS SITE FALLS ENTIRELY WITHIN A DEFAULT REGION"
				# print "BUT WE KNOW THAT WE ARE BETWEEN (not beyond) WEIGHT REGIONS"
				# print "OTHERWISE weight_insertion_index == len(sorted_chromosome_weights)"
				# print "    1. FIGURE OUT WHERE THE NEXT TRANSITION IS"
##				print 'site_dummy_weight', site_dummy_weight
##				print 'insertion_weight', insertion_weight
##				print 'offset_Ka_slice_index', offset_Ka_slice_index, Ka_slice_index + Ka_slice_offset
				
				next_weight = sorted_chromosome_weights[weight_insertion_index]
				last_index_of_current_region = next_weight.startcoord - matrix_length
				next_transition_start_index = last_index_of_current_region + 1
				# print "    2. WEIGHT *UP TO* THE NEXT TRANSITION"
				weighted_Ka_slice[Ka_slice_index:next_transition_start_index] = Ka_slice[Ka_slice_index:next_transition_start_index] * default_genome_weight
				# print "    3. SKIP THE INDEX TO THE NEXT TRANSITION"
				Ka_slice_index = next_transition_start_index - Ka_slice_offset
	return weighted_Ka_slice
	#++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
	#++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
	#++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

##	first_weight_index = slice_start_insertion_index
##	last_weight_index =  slice_end_insertion_index
##	if first_weight_index == 0: # slice starts before first weight region
##		current_weight_index = 0
##	else:
##		current_weight_index = first_weight_index - 1

##	if slice_start_dummy_weight == sorted_chromosome_weights[current_weight_index]: # slice starts in the middle of a weight region
##		current_weight = sorted_chromosome_weights[current_weight_index]
##		current_weight_value = current_weight_value.weight
##		weight_run_end_index = ((current_weight.stopcoord - matrix_length) + 1)
##	else: # slice starts between weight regions
##		current_weight_value = default_genome_weight
##		next_weight = sorted_chromosome_weights[current_weight_index]
##		weight_run_end_index = next_weight.startcoord - matrix_length
##	weight_transition_start_index = weight_run_end_index + 1


##	weighted_Ka_slice[:weight_transition_start_index] = Ka_slice[:weight_transition_start_index] * current_weight_value

##	for transition_step in range(len(matrix_length)):
##		## find all weight regions overlapping current window

##		## deal with cases where non-contiguous weight regions overlap one or more binding site:
##		## weight 1 -----------------------------
##		## weight 2                                  ------------------------
##		## 10bp matrix                        ==========


		
##	for weight_index in range(first_weight_index, last_weight_index+1):
##		pass


##============================================================
##============================================================
##============================================================
##============================================================
##	##------------------------------------------------
##	import Numeric
##	a = Numeric.arange(20.0)
##	print a
##	a * 2
##	a[5:8] * 2
##	a[5:8] * 10
##	x = Numeric.zeros((20), Numeric.Float)
##	print x
##	x[5:8] = a[5:8] * 10
##	print x
##	x[10:13] = a[10:13]*100
##	print x
##	##------------------------------------------------
##	current_weight_index =
##	current_weight_region_stop = sorted_chromosome_weights[current_weight_index].stopcoord
##	next_weight_index = current_weight_index+1
##	if next_weight_index < len(sorted_chromosome_weights):
##		next_weight_region_start = sorted_chromosome_weights[current_weight_index].startcoord
##	else:
##		next_weight_region_start = len(slice_of_Kas) + Ka_slice_offset ## check off-by-one
##============================================================
##============================================================
##============================================================
##============================================================
##============================================================
##============================================================
##============================================================

if __name__ == '__main__':
	import sys
	import os
	if len(sys.argv) <2:
		print >>sys.stderr, 'usage:', os.path.basename(sys.argv[0]), 'WEIGHT_FILE [MATRIX_LEN KA_OFFSET KA_SLICE_SIZE]'
		sys.exit()
	else:
		weight_filename = sys.argv[1]
		matrix_length = 10
		Ka_slice_offset = 0
		Ka_slice_size = 1000
		if len(sys.argv) >=3:
			matrix_length = int(sys.argv[2])
		if len(sys.argv) >=4:
			Ka_slice_offset = int(sys.argv[3])
		if len(sys.argv) >=5:
			Ka_slice_size = int(sys.argv[4])
		
	genome_weight_dict, default_weight = GenomeWeightParse(weight_filename)
	print 'Default Weight:', default_weight
	Ka_slice = Numeric.ones((Ka_slice_size), 'd')
	Ka_slice_chrom = '1'

	weighted_Ka_slice = ApplyGenomeWeights(Ka_slice, Ka_slice_offset, Ka_slice_chrom, matrix_length, genome_weight_dict, default_weight)
	print '\n\nbp\tweighted Ka'
	for index in range(len(weighted_Ka_slice)):
		print '\t'.join([str(index + Ka_slice_offset+1), str(weighted_Ka_slice[index])])
		
	Ka_slice_chrom = '2'
	weighted_Ka_slice = ApplyGenomeWeights(Ka_slice, Ka_slice_offset, Ka_slice_chrom, matrix_length, genome_weight_dict, default_weight)
	print '\n\nchromosome:', Ka_slice_chrom
	print 'bp\tweighted Ka'
	for index in range(len(weighted_Ka_slice)):
		print '\t'.join([str(index + Ka_slice_offset+1), str(weighted_Ka_slice[index])])
