from __future__ import division

## This one takes multiple PWMs and features, and normalizes correctly.

import sys
import os
from reverse_complement import reverse, complement
from optparse import OptionParser

from gomer_load_functions import load_chromosome_table, load_feature_file, load_chromosome_sequences, load_probability_matrix

point_size=6
color_array = ['red', 'blue','orange', 'green']


def main():	
	
	version_num = 0.001
	version = '%prog ' + str(version_num)
	usage = 'usage: %prog [options] ChromTable ProbMatrix FEATURE UPSTREAM_BP DOWNSTREAM_BP\n'
	
	parser = OptionParser(usage=usage, version=version)

	parser.add_option('-p', '--postscript',
					  action="store",
					  help='Save to postscript FILE.',
      			                  metavar='FILE')
	parser.add_option('-m', '--probability_matrix',
			  action="append",
			  help='Use additional position weight matrix file PWMFILE.',
			  metavar = 'PWMFILE')

	parser.add_option('-f', '--feature_name',
			  action="append",
			  help='Examine additional FEATURE.',
			  metavar = 'FEATURE')			  

	parser.add_option('-c', '--cutoff',
			  type="float",
			  help='I NEED DOCUMENTATION',
			  metavar = 'I NEED A NAME')			  


					  
	(options, args) = parser.parse_args()
	NORM_NUM_ARGS = 5



	if len(args) == (NORM_NUM_ARGS):
		chromosome_table_file_name = args[0]
		prob_matrix_filename = args[1]
		first_feature_name = args[2]
		upstream_bp = int(args[3])
		downstream_bp = int(args[4])

	else :
		parser.print_help()
		sys.exit(2)

	try:
		prob_matrix_filenames = [prob_matrix_filename]+options.probability_matrix
	except: prob_matrix_filenames = [prob_matrix_filename]
	try:
		feature_names = [first_feature_name]+options.feature_name
	except: feature_names = [first_feature_name]

	print 'PWM files: ',prob_matrix_filenames
	print 'Feature names: ',feature_names

	(feature_file_list,
	 chromosome_file_hash,
	 chromosome_flags_hash,
	 frequency_hash) = load_chromosome_table(chromosome_table_file_name)

	chromosome_hash, feature_dictionary, ambiguous_feature_names, feature_type_dict = load_feature_file(feature_file_list)

	## feature_dictionary has an entry for every non-ambiguous feature_name in the genome
	## REMEMBER THAT feature_dictionary KEYS MUST BE captialized!!!!
	## USE feature_name.upper()


        ## Load chromosome sequences

	load_chromosome_sequences(chromosome_file_hash, chromosome_flags_hash,
				  chromosome_hash, feature_dictionary,
				  ambiguous_feature_names)

	if not frequency_hash:
		print 'NO BASE FREQUENCIES SUPPLIED - CALCULATING . . .'
		frequency_hash = base_count(chromosome_hash)
		print 'FREQUENCIES:', frequency_hash

	## Get PWMs from PWM files, and get overall Kamax.

	probability_matrices = []
	for matrix_filename in prob_matrix_filenames:
		probability_matrices.append(load_probability_matrix(matrix_filename, frequency_hash))


        ## WE NOW DO SCORING FOR EACH FEATURE, ONE AT A TIME:
	# We will store feature info in dictionary features_to_plot
	features_to_plot = {}
	my_features = {}
	for i in range(0,len(feature_names)):
		feature_name = feature_names[i]		

	        # gomer.py -aFILE/--ambiguous=FILE : Print ambiguous names from feature file FILE
	        #is ambiguous_feature_names all caps?
		if feature_name.upper() in ambiguous_feature_names:
			print >>sys.stderr, 'Ambiguous Name:', feature_name
			print >>sys.stderr, '\nOther Names:\n' + \
			      '\n\n'.join([','.join(feature.AllNames()) for feature in ambiguous_feature_names[feature_name.upper()]])
			sys.exit(1)

		# raise NotImplementedError, 'Test to be sure the feature is in feature_dictionary'
		feature = feature_dictionary[feature_name.upper()]


	        ## Define section of chromosome sequence to examine:

		the_chromosome_I_care_about = feature.Chromosome
		chromosome_sequence = the_chromosome_I_care_about.Sequence.Sequence # Don't ask

		# Section of sequence I care about:
		# check whether start > stop
		# if so, switch upstream with downstream and "stop" with "start"
		if feature.Start > feature.Stop:  # feature.Strand = '-' #(-=crick)
			print "Switching upstream and downstream, and start and stop"
			upstream_bp, downstream_bp = downstream_bp, upstream_bp
			start = feature.Stop
			stop = feature.Start
			print 'upstream bp: ',upstream_bp
			print 'downstream bp: ',downstream_bp
			print 'new start:',start
			print 'new stop:',stop
		else:
			start = feature.Start
			print 'Feature Start: ',start
			stop = feature.Stop		
			print 'Feature Stop: ', stop
		if start - upstream_bp - 1 >= 0:
			begin_index = start - upstream_bp - 1
		else:
			begin_index = 0
			print "You asked to look too far upstream. I'll look from the beginning."
		if stop + downstream_bp - 1 <= len(chromosome_sequence):
			end_index = stop + downstream_bp - 1
		else:
			end_index = len(chromosome_sequence)
			print "You asked to look too far downstream. I'll look until the end."
		print 'begin index: ',begin_index,'  End index: ',end_index
		sequence_to_score = chromosome_sequence[begin_index:end_index+1]
		if feature.Strand == '-':  #If feature is on Crick strand
			# want to score reverse complement of sequence
			sequence_to_score = reverse(complement(sequence_to_score))
		##print sequence_to_score
		# I'm sure you already know all this, but I'm going to be an annoying pain in the ass and say it again, and again, and again:
		# 1. be sure to check for off by one errors
		# 2. remember that string index space starts at ZERO and base pair numbering of sequences start at ONE
		# 3. remember that the slice my_string[a:b] does NOT include the character at index 'b' - e.g.:
		# >>> range(20)[0:9]
		# [0, 1, 2, 3, 4, 5, 6, 7, 8]

                ## NOW, FOR EACH TF, SCORE EACH STRAND
		TFs = {}
		for matrix in probability_matrices:  # For each TF
                    hitlist = matrix.FilterScoreSequence(sequence_to_score, filter_cutoff_ratio=0)
                    strands = {'forward':hitlist.Forward,'revcomp':hitlist.ReverseComplement}
                    # Store strand score dictionary for this TF in TFs dictionary:
		    TFs[matrix.Name] = strands

                ## FOR THIS FEATURE, STORE 'TFs' DICTIONARY IN 'features_to_plot' DICTIONARY:
                features_to_plot[feature.Name]=TFs
		my_features[feature.Name] = feature


		# 1. Grab section of sequence you care about from the genome (chromosome)
		# 2. Get the scores for that region.
		# 3. Do your stuff - Graph it!!!
	##===============================================================================

		
	## NORMALIZATION:
	# For each TF, find the highest Ka value (score) found in 
	# all the sequences scored; call this number Kamax.
	# Normalize each TF to its Kamax.
	Kamaxes = {}
	for matrix in probability_matrices:  # For each TF
		possKamaxes = []
		for featurekey in features_to_plot: # For each feature
			scoretuple = features_to_plot[featurekey][matrix.Name]
			for strandkey in scoretuple:
				possKamaxes.append(max(scoretuple[strandkey]))
	        Kamax = max(possKamaxes)
	        Kamaxes[matrix.Name] = Kamax
	for matrix in probability_matrices:
		for featurekey in features_to_plot: # For each feature
			scoretuple = features_to_plot[featurekey][matrix.Name]
			scoretuple['forward'] = scoretuple['forward']/Kamaxes[matrix.Name]
			scoretuple['revcomp'] = -1 * scoretuple['revcomp']/Kamaxes[matrix.Name]



	## GRAPHING
	import biggles
	import Numeric
	framearray = biggles.FramedArray(len(features_to_plot), 1)
	framearray.ylabel='Normalized window score (Ka) for each feature'
	framearray.xlabel='Window number'
	framearray.yrange = (-1,1)
	framearray.cellspacing=2.
	#framearray.title=''
	w = 0
	for featurekey in features_to_plot:
		#p = biggles.FramedPlot()
		#framearray[w,0].title=featurekey
		# Feature is numbered from 0, and pre-feature indices are < 0
		# So make x-axis run from -upstream_bp to my_features[featurekey].Length+downstream_bp
		framearray[w,0].xrange = (-upstream_bp, my_features[featurekey].Length+downstream_bp)

		# Put a box on the plot to represent the feature
		featurebox = biggles.DataBox((0,-0.25),(my_features[featurekey].Length,0.25), color='red')
		featurelabel = biggles.DataLabel(0,-0.25,featurekey,color='red')
		framearray[w,0].add(featurebox)
		framearray[w,0].add(featurelabel)

		# Plot horizontal lines at y=Kamax and y=-Kamax
		# only do this if not doing normalization
		#line1 = biggles.LineX(Kamax)
		#framearray[w,0].add(line1)
		#line2 = biggles.LineX(-Kamax)
		#framearray[w,0].add(line2)

		# We have a finite number of symbols here; so can't plot more than that
		# many TFs without making changes (using colors, etc.)
		symbols = ["filled circle","filled square","filled triangle","filled diamond","filled inverted triangle","filled fancy square","filled fancy diamond","half filled circle","half filled square","half filled triangle","half filled diamond","half filled inverted triangle","half filled fancy square","half filled fancy diamond","filled octagon","circle","square","cross","dot","plus","asterisk","triangle","diamond","star","inverted triangle","starburst","fancy plus","fancy cross","fancy square","fancy diamond","octagon"]
		# For each TF, plot normalized forward strand scores vs. 
		# window number and opposite of normalized reverse
		# complement scores vs. window number (renumbering windows
		# to correspond with 0 at start of feature)
		q = 0
		if options.cutoff:
			cutoff = options.cutoff
			for matrix in probability_matrices:
				print matrix.Name, symbols[q]
				scoretuple = features_to_plot[featurekey][matrix.Name]
				x_list = []
				y_list = []
				for x,y in zip(Numeric.arange(-upstream_bp,len(scoretuple['forward'])-upstream_bp),
					       scoretuple['forward']):
					# if y >= cutoff or (-cutoff >= y):
					if y >= cutoff:
						x_list.append(x)
						y_list.append(y)
				center_line = biggles.LineY(0) 
				framearray[w,0].add(center_line)
				if x_list:
					x1 = biggles.Points(x_list, y_list, color=color_array[q],type=symbols[q], size=point_size)
					framearray[w,0].add(x1)
				##-----------------------------------------------------------
				x_list = []
				y_list = []
				for x,y in zip(Numeric.arange(-upstream_bp,len(scoretuple['revcomp'])-upstream_bp),
					       scoretuple['revcomp']):
					# if y >= cutoff or (-cutoff >= y):
					if -cutoff >= y:
						x_list.append(x)
						y_list.append(y)
				if x_list:
					x2 = biggles.Points(x_list, y_list, color=color_array[q],type=symbols[q], size=point_size)
					framearray[w,0].add(x2)
				
				##-----------------------------------------------------------
				q = q + 1
		else:
			for matrix in probability_matrices:
				scoretuple = features_to_plot[featurekey][matrix.Name]




				print matrix.Name, symbols[q]
				
				x1 = biggles.Points(Numeric.arange(-upstream_bp,len(scoretuple['forward'])-upstream_bp), scoretuple['forward'], color=color_array[q], type=symbols[q], size=point_size)
				framearray[w,0].add(x1)
				x2 = biggles.Points(Numeric.arange(-upstream_bp,len(scoretuple['revcomp'])-upstream_bp), scoretuple['revcomp'], color=color_array[q], type=symbols[q], size=point_size)
				# TF has same symbol for both forward&revcomp scores
				framearray[w,0].add(x2)
				center_line = biggles.LineY(0) 
				framearray[w,0].add(center_line)
				q = q + 1
	#	p.show()
	#	framearray[w,0].add(p)
		w = w + 1

	if options.postscript:
		# if user wanted ps, then make postscript file with that filename
		framearray.write_eps(options.postscript)
	else:
		framearray.show()
	



if __name__ == '__main__':
	main()


"""	
Help on module genome_feature_gomer:

NAME
    genome_feature_gomer

FILE
    /home/josh/GOMER/python_code/genome_feature_gomer.py

CLASSES
    __builtin__.object
        GenomeFeature
    
    class GenomeFeature(__builtin__.object)
     |  Methods defined here:
     |  
     |  AllNames(self)
     |      A list of all of the names for this feature
     |  
     |  Properties defined here:
     |  Chromosome
     |  ChromosomeName
     |  CommonName
     |  IsOrf
     |  Length
     |  Name
     |  Start
     |  Stop
     |  Strand
     |  Types

Help on module chromosome_gomer:

NAME
    chromosome_gomer

FILE
    /home/josh/GOMER/python_code/chromosome_gomer.py

    class Chromosome(__builtin__.object)
     |  Name
     |  Sequence
"""	



