#! /usr/bin/env python
from __future__ import division
#@+leo
#@+node:0::@file sequence_gomer.py
#@+body
#@@first
#@@first
#@@language python

"""
*Description:

A class for representing chromosomal sequences.  
This is mostly a wrapper on the sequence string, it doesn't really provide any
functionality, it just wraps some additional information with the sequence
string: 	

	- the name of the file containing the sequence
	- data from the sequence	file header:
		- the name(number) of the chromosome represented by the sequence
		- the accession number of the sequence
		- modifiers from the sequence file header
"""
##import os
##os.environ['PYCHECKER'] = '--allglobals --stdlib'
##import pychecker.checker

##import check_python_version
##check_python_version.CheckVersion()


#@<<class Sequence>>
#@+node:1::<<class Sequence>>
#@+body
class Sequence(object):

	#@+others
	#@+node:1::__init__
	#@+body
	def __init__(self, sequence, modifier_hash, accession_number, sequence_file_name, chromosome_name):
		self._sequence = sequence
		self._modifier_hash = modifier_hash
		# self._reference = reference
		self._accession_number = accession_number
		self._sequence_file_name = sequence_file_name
		self._chromosome_name = chromosome_name
	
	#@-body
	#@-node:1::__init__
	#@+node:2::__getitem__
	#@+body
	def  __getitem__(self, indexOrSlice):
	    try:
	        return self._sequence[indexOrSlice]
	    except TypeError:
	        return self._sequence[indexOrSlice.start:indexOrSlice.stop]
	
	
	#@-body
	#@-node:2::__getitem__
	#@+node:3::__len__
	#@+body
	def __len__(self):
		return len(self._sequence)
	
	#@-body
	#@-node:3::__len__
	#@+node:4::Sequence
	#@+body
	def _get_sequence(self): return self._sequence
	def _set_sequence(self, value): raise AttributeError, "Can't set 'Sequence' attribute"
	Sequence = property(_get_sequence, _set_sequence)
	
	#@-body
	#@-node:4::Sequence
	#@+node:5::Accession
	#@+body
	def _get_accession(self): return self._accession_number
	def _set_accession(self, value): raise AttributeError, "Can't set 'Accession' attribute"
	Accession = property(_get_accession, _set_accession)
	
	#@-body
	#@-node:5::Accession
	#@-others
#@-body
#@-node:1::<<class Sequence>>




#@<<class SliceTester>>
#@+node:2::<<class SliceTester>>
#@+body
class SliceTester(object):

	#@+others
	#@+node:1::__init__
	#@+body
	def __init__(self, sequence, name):
		self._sequence = sequence
		self._name = name
	
	#@-body
	#@-node:1::__init__
	#@+node:2::__getitem__
	#@+body
	def  __getitem__(self, indexOrSlice):
	    try:
	        return self._sequence[indexOrSlice]
	    except TypeError:
	        return self._sequence[indexOrSlice.start:indexOrSlice.stop]
	
	
	#@-body
	#@-node:2::__getitem__
	#@-others
#@-body
#@-node:2::<<class SliceTester>>


#@-body
#@-node:0::@file sequence_gomer.py
#@-leo
