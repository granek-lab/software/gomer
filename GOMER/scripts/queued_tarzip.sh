DIRECTORY="$1"
HOME_DIR="/nfs/fs/clarke/granek"
TAR_FILE="/local/queen/granek_tarfile$$.tgz"
LOCAL_TARFILE="/local/granek_tarfile$$.tgz"
SCRIPT_NAME="$HOME_DIR/q_tar.sh"
tar -zcf "$TAR_FILE" "$DIRECTORY"

echo "cp $TAR_FILE $LOCAL_TARFILE ; tar -zxf $LOCAL_TARFILE ; rm $LOCAL_TARFILE; sleep 30" > $SCRIPT_NAME
echo $SCRIPT_NAME
cat $SCRIPT_NAME

# for MACHINE in "drone1"  ;
for MACHINE in "drone1" "drone2" "drone3" ;
	do
    qsub -q short $SCRIPT_NAME
done
rm  $SCRIPT_NAME


# MACHINE=`hostname`
# DATA_TARFILE="/local/$MACHINE"_allTFs_data.tgz
# CACHE_TARFILE="/local/$MACHINE"_allTFs_cache.tgz

# cd /local/granek
# echo "tar -zcf $DATA_TARFILE  100X_output"
# echo "tar -zcf $CACHE_TARFILE 100X_TRANS_CACHE"
# tar -zcf $DATA_TARFILE  100X_output
# tar -zcf $CACHE_TARFILE 100X_TRANS_CACHE

# sleep 20

