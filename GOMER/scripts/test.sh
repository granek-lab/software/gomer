PYTHONOPTIMIZE=5

CONFIG=$GOMER_CONFIG
SEQ_TABLE=$GOMER_SEQ_TABLE
CACHE_DIR=$GOMER_CACHE_DIR
HOME_DIR=$GOMER_HOME_DIR

# LOCATION INDEPENDENT
STD_DIR="$HOME_DIR/GOMER/outputs/regression_standards"
OUT_DIR="$HOME_DIR/GOMER/outputs/regress_out"
MATRIX="$HOME_DIR/GOMER/input_files/binding_matrices/paper_gold.probs"
REGULATED="$HOME_DIR/GOMER/input_files/regulated_genes/a1_alpha2_regulated.list"
FILTER="--filter_cutoff_ratio 1e7"
GOMER_CMD="python2.3 $HOME_DIR/GOMER/python_code/gomer.py -c $CONFIG"
SHORT="--short $OUT_DIR/short_output"
#--------------------------------
# rm $OUT_DIR/*
#------------------------------------------------------
# rm -f $CACHE_DIR/*
#------------------------------------------------------
$GOMER_CMD $SHORT $SEQ_TABLE $MATRIX $REGULATED  > $OUT_DIR/full
# $GOMER_CMD $FILTER $SEQ_TABLE $MATRIX $REGULATED  > $OUT_DIR/full_1e7.refiltered
#------------------------------------------------------
rm -f $OUT_DIR/regress_diff
for COMMAND in                                                                 \
	"diff -s $STD_DIR/STD $OUT_DIR/full"                                    \
	;
	do
	echo "--------------------------------------------" >> $OUT_DIR/regress_diff ;
	echo $COMMAND >> $OUT_DIR/regress_diff ;
	$COMMAND >> $OUT_DIR/regress_diff ;
done

