HOME_DIR="/nfs/fs/clarke/granek"
PREFIX="LEU"
# for PREFIX in "CIN5" "FKH2" "GAL4" "GCN4" "PDR1" "PHO4" "RAP1" "YAP1" ;
for TAIL in "0" "500" "1000";
    do
    SCRIPT_NAME="$HOME_DIR/q"_"$PREFIX"_"$TAIL.sh"
    if [ -a $SCRIPT_NAME ] ; then 
	rm -f $SCRIPT_NAME ;
    fi

    echo "/local/granek/GOMER/scripts/all_TF_run_Leu3_3.py $TAIL $PREFIX" > $SCRIPT_NAME
    echo $SCRIPT_NAME
    cat $SCRIPT_NAME
    qsub $SCRIPT_NAME
    rm  $SCRIPT_NAME
    # echo "/local/granek/GOMER/scripts/all_TF_run.py $PREFIX" | qsub -q short
    # echo "/local/granek/GOMER/scripts/all_TF_run.py $TAIL $PREFIX" | qsub
    # echo "/local/granek/GOMER/scripts/all_TF_run.py $TAIL $PREFIX" | cat
done



