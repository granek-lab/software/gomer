TAIL="0"
# TAIL="250"
HOME_DIR="/nfs/fs/clarke/granek"

# QUEUE=""
# QUEUE="-q short"



QUEUE="-q short"
INPUT_DIR="projects/young_all_TFs/random160/clusterGOMER_runfiles_random160young"
OUTPUT_DIR="projects/young_all_TFs/random160/output"
##--------------------------------------------------------------------------
for PREFIX in "R027" "R019" "R030" "R084" "R095" "R093" ;
    do
    SCRIPT_NAME="$HOME_DIR/q"_"$PREFIX"_"$TAIL.sh"
    if [ -a $SCRIPT_NAME ] ; then 
	rm -f $SCRIPT_NAME ;
    fi

    echo "/local/granek/GOMER/scripts/all_TF_run_random.py $INPUT_DIR $OUTPUT_DIR $TAIL $PREFIX" > $SCRIPT_NAME
    echo $SCRIPT_NAME
    cat $SCRIPT_NAME
    # qsub $SCRIPT_NAME
	qsub $QUEUE $SCRIPT_NAME
    # echo "queue command: qsub $QUEUE $SCRIPT_NAME"
    rm  $SCRIPT_NAME
done
##--------------------------------------------------------------------------

QUEUE="-q short"
INPUT_DIR="projects/young_all_TFs/random80/clusterGOMER_runfiles_random80young"
OUTPUT_DIR="projects/young_all_TFs/random80/output"
##--------------------------------------------------------------------------
for PREFIX in "R096" "R049" "R057" "R061" ;
    do
    SCRIPT_NAME="$HOME_DIR/q"_"$PREFIX"_"$TAIL.sh"
    if [ -a $SCRIPT_NAME ] ; then 
	rm -f $SCRIPT_NAME ;
    fi

    echo "/local/granek/GOMER/scripts/all_TF_run_random.py $INPUT_DIR $OUTPUT_DIR $TAIL $PREFIX" > $SCRIPT_NAME
    echo $SCRIPT_NAME
    cat $SCRIPT_NAME
    # qsub $SCRIPT_NAME
	qsub $QUEUE $SCRIPT_NAME
    # echo "queue command: qsub $QUEUE $SCRIPT_NAME"
    rm  $SCRIPT_NAME
done
##--------------------------------------------------------------------------

QUEUE=""
INPUT_DIR="projects/young_all_TFs/random320/clusterGOMER_runfiles_random320young"
OUTPUT_DIR="projects/young_all_TFs/random320/output"
##--------------------------------------------------------------------------

for PREFIX in "R010" "R019" "R012" "R086" "R071" "R066" "R046" "R045" "R029" "R087" "R038" "R054" "R083" "R053" "R099" ;
    do
    SCRIPT_NAME="$HOME_DIR/q"_"$PREFIX"_"$TAIL.sh"
    if [ -a $SCRIPT_NAME ] ; then 
	rm -f $SCRIPT_NAME ;
    fi

    echo "/local/granek/GOMER/scripts/all_TF_run_random.py $INPUT_DIR $OUTPUT_DIR $TAIL $PREFIX" > $SCRIPT_NAME
    echo $SCRIPT_NAME
    cat $SCRIPT_NAME
    # qsub $SCRIPT_NAME
	qsub $QUEUE $SCRIPT_NAME
    # echo "queue command: qsub $QUEUE $SCRIPT_NAME"
    rm  $SCRIPT_NAME
done
##--------------------------------------------------------------------------
