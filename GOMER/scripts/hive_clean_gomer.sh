echo "Cleaning GOMER from the Hive"
for BASE in             \
	"/local"            \
	"/nfs/drone2/local" \
	"/nfs/drone3/local" \
	"/nfs/drone1/local" \
	;
	do
	CUR_DIR="$BASE/granek"
	cd $CUR_DIR
	pwd
## 	ls -ltr $CUR_DIR/granek
## -----GOMER-------
	for CUR_ITEM in \
		"$CUR_DIR/README"     \
		"$CUR_DIR/GOMER"      \
		"$CUR_DIR/gomer.pdf"  \
		"$CUR_DIR/genomes"    \
	;
	do
		if [ -a "$CUR_ITEM" ] ; then 
			chmod -R u+rw $CUR_ITEM ;
			rm -rf $CUR_ITEM ;
		fi
	done
done