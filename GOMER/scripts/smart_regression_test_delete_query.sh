##============================================================
##============================================================
if [ "queen" == `hostname` ]
then
    echo "I am QUEEN!" ;  
	## ln -s /nfs/fs/clarke/granek/hive_home/granek/ /local/granek
fi
if [ "queen" == `hostname` -o "drone1" == `hostname` -o 'drone2' == `hostname` -o 'drone3' == `hostname` ]
then
    echo "I am HIVE" ;  
	BASE_DIR="/local/granek"
	GOMER_BASE="$BASE_DIR/GOMER"
	PYTHON="/local/granek/local/bin/python2.3 -OO"
	CONFIG="-c $GOMER_BASE/input_files/config/gomer_config.hive"
	SEQ_TABLE="$GOMER_BASE/input_files/sequence_file_tables/cerevisiae_chromosome_table_with_mito.hive.oct_2002"
	CACHE_DIR="/local/granek/GOMER_DATA/cache"
	FAST_CACHE="/tmp/granek"
	if ! [ -a "$FAST_CACHE" ] ; then 
        mkdirhier $FAST_CACHE
	fi

else  
    echo "not hive" ;  
	BASE_DIR="/home/josh"
	GOMER_BASE="$BASE_DIR/GOMER"
	PYTHON="python2.3 -OO"
	CONFIG="-c $GOMER_BASE/input_files/config/gomer_config"
	SEQ_TABLE="$GOMER_BASE/input_files/sequence_file_tables/cerevisiae_chromosome_table_with_mito.oct_2002"
	CACHE_DIR="/blob/home/josh/GOMER_DATA/cache"
fi 
##============================================================
##============================================================

## Location Specific: GINGKO
#  BASE_DIR="/home/josh"
#  CACHE_DIR="$BASE_DIR/GOMER_DATA/cache"
#  SEQ_TABLE="$BASE_DIR/GOMER/input_files/sequence_file_tables/cerevisiae_chromosome_table_with_mito.oct_2002"
#  CONFIG="$BASE_DIR/GOMER/input_files/config/gomer_config"
##--------------------------------

# CONFIG=$GOMER_CONFIG
# SEQ_TABLE=$GOMER_SEQ_TABLE
# CACHE_DIR=$GOMER_CACHE_DIR
# BASE_DIR=$GOMER_BASE

# LOCATION INDEPENDENT
## PYTHON='python2.3'
## PYTHON='python -OO'
STD_DIR="$BASE_DIR/GOMER/outputs/regression_standards"
OUT_DIR="$BASE_DIR/GOMER/outputs/regress_out"
MATRIX="$BASE_DIR/GOMER/input_files/binding_matrices/paper_gold.probs"
REGULATED="$BASE_DIR/GOMER/input_files/regulated_genes/a1_alpha2_regulated.list"
FILTER="--filter_cutoff_ratio 1e7"
GOMER_CMD="$PYTHON $BASE_DIR/GOMER/python_code/gomer.py $CONFIG"
SHORT="--short $OUT_DIR/short_output"
DEL="--delete_cache"
#--------------------------------
if [ -a "$OUT_DIR" ] ; then 
	rm $OUT_DIR/* ;
else 
	mkdir $OUT_DIR ;
fi
#--------------------------------
echo CACHE_DIR is "$CACHE_DIR"
#--------------------------------
rm -f $CACHE_DIR/*
#--------------------------------
# /usr/bin/time --append --output=~/GOMER/outputs/full_1e7_C9 

## TESTS TO ADD:
## cooperativity:
##    synthetic tests
##    real tests?
##
#   -tNUM, --top=NUM      Print the top NUM hits in genome, and the feature
#                         closest to that hit
#   -rKAPPA_FILE  PARAM_STRING, --reg_region_kappa=KAPPA_FILE  PARAM_STRING
#   --coop_kappa=KAPPA_FILE  PARAM_STRING PROB_MATRIX FREE_CONC_RATIO K_DIMER_RATIO
#                         see documentation
#   --free_conc_ratio=CONC_RATIO
#                         Calculations will be done using (CONC_RATIO * 1/max_Ka,
#                         where max_Ka is the best possible Ka given by the
#                         PROB_MATRIX) as the free concentration. If this option
#                         is supplied more than once, a separate run will be done
#                         for each CONC_RATIO value
#   -aFILE, --ambiguous=FILE
#                         Print ambiguous names from feature file FILE
#   --feature_types=ORGANISM
#                         Print types of features found in ORGANISM,  if 'help' is
#                         given, known organisms will be printed
#   -fFEATURE, --feature=FEATURE
#                         Genome features of type FEATURE will be considered
#   --frequency=ChromTable
#                         Calculate the base frequencies of the sequences in
#                         ChromTable
#   --no_debug            Turn off some of the debugging info output to STDOUT
#   -PNUM, --pvalue=NUM   Run NUM simulations to estimate the p-value of the
#                         ROCAUC and MNCP values for the results.  Note: this is
#                         only available with versions of Python >=2.3, and can
#                         take a long time, depending on the number of simulations
#                         (1e5 -- 100000 is a good starting point).

## KAPPA FUNCTIONS
#  downstream_single_square_reg_region_model_gomer.py
#  single_gaussian_reg_region_model_gomer.py
#  coordinate_flank_square_reg_region_model_gomer.py
#  same_strand_simple_square_coop_model_gomer.py
#  downstream_same_strand_reg_region_model_gomer.py

#------------------------------------------------------
$GOMER_CMD  $FILTER $SEQ_TABLE $MATRIX $REGULATED -C9  $DEL > $OUT_DIR/full_1e7_C9
echo "$GOMER_CMD  $FILTER $SEQ_TABLE $MATRIX $REGULATED -C9  $DEL > $OUT_DIR/full_1e7_C9"
echo "Hit return to continue"
read Dummy
#------------------------------------------------------
#------------------------------------------------------
$GOMER_CMD  $FILTER $SEQ_TABLE $MATRIX $REGULATED  $DEL > $OUT_DIR/full_1e7.no_compression
echo "$GOMER_CMD  $FILTER $SEQ_TABLE $MATRIX $REGULATED  $DEL > $OUT_DIR/full_1e7.no_compression"
echo "Hit return to continue"
read Dummy
#------------------------------------------------------
#------------------------------------------------------
$GOMER_CMD $SHORT $SEQ_TABLE $MATRIX $REGULATED  > $OUT_DIR/full
echo "$GOMER_CMD $SHORT $SEQ_TABLE $MATRIX $REGULATED  > $OUT_DIR/full"
echo "Hit return to continue"
read Dummy

$GOMER_CMD $FILTER $SEQ_TABLE $MATRIX $REGULATED $DEL > $OUT_DIR/full_1e7.refiltered
echo "$GOMER_CMD $FILTER $SEQ_TABLE $MATRIX $REGULATED $DEL > $OUT_DIR/full_1e7.refiltered"
echo "Hit return to continue"
read Dummy
#------------------------------------------------------
rm -f $CACHE_DIR/*
#------------------------------------------------------
$GOMER_CMD -C9 $SEQ_TABLE $MATRIX $REGULATED  > $OUT_DIR/full_C9
echo "$GOMER_CMD -C9 $SEQ_TABLE $MATRIX $REGULATED  > $OUT_DIR/full_C9"
echo "Hit return to continue"
read Dummy

$GOMER_CMD -C9 $FILTER $SEQ_TABLE $MATRIX $REGULATED $DEL > $OUT_DIR/full_1e7_C9.refiltered
echo "$GOMER_CMD -C9 $FILTER $SEQ_TABLE $MATRIX $REGULATED $DEL > $OUT_DIR/full_1e7_C9.refiltered"
echo "Hit return to continue"
read Dummy
#------------------------------------------------------
rm -f $CACHE_DIR/*
#------------------------------------------------------
$GOMER_CMD --coordinate_feature  $BASE_DIR/GOMER/input_files/tests/coordinate_and_sequence_modes/600_1_upstream_feature_coordinates.tab $SEQ_TABLE  $MATRIX $BASE_DIR/GOMER/input_files/regulated_genes/a1_alpha2_regulated_coord_names.reg  $DEL > $OUT_DIR/full_coordinate_tab
echo "$GOMER_CMD --coordinate_feature  $BASE_DIR/GOMER/input_files/tests/coordinate_and_sequence_modes/600_1_upstream_feature_coordinates.tab $SEQ_TABLE  $MATRIX $BASE_DIR/GOMER/input_files/regulated_genes/a1_alpha2_regulated_coord_names.reg  $DEL > $OUT_DIR/full_coordinate_tab"
echo "Hit return to continue"
read Dummy
#------------------------------------------------------
$GOMER_CMD --sequence_feature $BASE_DIR/GOMER/input_files/tests/coordinate_and_sequence_modes/600_1_upstream_feature_coordinates.fsa $MATRIX $BASE_DIR/GOMER/input_files/regulated_genes/a1_alpha2_regulated_coord_names.reg  $DEL > $OUT_DIR/full_sequence
echo "$GOMER_CMD --sequence_feature $BASE_DIR/GOMER/input_files/tests/coordinate_and_sequence_modes/600_1_upstream_feature_coordinates.fsa $MATRIX $BASE_DIR/GOMER/input_files/regulated_genes/a1_alpha2_regulated_coord_names.reg  $DEL > $OUT_DIR/full_sequence"
echo "Hit return to continue"
read Dummy
#------------------------------------------------------
## $GOMER_CMD --coordinate_feature  ~/GOMER/input_files/tests/coordinate_and_sequence_modes/600_0_upstream_feature_coordinates.csv $SEQ_TABLE  $MATRIX ~/GOMER/input_files/regulated_genes/a1_alpha2_regulated_coord_names.reg  > $OUT_DIR/full_coordinate_csv
#------------------------------------------------------
# /usr/bin/time --append --output=~/GOMER/outputs/full_sequence 
#------------------------------------------------------
rm -f $OUT_DIR/regress_diff
for COMMAND in                                                                 \
	"diff -s $STD_DIR/STD_1e7_C9.precached $OUT_DIR/full_1e7_C9.precached"     \
	"diff -s $STD_DIR/STD_C9 $OUT_DIR/full_C9"                                 \
	"diff -s $STD_DIR/STD $OUT_DIR/full"                                    \
	"diff -s $STD_DIR/STD_1e7_C9.refiltered $OUT_DIR/full_1e7.refiltered"      \
	"diff -s $STD_DIR/STD_1e7_C9.refiltered $OUT_DIR/full_1e7_C9.refiltered"   \
	"diff -s $STD_DIR/STD_1e7_C9 $OUT_DIR/full_1e7_C9"                         \
	"diff -s $STD_DIR/STD_1e7.no_compression $OUT_DIR/full_1e7.no_compression" \
	"diff -s $STD_DIR/STD_coordinate $OUT_DIR/full_coordinate_tab"             \
	"diff -s $STD_DIR/STD_sequence $OUT_DIR/full_sequence"                     \
	"diff -s $STD_DIR/STD_short $OUT_DIR/short_output"                         \
	;
	do
	echo "--------------------------------------------" >> $OUT_DIR/regress_diff ;
	echo $COMMAND >> $OUT_DIR/regress_diff ;
	$COMMAND >> $OUT_DIR/regress_diff ;
done

## echo "--------------------------------------------------------------------------"  >> $OUT_DIR/regress_diff
## echo "diff -s $STD_DIR/STD_C9 $OUT_DIR/full_coordinate_tab" >> $OUT_DIR/regress_diff
## diff -s $STD_DIR/STD_C9 $OUT_DIR/full_coordinate_tab >> $OUT_DIR/regress_diff

# echo "--------------------------------------------------------------------------"  >> $OUT_DIR/regress_diff
# echo "diff -s $STD_DIR/STD_coordinate $OUT_DIR/full_coordinate_csv" >> $OUT_DIR/regress_diff
# diff -s $STD_DIR/STD_coordinate $OUT_DIR/full_coordinate_csv >> $OUT_DIR/regress_diff

## echo "--------------------------------------------------------------------------"  >> $OUT_DIR/regress_diff
## echo "diff -s $STD_DIR/STD_C9 $OUT_DIR/full_sequence" >> $OUT_DIR/regress_diff
## diff -s $STD_DIR/STD_C9 $OUT_DIR/full_sequence >> $OUT_DIR/regress_diff

# echo "diff -s ~/GOMER/outputs/STD_1e7_C9 ~/GOMER/outputs/full_1e7_C9" >> ~/GOMER/outputs/regress_diff
# diff -s ~/GOMER/outputs/STD_1e7_C9 ~/GOMER/outputs/full_1e7_C9 >> ~/GOMER/outputs/regress_diff

# echo "diff -s ~/GOMER/outputs/STD_1e7_C9 ~/GOMER/outputs/full_1e7.no_compression" >> ~/GOMER/outputs/regress_diff
# diff -s ~/GOMER/outputs/STD_1e7_C9 ~/GOMER/outputs/full_1e7.no_compression >> ~/GOMER/outputs/regress_diff

