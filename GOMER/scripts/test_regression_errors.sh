##============================================================
##============================================================
BASE_DIR="/home/josh"
GOMER_BASE="$BASE_DIR/GOMER"
## PYTHON="python2.3 -OO"
PYTHON="python2.3"
CONFIG="-c $GOMER_BASE/input_files/config/gomer_config"
# CONFIG="-c $GOMER_BASE/input_files/config/gomer_config_fast_cache"
SEQ_TABLE="$GOMER_BASE/input_files/sequence_file_tables/cerevisiae_chromosome_table_with_mito.oct_2002"
CACHE_DIR="/home/josh/GOMER_DATA/regression_cache"
FAST_CACHE_DIR="/dev/shm/josh"
#---------------------------------------------------------------------
FAST_CACHE_DIR=""  # turn off the fast cache for the moment
if [ $FAST_CACHE_DIR ] ; then
	if ! [ -a "$FAST_CACHE_DIR" ] ; then 
        mkdirhier $FAST_CACHE_DIR
	fi
	FAST_CACHE="--fast_cache=""$FAST_CACHE_DIR"
fi
CACHE="--cache_directory=""$CACHE_DIR"
GOMER_CMD="$PYTHON $BASE_DIR/GOMER/python_code/gomer.py $CONFIG $CACHE $FAST_CACHE"

#------------------------------------------------------
#------------------------------------------------------
STD_DIR="$BASE_DIR/GOMER/outputs/regression_standards/basic"
OUT_DIR="$BASE_DIR/GOMER/outputs/regress_out"
STDOUT="$OUT_DIR/regress_stdout"
STDERR="$OUT_DIR/regress_stderr"

MATRIX="$BASE_DIR/GOMER/input_files/binding_matrices/paper_gold.probs"
REGULATED="$BASE_DIR/GOMER/input_files/regulated_genes/a1_alpha2_regulated.list"
REGULATED_EXCLUDED="$BASE_DIR/GOMER/input_files/regulated_genes/a1_alpha2_regulated.excluded"
FILTER="--filter_cutoff_ratio 1e7"
# GOMER_CMD="$PYTHON $BASE_DIR/GOMER/python_code/gomer.py $CONFIG"
SHORT="--short $OUT_DIR/short_output"
#--------------------------------
#------------------------------------------------------
if [ -a $CACHE_DIR ] ; then 
	rm $CACHE_DIR/* ;
else 
	mkdir $CACHE_DIR ;
fi
#------------------------------------------------------
echo CACHE_DIR is "$CACHE_DIR"
#--------------------------------
# /usr/bin/time --append --output=~/GOMER/outputs/full_1e7_C9 
#------------------------------------------------------
MD5SUM_FILE="$OUT_DIR"/regress_out_md5sums
for COUNT in "1" "2" "3" "4" "5" "6";
	do
	$GOMER_CMD  $FILTER $SEQ_TABLE $MATRIX $REGULATED -C9
	md5sum "$CACHE_DIR"/* >> $MD5SUM_FILE
	mv $CACHE_DIR "$CACHE_DIR"_"$COUNT"
	mkdir $CACHE_DIR ;
done