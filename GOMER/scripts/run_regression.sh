##--------------------------------
export GOMER_BASE="/local/granek"
export GOMER_CACHE_DIR="$GOMER_BASE/GOMER_DATA/cache"
export GOMER_SEQ_TABLE="$GOMER_BASE/GOMER/input_files/sequence_file_tables/cerevisiae_chromosome_table_with_mito.hive"
export GOMER_CONFIG="$GOMER_BASE/GOMER/input_files/config/gomer_config.hive"
# export GOMER_SEQ_TABLE="$GOMER_BASE/GOMER/input_files/cerevisiae_chromosome_table_with_mito.hive"
# export GOMER_CONFIG="$GOMER_BASE/GOMER/input_files/gomer_config.hive"
##--------------------------------

PATH="/local/granek/local/bin:${PATH}"
bash $GOMER_BASE/GOMER/scripts/regression.sh
