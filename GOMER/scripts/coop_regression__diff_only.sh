if [ "queen" == `hostname` -o "drone1" == `hostname` -o 'drone2' == `hostname` -o 'drone3' == `hostname` ]
then
    echo "I am HIVE" ;  
	BASE_DIR="/local/granek"
elif [ "Gober.local" == `hostname` ]
then
    echo "Powerbook!" ;  
	BASE_DIR="/Users/joshgranek/lab"
else  
    echo "not hive" ;  
	BASE_DIR="/home/josh"
fi 
#---------------------------------------------------------------------
STD_DIR="$BASE_DIR/GOMER/outputs/regression_standards/coop"
OUT_DIR="$BASE_DIR/GOMER/outputs/coop_regress_out"
#------------------------------------------------------
#------------------------------------------------------
rm -f $OUT_DIR/coop_regress_diff
#------------------------------------------------------
## DIFF="diff -s -w"
## DIFF="diff -s"
## DIFF="tkdiff"

if [ -n "$1" ]              # Tested variable is quoted.
then
	DIFF=$1
else
	DIFF="diff -s"	
fi 

for FILENAME in                     \
	"seq_ABC_abd__a_b_10to30"		\
	"seq_ABC_abd__a_b_d_0to40"		\
	"seq_ABC_abd__a_b_d_10to30"		\
	"seq_ABC_abz__a_b_10to30"		\
	"seq_AB_ab_0to30"				\
	"seq_AB_ab_0to40"				\
	"seq_AB_ab_10to30"				\
	"seq_AB_ab_coopb_coopb10to30"	\
	"seq_AB_ab_coopb_coopd_10to30"	\
	"seq_AB_bb_10to30"				\
	"seq_AB_bb__no_coop"			\
	"seq_AB_db_10to30"				\
	"seq_AB_db_coopb_coopa_10to30"	\
	"seq_AB_dd__no_coop"			\
	"seq_AB_eb_coopb_coopa_10to30"	\
	"seq_AB_eb_coopb_coope_10to30"	\
	"seq_AB_ee__no_coop"			\
	"seq_AB_zb_coopb_coopa_10to30"	\
	"seq_A_a_10to30"				\
	"seq_A_a__no_coop"				\
	"seq_A_a_coopz_10to30"			\
	"seq_A_a_coopz_no_coop"			\
	"seq_A_b_0to30"					\
	"seq_A_b_0to40"					\
	"seq_A_b_10to30"				\
	"seq_A_b__no_coop"				\
	"seq_A_b_0to30_pconc1_cconc1_kdimer1"	\
	"seq_A_b_0to30_pconc5_cconc1_kdimer1"	\
	"seq_A_b_0to30_pconc1_cconc5_kdimer1"	\
	"seq_A_b_0to30_pconc1_cconc1_kdimer5"	\
	"seq_A_b_0to30_pconc2_cconc5_kdimer1"	\
	"seq_A_b_0to30_pconc5_cconc2_kdimer1"	\
	"seq_A_b_0to30_pconcNone_cconc5_kdimer1"	\
	"coop_random_probs_1"           \
	"coop_random_probs_2"           \
	"coop_random_probs_3"           \
	"coop_regress_stdout"           \
	"coop_regress_stderr"           \
	"Nrd1_Nab3_coop"						\
	"seq_ABC_abd"                   \
	"seq_ABC_abe"					\
	"seq_ABC_abz"					\
	"seq_AB_aa"						\
	"seq_AB_ab"						\
	"seq_AB_bb"						\
	"seq_AB_db"						\
	"seq_AB_dd"						\
	"seq_AB_eb"                     \
	"seq_AB_ee"						\
	"seq_AB_zb"						\
	"seq_A_a"						\
	"seq_A_b"						\
	"seq_A_z"						\
	;
	do
	STD_FILE="$STD_DIR"/STD_"$FILENAME"
	NEW_FILE="$OUT_DIR"/"$FILENAME"
	## echo "STD_FILE: $STD_FILE"
	## echo "NEW_FILE: $NEW_FILE"
	echo "--------------------------------------------" >> $OUT_DIR/coop_regress_diff ;
	COMMAND="$DIFF $NEW_FILE $STD_FILE"
	echo $COMMAND >> $OUT_DIR/coop_regress_diff ;
	$COMMAND >> $OUT_DIR/coop_regress_diff ;
done
