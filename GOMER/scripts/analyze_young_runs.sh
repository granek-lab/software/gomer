DATA_DIR="/nfs/fs/clarke/granek/GOMER/projects/young_all_TFs/set2/output"
ANALYSIS_DIR="/nfs/fs/clarke/granek/GOMER/projects/young_all_TFs/set2/analysis"
PROG="/nfs/fs/clarke/granek/GOMER/python_code/MNCPandROCAUC_for_grace.py"
PYTHON="/usr/bin/python2.2"

# for PREFIX in "CIN5" "FKH2" "GAL4" "GCN4" "PDR1" "PHO4" "RAP1" "YAP1" ;
for PREFIX in "ABF1" "ACE2" "ARO8" "BAS1" "CAD1" "DAL8" "DIG1" "FHL1" "FKH1" "FZF1" "GAT3" "GCR2" "HAP4" "HIR1" "HSF1" "IME4" "MBP1" "MCM1" "NDD1" "NRG1" "PHD1" "REB1" "RLM1" "ROX1" "SKN7" "SMP1" "STE1" "SUM1" "SWI4" "SWI5" "SWI6" "YAP5" "YAP6" ;
    do
	for TAIL in "0" "1000";
		do
		$PYTHON $PROG $DATA_DIR/"$PREFIX"_*.seq.*.bp.flank0_tail"$TAIL".out > $ANALYSIS_DIR/"$PREFIX"_tail"$TAIL".stats
	done
done



