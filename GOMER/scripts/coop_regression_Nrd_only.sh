
## /usr/bin/time -p -o ~/GOMER/outputs/regress_out/coop_run.stdout  --append ~/GOMER/scripts/coop_regression.sh 2>~/GOMER/outputs/regress_out/coop_run.stderr 1>~/GOMER/outputs/regress_out/coop_run.stdout
#---------------------------------------------------------------------
if [ "queen" == `hostname` -o   "drone1" == `hostname` -o 'drone2' == `hostname` -o 'drone3' == `hostname` ]
then
    echo "I am HIVE" ;  
	BASE_DIR="/local/granek"
	GOMER_BASE="$BASE_DIR/GOMER"
	PYTHON="/local/granek/local/bin/python2.3 -OO"
	CONFIG="-c $GOMER_BASE/input_files/config/gomer_config.hive"
	SEQ_TABLE="$GOMER_BASE/input_files/sequence_file_tables/cerevisiae_chromosome_table_with_mito.hive"
	CACHE_DIR="/local/granek/GOMER_DATA/cache"
	FAST_CACHE="/tmp/granek"
	if ! [ -a "$FAST_CACHE" ] ; then 
        mkdirhier $FAST_CACHE
	fi

else  
    echo "not hive" ;  
	BASE_DIR="/home/josh"
	GOMER_BASE="$BASE_DIR/GOMER"
	PYTHON="python2.3 -OO"
	CONFIG="-c $GOMER_BASE/input_files/config/gomer_config"
	SEQ_TABLE="$GOMER_BASE/input_files/sequence_file_tables/cerevisiae_chromosome_table_with_mito"
	CACHE_DIR="/blob/home/josh/GOMER_DATA/cache"
fi 
#---------------------------------------------------------------------
STD_DIR="$BASE_DIR/GOMER/outputs/regression_standards/coop"
OUT_DIR="$BASE_DIR/GOMER/outputs/coop_regress_out"
# #--------------------------------
REGULATED="$BASE_DIR/GOMER/projects/Nrd1/regulated/Nrd_regulated.reg"
GOMER_CMD="$PYTHON $BASE_DIR/GOMER/python_code/gomer.py $CONFIG"
FEATURE_TYPES="-f ORF -f snRNA -f snoRNA"
MATRIX_DIR="$BASE_DIR/GOMER/projects/Nrd1/matrices"
PRIMARY_MATRIX="Nrd1.probs"
#-----------------------------------------
REG_REGION_KAPPA="downstream_same_strand_reg_region_model_gomer.py"
REG_REGION_PARAMS="regulatory_five_prime=1;regulatory_three_prime=100"
REG_REGION="-r $REG_REGION_KAPPA $REG_REGION_PARAMS"
#-----------------------------------------
SECONDARY_MATRIX="$MATRIX_DIR/Nab3.probs"
COOP_DISTANCE="max_distance=70;min_distance=1"
COOP_KAPPA="same_strand_simple_square_coop_model_gomer.py"
SECOND_CONC="DEFAULT"
K_DIMER_RATIO="DEFAULT"
COOP="--coop_kappa $COOP_KAPPA $COOP_DISTANCE $SECONDARY_MATRIX $SECOND_CONC $K_DIMER_RATIO"
#------------------------------------------------------
#------------------------------------------------------
if [ -a "$OUT_DIR" ] ; then 
	rm $OUT_DIR/* ;
else 
	mkdir $OUT_DIR ;
fi
#------------------------------------------------------
#------------------------------------------------------
if [ -a $CACHE_DIR ] ; then 
	rm $CACHE_DIR/* ;
else 
	mkdir $CACHE_DIR ;
fi
#------------------------------------------------------
#------------------------------------------------------
#------------------------------------------------------
#------------------------------------------------------
rm -f $OUT_DIR/coop_regress_diff

#-------------------------------------------------------------------------------
#-------------------------------------------------------------------------------
$GOMER_CMD  $SEQ_TABLE "$MATRIX_DIR"/"$PRIMARY_MATRIX" $REGULATED $FEATURE_TYPES $REG_REGION $COOP > "$OUT_DIR"/Nrd1_Nab3_coop
#-------------------------------------------------------------------------------
#-------------------------------------------------------------------------------

for COMMAND in                                                                 \
	"diff -s $STD_DIR/STD_Nrd1_Nab3_coop $OUT_DIR/Nrd1_Nab3_coop"     \
	;
	do
	echo "--------------------------------------------" >> $OUT_DIR/coop_regress_diff ;
	echo $COMMAND >> $OUT_DIR/coop_regress_diff ;
	$COMMAND >> $OUT_DIR/coop_regress_diff ;
done


if [ -a "$FAST_CACHE" ] ; then 
	rmdir $FAST_CACHE
fi

