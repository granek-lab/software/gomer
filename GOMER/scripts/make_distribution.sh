echo "STEP 1"
GOMER_BASE="/home/josh/"
GOMER_HOME="$GOMER_BASE/GOMER"
SCRIPTS="$GOMER_HOME/scripts"
GOMER_INPUT="$GOMER_HOME/input_files"


DISTRIB="/tmp/GOMER_distribution"
DISTRIB_GOMER="$DISTRIB/GOMER"
EXAMPLES="$DISTRIB_GOMER/examples"
DISTRIB_STD_DIR="$DISTRIB_GOMER/outputs/regression_standards"
DISTRIB_INPUT="$DISTRIB_GOMER/input_files"
DISTRIB_SCRIPTS="$DISTRIB_GOMER/scripts"


TARFILE="/tmp/GOMER_distribution.tgz"
OUT_DIR="$GOMER_BASE/GOMER/outputs/regress_out"

#------------------------------------------
# echo $GOMER_BASE
#------------------------------------------
echo "Removing $DISTRIB"
if [ -a "$DISTRIB" ] ; then 
	chmod -R u+wr $DISTRIB
	rm -rf $DISTRIB
fi
if [ -a "$TARFILE" ] ; then 
	rm $TARFILE
fi
#------------------------------------------
mkdir  $DISTRIB
mkdir  $DISTRIB_GOMER
mkdir  $DISTRIB_SCRIPTS
mkdirhier $EXAMPLES
#------------------------------------------
echo "STEP 2"
cd $GOMER_HOME
#------------------------------------------
echo "STEP 3"
# $SCRIPTS/regression.sh
grep -v "#" $SCRIPTS/regression.sh > $DISTRIB_GOMER/regression.sh
#------------------------------------------
mkdirhier $DISTRIB_STD_DIR
cp $OUT_DIR/full_1e7_C9.precached    $DISTRIB_STD_DIR/STD_1e7_C9.precached  
cp $OUT_DIR/full_C9                  $DISTRIB_STD_DIR/STD_C9 
cp $OUT_DIR/full                     $DISTRIB_STD_DIR/STD 
cp $OUT_DIR/full_1e7.refiltered      $DISTRIB_STD_DIR/STD_1e7_C9.refiltered 
cp $OUT_DIR/full_1e7_C9.refiltered   $DISTRIB_STD_DIR/STD_1e7_C9.refiltered 
cp $OUT_DIR/full_1e7_C9              $DISTRIB_STD_DIR/STD_1e7_C9 
cp $OUT_DIR/full_1e7.no_compression  $DISTRIB_STD_DIR/STD_1e7.no_compression 
cp $OUT_DIR/full_coordinate_tab      $DISTRIB_STD_DIR/STD_coordinate 
cp $OUT_DIR/full_sequence            $DISTRIB_STD_DIR/STD_sequence 
cp $OUT_DIR/short_output             $DISTRIB_STD_DIR/STD_short 
#------------------------------------------
echo "STEP 5"
# for ORIGINAL in              \
# 	gomer_environment        \
# 	hive_clean.sh            \
# 	hive_clean_gomer.sh      \
# 	hive_clean_python.sh     \
# 	hive_install_gomer.sh    \
# 	hive_install_python.sh   \
# 	hive_runs.sh             \
# 	regression.sh            \
# 	run_regression.sh        \
# 	all_TF_run.py            \
# 	hive_clone_local.sh      \
# 	queued_clone.sh          \
#     hive_install_allTFs.sh   \
# 	queue_scripts            \
# 	enqueue.sh               \
# 	;
# do
# 	cp -r "$SCRIPTS/$ORIGINAL" $DISTRIB_SCRIPTS ;
# done
cp -r $SCRIPTS $DISTRIB_GOMER ;
#------------------------------------------
echo "STEP 6"
for ORIGINAL in \
	$GOMER_SEQ_TABLE                                                 \
	$GOMER_CONFIG                                                    \
	$GOMER_HOME/input_files/binding_matrices/paper_gold.probs        \
	$GOMER_HOME/input_files/regulated_genes/a1_alpha2_regulated.reg  \
	$GOMER_HOME/input_files/regulated_genes/a1_alpha2_regulated.list \
	$GOMER_HOME/examples/test_run_std_output      \
	$GOMER_HOME/examples/test_short_output        \
	;
do 
	cp $ORIGINAL $EXAMPLES ;
done
cp $GOMER_HOME/input_files/sequence_file_tables/cerevisiae_chromosome_table_with_mito.oct_2002 $EXAMPLES/cerevisiae_chromosome_table

#------------------------------------------
echo "STEP 6.5"
mkdir $DISTRIB_INPUT
for ORIGINAL in \
	$GOMER_INPUT/regulated_genes/a1_alpha2_regulated.list						 \
	$GOMER_INPUT/regulated_genes/a1_alpha2_regulated_coord_names.reg			 \
	$GOMER_INPUT/regulated_genes/empty.list										 \
	$GOMER_INPUT/binding_matrices/paper_gold.probs								 \
	$GOMER_INPUT/sequence_file_tables/cerevisiae_chromosome_table_with_mito.oct_2002	 \
	$GOMER_INPUT/sequence_file_tables/cerevisiae_chromosome_table_with_mito.jan_2004	 \
	$GOMER_INPUT/config/gomer_config_fast_cache									 \
	$GOMER_INPUT/config/gomer_config.hive	   									 \
	$GOMER_INPUT/config/gomer_config                                             \
	$GOMER_INPUT/config/gomer_config.jan_2004                                    \
	$GOMER_INPUT/tests/coordinate_and_sequence_modes                             \
	$GOMER_INPUT/weight_files/test_weights.sh                                    \
	$GOMER_INPUT/weight_files/tricky_weights.txt                                 \
	$GOMER_INPUT/weight_files/weighting_standard_spreadsheet.txt                 \
	$GOMER_INPUT/weight_files/weighting_standard_spreadsheet.xls                 \
	;
	do 
	DIRNAME=`dirname $ORIGINAL`
	CUR_DIR=`basename $DIRNAME`
	DEST_DIR="$DISTRIB_INPUT"/"$CUR_DIR"
	if ! [ -a "$DEST_DIR" ] ; then 
		mkdir $DEST_DIR ; fi
	cp -r $ORIGINAL $DEST_DIR
done
#------------------------------------------
echo "STEP 7"
mkdir $DISTRIB_GOMER/python_code
cp $GOMER_HOME/python_code/*.py $DISTRIB_GOMER/python_code/

mkdir $DISTRIB_GOMER/python_code/kappa_modules
cp $GOMER_HOME/python_code/kappa_modules/*.py $DISTRIB_GOMER/python_code/kappa_modules/

mkdir $DISTRIB_GOMER/intergenic_primers
for SUBDIR in    \
	oct_23_2002  \
	jan_29_2004  \
	;
	do 
	mkdir $DISTRIB_GOMER/intergenic_primers/$SUBDIR
	for FILE in \
		Intergenic_Coordinates            \
		ORF_and_Intergenic_Coordinates    \
		Yeast_Gene_Pairs_Coordinates      \
		;
		do 
		cp $GOMER_HOME/intergenic_primers/$SUBDIR/$FILE $DISTRIB_GOMER/intergenic_primers/$SUBDIR/$FILE
	done
done
#------------------------------------------
echo "STEP 9"
mkdirhier $DISTRIB/genomes/saccharomyces_cerevisiae/oct_23_2002
mkdirhier $DISTRIB/genomes/saccharomyces_cerevisiae/jan_29_2004

cp -r  $GOMER_BASE/genomes/saccharomyces_cerevisiae/oct_23_2002 $DISTRIB/genomes/saccharomyces_cerevisiae/
cp -r  $GOMER_BASE/genomes/saccharomyces_cerevisiae/jan_29_2004 $DISTRIB/genomes/saccharomyces_cerevisiae/

#------------------------------------------
echo "STEP 10"
echo "Read gomer.pdf"  > $DISTRIB/README
#------------------------------------------
# echo "STEP 11"
# cd ~/GOMER/documentation/
# pdflatex gomer.tex  > /dev/null
# bibtex gomer        > /dev/null
# makeindex gomer     > /dev/null
# pdflatex gomer.tex  > /dev/null
# pdflatex gomer.tex  > /dev/null
# pdflatex gomer.tex  > /dev/null
# echo "STEP 12"

# cp gomer.pdf $DISTRIB
# echo "STEP 13"
cp $GOMER_HOME/documentation/GOMER_manual_from_thesis/GOMER_manual.pdf $DISTRIB

#------------------------------------------
echo "STEP 14"
cd $DISTRIB
tar --dereference -zcf $TARFILE *
#------------------------------------------
echo "Removing $DISTRIB"
if [ -a "$DISTRIB" ] ; then 
	chmod -R u+wr $DISTRIB
	rm -rf $DISTRIB
fi
#------------------------------------------

