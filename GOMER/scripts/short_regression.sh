##============================================================
##============================================================
if [ "queen" == `hostname` ]
then
    echo "I am QUEEN!" ;  
	## ln -s /nfs/fs/clarke/granek/hive_home/granek/ /local/granek
fi
if [ "queen" == `hostname` -o "drone1" == `hostname` -o 'drone2' == `hostname` -o 'drone3' == `hostname` ]
then
    echo "I am HIVE" ;  
	BASE_DIR="/local/granek"
	GOMER_BASE="$BASE_DIR/GOMER"
	PYTHON="/local/granek/local/bin/python2.3 -OO"
	CONFIG="-c $GOMER_BASE/input_files/config/gomer_config.hive"
	SEQ_TABLE="$GOMER_BASE/input_files/sequence_file_tables/cerevisiae_chromosome_table_with_mito.hive.oct_2002"
	CACHE_DIR="/local/granek/GOMER_DATA/cache"
	FAST_CACHE="/tmp/granek"
	if ! [ -a "$FAST_CACHE" ] ; then 
        mkdirhier $FAST_CACHE
	fi

else  
    echo "not hive" ;  
	BASE_DIR="/home/josh"
	GOMER_BASE="$BASE_DIR/GOMER"
	PYTHON="python2.3 -OO"
	CONFIG="-c $GOMER_BASE/input_files/config/gomer_config"
	SEQ_TABLE="$GOMER_BASE/input_files/sequence_file_tables/cerevisiae_chromosome_table_with_mito.oct_2002"
	CACHE_DIR="/blob/home/josh/GOMER_DATA/cache"
fi 
##============================================================
##============================================================
STD_DIR="$BASE_DIR/GOMER/outputs/regression_standards"
OUT_DIR="$BASE_DIR/GOMER/outputs/regress_out"
MATRIX="$BASE_DIR/GOMER/input_files/binding_matrices/paper_gold.probs"
REGULATED="$BASE_DIR/GOMER/input_files/regulated_genes/a1_alpha2_regulated.list"
FILTER="--filter_cutoff_ratio 1e7"
GOMER_CMD="$PYTHON $BASE_DIR/GOMER/python_code/gomer.py $CONFIG"
SHORT="--short $OUT_DIR/short_output"
#--------------------------------
if [ -a "$OUT_DIR" ] ; then 
	rm $OUT_DIR/* ;
else 
	mkdir $OUT_DIR ;
fi
#--------------------------------
echo CACHE_DIR is "$CACHE_DIR"
#--------------------------------
rm -f $CACHE_DIR/*
#--------------------------------
$GOMER_CMD $SHORT $SEQ_TABLE $MATRIX $REGULATED  > $OUT_DIR/full
#------------------------------------------------------
rm -f $CACHE_DIR/*
#------------------------------------------------------
rm -f $OUT_DIR/regress_diff
for COMMAND in                                                                 \
	"diff -s $STD_DIR/STD $OUT_DIR/full"                                    \
	;
	do
	echo "--------------------------------------------" >> $OUT_DIR/regress_diff ;
	echo $COMMAND >> $OUT_DIR/regress_diff ;
	$COMMAND >> $OUT_DIR/regress_diff ;
done

