#! /usr/bin/env python

import os
import sys
import re
import string
import glob
import getopt

#------------------------------------------------------------------------------
base_dir='/local/granek'
gomer_base= os.path.join(base_dir,'GOMER')
queen_gomer_base = '/nfs/fs/clarke/granek/hive_home/granek/GOMER/'
all_tf_base=os.path.join(base_dir,'')
fast_cache_dir='/tmp/granek'
#------------------------------------------------------------------------------
#------------------------------------------------------------------------------
input_dir='projects/Leu3_ellen/2'
output_dir='projects/Leu3_ellen/output/2'
#------------------------------------------------------------------------------
## prob_directory='100X_TRANS_gomer_prob'
## regulated_directory='100X_TRANS_list'
## output_directory='100X_output'
config_filename='input_files/config/gomer_config.hive'
chrom_table_filename='input_files/sequence_file_tables/cerevisiae_chromosome_table_with_mito.hive.jan_2004'
# coord_feature_filename='intergenic_primers/jan_29_2004/Intergenic_Coordinates'
coord_feature_filename='intergenic_primers/jan_29_2004/ORF_and_Intergenic_Coordinates'
script_name='python_code/gomer.py'
#------------------------------------------------------------------------------
final_output_dir = os.path.join(queen_gomer_base, output_dir)
chrom_table_filename   = os.path.join(gomer_base, chrom_table_filename)
output_directory       = os.path.join(gomer_base, output_dir)
input_directory       = os.path.join(gomer_base, input_dir)
config_filename        = os.path.join(gomer_base, config_filename)
coord_feature_filename = os.path.join(gomer_base, coord_feature_filename)
script_name            = os.path.join(gomer_base, script_name)
## prob_directory         = os.path.join(all_tf_base, prob_directory)
## regulated_directory    = os.path.join(all_tf_base, regulated_directory)
#------------------------------------------------------------------------------
if not os.path.exists(output_directory):
	os.makedirs(output_directory)
#------------------------------------------------------------------------------
for filename in [final_output_dir,      
		 chrom_table_filename,  
		 output_directory,
		 input_directory,
		 config_filename,
		 coord_feature_filename,
		 script_name] :
	if not os.path.exists(filename):
		print >>sys.stderr, "File doesn't exist:", filename
		raise
#------------------------------------------------------------------------------
python_command='/local/granek/local/bin/python2.3 -OO'
#------------------------------------------------------------------------------
try:
	opts, args = getopt.getopt(sys.argv[1:], "t", ["test"])
except getopt.GetoptError:
	# print help information and exit:
	usage()
	sys.exit(2)
output = None
test = 0
for o, a in opts:
	if o in ("-t", "--test"):
		test = 1
#------------------------------------------------------------------------------
def usage():
	print >>sys.stderr, 'usage:', sys.argv[0], 'TAIL_LENGTH PREFIX_LIST'
	print >>sys.stderr, 'TAIL_LENGTH can be any positive integer or zero'
	print >>sys.stderr, 'PREFIX_LIST is a list of one or more prefixes, file names begining with these prefixes will be run'
	print >>sys.stderr, '-t/--test for a test run (only run on one file)'

if len(args) >= 2:
	tail_length = int(args[0])
	prefix_list = args[1:]
else:
	usage()
	sys.exit(2)
#------------------------------------------------------------------------------

kappa_filename='coordinate_linear_decay_reg_region_model_gomer.py'
## kappa_options= '"flank=0;tail=1000"'
## kappa_options= '"flank=0;tail=0"'
flank=0
tail=tail_length
kappa_options= '"flank=' + str(flank) + ';tail=' + str(tail) + '"'
#------------------------------------------------------------------------------
options_list = ['-D',
				'--coordinate_feature='+ coord_feature_filename,
				'-c', config_filename,
				'-r', kappa_filename,
				kappa_options
				]
#------------------------------------------------------------------------------
tf_name_re = re.compile('([A-Za-z0-9]+)\-BPwidth\d+\-motif\d+\.prob\.gprob')
#------------------------------------------------------------------------------

if not os.path.exists(fast_cache_dir):
	os.makedirs(fast_cache_dir)

reg_file_list=[]
reg_file_suffix='.gomerset'
prob_file_suffix='reformat.pwm'
long_out_suffix = '.out'
short_out_suffix = '.short'
for prefix in prefix_list:
	reg_file_list.extend(glob.glob(os.path.join(input_directory, prefix+'*'+reg_file_suffix)))
if test:
	reg_file_list = reg_file_list[:1]
for reg_filename in reg_file_list:
	##--------------------------------------------------------
	corename = reg_filename.replace(reg_file_suffix, '')
	prob_filename_list = glob.glob(corename+'*'+prob_file_suffix)
	if test:
		prob_filename_list = prob_filename_list[:1]
	for prob_filename in prob_filename_list:
		output_basename = os.path.basename(prob_filename)
		output_basename = output_basename.replace(prob_file_suffix, '')
		output_basename = output_basename + 'flank'+ str(flank) + '_tail' + str(tail)
		long_output_filename = os.path.join(output_directory, output_basename) + long_out_suffix
		short_output_filename = os.path.join(output_directory, output_basename) + short_out_suffix
		##-------------------------------------------------------
		command_string = ' '.join([python_command, script_name,
					   chrom_table_filename, prob_filename, reg_filename,
					   '-s', short_output_filename]
					  + options_list +
					  ['>',long_output_filename])
		print '-'*50, '\n', command_string, '\n', '-'*50
		os.system(command_string)
		if not os.path.samefile(output_directory, final_output_dir):
			os.system(' '.join(('mv', long_output_filename, final_output_dir, '&')))
			os.system(' '.join(('mv', short_output_filename, final_output_dir, '&')))
