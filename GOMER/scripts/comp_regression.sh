
if [ "queen" == `hostname` -o "drone1" == `hostname` -o 'drone2' == `hostname` -o 'drone3' == `hostname` ]
then
    echo "I am HIVE" ;  
	BASE_DIR="/local/granek"
	GOMER_BASE="$BASE_DIR/GOMER"
	## PYTHON="/local/granek/local/bin/python2.3 -OO"
	PYTHON="/local/granek/local/bin/python2.3"
	CONFIG="-c $GOMER_BASE/input_files/config/gomer_config.jan_2004.hive"
	SEQ_TABLE="$GOMER_BASE/input_files/sequence_file_tables/cerevisiae_chromosome_table_with_mito.hive.jan_2004"
	CACHE_DIR="/local/granek/GOMER_DATA/comp_regression_cache"
	FAST_CACHE_DIR="/tmp/granek"
elif [ "Gober.local" == `hostname` ]
then
    echo "Powerbook!" ;  
	BASE_DIR="/Users/joshgranek/lab"
	GOMER_BASE="$BASE_DIR/GOMER"
	## PYTHON="python2.3 -OO"
	PYTHON="python2.3"
	CONFIG="-c $GOMER_BASE/input_files/config/gomer_config.pb.jan_2004"
	SEQ_TABLE="$GOMER_BASE/input_files/sequence_file_tables/cerevisiae_chromosome_table_with_mito.pb.jan_2004"
	CACHE_DIR="$BASE_DIR/GOMER_DATA/comp_regression_cache"
else  
    echo "not hive" ;  
	BASE_DIR="/home/josh"
	GOMER_BASE="$BASE_DIR/GOMER"
	## PYTHON="python2.3 -OO"
	PYTHON="python2.3"
	CONFIG="-c $GOMER_BASE/input_files/config/gomer_config.jan_2004"
	SEQ_TABLE="$GOMER_BASE/input_files/sequence_file_tables/cerevisiae_chromosome_table_with_mito.jan_2004"
	CACHE_DIR="/home/josh/GOMER_DATA/comp_regression_cache"
	FAST_CACHE_DIR="/dev/shm/josh"
fi 
#---------------------------------------------------------------------
STD_DIR="$BASE_DIR/GOMER/outputs/regression_standards/comp"
OUT_DIR="$BASE_DIR/GOMER/outputs/comp_regress_out"
STDOUT="$OUT_DIR/comp_regress_stdout"
STDERR="$OUT_DIR/comp_regress_stderr"
# #--------------------------------
FAST_CACHE_DIR=""
if [ $FAST_CACHE_DIR ] ; then
	if ! [ -a "$FAST_CACHE_DIR" ] ; then 
        mkdirhier $FAST_CACHE_DIR
	fi
	FAST_CACHE="--fast_cache=""$FAST_CACHE_DIR"
fi
# REGULATED="$BASE_DIR/GOMER/projects/Nrd1/regulated/Nrd_regulated.reg"
CACHE="--cache_directory=""$CACHE_DIR"
GOMER_CMD="$PYTHON $BASE_DIR/GOMER/python_code/gomer.py $CONFIG $CACHE $FAST_CACHE"

#------------------------------------------------------
#------------------------------------------------------
if [ -a "$OUT_DIR" ] ; then 
	rm $OUT_DIR/* ;
else 
	mkdir $OUT_DIR ;
fi
#------------------------------------------------------
#------------------------------------------------------
if [ -a $CACHE_DIR ] ; then 
	rm $CACHE_DIR/* ;
else 
	mkdir $CACHE_DIR ;
fi
#------------------------------------------------------
#------------------------------------------------------




##++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
##==============================================================================
TEST_DIR="$GOMER_BASE/input_files/tests/comp_tests"
# TEST_DIR="$GOMER_BASE/input_files/tests/comp_tests/old/"
# TEST_CODE_DIR="$GOMER_BASE/input_files/tests/coop_tests"
BASE_SEQ="$GOMER_BASE/input_files/tests/coop_tests/base_sequence.fsa"
TEST_CODE_DIR="$GOMER_BASE/input_files/tests/test_code"
## GEN_SEQ="$PYTHON $TEST_CODE_DIR/generate_sequence.py"
GEN_SEQ="$PYTHON $TEST_CODE_DIR/generate_sequence.py --equal_prob_bases"
SEQ_DIR="$OUT_DIR"
##-----------------------------------------------------------------
# $GEN_SEQ $BASE_SEQ $TEST_DIR/template_A $TEST_DIR/comp_a.probs > $SEQ_DIR/seq_A_a
# $GEN_SEQ $BASE_SEQ $TEST_DIR/template_A $TEST_DIR/comp_b.probs > $SEQ_DIR/seq_A_b
# $GEN_SEQ $BASE_SEQ $TEST_DIR/template_A $TEST_DIR/comp_z.probs > $SEQ_DIR/seq_A_z

# $GEN_SEQ $BASE_SEQ $TEST_DIR/template_AB $TEST_DIR/comp_a.probs $TEST_DIR/comp_a.probs > $SEQ_DIR/seq_AB_aa
# $GEN_SEQ $BASE_SEQ $TEST_DIR/template_AB $TEST_DIR/comp_b.probs $TEST_DIR/comp_b.probs > $SEQ_DIR/seq_AB_bb
# $GEN_SEQ $BASE_SEQ $TEST_DIR/template_AB $TEST_DIR/comp_a.probs $TEST_DIR/comp_b.probs > $SEQ_DIR/seq_AB_ab
$GEN_SEQ $BASE_SEQ $TEST_DIR/template_AB $TEST_DIR/comp_a.probs $TEST_DIR/comp_b.probs > $SEQ_DIR/seq_AB_ab
$GEN_SEQ $BASE_SEQ $TEST_DIR/template_AB $TEST_DIR/comp_z.probs $TEST_DIR/comp_b.probs > $SEQ_DIR/seq_AB_zb
$GEN_SEQ $BASE_SEQ $TEST_DIR/template_AB $TEST_DIR/comp_a.probs $TEST_DIR/comp_y.probs > $SEQ_DIR/seq_AB_ay
$GEN_SEQ $BASE_SEQ $TEST_DIR/template_AB $TEST_DIR/comp_z.probs $TEST_DIR/comp_y.probs > $SEQ_DIR/seq_AB_zy


$GEN_SEQ $BASE_SEQ $TEST_DIR/template_AB_multi $TEST_DIR/comp_a.probs $TEST_DIR/comp_b.probs > $SEQ_DIR/seq_AB_ab_multi
# $GEN_SEQ $BASE_SEQ $TEST_DIR/template_AB $TEST_DIR/comp_z.probs $TEST_DIR/comp_b.probs > $SEQ_DIR/seq_AB_zb
# $GEN_SEQ $BASE_SEQ $TEST_DIR/template_AB $TEST_DIR/comp_d.probs $TEST_DIR/comp_b.probs > $SEQ_DIR/seq_AB_db
# $GEN_SEQ $BASE_SEQ $TEST_DIR/template_AB $TEST_DIR/comp_d.probs $TEST_DIR/comp_d.probs > $SEQ_DIR/seq_AB_dd

$GEN_SEQ $BASE_SEQ $TEST_DIR/template_ABC $TEST_DIR/comp_a.probs $TEST_DIR/comp_b.probs $TEST_DIR/comp_g.probs> $SEQ_DIR/seq_ABC_abg
$GEN_SEQ $BASE_SEQ $TEST_DIR/template_ABC $TEST_DIR/comp_a.probs $TEST_DIR/comp_b.probs $TEST_DIR/comp_h.probs> $SEQ_DIR/seq_ABC_abh
$GEN_SEQ $BASE_SEQ $TEST_DIR/template_ABC $TEST_DIR/comp_a.probs $TEST_DIR/comp_g.probs $TEST_DIR/comp_b.probs> $SEQ_DIR/seq_ABC_agb

# $GEN_SEQ $BASE_SEQ $TEST_DIR/template_ABC $TEST_DIR/comp_a.probs $TEST_DIR/comp_b.probs $TEST_DIR/comp_z.probs> $SEQ_DIR/seq_ABC_abz
# $GEN_SEQ $BASE_SEQ $TEST_DIR/template_ABC $TEST_DIR/comp_a.probs $TEST_DIR/comp_b.probs $TEST_DIR/comp_d.probs> $SEQ_DIR/seq_ABC_abd

##-----------------------------------------------------------------
# $GEN_SEQ $BASE_SEQ $TEST_DIR/template_AB $TEST_DIR/comp_e.probs $TEST_DIR/comp_b.probs > $SEQ_DIR/seq_AB_eb
# $GEN_SEQ $BASE_SEQ $TEST_DIR/template_AB $TEST_DIR/comp_e.probs $TEST_DIR/comp_e.probs > $SEQ_DIR/seq_AB_ee

# $GEN_SEQ $BASE_SEQ $TEST_DIR/template_ABC $TEST_DIR/comp_a.probs $TEST_DIR/comp_b.probs $TEST_DIR/comp_e.probs> $SEQ_DIR/seq_ABC_abe
##-----------------------------------------------------------------
##==============================================================================
##==============================================================================
## HOMOTYPIC

# $GOMER_CMD --sequence_feature $SEQ_DIR/seq_A_b $TEST_DIR/comp_b.probs \
# 		 $GOMER_BASE/input_files/regulated_genes/empty.list > $OUT_DIR/seq_A_b__no_comp

# $GOMER_CMD --sequence_feature $SEQ_DIR/seq_A_b $TEST_DIR/comp_b.probs \
# 		 $GOMER_BASE/input_files/regulated_genes/empty.list \
# 		 --comp_kappa=simple_square_comp_model_gomer.py  "max_distance=10" $TEST_DIR/comp_b.probs \
# 		 default > $OUT_DIR/seq_A_b_max10

# $GOMER_CMD --sequence_feature $SEQ_DIR/seq_A_b $TEST_DIR/comp_b.probs \
# 		 $GOMER_BASE/input_files/regulated_genes/empty.list \
# 		 --comp_kappa=simple_square_comp_model_gomer.py  "max_distance=20" $TEST_DIR/comp_b.probs \
# 		 default > $OUT_DIR/seq_A_b_max20

# $GOMER_CMD --sequence_feature $SEQ_DIR/seq_A_b $TEST_DIR/comp_b.probs \
# 		 $GOMER_BASE/input_files/regulated_genes/empty.list \
# 		 --comp_kappa=simple_square_comp_model_gomer.py  "max_distance=5" $TEST_DIR/comp_b.probs \
# 		 default > $OUT_DIR/seq_A_b_max5

# ##--------------------------------------

# $GOMER_CMD --sequence_feature $SEQ_DIR/seq_A_a $TEST_DIR/comp_a.probs \
# 		 $GOMER_BASE/input_files/regulated_genes/empty.list > $OUT_DIR/seq_A_a__no_comp

# $GOMER_CMD --sequence_feature $SEQ_DIR/seq_A_a $TEST_DIR/comp_a.probs \
# 		 $GOMER_BASE/input_files/regulated_genes/empty.list \
# 		 --comp_kappa=simple_square_comp_model_gomer.py  "max_distance=10" $TEST_DIR/comp_a.probs \
# 		 default > $OUT_DIR/seq_A_a_max10

# ##--------------------------------------

# $GOMER_CMD --sequence_feature $SEQ_DIR/seq_A_a $TEST_DIR/comp_z.probs \
# 		 $GOMER_BASE/input_files/regulated_genes/empty.list > $OUT_DIR/seq_A_a_compz_no_comp

# $GOMER_CMD --sequence_feature $SEQ_DIR/seq_A_a $TEST_DIR/comp_z.probs \
# 		 $GOMER_BASE/input_files/regulated_genes/empty.list \
# 		 --comp_kappa=simple_square_comp_model_gomer.py  "max_distance=10" $TEST_DIR/comp_z.probs \
# 		 default > $OUT_DIR/seq_A_a_compz_max10
##==============================================================================
##==============================================================================
## HETEROTYPIC
export GOMER_HOME='/home/josh/GOMER/'
GOMER='/home/josh/GOMER/python_code/gomer.py'

# $GOMER_CMD --sequence_feature $SEQ_DIR/seq_AB_bb $TEST_DIR/comp_b.probs \
# 		 $GOMER_BASE/input_files/regulated_genes/empty.list > $OUT_DIR/seq_AB_bb__no_comp

# $GOMER_CMD --sequence_feature $SEQ_DIR/seq_AB_bb $TEST_DIR/comp_b.probs \
# 		 $GOMER_BASE/input_files/regulated_genes/empty.list \
# 		 --comp_kappa=simple_square_comp_model_gomer.py \
# 		 "max_distance=10" $TEST_DIR/comp_b.probs \
# 		 default > $OUT_DIR/seq_AB_bb_max10
##--------------------------------------
$GOMER_CMD --sequence_feature $SEQ_DIR/seq_AB_ab $TEST_DIR/comp_a.probs \
		$GOMER_BASE/input_files/regulated_genes/empty.list \
		--output $OUT_DIR/seq_AB_ab_compa_NO_COMP 1>> $STDOUT 2>> $STDERR

$GOMER_CMD --sequence_feature $SEQ_DIR/seq_AB_ab $TEST_DIR/comp_a.probs \
		 $GOMER_BASE/input_files/regulated_genes/empty.list \
		 --comp_kappa=simple_square_comp_model_gomer.py \
		 "max_distance=10" $TEST_DIR/comp_b.probs \
		 default --output $OUT_DIR/seq_AB_ab_compa_compb_max10 1>> $STDOUT 2>> $STDERR

$GOMER_CMD --sequence_feature $SEQ_DIR/seq_AB_ab $TEST_DIR/comp_a.probs \
		 $GOMER_BASE/input_files/regulated_genes/empty.list \
		 --comp_kappa=simple_square_comp_model_gomer.py \
		 "max_distance=10" $TEST_DIR/comp_b.probs \
		 1.0 --output $OUT_DIR/seq_AB_ab_compa_compb_max10_conc1_0 1>> $STDOUT 2>> $STDERR

$GOMER_CMD --sequence_feature $SEQ_DIR/seq_AB_ab $TEST_DIR/comp_a.probs \
		 $GOMER_BASE/input_files/regulated_genes/empty.list \
		 --comp_kappa=simple_square_comp_model_gomer.py \
		 "max_distance=10" $TEST_DIR/comp_b.probs \
		 2.0 --output $OUT_DIR/seq_AB_ab_compa_compb_max10_conc2_0 1>> $STDOUT 2>> $STDERR

$GOMER_CMD --sequence_feature $SEQ_DIR/seq_AB_ab $TEST_DIR/comp_a.probs \
		 $GOMER_BASE/input_files/regulated_genes/empty.list \
		 --comp_kappa=simple_square_comp_model_gomer.py \
		 "max_distance=10" $TEST_DIR/comp_b.probs \
		 4.0 --output $OUT_DIR/seq_AB_ab_compa_compb_max10_conc4_0 1>> $STDOUT 2>> $STDERR

$GOMER_CMD --sequence_feature $SEQ_DIR/seq_AB_ab $TEST_DIR/comp_a.probs \
		 $GOMER_BASE/input_files/regulated_genes/empty.list \
		 --comp_kappa=simple_square_comp_model_gomer.py \
		 "max_distance=10" $TEST_DIR/comp_b.probs \
		 0.5 --output $OUT_DIR/seq_AB_ab_compa_compb_max10_conc0_5 1>> $STDOUT 2>> $STDERR

$GOMER_CMD --sequence_feature $SEQ_DIR/seq_AB_ab $TEST_DIR/comp_a.probs \
		 $GOMER_BASE/input_files/regulated_genes/empty.list \
		 --comp_kappa=simple_square_comp_model_gomer.py \
		 "max_distance=10" $TEST_DIR/comp_b.probs \
		 0.25 --output $OUT_DIR/seq_AB_ab_compa_compb_max10_conc0_25 1>> $STDOUT 2>> $STDERR

$GOMER_CMD --sequence_feature $SEQ_DIR/seq_AB_ab $TEST_DIR/comp_a.probs \
		 $GOMER_BASE/input_files/regulated_genes/empty.list \
		 --comp_kappa=simple_square_comp_model_gomer.py \
		 "max_distance=9" $TEST_DIR/comp_b.probs \
		 1.0 --output $OUT_DIR/seq_AB_ab_compa_compb_max9 1>> $STDOUT 2>> $STDERR

$GOMER_CMD --sequence_feature $SEQ_DIR/seq_AB_ab $TEST_DIR/comp_a.probs \
		 $GOMER_BASE/input_files/regulated_genes/empty.list \
		 --free_conc_ratio=2.0 \
		 --comp_kappa=simple_square_comp_model_gomer.py \
		 "max_distance=10" $TEST_DIR/comp_b.probs \
		 default --output $OUT_DIR/seq_AB_ab_compa2_0_compb_max10 1>> $STDOUT 2>> $STDERR


##------------------------------------------------------------------------------
$GOMER_CMD --sequence_feature $SEQ_DIR/seq_AB_zb $TEST_DIR/comp_a.probs \
		 $GOMER_BASE/input_files/regulated_genes/empty.list \
		 --comp_kappa=simple_square_comp_model_gomer.py \
		 "max_distance=10" $TEST_DIR/comp_b.probs \
		 default --output $OUT_DIR/seq_AB_zb_compa_compb 1>> $STDOUT 2>> $STDERR

$GOMER_CMD --sequence_feature $SEQ_DIR/seq_AB_ay $TEST_DIR/comp_a.probs \
		 $GOMER_BASE/input_files/regulated_genes/empty.list \
		 --comp_kappa=simple_square_comp_model_gomer.py \
		 "max_distance=10" $TEST_DIR/comp_b.probs \
		 default --output $OUT_DIR/seq_AB_ay_compa_compb 1>> $STDOUT 2>> $STDERR

$GOMER_CMD --sequence_feature $SEQ_DIR/seq_AB_zy $TEST_DIR/comp_a.probs \
		 $GOMER_BASE/input_files/regulated_genes/empty.list \
		 --comp_kappa=simple_square_comp_model_gomer.py \
		 "max_distance=10" $TEST_DIR/comp_b.probs \
		 default --output $OUT_DIR/seq_AB_zy_compa_compb 1>> $STDOUT 2>> $STDERR
##------------------------------------------------------------------------------
$GOMER_CMD --sequence_feature $SEQ_DIR/seq_AB_ab_multi $TEST_DIR/comp_a.probs \
		 $GOMER_BASE/input_files/regulated_genes/empty.list \
		 --comp_kappa=simple_square_comp_model_gomer.py \
		 "max_distance=10" $TEST_DIR/comp_b.probs \
		 default --output $OUT_DIR/seq_AB_ab_multi__compa_compb 1>> $STDOUT 2>> $STDERR
##------------------------------------------------------------------------------
$GOMER_CMD --sequence_feature $SEQ_DIR/seq_ABC_abg $TEST_DIR/comp_a.probs \
		 $GOMER_BASE/input_files/regulated_genes/empty.list \
		 --comp_kappa=simple_square_comp_model_gomer.py \
		 "max_distance=10" $TEST_DIR/comp_b.probs default \
		 --comp_kappa=simple_square_comp_model_gomer.py \
		 "max_distance=10" $TEST_DIR/comp_g.probs default \
		 --output $OUT_DIR/seq_ABC_abg_compa_compb_compg 1>> $STDOUT 2>> $STDERR

$GOMER_CMD --sequence_feature $SEQ_DIR/seq_ABC_abh $TEST_DIR/comp_a.probs \
		 $GOMER_BASE/input_files/regulated_genes/empty.list \
		 --comp_kappa=simple_square_comp_model_gomer.py \
		 "max_distance=10" $TEST_DIR/comp_b.probs default \
		 --comp_kappa=simple_square_comp_model_gomer.py \
		 "max_distance=10" $TEST_DIR/comp_h.probs default \
		 --output $OUT_DIR/seq_ABC_abh_compa_compb_comph 1>> $STDOUT 2>> $STDERR

$GOMER_CMD --sequence_feature $SEQ_DIR/seq_ABC_agb $TEST_DIR/comp_a.probs \
		 $GOMER_BASE/input_files/regulated_genes/empty.list \
		 --comp_kappa=simple_square_comp_model_gomer.py \
		 "max_distance=10" $TEST_DIR/comp_g.probs default \
		 --comp_kappa=simple_square_comp_model_gomer.py \
		 "max_distance=10" $TEST_DIR/comp_b.probs default \
		 --output $OUT_DIR/seq_ABC_agb_compa_compg_compb 1>> $STDOUT 2>> $STDERR

##------------------------------------------------------------------------------
$GOMER_CMD --sequence_feature $SEQ_DIR/seq_ABC_abg $TEST_DIR/comp_a.probs \
		 $GOMER_BASE/input_files/regulated_genes/empty.list \
		 --comp_kappa=simple_square_comp_model_gomer.py \
		 "max_distance=10" $TEST_DIR/comp_b.probs default \
		 --comp_kappa=simple_square_comp_model_gomer.py \
		 "max_distance=10" $TEST_DIR/comp_g.probs 2.0 \
		 --output $OUT_DIR/seq_ABC_abg_compa_compb_compg_2_0 1>> $STDOUT 2>> $STDERR

$GOMER_CMD --sequence_feature $SEQ_DIR/seq_ABC_abg $TEST_DIR/comp_a.probs \
		 $GOMER_BASE/input_files/regulated_genes/empty.list \
		 --comp_kappa=simple_square_comp_model_gomer.py \
		 "max_distance=10" $TEST_DIR/comp_b.probs 2.0 \
		 --comp_kappa=simple_square_comp_model_gomer.py \
		 "max_distance=10" $TEST_DIR/comp_g.probs default \
		 --output $OUT_DIR/seq_ABC_abg_compa_compb2_0_compg 1>> $STDOUT 2>> $STDERR

$GOMER_CMD --sequence_feature $SEQ_DIR/seq_ABC_abg $TEST_DIR/comp_a.probs \
		 $GOMER_BASE/input_files/regulated_genes/empty.list \
		 --comp_kappa=simple_square_comp_model_gomer.py \
		 "max_distance=10" $TEST_DIR/comp_b.probs 2.0 \
		 --comp_kappa=simple_square_comp_model_gomer.py \
		 "max_distance=10" $TEST_DIR/comp_g.probs 2.0 \
		 --output $OUT_DIR/seq_ABC_abg_compa_compb2_0_compg2_0 1>> $STDOUT 2>> $STDERR

$GOMER_CMD --sequence_feature $SEQ_DIR/seq_ABC_abg $TEST_DIR/comp_a.probs \
		 $GOMER_BASE/input_files/regulated_genes/empty.list \
		 --free_conc_ratio=2.0 \
		 --comp_kappa=simple_square_comp_model_gomer.py \
		 "max_distance=10" $TEST_DIR/comp_b.probs default \
		 --comp_kappa=simple_square_comp_model_gomer.py \
		 "max_distance=10" $TEST_DIR/comp_g.probs default \
		 --output $OUT_DIR/seq_ABC_abg_compa2_0_compb_compg 1>> $STDOUT 2>> $STDERR
##==============================================================================
##==============================================================================
P_CONC="1"
C_CONC="1"
$GOMER_CMD --sequence_feature $SEQ_DIR/seq_ABC_abg $TEST_DIR/comp_b.probs \
	$GOMER_BASE/input_files/regulated_genes/empty.list \
	--free_conc $P_CONC \
	--comp_kappa=simple_square_comp_model_gomer.py  "max_distance=10" $TEST_DIR/comp_b.probs $C_CONC \
	--output $OUT_DIR/seq_ABC_abg_10_pconc${P_CONC}_cconc${C_CONC}   1>> $STDOUT 2>> $STDERR

P_CONC="5"
C_CONC="1"
$GOMER_CMD --sequence_feature $SEQ_DIR/seq_ABC_abg $TEST_DIR/comp_b.probs \
	$GOMER_BASE/input_files/regulated_genes/empty.list \
	--free_conc $P_CONC \
	--comp_kappa=simple_square_comp_model_gomer.py  "max_distance=10" $TEST_DIR/comp_b.probs $C_CONC \
	--output $OUT_DIR/seq_ABC_abg_10_pconc${P_CONC}_cconc${C_CONC}   1>> $STDOUT 2>> $STDERR

P_CONC="1"
C_CONC="5"
$GOMER_CMD --sequence_feature $SEQ_DIR/seq_ABC_abg $TEST_DIR/comp_b.probs \
	$GOMER_BASE/input_files/regulated_genes/empty.list \
	--free_conc $P_CONC \
	--comp_kappa=simple_square_comp_model_gomer.py  "max_distance=10" $TEST_DIR/comp_b.probs $C_CONC \
	--output $OUT_DIR/seq_ABC_abg_10_pconc${P_CONC}_cconc${C_CONC}   1>> $STDOUT 2>> $STDERR

P_CONC="1"
C_CONC="1"
$GOMER_CMD --sequence_feature $SEQ_DIR/seq_ABC_abg $TEST_DIR/comp_b.probs \
	$GOMER_BASE/input_files/regulated_genes/empty.list \
	--free_conc $P_CONC \
	--comp_kappa=simple_square_comp_model_gomer.py  "max_distance=10" $TEST_DIR/comp_b.probs $C_CONC \
	--output $OUT_DIR/seq_ABC_abg_10_pconc${P_CONC}_cconc${C_CONC}   1>> $STDOUT 2>> $STDERR

P_CONC="2"
C_CONC="5"
$GOMER_CMD --sequence_feature $SEQ_DIR/seq_ABC_abg $TEST_DIR/comp_b.probs \
	$GOMER_BASE/input_files/regulated_genes/empty.list \
	--free_conc $P_CONC \
	--comp_kappa=simple_square_comp_model_gomer.py  "max_distance=10" $TEST_DIR/comp_b.probs $C_CONC \
 	--output $OUT_DIR/seq_ABC_abg_10_pconc${P_CONC}_cconc${C_CONC}   1>> $STDOUT 2>> $STDERR

P_CONC="5"
C_CONC="2"
$GOMER_CMD --sequence_feature $SEQ_DIR/seq_ABC_abg $TEST_DIR/comp_b.probs \
	$GOMER_BASE/input_files/regulated_genes/empty.list \
	--free_conc $P_CONC \
	--comp_kappa=simple_square_comp_model_gomer.py  "max_distance=10" $TEST_DIR/comp_b.probs $C_CONC \
 	--output $OUT_DIR/seq_ABC_abg_10_pconc${P_CONC}_cconc${C_CONC}   1>> $STDOUT 2>> $STDERR

P_CONC="None"
C_CONC="5"
$GOMER_CMD --sequence_feature $SEQ_DIR/seq_ABC_abg $TEST_DIR/comp_b.probs \
	$GOMER_BASE/input_files/regulated_genes/empty.list \
	--comp_kappa=simple_square_comp_model_gomer.py  "max_distance=10" $TEST_DIR/comp_b.probs $C_CONC \
 	--output $OUT_DIR/seq_ABC_abg_10_pconc${P_CONC}_cconc${C_CONC}   1>> $STDOUT 2>> $STDERR
##==============================================================================
##==============================================================================
## TEST MULTIPLE MATRICES
$GOMER_CMD --sequence_feature $SEQ_DIR/seq_ABC_abg $TEST_DIR/comp_a.probs \
	$GOMER_BASE/input_files/regulated_genes/empty.list \
	--comp_kappa=simple_square_comp_model_gomer.py "max_distance=10" $TEST_DIR/comp_b.probs  0.3    \
	--comp_kappa=simple_square_comp_model_gomer.py "max_distance=13" $TEST_DIR/comp_g.probs  0.0781 \
	--comp_kappa=simple_square_comp_model_gomer.py "max_distance=11" $TEST_DIR/comp_h.probs  552    \
	--comp_kappa=simple_square_comp_model_gomer.py "max_distance=17" $TEST_DIR/random1.probs 0.79   \
	--comp_kappa=simple_square_comp_model_gomer.py "max_distance=16" $TEST_DIR/random2.probs 1.63   \
	--comp_kappa=simple_square_comp_model_gomer.py "max_distance=15" $TEST_DIR/random3.probs 30.7   \
	--comp_kappa=simple_square_comp_model_gomer.py "max_distance=14" $TEST_DIR/random4.probs 0.58   \
	--output $OUT_DIR/comp_random_probs_1   1>> $STDOUT 2>> $STDERR
##==============================================================================
$GOMER_CMD --sequence_feature $SEQ_DIR/seq_ABC_abg $TEST_DIR/comp_a.probs \
	$GOMER_BASE/input_files/regulated_genes/empty.list \
	--comp_kappa=simple_square_comp_model_gomer.py "max_distance=10" $TEST_DIR/comp_b.probs  0.84   \
	--comp_kappa=simple_square_comp_model_gomer.py "max_distance=13" $TEST_DIR/comp_g.probs  0.84   \
	--comp_kappa=simple_square_comp_model_gomer.py "max_distance=11" $TEST_DIR/comp_h.probs  2.09   \
	--comp_kappa=simple_square_comp_model_gomer.py "max_distance=17" $TEST_DIR/random1.probs 0.914  \
	--comp_kappa=simple_square_comp_model_gomer.py "max_distance=16" $TEST_DIR/random2.probs 9.9    \
	--comp_kappa=simple_square_comp_model_gomer.py "max_distance=15" $TEST_DIR/random3.probs 9.0    \
	--comp_kappa=simple_square_comp_model_gomer.py "max_distance=14" $TEST_DIR/random4.probs 0.058  \
	--output $OUT_DIR/comp_random_probs_2   1>> $STDOUT 2>> $STDERR
##==============================================================================
$GOMER_CMD --sequence_feature $SEQ_DIR/seq_ABC_abg $TEST_DIR/comp_a.probs \
	$GOMER_BASE/input_files/regulated_genes/empty.list \
	--comp_kappa=simple_square_comp_model_gomer.py "max_distance=18" $TEST_DIR/comp_b.probs  0.84   \
	--comp_kappa=simple_square_comp_model_gomer.py "max_distance=17" $TEST_DIR/comp_g.probs  0.84   \
	--comp_kappa=simple_square_comp_model_gomer.py "max_distance=16" $TEST_DIR/comp_h.probs  2.09   \
	--comp_kappa=simple_square_comp_model_gomer.py "max_distance=15" $TEST_DIR/random1.probs 0.914  \
	--comp_kappa=simple_square_comp_model_gomer.py "max_distance=14" $TEST_DIR/random2.probs 9.9    \
	--comp_kappa=simple_square_comp_model_gomer.py "max_distance=13" $TEST_DIR/random3.probs 9.0    \
	--comp_kappa=simple_square_comp_model_gomer.py "max_distance=12" $TEST_DIR/random4.probs 0.058  \
	--output $OUT_DIR/comp_random_probs_3   1>> $STDOUT 2>> $STDERR
##==============================================================================
##==============================================================================
$GOMER_CMD --sequence_feature $SEQ_DIR/seq_ABC_abg $TEST_DIR/comp_a.probs \
	$GOMER_BASE/input_files/regulated_genes/empty.list \
	--comp_kappa=simple_square_comp_model_gomer.py "max_distance=10" $TEST_DIR/comp_b.probs  0.3    \
	--comp_kappa=simple_square_comp_model_gomer.py "max_distance=13" $TEST_DIR/comp_g.probs  0.0781 \
	--comp_kappa=simple_square_comp_model_gomer.py "max_distance=11" $TEST_DIR/comp_h.probs  552    \
	--coop_kappa=simple_square_coop_model_gomer.py "min_distance=6;max_distance=27" $TEST_DIR/random1.probs 0.79 86.6    \
	--coop_kappa=simple_square_coop_model_gomer.py "min_distance=5;max_distance=29" $TEST_DIR/random2.probs 1.63 88.3     \
	--coop_kappa=simple_square_coop_model_gomer.py "min_distance=7;max_distance=25" $TEST_DIR/random3.probs 30.7 680 \
	--coop_kappa=simple_square_coop_model_gomer.py "min_distance=8;max_distance=23" $TEST_DIR/random4.probs 0.58 483\
	--output $OUT_DIR/comp_coop_random_probs_1   1>> $STDOUT 2>> $STDERR
##==============================================================================
$GOMER_CMD --sequence_feature $SEQ_DIR/seq_ABC_abg $TEST_DIR/comp_a.probs \
	$GOMER_BASE/input_files/regulated_genes/empty.list \
	--comp_kappa=simple_square_comp_model_gomer.py "max_distance=10" $TEST_DIR/comp_b.probs  0.3    \
	--comp_kappa=simple_square_comp_model_gomer.py "max_distance=13" $TEST_DIR/comp_g.probs  0.0781 \
	--comp_kappa=simple_square_comp_model_gomer.py "max_distance=11" $TEST_DIR/comp_h.probs  552    \
	--coop_kappa=simple_square_coop_model_gomer.py "min_distance=5;max_distance=29" $TEST_DIR/random1.probs 0.914 0.655  \
	--coop_kappa=simple_square_coop_model_gomer.py "min_distance=7;max_distance=25" $TEST_DIR/random2.probs 9.9 1.67     \
	--coop_kappa=simple_square_coop_model_gomer.py "min_distance=6;max_distance=27" $TEST_DIR/random3.probs 9.0 0.982    \
	--coop_kappa=simple_square_coop_model_gomer.py "min_distance=8;max_distance=23" $TEST_DIR/random4.probs 0.058 0.145  \
	--output $OUT_DIR/comp_coop_random_probs_2   1>> $STDOUT 2>> $STDERR
##==============================================================================
$GOMER_CMD --sequence_feature $SEQ_DIR/seq_ABC_abg $TEST_DIR/comp_a.probs \
	$GOMER_BASE/input_files/regulated_genes/empty.list \
	--comp_kappa=simple_square_comp_model_gomer.py "max_distance=10" $TEST_DIR/comp_b.probs  0.3    \
	--comp_kappa=simple_square_comp_model_gomer.py "max_distance=13" $TEST_DIR/comp_g.probs  0.0781 \
	--comp_kappa=simple_square_comp_model_gomer.py "max_distance=11" $TEST_DIR/comp_h.probs  552    \
	--coop_kappa=simple_square_coop_model_gomer.py "min_distance=5;max_distance=32" $TEST_DIR/random1.probs 0.914 0.655  \
	--coop_kappa=simple_square_coop_model_gomer.py "min_distance=1;max_distance=25" $TEST_DIR/random2.probs 9.9 1.67     \
	--coop_kappa=simple_square_coop_model_gomer.py "min_distance=6;max_distance=24" $TEST_DIR/random3.probs 9.0 0.982    \
	--coop_kappa=simple_square_coop_model_gomer.py "min_distance=10;max_distance=23" $TEST_DIR/random4.probs 0.058 0.145  \
	--output $OUT_DIR/comp_coop_random_probs_3   1>> $STDOUT 2>> $STDERR
##==============================================================================
##==============================================================================
$GOMER_CMD --sequence_feature $SEQ_DIR/seq_ABC_abg $TEST_DIR/comp_a.probs \
	$GOMER_BASE/input_files/regulated_genes/empty.list \
	--comp_kappa=simple_square_comp_model_gomer.py "max_distance=10" $TEST_DIR/comp_b.probs  0.3    \
	--comp_kappa=simple_square_comp_model_gomer.py "max_distance=13" $TEST_DIR/comp_g.probs  0.0781 \
	--comp_kappa=simple_square_comp_model_gomer.py "max_distance=11" $TEST_DIR/comp_h.probs  552    \
	--output $OUT_DIR/comp_nocoop_random_probs_1   1>> $STDOUT 2>> $STDERR
##==============================================================================

#--------------------------------
#--------------------------------
PROJECT_BASE="$TEST_DIR/Sum1_Ndt80"
MATRIX_DIR="$PROJECT_BASE/binding_matrices"
REGULATED_DIR="$PROJECT_BASE/regulated"
#--------------------------------
SUM1_MATRIX="$MATRIX_DIR/sum1_bound_matrix.probs"
NDT80_MATRIX="$MATRIX_DIR/ndt80_bound_matrix.probs"
#--------------------------------
SUM1_REGULATED="$REGULATED_DIR/Sum1_regulated.reg"
NDT80_REGULATED="$REGULATED_DIR/3x_Ndt80_Induction.reg"
SUM1_NDT80_REGULATED="$REGULATED_DIR/Sum1_Ndt80_intersection.reg"
#------------------------------------------------------
#------------------------------------------------------#------------------------------------------------------
#------------------------------------------------------
$GOMER_CMD  $SEQ_TABLE $SUM1_MATRIX $SUM1_REGULATED  --output $OUT_DIR/Sum1_nocomp 1>> $STDOUT 2>> $STDERR
$GOMER_CMD  $SEQ_TABLE $NDT80_MATRIX $NDT80_REGULATED  --output $OUT_DIR/Ndt80_nocomp 1>> $STDOUT 2>> $STDERR
#------------------------------------------------------
#------------------------------------------------------
#-----------
P_CONC="1.0"
C_CONC="1.0"
#-----------
$GOMER_CMD $SEQ_TABLE $NDT80_MATRIX $SUM1_NDT80_REGULATED --comp_kappa=simple_square_comp_model_gomer.py "max_distance=1" $SUM1_MATRIX $C_CONC --free_conc_ratio="$P_CONC" --output "$OUT_DIR"/Ndt80_"$P_CONC"__comp_Sum1_"$C_CONC"  1>> $STDOUT 2>> $STDERR
#-------------------------------------------------------------------------------



# $GOMER_CMD --sequence_feature $SEQ_DIR/seq_AB_ab $TEST_DIR/comp_b.probs \
# 		 $GOMER_BASE/input_files/regulated_genes/empty.list \
# 		 --comp_kappa=simple_square_comp_model_gomer.py \
# 		 "max_distance=10" $TEST_DIR/comp_b.probs \
# 		 1.0 > $OUT_DIR/seq_AB_ab_compb_compb_max10


# $GOMER_CMD --sequence_feature $SEQ_DIR/seq_AB_ab $TEST_DIR/comp_b.probs \
# 		 $GOMER_BASE/input_files/regulated_genes/empty.list \
# 		 --comp_kappa=simple_square_comp_model_gomer.py \
# 		 "max_distance=10" $TEST_DIR/comp_a.probs \
# 		 default > $OUT_DIR/seq_AB_ab_max10

# $GOMER_CMD --sequence_feature $SEQ_DIR/seq_AB_ab $TEST_DIR/comp_b.probs \
# 		 $GOMER_BASE/input_files/regulated_genes/empty.list \
# 		 --comp_kappa=simple_square_comp_model_gomer.py \
# 		 "max_distance=20" $TEST_DIR/comp_a.probs \
# 		 default > $OUT_DIR/seq_AB_ab_max20

# $GOMER_CMD --sequence_feature $SEQ_DIR/seq_AB_zb $TEST_DIR/comp_b.probs \
# 		 $GOMER_BASE/input_files/regulated_genes/empty.list \
# 		 --comp_kappa=simple_square_comp_model_gomer.py \
# 		 "max_distance=10" $TEST_DIR/comp_a.probs \
# 		 default > $OUT_DIR/seq_AB_zb_compb_compa_max10

# $GOMER_CMD --sequence_feature $SEQ_DIR/seq_AB_db $TEST_DIR/comp_b.probs \
# 		 $GOMER_BASE/input_files/regulated_genes/empty.list \
# 		 --comp_kappa=simple_square_comp_model_gomer.py \
# 		 "max_distance=10" $TEST_DIR/comp_a.probs \
# 		 default > $OUT_DIR/seq_AB_db_compb_compa_max10

# $GOMER_CMD --sequence_feature $SEQ_DIR/seq_AB_ab $TEST_DIR/comp_b.probs \
# 		 $GOMER_BASE/input_files/regulated_genes/empty.list \
# 		 --comp_kappa=simple_square_comp_model_gomer.py \
# 		 "max_distance=10" $TEST_DIR/comp_d.probs \
# 		 default > $OUT_DIR/seq_AB_ab_compb_compd_max10

# $GOMER_CMD --sequence_feature $SEQ_DIR/seq_AB_db $TEST_DIR/comp_b.probs \
# 		 $GOMER_BASE/input_files/regulated_genes/empty.list \
# 		 --comp_kappa=simple_square_comp_model_gomer.py \
# 		 "max_distance=10" $TEST_DIR/comp_d.probs \
# 		 default > $OUT_DIR/seq_AB_db_max10

# ##-----------------------------------------------------------------
# $GOMER_CMD --sequence_feature $SEQ_DIR/seq_AB_dd $TEST_DIR/comp_d.probs \
# 		 $GOMER_BASE/input_files/regulated_genes/empty.list > $OUT_DIR/seq_AB_dd__no_comp
# ##-----------------------------------------------------------------
# ##-----------------------------------------------------------------
# $GOMER_CMD --sequence_feature $SEQ_DIR/seq_AB_ee $TEST_DIR/comp_e.probs \
# 		 $GOMER_BASE/input_files/regulated_genes/empty.list > $OUT_DIR/seq_AB_ee__no_comp

# $GOMER_CMD --sequence_feature $SEQ_DIR/seq_AB_eb $TEST_DIR/comp_b.probs \
# 		 $GOMER_BASE/input_files/regulated_genes/empty.list \
# 		 --comp_kappa=simple_square_comp_model_gomer.py \
# 		 "max_distance=10" $TEST_DIR/comp_a.probs \
# 		 default > $OUT_DIR/seq_AB_eb_compb_compa_max10

# $GOMER_CMD --sequence_feature $SEQ_DIR/seq_AB_ab $TEST_DIR/comp_b.probs \
# 		 $GOMER_BASE/input_files/regulated_genes/empty.list \
# 		 --comp_kappa=simple_square_comp_model_gomer.py \
# 		 "max_distance=10" $TEST_DIR/comp_e.probs \
# 		 default > $OUT_DIR/seq_AB_eb_compb_compe_max10
##-----------------------------------------------------------------
##-----------------------------------------------------------------
##==============================================================================
##==============================================================================
## HETEROTYPIC TWO SECONDARIES
export GOMER_HOME='/home/josh/GOMER/'
GOMER='/home/josh/GOMER/python_code/gomer.py'

# $GEN_SEQ $BASE_SEQ $TEST_DIR/template_ABC $TEST_DIR/comp_a.probs $TEST_DIR/comp_b.probs $TEST_DIR/comp_z.probs> $SEQ_DIR/seq_ABC_abz
# $GEN_SEQ $BASE_SEQ $TEST_DIR/template_ABC $TEST_DIR/comp_a.probs $TEST_DIR/comp_b.probs $TEST_DIR/comp_d.probs> $SEQ_DIR/seq_ABC_abd

# $GOMER_CMD --sequence_feature $SEQ_DIR/seq_ABC_abz $TEST_DIR/comp_a.probs \
# 		 $GOMER_BASE/input_files/regulated_genes/empty.list \
# 		 --comp_kappa=simple_square_comp_model_gomer.py \
# 		 "max_distance=10" $TEST_DIR/comp_b.probs \
# 		 default > $OUT_DIR/seq_ABC_abz__a_b_max10

# $GOMER_CMD --sequence_feature $SEQ_DIR/seq_ABC_abd $TEST_DIR/comp_a.probs \
# 		 $GOMER_BASE/input_files/regulated_genes/empty.list \
# 		 --comp_kappa=simple_square_comp_model_gomer.py \
# 		 "max_distance=10" $TEST_DIR/comp_b.probs \
# 		 default > $OUT_DIR/seq_ABC_abd__a_b_max10

# $GOMER_CMD --sequence_feature $SEQ_DIR/seq_ABC_abd $TEST_DIR/comp_a.probs \
# 		 $GOMER_BASE/input_files/regulated_genes/empty.list \
# 		 --comp_kappa=simple_square_comp_model_gomer.py \
# 		 "max_distance=10" $TEST_DIR/comp_b.probs \
# 		 default								\
# 		 \
# 		 --comp_kappa=simple_square_comp_model_gomer.py \
# 		 "max_distance=10" $TEST_DIR/comp_d.probs \
# 		 default > $OUT_DIR/seq_ABC_abd__a_b_d_max10


# $GOMER_CMD --sequence_feature $SEQ_DIR/seq_ABC_abd $TEST_DIR/comp_a.probs \
# 		 $GOMER_BASE/input_files/regulated_genes/empty.list \
# 		 --comp_kappa=simple_square_comp_model_gomer.py \
# 		 "max_distance=40" $TEST_DIR/comp_b.probs \
# 		 default							   \
# 		 \
# 		 --comp_kappa=simple_square_comp_model_gomer.py \
# 		 "max_distance=40" $TEST_DIR/comp_d.probs \
# 		 default > $OUT_DIR/seq_ABC_abd__a_b_d_max40
##==============================================================================
##++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
## rm -f $CACHE_DIR/*
#------------------------------------------------------
rm -f $OUT_DIR/comp_regress_diff
#------------------------------------------------------
for FILENAME in                     \
	"seq_ABC_abg_compa2_0_compb_compg"		 \
	"seq_ABC_abg_compa_compb2_0_compg"		 \
	"seq_ABC_abg_compa_compb2_0_compg2_0"	 \
	"seq_ABC_abg_compa_compb_compg"			 \
	"seq_ABC_abg_compa_compb_compg_2_0"		 \
	"seq_ABC_abh_compa_compb_comph"			 \
	"seq_ABC_agb_compa_compg_compb"			 \
	"seq_AB_ab_compa2_0_compb_max10"		 \
	"seq_AB_ab_compa_NO_COMP"				 \
	"seq_AB_ab_compa_compb_max10"			 \
	"seq_AB_ab_compa_compb_max10_conc0_25"	 \
	"seq_AB_ab_compa_compb_max10_conc0_5"	 \
	"seq_AB_ab_compa_compb_max10_conc1_0"	 \
	"seq_AB_ab_compa_compb_max10_conc2_0"	 \
	"seq_AB_ab_compa_compb_max10_conc4_0"	 \
	"seq_AB_ab_compa_compb_max9"			 \
	"seq_AB_ab_multi"						 \
	"seq_AB_ab_multi__compa_compb"			 \
	"seq_AB_ay_compa_compb"					 \
	"seq_AB_zb_compa_compb"					 \
	"seq_AB_zy_compa_compb"					 \
	"seq_ABC_abg_10_pconc5_cconc1"			 \
	"seq_ABC_abg_10_pconc1_cconc5"			 \
	"seq_ABC_abg_10_pconc1_cconc1"			 \
	"seq_ABC_abg_10_pconc2_cconc5"			 \
	"seq_ABC_abg_10_pconc5_cconc2"			 \
	"seq_ABC_abg_10_pconcNone_cconc5"		 \
	"comp_random_probs_1"					 \
	"comp_random_probs_3"					 \
	"comp_random_probs_2"					 \
	"comp_coop_random_probs_1"               \
	"comp_coop_random_probs_2"               \
	"comp_coop_random_probs_3"               \
	"comp_nocoop_random_probs_1"             \
	"Ndt80_nocomp"							 \
	"Sum1_nocomp"							 \
	"Ndt80_1.0__comp_Sum1_1.0"               \
	"comp_regress_stderr"                    \
	"comp_regress_stdout"                    \
	"seq_ABC_abg"							 \
	"seq_ABC_abh"							 \
	"seq_ABC_agb"							 \
	"seq_AB_ab"								 \
	"seq_AB_ay"								 \
	"seq_AB_zb"								 \
	"seq_AB_zy"								 \
 	;
 	do
 	STD_FILE="$STD_DIR"/STD_"$FILENAME"
 	NEW_FILE="$OUT_DIR"/"$FILENAME"
 	# echo "STD_FILE: $STD_FILE"
 	# echo "NEW_FILE: $NEW_FILE"
 	echo "--------------------------------------------" >> $OUT_DIR/comp_regress_diff ;
 	COMMAND="diff -s $STD_FILE $NEW_FILE"
 	echo $COMMAND >> $OUT_DIR/comp_regress_diff ;
 	$COMMAND >> $OUT_DIR/comp_regress_diff ;
done

if [ -a "$FAST_CACHE_DIR" ] ; then 
	rmdir $FAST_CACHE_DIR
fi

if [ -a $CACHE_DIR ] ; then 
	rm $CACHE_DIR/*
	rmdir $CACHE_DIR
fi
