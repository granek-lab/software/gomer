#! /usr/bin/env python

"""PATH STRING REPLACEMENT

Recurses through directory tree rooted at PATH, and replaces all occurences
of STRING with REPLACEMENT in each file in the tree.
"""


import re, os, stat

def fix_file (file_name, old_string, replacement_string) :
	original_stat = os.stat(file_name)
	old_string_re = re.compile (old_string)
	file_handle = open(file_name, 'r+')
	changed = 0
	lines_of_file = file_handle.readlines()
	for line_num in range(len(lines_of_file)):
		cur_line = lines_of_file[line_num]
		if (old_string_re.search(cur_line)) :
			changed = 1
			# print cur_line.rstrip()
			lines_of_file[line_num] = re.sub(old_string, replacement_string, cur_line)
			# print fixed_line
			# file_handle.write(fixed_line)
	if changed :
		file_handle.seek(0)
		file_handle.writelines(lines_of_file)
		file_handle.truncate()
	file_handle.close
	os.utime(file_name, (original_stat[stat.ST_ATIME], original_stat[stat.ST_MTIME]))

def fix_list_of_files (arg, dirname, file_list):
	old_string = arg[0]
	replacement_string = arg[1]
	for name in file_list :
		full_name = os.path.join(dirname, name)
		if os.path.isfile(full_name) :
			fix_file (full_name, old_string, replacement_string)



if __name__ == '__main__':
	import getopt, sys, os
	def usage() :
		print >>sys.stderr, 'usage: ', sys.argv[0], __doc__

	if len(sys.argv) == 4:
		directory_to_walk = sys.argv[1]
		old_string = sys.argv[2]
		replacement_string = sys.argv[3]
		
	else:
		usage()
		sys.exit(0)

	os.path.walk(directory_to_walk, fix_list_of_files, (old_string, replacement_string))

