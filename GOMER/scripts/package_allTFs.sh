GOMER_BASE="/home/josh/"
GOMER_HOME="$GOMER_BASE/GOMER"

ALL_TF_DIR="$GOMER_HOME/projects/all_TFs"
TARZIPPED_ALL_TF_DIR="$GOMER_HOME/projects/all_TFs.tar.gz"
TARFILE="/tmp/all_TFs.tgz"

if [ -a "$TARFILE" ] ; then 
	rm $TARFILE ; 
fi

if [ -a "$TARZIPPED_ALL_TF_DIR" ] ; then 
	cp $TARZIPPED_ALL_TF_DIR $TARFILE ;
else 
	cd $ALL_TF_DIR ;
	tar --dereference -zcf $TARFILE * ;
fi
