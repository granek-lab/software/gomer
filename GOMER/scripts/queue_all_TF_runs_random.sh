# INPUT_DIR="projects/young_all_TFs/random40/clusterGOMER_runfiles_random40young"
# OUTPUT_DIR="projects/young_all_TFs/random40/output"
INPUT_DIR="projects/young_all_TFs/random_runs/clusterGOMER_runfiles_randomyoung"
OUTPUT_DIR="projects/young_all_TFs/random_runs/output"
TAIL="0"
# TAIL="250"
HOME_DIR="/nfs/fs/clarke/granek"

QUEUE=""


##--------------------------------------------------------------------------
for PREFIX in "R040" ;
    do
    SCRIPT_NAME="$HOME_DIR/q"_"$PREFIX"_"$TAIL.sh"
    if [ -a $SCRIPT_NAME ] ; then 
	rm -f $SCRIPT_NAME ;
    fi

    echo "/local/granek/GOMER/scripts/all_TF_run_random.py $INPUT_DIR $OUTPUT_DIR $TAIL $PREFIX" > $SCRIPT_NAME
    echo $SCRIPT_NAME
    cat $SCRIPT_NAME
    # qsub $SCRIPT_NAME
	qsub $QUEUE $SCRIPT_NAME
    # echo "queue command: qsub $QUEUE $SCRIPT_NAME"
    rm  $SCRIPT_NAME
done
##--------------------------------------------------------------------------

QUEUE="-q short"
# QUEUE=""
##--------------------------------------------------------------------------
for PREFIX in ;
    do
    SCRIPT_NAME="$HOME_DIR/q"_"$PREFIX"_"$TAIL.sh"
    if [ -a $SCRIPT_NAME ] ; then 
	rm -f $SCRIPT_NAME ;
    fi

    echo "/local/granek/GOMER/scripts/all_TF_run_random.py $INPUT_DIR $OUTPUT_DIR $TAIL $PREFIX" > $SCRIPT_NAME
    echo $SCRIPT_NAME
    cat $SCRIPT_NAME
    # qsub $SCRIPT_NAME
	qsub $QUEUE $SCRIPT_NAME
    # echo "queue command: qsub $QUEUE $SCRIPT_NAME"
    rm  $SCRIPT_NAME
done
##--------------------------------------------------------------------------
