if [ "queen" == `hostname` -o "drone1" == `hostname` -o 'drone2' == `hostname` -o 'drone3' == `hostname` ]
then
    echo "I am HIVE" ;  
	BASE_DIR="/local/granek"
elif [ "Gober.local" == `hostname` ]
then
    echo "Powerbook!" ;  
	BASE_DIR="/Users/joshgranek/lab"
else  
    echo "not hive" ;  
	BASE_DIR="/home/josh"
fi 
#---------------------------------------------------------------------
STD_DIR="$BASE_DIR/GOMER/outputs/regression_standards/basic"
OUT_DIR="$BASE_DIR/GOMER/outputs/regress_out"
rm -f $OUT_DIR/regress_diff
#---------------------------------------------------------------------
## DIFF="diff -s -w"
## DIFF="diff -s"
## DIFF="tkdiff"

if [ -n "$1" ]              # Tested variable is quoted.
then
	DIFF=$1
else
	DIFF="diff -s"	
fi 


for FILENAME in                \
	"full_1e7_C9.precached"    \
	"full_C9"                  \
	"full"                     \
	"full_excluded"            \
	"full_1e7.refiltered"      \
	"full_1e7_C9.refiltered"   \
	"full_1e7_C9"              \
	"full_1e7.no_compression"  \
	"full_coordinate_tab"      \
	"full_sequence"            \
	"short_output"             \
	"regress_stdout"           \
	"regress_stderr"           \
 	;
 	do
 	STD_FILE="$STD_DIR"/STD_"$FILENAME"
 	NEW_FILE="$OUT_DIR"/"$FILENAME"
 	echo "--------------------------------------------" >> $OUT_DIR/regress_diff ;
 	COMMAND="$DIFF $NEW_FILE $STD_FILE"
 	echo $COMMAND >> $OUT_DIR/regress_diff ;
 	$COMMAND >> $OUT_DIR/regress_diff ;
done
