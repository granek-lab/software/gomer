if [ "queen" == `hostname` -o "drone1" == `hostname` -o 'drone2' == `hostname` -o 'drone3' == `hostname` ]
then
    echo "I am HIVE" ;  
	BASE_DIR="/local/granek"
elif [ "Gober.local" == `hostname` ]
then
    echo "Powerbook!" ;  
	BASE_DIR="/Users/joshgranek/lab"
else  
    echo "not hive" ;  
	BASE_DIR="/home/josh"
fi 
#---------------------------------------------------------------------
GOMER_DIR="$BASE_DIR"/GOMER
OUT_DIR="$GOMER_DIR"/outputs/test_generate_ROC_curve
REGULATED="$GOMER_DIR"/input_files/regulated_genes/a1_alpha2_regulated.reg
REGULATED_EXCLUDED="$GOMER_DIR"/input_files/regulated_genes/a1_alpha2_regulated.excluded
REGULATED_COMMON="$GOMER_DIR"/input_files/regulated_genes/a1_alpha2_regulated.common
GOMER_OUTPUT="$GOMER_DIR"/outputs/regress_out/full
FEATURE_TABLE="$BASE_DIR"/genomes/saccharomyces_cerevisiae/oct_23_2002/chromosomal_feature.tab
TEST_OUTPUT="$GOMER_DIR"/input_files/tests/generate_ROC_curve/test_data.scores
TEST_REG="$GOMER_DIR"/input_files/tests/generate_ROC_curve/test_data.reg
CAD1_TEST_OUTPUT="$GOMER_DIR"/input_files/tests/generate_ROC_curve/CAD1_1_12.long.new
CAD1_TEST_REG="$GOMER_DIR"/input_files/tests/generate_ROC_curve/CAD1_1.gomerset
#---------------------------------------------------------------------
# OUTFILE="$OUT_DIR"/original
OUTFILE="$OUT_DIR"/new

for COMMAND in \
	"$GOMER_DIR/python_code/generate_ROC_curve.py $GOMER_OUTPUT $REGULATED_COMMON --data $OUTFILE" \
	"$GOMER_DIR/python_code/generate_ROC_curve.py $GOMER_OUTPUT $REGULATED --alias $FEATURE_TABLE --data -" \
	"$GOMER_DIR/python_code/generate_ROC_curve.py $GOMER_OUTPUT $REGULATED --statistics" \
	"$GOMER_DIR/python_code/generate_ROC_curve.py $GOMER_OUTPUT $REGULATED --statistics --alias $FEATURE_TABLE" \
	"$GOMER_DIR/python_code/generate_ROC_curve.py $GOMER_OUTPUT $REGULATED_EXCLUDED --statistics" \
	"$GOMER_DIR/python_code/generate_ROC_curve.py $GOMER_OUTPUT $REGULATED_COMMON --statistics" \
	"$GOMER_DIR/python_code/generate_ROC_curve.py $GOMER_OUTPUT $REGULATED_COMMON --statistics --alias $FEATURE_TABLE" \
	"$GOMER_DIR/python_code/generate_ROC_curve.py $TEST_OUTPUT $TEST_REG --data -" \
	"$GOMER_DIR/python_code/generate_ROC_curve.py $TEST_OUTPUT $TEST_REG --step --data -" \
	"$GOMER_DIR/python_code/generate_ROC_curve.py $CAD1_TEST_OUTPUT $CAD1_TEST_REG --data -" \
	"$GOMER_DIR/python_code/generate_ROC_curve.py $CAD1_TEST_OUTPUT $CAD1_TEST_REG --step --data -" \
	;
	do
	echo "----------------------------------------------"
	echo $COMMAND
	echo $COMMAND >> $OUTFILE
	$COMMAND >> $OUTFILE
	echo "----------------------------------------------" >> $OUTFILE
done


