#! /usr/bin/env python
from __future__ import division

import sys
import os
import time

def usage():
	print >>sys.stderr, 'usage:', sys.argv[0], 'SOURCE_DIR TARGET_DIR'


def main():
	if len(sys.argv) <> 3:
		usage()
		sys.exit(2)
	
	source_dir = sys.argv[1]
	target_dir = sys.argv[2]
	queue_name = "long"
	tarfile_base="granek_tarfile" + str(int(time.time())) + ".tgz"
	queen_tarfile = os.path.join('/nfs/queen/local/', tarfile_base)
	local_tarfile = os.path.join('/local/', tarfile_base)

	script_name = "/local/q_tar.sh" 

	queue_script = '\n'.join((
		' '.join(('cp', queen_tarfile, local_tarfile)),
		'tar --directory ' + target_dir + ' -zxf ' + local_tarfile,
		'rm ' + local_tarfile,
		'sleep 30'
		)) + '\n'

	script_handle = open(script_name, 'w')
	script_handle.write(queue_script)
	script_handle.close()

	tar_system_command = ' '.join(('tar -zcf', local_tarfile, source_dir))
	os.system(tar_system_command)

	queue_system_command = 'qsub -q ' + queue_name + ' ' + script_name

	for machine in ['drone1', 'drone2', 'drone3']:
##	for machine in ['drone1']:
		os.system(queue_system_command)

	
	user_input = raw_input('Delete tarfile (' + local_tarfile + ') [y/n]:')
	if user_input[0].lower() == 'y':
		print 'Removing tarfile'
		os.remove(local_tarfile)
	else:
		print 'NOT removing tarfile'
		
	os.remove(script_name)

if __name__ == "__main__":
	main()


 
# for MACHINE in "drone1" "drone2" "drone3" ; 
# for MACHINE in "drone1"  ; 
#   do 
#     qsub -q short $SCRIPT_NAME 
# done 
# rm  $SCRIPT_NAME 
