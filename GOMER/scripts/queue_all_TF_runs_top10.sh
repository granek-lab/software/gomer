TAIL="0"
HOME_DIR="/nfs/fs/clarke/granek"
for PREFIX in "ABF1" "ACE2" "ARO80" "BAS1" "CAD1" "CIN5" "DIG1" "FHL1" "FKH1" "FKH2" "GCN4" "HAL9" "HAP4" "HIR1" "IME4" "INO2" "LEU3" "MAC1" "MBP1" "MCM1" "MSN4" "NDD1" "NRG1" "PDR1" "PHD1" "PHO4" "RAP1" "REB1" "RLM1" "RME1" "ROX1" "SKN7" "SMP1" "STE12" "SUM1" "SWI4" "SWI5" "SWI6" "YAP1" "YAP5" "YAP6" ;
    do
    SCRIPT_NAME="$HOME_DIR/q"_"$PREFIX"_"$TAIL.sh"
    if [ -a $SCRIPT_NAME ] ; then 
	rm -f $SCRIPT_NAME ;
    fi

    echo "/local/granek/GOMER/scripts/all_TF_run_top10.py $TAIL $PREFIX" > $SCRIPT_NAME
    echo $SCRIPT_NAME
    cat $SCRIPT_NAME
    qsub $SCRIPT_NAME
    rm  $SCRIPT_NAME
done



