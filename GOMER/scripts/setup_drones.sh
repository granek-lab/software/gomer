HOME_DIR="/nfs/fs/clarke/granek"
DRONE_TARFILE="$HOME_DIR/granek_drone.tgz"
HIVE_BASE="$HOME_DIR/hive_home/granek/"
CLONE_SCRIPT="$HIVE_BASE/GOMER/scripts/queued_clone.sh"
                

if [ -a $DRONE_TARFILE ] ; then
	rm -rf $DRONE_TARFILE ;
fi

cd $HIVE_BASE
tar -zcf $DRONE_TARFILE                                            \
							local                                   \
							genomes                                 \
							GOMER/python_code                       \
							GOMER/input_files/config                \
							GOMER/input_files/binding_matrices      \
							GOMER/input_files/regulated_genes       \
							GOMER/input_files/sequence_file_tables  \
							GOMER/intergenic_primers/jan_29_2004    \
							GOMER/projects/young_all_TFs/set1/input \
							GOMER/projects/young_all_TFs/set2/input \
							GOMER/scripts                           \


for DRONE in "drone1" "drone2" "drone3" ;
	do
	qsub -q short $CLONE_SCRIPT
done
