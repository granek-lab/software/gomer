##============================================================
##============================================================
if [ "queen" == `hostname` ]
then
    echo "I am QUEEN!" ;  
	## ln -s /nfs/fs/clarke/granek/hive_home/granek/ /local/granek
fi
if [ "queen" == `hostname` -o "drone1" == `hostname` -o 'drone2' == `hostname` -o 'drone3' == `hostname` ]
then
    echo "I am HIVE" ;  
	BASE_DIR="/local/granek"
	GOMER_BASE="$BASE_DIR/GOMER"
	## PYTHON="/local/granek/local/bin/python2.3 -OO"
	PYTHON="/local/granek/local/bin/python2.3"
	CONFIG="-c $GOMER_BASE/input_files/config/gomer_config.hive"
	SEQ_TABLE="$GOMER_BASE/input_files/sequence_file_tables/cerevisiae_chromosome_table_with_mito.hive.oct_2002"
	CACHE_DIR="/local/granek/GOMER_DATA/regression_cache"
	# CACHE_DIR="/local/granek/GOMER_DATA/cache"
	FAST_CACHE_DIR="/tmp/granek"
else  
    echo "not hive" ;  
	BASE_DIR="/home/josh"
	GOMER_BASE="$BASE_DIR/GOMER"
	## PYTHON="python2.3 -OO"
	PYTHON="python2.3"
	CONFIG="-c $GOMER_BASE/input_files/config/gomer_config"
	# CONFIG="-c $GOMER_BASE/input_files/config/gomer_config_fast_cache"
	SEQ_TABLE="$GOMER_BASE/input_files/sequence_file_tables/cerevisiae_chromosome_table_with_mito.oct_2002"
	CACHE_DIR="/home/josh/GOMER_DATA/regression_cache"
	FAST_CACHE_DIR="/dev/shm/josh"
fi 
#---------------------------------------------------------------------
FAST_CACHE_DIR=""  # turn off the fast cache for the moment
if [ $FAST_CACHE_DIR ] ; then
	if ! [ -a "$FAST_CACHE_DIR" ] ; then 
        mkdirhier $FAST_CACHE_DIR
	fi
	FAST_CACHE="--fast_cache=""$FAST_CACHE_DIR"
fi
CACHE="--cache_directory=""$CACHE_DIR"
GOMER_CMD="$PYTHON $BASE_DIR/GOMER/python_code/gomer.py $CONFIG $CACHE $FAST_CACHE"

#------------------------------------------------------
#------------------------------------------------------
STD_DIR="$BASE_DIR/GOMER/outputs/regression_standards/basic"
OUT_DIR="$BASE_DIR/GOMER/outputs/regress_out"
STDOUT="$OUT_DIR/regress_stdout"
STDERR="$OUT_DIR/regress_stderr"

MATRIX="$BASE_DIR/GOMER/input_files/binding_matrices/paper_gold.probs"
MATRIX_600="$BASE_DIR/GOMER/input_files/binding_matrices/paper_gold.600.probs"
REGULATED="$BASE_DIR/GOMER/input_files/regulated_genes/a1_alpha2_regulated.list"
REGULATED_EXCLUDED="$BASE_DIR/GOMER/input_files/regulated_genes/a1_alpha2_regulated.excluded"
FILTER="--filter_cutoff_ratio 1e7"
# GOMER_CMD="$PYTHON $BASE_DIR/GOMER/python_code/gomer.py $CONFIG"
SHORT="--short $OUT_DIR/short_output"
#--------------------------------
if [ -a "$OUT_DIR" ] ; then 
	rm $OUT_DIR/* ;
else 
	mkdir $OUT_DIR ;
fi
#--------------------------------
#------------------------------------------------------
if [ -a $CACHE_DIR ] ; then 
	rm $CACHE_DIR/* ;
else 
	mkdir $CACHE_DIR ;
fi
#------------------------------------------------------
echo CACHE_DIR is "$CACHE_DIR"
#--------------------------------
# /usr/bin/time --append --output=~/GOMER/outputs/full_1e7_C9 
#------------------------------------------------------
$GOMER_CMD  $FILTER $SEQ_TABLE $MATRIX $REGULATED -C9 --output $OUT_DIR/full_1e7_C9 1>> $STDOUT 2>> $STDERR
$GOMER_CMD  $FILTER $SEQ_TABLE $MATRIX $REGULATED -C9 --output $OUT_DIR/full_1e7_C9.precached 1>> $STDOUT 2>> $STDERR
#------------------------------------------------------
rm -f $CACHE_DIR/*
#------------------------------------------------------
$GOMER_CMD  $FILTER $SEQ_TABLE $MATRIX $REGULATED --output $OUT_DIR/full_1e7.no_compression 1>> $STDOUT 2>> $STDERR
#------------------------------------------------------
rm -f $CACHE_DIR/*
#------------------------------------------------------
$GOMER_CMD $SEQ_TABLE $MATRIX $REGULATED $SHORT --output $OUT_DIR/full 1>> $STDOUT 2>> $STDERR
$GOMER_CMD $SEQ_TABLE $MATRIX $REGULATED_EXCLUDED --output $OUT_DIR/full_excluded 1>> $STDOUT 2>> $STDERR
$GOMER_CMD $FILTER $SEQ_TABLE $MATRIX $REGULATED --output $OUT_DIR/full_1e7.refiltered 1>> $STDOUT 2>> $STDERR
#------------------------------------------------------
rm -f $CACHE_DIR/*
#------------------------------------------------------
$GOMER_CMD -C9 $SEQ_TABLE $MATRIX $REGULATED --output $OUT_DIR/full_C9 1>> $STDOUT 2>> $STDERR
$GOMER_CMD -C9 $FILTER $SEQ_TABLE $MATRIX $REGULATED --output $OUT_DIR/full_1e7_C9.refiltered 1>> $STDOUT 2>> $STDERR
#------------------------------------------------------
rm -f $CACHE_DIR/*
#------------------------------------------------------
$GOMER_CMD  $SEQ_TABLE $MATRIX_600 $REGULATED -D --output $OUT_DIR/full_matrix600 >> $STDOUT 2>> $STDERR
#------------------------------------------------------
rm -f $CACHE_DIR/*
#------------------------------------------------------
TEST_SEQ_DIR="$BASE_DIR/GOMER/input_files/tests/coordinate_and_sequence_modes"
#------------------------------------------------------
$GOMER_CMD --coordinate_feature  $TEST_SEQ_DIR/600_1_upstream_feature_coordinates.tab \
			$SEQ_TABLE  $MATRIX \
			$BASE_DIR/GOMER/input_files/regulated_genes/a1_alpha2_regulated_coord_names.reg \
			--output $OUT_DIR/full_coordinate_tab 1>> $STDOUT 2>> $STDERR
#------------------------------------------------------
## $GOMER_CMD  --coordinate_feature $TEST_SEQ_DIR/600_1_upstream_feature_coordinates_part1.tab --coordinate_feature $TEST_SEQ_DIR/600_1_upstream_feature_coordinates_part2.tab --coordinate_feature $TEST_SEQ_DIR/600_1_upstream_feature_coordinates_part3.tab $SEQ_TABLE  $MATRIX $BASE_DIR/GOMER/input_files/regulated_genes/a1_alpha2_regulated_coord_names.reg --output $OUT_DIR/full_coordinate_tab_split 1>> $STDOUT 2>> $STDERR
#------------------------------------------------------
$GOMER_CMD --sequence_feature $TEST_SEQ_DIR/600_1_upstream_feature_coordinates.fsa $MATRIX $BASE_DIR/GOMER/input_files/regulated_genes/a1_alpha2_regulated_coord_names.reg --output $OUT_DIR/full_sequence 1>> $STDOUT 2>> $STDERR
#------------------------------------------------------
## $GOMER_CMD  --sequence_feature $TEST_SEQ_DIR/600_1_upstream_feature_coordinates_part1.fsa --sequence_feature $TEST_SEQ_DIR/600_1_upstream_feature_coordinates_part2.fsa --sequence_feature $TEST_SEQ_DIR/600_1_upstream_feature_coordinates_part3.fsa $MATRIX $BASE_DIR/GOMER/input_files/regulated_genes/a1_alpha2_regulated_coord_names.reg --output $OUT_DIR/full_sequence_split 1>> $STDOUT 2>> $STDERR
#------------------------------------------------------
## $GOMER_CMD --coordinate_feature  ~/GOMER/input_files/tests/coordinate_and_sequence_modes/600_0_upstream_feature_coordinates.csv $SEQ_TABLE  $MATRIX ~/GOMER/input_files/regulated_genes/a1_alpha2_regulated_coord_names.reg --output $OUT_DIR/full_coordinate_csv
#------------------------------------------------------
# /usr/bin/time --append --output=~/GOMER/outputs/full_sequence 
#------------------------------------------------------
#---------------------------------------------------------------------
rm -f $OUT_DIR/regress_diff
#---------------------------------------------------------------------
## DIFF="diff -s -w"
DIFF="diff -s"
## DIFF="tkdiff"
for FILENAME in                \
	"full_1e7_C9.precached"    \
	"full_C9"                  \
	"full"                     \
	"full_excluded"            \
	"full_1e7.refiltered"      \
	"full_1e7_C9.refiltered"   \
	"full_1e7_C9"              \
	"full_1e7.no_compression"  \
	"full_coordinate_tab"      \
	"full_sequence"            \
	"full_matrix600"           \
	"short_output"             \
	"regress_stdout"           \
	"regress_stderr"           \
 	;
 	do
 	STD_FILE="$STD_DIR"/STD_"$FILENAME"
 	NEW_FILE="$OUT_DIR"/"$FILENAME"
 	echo "--------------------------------------------" >> $OUT_DIR/regress_diff ;
 	COMMAND="$DIFF $STD_FILE $NEW_FILE"
 	echo $COMMAND >> $OUT_DIR/regress_diff ;
 	$COMMAND >> $OUT_DIR/regress_diff ;
done





if [ -a "$FAST_CACHE_DIR" ] ; then 
	rmdir $FAST_CACHE_DIR
fi

