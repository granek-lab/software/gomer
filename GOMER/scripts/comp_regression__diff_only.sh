if [ "queen" == `hostname` -o "drone1" == `hostname` -o 'drone2' == `hostname` -o 'drone3' == `hostname` ]
then
    echo "I am HIVE" ;  
	BASE_DIR="/local/granek"
elif [ "Gober.local" == `hostname` ]
then
    echo "Powerbook!" ;  
	BASE_DIR="/Users/joshgranek/lab"
else  
    echo "not hive" ;  
	BASE_DIR="/home/josh"
fi 
#---------------------------------------------------------------------
STD_DIR="$BASE_DIR/GOMER/outputs/regression_standards/comp"
OUT_DIR="$BASE_DIR/GOMER/outputs/comp_regress_out"
# #--------------------------------
##==============================================================================
##++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
#------------------------------------------------------
rm $OUT_DIR/comp_regress_diff
#------------------------------------------------------
## DIFF="diff -s -w" 
## DIFF="diff -s" 
## DIFF="tkdiff" 

if [ -n "$1" ]              # Tested variable is quoted.
then
	DIFF=$1
else
	DIFF="diff -s"	
fi 


for FILENAME in                     \
	"seq_ABC_abg_compa2_0_compb_compg"		 \
	"seq_ABC_abg_compa_compb2_0_compg"		 \
	"seq_ABC_abg_compa_compb2_0_compg2_0"	 \
	"seq_ABC_abg_compa_compb_compg"			 \
	"seq_ABC_abg_compa_compb_compg_2_0"		 \
	"seq_ABC_abh_compa_compb_comph"			 \
	"seq_ABC_agb_compa_compg_compb"			 \
	"seq_AB_ab_compa2_0_compb_max10"		 \
	"seq_AB_ab_compa_NO_COMP"				 \
	"seq_AB_ab_compa_compb_max10"			 \
	"seq_AB_ab_compa_compb_max10_conc0_25"	 \
	"seq_AB_ab_compa_compb_max10_conc0_5"	 \
	"seq_AB_ab_compa_compb_max10_conc1_0"	 \
	"seq_AB_ab_compa_compb_max10_conc2_0"	 \
	"seq_AB_ab_compa_compb_max10_conc4_0"	 \
	"seq_AB_ab_compa_compb_max9"			 \
	"seq_AB_ab_multi"						 \
	"seq_AB_ab_multi__compa_compb"			 \
	"seq_AB_ay_compa_compb"					 \
	"seq_AB_zb_compa_compb"					 \
	"seq_AB_zy_compa_compb"					 \
	"seq_ABC_abg_10_pconc5_cconc1"					 \
	"seq_ABC_abg_10_pconc1_cconc5"					 \
	"seq_ABC_abg_10_pconc1_cconc1"					 \
	"seq_ABC_abg_10_pconc2_cconc5"					 \
	"seq_ABC_abg_10_pconc5_cconc2"					 \
	"seq_ABC_abg_10_pconcNone_cconc5"					 \
	"comp_random_probs_1"					 \
	"comp_random_probs_3"					 \
	"comp_random_probs_2"					 \
	"comp_coop_random_probs_1"               \
	"comp_coop_random_probs_2"               \
	"comp_coop_random_probs_3"               \
	"comp_nocoop_random_probs_1"             \
	"Ndt80_nocomp"							 \
	"Sum1_nocomp"							 \
	"Ndt80_1.0__comp_Sum1_1.0"               \
	"comp_regress_stderr"                    \
	"comp_regress_stdout"                    \
	"seq_ABC_abg"							 \
	"seq_ABC_abh"							 \
	"seq_ABC_agb"							 \
	"seq_AB_ab"								 \
	"seq_AB_ay"								 \
	"seq_AB_zb"								 \
	"seq_AB_zy"								 \
 	;
 	do
 	STD_FILE="$STD_DIR"/STD_"$FILENAME"
 	NEW_FILE="$OUT_DIR"/"$FILENAME"
 	# echo "STD_FILE: $STD_FILE"
 	# echo "NEW_FILE: $NEW_FILE"
 	echo "--------------------------------------------" >> $OUT_DIR/comp_regress_diff ;
	COMMAND="$DIFF  $NEW_FILE $STD_FILE"
 	echo $COMMAND >> $OUT_DIR/comp_regress_diff ;
 	$COMMAND >> $OUT_DIR/comp_regress_diff ;
done

