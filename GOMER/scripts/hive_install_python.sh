##--------------------------------------
bash $HOME/gomer_scripts/hive_clean_python.sh
##--------------------------------------

INSTALL_SUFFIX="granek/local"
INSTALL_DIR="/local/$INSTALL_SUFFIX"
PYTHON="$INSTALL_DIR/bin/python2.3"
echo $INSTALL_DIR
echo $INSTALL_SUFFIX
echo $PYTHON

if ! [ -a "$INSTALL_DIR" ] ; then 
	mkdirhier $INSTALL_DIR ; 
fi


##-------------INSTALL PYTHON2.3--------
cd /nfs/fs/clarke/granek/python2.3_source/python/python2.3-2.3
make clean
./configure --prefix="$INSTALL_DIR"
make
make test
make install

##-------------INSTALL PSYCO--------
cd /nfs/fs/clarke/granek/downloads/psyco-1.1.1
$PYTHON setup.py install

##-------------INSTALL NUMERIC--------
cd /nfs/fs/clarke/granek/python2.3_source/numeric/python-numeric-23.1
$PYTHON setup.py install  
sh makeclean.sh

# ##-------------COPY TO DRONES---------
# for BASE in             \
# 	"/nfs/drone2/local" \
# 	"/nfs/drone3/local" \
# 	"/nfs/drone1/local" \
# 	;
# 	do
# 	##CUR_DIR=$BASE/$INSTALL_SUFFIX
# 	## mkdirhier $CUR_DIR
# 	cp -r /local/granek $BASE
# done
