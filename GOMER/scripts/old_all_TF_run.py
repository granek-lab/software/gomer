#! /usr/bin/env python

import os
import sys
import re
import string
import glob
import getopt

"""
usage: gomer.py [options] ChromTable ProbMatrix RegOrf
       gomer.py --coordinate_feature CoordinateFeature ChromTable ProbMatrix RegOrf

options:
  -rKAPPA_FILE  PARAM_STRING, --reg_region_kappa=KAPPA_FILE  PARAM_STRING
  -sOUTPUT_FILE, --short_output=OUTPUT_FILE
"""

#------------------------------------------------------------------------------
base_dir='/local/granek'
gomer_base= os.path.join(base_dir,'GOMER')
all_tf_base=os.path.join(base_dir,'')
fast_cache_dir='/tmp/granek'
### base_dir='/Users/joshgranek/lab'
##base_dir='/home/josh'
##gomer_base= os.path.join(base_dir,'GOMER')
##all_tf_base=os.path.join(base_dir,'GOMER/projects/all_TFs')
#------------------------------------------------------------------------------
prob_directory='100X_TRANS_gomer_prob'
regulated_directory='100X_TRANS_list'
output_directory='100X_output'
config_filename='100X_TRANS_GOMER/100X_intergenic_config'
chrom_table_filename='100X_TRANS_GOMER/chromosome_table_file'

coord_feature_filename='intergenic_primers/Intergenic_Features'
script_name='python_code/gomer.py'
#------------------------------------------------------------------------------
output_directory       = os.path.join(all_tf_base, output_directory)
prob_directory         = os.path.join(all_tf_base, prob_directory)
regulated_directory    = os.path.join(all_tf_base, regulated_directory)
config_filename        = os.path.join(all_tf_base, config_filename)
chrom_table_filename   = os.path.join(all_tf_base, chrom_table_filename)

coord_feature_filename = os.path.join(gomer_base, coord_feature_filename)
script_name            = os.path.join(gomer_base, script_name)
#------------------------------------------------------------------------------
if not os.path.exists(output_directory):
	os.makedirs(output_directory)
#------------------------------------------------------------------------------
for filename in [prob_directory,regulated_directory, config_filename,
				 chrom_table_filename, coord_feature_filename,script_name]:
	if not os.path.exists(filename):
		print >>sys.stderr, "File doesn't exist:", filename
		raise
#------------------------------------------------------------------------------
python_command='/local/granek/local/bin/python2.3 -OO'
## kappa_filename='coordinate_single_square_reg_region_model_gomer.py'
## kappa_options= '" "'
kappa_filename='coordinate_linear_decay_reg_region_model_gomer.py'
kappa_options= '"flank=0;tail=1000"'
## kappa_options= 'flank=0::tail=1000'
#------------------------------------------------------------------------------
options_list = ['-C9',
				'--no_debug',
				'--filter_cutoff_ratio=1e5',
				'--coordinate_feature='+ coord_feature_filename,
				'-c', config_filename,
				'-r', kappa_filename, kappa_options
				]
#------------------------------------------------------------------------------
tf_name_re = re.compile('([A-Za-z0-9]+)\-BPwidth\d+\-motif\d+\.prob\.gprob')
prob_file_endings = ('.prob', '.gprob')
#------------------------------------------------------------------------------


try:
	opts, args = getopt.getopt(sys.argv[1:], "t", ["test"])
except getopt.GetoptError:
	# print help information and exit:
	usage()
	sys.exit(2)
output = None
test = 0
for o, a in opts:
	if o in ("-t", "--test"):
		test = 1
#------------------------------------------------------------------------------



if len(args) >= 1:
	prefix_list = args
elif len(args) == 0:
	first_letters = string.uppercase + string.lowercase
	print >>sys.stderr, 'Using all first letters'
else:
	print >>sys.stderr, 'Should supply a list of prefixes'
	print >>sys.stderr, '-t/--test for a test run (only run on one file)'

if not os.path.exists(fast_cache_dir):
	os.makedirs(fast_cache_dir)

prob_file_list = []
for prefix in prefix_list:
	prob_file_list.extend(glob.glob(os.path.join(prob_directory, prefix+'*')))
if test:
	prob_file_list = [prob_file_list[0]]
for prob_filename in prob_file_list:
	##--------------------------------------------------------
	output_basename = os.path.basename(prob_filename)
	for ending in prob_file_endings:
		output_basename = output_basename.replace(ending, '')
	
	output_basename = output_basename + 'flank0_tail1000'
	output_basename = output_basename + '.out'
	out_filename = os.path.join(output_directory, output_basename)
	##--------------------------------------------------------
	
	tf_name_match = tf_name_re.match(os.path.basename(prob_filename))
	if tf_name_match :
		tf_name = tf_name_match.group(1)
		regulated_filename = "100X-"+ tf_name +".list"
		regulated_filename = os.path.join(regulated_directory, regulated_filename)
	else:
		raise "Doesn't match pattern for probability file name: " + os.path.basename(prob_filename)
	##--------------------------------------------------------
	command_string = ' '.join((python_command, script_name,
							   chrom_table_filename, prob_filename, regulated_filename,
							   '-s', out_filename)
							  + tuple(options_list))
	print '-'*50, '\n', command_string, '\n', '-'*50
	os.system(command_string)
	for filename in [prob_filename, regulated_filename]:
		if not os.path.exists(filename):
			print >>sys.stderr, "File doesn't exist:", filename
			raise

	## os.system(command_string)
	
"""
ls -1 /home/dnoll1/100X_TRANS_gomer_prob_files/LEU3* | xargs -n1 -i --config=/home/dnoll1/100X_TRANS_GOMER_run/100X_intergenic_config_file --coordinate_feature=/home/dnoll1/GOMER/intergenic_primers/Intergenic_Features.clean --david_output={}.gomer /home/dnoll1/100X_TRANS_GOMER_run/chromosome_table_file {} /home/dnoll1/100X_TRANS_list_folder/100X-LEU3.list
"""

"100X-ACE2.list"

"""
ACE2-BPwidth10-motif1.prob.gprob
ACE2-BPwidth13-motif8.prob.gprob
ACE2-BPwidth17-motif6.prob.gprob
ACE2-BPwidth6-motif4.prob.gprob
ACE2-BPwidth10-motif10.prob.gprob
ACE2-BPwidth13-motif9.prob.gprob
ACE2-BPwidth17-motif7.prob.gprob
ACE2-BPwidth6-motif5.prob.gprob
"""
