
## /usr/bin/time -p -o ~/GOMER/outputs/regress_out/coop_run.stdout  --append ~/GOMER/scripts/coop_regression.sh 2>~/GOMER/outputs/regress_out/coop_run.stderr 1>~/GOMER/outputs/regress_out/coop_run.stdout
#---------------------------------------------------------------------
# if [ "queen" == `hostname`]
# then
#     echo "I am QUEEN!" ;  
# 	## ln -s /nfs/fs/clarke/granek/hive_home/granek/ /local/granek
# fi
if [ "queen" == `hostname` -o "drone1" == `hostname` -o 'drone2' == `hostname` -o 'drone3' == `hostname` ]
then
    echo "I am HIVE" ;  
	BASE_DIR="/local/granek"
	GOMER_BASE="$BASE_DIR/GOMER"
	PYTHON="/local/granek/local/bin/python2.3 -OO"
	CONFIG="-c $GOMER_BASE/input_files/config/gomer_config.jan_2004.hive"
	SEQ_TABLE="$GOMER_BASE/input_files/sequence_file_tables/cerevisiae_chromosome_table_with_mito.hive.jan_2004"
	CACHE_DIR="/local/granek/GOMER_DATA/cache"
	FAST_CACHE="/tmp/granek"
	if ! [ -a "$FAST_CACHE" ] ; then 
        mkdirhier $FAST_CACHE
	fi

else  
    echo "not hive" ;  
	BASE_DIR="/home/josh"
	GOMER_BASE="$BASE_DIR/GOMER"
	PYTHON="python2.3 -OO"
	CONFIG="-c $GOMER_BASE/input_files/config/gomer_config.jan_2004"
	SEQ_TABLE="$GOMER_BASE/input_files/sequence_file_tables/cerevisiae_chromosome_table_with_mito.jan_2004"
	CACHE_DIR="/blob/home/josh/GOMER_DATA/cache"
fi 
#---------------------------------------------------------------------
STD_DIR="$BASE_DIR/GOMER/outputs/regression_standards/coop"
OUT_DIR="$BASE_DIR/GOMER/outputs/coop_regress_out.jan_2004"
# #--------------------------------
REGULATED="$BASE_DIR/GOMER/projects/Nrd1/regulated/Nrd_regulated.reg"
GOMER_CMD="$PYTHON $BASE_DIR/GOMER/python_code/gomer.py $CONFIG"
FEATURE_TYPES="-f ORF -f snRNA -f snoRNA"
MATRIX_DIR="$BASE_DIR/GOMER/projects/Nrd1/matrices"
PRIMARY_MATRIX="Nrd1.probs"
#-----------------------------------------
REG_REGION_KAPPA="downstream_same_strand_reg_region_model_gomer.py"
REG_REGION_PARAMS="regulatory_five_prime=1;regulatory_three_prime=100"
REG_REGION="-r $REG_REGION_KAPPA $REG_REGION_PARAMS"
#-----------------------------------------
SECONDARY_MATRIX="$MATRIX_DIR/Nab3.probs"
COOP_DISTANCE="max_distance=70;min_distance=1"
COOP_KAPPA="same_strand_simple_square_coop_model_gomer.py"
SECOND_CONC="DEFAULT"
K_DIMER_RATIO="DEFAULT"
COOP="--coop_kappa $COOP_KAPPA $COOP_DISTANCE $SECONDARY_MATRIX $SECOND_CONC $K_DIMER_RATIO"
#------------------------------------------------------
#------------------------------------------------------
if [ -a "$OUT_DIR" ] ; then 
	rm $OUT_DIR/* ;
else 
	mkdir $OUT_DIR ;
fi
#------------------------------------------------------
#------------------------------------------------------
if [ -a $CACHE_DIR ] ; then 
	rm $CACHE_DIR/* ;
else 
	mkdir $CACHE_DIR ;
fi
#------------------------------------------------------
#------------------------------------------------------




##++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
##==============================================================================
TEST_DIR="$GOMER_BASE/input_files/tests/coop_tests"
GEN_SEQ="$PYTHON $TEST_DIR/generate_sequence.py"
SEQ_DIR="$OUT_DIR"
##-----------------------------------------------------------------
$GEN_SEQ $TEST_DIR/base_sequence.fsa $TEST_DIR/template_A $TEST_DIR/coop_a.probs > $SEQ_DIR/seq_A_a
$GEN_SEQ $TEST_DIR/base_sequence.fsa $TEST_DIR/template_A $TEST_DIR/coop_b.probs > $SEQ_DIR/seq_A_b
$GEN_SEQ $TEST_DIR/base_sequence.fsa $TEST_DIR/template_A $TEST_DIR/coop_z.probs > $SEQ_DIR/seq_A_z

$GEN_SEQ $TEST_DIR/base_sequence.fsa $TEST_DIR/template_AB $TEST_DIR/coop_a.probs $TEST_DIR/coop_a.probs > $SEQ_DIR/seq_AB_aa
$GEN_SEQ $TEST_DIR/base_sequence.fsa $TEST_DIR/template_AB $TEST_DIR/coop_b.probs $TEST_DIR/coop_b.probs > $SEQ_DIR/seq_AB_bb
$GEN_SEQ $TEST_DIR/base_sequence.fsa $TEST_DIR/template_AB $TEST_DIR/coop_a.probs $TEST_DIR/coop_b.probs > $SEQ_DIR/seq_AB_ab
$GEN_SEQ $TEST_DIR/base_sequence.fsa $TEST_DIR/template_AB $TEST_DIR/coop_z.probs $TEST_DIR/coop_b.probs > $SEQ_DIR/seq_AB_zb
$GEN_SEQ $TEST_DIR/base_sequence.fsa $TEST_DIR/template_AB $TEST_DIR/coop_d.probs $TEST_DIR/coop_b.probs > $SEQ_DIR/seq_AB_db
$GEN_SEQ $TEST_DIR/base_sequence.fsa $TEST_DIR/template_AB $TEST_DIR/coop_d.probs $TEST_DIR/coop_d.probs > $SEQ_DIR/seq_AB_dd

$GEN_SEQ $TEST_DIR/base_sequence.fsa $TEST_DIR/template_ABC $TEST_DIR/coop_a.probs $TEST_DIR/coop_b.probs $TEST_DIR/coop_z.probs> $SEQ_DIR/seq_ABC_abz
$GEN_SEQ $TEST_DIR/base_sequence.fsa $TEST_DIR/template_ABC $TEST_DIR/coop_a.probs $TEST_DIR/coop_b.probs $TEST_DIR/coop_d.probs> $SEQ_DIR/seq_ABC_abd

##-----------------------------------------------------------------
$GEN_SEQ $TEST_DIR/base_sequence.fsa $TEST_DIR/template_AB $TEST_DIR/coop_e.probs $TEST_DIR/coop_b.probs > $SEQ_DIR/seq_AB_eb
$GEN_SEQ $TEST_DIR/base_sequence.fsa $TEST_DIR/template_AB $TEST_DIR/coop_e.probs $TEST_DIR/coop_e.probs > $SEQ_DIR/seq_AB_ee

$GEN_SEQ $TEST_DIR/base_sequence.fsa $TEST_DIR/template_ABC $TEST_DIR/coop_a.probs $TEST_DIR/coop_b.probs $TEST_DIR/coop_e.probs> $SEQ_DIR/seq_ABC_abe
##-----------------------------------------------------------------
##==============================================================================
##==============================================================================
## HOMOTYPIC

$GOMER_CMD --sequence_feature $SEQ_DIR/seq_A_b $TEST_DIR/coop_b.probs \
		 $GOMER_BASE/input_files/regulated_genes/empty.list > $OUT_DIR/seq_A_b__no_coop

$GOMER_CMD --sequence_feature $SEQ_DIR/seq_A_b $TEST_DIR/coop_b.probs \
		 $GOMER_BASE/input_files/regulated_genes/empty.list \
		 --coop_kappa=simple_square_coop_model_gomer.py  "min_distance=0;max_distance=30" $TEST_DIR/coop_b.probs \
		 default default > $OUT_DIR/seq_A_b_0to30

$GOMER_CMD --sequence_feature $SEQ_DIR/seq_A_b $TEST_DIR/coop_b.probs \
		 $GOMER_BASE/input_files/regulated_genes/empty.list \
		 --coop_kappa=simple_square_coop_model_gomer.py  "min_distance=10;max_distance=30" $TEST_DIR/coop_b.probs \
		 default default > $OUT_DIR/seq_A_b_10to30

$GOMER_CMD --sequence_feature $SEQ_DIR/seq_A_b $TEST_DIR/coop_b.probs \
		 $GOMER_BASE/input_files/regulated_genes/empty.list \
		 --coop_kappa=simple_square_coop_model_gomer.py  "min_distance=0;max_distance=40" $TEST_DIR/coop_b.probs \
		 default default > $OUT_DIR/seq_A_b_0to40

##--------------------------------------

$GOMER_CMD --sequence_feature $SEQ_DIR/seq_A_a $TEST_DIR/coop_a.probs \
		 $GOMER_BASE/input_files/regulated_genes/empty.list > $OUT_DIR/seq_A_a__no_coop

$GOMER_CMD --sequence_feature $SEQ_DIR/seq_A_a $TEST_DIR/coop_a.probs \
		 $GOMER_BASE/input_files/regulated_genes/empty.list \
		 --coop_kappa=simple_square_coop_model_gomer.py  "min_distance=10;max_distance=30" $TEST_DIR/coop_a.probs \
		 default default > $OUT_DIR/seq_A_a_10to30

##--------------------------------------

$GOMER_CMD --sequence_feature $SEQ_DIR/seq_A_a $TEST_DIR/coop_z.probs \
		 $GOMER_BASE/input_files/regulated_genes/empty.list > $OUT_DIR/seq_A_a_coopz_no_coop

$GOMER_CMD --sequence_feature $SEQ_DIR/seq_A_a $TEST_DIR/coop_z.probs \
		 $GOMER_BASE/input_files/regulated_genes/empty.list \
		 --coop_kappa=simple_square_coop_model_gomer.py  "min_distance=10;max_distance=30" $TEST_DIR/coop_z.probs \
		 default default > $OUT_DIR/seq_A_a_coopz_10to30
##==============================================================================
##==============================================================================
## HETEROTYPIC
export GOMER_HOME='/home/josh/GOMER/'
GOMER='/home/josh/GOMER/python_code/gomer.py'

$GOMER_CMD --sequence_feature $SEQ_DIR/seq_AB_bb $TEST_DIR/coop_b.probs \
		 $GOMER_BASE/input_files/regulated_genes/empty.list > $OUT_DIR/seq_AB_bb__no_coop

$GOMER_CMD --sequence_feature $SEQ_DIR/seq_AB_bb $TEST_DIR/coop_b.probs \
		 $GOMER_BASE/input_files/regulated_genes/empty.list \
		 --coop_kappa=simple_square_coop_model_gomer.py \
		 "min_distance=10;max_distance=30" $TEST_DIR/coop_b.probs \
		 default default > $OUT_DIR/seq_AB_bb_10to30
##--------------------------------------
$GOMER_CMD --sequence_feature $SEQ_DIR/seq_AB_ab $TEST_DIR/coop_b.probs \
		 $GOMER_BASE/input_files/regulated_genes/empty.list \
		 --coop_kappa=simple_square_coop_model_gomer.py \
		 "min_distance=10;max_distance=30" $TEST_DIR/coop_b.probs \
		 default default > $OUT_DIR/seq_AB_ab_coopb_coopb10to30

$GOMER_CMD --sequence_feature $SEQ_DIR/seq_AB_ab $TEST_DIR/coop_b.probs \
		 $GOMER_BASE/input_files/regulated_genes/empty.list \
		 --coop_kappa=simple_square_coop_model_gomer.py \
		 "min_distance=10;max_distance=30" $TEST_DIR/coop_a.probs \
		 default default > $OUT_DIR/seq_AB_ab_10to30

$GOMER_CMD --sequence_feature $SEQ_DIR/seq_AB_ab $TEST_DIR/coop_b.probs \
		 $GOMER_BASE/input_files/regulated_genes/empty.list \
		 --coop_kappa=simple_square_coop_model_gomer.py \
		 "min_distance=0;max_distance=30" $TEST_DIR/coop_a.probs \
		 default default > $OUT_DIR/seq_AB_ab_0to30

$GOMER_CMD --sequence_feature $SEQ_DIR/seq_AB_ab $TEST_DIR/coop_b.probs \
		 $GOMER_BASE/input_files/regulated_genes/empty.list \
		 --coop_kappa=simple_square_coop_model_gomer.py \
		 "min_distance=0;max_distance=40" $TEST_DIR/coop_a.probs \
		 default default > $OUT_DIR/seq_AB_ab_0to40

$GOMER_CMD --sequence_feature $SEQ_DIR/seq_AB_zb $TEST_DIR/coop_b.probs \
		 $GOMER_BASE/input_files/regulated_genes/empty.list \
		 --coop_kappa=simple_square_coop_model_gomer.py \
		 "min_distance=10;max_distance=30" $TEST_DIR/coop_a.probs \
		 default default > $OUT_DIR/seq_AB_zb_coopb_coopa_10to30

$GOMER_CMD --sequence_feature $SEQ_DIR/seq_AB_db $TEST_DIR/coop_b.probs \
		 $GOMER_BASE/input_files/regulated_genes/empty.list \
		 --coop_kappa=simple_square_coop_model_gomer.py \
		 "min_distance=10;max_distance=30" $TEST_DIR/coop_a.probs \
		 default default > $OUT_DIR/seq_AB_db_coopb_coopa_10to30

$GOMER_CMD --sequence_feature $SEQ_DIR/seq_AB_ab $TEST_DIR/coop_b.probs \
		 $GOMER_BASE/input_files/regulated_genes/empty.list \
		 --coop_kappa=simple_square_coop_model_gomer.py \
		 "min_distance=10;max_distance=30" $TEST_DIR/coop_d.probs \
		 default default > $OUT_DIR/seq_AB_ab_coopb_coopd_10to30

$GOMER_CMD --sequence_feature $SEQ_DIR/seq_AB_db $TEST_DIR/coop_b.probs \
		 $GOMER_BASE/input_files/regulated_genes/empty.list \
		 --coop_kappa=simple_square_coop_model_gomer.py \
		 "min_distance=10;max_distance=30" $TEST_DIR/coop_d.probs \
		 default default > $OUT_DIR/seq_AB_db_10to30

##-----------------------------------------------------------------
$GOMER_CMD --sequence_feature $SEQ_DIR/seq_AB_dd $TEST_DIR/coop_d.probs \
		 $GOMER_BASE/input_files/regulated_genes/empty.list > $OUT_DIR/seq_AB_dd__no_coop
##-----------------------------------------------------------------
##-----------------------------------------------------------------
$GOMER_CMD --sequence_feature $SEQ_DIR/seq_AB_ee $TEST_DIR/coop_e.probs \
		 $GOMER_BASE/input_files/regulated_genes/empty.list > $OUT_DIR/seq_AB_ee__no_coop

$GOMER_CMD --sequence_feature $SEQ_DIR/seq_AB_eb $TEST_DIR/coop_b.probs \
		 $GOMER_BASE/input_files/regulated_genes/empty.list \
		 --coop_kappa=simple_square_coop_model_gomer.py \
		 "min_distance=10;max_distance=30" $TEST_DIR/coop_a.probs \
		 default default > $OUT_DIR/seq_AB_eb_coopb_coopa_10to30

$GOMER_CMD --sequence_feature $SEQ_DIR/seq_AB_ab $TEST_DIR/coop_b.probs \
		 $GOMER_BASE/input_files/regulated_genes/empty.list \
		 --coop_kappa=simple_square_coop_model_gomer.py \
		 "min_distance=10;max_distance=30" $TEST_DIR/coop_e.probs \
		 default default > $OUT_DIR/seq_AB_eb_coopb_coope_10to30
##-----------------------------------------------------------------
##-----------------------------------------------------------------
##==============================================================================
##==============================================================================
## HETEROTYPIC TWO SECONDARIES
export GOMER_HOME='/home/josh/GOMER/'
GOMER='/home/josh/GOMER/python_code/gomer.py'

$GEN_SEQ $TEST_DIR/base_sequence.fsa $TEST_DIR/template_ABC $TEST_DIR/coop_a.probs $TEST_DIR/coop_b.probs $TEST_DIR/coop_z.probs> $SEQ_DIR/seq_ABC_abz
$GEN_SEQ $TEST_DIR/base_sequence.fsa $TEST_DIR/template_ABC $TEST_DIR/coop_a.probs $TEST_DIR/coop_b.probs $TEST_DIR/coop_d.probs> $SEQ_DIR/seq_ABC_abd

$GOMER_CMD --sequence_feature $SEQ_DIR/seq_ABC_abz $TEST_DIR/coop_b.probs \
		 $GOMER_BASE/input_files/regulated_genes/empty.list \
		 --coop_kappa=simple_square_coop_model_gomer.py \
		 "min_distance=10;max_distance=30" $TEST_DIR/coop_a.probs \
		 default default > $OUT_DIR/seq_ABC_abz__a_b_10to30

$GOMER_CMD --sequence_feature $SEQ_DIR/seq_ABC_abd $TEST_DIR/coop_b.probs \
		 $GOMER_BASE/input_files/regulated_genes/empty.list \
		 --coop_kappa=simple_square_coop_model_gomer.py \
		 "min_distance=10;max_distance=30" $TEST_DIR/coop_a.probs \
		 default default > $OUT_DIR/seq_ABC_abd__a_b_10to30

$GOMER_CMD --sequence_feature $SEQ_DIR/seq_ABC_abd $TEST_DIR/coop_b.probs \
		 $GOMER_BASE/input_files/regulated_genes/empty.list \
		 --coop_kappa=simple_square_coop_model_gomer.py \
		 "min_distance=10;max_distance=30" $TEST_DIR/coop_a.probs \
		 default default								\
		 \
		 --coop_kappa=simple_square_coop_model_gomer.py \
		 "min_distance=10;max_distance=30" $TEST_DIR/coop_d.probs \
		 default default > $OUT_DIR/seq_ABC_abd__a_b_d_10to30


$GOMER_CMD --sequence_feature $SEQ_DIR/seq_ABC_abd $TEST_DIR/coop_b.probs \
		 $GOMER_BASE/input_files/regulated_genes/empty.list \
		 --coop_kappa=simple_square_coop_model_gomer.py \
		 "min_distance=0;max_distance=40" $TEST_DIR/coop_a.probs \
		 default default							   \
		 \
		 --coop_kappa=simple_square_coop_model_gomer.py \
		 "min_distance=0;max_distance=40" $TEST_DIR/coop_d.probs \
		 default default > $OUT_DIR/seq_ABC_abd__a_b_d_0to40
##==============================================================================
##++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
rm -f $CACHE_DIR/*
#------------------------------------------------------
#------------------------------------------------------
rm -f $OUT_DIR/coop_regress_diff

#-------------------------------------------------------------------------------
for FILENAME in                     \
	"seq_ABC_abd"                   \
	"seq_ABC_abd__a_b_10to30"		\
	"seq_ABC_abd__a_b_d_0to40"		\
	"seq_ABC_abd__a_b_d_10to30"		\
	"seq_ABC_abe"					\
	"seq_ABC_abz"					\
	"seq_ABC_abz__a_b_10to30"		\
	"seq_AB_aa"						\
	"seq_AB_ab"						\
	"seq_AB_ab_0to30"				\
	"seq_AB_ab_0to40"				\
	"seq_AB_ab_10to30"				\
	"seq_AB_ab_coopb_coopb10to30"	\
	"seq_AB_ab_coopb_coopd_10to30"	\
	"seq_AB_bb"						\
	"seq_AB_bb_10to30"				\
	"seq_AB_bb__no_coop"			\
	"seq_AB_db"						\
	"seq_AB_db_10to30"				\
	"seq_AB_db_coopb_coopa_10to30"	\
	"seq_AB_dd"						\
	"seq_AB_dd__no_coop"			\
	"seq_AB_eb"                     \
	"seq_AB_eb_coopb_coopa_10to30"	\
	"seq_AB_eb_coopb_coope_10to30"	\
	"seq_AB_ee"						\
	"seq_AB_ee__no_coop"			\
	"seq_AB_zb"						\
	"seq_AB_zb_coopb_coopa_10to30"	\
	"seq_A_a"						\
	"seq_A_a_10to30"				\
	"seq_A_a__no_coop"				\
	"seq_A_a_coopz_10to30"			\
	"seq_A_a_coopz_no_coop"			\
	"seq_A_b"						\
	"seq_A_b_0to30"					\
	"seq_A_b_0to40"					\
	"seq_A_b_10to30"				\
	"seq_A_b__no_coop"				\
	"seq_A_z"						\
	;
	do
	STD_FILE="$STD_DIR"/STD_"$FILENAME"
	NEW_FILE="$OUT_DIR"/"$FILENAME"
	echo "STD_FILE: $STD_FILE"
	echo "NEW_FILE: $NEW_FILE"
	echo "--------------------------------------------" >> $OUT_DIR/coop_regress_diff ;
	COMMAND="diff -s $STD_FILE $NEW_FILE"
	echo $COMMAND >> $OUT_DIR/coop_regress_diff ;
	$COMMAND >> $OUT_DIR/coop_regress_diff ;
done
#-------------------------------------------------------------------------------

#-------------------------------------------------------------------------------
#-------------------------------------------------------------------------------
$GOMER_CMD  $SEQ_TABLE "$MATRIX_DIR"/"$PRIMARY_MATRIX" $REGULATED $FEATURE_TYPES $REG_REGION $COOP > "$OUT_DIR"/Nrd1_Nab3_coop
#-------------------------------------------------------------------------------
#-------------------------------------------------------------------------------

for COMMAND in                                                                 \
	"diff -s $STD_DIR/STD_Nrd1_Nab3_coop $OUT_DIR/Nrd1_Nab3_coop"     \
	;
	do
	echo "--------------------------------------------" >> $OUT_DIR/coop_regress_diff ;
	echo $COMMAND >> $OUT_DIR/coop_regress_diff ;
	$COMMAND >> $OUT_DIR/coop_regress_diff ;
done


if [ -a "$FAST_CACHE" ] ; then 
	rmdir $FAST_CACHE
fi

