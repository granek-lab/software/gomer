if [ "queen" == `hostname` ]
then
    echo "REFUSING TO RUN ON QUEEN, MUST BE RUN ON DRONES" ;  
elif [ "drone1" == `hostname` -o 'drone2' == `hostname` -o 'drone3' == `hostname` ]
then
	mkdir /local/granek
	mkdir /local/granek/GOMER/
	cp -r /nfs/fs/clarke/granek/GOMER/python_code/ /local/granek/GOMER/
	mkdir /local/granek/GOMER_DATA
	mkdir /local/granek/GOMER_DATA/cache

	/local/granek/GOMER/python_code/gomer.py /nfs/fs/clarke/granek/GOMER/input_files/sequence_file_tables/cerevisiae_chromosome_table_with_mito.hive_slow.jan_2004 /nfs/fs/clarke/granek/GOMER/projects/young_all_TFs/FDR_runs/clusterGOMER_runfiles3_032204/CAD1_1.trainingset.seq.12.bp.reformat.pwm /nfs/fs/clarke/granek/GOMER/projects/young_all_TFs/FDR_runs/clusterGOMER_runfiles3_032204/CAD1_1.gomerset -D --coordinate_feature=/nfs/fs/clarke/granek/GOMER/intergenic_primers/jan_29_2004/Intergenic_Coordinates.Young_subset -c /nfs/fs/clarke/granek/GOMER/input_files/config/gomer_config.hive -r coordinate_linear_decay_reg_region_model_gomer.py "flank=0;tail=0" > /nfs/fs/clarke/granek/check_drones.output
	
	rm -rf /local/granek
fi