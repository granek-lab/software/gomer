echo "RUN FROM AGENT!!!!"

GOMER_TARFILE='/tmp/GOMER_distribution.tgz'
ALL_TF_TARFILE='/tmp/all_TFs.tgz'
SCRIPTS='/local/granek/GOMER/scripts'

for COMMAND in \
	"/home/josh/GOMER/scripts/make_distribution.sh"                    \
	"ssh granek@queen bash gomer_scripts/hive_clean_gomer.sh"   \
	"ssh granek@queen rm GOMER_distribution.tgz"                \
	"scp $GOMER_TARFILE granek@queen:~/"           \
	"ssh granek@queen bash gomer_scripts/hive_install_gomer.sh" \
	                                                            \
	"bash /home/josh/GOMER/scripts/package_allTFs.sh"           \
	"scp $ALL_TF_TARFILE granek@queen:~/"                       \
	"ssh granek@queen bash $SCRIPTS/hive_install_allTFs.sh" \
	;
	do
	echo $COMMAND
	$COMMAND
	
done
