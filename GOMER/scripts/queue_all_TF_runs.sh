# for PREFIX in "A" "B" "C" "D" "E" "F" "G" "H" "I" "J" "K" "L" "M" "N" "O" "P" "Q" "R" "S" "T" "U" "V" "W" "X" "Y" "Z";

# TAIL="1000"
TAIL="250"
HOME_DIR="/nfs/fs/clarke/granek"
# for PREFIX in  "GAL4" "GCN4" "RAP1" "YAP1" ;
# for PREFIX in "ABF1" "ACE2" "ARO8" "BAS1" "CAD1" "DAL8" "DIG1" "FHL1" "FKH1" "FZF1" "GAT3" "GCR2" "HAP4" "HIR1" "HSF1" "IME4" "MBP1" "MCM1" "NDD1" "NRG1" "PHD1" "REB1" "RLM1" "ROX1" "SKN7" "SMP1" "STE1" "SUM1" "SWI4" "SWI5" "SWI6" "YAP5" "YAP6" ;
for PREFIX in "CIN5" "FKH2" "GAL4" "GCN4" "PDR1" "PHO4" "RAP1" "YAP1" ;
    do
    SCRIPT_NAME="$HOME_DIR/q"_"$PREFIX"_"$TAIL.sh"
    if [ -a $SCRIPT_NAME ] ; then 
	rm -f $SCRIPT_NAME ;
    fi

    echo "/local/granek/GOMER/scripts/all_TF_run.py $TAIL $PREFIX" > $SCRIPT_NAME
    echo $SCRIPT_NAME
    cat $SCRIPT_NAME
    qsub $SCRIPT_NAME
    rm  $SCRIPT_NAME
    # echo "/local/granek/GOMER/scripts/all_TF_run.py $PREFIX" | qsub -q short
    # echo "/local/granek/GOMER/scripts/all_TF_run.py $TAIL $PREFIX" | qsub
    # echo "/local/granek/GOMER/scripts/all_TF_run.py $TAIL $PREFIX" | cat
done



