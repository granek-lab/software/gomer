G_HOME="c:/josh/GOMER"
CONF="-c c:/josh/GOMER/input_files/config/gomer_config_home"

rm -f $G_HOME/outputs/regress_diff

echo "--------------------------------------------------------------------------"  >> $G_HOME/outputs/regress_diff
echo "diff -s $G_HOME/outputs/STD_1e7_C9.precached $G_HOME/outputs/full_1e7_C9.precached" >> $G_HOME/outputs/regress_diff
diff -s $G_HOME/outputs/STD_1e7_C9.precached $G_HOME/outputs/full_1e7_C9.precached >> $G_HOME/outputs/regress_diff

echo "--------------------------------------------------------------------------"  >> $G_HOME/outputs/regress_diff
echo "diff -s $G_HOME/outputs/STD_C9 $G_HOME/outputs/full_C9" >> $G_HOME/outputs/regress_diff
diff -s $G_HOME/outputs/STD_C9 $G_HOME/outputs/full_C9 >> $G_HOME/outputs/regress_diff

echo "--------------------------------------------------------------------------"  >> $G_HOME/outputs/regress_diff
echo "diff -s $G_HOME/outputs/STD_1e7_C9.refiltered $G_HOME/outputs/full_1e7_C9.refiltered" >> $G_HOME/outputs/regress_diff
diff -s $G_HOME/outputs/STD_1e7_C9.refiltered $G_HOME/outputs/full_1e7_C9.refiltered >> $G_HOME/outputs/regress_diff

echo "--------------------------------------------------------------------------"  >> $G_HOME/outputs/regress_diff
echo "diff -s $G_HOME/outputs/STD_coordinate $G_HOME/outputs/full_coordinate_tab" >> $G_HOME/outputs/regress_diff
diff -s $G_HOME/outputs/STD_coordinate $G_HOME/outputs/full_coordinate_tab >> $G_HOME/outputs/regress_diff

echo "--------------------------------------------------------------------------"  >> $G_HOME/outputs/regress_diff
echo "diff -s $G_HOME/outputs/STD_coordinate $G_HOME/outputs/full_coordinate_csv" >> $G_HOME/outputs/regress_diff
diff -s $G_HOME/outputs/STD_coordinate $G_HOME/outputs/full_coordinate_csv >> $G_HOME/outputs/regress_diff

echo "--------------------------------------------------------------------------"  >> $G_HOME/outputs/regress_diff
echo "diff -s $G_HOME/outputs/STD_sequence $G_HOME/outputs/full_sequence" >> $G_HOME/outputs/regress_diff
diff -s $G_HOME/outputs/STD_sequence $G_HOME/outputs/full_sequence >> $G_HOME/outputs/regress_diff

