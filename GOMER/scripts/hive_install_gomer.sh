GOMER_BASE="/local/granek"
GOMER_CACHE_DIR="$GOMER_BASE/GOMER_DATA/cache"

if ! [ -a "$GOMER_CACHE_DIR" ] ; then 
	mkdirhier $GOMER_CACHE_DIR ; 
fi

bash $HOME/gomer_scripts/hive_clean_gomer.sh
tar --directory="$GOMER_BASE" -zxf ~/GOMER_distribution.tgz

##--------------------------------------
# for BASE in             \
# 	"/nfs/drone2/local" \
# 	"/nfs/drone3/local" \
# 	"/nfs/drone1/local" \
# 	;
# 	do
# 	cd $BASE
# 	pwd
# 	ls -ltr $BASE/granek
# 	rm -rf  $BASE/granek
# 	ls -ltr $BASE/granek
# done
